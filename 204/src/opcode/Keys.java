/**
 * rsc
 * 10-02-2016
 */

package opcode;

import java.math.BigInteger;

public class Keys {

    public static BigInteger exponent204 = new BigInteger("58778699976184461502525193738213253649000149147835990136706041084440742975821");
    //private static BigInteger modulus204 = new BigInteger("7162900525229798032761816791230527296329313291232324290237849263501208207972894053929065636522363163621000728841182238772712427862772219676577293600221789");
    public static BigInteger modulus204 = new BigInteger("7656522762491711741880224224809835569769759737077076094091609307381193032090602256314159126169417567841597729801408692196383745596665658895073411749475443");

    public static BigInteger exponent234 = new BigInteger("10001", 16);
    public static BigInteger modulus234 = new BigInteger("ca950472ae9765185bf290ff54a823b1d29b46dc3cf676203bb871efa278d9c49e16defc53ff479305123454505082f4700b0da381047f51b872f9bbeea653f21fd248a10ff5239b30234add35913cb6068d316edd418611334ae047fcd9acb7b0c13b30393a26204dc85183e0a95555c01bee800440e974bb9b441f464f4057", 16);

    public static BigInteger getExponent(int version) {
        switch(version) {
            case 235:
                return exponent234;
            default:
                return exponent204;
        }
    }

    public static BigInteger getModulus(int version) {
        switch(version) {
            case 235:
                return modulus234;
            default:
                return modulus204;
        }
    }
}
