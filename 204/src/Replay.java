/**
 *	rscplus
 *
 *	This file is part of rscplus.
 *
 *	rscplus is free software: you can redistribute it and/or modify
 *	it under the terms of the GNU General Public License as published by
 *	the Free Software Foundation, either version 3 of the License, or
 *	(at your option) any later version.
 *
 *	rscplus is distributed in the hope that it will be useful,
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	GNU General Public License for more details.
 *
 *	You should have received a copy of the GNU General Public License
 *	along with rscplus.  If not, see <http://www.gnu.org/licenses/>.
 *
 *	Authors: see <https://github.com/OrN/rscplus>
 */


import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class Replay {
    // If we ever change replays in a way that breaks backwards compatibility, we need to increment this
    public static int VERSION = 0;

    static DataOutputStream output = null;
    static DataOutputStream input = null;
    static DataOutputStream keys = null;
    static DataOutputStream keyboard = null;
    static DataOutputStream mouse = null;

    static DataInputStream play_keys = null;
    static DataInputStream play_keyboard = null;
    static DataInputStream play_mouse = null;

    public static final byte KEYBOARD_TYPED = 0;
    public static final byte KEYBOARD_PRESSED = 1;
    public static final byte KEYBOARD_RELEASED = 2;

    public static final byte MOUSE_CLICKED = 0;
    public static final byte MOUSE_ENTERED = 1;
    public static final byte MOUSE_EXITED = 2;
    public static final byte MOUSE_PRESSED = 3;
    public static final byte MOUSE_RELEASED = 4;
    public static final byte MOUSE_DRAGGED = 5;
    public static final byte MOUSE_MOVED = 6;
    public static final byte MOUSE_WHEEL_MOVED = 7;

    public static final int DEFAULT_PORT = 43594;

    public static boolean isPlaying = false;
    public static boolean isRecording = false;
    public static boolean paused = false;
    public static boolean closeDialogue = false;

    // Hack for player position
    public static boolean ignoreFirstMovement = true;

    public static int fps = 50;
    public static float fpsPlayMultiplier = 1.0f;
    public static float prevFPSPlayMultiplier = fpsPlayMultiplier;
    public static int frame_time_slice;
    public static int connection_port;

    public static Thread replayThread = null;

    public static int replay_version;
    public static int client_version;
    public static int prevPlayerX;
    public static int prevPlayerY;

    public static int timestamp;
    public static int timestamp_kb_input;
    public static int timestamp_mouse_input;

    public static boolean started_record_kb_mouse = true;

    public static void incrementTimestamp() {
        timestamp++;

        // EOF is -1
        if (timestamp == -1) {
            timestamp = 0;
        }
    }


    public static void initializeReplayRecording(String username_login) {
        // No username specified, exit
        if (username_login.length() == 0)
            return;

        String timeStamp = new SimpleDateFormat("MM-dd-yyyy HH.mm.ss").format(new Date());

        String recordingDirectory = "replay/" + username_login;
        try{Files.createDirectories(Paths.get(recordingDirectory));}catch(Exception e){System.err.println("unable to create replay dir");}
        recordingDirectory = recordingDirectory + "/" + timeStamp;
        try{Files.createDirectories(Paths.get(recordingDirectory));}catch(Exception e){System.err.println("unable to create replay dir");}

        try {
            // Write out version information
            DataOutputStream version = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(recordingDirectory + "/version.bin"))));
            version.writeInt(Replay.VERSION);
            version.writeInt(235);
            version.close();

            output = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/out.bin.gz")))));
            input = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/in.bin.gz")))));
            keys = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(recordingDirectory + "/keys.bin"))));
            keyboard = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/keyboard.bin.gz")))));
            mouse = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(new FileOutputStream(new File(recordingDirectory + "/mouse.bin.gz")))));
            started_record_kb_mouse = true; //need this to know whether or not to close the file if the user changes settings mid-recording
            timestamp = 0;

            System.err.println("Replay recording started");
        } catch (Exception e) {
            output = null;
            input = null;
            keys = null;
            keyboard = null;
            mouse = null;
            System.err.println("Unable to create replay files");
            return;
        }

        isRecording = true;
    }

    public static void closeReplayRecording() {
        if (input == null)
            return;

        try {
            // Write EOF values
            input.writeInt(-1);
            output.writeInt(-1);

            output.close();
            input.close();
            keys.close();
            if (started_record_kb_mouse) {
                keyboard.writeInt(-1);
                mouse.writeInt(-1);
                keyboard.close();
                mouse.close();
            }

            output = null;
            input = null;
            keys = null;
            keyboard = null;
            mouse = null;

            System.err.println("Replay recording stopped");
        } catch (Exception e) {
            output = null;
            input = null;
            keys = null;
            keyboard = null;
            mouse = null;
            System.err.println("Unable to close replay files");
            return;
        }

        isRecording = false;
    }

    public static void update() {
        // Increment the replay timestamp
        if (Replay.isRecording) {
            Replay.incrementTimestamp();
        }
    }

    public static boolean isValid(String path) {
        return (new File(path + "/in.bin.gz").exists() && new File(path + "/keys.bin").exists() && new File(path + "/version.bin").exists());
    }

    public static void resetFrameTimeSlice() {
        frame_time_slice = 1000 / fps;
    }

    // adjusts frame time slice
    public static int getFrameTimeSlice() {
        return frame_time_slice;
    }

    public static void updateFrameTimeSlice() {
        if (paused)
            return;

        if (isPlaying) {
            frame_time_slice = 1000 / ((int)(fps * fpsPlayMultiplier));
            return;
        }

        frame_time_slice = 1000 / fps;
    }

    public static int getFPS() {
        if (isPlaying) {
            return (int)(fps * fpsPlayMultiplier);
        }

        return fps;
    }

    public static void resetPort() {
        connection_port = Replay.DEFAULT_PORT;
    }

    public static void shutdown_error() {
        closeReplayRecording();
        System.err.println("Recording has been stopped because of an error");
        System.err.println("Please log back in to start recording again");
    }

    public static void dumpKeyboardInput(int keycode, byte event, char keychar, int modifier) {
        if (keyboard == null)
            return;

        try {
            keyboard.writeInt(timestamp);
            keyboard.writeByte(event);
            keyboard.writeChar(keychar);
            keyboard.writeInt(keycode);
            keyboard.writeInt(modifier);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }
    }

    public static void dumpMouseInput(byte event, int x, int y, int rotation, int modifier, int clickCount, int scrollType, int scrollAmount, boolean popupTrigger, int button) {
        if (mouse == null)
            return;

        try {
            mouse.writeInt(timestamp);
            mouse.writeByte(event);
            mouse.writeInt(x);
            mouse.writeInt(y);
            mouse.writeInt(rotation);
            mouse.writeInt(modifier);
            mouse.writeInt(clickCount);
            mouse.writeInt(scrollType);
            mouse.writeInt(scrollAmount);
            mouse.writeBoolean(popupTrigger);
            mouse.writeInt(button);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }
    }

    public static void dumpRawInputStream(byte[] b, int n, int n2, int n5, int bytesread) {
        if (input == null)
            return;

        int off = n2 + n5;

        try {
            input.writeInt(timestamp);
            input.writeInt(bytesread);
            input.write(b, off, bytesread);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }
    }

    public static void dumpRawOutputStream(byte[] b, int off, int len) {
        if (output == null)
            return;

        try {
            boolean isLogin = false;
            int pos = -1;
            byte[] out_b = null;
            // for the first bytes if byte == (byte)Client.version, 4 bytes before indicate if its
            // login or reconnect and 5 its what determines if its login-related
            for (int i = off + 5; i < off + Math.min(15, len); i++) {
                if (b[i] == (byte)235 && b[i - 5] == 0) {
                    isLogin = true;
                    pos = i + 1;
                    out_b = b.clone();
                    break;
                }
            }
            if (isLogin && pos != -1) {
                for (int i = pos; i < off + len; i++) {
                    out_b[i] = 0;
                }

                output.writeInt(timestamp);
                output.writeInt(len);
                output.write(out_b, off, len);
                return;
            }

            output.writeInt(timestamp);
            output.writeInt(len);
            output.write(b, off, len);
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }
    }

    public static int hookXTEAKey(int key) {
        if (play_keys != null) {
            try {
                return play_keys.readInt();
            } catch (Exception e) {
                e.printStackTrace();
                shutdown_error();
                return key;
            }
        }

        if (keys == null)
            return key;

        try {
            keys.writeInt(key); // data length
        } catch (Exception e) {
            e.printStackTrace();
            shutdown_error();
        }

        return key;
    }
}
