package com.aposbot;

import java.awt.Canvas;
import java.awt.Font;
import java.awt.Image;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public final class Constants {

    public static final String[] SKILL_NAMES = {
            "Attack", "Defense", "Strength", "Hits", "Ranged", "Prayer", "Magic",
            "Cooking", "Woodcut", "Fletching", "Fishing", "Firemaking", "Crafting",
            "Smithing", "Mining", "Herblaw", "Agility", "Thieving"
    };

    public static final String[] SPELL_NAMES = {
            "Wind strike", "Confuse", "Water strike", "Enchant lvl-1 amulet",
            "Earth strike", "Weaken", "Fire strike", "Bones to bananas",
            "Wind bolt", "Curse", "Low level alchemy", "Water bolt",
            "Varrock teleport", "Enchant lvl-2 amulet", "Earth bolt",
            "Lumbridge teleport", "Telekinetic grab", "Fire bolt",
            "Falador teleport", "Crumble undead", "Wind blast", "Superheat item",
            "Camelot teleport", "Water blast", "Enchant lvl-3 amulet",
            "Iban blast", "Ardougne teleport", "Earth blast", "High level alchemy",
            "Charge Water Orb", "Enchant lvl-4 amulet", "Watchtower teleport",
            "Fire blast", "Claws of Guthix", "Saradomin strike",
            "Flames of Zamorak", "Charge earth Orb", "Wind wave",
            "Charge Fire Orb", "Water wave", "Charge air Orb", "Vulnerability",
            "Enchant lvl-5 amulet", "Earth wave", "Enfeeble", "Fire wave", "Stun",
            "Charge"
    };

    public static final List<Image> ICONS = new ArrayList<>();

    public static final Charset UTF_8 = Charset.forName("UTF-8");
    public static final Charset DEFAULT_CHARSET = Charset.defaultCharset();

    public static final Random RANDOM = new BobRand();

    public static final int OP_NPC_ATTACK = 190;
    public static final int OP_NPC_ACTION = 202;
    public static final int OP_NPC_TALK = 153;
    public static final int OP_NPC_USEWITH = 135;
    public static final int OP_NPC_CAST = 50;

    public static final int OP_PLAYER_CAST = 229;
    public static final int OP_PLAYER_ATTACK = 171;
    public static final int OP_PLAYER_TRADE = 142;
    public static final int OP_PLAYER_FOLLOW = 165;
    public static final int OP_PLAYER_USEWITH = 113;

    public static final int OP_INV_ACTION = 90;
    public static final int OP_INV_DROP = 246;
    public static final int OP_INV_EQUIP = 169;
    public static final int OP_INV_UNEQUIP = 170;
    public static final int OP_INV_USEWITH = 91;
    public static final int OP_INV_CAST = 4;

    public static final int OP_GITEM_TAKE = 247;
    public static final int OP_GITEM_USEWITH = 53;
    public static final int OP_GITEM_CAST = 249;

    public static final int OP_OBJECT_USEWITH = 115;
    public static final int OP_OBJECT_ACTION1 = 136;
    public static final int OP_OBJECT_ACTION2 = 79;

    public static final int OP_BOUND_USEWITH = 161;
    public static final int OP_BOUND_ACTION1 = 14;
    public static final int OP_BOUND_ACTION2 = 127;

    // don't forget the trap
    public static final int OP_BANK_DEPOSIT = 23;
    public static final int OP_BANK_WITHDRAW = 22;
    public static final int OP_BANK_CLOSE = 212;

    public static final int OP_TRADE_ACCEPT = 55;
    public static final int OP_TRADE_CONFIRM = 104;
    public static final int OP_TRADE_DECLINE = 230;

    public static final int OP_SHOP_BUY = 236;
    public static final int OP_SHOP_SELL = 221;
    public static final int OP_SHOP_CLOSE = 166;

    public static final int OP_PRAYER_ENABLE = 60;
    public static final int OP_PRAYER_DISABLE = 254;

    public static final int OP_SELF_CAST = 137;
    public static final int OP_DIALOG_ANSWER = 116;
    public static final int OP_SET_COMBAT_STYLE = 29;
    public static final int OP_LOGOUT = 102;
    public static final Font UI_FONT = getUIFont();

    public static final String DEFAULT_JAR = "rsclassic.jar";

    private Constants() {
    }

    private static Font getUIFont() {
        Font _default = new Canvas().getFont();
        int style = Font.PLAIN;
        int size = 12;
        if (_default != null) {
            style = _default.getStyle();
            size = _default.getSize();
        }
        String name = Font.SANS_SERIF;
        return new Font(name, style, size);
    }
}
