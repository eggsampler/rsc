import script.apos.PaintListener;
import client.mudclient;
import script.apos.Script;
public final class HopTest extends Script {

	public HopTest(mudclient ex) {
		super(ex);
	}

	@Override
	public void init(String params) {
	}

	@Override
	public int main() {
		autohop(false);
		stopScript();
		return 1000;
	}

	@Override
	public void paint() {
	}

	@Override
	public void onServerMessage(String str) {
	}
}
