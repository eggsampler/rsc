for advanced params you need to launch it from the command line
> java -jar bot204.jar --username ... --password ...

params:
--server classic1.runescape.com
--world 1
--username username for prefill
--password password for prefill
--width width
--height height

hotkeys:
F2 zoom
F3 experimental hud shit inspired by rscplus (runs scripulis/hud.js)
F4 print packet opcodes in terminal
F11 start/stop apos script
F12 select apos script

for apos scripts
> cd Scripts
> sed -i '1s/^/import script.apos.Script;\n/' *
- means the scripts need to have "import script.apos.Script;"
> sed -i 's/(Extension /(mudclient /' *
> sed -i 's/(IClient /(mudclient /' *
- scripts must refer to mudclient instead of Extension or IClient
> sed -i '1s/^/import client.mudclient;\n/' *
- scripts need to have "import client.mudclient"
> sed -i 's/AutoLogin.setAutoLogin/setAutoLogin/' *
- scripts need to have just "setAutoLogin" instead of "AutoLogin.setAutoLogin"
> sed -i '1s/^/import script.apos.PaintListener;\n/' *
- scripts need to have "import script.apos.PaintListener" IF NEEDED

OR you can just use a text editor to edit the scripts


the client can compile some scripts on the fly when you select them (apparently ones without dependencies on other classes, unless those dependencies are already precompiled, like how S_Fighter depends on PathWalker)
but if that doesnt work compile manually:
> javac -classpath "Scripts;bot204.jar" Scripts/*.java

copy classes to Scripts/ after compilation
