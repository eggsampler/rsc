// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package jagex;

import java.io.*;
import java.net.URL;

// Referenced classes of package jagex:
//            n

public class Utility {

    public static URL appletCodeBase = null;
    static String censoredWords1[] = {
            "fuck", "bastard", "lesbian", "prostitut", "spastic", "vagina", "retard", "arsehole", "asshole", "tosser",
            "homosex", "hetrosex", "hitler", "urinate"
    };
    static String censoredWords2[] = {
            "shit", "lesbo", "phuck", "bitch", "penis", "bisex", "sperm", "rapist", "shag", "slag",
            "slut", "clit", "cunt", "piss", "nazi", "urine"
    };
    static String censoredWords3[] = {
            "wank", "naked", "fag", "niga", "nige", "gay", "rape", "cock", "homo", "twat",
            "arse", "crap"
    };
    static int pdb;
    static int qdb[] = new int[1000];
    static int rdb[] = new int[1000];
    static String sdb[] = new String[1000];
    static int tdb[] = new int[1000];
    static int udb[] = new int[1000];

    public Utility() {
    }

    public static InputStream openStream(String s)
            throws IOException {
        Object obj;
        if (appletCodeBase == null) {
            obj = new FileInputStream(s);
        } else {
            URL url = new URL(appletCodeBase, s);
            obj = url.openStream();
        }
        return ((InputStream) (obj));
    }

    public static void loadData(String s, byte abyte0[], int i)
            throws IOException {
        if (s.startsWith("../gamedata")) {
            s = "./data40" + s.substring(2);
        }
        InputStream inputstream = openStream(s);
        DataInputStream datainputstream = new DataInputStream(inputstream);
        try {
            datainputstream.readFully(abyte0, 0, i);
        } catch (EOFException _ex) {
        }
        datainputstream.close();
    }

    public static int getUnsignedByte(byte byte0) {
        return byte0 & 0xff;
    }

    public static int getUnsignedShort(byte abyte0[], int i) {
        return ((abyte0[i] & 0xff) << 8) + (abyte0[i + 1] & 0xff);
    }

    public static int getUnsignedInt(byte abyte0[], int i) {
        return ((abyte0[i] & 0xff) << 24) + ((abyte0[i + 1] & 0xff) << 16) + ((abyte0[i + 2] & 0xff) << 8) + (abyte0[i + 3] & 0xff);
    }

    public static long getUnsignedLong(byte abyte0[], int i) {
        return (((long) getUnsignedInt(abyte0, i) & 0xffffffffL) << 32) + ((long) getUnsignedInt(abyte0, i + 4) & 0xffffffffL);
    }

    public static int getSignedShort(byte abyte0[], int i) {
        int j = getUnsignedByte(abyte0[i]) * 256 + getUnsignedByte(abyte0[i + 1]);
        if (j > 32767)
            j -= 0x10000;
        return j;
    }

    public static String formatAuthString(String s, int i) {
        String s1 = "";
        for (int j = 0; j < i; j++)
            if (j >= s.length()) {
                s1 = s1 + " ";
            } else {
                char c = s.charAt(j);
                if (c >= 'a' && c <= 'z')
                    s1 = s1 + c;
                else if (c >= 'A' && c <= 'Z')
                    s1 = s1 + c;
                else if (c >= '0' && c <= '9')
                    s1 = s1 + c;
                else
                    s1 = s1 + '_';
            }

        return s1;
    }

    public static long username2hash(String s) {
        String s1 = "";
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (c >= 'a' && c <= 'z')
                s1 = s1 + c;
            else if (c >= 'A' && c <= 'Z')
                s1 = s1 + (char) ((c + 97) - 65);
            else if (c >= '0' && c <= '9')
                s1 = s1 + c;
            else
                s1 = s1 + ' ';
        }

        s1 = s1.trim();
        if (s1.length() > 12)
            s1 = s1.substring(0, 12);
        long l = 0L;
        for (int j = 0; j < s1.length(); j++) {
            char c1 = s1.charAt(j);
            l *= 37L;
            if (c1 >= 'a' && c1 <= 'z')
                l += (1 + c1) - 97;
            else if (c1 >= '0' && c1 <= '9')
                l += (27 + c1) - 48;
        }

        return l;
    }

    public static String hash2username(long l) {
        String s = "";
        while (l != 0L) {
            int i = (int) (l % 37L);
            l /= 37L;
            if (i == 0)
                s = " " + s;
            else if (i < 27) {
                if (l % 37L == 0L)
                    s = (char) ((i + 65) - 1) + s;
                else
                    s = (char) ((i + 97) - 1) + s;
            } else {
                s = (char) ((i + 48) - 27) + s;
            }
        }
        return s;
    }

    public static byte[] readDataFile(String s)
            throws IOException {
        s = "./data40/" + s;
        int i = 0;
        int j = 0;
        int k = 0;
        byte abyte0[] = null;
        while (i < 2)
            try {
                if (i == 1)
                    s = s.toUpperCase();
                InputStream inputstream = openStream(s);
                DataInputStream datainputstream = new DataInputStream(inputstream);
                byte abyte2[] = new byte[6];
                datainputstream.readFully(abyte2, 0, 6);
                j = ((abyte2[0] & 0xff) << 16) + ((abyte2[1] & 0xff) << 8) + (abyte2[2] & 0xff);
                k = ((abyte2[3] & 0xff) << 16) + ((abyte2[4] & 0xff) << 8) + (abyte2[5] & 0xff);
                int l = 0;
                abyte0 = new byte[k];
                int i1;
                for (; l < k; l += i1) {
                    i1 = k - l;
                    if (i1 > 1000)
                        i1 = 1000;
                    datainputstream.readFully(abyte0, l, i1);
                }

                i = 2;
                datainputstream.close();
            } catch (IOException _ex) {
                i++;
            }
        if (k != j) {
            byte abyte1[] = new byte[j];
            BZLib.decompress(abyte1, j, abyte0, k, 0);
            return abyte1;
        } else {
            return abyte0;
        }
    }

    public static int getDataFileOffset(String s, byte abyte0[]) {
        int i = abyte0[0] * 256 + abyte0[1];
        int j = 0;
        s = s.toUpperCase();
        for (int k = 0; k < s.length(); k++)
            j = (j * 61 + s.charAt(k)) - 32;

        int l = 2 + i * 10;
        for (int i1 = 0; i1 < i; i1++) {
            int j1 = (abyte0[i1 * 10 + 2] & 0xff) * 0x1000000 + (abyte0[i1 * 10 + 3] & 0xff) * 0x10000 + (abyte0[i1 * 10 + 4] & 0xff) * 256 + (abyte0[i1 * 10 + 5] & 0xff);
            int k1 = (abyte0[i1 * 10 + 9] & 0xff) * 0x10000 + (abyte0[i1 * 10 + 10] & 0xff) * 256 + (abyte0[i1 * 10 + 11] & 0xff);
            if (j1 == j)
                return l;
            l += k1;
        }

        return 0;
    }

    public static byte[] unpackData(String filename, int i, byte archiveData[], byte fileData[]) {
        int numEntries = archiveData[0] * 256 + archiveData[1];
        int wantedHash = 0;
        filename = filename.toUpperCase();
        for (int l = 0; l < filename.length(); l++)
            wantedHash = (wantedHash * 61 + filename.charAt(l)) - 32;

        int offset = 2 + numEntries * 10;
        for (int j1 = 0; j1 < numEntries; j1++) {
            int fileHash = (archiveData[j1 * 10 + 2] & 0xff) * 0x1000000 + (archiveData[j1 * 10 + 3] & 0xff) * 0x10000 + (archiveData[j1 * 10 + 4] & 0xff) * 256 + (archiveData[j1 * 10 + 5] & 0xff);
            int fileSize = (archiveData[j1 * 10 + 6] & 0xff) * 0x10000 + (archiveData[j1 * 10 + 7] & 0xff) * 256 + (archiveData[j1 * 10 + 8] & 0xff);
            int fileSizeCompressed = (archiveData[j1 * 10 + 9] & 0xff) * 0x10000 + (archiveData[j1 * 10 + 10] & 0xff) * 256 + (archiveData[j1 * 10 + 11] & 0xff);
            if (fileHash == wantedHash) {
                if (fileSize != fileSizeCompressed) {
                    BZLib.decompress(fileData, fileSize, archiveData, fileSizeCompressed, offset);
                } else {
                    for (int j2 = 0; j2 < fileSize; j2++)
                        fileData[j2] = archiveData[offset + j2];

                }
                return fileData;
            }
            offset += fileSizeCompressed;
        }

        return null;
    }

    public static byte[] unpackData(String filename, int i, byte archiveData[]) {
        int numEntries = archiveData[0] * 256 + archiveData[1];
        int wantedHash = 0;
        filename = filename.toUpperCase();
        for (int l = 0; l < filename.length(); l++)
            wantedHash = (wantedHash * 61 + filename.charAt(l)) - 32;

        int offset = 2 + numEntries * 10;
        for (int j1 = 0; j1 < numEntries; j1++) {
            int fileHash = (archiveData[j1 * 10 + 2] & 0xff) * 0x1000000 + (archiveData[j1 * 10 + 3] & 0xff) * 0x10000 + (archiveData[j1 * 10 + 4] & 0xff) * 256 + (archiveData[j1 * 10 + 5] & 0xff);
            int fileSize = (archiveData[j1 * 10 + 6] & 0xff) * 0x10000 + (archiveData[j1 * 10 + 7] & 0xff) * 256 + (archiveData[j1 * 10 + 8] & 0xff);
            int fileSizeCompressed = (archiveData[j1 * 10 + 9] & 0xff) * 0x10000 + (archiveData[j1 * 10 + 10] & 0xff) * 256 + (archiveData[j1 * 10 + 11] & 0xff);
            if (fileHash == wantedHash) {
                byte fileData[] = new byte[fileSize + i];
                if (fileSize != fileSizeCompressed) {
                    BZLib.decompress(fileData, fileSize, archiveData, fileSizeCompressed, offset);
                } else {
                    for (int j2 = 0; j2 < fileSize; j2++)
                        fileData[j2] = archiveData[offset + j2];

                }
                return fileData;
            }
            offset += fileSizeCompressed;
        }

        return null;
    }

    public static String filterString(String s, boolean flag) {
        for (int i = 0; i < 2; i++) {
            String s1 = s;
            pdb = 0;
            int j = 0;
            for (int k = 0; k < s.length(); k++) {
                char c = s.charAt(k);
                if (c >= 'A' && c <= 'Z')
                    c = (char) ((c + 97) - 65);
                if (flag && c == '@' && k + 4 < s.length() && s.charAt(k + 4) == '@') {
                    k += 4;
                } else {
                    byte byte0;
                    if (c >= 'a' && c <= 'z' || c >= '0' && c <= '9')
                        byte0 = 0;
                    else if (c == '\'')
                        byte0 = 1;
                    else if (c == '\r' || c == ' ' || c == '.' || c == ',' || c == '-' || c == '(' || c == ')' || c == '?' || c == '!')
                        byte0 = 2;
                    else
                        byte0 = 3;
                    int l = pdb;
                    for (int i1 = 0; i1 < l; i1++)
                        if (byte0 == 3) {
                            if (udb[i1] > 0 && udb[i1] < qdb[i1] + sdb[i1].length() / 2) {
                                udb[pdb] = udb[i1] + 1;
                                tdb[pdb] = tdb[i1];
                                rdb[pdb] = rdb[i1] + 1;
                                qdb[pdb] = qdb[i1];
                                sdb[pdb++] = sdb[i1];
                                udb[i1] = -udb[i1];
                            }
                        } else {
                            char c1 = sdb[i1].charAt(rdb[i1]);
                            if (areSimilar(c, c1)) {
                                rdb[i1]++;
                                if (udb[i1] < 0)
                                    udb[i1] = -udb[i1];
                            } else if ((c == ' ' || c == '\r') && qdb[i1] == 0) {
                                rdb[i1] = 0x1869f;
                            } else {
                                char c2 = sdb[i1].charAt(rdb[i1] - 1);
                                if (byte0 == 0 && !areSimilar(c, c2))
                                    rdb[i1] = 0x1869f;
                            }
                        }

                    if (byte0 >= 2)
                        j = 1;
                    if (byte0 <= 2) {
                        for (int j1 = 0; j1 < censoredWords1.length; j1++)
                            if (areSimilar(c, censoredWords1[j1].charAt(0))) {
                                udb[pdb] = 1;
                                tdb[pdb] = k;
                                rdb[pdb] = 1;
                                qdb[pdb] = 1;
                                sdb[pdb++] = censoredWords1[j1];
                            }

                        for (int l1 = 0; l1 < censoredWords2.length; l1++)
                            if (areSimilar(c, censoredWords2[l1].charAt(0))) {
                                udb[pdb] = 1;
                                tdb[pdb] = k;
                                rdb[pdb] = 1;
                                qdb[pdb] = j;
                                sdb[pdb++] = censoredWords2[l1];
                            }

                        if (j == 1) {
                            for (int j2 = 0; j2 < censoredWords3.length; j2++)
                                if (areSimilar(c, censoredWords3[j2].charAt(0))) {
                                    udb[pdb] = 1;
                                    tdb[pdb] = k;
                                    rdb[pdb] = 1;
                                    qdb[pdb] = 1;
                                    sdb[pdb++] = censoredWords3[j2];
                                }

                        }
                        if (byte0 == 0)
                            j = 0;
                    }
                    for (int k1 = 0; k1 < pdb; k1++)
                        if (rdb[k1] >= sdb[k1].length()) {
                            if (rdb[k1] < 0x1869f) {
                                String s2 = "";
                                for (int k2 = 0; k2 < s.length(); k2++)
                                    if (k2 < tdb[k1] || k2 > k)
                                        s2 = s2 + s.charAt(k2);
                                    else
                                        s2 = s2 + "*";

                                s = s2;
                            }
                            pdb--;
                            for (int i2 = k1; i2 < pdb; i2++) {
                                qdb[i2] = qdb[i2 + 1];
                                rdb[i2] = rdb[i2 + 1];
                                sdb[i2] = sdb[i2 + 1];
                                tdb[i2] = tdb[i2 + 1];
                                udb[i2] = udb[i2 + 1];
                            }

                            k1--;
                        }

                }
            }

            if (s.equalsIgnoreCase(s1))
                break;
        }

        return s;
    }

    static boolean areSimilar(char c, char c1) {
        if (c == c1)
            return true;
        if (c1 == 'i' && (c == 'l' || c == '1' || c == '!' || c == '|' || c == ':' || c == '\246' || c == ';'))
            return true;
        if (c1 == 's' && (c == '5' || c == '$'))
            return true;
        if (c1 == 'a' && (c == '4' || c == '@'))
            return true;
        if (c1 == 'c' && (c == '(' || c == '<' || c == '['))
            return true;
        if (c1 == 'o' && c == '0')
            return true;
        return c1 == 'u' && c == 'v';
    }

}
