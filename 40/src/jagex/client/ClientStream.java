// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package jagex.client;

import jagex.Buffer;

import java.applet.Applet;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;

public class ClientStream extends Buffer
        implements Runnable {

    public int offset;
    public byte buf[];
    private boolean socketException;
    private String socketExceptionMessage;
    private byte buffer[];
    private int endoffset;
    private int writeoffset;
    private Thread thread;
    private boolean closed;

    public ClientStream(InputStream inputstream) {
        super(inputstream);
        socketException = false;
        socketExceptionMessage = "error in twriter";
        closed = true;
        offset = 3;
    }

    public ClientStream(Socket socket)
            throws IOException {
        super(socket);
        socketException = false;
        socketExceptionMessage = "error in twriter";
        closed = true;
        offset = 3;
    }

    public ClientStream(String s)
            throws IOException {
        super(s);
        socketException = false;
        socketExceptionMessage = "error in twriter";
        closed = true;
        offset = 3;
    }

    public ClientStream(byte abyte0[]) {
        super(abyte0);
        socketException = false;
        socketExceptionMessage = "error in twriter";
        closed = true;
        offset = 3;
    }

    public static ClientStream create(String s, Applet applet, int i1)
            throws IOException {
        Socket socket;
        if (applet != null)
            socket = new Socket(InetAddress.getByName(applet.getCodeBase().getHost()), i1);
        else
            socket = new Socket(InetAddress.getByName(s), i1);
        socket.setSoTimeout(30000);
        return new ClientStream(socket);
    }

    public void close() {
        if (super.closing)
            return;
        try {
            if (super.socket != null)
                super.socket.close();
            if (thread != null) {
                closed = true;
                synchronized (this) {
                    notify();
                }
                thread = null;
            }
            if (super.input != null)
                super.input.close();
            if (super.output != null)
                super.output.close();
            buffer = null;
            buf = null;
            return;
        } catch (IOException _ex) {
            System.out.println("Error closing stream");
        }
    }

    public void writeBytes(byte abyte0[], int i1, int j1)
            throws IOException {
        if (super.closing) {
            return;
        } else {
            super.output.write(abyte0, i1, j1);
            return;
        }
    }

    public void writeBytes(byte buff[], int off, int len, boolean flush)
            throws IOException {
        if (super.closing)
            return;
        if (buffer == null)
            buffer = new byte[5000];
        synchronized (this) {
            for (int k1 = 0; k1 < len; k1++) {
                buffer[writeoffset] = buff[k1 + off];
                writeoffset = (writeoffset + 1) % 5000;
                if (writeoffset != (endoffset + 4900) % 5000)
                    continue;
                socketException = true;
                socketExceptionMessage = "Write buffer full! " + len;
                k1 = len + 1;
                closed = true;
                super.input.close();
                super.output.close();
                super.closing = true;
                break;
            }

            if (flush) {
                if (thread == null) {
                    closed = false;
                    thread = new Thread(this);
                    thread.setDaemon(true);
                    thread.setPriority(4);
                    thread.start();
                }
                notify();
            }
        }
        if (socketException)
            throw new IOException(socketExceptionMessage);
        else
            return;
    }

    public void flush() {
        synchronized (this) {
            if (writeoffset == endoffset || buffer == null)
                return;
            if (thread == null) {
                closed = false;
                thread = new Thread(this);
                thread.setDaemon(true);
                thread.setPriority(4);
                thread.start();
            }
            notify();
        }
    }

    public void run() {
        while (thread != null && !closed) {
            int i1;
            int j1;
            synchronized (this) {
                if (writeoffset == endoffset)
                    try {
                        wait();
                    } catch (InterruptedException _ex) {
                    }
                if (thread == null || closed)
                    return;
                j1 = endoffset;
                if (writeoffset >= endoffset)
                    i1 = writeoffset - endoffset;
                else
                    i1 = 5000 - endoffset;
            }
            if (i1 > 0) {
                try {
                    super.output.write(buffer, j1, i1);
                } catch (IOException ioexception) {
                    socketException = true;
                    socketExceptionMessage = "Twriter IOEx:" + ioexception;
                }
                endoffset = (endoffset + i1) % 5000;
                try {
                    if (writeoffset == endoffset)
                        super.output.flush();
                } catch (IOException ioexception1) {
                    socketException = true;
                    socketExceptionMessage = "Twriter IOEx:" + ioexception1;
                }
            }
        }
    }

    public void newPacket(int i1) {
        if (buf == null)
            buf = new byte[4000];
        buf[2] = (byte) i1;
        offset = 3;
    }

    public void writeByte(int i1) {
        buf[offset++] = (byte) i1;
    }

    public void writeShort(int i1) {
        buf[offset++] = (byte) (i1 >> 8);
        buf[offset++] = (byte) i1;
    }

    public void writeInt(int i1) {
        buf[offset++] = (byte) (i1 >> 24);
        buf[offset++] = (byte) (i1 >> 16);
        buf[offset++] = (byte) (i1 >> 8);
        buf[offset++] = (byte) i1;
    }

    public void writeLong(long l1) {
        writeInt((int) (l1 >> 32));
        writeInt((int) (l1 & -1L));
    }

    public void writeString(String s) {
        s.getBytes(0, s.length(), buf, offset);
        offset += s.length();
    }

    public void putShort(int i1, int off) {
        buf[off++] = (byte) (i1 >> 8);
        buf[off++] = (byte) i1;
    }

    public void flushPacket2()
            throws IOException {
        buf[0] = (byte) ((offset - 2) / 256);
        buf[1] = (byte) (offset - 2 & 0xff);
        writeBytes(buf, 0, offset, true);
    }

    public void flushPacket() {
        buf[0] = (byte) ((offset - 2) / 256);
        buf[1] = (byte) (offset - 2 & 0xff);
        try {
            writeBytes(buf, 0, offset, true);
            return;
        } catch (IOException _ex) {
            return;
        }
    }

    public void sendPacket()// ALSO KONWN AS DONT SEND IT
    {
        buf[0] = (byte) ((offset - 2) / 256);
        buf[1] = (byte) (offset - 2 & 0xff);
        try {
            writeBytes(buf, 0, offset, false);
            return;
        } catch (IOException _ex) {
            return;
        }
    }
}
