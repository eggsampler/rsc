// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package jagex.client;

import jagex.BZLib;
import jagex.Utility;

import java.applet.Applet;
import java.awt.*;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.DataInputStream;
import java.io.IOException;

// Referenced classes of package jagex.client:
//            d, i

public class GameApplet extends Applet
        implements Runnable {

    static GameFrame gameFrame = null;
    public int yOffset;
    public int lastMouseAction;
    public int loadingStep;
    public String up;
    public boolean keyLcb;
    public boolean keyRcb;
    public boolean keyLeft;
    public boolean keyRight;
    public boolean keyUp;
    public boolean keyDown;
    public boolean keySpace;
    public boolean keyNm;
    public int threadSleep;
    public int mouseX;
    public int mouseY;
    public int mouseButtonDown;
    public int lastMouseButtonDown;
    public int lastKeyCode1;
    public int lastKeyCode2;
    public boolean interlace;
    public String inputTextCurrent;
    public String inputTextFinal;
    public String inputPmCurrent;
    public String inputPmFinal;
    public int fps;
    private int appletWidth;
    private int appletHeight;
    private Thread appletThread;
    private int targetFps;
    private int maxDrawTime;
    private long timings[];
    private boolean appletMode;
    private int stopTimeout;
    private int interlaceTimer;
    private boolean vp;
    private int loadingProgressPercent;
    private String loadingProgressText;
    private Font yp;
    private Font zp;
    private Font aq;
    private Image imageLogo;
    private Graphics graphics;
    public GameApplet() {
        appletWidth = 512;
        appletHeight = 384;
        targetFps = 20;
        maxDrawTime = 1000;
        timings = new long[10];
        loadingStep = 1;
        vp = false;
        loadingProgressText = "Loading";
        yp = new Font("TimesRoman", 0, 15);
        zp = new Font("Helvetica", 1, 13);
        aq = new Font("Helvetica", 0, 12);
        keyLcb = false;
        keyRcb = false;
        keyLeft = false;
        keyRight = false;
        keyUp = false;
        keyDown = false;
        keySpace = false;
        keyNm = false;
        threadSleep = 1;
        interlace = false;
        inputTextCurrent = "";
        inputTextFinal = "";
        inputPmCurrent = "";
        inputPmFinal = "";
    }

    public void write(String path, String filename, byte[] bytes) {
        /*try {
            Files.write(Paths.get("./data40/gamedata/" + path + "/" + filename),
                    Utility.unpackData(filename, Utility.getDataFileOffset(filename, bytes), bytes),
                    StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }

    public void startGame() {
    }

    public synchronized void handleInputs() {
    }

    public void onClosing() {
    }

    public synchronized void draw() {
    }

    public void drawHbar() {
    }

    public final void startApplication(int width, int height, String title, boolean resizable) {
        appletMode = false;
        System.out.println("Started application");
        appletWidth = width;
        appletHeight = height;
        gameFrame = new GameFrame(this, width, height, title, resizable, false);
        loadingStep = 1;
        appletThread = new Thread(this);
        appletThread.start();
        appletThread.setPriority(1);
    }

    public final boolean inAppletMode() {
        // override loadProjectile make it load the archives instead
        return true;//appletMode;
    }

    public final void createFrame(int width, int height, String title, boolean resizable) {
        if (gameFrame != null) {
            return;
        } else {
            appletWidth = width;
            appletHeight = height;
            gameFrame = new GameFrame(this, width, height, title, resizable, appletMode);
            return;
        }
    }

    public final void resizeFrame(int width, int height) {
        if (gameFrame == null) {
            return;
        } else {
            gameFrame.resize(width, height);
            appletWidth = width;
            appletHeight = height;
            return;
        }
    }

    public final void setFrameIcon(Image image) {
        if (gameFrame == null) {
            return;
        } else {
            gameFrame.setIconImage(image);
            return;
        }
    }

    public void resize(int j, int l) {
        resizeFrame(j, l);
    }

    public final Graphics getGraphics() {
        if (gameFrame == null)
            return super.getGraphics();
        else
            return gameFrame.getGraphics();
    }

    public final int getAppletWidth() {
        return appletWidth;
    }

    public final int getAppletHeight() {
        return appletHeight;
    }

    public final Image createImage(int width, int height) {
        if (gameFrame == null)
            return super.createImage(width, height);
        else
            return gameFrame.createImage(width, height);
    }

    public Frame getGameFrame() {
        return gameFrame;
    }

    public final void setTargetFps(int fps) {
        targetFps = 1000 / fps;
    }

    public final void setMaxDrawTime(int time) {
        maxDrawTime = time;
    }

    public final void resetTimings() {
        for (int j = 0; j < 10; j++)
            timings[j] = 0L;

    }

    public synchronized boolean keyDown(Event event, int key) {
        handleKeyPress(key);
        lastKeyCode1 = key;
        lastKeyCode2 = key;
        lastMouseAction = 0;
        if (key == 1006)
            keyLeft = true;
        if (key == 1007)
            keyRight = true;
        if (key == 1004)
            keyUp = true;
        if (key == 1005)
            keyDown = true;
        if ((char) key == ' ')
            keySpace = true;
        if ((char) key == 'n' || (char) key == 'm')
            keyNm = true;
        if ((char) key == 'N' || (char) key == 'M')
            keyNm = true;
        if ((char) key == '{')
            keyLcb = true;
        if ((char) key == '}')
            keyRcb = true;
        if (key == 1008)
            interlace = !interlace;
        if ((key >= 97 && key <= 122 || key >= 65 && key <= 90 || key >= 48 && key <= 57 || key == 32) && inputTextCurrent.length() < 16)
            inputTextCurrent += (char) key;
        if (key >= 32 && key <= 122 && inputPmCurrent.length() < 80)
            inputPmCurrent += (char) key;
        if (key == 8 && inputTextCurrent.length() > 0)
            inputTextCurrent = inputTextCurrent.substring(0, inputTextCurrent.length() - 1);
        if (key == 8 && inputPmCurrent.length() > 0)
            inputPmCurrent = inputPmCurrent.substring(0, inputPmCurrent.length() - 1);
        if (key == 10 || key == 13) {
            inputTextFinal = inputTextCurrent;
            inputPmFinal = inputPmCurrent;
        }
        return true;
    }

    public void handleKeyPress(int j) {
    }

    public synchronized boolean keyUp(Event event, int j) {
        lastKeyCode1 = 0;
        if (j == 1006)
            keyLeft = false;
        if (j == 1007)
            keyRight = false;
        if (j == 1004)
            keyUp = false;
        if (j == 1005)
            keyDown = false;
        if ((char) j == ' ')
            keySpace = false;
        if ((char) j == 'n' || (char) j == 'm')
            keyNm = false;
        if ((char) j == 'N' || (char) j == 'M')
            keyNm = false;
        if ((char) j == '{')
            keyLcb = false;
        if ((char) j == '}')
            keyRcb = false;
        return true;
    }

    public synchronized boolean mouseMove(Event event, int j, int l) {
        mouseX = j;
        mouseY = l + yOffset;
        mouseButtonDown = 0;
        lastMouseAction = 0;
        return true;
    }

    public synchronized boolean mouseUp(Event event, int j, int l) {
        mouseX = j;
        mouseY = l + yOffset;
        mouseButtonDown = 0;
        return true;
    }

    public synchronized boolean mouseDown(Event event, int j, int l) {
        mouseX = j;
        mouseY = l + yOffset;
        if (event.metaDown())
            mouseButtonDown = 2;
        else
            mouseButtonDown = 1;
        lastMouseButtonDown = mouseButtonDown;
        lastMouseAction = 0;
        return true;
    }

    public synchronized boolean mouseDrag(Event event, int j, int l) {
        mouseX = j;
        mouseY = l + yOffset;
        if (event.metaDown())
            mouseButtonDown = 2;
        else
            mouseButtonDown = 1;
        return true;
    }

    public final void init() {
        appletMode = true;
        System.out.println("Started applet");
        appletWidth = size().width;
        appletHeight = size().height;
        loadingStep = 1;
        Utility.appletCodeBase = getCodeBase();
        appletThread = new Thread(this);
        appletThread.start();
    }

    public final void start() {
        if (stopTimeout >= 0)
            stopTimeout = 0;
    }

    public final void stop() {
        if (stopTimeout >= 0)
            stopTimeout = 4000 / targetFps;
    }

    public final void destroy() {
        stopTimeout = -1;
        try {
            Thread.sleep(5000L);
        } catch (Exception _ex) {
        }
        if (stopTimeout == -1) {
            System.out.println("5 seconds expired, forcing kill");
            closeProgram();
            if (appletThread != null) {
                appletThread.stop();
                appletThread = null;
            }
        }
    }

    public final void closeProgram() {
        stopTimeout = -2;
        System.out.println("Closing program");
        onClosing();
        try {
            Thread.sleep(1000L);
        } catch (Exception _ex) {
        }
        if (gameFrame != null)
            gameFrame.dispose();
        if (!appletMode)
            System.exit(0);
    }

    public final void run() {
        if (loadingStep == 1) {
            loadingStep = 2;
            graphics = getGraphics();
            loadJagex();
            drawLoadingScreen(0, "Loading...");
            startGame();
            loadingStep = 0;
        }
        int j = 0;
        int i1 = 256;
        int j1 = 1;
        int k1 = 0;
        for (int i2 = 0; i2 < 10; i2++)
            timings[i2] = System.currentTimeMillis();

        long l = System.currentTimeMillis();
        while (stopTimeout >= 0) {
            if (stopTimeout > 0) {
                stopTimeout--;
                if (stopTimeout == 0) {
                    closeProgram();
                    appletThread = null;
                    return;
                }
            }
            int j2 = i1;
            int k2 = j1;
            i1 = 300;
            j1 = 1;
            long l1 = System.currentTimeMillis();
            if (timings[j] == 0L) {
                i1 = j2;
                j1 = k2;
            } else if (l1 > timings[j])
                i1 = (int) ((long) (2560 * targetFps) / (l1 - timings[j]));
            if (i1 < 25)
                i1 = 25;
            if (i1 > 256) {
                i1 = 256;
                j1 = (int) ((long) targetFps - (l1 - timings[j]) / 10L);
                if (j1 < threadSleep)
                    j1 = threadSleep;
            }
            try {
                Thread.sleep(j1);
            } catch (InterruptedException _ex) {
            }
            timings[j] = l1;
            j = (j + 1) % 10;
            if (j1 > 1) {
                for (int l2 = 0; l2 < 10; l2++)
                    if (timings[l2] != 0L)
                        timings[l2] += j1;

            }
            int i3 = 0;
            while (k1 < 256) {
                handleInputs();
                k1 += i1;
                if (++i3 > maxDrawTime) {
                    k1 = 0;
                    interlaceTimer += 6;
                    if (interlaceTimer > 25) {
                        interlaceTimer = 0;
                        interlace = true;
                    }
                    break;
                }
            }
            interlaceTimer--;
            k1 &= 0xff;
            draw();
            fps = (1000 * i1) / (targetFps * 256);
            if (appletMode && j == 0)
                showStatus("Fps:" + fps + "Del:" + j1);
            if (gameFrame != null && (gameFrame.getWidth() != appletWidth || gameFrame.getHeight() != appletHeight))
                resize(gameFrame.getWidth(), gameFrame.getHeight());
        }
        if (stopTimeout == -1)
            closeProgram();
        appletThread = null;
    }

    public final void update(Graphics g) {
        paint(g);
    }

    public final void paint(Graphics g) {
        if (loadingStep == 2 && imageLogo != null) {
            drawLoadingScreen(loadingProgressPercent, loadingProgressText);
            return;
        }
        if (loadingStep == 0)
            drawHbar();
    }

    public void loadJagex() {
        try {
            byte abyte0[] = Utility.readDataFile("jagex.jag");
            byte abyte1[] = Utility.unpackData("logo.tga", 0, abyte0);
            imageLogo = createImage(abyte1);
            Surface.addFont(Utility.unpackData("h11p.jf", 0, abyte0));
            Surface.addFont(Utility.unpackData("h12b.jf", 0, abyte0));
            Surface.addFont(Utility.unpackData("h12p.jf", 0, abyte0));
            Surface.addFont(Utility.unpackData("h13b.jf", 0, abyte0));
            Surface.addFont(Utility.unpackData("h14b.jf", 0, abyte0));
            Surface.addFont(Utility.unpackData("h16b.jf", 0, abyte0));
            Surface.addFont(Utility.unpackData("h20b.jf", 0, abyte0));
            Surface.addFont(Utility.unpackData("h24b.jf", 0, abyte0));
            return;
        } catch (IOException _ex) {
            System.out.println("Error loading jagex.dat");
        }
    }

    public void drawLoadingScreen(int j, String s) {
        int l = (appletWidth - 281) / 2;
        int i1 = (appletHeight - 148) / 2;
        graphics.setColor(Color.black);
        graphics.fillRect(0, 0, appletWidth, appletHeight);
        if (!vp)
            graphics.drawImage(imageLogo, l, i1, this);
        l += 2;
        i1 += 90;
        loadingProgressPercent = j;
        loadingProgressText = s;
        graphics.setColor(new Color(132, 132, 132));
        if (vp)
            graphics.setColor(new Color(220, 0, 0));
        graphics.drawRect(l - 2, i1 - 2, 280, 23);
        graphics.fillRect(l, i1, (277 * j) / 100, 20);
        graphics.setColor(new Color(198, 198, 198));
        if (vp)
            graphics.setColor(new Color(255, 255, 255));
        drawString(graphics, s, yp, l + 138, i1 + 10);
        if (!vp) {
            drawString(graphics, "Created by JAGeX - visit www.jagex.com", zp, l + 138, i1 + 30);
            drawString(graphics, "Copyright \2512000 Andrew Gower", zp, l + 138, i1 + 44);
        } else {
            graphics.setColor(new Color(132, 132, 152));
            drawString(graphics, "Copyright \2512000 Andrew Gower", aq, l + 138, appletHeight - 20);
        }
        if (up != null) {
            graphics.setColor(Color.white);
            drawString(graphics, up, zp, l + 138, i1 - 120);
        }
    }

    public void showLoadingProgress(int j, String s) {
        int l = (appletWidth - 281) / 2;
        int i1 = (appletHeight - 148) / 2;
        l += 2;
        i1 += 90;
        loadingProgressPercent = j;
        loadingProgressText = s;
        int j1 = (277 * j) / 100;
        graphics.setColor(new Color(132, 132, 132));
        if (vp)
            graphics.setColor(new Color(220, 0, 0));
        graphics.fillRect(l, i1, j1, 20);
        graphics.setColor(Color.black);
        graphics.fillRect(l + j1, i1, 277 - j1, 20);
        graphics.setColor(new Color(198, 198, 198));
        if (vp)
            graphics.setColor(new Color(255, 255, 255));
        drawString(graphics, s, yp, l + 138, i1 + 10);
    }

    public void drawString(Graphics g, String s, Font font, int j, int l) {
        Object obj;
        if (gameFrame == null)
            obj = this;
        else
            obj = gameFrame;
        FontMetrics fontmetrics = ((Component) (obj)).getFontMetrics(font);
        fontmetrics.stringWidth(s);
        g.setFont(font);
        g.drawString(s, j - fontmetrics.stringWidth(s) / 2, l + fontmetrics.getHeight() / 4);
    }

    public Image createImage(byte abyte0[]) {
        int j = abyte0[13] * 256 + abyte0[12];
        int l = abyte0[15] * 256 + abyte0[14];
        byte abyte1[] = new byte[256];
        byte abyte2[] = new byte[256];
        byte abyte3[] = new byte[256];
        for (int i1 = 0; i1 < 256; i1++) {
            abyte1[i1] = abyte0[20 + i1 * 3];
            abyte2[i1] = abyte0[19 + i1 * 3];
            abyte3[i1] = abyte0[18 + i1 * 3];
        }

        IndexColorModel indexcolormodel = new IndexColorModel(8, 256, abyte1, abyte2, abyte3);
        byte abyte4[] = new byte[j * l];
        int j1 = 0;
        for (int k1 = l - 1; k1 >= 0; k1--) {
            for (int l1 = 0; l1 < j; l1++)
                abyte4[j1++] = abyte0[786 + l1 + k1 * j];

        }

        MemoryImageSource memoryimagesource = new MemoryImageSource(j, l, indexcolormodel, abyte4, 0, j);
        Image image = createImage(memoryimagesource);
        return image;
    }

    public byte[] readDataFile(String s, String s1, int j)
            throws IOException {
        s = "./data40/" + s;
        int l = 0;
        int i1 = 0;
        int j1 = 0;
        byte abyte0[] = null;
        while (l < 2)
            try {
                showLoadingProgress(j, "Loading " + s1 + " - 0%");
                if (l == 1)
                    s = s.toUpperCase();
                java.io.InputStream inputstream = Utility.openStream(s);
                DataInputStream datainputstream = new DataInputStream(inputstream);
                byte abyte2[] = new byte[6];
                datainputstream.readFully(abyte2, 0, 6);
                i1 = ((abyte2[0] & 0xff) << 16) + ((abyte2[1] & 0xff) << 8) + (abyte2[2] & 0xff);
                j1 = ((abyte2[3] & 0xff) << 16) + ((abyte2[4] & 0xff) << 8) + (abyte2[5] & 0xff);
                showLoadingProgress(j, "Loading " + s1 + " - 5%");
                int k1 = 0;
                abyte0 = new byte[j1];
                while (k1 < j1) {
                    int l1 = j1 - k1;
                    if (l1 > 1000)
                        l1 = 1000;
                    datainputstream.readFully(abyte0, k1, l1);
                    k1 += l1;
                    showLoadingProgress(j, "Loading " + s1 + " - " + (5 + (k1 * 95) / j1) + "%");
                }
                l = 2;
                datainputstream.close();
            } catch (IOException _ex) {
                l++;
            }
        showLoadingProgress(j, "Unpacking " + s1);
        if (j1 != i1) {
            byte abyte1[] = new byte[i1];
            BZLib.decompress(abyte1, i1, abyte0, j1, 0);
            return abyte1;
        } else {
            return abyte0;
        }
    }

}
