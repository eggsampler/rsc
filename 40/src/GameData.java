// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

import jagex.Buffer;
import jagex.Utility;

import java.io.IOException;

public class GameData {

    public static int objectCount;
    public static int itemSpriteCount;
    public static String objectVar1[][];// item name
    public static String objectVar2[];// item description
    public static String yfb[];
    public static String objectVar6[];// item command
    public static int objectVar3[];// used game world sprite id?
    public static int objectVar4[];
    public static int objectVar5[];
    public static int objectVar7[];
    public static int objectVar8[];
    public static int objectVar9[];
    public static int objectVar10[];
    public static int objectVar11[];
    public static int objectVar12[];
    public static int objectVar13[];// item ammo name/id
    public static int objectVar14[];
    public static int objectVar15[];// item type/id or somehting
    public static int objectVar16[];// item sprite colour
    public static int objectVar17[];
    public static int objectVar18[];// is item special
    public static int npcCount;
    public static String npcVar1[][];// npc name
    public static String npcVar2[];// npc description
    public static int npcVar3[];
    public static int npcVar4[];
    public static int npcVar5[];
    public static int npcVar6[];
    public static int npcVar7[];
    public static int npcVar8[];
    public static int npcVar9[];
    public static int npcVar10[];
    public static int npcVar11[];
    public static int npcVar12[];
    public static int npcVar13[];
    public static int npcVar14[];
    public static int npcVar15[];
    public static int npcVar16[];
    public static int npcVar17[];
    public static int npcVar18[][];// npc sprites
    public static int npcVar19[];// some colour
    public static int npcVar20[];// some other colour
    public static int npcVar21[];// some other colour encore
    public static int npcVar22[];// some other colour apex
    public static int npcVar23[];
    public static int npcVar24[];
    public static int npcVar25[];
    public static int npcVar26[];
    public static int npcVar27[];
    public static int npcVar28[][];// npc drops
    public static int npcVar29[][];// number of dropped item
    public static int entityCount;// texture? count
    public static String entityVar1[];// name of texture?
    public static String entityVar2[];// texture? filename
    public static int entityVar3[];// texture? mask
    public static int entityVar4[];
    public static int entityVar5[];// some flag
    public static int entityVar6[];// some other flag
    public static int entityVar7[];
    public static int locationCount;// number of objects
    public static String locationVar1[][];// name of object
    public static String locationVar2[];// object description
    public static String locationVar8[];// object command
    public static String locationVar9[];// object alternative command
    public static int locationVar3[];// object model name/id
    public static int locationVar4[];
    public static int locationVar5[];
    public static int locationVar6[];
    public static int locationVar7[];
    public static int locationVar10[];
    public static int boundaryCount;// number of wall objects
    public static String boundaryVar1[][];// wall object name
    public static String boundaryVar2[];// wall object description
    public static String boundaryVar9[];// wall object command
    public static String boundaryVar10[];// wall object alternative command
    public static int boundaryVar3[];
    public static int boundaryVar4[];
    public static int boundaryVar5[];
    public static int boundaryVar6[];
    public static int boundaryVar7[];
    public static int boundaryVar8[];
    public static int roofCount;// number of roofs
    public static String roofVar1[];// roof name
    public static int roofVar2[];
    public static int roofVar3[];
    public static int roofVar4[];
    public static int floorCount;// number of floors
    public static String floorVar1[];// floor name
    public static int floorVar2[];
    public static int floorVar3[];
    public static int floorVar4[];
    public static int projectileCount;// number of projectiles
    public static int ijb;
    public static String projectileVar1[];// projectile name
    public static String projectileVar6[];// projectile ammo name
    public static int projectileVar2[];// projectile type?
    public static int projectileVar3[];// projectile source type? wut
    public static int projectileVar4[];
    public static int projectileVar5[];
    public static int projectileVarBonus[];
    public static int projectileVar7[];// projectile is not a spell?
    public static int spellCount;// number of spells
    public static String spellVar1[];// spell name
    public static String spellVar3[];// spell description
    public static int spellVar2[];// magic level required
    public static int spellVar6[];
    public static int spellVar4[];
    public static int spellVar5[];// projectile source type? projectileVar3
    public static int spellVar7[][];// required runes
    public static int spellVar8[][];// number of required runes
    public static int shopCount;// number of shops
    public static String shopVar1[];// shop name
    public static int shopVar2[];// number of items in the shop
    public static int shopVar3[];
    public static int shopVar4[];// item respawn time?
    public static int shopVar5[];// shop type?
    public static int shopVar6[];
    public static int shopVar7[][];// item name/id
    public static int shopVar8[][];// item count
    public static int shopVar9[][];// item price
    public static String lkb[] = {
            "attack", "defense", "damage", "hits", "agression", "bravery", "regenerate", "perception"
    };
    public static int modelCount;
    public static String modelName[] = new String[200];
    static String kkb[] = {
            "attack", "defense", "strength", "hits", "ranged", "prayer", "magic", "cooking", "woodcutting", "fletching",
            "fishing", "firemaking", "crafting", "smithing", "mining", "herblaw"
    };
    public final int tfb = 0xbc614e;
    public GameData() {
    }

    public static void loadData() {
        try {
            loadProjectile(new Buffer("../gamedata/config/projectile.txt"));
            loadEntity(new Buffer("../gamedata/config/entity.txt"));
            loadObjects(new Buffer("../gamedata/config/objects.txt"));
            loadNpc(new Buffer("../gamedata/config/npc.txt"));
            loadLocation(new Buffer("../gamedata/config/location.txt"));
            loadBoundary(new Buffer("../gamedata/config/boundary.txt"));
            loadRoof(new Buffer("../gamedata/config/roof.txt"));
            loadFloor(new Buffer("../gamedata/config/floor.txt"));
            loadSpells(new Buffer("../gamedata/config/spells.txt"));
            loadShop(new Buffer("../gamedata/config/shop.txt"));
            wo();
            return;
        } catch (IOException ioexception) {
            System.out.println("Error loading config files");
            ioexception.printStackTrace();
            return;
        }
    }

    public static void loadData(byte abyte0[]) {
        try {
            loadProjectile(new Buffer(abyte0, Utility.getDataFileOffset("projectile.txt", abyte0)));
            loadEntity(new Buffer(abyte0, Utility.getDataFileOffset("entity.txt", abyte0)));
            loadObjects(new Buffer(abyte0, Utility.getDataFileOffset("objects.txt", abyte0)));
            loadNpc(new Buffer(abyte0, Utility.getDataFileOffset("npc.txt", abyte0)));
            loadLocation(new Buffer(abyte0, Utility.getDataFileOffset("location.txt", abyte0)));
            loadBoundary(new Buffer(abyte0, Utility.getDataFileOffset("boundary.txt", abyte0)));
            loadRoof(new Buffer(abyte0, Utility.getDataFileOffset("roof.txt", abyte0)));
            loadFloor(new Buffer(abyte0, Utility.getDataFileOffset("floor.txt", abyte0)));
            loadSpells(new Buffer(abyte0, Utility.getDataFileOffset("spells.txt", abyte0)));
            loadShop(new Buffer(abyte0, Utility.getDataFileOffset("shop.txt", abyte0)));
            wo();
            return;
        } catch (IOException ioexception) {
            System.out.println("Error loading config files");
            ioexception.printStackTrace();
            return;
        }
    }

    static String pad(Object s, int n) {
        return String.format("%1$-" + n + "s", String.valueOf(s));
    }

    public static void loadShop(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        shopCount = i;
        System.out.println("Found " + i + " shops");
        shopVar1 = new String[i];
        shopVar2 = new int[i];
        shopVar6 = new int[i];
        shopVar3 = new int[i];
        shopVar4 = new int[i];
        shopVar5 = new int[i];
        shopVar7 = new int[i][40];
        shopVar8 = new int[i][40];
        shopVar9 = new int[i][40];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            shopVar1[j] = f1.readDataString();
            int k = shopVar2[j] = f1.readDataInt();
            shopVar3[j] = f1.readDataInt();
            shopVar4[j] = f1.readDataInt();
            shopVar5[j] = f1.readDataInt();
            shopVar6[j] = f1.readDataInt();
            for (int l = 0; l < k; l++) {
                f1.skipToNextDataValue();
                String s = f1.readDataString();
                shopVar7[j][l] = getItemId(s);
                shopVar8[j][l] = f1.readDataInt();
                shopVar9[j][l] = f1.readDataInt();
            }
        }
    }

    public static void loadSpells(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        spellCount = i;
        System.out.println("Found " + i + " skills");// but the file is spells.txt.. :(
        spellVar1 = new String[i];
        spellVar3 = new String[i];
        spellVar2 = new int[i];
        spellVar4 = new int[i];
        spellVar5 = new int[i];
        spellVar6 = new int[i];
        spellVar7 = new int[i][];
        spellVar8 = new int[i][];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            spellVar1[j] = f1.readDataString();
            spellVar2[j] = f1.readDataInt();
            spellVar3[j] = f1.readDataString();
            spellVar4[j] = f1.readDataInt();
            spellVar5[j] = f1.readDataInt();
            f1.skipToNextDataValue();
            int k = spellVar6[j] = f1.readDataInt();

            spellVar7[j] = new int[k];
            spellVar8[j] = new int[k];
            for (int l = 0; l < k; l++) {
                String s = f1.readDataString();
                spellVar7[j][l] = getItemId(s);
                spellVar8[j][l] = f1.readDataInt();
            }
        }

        f1.close();
    }

    public static void loadProjectile(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        projectileCount = i;
        System.out.println("Found " + i + " projectiles");
        projectileVar1 = new String[i];
        projectileVar6 = new String[i];
        projectileVar2 = new int[i];
        projectileVar3 = new int[i];
        projectileVar4 = new int[i];
        projectileVar5 = new int[i];
        projectileVarBonus = new int[i];
        projectileVar7 = new int[i];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            projectileVar1[j] = f1.readDataString();
            projectileVar2[j] = f1.readDataInt();
            projectileVar3[j] = f1.readDataInt();
            projectileVar4[j] = f1.readDataInt();
            projectileVar5[j] = f1.readDataInt();
            projectileVar6[j] = f1.readDataString();
            projectileVar7[j] = f1.readDataInt();
            if (projectileVar2[j] + 1 > ijb)
                ijb = projectileVar2[j] + 1;
        }

        f1.close();
    }

    public static void wo() {
        for (int i = 0; i < projectileCount; i++)
            projectileVarBonus[i] = getItemId(projectileVar6[i]);

    }

    public static void loadEntity(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        entityCount = i;
        System.out.println("Found " + i + " entities");
        entityVar1 = new String[i];
        entityVar2 = new String[i];
        entityVar4 = new int[i];
        entityVar3 = new int[i];
        entityVar5 = new int[i];
        entityVar6 = new int[i];
        entityVar7 = new int[i];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            entityVar1[j] = f1.readDataString();
            entityVar2[j] = f1.readDataString();
            entityVar3[j] = f1.readDataHex();
            entityVar4[j] = f1.readDataInt();
            entityVar5[j] = f1.readDataInt();
            entityVar6[j] = f1.readDataInt();
        }

        f1.close();
    }

    public static void loadNpc(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        npcCount = i;
        System.out.println("Found " + i + " npcs");
        npcVar1 = new String[i][];
        npcVar2 = new String[i];
        npcVar3 = new int[i];
        npcVar4 = new int[i];
        npcVar5 = new int[i];
        npcVar6 = new int[i];
        npcVar7 = new int[i];
        npcVar8 = new int[i];
        npcVar9 = new int[i];
        npcVar10 = new int[i];
        npcVar11 = new int[i];
        npcVar12 = new int[i];
        npcVar13 = new int[i];
        npcVar14 = new int[i];
        npcVar15 = new int[i];
        npcVar16 = new int[i];
        npcVar17 = new int[i];
        npcVar18 = new int[i][12];
        npcVar19 = new int[i];
        npcVar20 = new int[i];
        npcVar21 = new int[i];
        npcVar22 = new int[i];
        npcVar23 = new int[i];
        npcVar24 = new int[i];
        npcVar25 = new int[i];
        npcVar26 = new int[i];
        npcVar27 = new int[i];
        npcVar28 = new int[i][];
        npcVar29 = new int[i][];
        String[][] npcVar18s = new String[i][12];
        String[][] npcVar28s = new String[i][];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            int k = f1.readDataInt();
            npcVar1[j] = new String[k];
            for (int l = 0; l < k; l++)
                npcVar1[j][l] = f1.readDataString();

            npcVar2[j] = f1.readDataString();
            f1.skipToNextDataValue();
            npcVar3[j] = f1.readDataInt();
            npcVar4[j] = f1.readDataInt();
            npcVar5[j] = f1.readDataInt();
            npcVar6[j] = f1.readDataInt();
            npcVar7[j] = f1.readDataInt();
            npcVar8[j] = f1.readDataInt();
            npcVar9[j] = f1.readDataInt();
            npcVar10[j] = f1.readDataInt();
            npcVar11[j] = f1.readDataInt();
            npcVar12[j] = f1.readDataInt();
            npcVar13[j] = f1.readDataInt();
            npcVar14[j] = f1.readDataInt();
            npcVar15[j] = f1.readDataInt();
            npcVar16[j] = f1.readDataInt();
            npcVar17[j] = f1.readDataInt();
            f1.skipToNextDataValue();
            for (int i1 = 0; i1 < 12; i1++) {
                npcVar18s[j][i1] = f1.readDataString();
                npcVar18[j][i1] = getAnimationId(npcVar18s[j][i1]);
            }

            npcVar19[j] = f1.readDataHex();
            npcVar20[j] = f1.readDataHex();
            npcVar21[j] = f1.readDataHex();
            npcVar22[j] = f1.readDataHex();
            f1.skipToNextDataValue();
            npcVar23[j] = f1.readDataInt();
            npcVar24[j] = f1.readDataInt();
            npcVar25[j] = f1.readDataInt();
            npcVar26[j] = f1.readDataInt();
            npcVar27[j] = f1.readDataInt();
            f1.skipToNextDataValue();
            int j1 = f1.readDataInt();
            npcVar28[j] = new int[j1];
            npcVar29[j] = new int[j1];
            npcVar28s[j] = new String[j1];
            for (int k1 = 0; k1 < j1; k1++) {
                npcVar28s[j][k1] = f1.readDataString();
                npcVar28[j][k1] = getItemId(npcVar28s[j][k1]);
                npcVar29[j][k1] = f1.readDataInt();
            }

        }

        f1.close();
    }

    public static void loadObjects(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        objectCount = i;
        System.out.println("Found " + i + " objects");
        objectVar1 = new String[i][];
        objectVar2 = new String[i];
        objectVar6 = new String[i];
        objectVar3 = new int[i];
        objectVar4 = new int[i];
        objectVar5 = new int[i];
        objectVar7 = new int[i];
        objectVar8 = new int[i];
        objectVar9 = new int[i];
        objectVar10 = new int[i];
        objectVar11 = new int[i];
        objectVar12 = new int[i];
        objectVar13 = new int[i];
        objectVar14 = new int[i];
        objectVar15 = new int[i];
        objectVar16 = new int[i];
        objectVar17 = new int[i];
        objectVar18 = new int[i];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            int k = f1.readDataInt();
            objectVar1[j] = new String[k];
            for (int l = 0; l < k; l++)
                objectVar1[j][l] = f1.readDataString();

            objectVar2[j] = f1.readDataString();
            f1.skipToNextDataValue();
            objectVar3[j] = f1.readDataInt();
            if (objectVar3[j] >= itemSpriteCount)
                itemSpriteCount = objectVar3[j] + 1;
            objectVar4[j] = f1.readDataInt();
            objectVar5[j] = f1.readDataInt();
            objectVar6[j] = f1.readDataString();
            f1.skipToNextDataValue();
            objectVar7[j] = f1.readDataInt();
            objectVar8[j] = f1.readDataInt();
            objectVar9[j] = f1.readDataInt();
            objectVar10[j] = f1.readDataInt();
            objectVar11[j] = f1.readDataInt();
            objectVar12[j] = f1.readDataInt();
            String var13s = f1.readDataString();
            objectVar13[j] = getProjectileId(var13s) + 1;
            f1.skipToNextDataValue();
            objectVar14[j] = f1.readDataInt();
            String var15s = f1.readDataString();
            objectVar15[j] = getAnimationId(var15s);
            objectVar16[j] = f1.readDataHex();
            objectVar17[j] = f1.readDataInt();
            objectVar18[j] = f1.readDataInt();

        }

        f1.close();
    }

    public static void loadLocation(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        locationCount = i;
        System.out.println("Found " + i + " locations");
        locationVar1 = new String[i][];
        locationVar2 = new String[i];
        locationVar8 = new String[i];
        locationVar9 = new String[i];
        locationVar3 = new int[i];
        locationVar4 = new int[i];
        locationVar5 = new int[i];
        locationVar6 = new int[i];
        locationVar7 = new int[i];
        locationVar10 = new int[i];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            int k = f1.readDataInt();
            locationVar1[j] = new String[k];
            for (int l = 0; l < k; l++)
                locationVar1[j][l] = f1.readDataString();

            locationVar2[j] = f1.readDataString();
            f1.skipToNextDataValue();
            String var3s = f1.readDataString();
            locationVar3[j] = getModelId(var3s);
            locationVar4[j] = f1.readDataInt();
            locationVar5[j] = f1.readDataInt();
            locationVar6[j] = f1.readDataInt();
            locationVar7[j] = f1.readDataInt();
            locationVar8[j] = f1.readDataString();
            locationVar9[j] = f1.readDataString();
            locationVar10[j] = f1.readDataInt();
            if (locationVar8[j].equals("_"))
                locationVar8[j] = "WalkTo";
            if (locationVar9[j].equals("_"))
                locationVar9[j] = "Examine";

        }

        f1.close();
    }

    public static void loadBoundary(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        boundaryCount = i;
        System.out.println("Found " + i + " boundaries");
        boundaryVar1 = new String[i][];
        boundaryVar2 = new String[i];
        boundaryVar9 = new String[i];
        boundaryVar10 = new String[i];
        boundaryVar3 = new int[i];
        boundaryVar4 = new int[i];
        boundaryVar5 = new int[i];
        boundaryVar6 = new int[i];
        boundaryVar7 = new int[i];
        boundaryVar8 = new int[i];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            int k = f1.readDataInt();
            boundaryVar1[j] = new String[k];
            for (int l = 0; l < k; l++)
                boundaryVar1[j][l] = f1.readDataString();

            boundaryVar2[j] = f1.readDataString();
            f1.skipToNextDataValue();
            boundaryVar3[j] = f1.readDataInt();
            boundaryVar4[j] = f1.readDataInt();
            boundaryVar5[j] = f1.readDataInt();
            boundaryVar6[j] = f1.readDataInt();
            boundaryVar7[j] = f1.readDataInt();
            boundaryVar8[j] = f1.readDataInt();
            boundaryVar9[j] = f1.readDataString();
            boundaryVar10[j] = f1.readDataString();
            if (boundaryVar9[j].equals("_"))
                boundaryVar9[j] = "WalkTo";
            if (boundaryVar10[j].equals("_"))
                boundaryVar10[j] = "Examine";

        }

        f1.close();
    }

    public static void loadRoof(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        roofCount = i;
        System.out.println("Found " + i + " roofs");
        roofVar1 = new String[i];
        roofVar2 = new int[i];
        roofVar3 = new int[i];
        roofVar4 = new int[i];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            roofVar1[j] = f1.readDataString();
            roofVar2[j] = f1.readDataInt();
            roofVar3[j] = f1.readDataInt();
            roofVar4[j] = f1.readDataInt();
        }

        f1.close();
    }

    public static void loadFloor(Buffer f1)
            throws IOException {
        f1.skipToNextDataValue();
        int i = f1.readDataInt();
        floorCount = i;
        System.out.println("Found " + i + " floors");
        floorVar1 = new String[i];
        floorVar2 = new int[i];
        floorVar3 = new int[i];
        floorVar4 = new int[i];
        for (int j = 0; j < i; j++) {
            f1.skipToNextDataValue();
            floorVar1[j] = f1.readDataString();
            floorVar2[j] = f1.readDataInt();
            floorVar3[j] = f1.readDataInt();
            floorVar4[j] = f1.readDataInt();
        }

        f1.close();
    }

    public static int getAnimationId(String name) {
        if (name.equalsIgnoreCase("na"))
            return -1;
        for (int i = 0; i < entityCount; i++)
            if (name.equalsIgnoreCase(entityVar1[i]))
                return i;

        System.out.println("WARNING: unable to match entity " + name);
        return 0;
    }

    public static int getModelId(String name) {
        if (name.equalsIgnoreCase("na"))
            return 0;
        for (int i = 0; i < modelCount; i++)
            if (modelName[i].equalsIgnoreCase(name))
                return i;

        modelName[modelCount++] = name;
        return modelCount - 1;
    }

    public static int getItemId(String name) {
        if (name.equalsIgnoreCase("na"))
            return 0;
        for (int i = 0; i < objectCount; i++) {
            for (int j = 0; j < objectVar1[i].length; j++)
                if (objectVar1[i][j].equalsIgnoreCase(name))
                    return i;

        }

        System.out.println("WARNING: unable to match object: " + name);
        return 0;
    }

    public static int getProjectileId(String s) {
        if (s.equals("_"))
            return -1;
        for (int i = 0; i < projectileCount; i++)
            if (projectileVar1[i].equalsIgnoreCase(s))
                return i;

        System.out.println("WARNING: unable to match projectile: " + s);
        return -1;
    }

}
