import jagex.DataUtil;
import jagex.client.Model;
import jagex.client.Scene;
import jagex.client.Surface;
import java.io.IOException;

public class Terrain {
	boolean aBoolean52;
	boolean aBoolean53;
	Surface surface;
	Scene scene;
	int anInt56;
	final int anInt57 = 12345678;
	final int anInt58 = 128;
	int[] color_tbl;
	int anInt60;
	int[] anIntArray61;
	int[] anIntArray62;
	int[] anIntArray63;
	byte[] land_arc;
	byte[] map_arc;
	byte[] mem_land_arc;
	byte[] mem_map_arc;
	byte[][] tile_height;
	byte[][] tile_color;
	byte[][] aByteArrayArray70;
	byte[][] aByteArrayArray71;
	byte[][] aByteArrayArray72;
	byte[][] aByteArrayArray73;
	byte[][] aByteArrayArray74;
	int[][] anIntArrayArray75;
	int reg_w;
	int reg_h;
	int[] anIntArray78;
	int[] anIntArray79;
	int[][] dijk_prev;
	int[][] collision_flags;
	int[][] anIntArrayArray82;
	boolean aBoolean83;
	Model[] ground;
	Model[][] walls;
	Model[][] rooves;
	Model mesh;

	public Terrain(Scene class3, Surface class5) {
		this.aBoolean52 = false;
		this.aBoolean53 = true;
		this.anInt56 = 750;
		this.color_tbl = new int[256];
		this.tile_height = new byte[4][2304];
		this.tile_color = new byte[4][2304];
		this.aByteArrayArray70 = new byte[4][2304];
		this.aByteArrayArray71 = new byte[4][2304];
		this.aByteArrayArray72 = new byte[4][2304];
		this.aByteArrayArray73 = new byte[4][2304];
		this.aByteArrayArray74 = new byte[4][2304];
		this.anIntArrayArray75 = new int[4][2304];
		this.reg_w = 96;
		this.reg_h = 96;
		this.anIntArray78 = new int[this.reg_w * this.reg_h * 2];
		this.anIntArray79 = new int[this.reg_w * this.reg_h * 2];
		this.dijk_prev = new int[this.reg_w][this.reg_h];
		this.collision_flags = new int[this.reg_w][this.reg_h];
		this.anIntArrayArray82 = new int[this.reg_w][this.reg_h];
		this.aBoolean83 = false;
		this.ground = new Model[64];
		this.walls = new Model[4][64];
		this.rooves = new Model[4][64];
		this.scene = class3;
		this.surface = class5;
		for (int i = 0; i < 64; i++) {
			this.color_tbl[i] = Scene.rgb(255 - i * 4, 255 - (int) (i * 1.75D),
					255 - i * 4);
		}
		for (int j = 0; j < 64; j++) {
			this.color_tbl[(j + 64)] = Scene.rgb(j * 3, 144, 0);
		}
		for (int k = 0; k < 64; k++) {
			this.color_tbl[(k + 128)] = Scene.rgb(192 - (int) (k * 1.5D),
					144 - (int) (k * 1.5D), 0);
		}
		for (int l = 0; l < 64; l++)
			this.color_tbl[(l + 192)] = Scene.rgb(96 - (int) (l * 1.5D),
					48 + (int) (l * 1.5D), 0);
	}

	public int create_path(int start_x, int start_y, int end_x2, int end_y2,
			int end_x1, int end_y1, int[] path_x, int[] path_y, boolean flag) {
		for (int k1 = 0; k1 < this.reg_w; k1++) {
			for (int l1 = 0; l1 < this.reg_h; l1++) {
				this.dijk_prev[k1][l1] = 0;
			}
		}

		int write_ptr = 0;
		int read_ptr = 0;
		int cur_x = start_x;
		int cur_y = start_y;
		this.dijk_prev[start_x][start_y] = 99;
		path_x[write_ptr] = start_x;
		path_y[(write_ptr++)] = start_y;
		int path_sz = path_x.length;
		boolean found = false;
		while (read_ptr != write_ptr) {
			cur_x = path_x[read_ptr];
			cur_y = path_y[read_ptr];
			read_ptr = (read_ptr + 1) % path_sz;
			if ((cur_x >= end_x2) && (cur_x <= end_x1) && (cur_y >= end_y2)
					&& (cur_y <= end_y1)) {
				found = true;
				break;
			}
			if (flag) {
				if ((cur_x > 0)
						&& (cur_x - 1 >= end_x2)
						&& (cur_x - 1 <= end_x1)
						&& (cur_y >= end_y2)
						&& (cur_y <= end_y1)
						&& ((this.collision_flags[(cur_x - 1)][cur_y] & 0x8) == 0)) {
					found = true;
					break;
				}
				if ((cur_x < this.reg_w - 1)
						&& (cur_x + 1 >= end_x2)
						&& (cur_x + 1 <= end_x1)
						&& (cur_y >= end_y2)
						&& (cur_y <= end_y1)
						&& ((this.collision_flags[(cur_x + 1)][cur_y] & 0x2) == 0)) {
					found = true;
					break;
				}
				if ((cur_y > 0)
						&& (cur_x >= end_x2)
						&& (cur_x <= end_x1)
						&& (cur_y - 1 >= end_y2)
						&& (cur_y - 1 <= end_y1)
						&& ((this.collision_flags[cur_x][(cur_y - 1)] & 0x4) == 0)) {
					found = true;
					break;
				}
				if ((cur_y < this.reg_h - 1)
						&& (cur_x >= end_x2)
						&& (cur_x <= end_x1)
						&& (cur_y + 1 >= end_y2)
						&& (cur_y + 1 <= end_y1)
						&& ((this.collision_flags[cur_x][(cur_y + 1)] & 0x1) == 0)) {
					found = true;
					break;
				}
			}
			if ((cur_x > 0) && (this.dijk_prev[(cur_x - 1)][cur_y] == 0)
					&& ((this.collision_flags[(cur_x - 1)][cur_y] & 0x78) == 0)) {
				path_x[write_ptr] = (cur_x - 1);
				path_y[write_ptr] = cur_y;
				write_ptr = (write_ptr + 1) % path_sz;
				this.dijk_prev[(cur_x - 1)][cur_y] = 2;
			}
			if ((cur_x < this.reg_w - 1)
					&& (this.dijk_prev[(cur_x + 1)][cur_y] == 0)
					&& ((this.collision_flags[(cur_x + 1)][cur_y] & 0x72) == 0)) {
				path_x[write_ptr] = (cur_x + 1);
				path_y[write_ptr] = cur_y;
				write_ptr = (write_ptr + 1) % path_sz;
				this.dijk_prev[(cur_x + 1)][cur_y] = 8;
			}
			if ((cur_y > 0) && (this.dijk_prev[cur_x][(cur_y - 1)] == 0)
					&& ((this.collision_flags[cur_x][(cur_y - 1)] & 0x74) == 0)) {
				path_x[write_ptr] = cur_x;
				path_y[write_ptr] = (cur_y - 1);
				write_ptr = (write_ptr + 1) % path_sz;
				this.dijk_prev[cur_x][(cur_y - 1)] = 1;
			}
			if ((cur_y < this.reg_h - 1)
					&& (this.dijk_prev[cur_x][(cur_y + 1)] == 0)
					&& ((this.collision_flags[cur_x][(cur_y + 1)] & 0x71) == 0)) {
				path_x[write_ptr] = cur_x;
				path_y[write_ptr] = (cur_y + 1);
				write_ptr = (write_ptr + 1) % path_sz;
				this.dijk_prev[cur_x][(cur_y + 1)] = 4;
			}
			if ((cur_x > 0)
					&& (cur_y > 0)
					&& ((this.collision_flags[cur_x][(cur_y - 1)] & 0x74) == 0)
					&& ((this.collision_flags[(cur_x - 1)][cur_y] & 0x78) == 0)
					&& ((this.collision_flags[(cur_x - 1)][(cur_y - 1)] & 0x7C) == 0)
					&& (this.dijk_prev[(cur_x - 1)][(cur_y - 1)] == 0)) {
				path_x[write_ptr] = (cur_x - 1);
				path_y[write_ptr] = (cur_y - 1);
				write_ptr = (write_ptr + 1) % path_sz;
				this.dijk_prev[(cur_x - 1)][(cur_y - 1)] = 3;
			}
			if ((cur_x < this.reg_w - 1)
					&& (cur_y > 0)
					&& ((this.collision_flags[cur_x][(cur_y - 1)] & 0x74) == 0)
					&& ((this.collision_flags[(cur_x + 1)][cur_y] & 0x72) == 0)
					&& ((this.collision_flags[(cur_x + 1)][(cur_y - 1)] & 0x76) == 0)
					&& (this.dijk_prev[(cur_x + 1)][(cur_y - 1)] == 0)) {
				path_x[write_ptr] = (cur_x + 1);
				path_y[write_ptr] = (cur_y - 1);
				write_ptr = (write_ptr + 1) % path_sz;
				this.dijk_prev[(cur_x + 1)][(cur_y - 1)] = 9;
			}
			if ((cur_x > 0)
					&& (cur_y < this.reg_h - 1)
					&& ((this.collision_flags[cur_x][(cur_y + 1)] & 0x71) == 0)
					&& ((this.collision_flags[(cur_x - 1)][cur_y] & 0x78) == 0)
					&& ((this.collision_flags[(cur_x - 1)][(cur_y + 1)] & 0x79) == 0)
					&& (this.dijk_prev[(cur_x - 1)][(cur_y + 1)] == 0)) {
				path_x[write_ptr] = (cur_x - 1);
				path_y[write_ptr] = (cur_y + 1);
				write_ptr = (write_ptr + 1) % path_sz;
				this.dijk_prev[(cur_x - 1)][(cur_y + 1)] = 6;
			}
			if ((cur_x >= this.reg_w - 1)
					|| (cur_y >= this.reg_h - 1)
					|| ((this.collision_flags[cur_x][(cur_y + 1)] & 0x71) != 0)
					|| ((this.collision_flags[(cur_x + 1)][cur_y] & 0x72) != 0)
					|| ((this.collision_flags[(cur_x + 1)][(cur_y + 1)] & 0x73) != 0)
					|| (this.dijk_prev[(cur_x + 1)][(cur_y + 1)] != 0)) {
				continue;
			}

			path_x[write_ptr] = (cur_x + 1);
			path_y[write_ptr] = (cur_y + 1);
			write_ptr = (write_ptr + 1) % path_sz;
			this.dijk_prev[(cur_x + 1)][(cur_y + 1)] = 12;
		}

		if (!found)
			return -1;
		read_ptr = 0;
		path_x[read_ptr] = cur_x;
		path_y[(read_ptr++)] = cur_y;
		int last_prev;
		int prev = last_prev = this.dijk_prev[cur_x][cur_y];
		for (; (cur_x != start_x) || (cur_y != start_y); prev = this.dijk_prev[cur_x][cur_y]) {
			if (prev != last_prev) {
				last_prev = prev;
				path_x[read_ptr] = cur_x;
				path_y[(read_ptr++)] = cur_y;
			}
			if ((prev & 0x2) != 0)
				cur_x++;
			else if ((prev & 0x8) != 0)
				cur_x--;
			if ((prev & 0x1) != 0)
				cur_y++;
			else if ((prev & 0x4) != 0) {
				cur_y--;
			}
		}
		return read_ptr;
	}

	public void set(int x, int y, int k) {
		this.collision_flags[x][y] |= k;
	}

	public void unset(int x, int y, int k) {
		this.collision_flags[x][y] &= 65535 - k;
	}

	public void wall_plot(int x, int y, int type, int l) {
		if ((x < 0) || (y < 0) || (x >= this.reg_w - 1)
				|| (y >= this.reg_h - 1))
			return;
		if (Config.anIntArray556[l] == 1) {
			if (type == 0) {
				this.collision_flags[x][y] |= 1;
				if (y > 0)
					set(x, y - 1, 4);
			} else if (type == 1) {
				this.collision_flags[x][y] |= 2;
				if (x > 0)
					set(x - 1, y, 8);
			} else if (type == 2) {
				this.collision_flags[x][y] |= 16;
			} else if (type == 3) {
				this.collision_flags[x][y] |= 32;
			}
			method136(x, y, 1, 1);
		}
	}

	public void wall_remove(int i, int j, int k, int l) {
		if ((i < 0) || (j < 0) || (i >= this.reg_w - 1)
				|| (j >= this.reg_h - 1))
			return;
		if (Config.anIntArray556[l] == 1) {
			if (k == 0) {
				this.collision_flags[i][j] &= 65534;
				if (j > 0)
					unset(i, j - 1, 4);
			} else if (k == 1) {
				this.collision_flags[i][j] &= 65533;
				if (i > 0)
					unset(i - 1, j, 8);
			} else if (k == 2) {
				this.collision_flags[i][j] &= 65519;
			} else if (k == 3) {
				this.collision_flags[i][j] &= 65503;
			}
			method136(i, j, 1, 1);
		}
	}

	public void obj_plot(int i, int j, int id) {
		if ((i < 0) || (j < 0) || (i >= this.reg_w - 1)
				|| (j >= this.reg_h - 1))
			return;
		if ((Config.anIntArray546[id] == 1) || (Config.anIntArray546[id] == 2)) {
			int l = method149(i, j);
			int j1;
			int i1;
			if ((l == 0) || (l == 4)) {
				i1 = Config.anIntArray544[id];
				j1 = Config.anIntArray545[id];
			} else {
				j1 = Config.anIntArray544[id];
				i1 = Config.anIntArray545[id];
			}
			for (int k1 = i; k1 < i + i1; k1++) {
				for (int l1 = j; l1 < j + j1; l1++) {
					if (Config.anIntArray546[id] == 1) {
						this.collision_flags[k1][l1] |= 64;
					} else if (l == 0) {
						this.collision_flags[k1][l1] |= 2;
						if (k1 > 0)
							set(k1 - 1, l1, 8);
					} else if (l == 2) {
						this.collision_flags[k1][l1] |= 4;
						if (l1 < this.reg_h - 1)
							set(k1, l1 + 1, 1);
					} else if (l == 4) {
						this.collision_flags[k1][l1] |= 8;
						if (k1 < this.reg_w - 1)
							set(k1 + 1, l1, 2);
					} else if (l == 6) {
						this.collision_flags[k1][l1] |= 1;
						if (l1 > 0) {
							set(k1, l1 - 1, 4);
						}
					}
				}
			}
			method136(i, j, i1, j1);
		}
	}

	public void obj_remove(int i, int j, int k) {
		if ((i < 0) || (j < 0) || (i >= this.reg_w - 1)
				|| (j >= this.reg_h - 1))
			return;
		if ((Config.anIntArray546[k] == 1) || (Config.anIntArray546[k] == 2)) {
			int l = method149(i, j);
			int j1;
			int i1;
			if ((l == 0) || (l == 4)) {
				i1 = Config.anIntArray544[k];
				j1 = Config.anIntArray545[k];
			} else {
				j1 = Config.anIntArray544[k];
				i1 = Config.anIntArray545[k];
			}
			for (int k1 = i; k1 < i + i1; k1++) {
				for (int l1 = j; l1 < j + j1; l1++) {
					if (Config.anIntArray546[k] == 1) {
						this.collision_flags[k1][l1] &= 65471;
					} else if (l == 0) {
						this.collision_flags[k1][l1] &= 65533;
						if (k1 > 0)
							unset(k1 - 1, l1, 8);
					} else if (l == 2) {
						this.collision_flags[k1][l1] &= 65531;
						if (l1 < this.reg_h - 1)
							unset(k1, l1 + 1, 1);
					} else if (l == 4) {
						this.collision_flags[k1][l1] &= 65527;
						if (k1 < this.reg_w - 1)
							unset(k1 + 1, l1, 2);
					} else if (l == 6) {
						this.collision_flags[k1][l1] &= 65534;
						if (l1 > 0) {
							unset(k1, l1 - 1, 4);
						}
					}
				}
			}
			method136(i, j, i1, j1);
		}
	}

	public void method136(int i, int j, int k, int l) {
		if ((i < 1) || (j < 1) || (i + k >= this.reg_w)
				|| (j + l >= this.reg_h))
			return;
		for (int i1 = i; i1 <= i + k; i1++)
			for (int j1 = j; j1 <= j + l; j1++)
				if (((collision_flags(i1, j1) & 0x63) != 0)
						|| ((collision_flags(i1 - 1, j1) & 0x59) != 0)
						|| ((collision_flags(i1, j1 - 1) & 0x56) != 0)
						|| ((collision_flags(i1 - 1, j1 - 1) & 0x6C) != 0))
					method137(i1, j1, 35);
				else
					method137(i1, j1, 0);
	}

	public void method137(int i, int j, int k) {
		int l = i / 12;
		int i1 = j / 12;
		int j1 = (i - 1) / 12;
		int k1 = (j - 1) / 12;
		method138(l, i1, i, j, k);
		if (l != j1)
			method138(j1, i1, i, j, k);
		if (i1 != k1)
			method138(l, k1, i, j, k);
		if ((l != j1) && (i1 != k1))
			method138(j1, k1, i, j, k);
	}

	public void method138(int i, int j, int k, int l, int i1) {
		Model class7 = this.ground[(i + j * 8)];
		for (int j1 = 0; j1 < class7.vert_cnt; j1++)
			if ((class7.vert_x[j1] == k * 128)
					&& (class7.vert_z[j1] == l * 128)) {
				class7.set_vert_ambient(j1, i1);
				return;
			}
	}

	public int collision_flags(int x, int y) {
		if ((x < 0) || (y < 0) || (x >= this.reg_w) || (y >= this.reg_h)) {
			return 0;
		}
		return this.collision_flags[x][y];
	}

	public int calc_z(int x, int y) {
		int tile_x = x >> 7;
		int tile_y = y >> 7;
		int off_x = x & 0x7F;
		int off_y = y & 0x7F;
		if ((tile_x < 0) || (tile_y < 0) || (tile_x >= this.reg_w - 1)
				|| (tile_y >= this.reg_h - 1))
			return 0;
		int base;
		int diff_h_a;
		int diff_h_b;
		if (off_x <= 128 - off_y) {
			base = tile_height(tile_x, tile_y);
			diff_h_a = tile_height(tile_x + 1, tile_y) - base;
			diff_h_b = tile_height(tile_x, tile_y + 1) - base;
		} else {
			base = tile_height(tile_x + 1, tile_y + 1);
			diff_h_a = tile_height(tile_x, tile_y + 1) - base;
			diff_h_b = tile_height(tile_x + 1, tile_y) - base;
			off_x = 128 - off_x;
			off_y = 128 - off_y;
		}
		int z = base + diff_h_a * off_x / 128 + diff_h_b * off_y / 128;
		return z;
	}

	public int tile_height(int x, int y) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return 0;
		byte part = 0;
		if ((x >= 48) && (y < 48)) {
			part = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			part = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			part = 3;
			x -= 48;
			y -= 48;
		}
		return (this.tile_height[part][(x * 48 + y)] & 0xFF) * 3;
	}

	public int method142(int i, int j) {
		if ((i < 0) || (i >= 96) || (j < 0) || (j >= 96))
			return 0;
		byte byte0 = 0;
		if ((i >= 48) && (j < 48)) {
			byte0 = 1;
			i -= 48;
		} else if ((i < 48) && (j >= 48)) {
			byte0 = 2;
			j -= 48;
		} else if ((i >= 48) && (j >= 48)) {
			byte0 = 3;
			i -= 48;
			j -= 48;
		}
		return this.tile_color[byte0][(i * 48 + j)] & 0xFF;
	}

	public int method143(int x, int y, int k) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return 0;
		byte part = 0;
		if ((x >= 48) && (y < 48)) {
			part = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			part = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			part = 3;
			x -= 48;
			y -= 48;
		}
		return this.aByteArrayArray73[part][(x * 48 + y)] & 0xFF;
	}

	public void method144(int x, int y, int k) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return;
		byte part = 0;
		if ((x >= 48) && (y < 48)) {
			part = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			part = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			part = 3;
			x -= 48;
			y -= 48;
		}
		this.aByteArrayArray73[part][(x * 48 + y)] = (byte) k;
	}

	public int method145(int i, int j, int k) {
		int l = method143(i, j, k);
		if (l == 0)
			return -1;
		int i1 = Config.anIntArray563[(l - 1)];
		return i1 != 2 ? 0 : 1;
	}

	public int method146(int i, int j, int k, int l) {
		int i1 = method143(i, j, k);
		if (i1 == 0) {
			return l;
		}
		return Config.anIntArray562[(i1 - 1)];
	}

	public int method147(int x, int y) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return 0;
		byte byte0 = 0;
		if ((x >= 48) && (y < 48)) {
			byte0 = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			byte0 = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			byte0 = 3;
			x -= 48;
			y -= 48;
		}
		return this.anIntArrayArray75[byte0][(x * 48 + y)];
	}

	public int method148(int x, int y) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return 0;
		byte part = 0;
		if ((x >= 48) && (y < 48)) {
			part = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			part = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			part = 3;
			x -= 48;
			y -= 48;
		}
		return this.aByteArrayArray72[part][(x * 48 + y)];
	}

	public int method149(int x, int y) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return 0;
		byte part = 0;
		if ((x >= 48) && (y < 48)) {
			part = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			part = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			part = 3;
			x -= 48;
			y -= 48;
		}
		return this.aByteArrayArray74[part][(x * 48 + y)];
	}

	public boolean method150(int i, int j) {
		return (method148(i, j) > 0) || (method148(i - 1, j) > 0)
				|| (method148(i - 1, j - 1) > 0) || (method148(i, j - 1) > 0);
	}

	public boolean method151(int i, int j) {
		return (method148(i, j) > 0) && (method148(i - 1, j) > 0)
				&& (method148(i - 1, j - 1) > 0) && (method148(i, j - 1) > 0);
	}

	public int method152(int x, int y) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return 0;
		byte part = 0;
		if ((x >= 48) && (y < 48)) {
			part = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			part = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			part = 3;
			x -= 48;
			y -= 48;
		}
		return this.aByteArrayArray71[part][(x * 48 + y)] & 0xFF;
	}

	public int method153(int x, int y) {
		if ((x < 0) || (x >= 96) || (y < 0) || (y >= 96))
			return 0;
		byte part = 0;
		if ((x >= 48) && (y < 48)) {
			part = 1;
			x -= 48;
		} else if ((x < 48) && (y >= 48)) {
			part = 2;
			y -= 48;
		} else if ((x >= 48) && (y >= 48)) {
			part = 3;
			x -= 48;
			y -= 48;
		}
		return this.aByteArrayArray70[part][(x * 48 + y)] & 0xFF;
	}

	public void method154(int i, int j, int k, int l) {
		String s = "m" + k + i / 10 + i % 10 + j / 10 + j % 10;
		try {
			if (this.land_arc != null) {
				byte[] file_buf = DataUtil.entry_extract(s + ".hei", 0,
						this.land_arc);
				if ((file_buf == null) && (this.mem_land_arc != null))
					file_buf = DataUtil.entry_extract(s + ".hei", 0,
							this.mem_land_arc);
				if ((file_buf != null) && (file_buf.length > 0)) {
					int j1 = 0;
					int prev_val = 0;
					for (int j3 = 0; j3 < 2304;) {
						int height = file_buf[(j1++)] & 0xFF;
						if (height < 128) {
							this.tile_height[l][(j3++)] = (byte) height;
							prev_val = height;
						}
						if (height >= 128) {
							for (int l5 = 0; l5 < height - 128; l5++) {
								this.tile_height[l][(j3++)] = (byte) prev_val;
							}
						}
					}

					prev_val = 64;
					for (int k4 = 0; k4 < 48; k4++) {
						for (int i6 = 0; i6 < 48; i6++) {
							prev_val = this.tile_height[l][(i6 * 48 + k4)]
									+ prev_val & 0x7F;
							this.tile_height[l][(i6 * 48 + k4)] = (byte) (prev_val * 2);
						}

					}

					prev_val = 0;
					for (int j6 = 0; j6 < 2304;) {
						int k7 = file_buf[(j1++)] & 0xFF;
						if (k7 < 128) {
							this.tile_color[l][(j6++)] = (byte) k7;
							prev_val = k7;
						}
						if (k7 >= 128) {
							for (int i9 = 0; i9 < k7 - 128; i9++) {
								this.tile_color[l][(j6++)] = (byte) prev_val;
							}
						}
					}

					prev_val = 35;
					for (int l7 = 0; l7 < 48; l7++) {
						for (int j9 = 0; j9 < 48; j9++) {
							prev_val = this.tile_color[l][(j9 * 48 + l7)]
									+ prev_val & 0x7F;
							this.tile_color[l][(j9 * 48 + l7)] = (byte) (prev_val * 2);
						}
					}
				} else {
					for (int k1 = 0; k1 < 2304; k1++) {
						this.tile_height[l][k1] = 0;
						this.tile_color[l][k1] = 0;
					}
				}

				file_buf = DataUtil.entry_extract(s + ".dat", 0, this.map_arc);
				if ((file_buf == null) && (this.mem_map_arc != null))
					file_buf = DataUtil.entry_extract(s + ".dat", 0,
							this.mem_map_arc);
				if ((file_buf == null) || (file_buf.length == 0))
					throw new IOException();
				int l1 = 0;
				for (int l2 = 0; l2 < 2304;) {
					int k3 = file_buf[(l1++)] & 0xFF;
					if (k3 < 128)
						this.aByteArrayArray70[l][(l2++)] = (byte) k3;
					else {
						for (int l4 = 0; l4 < k3 - 128; l4++) {
							this.aByteArrayArray70[l][(l2++)] = 0;
						}
					}
				}

				for (int l3 = 0; l3 < 2304;) {
					int i5 = file_buf[(l1++)] & 0xFF;
					if (i5 < 128)
						this.aByteArrayArray71[l][(l3++)] = (byte) i5;
					else {
						for (int k6 = 0; k6 < i5 - 128; k6++) {
							this.aByteArrayArray71[l][(l3++)] = 0;
						}
					}
				}

				for (int j5 = 0; j5 < 2304;) {
					int l6 = file_buf[(l1++)] & 0xFF;
					if (l6 < 128)
						this.anIntArrayArray75[l][(j5++)] = l6;
					else {
						for (int i8 = 0; i8 < l6 - 128; i8++) {
							this.anIntArrayArray75[l][(j5++)] = 0;
						}
					}
				}

				for (int i7 = 0; i7 < 2304;) {
					int j8 = file_buf[(l1++)] & 0xFF;
					if (j8 < 128)
						this.anIntArrayArray75[l][(i7++)] = (j8 + 12000);
					else {
						i7 += j8 - 128;
					}
				}
				for (int k8 = 0; k8 < 2304;) {
					int k9 = file_buf[(l1++)] & 0xFF;
					if (k9 < 128)
						this.aByteArrayArray72[l][(k8++)] = (byte) k9;
					else {
						for (int j10 = 0; j10 < k9 - 128; j10++) {
							this.aByteArrayArray72[l][(k8++)] = 0;
						}
					}
				}

				int l9 = 0;
				for (int k10 = 0; k10 < 2304;) {
					int i11 = file_buf[(l1++)] & 0xFF;
					if (i11 < 128) {
						this.aByteArrayArray73[l][(k10++)] = (byte) i11;
						l9 = i11;
					} else {
						for (int l11 = 0; l11 < i11 - 128; l11++) {
							this.aByteArrayArray73[l][(k10++)] = (byte) l9;
						}
					}
				}

				for (int j11 = 0; j11 < 2304;) {
					int i12 = file_buf[(l1++)] & 0xFF;
					if (i12 < 128)
						this.aByteArrayArray74[l][(j11++)] = (byte) i12;
					else {
						for (int l12 = 0; l12 < i12 - 128; l12++) {
							this.aByteArrayArray74[l][(j11++)] = 0;
						}
					}
				}

				file_buf = DataUtil.entry_extract(s + ".loc", 0, this.map_arc);
				if ((file_buf != null) && (file_buf.length > 0)) {
					int i2 = 0;
					for (int j12 = 0; j12 < 2304;) {
						int val = file_buf[(i2++)] & 0xFF;
						if (val < 128)
							this.anIntArrayArray75[l][(j12++)] = (val + 48000);
						else {
							j12 += val - 128;
						}
					}
					return;
				}
			} else {
				byte[] abyte1 = new byte[20736];
				DataUtil.read("../gamedata/maps/" + s + ".jm", abyte1, 20736);
				int j2 = 0;
				int i3 = 0;
				for (int i4 = 0; i4 < 2304; i4++) {
					j2 = j2 + abyte1[(i3++)] & 0xFF;
					this.tile_height[l][i4] = (byte) j2;
				}

				j2 = 0;
				for (int k5 = 0; k5 < 2304; k5++) {
					j2 = j2 + abyte1[(i3++)] & 0xFF;
					this.tile_color[l][k5] = (byte) j2;
				}

				for (int j7 = 0; j7 < 2304; j7++) {
					this.aByteArrayArray70[l][j7] = abyte1[(i3++)];
				}
				for (int l8 = 0; l8 < 2304; l8++) {
					this.aByteArrayArray71[l][l8] = abyte1[(i3++)];
				}
				for (int i10 = 0; i10 < 2304; i10++) {
					this.anIntArrayArray75[l][i10] = ((abyte1[i3] & 0xFF) * 256 + (abyte1[(i3 + 1)] & 0xFF));
					i3 += 2;
				}

				for (int l10 = 0; l10 < 2304; l10++) {
					this.aByteArrayArray72[l][l10] = abyte1[(i3++)];
				}
				for (int k11 = 0; k11 < 2304; k11++) {
					this.aByteArrayArray73[l][k11] = abyte1[(i3++)];
				}
				for (int k12 = 0; k12 < 2304; k12++) {
					this.aByteArrayArray74[l][k12] = abyte1[(i3++)];
				}
			}
			return;
		} catch (IOException _ex) {
			int i1 = 0;

			for (; i1 < 2304; i1++) {
				this.tile_height[l][i1] = 0;
				this.tile_color[l][i1] = 0;
				this.aByteArrayArray70[l][i1] = 0;
				this.aByteArrayArray71[l][i1] = 0;
				this.anIntArrayArray75[l][i1] = 0;
				this.aByteArrayArray72[l][i1] = 0;
				this.aByteArrayArray73[l][i1] = 0;
				if (k == 0)
					this.aByteArrayArray73[l][i1] = -6;
				if (k == 3)
					this.aByteArrayArray73[l][i1] = 8;
				this.aByteArrayArray74[l][i1] = 0;
			}
		}
	}

	public void method155() {
		if (this.aBoolean53)
			this.scene.dispose();
		for (int i = 0; i < 64; i++) {
			this.ground[i] = null;
			for (int j = 0; j < 4; j++) {
				this.walls[j][i] = null;
			}
			for (int k = 0; k < 4; k++) {
				this.rooves[k][i] = null;
			}
		}

		System.gc();
	}

	public void load(int i, int j, int k) {
		method155();
		int r_x = (i + 24) / 48;
		int r_y = (j + 24) / 48;
		method159(i, j, k, true);
		if (k == 0) {
			method159(i, j, 1, false);
			method159(i, j, 2, false);
			method154(r_x - 1, r_y - 1, k, 0);
			method154(r_x, r_y - 1, k, 1);
			method154(r_x - 1, r_y, k, 2);
			method154(r_x, r_y, k, 3);
			method157();
		}
	}

	public void method157() {
		for (int i = 0; i < 96; i++)
			for (int j = 0; j < 96; j++)
				if (method143(i, j, 0) == 250)
					if ((i == 47) && (method143(i + 1, j, 0) != 250)
							&& (method143(i + 1, j, 0) != 2))
						method144(i, j, 9);
					else if ((j == 47) && (method143(i, j + 1, 0) != 250)
							&& (method143(i, j + 1, 0) != 2))
						method144(i, j, 9);
					else
						method144(i, j, 2);
	}

	public void method158(int i, int j, int k, int l, int i1) {
		int j1 = i * 3;
		int k1 = j * 3;
		int l1 = this.scene.method229(l);
		int i2 = this.scene.method229(i1);
		l1 = l1 >> 1 & 0x7F7F7F;
		i2 = i2 >> 1 & 0x7F7F7F;
		if (k == 0) {
			this.surface.line_horiz(j1, k1, 3, l1);
			this.surface.line_horiz(j1, k1 + 1, 2, l1);
			this.surface.line_horiz(j1, k1 + 2, 1, l1);
			this.surface.line_horiz(j1 + 2, k1 + 1, 1, i2);
			this.surface.line_horiz(j1 + 1, k1 + 2, 2, i2);
			return;
		}
		if (k == 1) {
			this.surface.line_horiz(j1, k1, 3, i2);
			this.surface.line_horiz(j1 + 1, k1 + 1, 2, i2);
			this.surface.line_horiz(j1 + 2, k1 + 2, 1, i2);
			this.surface.line_horiz(j1, k1 + 1, 1, l1);
			this.surface.line_horiz(j1, k1 + 2, 2, l1);
		}
	}

	public void method159(int i, int j, int k, boolean flag) {
		int l = (i + 24) / 48;
		int i1 = (j + 24) / 48;
		method154(l - 1, i1 - 1, k, 0);
		method154(l, i1 - 1, k, 1);
		method154(l - 1, i1, k, 2);
		method154(l, i1, k, 3);
		method157();
		if (this.mesh == null)
			this.mesh = new Model(this.reg_w * this.reg_h * 2 + 256, this.reg_w
					* this.reg_h * 2 + 256, true, true, false, false, true);
		if (flag) {
			this.surface.clear();
			for (int j1 = 0; j1 < 96; j1++) {
				for (int l1 = 0; l1 < 96; l1++) {
					this.collision_flags[j1][l1] = 0;
				}
			}

			Model class7 = this.mesh;
			class7.clear();
			for (int j2 = 0; j2 < 96; j2++) {
				for (int i3 = 0; i3 < 96; i3++) {
					int i4 = -tile_height(j2, i3);
					if ((method143(j2, i3, k) > 0)
							&& (Config.anIntArray563[(method143(j2, i3, k) - 1)] == 4))
						i4 = 0;
					if ((method143(j2 - 1, i3, k) > 0)
							&& (Config.anIntArray563[(method143(j2 - 1, i3, k) - 1)] == 4))
						i4 = 0;
					if ((method143(j2, i3 - 1, k) > 0)
							&& (Config.anIntArray563[(method143(j2, i3 - 1, k) - 1)] == 4))
						i4 = 0;
					if ((method143(j2 - 1, i3 - 1, k) > 0)
							&& (Config.anIntArray563[(method143(j2 - 1, i3 - 1,
									k) - 1)] == 4))
						i4 = 0;
					int j5 = class7.vert_get(j2 * 128, i4, i3 * 128);
					int j7 = (int) (Math.random() * 10.0D) - 5;
					class7.set_vert_ambient(j5, j7);
				}

			}

			for (int j3 = 0; j3 < 95; j3++) {
				for (int j4 = 0; j4 < 95; j4++) {
					int k5 = method142(j3, j4);
					int k7 = this.color_tbl[k5];
					int i10 = k7;
					int k12 = k7;
					int l14 = 0;
					if ((k == 1) || (k == 2)) {
						k7 = 12345678;
						i10 = 12345678;
						k12 = 12345678;
					}
					if (method143(j3, j4, k) > 0) {
						int l16 = method143(j3, j4, k);
						int l5 = Config.anIntArray563[(l16 - 1)];
						int i19 = method145(j3, j4, k);
						k7 = i10 = Config.anIntArray562[(l16 - 1)];
						if (l5 == 4) {
							k7 = 1;
							i10 = 1;
							if (l16 == 12) {
								k7 = 31;
								i10 = 31;
							}
						}
						if (l5 == 5) {
							if ((method147(j3, j4) > 0)
									&& (method147(j3, j4) < 24000))
								if ((method146(j3 - 1, j4, k, k12) != 12345678)
										&& (method146(j3, j4 - 1, k, k12) != 12345678)) {
									k7 = method146(j3 - 1, j4, k, k12);
									l14 = 0;
								} else if ((method146(j3 + 1, j4, k, k12) != 12345678)
										&& (method146(j3, j4 + 1, k, k12) != 12345678)) {
									i10 = method146(j3 + 1, j4, k, k12);
									l14 = 0;
								} else if ((method146(j3 + 1, j4, k, k12) != 12345678)
										&& (method146(j3, j4 - 1, k, k12) != 12345678)) {
									i10 = method146(j3 + 1, j4, k, k12);
									l14 = 1;
								} else if ((method146(j3 - 1, j4, k, k12) != 12345678)
										&& (method146(j3, j4 + 1, k, k12) != 12345678)) {
									k7 = method146(j3 - 1, j4, k, k12);
									l14 = 1;
								}
						} else if ((l5 != 2)
								|| ((method147(j3, j4) > 0) && (method147(j3,
										j4) < 24000)))
							if ((method145(j3 - 1, j4, k) != i19)
									&& (method145(j3, j4 - 1, k) != i19)) {
								k7 = k12;
								l14 = 0;
							} else if ((method145(j3 + 1, j4, k) != i19)
									&& (method145(j3, j4 + 1, k) != i19)) {
								i10 = k12;
								l14 = 0;
							} else if ((method145(j3 + 1, j4, k) != i19)
									&& (method145(j3, j4 - 1, k) != i19)) {
								i10 = k12;
								l14 = 1;
							} else if ((method145(j3 - 1, j4, k) != i19)
									&& (method145(j3, j4 + 1, k) != i19)) {
								k7 = k12;
								l14 = 1;
							}
						if (Config.anIntArray564[(l16 - 1)] != 0)
							this.collision_flags[j3][j4] |= 64;
						if (Config.anIntArray563[(l16 - 1)] == 2)
							this.collision_flags[j3][j4] |= 128;
					}
					method158(j3, j4, l14, k7, i10);
					int i17 = tile_height(j3 + 1, j4 + 1)
							- tile_height(j3 + 1, j4) + tile_height(j3, j4 + 1)
							- tile_height(j3, j4);
					if ((k7 != i10) || (i17 != 0)) {
						int[] ai = new int[3];
						int[] ai7 = new int[3];
						if (l14 == 0) {
							if (k7 != 12345678) {
								ai[0] = (j4 + j3 * this.reg_w + this.reg_w);
								ai[1] = (j4 + j3 * this.reg_w);
								ai[2] = (j4 + j3 * this.reg_w + 1);
								int l21 = class7.face_add(3, ai, 12345678, k7);
								this.anIntArray78[l21] = j3;
								this.anIntArray79[l21] = j4;
								class7.face_pick_tag[l21] = (200000 + l21);
							}
							if (i10 != 12345678) {
								ai7[0] = (j4 + j3 * this.reg_w + 1);
								ai7[1] = (j4 + j3 * this.reg_w + this.reg_w + 1);
								ai7[2] = (j4 + j3 * this.reg_w + this.reg_w);
								int i22 = class7
										.face_add(3, ai7, 12345678, i10);
								this.anIntArray78[i22] = j3;
								this.anIntArray79[i22] = j4;
								class7.face_pick_tag[i22] = (200000 + i22);
							}
						} else {
							if (k7 != 12345678) {
								ai[0] = (j4 + j3 * this.reg_w + 1);
								ai[1] = (j4 + j3 * this.reg_w + this.reg_w + 1);
								ai[2] = (j4 + j3 * this.reg_w);
								int j22 = class7.face_add(3, ai, 12345678, k7);
								this.anIntArray78[j22] = j3;
								this.anIntArray79[j22] = j4;
								class7.face_pick_tag[j22] = (200000 + j22);
							}
							if (i10 != 12345678) {
								ai7[0] = (j4 + j3 * this.reg_w + this.reg_w);
								ai7[1] = (j4 + j3 * this.reg_w);
								ai7[2] = (j4 + j3 * this.reg_w + this.reg_w + 1);
								int k22 = class7
										.face_add(3, ai7, 12345678, i10);
								this.anIntArray78[k22] = j3;
								this.anIntArray79[k22] = j4;
								class7.face_pick_tag[k22] = (200000 + k22);
							}
						}
					} else if (k7 != 12345678) {
						int[] ai1 = new int[4];
						ai1[0] = (j4 + j3 * this.reg_w + this.reg_w);
						ai1[1] = (j4 + j3 * this.reg_w);
						ai1[2] = (j4 + j3 * this.reg_w + 1);
						ai1[3] = (j4 + j3 * this.reg_w + this.reg_w + 1);
						int l19 = class7.face_add(4, ai1, 12345678, k7);
						this.anIntArray78[l19] = j3;
						this.anIntArray79[l19] = j4;
						class7.face_pick_tag[l19] = (200000 + l19);
					}
				}

			}

			for (int k4 = 1; k4 < 95; k4++) {
				for (int i6 = 1; i6 < 95; i6++) {
					if ((method143(k4, i6, k) > 0)
							&& (Config.anIntArray563[(method143(k4, i6, k) - 1)] == 4)) {
						int l7 = Config.anIntArray562[(method143(k4, i6, k) - 1)];
						int j10 = class7.vert_get(k4 * 128,
								-tile_height(k4, i6), i6 * 128);
						int l12 = class7.vert_get((k4 + 1) * 128,
								-tile_height(k4 + 1, i6), i6 * 128);
						int i15 = class7.vert_get((k4 + 1) * 128,
								-tile_height(k4 + 1, i6 + 1), (i6 + 1) * 128);
						int j17 = class7.vert_get(k4 * 128,
								-tile_height(k4, i6 + 1), (i6 + 1) * 128);
						int[] ai2 = { j10, l12, i15, j17 };

						int i20 = class7.face_add(4, ai2, l7, 12345678);
						this.anIntArray78[i20] = k4;
						this.anIntArray79[i20] = i6;
						class7.face_pick_tag[i20] = (200000 + i20);
						method158(k4, i6, 0, l7, l7);
					} else if ((method143(k4, i6, k) == 0)
							|| (Config.anIntArray563[(method143(k4, i6, k) - 1)] != 3)) {
						if ((method143(k4, i6 + 1, k) > 0)
								&& (Config.anIntArray563[(method143(k4, i6 + 1,
										k) - 1)] == 4)) {
							int i8 = Config.anIntArray562[(method143(k4,
									i6 + 1, k) - 1)];
							int k10 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6), i6 * 128);
							int i13 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6), i6 * 128);
							int j15 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6 + 1),
									(i6 + 1) * 128);
							int k17 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6 + 1), (i6 + 1) * 128);
							int[] ai3 = { k10, i13, j15, k17 };

							int j20 = class7.face_add(4, ai3, i8, 12345678);
							this.anIntArray78[j20] = k4;
							this.anIntArray79[j20] = i6;
							class7.face_pick_tag[j20] = (200000 + j20);
							method158(k4, i6, 0, i8, i8);
						}
						if ((method143(k4, i6 - 1, k) > 0)
								&& (Config.anIntArray563[(method143(k4, i6 - 1,
										k) - 1)] == 4)) {
							int j8 = Config.anIntArray562[(method143(k4,
									i6 - 1, k) - 1)];
							int l10 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6), i6 * 128);
							int j13 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6), i6 * 128);
							int k15 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6 + 1),
									(i6 + 1) * 128);
							int l17 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6 + 1), (i6 + 1) * 128);
							int[] ai4 = { l10, j13, k15, l17 };

							int k20 = class7.face_add(4, ai4, j8, 12345678);
							this.anIntArray78[k20] = k4;
							this.anIntArray79[k20] = i6;
							class7.face_pick_tag[k20] = (200000 + k20);
							method158(k4, i6, 0, j8, j8);
						}
						if ((method143(k4 + 1, i6, k) > 0)
								&& (Config.anIntArray563[(method143(k4 + 1, i6,
										k) - 1)] == 4)) {
							int k8 = Config.anIntArray562[(method143(k4 + 1,
									i6, k) - 1)];
							int i11 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6), i6 * 128);
							int k13 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6), i6 * 128);
							int l15 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6 + 1),
									(i6 + 1) * 128);
							int i18 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6 + 1), (i6 + 1) * 128);
							int[] ai5 = { i11, k13, l15, i18 };

							int l20 = class7.face_add(4, ai5, k8, 12345678);
							this.anIntArray78[l20] = k4;
							this.anIntArray79[l20] = i6;
							class7.face_pick_tag[l20] = (200000 + l20);
							method158(k4, i6, 0, k8, k8);
						}
						if ((method143(k4 - 1, i6, k) > 0)
								&& (Config.anIntArray563[(method143(k4 - 1, i6,
										k) - 1)] == 4)) {
							int l8 = Config.anIntArray562[(method143(k4 - 1,
									i6, k) - 1)];
							int j11 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6), i6 * 128);
							int l13 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6), i6 * 128);
							int i16 = class7.vert_get((k4 + 1) * 128,
									-tile_height(k4 + 1, i6 + 1),
									(i6 + 1) * 128);
							int j18 = class7.vert_get(k4 * 128,
									-tile_height(k4, i6 + 1), (i6 + 1) * 128);
							int[] ai6 = { j11, l13, i16, j18 };

							int i21 = class7.face_add(4, ai6, l8, 12345678);
							this.anIntArray78[i21] = k4;
							this.anIntArray79[i21] = i6;
							class7.face_pick_tag[i21] = (200000 + i21);
							method158(k4, i6, 0, l8, l8);
						}
					}
				}
			}

			class7.set_light(true, 40, 48, -50, -10, -50);
			this.ground = this.mesh.split(1536, 1536, 8, 64, 233, false);
			for (int j6 = 0; j6 < 64; j6++) {
				this.scene.add(this.ground[j6]);
			}
			for (int i9 = 0; i9 < 96; i9++) {
				for (int k11 = 0; k11 < 96; k11++) {
					this.anIntArrayArray82[i9][k11] = tile_height(i9, k11);
				}
			}
		}

		this.mesh.clear();
		int k1 = 6316128;
		for (int i2 = 0; i2 < 95; i2++) {
			for (int k2 = 0; k2 < 95; k2++) {
				int k3 = method152(i2, k2);
				if ((k3 > 0)
						&& ((Config.anIntArray557[(k3 - 1)] == 0) || (this.aBoolean52))) {
					method161(this.mesh, k3 - 1, i2, k2, i2 + 1, k2);
					if ((flag) && (Config.anIntArray556[(k3 - 1)] != 0)) {
						this.collision_flags[i2][k2] |= 1;
						if (k2 > 0)
							set(i2, k2 - 1, 4);
					}
					if (flag)
						this.surface.line_horiz(i2 * 3, k2 * 3, 3, k1);
				}
				k3 = method153(i2, k2);
				if ((k3 > 0)
						&& ((Config.anIntArray557[(k3 - 1)] == 0) || (this.aBoolean52))) {
					method161(this.mesh, k3 - 1, i2, k2, i2, k2 + 1);
					if ((flag) && (Config.anIntArray556[(k3 - 1)] != 0)) {
						this.collision_flags[i2][k2] |= 2;
						if (i2 > 0)
							set(i2 - 1, k2, 8);
					}
					if (flag)
						this.surface.line_vert(i2 * 3, k2 * 3, 3, k1);
				}
				k3 = method147(i2, k2);
				if ((k3 > 0)
						&& (k3 < 12000)
						&& ((Config.anIntArray557[(k3 - 1)] == 0) || (this.aBoolean52))) {
					method161(this.mesh, k3 - 1, i2, k2, i2 + 1, k2 + 1);
					if ((flag) && (Config.anIntArray556[(k3 - 1)] != 0))
						this.collision_flags[i2][k2] |= 32;
					if (flag) {
						this.surface.plot(i2 * 3, k2 * 3, k1);
						this.surface.plot(i2 * 3 + 1, k2 * 3 + 1, k1);
						this.surface.plot(i2 * 3 + 2, k2 * 3 + 2, k1);
					}
				}
				if ((k3 > 12000)
						&& (k3 < 24000)
						&& ((Config.anIntArray557[(k3 - 12001)] == 0) || (this.aBoolean52))) {
					method161(this.mesh, k3 - 12001, i2 + 1, k2, i2, k2 + 1);
					if ((flag) && (Config.anIntArray556[(k3 - 12001)] != 0))
						this.collision_flags[i2][k2] |= 16;
					if (flag) {
						this.surface.plot(i2 * 3 + 2, k2 * 3, k1);
						this.surface.plot(i2 * 3 + 1, k2 * 3 + 1, k1);
						this.surface.plot(i2 * 3, k2 * 3 + 2, k1);
					}
				}
			}

		}

		if (flag)
			this.surface.col_sprite_define(this.anInt56 - 1, 0, 0, 285, 285);
		this.mesh.set_light(false, 60, 24, -50, -10, -50);
		this.walls[k] = this.mesh.split(1536, 1536, 8, 64, 338, true);
		for (int l2 = 0; l2 < 64; l2++) {
			this.scene.add(this.walls[k][l2]);
		}
		for (int l3 = 0; l3 < 95; l3++) {
			for (int l4 = 0; l4 < 95; l4++) {
				int k6 = method152(l3, l4);
				if (k6 > 0)
					method162(k6 - 1, l3, l4, l3 + 1, l4);
				k6 = method153(l3, l4);
				if (k6 > 0)
					method162(k6 - 1, l3, l4, l3, l4 + 1);
				k6 = method147(l3, l4);
				if ((k6 > 0) && (k6 < 12000))
					method162(k6 - 1, l3, l4, l3 + 1, l4 + 1);
				if ((k6 > 12000) && (k6 < 24000)) {
					method162(k6 - 12001, l3 + 1, l4, l3, l4 + 1);
				}
			}
		}

		for (int i5 = 1; i5 < 95; i5++) {
			for (int l6 = 1; l6 < 95; l6++) {
				int j9 = method148(i5, l6);
				if (j9 > 0) {
					int l11 = i5;
					int i14 = l6;
					int j16 = i5 + 1;
					int k18 = l6;
					int j19 = i5 + 1;
					int j21 = l6 + 1;
					int l22 = i5;
					int j23 = l6 + 1;
					int l23 = 0;
					int j24 = this.anIntArrayArray82[l11][i14];
					int l24 = this.anIntArrayArray82[j16][k18];
					int j25 = this.anIntArrayArray82[j19][j21];
					int l25 = this.anIntArrayArray82[l22][j23];
					if (j24 > 80000)
						j24 -= 80000;
					if (l24 > 80000)
						l24 -= 80000;
					if (j25 > 80000)
						j25 -= 80000;
					if (l25 > 80000)
						l25 -= 80000;
					if (j24 > l23)
						l23 = j24;
					if (l24 > l23)
						l23 = l24;
					if (j25 > l23)
						l23 = j25;
					if (l25 > l23)
						l23 = l25;
					if (l23 >= 80000)
						l23 -= 80000;
					if (j24 < 80000)
						this.anIntArrayArray82[l11][i14] = l23;
					else
						this.anIntArrayArray82[l11][i14] -= 80000;
					if (l24 < 80000)
						this.anIntArrayArray82[j16][k18] = l23;
					else
						this.anIntArrayArray82[j16][k18] -= 80000;
					if (j25 < 80000)
						this.anIntArrayArray82[j19][j21] = l23;
					else
						this.anIntArrayArray82[j19][j21] -= 80000;
					if (l25 < 80000)
						this.anIntArrayArray82[l22][j23] = l23;
					else {
						this.anIntArrayArray82[l22][j23] -= 80000;
					}
				}
			}
		}

		this.mesh.clear();
		for (int i7 = 1; i7 < 95; i7++) {
			for (int k9 = 1; k9 < 95; k9++) {
				int i12 = method148(i7, k9);
				if (i12 > 0) {
					int j14 = i7;
					int k16 = k9;
					int l18 = i7 + 1;
					int k19 = k9;
					int k21 = i7 + 1;
					int i23 = k9 + 1;
					int k23 = i7;
					int i24 = k9 + 1;
					int k24 = i7 * 128;
					int i25 = k9 * 128;
					int k25 = k24 + 128;
					int i26 = i25 + 128;
					int j26 = k24;
					int k26 = i25;
					int l26 = k25;
					int i27 = i26;
					int j27 = this.anIntArrayArray82[j14][k16];
					int k27 = this.anIntArrayArray82[l18][k19];
					int l27 = this.anIntArrayArray82[k21][i23];
					int i28 = this.anIntArrayArray82[k23][i24];
					int j28 = Config.anIntArray559[(i12 - 1)];
					if ((method151(j14, k16)) && (j27 < 80000)) {
						j27 += j28 + 80000;
						this.anIntArrayArray82[j14][k16] = j27;
					}
					if ((method151(l18, k19)) && (k27 < 80000)) {
						k27 += j28 + 80000;
						this.anIntArrayArray82[l18][k19] = k27;
					}
					if ((method151(k21, i23)) && (l27 < 80000)) {
						l27 += j28 + 80000;
						this.anIntArrayArray82[k21][i23] = l27;
					}
					if ((method151(k23, i24)) && (i28 < 80000)) {
						i28 += j28 + 80000;
						this.anIntArrayArray82[k23][i24] = i28;
					}
					if (j27 >= 80000)
						j27 -= 80000;
					if (k27 >= 80000)
						k27 -= 80000;
					if (l27 >= 80000)
						l27 -= 80000;
					if (i28 >= 80000)
						i28 -= 80000;
					byte byte0 = 16;
					if (!method150(j14 - 1, k16))
						k24 -= byte0;
					if (!method150(j14 + 1, k16))
						k24 += byte0;
					if (!method150(j14, k16 - 1))
						i25 -= byte0;
					if (!method150(j14, k16 + 1))
						i25 += byte0;
					if (!method150(l18 - 1, k19))
						k25 -= byte0;
					if (!method150(l18 + 1, k19))
						k25 += byte0;
					if (!method150(l18, k19 - 1))
						k26 -= byte0;
					if (!method150(l18, k19 + 1))
						k26 += byte0;
					if (!method150(k21 - 1, i23))
						l26 -= byte0;
					if (!method150(k21 + 1, i23))
						l26 += byte0;
					if (!method150(k21, i23 - 1))
						i26 -= byte0;
					if (!method150(k21, i23 + 1))
						i26 += byte0;
					if (!method150(k23 - 1, i24))
						j26 -= byte0;
					if (!method150(k23 + 1, i24))
						j26 += byte0;
					if (!method150(k23, i24 - 1))
						i27 -= byte0;
					if (!method150(k23, i24 + 1))
						i27 += byte0;
					i12 = Config.anIntArray560[(i12 - 1)];
					j27 = -j27;
					k27 = -k27;
					l27 = -l27;
					i28 = -i28;
					if ((method147(i7, k9) > 12000)
							&& (method147(i7, k9) < 24000)
							&& (method148(i7 - 1, k9 - 1) == 0)) {
						int[] ai8 = new int[3];
						ai8[0] = this.mesh.vert_get(l26, l27, i26);
						ai8[1] = this.mesh.vert_get(j26, i28, i27);
						ai8[2] = this.mesh.vert_get(k25, k27, k26);
						this.mesh.face_add(3, ai8, i12, 12345678);
					} else if ((method147(i7, k9) > 12000)
							&& (method147(i7, k9) < 24000)
							&& (method148(i7 + 1, k9 + 1) == 0)) {
						int[] ai9 = new int[3];
						ai9[0] = this.mesh.vert_get(k24, j27, i25);
						ai9[1] = this.mesh.vert_get(k25, k27, k26);
						ai9[2] = this.mesh.vert_get(j26, i28, i27);
						this.mesh.face_add(3, ai9, i12, 12345678);
					} else if ((method147(i7, k9) > 0)
							&& (method147(i7, k9) < 12000)
							&& (method148(i7 + 1, k9 - 1) == 0)) {
						int[] ai10 = new int[3];
						ai10[0] = this.mesh.vert_get(j26, i28, i27);
						ai10[1] = this.mesh.vert_get(k24, j27, i25);
						ai10[2] = this.mesh.vert_get(l26, l27, i26);
						this.mesh.face_add(3, ai10, i12, 12345678);
					} else if ((method147(i7, k9) > 0)
							&& (method147(i7, k9) < 12000)
							&& (method148(i7 - 1, k9 + 1) == 0)) {
						int[] ai11 = new int[3];
						ai11[0] = this.mesh.vert_get(k25, k27, k26);
						ai11[1] = this.mesh.vert_get(l26, l27, i26);
						ai11[2] = this.mesh.vert_get(k24, j27, i25);
						this.mesh.face_add(3, ai11, i12, 12345678);
					} else if ((j27 == k27) && (l27 == i28)) {
						int[] ai12 = new int[4];
						ai12[0] = this.mesh.vert_get(k24, j27, i25);
						ai12[1] = this.mesh.vert_get(k25, k27, k26);
						ai12[2] = this.mesh.vert_get(l26, l27, i26);
						ai12[3] = this.mesh.vert_get(j26, i28, i27);
						this.mesh.face_add(4, ai12, i12, 12345678);
					} else if ((j27 == i28) && (k27 == l27)) {
						int[] ai13 = new int[4];
						ai13[0] = this.mesh.vert_get(j26, i28, i27);
						ai13[1] = this.mesh.vert_get(k24, j27, i25);
						ai13[2] = this.mesh.vert_get(k25, k27, k26);
						ai13[3] = this.mesh.vert_get(l26, l27, i26);
						this.mesh.face_add(4, ai13, i12, 12345678);
					} else {
						boolean flag1 = true;
						if (method148(i7 - 1, k9 - 1) > 0)
							flag1 = false;
						if (method148(i7 + 1, k9 + 1) > 0)
							flag1 = false;
						if (!flag1) {
							int[] ai14 = new int[3];
							ai14[0] = this.mesh.vert_get(k25, k27, k26);
							ai14[1] = this.mesh.vert_get(l26, l27, i26);
							ai14[2] = this.mesh.vert_get(k24, j27, i25);
							this.mesh.face_add(3, ai14, i12, 12345678);
							int[] ai16 = new int[3];
							ai16[0] = this.mesh.vert_get(j26, i28, i27);
							ai16[1] = this.mesh.vert_get(k24, j27, i25);
							ai16[2] = this.mesh.vert_get(l26, l27, i26);
							this.mesh.face_add(3, ai16, i12, 12345678);
						} else {
							int[] ai15 = new int[3];
							ai15[0] = this.mesh.vert_get(k24, j27, i25);
							ai15[1] = this.mesh.vert_get(k25, k27, k26);
							ai15[2] = this.mesh.vert_get(j26, i28, i27);
							this.mesh.face_add(3, ai15, i12, 12345678);
							int[] ai17 = new int[3];
							ai17[0] = this.mesh.vert_get(l26, l27, i26);
							ai17[1] = this.mesh.vert_get(j26, i28, i27);
							ai17[2] = this.mesh.vert_get(k25, k27, k26);
							this.mesh.face_add(3, ai17, i12, 12345678);
						}
					}
				}
			}

		}

		this.mesh.set_light(true, 50, 50, -50, -10, -50);
		this.rooves[k] = this.mesh.split(1536, 1536, 8, 64, 169, true);
		for (int l9 = 0; l9 < 64; l9++) {
			this.scene.add(this.rooves[k][l9]);
		}
		for (int j12 = 0; j12 < 96; j12++)
			for (int k14 = 0; k14 < 96; k14++)
				if (this.anIntArrayArray82[j12][k14] >= 80000)
					this.anIntArrayArray82[j12][k14] -= 80000;
	}

	public void populate(Model[] aclass7) {
		for (int i = 0; i < this.reg_w - 2; i++)
			for (int j = 0; j < this.reg_h - 2; j++)
				if ((method147(i, j) > 48000) && (method147(i, j) < 60000)) {
					int k = method147(i, j) - 48001;
					int l = method149(i, j);
					int j1;
					int i1;
					if ((l == 0) || (l == 4)) {
						i1 = Config.anIntArray544[k];
						j1 = Config.anIntArray545[k];
					} else {
						j1 = Config.anIntArray544[k];
						i1 = Config.anIntArray545[k];
					}
					obj_plot(i, j, k);
					Model class7 = aclass7[Config.model_id[k]].copy(false,
							true, false, false);
					int k1 = (i + i + i1) * 128 / 2;
					int i2 = (j + j + j1) * 128 / 2;
					class7.translate(k1, -calc_z(k1, i2), i2);
					class7.orient(0, method149(i, j) * 32, 0);
					this.scene.add(class7);
					class7.set_light(48, 48, -50, -10, -50);
					if ((i1 > 1) || (j1 > 1))
						for (int k2 = i; k2 < i + i1; k2++)
							for (int l2 = j; l2 < j + j1; l2++)
								if (((k2 > i) || (l2 > j))
										&& (method147(k2, l2) - 48001 == k)) {
									int l1 = k2;
									int j2 = l2;
									byte byte0 = 0;
									if ((l1 >= 48) && (j2 < 48)) {
										byte0 = 1;
										l1 -= 48;
									} else if ((l1 < 48) && (j2 >= 48)) {
										byte0 = 2;
										j2 -= 48;
									} else if ((l1 >= 48) && (j2 >= 48)) {
										byte0 = 3;
										l1 -= 48;
										j2 -= 48;
									}
									this.anIntArrayArray75[byte0][(l1 * 48 + j2)] = 0;
								}
				}
	}

	public void method161(Model class7, int i, int j, int k, int l, int i1) {
		method137(j, k, 40);
		method137(l, i1, 40);
		int j1 = Config.anIntArray553[i];
		int k1 = Config.anIntArray554[i];
		int l1 = Config.anIntArray555[i];
		int i2 = j * 128;
		int j2 = k * 128;
		int k2 = l * 128;
		int l2 = i1 * 128;
		int i3 = class7.vert_get(i2, -this.anIntArrayArray82[j][k], j2);
		int j3 = class7.vert_get(i2, -this.anIntArrayArray82[j][k] - j1, j2);
		int k3 = class7.vert_get(k2, -this.anIntArrayArray82[l][i1] - j1, l2);
		int l3 = class7.vert_get(k2, -this.anIntArrayArray82[l][i1], l2);
		int[] ai = { i3, j3, k3, l3 };

		int i4 = class7.face_add(4, ai, k1, l1);
		if (Config.anIntArray557[i] == 5) {
			class7.face_pick_tag[i4] = (30000 + i);
			return;
		}
		class7.face_pick_tag[i4] = 0;
	}

	public void method162(int i, int j, int k, int l, int i1) {
		int j1 = Config.anIntArray553[i];
		if (this.anIntArrayArray82[j][k] < 80000)
			this.anIntArrayArray82[j][k] += 80000 + j1;
		if (this.anIntArrayArray82[l][i1] < 80000)
			this.anIntArrayArray82[l][i1] += 80000 + j1;
	}
}