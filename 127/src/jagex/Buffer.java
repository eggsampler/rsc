package jagex;

import java.io.IOException;
import java.math.BigInteger;

public class Buffer {
	public static int[] frames_created = new int[256];
	public static int[] frames_cum_size = new int[256];
	protected int max_frame_sz;
	protected boolean error;
	protected String err_str;
	protected int flush_tick;
	static char[] CHARSET;
	private int len;
	public int idle_tick;
	public int time_out;
	private int frame_base;
	private int ptr;
	private int op_size;
	private byte[] buf;
	private static int[] BITMASKS = { 0, 1, 3, 7, 15, 31, 63, 127, 255, 511,
			1023, 2047, 4095, 8191, 16383, 32767, 65535, 131071, 262143,
			524287, 1048575, 2097151, 4194303, 8388607, 16777215, 33554431,
			67108863, 134217727, 268435455, 536870911, 1073741823, 2147483647,
			-1 };

	public Buffer() {
		max_frame_sz = 5000;
		error = false;
		err_str = "";
		ptr = 3;
		op_size = 8;
	}

	public void close() {
	}

	public int read() throws IOException {
		return 0;
	}

	public int available() throws IOException {
		return 0;
	}

	public void read(byte[] abyte0, int j, int i) throws IOException {
	}

	public void write(byte[] abyte0, int i, int j) throws IOException {
	}

	public int method376(byte[] abyte0, int i, int j) throws IOException {
		return 0;
	}

	public int _read() throws IOException {
		return read();
	}

	public int short_read() throws IOException {
		int i = _read();
		int j = _read();
		return i * 256 + j;
	}

	public int int_read() throws IOException {
		int i = short_read();
		int j = short_read();
		return i * 65536 + j;
	}

	public void read(byte[] out, int len) throws IOException {
		read(out, 0, len);
	}

	public int frame_read(byte[] out) {
		try {
			idle_tick += 1;
			if ((time_out > 0) && (idle_tick > time_out)) {
				error = true;
				err_str = "time-out";
				time_out += time_out;
				return 0;
			}
			if ((len == 0) && (available() >= 2)) {
				len = read();
				if (len >= 160)
					len = ((len - 160) * 256 + read());
			}
			if ((len > 0) && (available() >= len)) {
				if (len >= 160) {
					read(out, len);
				} else {
					out[(len - 1)] = ((byte) read());
					if (len > 1)
						read(out, len - 1);
				}
				int i = len;
				len = 0;
				idle_tick = 0;
				return i;
			}
		} catch (IOException ioexception) {
			error = true;
			err_str = ioexception.getMessage();
		}
		return 0;
	}

	public void byte_put(int i) {
		buf[(ptr++)] = ((byte) i);
	}

	public void short_put(int i) {
		buf[(ptr++)] = ((byte) (i >> 8));
		buf[(ptr++)] = ((byte) i);
	}

	public void size_put(int i, int off) {
		buf[(frame_base + off)] = ((byte) (i >> 8));
		buf[(frame_base + off + 1)] = ((byte) i);
	}

	public void int_put(int i) {
		buf[(ptr++)] = ((byte) (i >> 24));
		buf[(ptr++)] = ((byte) (i >> 16));
		buf[(ptr++)] = ((byte) (i >> 8));
		buf[(ptr++)] = ((byte) i);
	}

	public void long_put(long l) {
		int_put((int) (l >> 32));
		int_put((int) (l & 0xFFFFFFFF));
	}

	public void str_put(String s) {
		s.getBytes(0, s.length(), buf, ptr);
		ptr += s.length();
	}

	public void bytes_put(byte[] bytes, int off, int len) {
		for (int k = 0; k < len; k++)
			buf[(ptr++)] = bytes[(off + k)];
	}

	public void enc_cred_put(long l, int i, BigInteger exp, BigInteger mod) {
		byte[] block = new byte[15];
		block[0] = ((byte) (int) (1.0D + Math.random() * 127.0D));
		block[1] = ((byte) (int) (Math.random() * 256.0D));
		block[2] = ((byte) (int) (Math.random() * 256.0D));
		DataUtil.int_put(block, 3, i);
		DataUtil.long_put(block, 7, l);
		BigInteger _block = new BigInteger(1, block);
		BigInteger _enc = _block.modPow(exp, mod);
		byte[] enc = _enc.toByteArray();
		buf[(ptr++)] = ((byte) enc.length);
		for (int j = 0; j < enc.length; j++)
			buf[(ptr++)] = enc[j];
	}

	public void enc_cred_put(String s, int i, BigInteger exp, BigInteger mod) {
		byte[] str = s.getBytes();
		int j = str.length;
		byte[] block = new byte[15];
		for (int k = 0; k < j; k += 7) {
			block[0] = ((byte) (int) (1.0D + Math.random() * 127.0D));
			block[1] = ((byte) (int) (Math.random() * 256.0D));
			block[2] = ((byte) (int) (Math.random() * 256.0D));
			block[3] = ((byte) (int) (Math.random() * 256.0D));
			DataUtil.int_put(block, 4, i);
			for (int l = 0; l < 7; l++) {
				if (k + l < j)
					block[(8 + l)] = str[(k + l)];
				else
					block[(8 + l)] = 32;
			}
			BigInteger _block = new BigInteger(1, block);
			BigInteger _enc = _block.modPow(exp, mod);
			byte[] enc = _enc.toByteArray();
			buf[(ptr++)] = ((byte) enc.length);
			for (int i1 = 0; i1 < enc.length; i1++)
				buf[(ptr++)] = enc[i1];
		}
	}

	public void enter(int op) {
		if (frame_base > max_frame_sz * 4 / 5) {
			try {
				flush(0);
			} catch (IOException ioexception) {
				error = true;
				err_str = ioexception.getMessage();
			}
		}
		if (buf == null)
			buf = new byte[max_frame_sz];
		buf[(frame_base + 2)] = ((byte) op);
		buf[(frame_base + 3)] = 0;
		ptr = (frame_base + 3);
		op_size = 8;
	}

	public void exit() {
		if (op_size != 8)
			ptr += 1;
		int i = ptr - frame_base - 2;
		if (i >= 160) {
			buf[frame_base] = ((byte) (160 + i / 256));
			buf[(frame_base + 1)] = ((byte) (i & 0xFF));
		} else {
			buf[frame_base] = ((byte) i);
			ptr -= 1;
			buf[(frame_base + 1)] = buf[ptr];
		}
		if (max_frame_sz <= 10000) {
			int op = buf[(frame_base + 2)] & 0xFF;
			frames_created[op] += 1;
			frames_cum_size[op] += ptr - frame_base;
		}
		frame_base = ptr;
	}

	public void send() throws IOException {
		exit();
		flush(0);
	}

	public void flush(int thresh) throws IOException {
		if (error) {
			frame_base = 0;
			ptr = 3;
			error = false;
			throw new IOException(err_str);
		}
		flush_tick += 1;
		if (flush_tick < thresh)
			return;
		if (frame_base > 0) {
			flush_tick = 0;
			write(buf, 0, frame_base);
		}
		frame_base = 0;
		ptr = 3;
	}

	public boolean in_frame() {
		return frame_base > 0;
	}

	static {
		CHARSET = new char[256];
		for (int i = 0; i < 256; i++) {
			CHARSET[i] = ((char) i);
		}
		CHARSET[61] = '=';
		CHARSET[59] = ';';
		CHARSET[42] = '*';
		CHARSET[43] = '+';
		CHARSET[44] = ',';
		CHARSET[45] = '-';
		CHARSET[46] = '.';
		CHARSET[47] = '/';
		CHARSET[92] = '\\';
		CHARSET[124] = '|';
		CHARSET[33] = '!';
		CHARSET[34] = '"';
	}
}