package jagex.client;

import jagex.DataUtil;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ColorModel;
import java.awt.image.DirectColorModel;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

public class Surface implements ImageProducer, ImageObserver {
	/* color constants */
	public static final int BLACK = 0;
	public static final int WHITE = 0xFFFFFF;
	public static final int RED = 0xFF0000;
	public static final int DARK_RED = 0xBB0000;
	public static final int GREEN = 0xFF00;
	public static final int BLUE = 0xFF;
	public static final int YELLOW = 0xFFFF00;
	public static final int CYAN = 0xFFFF;
	public static final int MAGENTA = 0xFF00FF;
	public static final int LIGHT_GRAY = 12632256;
	public static final int GRAY = 8421504;
	public static final int DARK_GRAY = 4210752;
	public static final int ORANGE = 0xFF7700;

	/* font constants */
	public static final int FONT_REG = 0;
	public static final int FONT_BOLD = 1;
	public static final int FONT_M_BOLD = 3;
	public static final int FONT_L_BOLD = 4;
	public static final int FONT_XL_BOLD = 5;
	public static final int FONT_XXL_BOLD = 7;

	public int w;
	public int h;
	public int area;
	public int max_w;
	public int max_h;
	ColorModel model;
	public int[] pixels;
	ImageConsumer consumer;
	public Image img;
	public int[][] sprite_pixels;
	public byte[][] sprite_raster;
	public int[][] sprite_palette;
	public int[] sprite_width;
	public int[] sprite_height;
	public int[] sprite_trans_x;
	public int[] sprite_trans_y;
	public int[] sprite_mask_w;
	public int[] sprite_mask_h;
	public boolean[] sprite_trans;
	private int origin_y;
	private int top_y;
	private int origin_x;
	private int top_x;
	public boolean skip_lines;
	static byte[][] fonts = new byte[50][];
	static int[] gylph_idx;
	static int font_cnt;
	public boolean logged_in;
	int[] sin256_tbl;
	int[] sprite_start_x;
	int[] sprite_end_x;
	int[] sprite_start_u;
	int[] sprite_end_u;
	int[] sprite_start_v;
	int[] sprite_end_v;

	public Surface(int w, int h, int size, Component component) {
		skip_lines = false;
		logged_in = false;
		top_y = h;
		top_x = w;
		max_w = (this.w = w);
		max_h = (this.h = h);
		area = (w * h);
		pixels = new int[w * h];
		sprite_pixels = new int[size][];
		sprite_trans = new boolean[size];
		sprite_raster = new byte[size][];
		sprite_palette = new int[size][];
		sprite_width = new int[size];
		sprite_height = new int[size];
		sprite_mask_w = new int[size];
		sprite_mask_h = new int[size];
		sprite_trans_x = new int[size];
		sprite_trans_y = new int[size];
		if ((w > 1) && (h > 1) && (component != null)) {
			model = new DirectColorModel(32, 16711680, 65280, 255);
			int l = w * h;
			for (int i1 = 0; i1 < l; i1++) {
				pixels[i1] = 0;
			}
			img = component.createImage(this);
			flush();
			component.prepareImage(img, component);
			flush();
			component.prepareImage(img, component);
			flush();
			component.prepareImage(img, component);
		}
	}

	public synchronized void init(int w, int h) {
		if (w > max_w)
			w = max_w;
		if (h > max_h)
			h = max_h;
		this.w = w;
		this.h = h;
		area = (w * h);
	}

	public synchronized void addConsumer(ImageConsumer consumer) {
		this.consumer = consumer;
		consumer.setDimensions(w, h);
		consumer.setProperties(null);
		consumer.setColorModel(model);
		consumer.setHints(14);
	}

	public synchronized boolean isConsumer(ImageConsumer consumer) {
		return this.consumer == consumer;
	}

	public synchronized void removeConsumer(ImageConsumer consumer) {
		if (this.consumer == consumer)
			consumer = null;
	}

	public void startProduction(ImageConsumer consumer) {
		addConsumer(consumer);
	}

	public void requestTopDownLeftRightResend(ImageConsumer imageconsumer) {
		System.out.println("TDLR");
	}

	public synchronized void flush() {
		if (consumer != null) {
			consumer.setPixels(0, 0, w, h, model,
					pixels, 0, w);
			consumer.imageComplete(2);
		}
	}

	public void set_rect(int o_x, int o_y, int t_x, int t_y) {
		if (o_x < 0)
			o_x = 0;
		if (o_y < 0)
			o_y = 0;
		if (t_x > w)
			t_x = w;
		if (t_y > h)
			t_y = h;
		origin_x = o_x;
		origin_y = o_y;
		top_x = t_x;
		top_y = t_y;
	}

	public void reset() {
		origin_x = 0;
		origin_y = 0;
		top_x = w;
		top_y = h;
	}

	public void copy(Graphics g, int i, int j) {
		flush();
		g.drawImage(img, i, j, this);
	}

	public void clear() {
		int i = w * h;
		if (!skip_lines) {
			for (int j = 0; j < i; j++) {
				pixels[j] = 0;
			}
			return;
		}
		int k = 0;
		for (int l = -h; l < 0; l += 2) {
			for (int i1 = -w; i1 < 0; i1++) {
				pixels[(k++)] = 0;
			}
			k += w;
		}
	}

	public void circle_fill(int x1, int y1, int rad, int col, int opac) {
		int a = 256 - opac;
		int r = (col >> 16 & 0xFF) * opac;
		int g = (col >> 8 & 0xFF) * opac;
		int b = (col & 0xFF) * opac;
		int start_y = y1 - rad;
		if (start_y < 0)
			start_y = 0;
		int end_y = y1 + rad;
		if (end_y >= h)
			end_y = h - 1;
		byte step = 1;
		if (skip_lines) {
			step = 2;
			if ((start_y & 0x1) != 0)
				start_y++;
		}
		for (int y = start_y; y <= end_y; y += step) {
			int dy = y - y1;
			int len = (int) Math.sqrt(rad * rad - dy * dy);
			int start_x = x1 - len;
			if (start_x < 0)
				start_x = 0;
			int end_x = x1 + len;
			if (end_x >= w)
				end_x = w - 1;
			int ptr = start_x + y * w;
			for (int i5 = start_x; i5 <= end_x; i5++) {
				int _r = (pixels[ptr] >> 16 & 0xFF) * a;
				int _g = (pixels[ptr] >> 8 & 0xFF) * a;
				int _b = (pixels[ptr] & 0xFF) * a;
				int pix = (r + _r >> 8 << 16) + (g + _g >> 8 << 8)
						+ (b + _b >> 8);
				pixels[(ptr++)] = pix;
			}
		}
	}

	public void rect_fill(int x, int y, int w, int h, int col, int opac) {
		if (x < origin_x) {
			w -= origin_x - x;
			x = origin_x;
		}
		if (y < origin_y) {
			h -= origin_y - y;
			y = origin_y;
		}
		if (x + w > top_x)
			w = top_x - x;
		if (y + h > top_y)
			h = top_y - y;
		int k1 = 256 - opac;
		int l1 = (col >> 16 & 0xFF) * opac;
		int i2 = (col >> 8 & 0xFF) * opac;
		int j2 = (col & 0xFF) * opac;
		int ptr_step = this.w - w;
		byte step = 1;
		if (skip_lines) {
			step = 2;
			ptr_step += this.w;
			if ((y & 0x1) != 0) {
				y++;
				h--;
			}
		}
		int ptr = x + y * this.w;
		for (int l3 = 0; l3 < h; l3 += step) {
			for (int i4 = -w; i4 < 0; i4++) {
				int k2 = (pixels[ptr] >> 16 & 0xFF) * k1;
				int l2 = (pixels[ptr] >> 8 & 0xFF) * k1;
				int i3 = (pixels[ptr] & 0xFF) * k1;
				int j4 = (l1 + k2 >> 8 << 16) + (i2 + l2 >> 8 << 8)
						+ (j2 + i3 >> 8);
				pixels[(ptr++)] = j4;
			}

			ptr += ptr_step;
		}
	}

	public void grad_rect_fill(int x1, int y1, int w, int h, int col_a,
			int col_b) {
		if (x1 < origin_x) {
			w -= origin_x - x1;
			x1 = origin_x;
		}
		if (x1 + w > top_x)
			w = top_x - x1;
		int b_r = col_b >> 16 & 0xFF;
		int b_g = col_b >> 8 & 0xFF;
		int b_b = col_b & 0xFF;
		int a_r = col_a >> 16 & 0xFF;
		int a_g = col_a >> 8 & 0xFF;
		int a_b = col_a & 0xFF;
		int ptr_step = w - w;
		byte step = 1;
		if (skip_lines) {
			step = 2;
			ptr_step += w;
			if ((y1 & 0x1) != 0) {
				y1++;
				h--;
			}
		}
		int ptr = x1 + y1 * w;
		for (int y = 0; y < h; y += step)
			if ((y + y1 >= origin_y) && (y + y1 < top_y)) {
				int l3 = ((b_r * y + a_r * (h - y)) / h << 16)
						+ ((b_g * y + a_g * (h - y)) / h << 8)
						+ (b_b * y + a_b * (h - y)) / h;
				for (int i4 = -w; i4 < 0; i4++) {
					pixels[(ptr++)] = l3;
				}
				ptr += ptr_step;
			} else {
				ptr += w;
			}
	}

	public void rect_fill(int x, int y, int w, int h, int col) {
		if (x < origin_x) {
			w -= origin_x - x;
			x = origin_x;
		}
		if (y < origin_y) {
			h -= origin_y - y;
			y = origin_y;
		}
		if (x + w > top_x)
			w = top_x - x;
		if (y + h > top_y)
			h = top_y - y;
		int ptr_step = this.w - w;
		byte step = 1;
		if (skip_lines) {
			step = 2;
			ptr_step += this.w;
			if ((y & 0x1) != 0) {
				y++;
				h--;
			}
		}
		int ptr = x + y * this.w;
		for (int yy = -h; yy < 0; yy += step)
		{
			for (int xx = -w; xx < 0; xx++)
			{
				pixels[(ptr++)] = col;
			}
			ptr += ptr_step;
		}
	}

	public void rect_draw(int x, int y, int w, int h, int col) {
		line_horiz(x, y, w, col);
		line_horiz(x, y + h - 1, w, col);
		line_vert(x, y, h, col);
		line_vert(x + w - 1, y, h, col);
	}

	public void line_horiz(int x1, int y1, int w, int col) {
		if ((y1 < origin_y) || (y1 >= top_y))
			return;
		if (x1 < origin_x) {
			w -= origin_x - x1;
			x1 = origin_x;
		}
		if (x1 + w > top_x)
			w = top_x - x1;
		int i1 = x1 + y1 * this.w;
		for (int j1 = 0; j1 < w; j1++)
			pixels[(i1 + j1)] = col;
	}

	public void line_vert(int x, int y, int h, int col) {
		if ((x < origin_x) || (x >= top_x))
			return;
		if (y < origin_y) {
			h -= origin_y - y;
			y = origin_y;
		}
		if (y + h > top_x)
			h = top_y - y;
		int i1 = x + y * w;
		for (int j1 = 0; j1 < h; j1++)
			this.pixels[(i1 + j1 * w)] = col;
	}

	public void plot(int x, int y, int col) {
		if ((x >= origin_x) && (y >= origin_y) && (x < top_x)
				&& (y < top_y))
			pixels[(x + y * w)] = col;
	}

	public void darken() {
		int k = w * h;
		for (int j = 0; j < k; j++) {
			int i = pixels[j] & 0xFFFFFF;
			pixels[j] = ((i >>> 1 & 0x7F7F7F) + (i >>> 2 & 0x3F3F3F)
					+ (i >>> 3 & 0x1F1F1F) + (i >>> 4 & 0xF0F0F));
		}
	}

	public void blur(int dx, int dy, int sx, int sy, int w, int h) {
		for (int x = sx; x < sx + w; x++) {
			for (int y = sy; y < sy + h; y++) {
				int sum_r = 0;
				int sum_g = 0;
				int sum_b = 0;
				int samples = 0;
				for (int _x = x - dx; _x <= x + dx; _x++) {
					if ((_x >= 0) && (_x < this.w)) {
						for (int _y = y - dy; _y <= y + dy; _y++) {
							if ((_y >= 0) && (_y < this.h)) {
								int pix = pixels[(_x + this.w * _y)];
								sum_r += (pix >> 16 & 0xFF);
								sum_g += (pix >> 8 & 0xFF);
								sum_b += (pix & 0xFF);
								samples++;
							}
						}
					}
				}
				pixels[(x + this.w * y)] = ((sum_r / samples << 16)
							+ (sum_g / samples << 8) + sum_b / samples);
			}
		}
	}

	public static int rgb(int r, int g, int b) {
		return (r << 16) + (g << 8) + b;
	}

	public void sprites_release() {
		for (int i = 0; i < sprite_pixels.length; i++) {
			sprite_pixels[i] = null;
			sprite_width[i] = 0;
			sprite_height[i] = 0;
			sprite_raster[i] = null;
			sprite_palette[i] = null;
		}
	}

	public void sprite_define(int off, byte[] dat_buf, byte[] idx_buf, int cnt) {
		int idx_ptr = DataUtil.short_get(dat_buf, 0);
		int mask_w = DataUtil.short_get(idx_buf, idx_ptr);
		idx_ptr += 2;
		int mask_h = DataUtil.short_get(idx_buf, idx_ptr);
		idx_ptr += 2;
		int sz = idx_buf[(idx_ptr++)] & 0xFF;
		int[] palette = new int[sz];
		palette[0] = 16711935;
		for (int ptr = 0; ptr < sz - 1; ptr++) {
			palette[(ptr + 1)] = (((idx_buf[idx_ptr] & 0xFF) << 16)
					+ ((idx_buf[(idx_ptr + 1)] & 0xFF) << 8) + (idx_buf[(idx_ptr + 2)] & 0xFF));
			idx_ptr += 3;
		}

		int dat_ptr = 2;
		for (int idx = off; idx < off + cnt; idx++) {
			sprite_trans_x[idx] = (idx_buf[(idx_ptr++)] & 0xFF);
			sprite_trans_y[idx] = (idx_buf[(idx_ptr++)] & 0xFF);
			sprite_width[idx] = DataUtil.short_get(idx_buf, idx_ptr);
			idx_ptr += 2;
			sprite_height[idx] = DataUtil.short_get(idx_buf, idx_ptr);
			idx_ptr += 2;
			int idx_mode = idx_buf[(idx_ptr++)] & 0xFF;
			int raster_len = sprite_width[idx] * sprite_height[idx];
			sprite_raster[idx] = new byte[raster_len];
			sprite_palette[idx] = palette;
			sprite_mask_w[idx] = mask_w;
			sprite_mask_h[idx] = mask_h;
			sprite_pixels[idx] = null;
			sprite_trans[idx] = false;
			if ((sprite_trans_x[idx] != 0)
					|| (sprite_trans_y[idx] != 0))
				sprite_trans[idx] = true;
			if (idx_mode == 0) {
				for (int ptr = 0; ptr < raster_len; ptr++) {
					sprite_raster[idx][ptr] = dat_buf[(dat_ptr++)];
					if (sprite_raster[idx][ptr] == 0)
						sprite_trans[idx] = true;
				}
			} else if (idx_mode == 1)
				for (int x = 0; x < sprite_width[idx]; x++)
					for (int y = 0; y < sprite_height[idx]; y++) {
						sprite_raster[idx][(x + y * sprite_width[idx])] = dat_buf[(dat_ptr++)];
						if (sprite_raster[idx][(x + y
								* sprite_width[idx])] == 0)
							sprite_trans[idx] = true;
					}
		}
	}

	public void sprite_define(byte[] data, int data_off, int off,
			boolean trans, int sprite_horiz_cnt, int sprite_vert_cnt,
			boolean indexed) {
		int w = (data[(13 + data_off)] & 0xFF) * 256
				+ (data[(12 + data_off)] & 0xFF);
		int h = (data[(15 + data_off)] & 0xFF) * 256
				+ (data[(14 + data_off)] & 0xFF);
		int trans_ptr = -1;
		int[] palette = new int[256];
		for (int ptr = 0; ptr < 256; ptr++) {
			palette[ptr] = (-16777216
					+ ((data[(data_off + 20 + ptr * 3)] & 0xFF) << 16)
					+ ((data[(data_off + 19 + ptr * 3)] & 0xFF) << 8) + (data[(data_off + 18 + ptr * 3)] & 0xFF));
			if (palette[ptr] == -65281) {
				trans_ptr = ptr;
			}
		}
		if (trans_ptr == -1)
			trans = false;
		if ((indexed) && (trans))
			palette[trans_ptr] = palette[0];
		int _w = w / sprite_horiz_cnt;
		int _h = h / sprite_vert_cnt;
		int[] raster = new int[_w * _h];
		for (int _y = 0; _y < sprite_vert_cnt; _y++)
			for (int _x = 0; _x < sprite_horiz_cnt; _x++) {
				int ptr = 0;
				for (int x = _h * _y; x < _h * (_y + 1); x++) {
					for (int y = _w * _x; y < _w * (_x + 1); y++) {
						if (indexed)
							raster[(ptr++)] = (data[(data_off + 786 + y + (h
									- x - 1)
									* w)] & 0xFF);
						else {
							raster[(ptr++)] = palette[(data[(data_off + 786 + y + (h
									- x - 1)
									* w)] & 0xFF)];
						}
					}
				}
				if (indexed)
					sprite_define(raster, _w, _h, off++, trans, palette,
							trans_ptr);
				else
					sprite_define(raster, _w, _h, off++, trans, null, -65281);
			}
	}

	private void sprite_define(int[] raster, int w, int h, int off,
			boolean alpha, int[] palette, int trans_val) {
		int start_x = 0;
		int start_y = 0;
		int end_x = w;
		int end_y = h;
		if (alpha) {
			for (int y = 0; y < h; y++) {
				for (int x = 0; x < w; x++) {
					int ptr = raster[(x + y * w)];
					if (ptr != trans_val) {
						start_y = y;
					}
				}

			}

			for (int x = 0; x < w; x++) {
				for (int y = 0; y < h; y++) {
					int ptr = raster[(x + y * w)];
					if (ptr != trans_val) {
						start_x = x;
					}
				}

			}

			for (int y = h - 1; y >= 0; y--) {
				for (int x = 0; x < w; x++) {
					int ptr = raster[(x + y * w)];
					if (ptr != trans_val) {
						end_y = y + 1;
					}
				}

			}

			for (int x = w - 1; x >= 0; x--) {
				for (int y = 0; y < h; y++) {
					int ptr = raster[(x + y * w)];
					if (ptr != trans_val) {
						end_x = x + 1;
					}
				}
			}
		}

		sprite_width[off] = (end_x - start_x);
		sprite_height[off] = (end_y - start_y);
		sprite_trans[off] = alpha;
		sprite_trans_x[off] = start_x;
		sprite_trans_y[off] = start_y;
		sprite_mask_w[off] = w;
		sprite_mask_h[off] = h;
		if (palette == null) {
			sprite_pixels[off] = new int[(end_x - start_x)
					* (end_y - start_y)];
			int ptr = 0;
			for (int y = start_y; y < end_y; y++) {
				for (int x = start_x; x < end_x; x++) {
					int pix = raster[(x + y * w)];
					if (alpha) {
						if (pix == trans_val)
							pix = 0;
						if (pix == -16777216)
							pix = -16711423;
					}
					sprite_pixels[off][(ptr++)] = (pix & 0xFFFFFF);
				}

			}

			return;
		}
		sprite_raster[off] = new byte[(end_x - start_x)
				* (end_y - start_y)];
		sprite_palette[off] = palette;
		int ptr = 0;
		for (int y = start_y; y < end_y; y++)
			for (int x = start_x; x < end_x; x++) {
				int idx = raster[(x + y * w)];
				if (alpha)
					if (idx == trans_val)
						idx = 0;
					else if (idx == 0)
						idx = trans_val;
				sprite_raster[off][(ptr++)] = ((byte) idx);
			}
	}

	public void sprite_posterize(int i) {
		int len = sprite_width[i] * sprite_height[i];
		int[] pixels = sprite_pixels[i];
		int[] rgb332_idx = new int[32768];
		for (int ptr = 0; ptr < len; ptr++) {
			int l = pixels[ptr];
			rgb332_idx[(((l & 0xF80000) >> 9) + ((l & 0xF800) >> 6) + ((l & 0xF8) >> 3))] += 1;
		}

		int[] palette = new int[256];
		palette[0] = 16711935;
		int[] freq = new int[256];
		for (int col = 0; col < 32768; col++) {
			int use = rgb332_idx[col];
			if (use > freq['ÿ']) {
				for (int k1 = 1; k1 < 256; k1++) {
					if (use <= freq[k1])
						continue;
					for (int i2 = 255; i2 > k1; i2--) {
						palette[i2] = palette[(i2 - 1)];
						freq[i2] = freq[(i2 - 1)];
					}

					palette[k1] = (((col & 0x7C00) << 9) + ((col & 0x3E0) << 6)
							+ ((col & 0x1F) << 3) + 263172);
					freq[k1] = use;
					break;
				}
			}

			rgb332_idx[col] = -1;
		}

		byte[] raster = new byte[len];
		for (int l1 = 0; l1 < len; l1++) {
			int pix = pixels[l1];
			int pix332 = ((pix & 0xF80000) >> 9) + ((pix & 0xF800) >> 6)
					+ ((pix & 0xF8) >> 3);
			int new_pix = rgb332_idx[pix332];
			if (new_pix == -1) {
				int min_dist = 999999999;
				int r = pix >> 16 & 0xFF;
				int g = pix >> 8 & 0xFF;
				int b = pix & 0xFF;
				for (int ptr = 0; ptr < 256; ptr++) {
					int _pix = palette[ptr];
					int _r = _pix >> 16 & 0xFF;
					int _g = _pix >> 8 & 0xFF;
					int _b = _pix & 0xFF;
					int dist = (r - _r) * (r - _r) + (g - _g) * (g - _g)
							+ (b - _b) * (b - _b);
					if (dist < min_dist) {
						min_dist = dist;
						new_pix = ptr;
					}
				}

				rgb332_idx[pix332] = new_pix;
			}
			raster[l1] = (byte) new_pix;
		}

		sprite_raster[i] = raster;
		sprite_palette[i] = palette;
		sprite_pixels[i] = null;
	}

	public void sprite_resolve(int i) {
		if (sprite_raster[i] == null)
			return;
		int len = sprite_width[i] * sprite_height[i];
		byte[] raster = sprite_raster[i];
		int[] palette = sprite_palette[i];
		int[] pixels = new int[len];
		for (int ptr = 0; ptr < len; ptr++) {
			int pix = palette[(raster[ptr] & 0xFF)];
			if (pix == 0)
				pix = 1;
			else if (pix == 16711935)
				pix = 0;
			pixels[ptr] = pix;
		}

		sprite_pixels[i] = pixels;
		sprite_raster[i] = null;
		sprite_palette[i] = null;
	}

	public void col_sprite_define(int i, int x1, int y1, int w, int h) {
		sprite_width[i] = w;
		sprite_height[i] = h;
		sprite_trans[i] = false;
		sprite_trans_x[i] = 0;
		sprite_trans_y[i] = 0;
		sprite_mask_w[i] = w;
		sprite_mask_h[i] = h;
		int len = w * h;
		int ptr = 0;
		sprite_pixels[i] = new int[len];
		for (int x = x1; x < x1 + w; x++)
			for (int y = y1; y < y1 + h; y++)
				sprite_pixels[i][(ptr++)] = pixels[(x + y * w)];
	}

	public void row_sprite_define(int i, int x1, int y1, int w, int h) {
		sprite_width[i] = w;
		sprite_height[i] = h;
		sprite_trans[i] = false;
		sprite_trans_x[i] = 0;
		sprite_trans_y[i] = 0;
		sprite_mask_w[i] = w;
		sprite_mask_h[i] = h;
		int len = w * h;
		int ptr = 0;
		sprite_pixels[i] = new int[len];
		for (int y = y1; y < y1 + h; y++)
			for (int x = x1; x < x1 + w; x++)
				sprite_pixels[i][(ptr++)] = pixels[(x + y * w)];
	}

	public void sprite_plot(int x, int y, int off) {
		if (sprite_trans[off]) {
			x += sprite_trans_x[off];
			y += sprite_trans_y[off];
		}
		int ptr = x + y * w;
		int sprite_ptr = 0;
		int h = sprite_height[off];
		int w = sprite_width[off];
		int ptr_step = w - w;
		int sprite_ptr_step = 0;
		if (y < origin_y) {
			int dy = origin_y - y;
			h -= dy;
			y = origin_y;
			sprite_ptr += dy * w;
			ptr += dy * w;
		}
		if (y + h >= top_y)
			h -= y + h - top_y + 1;
		if (x < origin_x) {
			int dx = origin_x - x;
			w -= dx;
			x = origin_x;
			sprite_ptr += dx;
			ptr += dx;
			sprite_ptr_step += dx;
			ptr_step += dx;
		}
		if (x + w >= top_x) {
			int l2 = x + w - top_x + 1;
			w -= l2;
			sprite_ptr_step += l2;
			ptr_step += l2;
		}
		if ((w <= 0) || (h <= 0))
			return;
		byte step = 1;
		if (skip_lines) {
			step = 2;
			ptr_step += w;
			sprite_ptr_step += sprite_width[off];
			if ((y & 0x1) != 0) {
				ptr += w;
				h--;
			}
		}
		if (sprite_pixels[off] == null) {
			plot(pixels, sprite_raster[off],
					sprite_palette[off], sprite_ptr, ptr, w, h, ptr_step,
					sprite_ptr_step, step);
			return;
		}
		plot(pixels, sprite_pixels[off], 0, sprite_ptr, ptr, w, h,
				ptr_step, sprite_ptr_step, step);
	}

	public void resize_sprite_plot(int x, int y, int new_w, int new_h, int off) {
		try {
			int w = sprite_width[off];
			int h = sprite_height[off];
			int sprite_ptr_step = 0;
			int sprite_ptr = 0;
			int ratio_w = (w << 16) / new_w;
			int ratio_h = (h << 16) / new_h;
			if (sprite_trans[off]) {
				int mask_w = sprite_mask_w[off];
				int mask_h = sprite_mask_h[off];
				ratio_w = (mask_w << 16) / new_w;
				ratio_h = (mask_h << 16) / new_h;
				x += (sprite_trans_x[off] * new_w + mask_w - 1) / mask_w;
				y += (sprite_trans_y[off] * new_h + mask_h - 1) / mask_h;
				if (sprite_trans_x[off] * new_w % mask_w != 0)
					sprite_ptr_step = (mask_w - sprite_trans_x[off]
							* new_w % mask_w << 16)
							/ new_w;
				if (sprite_trans_y[off] * new_h % mask_h != 0)
					sprite_ptr = (mask_h - sprite_trans_y[off] * new_h
							% mask_h << 16)
							/ new_h;
				new_w = new_w
						* (sprite_width[off] - (sprite_ptr_step >> 16))
						/ mask_w;
				new_h = new_h * (sprite_height[off] - (sprite_ptr >> 16))
						/ mask_h;
			}
			int ptr = x + y * w;
			int ptr_step = w - new_w;
			if (y < origin_y) {
				int dy = origin_y - y;
				new_h -= dy;
				y = 0;
				ptr += dy * w;
				sprite_ptr += ratio_h * dy;
			}
			if (y + new_h >= top_y)
				new_h -= y + new_h - top_y + 1;
			if (x < origin_x) {
				int dx = origin_x - x;
				new_w -= dx;
				x = 0;
				ptr += dx;
				sprite_ptr_step += ratio_w * dx;
				ptr_step += dx;
			}
			if (x + new_w >= top_x) {
				int dx = x + new_w - top_x + 1;
				new_w -= dx;
				ptr_step += dx;
			}
			byte step = 1;
			if (skip_lines) {
				step = 2;
				ptr_step += w;
				ratio_h += ratio_h;
				if ((y & 0x1) != 0) {
					ptr += w;
					new_h--;
				}
			}
			resize_plot(pixels, sprite_pixels[off], 0,
					sprite_ptr_step, sprite_ptr, ptr, ptr_step, new_w, new_h,
					ratio_w, ratio_h, w, step);
			return;
		} catch (Exception _ex) {
			System.out.println("error in sprite clipping routine");
		}
	}

	public void trans_sprite_plot(int x, int y, int off, int opac) {
		if (sprite_trans[off]) {
			x += sprite_trans_x[off];
			y += sprite_trans_y[off];
		}
		int ptr = x + y * w;
		int sprite_ptr = 0;
		int h = sprite_height[off];
		int w = sprite_width[off];
		int ptr_step = w - w;
		int sprite_ptr_step = 0;
		if (y < origin_y) {
			int dy = origin_y - y;
			h -= dy;
			y = origin_y;
			sprite_ptr += dy * w;
			ptr += dy * w;
		}
		if (y + h >= top_y)
			h -= y + h - top_y + 1;
		if (x < origin_x) {
			int dx = origin_x - x;
			w -= dx;
			x = origin_x;
			sprite_ptr += dx;
			ptr += dx;
			sprite_ptr_step += dx;
			ptr_step += dx;
		}
		if (x + w >= top_x) {
			int dx = x + w - top_x + 1;
			w -= dx;
			sprite_ptr_step += dx;
			ptr_step += dx;
		}
		if ((w <= 0) || (h <= 0))
			return;
		byte step = 1;
		if (skip_lines) {
			step = 2;
			ptr_step += w;
			sprite_ptr_step += sprite_width[off];
			if ((y & 0x1) != 0) {
				ptr += w;
				h--;
			}
		}
		if (sprite_pixels[off] == null) {
			trans_plot(pixels, sprite_raster[off],
					sprite_palette[off], sprite_ptr, ptr, w, h, ptr_step,
					sprite_ptr_step, step, opac);
			return;
		}
		trans_plot(pixels, sprite_pixels[off], 0, sprite_ptr, ptr, w,
				h, ptr_step, sprite_ptr_step, step, opac);
	}

	public void resize_trans_sprite_plot(int x, int y, int new_w, int new_h,
			int off, int opac) {
		try {
			int w = sprite_width[off];
			int h = sprite_height[off];
			int in_ptr = 0;
			int in_ptr_step = 0;
			int ratio_w = (w << 16) / new_w;
			int ratio_h = (h << 16) / new_h;
			if (sprite_trans[off]) {
				int i3 = sprite_mask_w[off];
				int k3 = sprite_mask_h[off];
				ratio_w = (i3 << 16) / new_w;
				ratio_h = (k3 << 16) / new_h;
				x += (sprite_trans_x[off] * new_w + i3 - 1) / i3;
				y += (sprite_trans_y[off] * new_h + k3 - 1) / k3;
				if (sprite_trans_x[off] * new_w % i3 != 0)
					in_ptr = (i3 - sprite_trans_x[off] * new_w % i3 << 16)
							/ new_w;
				if (sprite_trans_y[off] * new_h % k3 != 0)
					in_ptr_step = (k3 - sprite_trans_y[off] * new_h % k3 << 16)
							/ new_h;
				new_w = new_w * (sprite_width[off] - (in_ptr >> 16)) / i3;
				new_h = new_h * (sprite_height[off] - (in_ptr_step >> 16))
						/ k3;
			}
			int out_ptr = x + y * w;
			int out_ptr_step = w - new_w;
			if (y < origin_y) {
				int dy = origin_y - y;
				new_h -= dy;
				y = 0;
				out_ptr += dy * w;
				in_ptr_step += ratio_h * dy;
			}
			if (y + new_h >= top_y)
				new_h -= y + new_h - top_y + 1;
			if (x < origin_x) {
				int dx = origin_x - x;
				new_w -= dx;
				x = 0;
				out_ptr += dx;
				in_ptr += ratio_w * dx;
				out_ptr_step += dx;
			}
			if (x + new_w >= top_x) {
				int dx = x + new_w - top_x + 1;
				new_w -= dx;
				out_ptr_step += dx;
			}
			byte step = 1;
			if (skip_lines) {
				step = 2;
				out_ptr_step += w;
				ratio_h += ratio_h;
				if ((y & 0x1) != 0) {
					out_ptr += w;
					new_h--;
				}
			}
			resize_trans_plot(pixels, sprite_pixels[off], 0, in_ptr,
					in_ptr_step, out_ptr, out_ptr_step, new_w, new_h, ratio_w,
					ratio_h, w, step, opac);
			return;
		} catch (Exception _ex) {
			System.out.println("error in sprite clipping routine");
		}
	}

	public void resize_tint_sprite_plot(int x, int y, int new_w, int new_h,
			int off, int col) {
		try {
			int w = sprite_width[off];
			int h = sprite_height[off];
			int in_ptr = 0;
			int in_ptr_step = 0;
			int ratio_w = (w << 16) / new_w;
			int ratio_h = (h << 16) / new_h;
			if (sprite_trans[off]) {
				int mask_w = sprite_mask_w[off];
				int mask_h = sprite_mask_h[off];
				ratio_w = (mask_w << 16) / new_w;
				ratio_h = (mask_h << 16) / new_h;
				x += (sprite_trans_x[off] * new_w + mask_w - 1) / mask_w;
				y += (sprite_trans_y[off] * new_h + mask_h - 1) / mask_h;
				if (sprite_trans_x[off] * new_w % mask_w != 0)
					in_ptr = (mask_w - sprite_trans_x[off] * new_w
							% mask_w << 16)
							/ new_w;
				if (sprite_trans_y[off] * new_h % mask_h != 0)
					in_ptr_step = (mask_h - sprite_trans_y[off] * new_h
							% mask_h << 16)
							/ new_h;
				new_w = new_w * (sprite_width[off] - (in_ptr >> 16))
						/ mask_w;
				new_h = new_h * (sprite_height[off] - (in_ptr_step >> 16))
						/ mask_h;
			}
			int out_ptr = x + y * w;
			int out_ptr_step = w - new_w;
			if (y < origin_y) {
				int dy = origin_y - y;
				new_h -= dy;
				y = 0;
				out_ptr += dy * w;
				in_ptr_step += ratio_h * dy;
			}
			if (y + new_h >= top_y)
				new_h -= y + new_h - top_y + 1;
			if (x < origin_x) {
				int dx = origin_x - x;
				new_w -= dx;
				x = 0;
				out_ptr += dx;
				in_ptr += ratio_w * dx;
				out_ptr_step += dx;
			}
			if (x + new_w >= top_x) {
				int dx = x + new_w - top_x + 1;
				new_w -= dx;
				out_ptr_step += dx;
			}
			byte step = 1;
			if (skip_lines) {
				step = 2;
				out_ptr_step += w;
				ratio_h += ratio_h;
				if ((y & 0x1) != 0) {
					out_ptr += w;
					new_h--;
				}
			}
			resize_tint_plot(pixels, sprite_pixels[off], 0, in_ptr,
					in_ptr_step, out_ptr, out_ptr_step, new_w, new_h, ratio_w,
					ratio_h, w, step, col);
			return;
		} catch (Exception _ex) {
			System.out.println("error in sprite clipping routine");
		}
	}

	private void plot(int[] out, int[] in, int i, int in_ptr, int out_ptr,
			int w, int h, int out_ptr_step, int in_ptr_step, int step) {
		int blocks = -(w >> 2);
		w = -(w & 0x3);
		for (int j2 = -h; j2 < 0; j2 += step) {
			for (int k2 = blocks; k2 < 0; k2++) {
				i = in[(in_ptr++)];
				if (i != 0)
					out[(out_ptr++)] = i;
				else
					out_ptr++;
				i = in[(in_ptr++)];
				if (i != 0)
					out[(out_ptr++)] = i;
				else
					out_ptr++;
				i = in[(in_ptr++)];
				if (i != 0)
					out[(out_ptr++)] = i;
				else
					out_ptr++;
				i = in[(in_ptr++)];
				if (i != 0)
					out[(out_ptr++)] = i;
				else {
					out_ptr++;
				}
			}
			for (int l2 = w; l2 < 0; l2++) {
				i = in[(in_ptr++)];
				if (i != 0)
					out[(out_ptr++)] = i;
				else {
					out_ptr++;
				}
			}
			out_ptr += out_ptr_step;
			in_ptr += in_ptr_step;
		}
	}

	private void plot(int[] out, byte[] raster, int[] palette, int in_ptr,
			int out_ptr, int w, int h, int out_ptr_step, int in_ptr_step,
			int step) {
		int blocks = -(w >> 2);
		w = -(w & 0x3);
		for (int i2 = -h; i2 < 0; i2 += step) {
			for (int j2 = blocks; j2 < 0; j2++) {
				byte idx = raster[(in_ptr++)];
				if (idx != 0)
					out[(out_ptr++)] = palette[(idx & 0xFF)];
				else
					out_ptr++;
				idx = raster[(in_ptr++)];
				if (idx != 0)
					out[(out_ptr++)] = palette[(idx & 0xFF)];
				else
					out_ptr++;
				idx = raster[(in_ptr++)];
				if (idx != 0)
					out[(out_ptr++)] = palette[(idx & 0xFF)];
				else
					out_ptr++;
				idx = raster[(in_ptr++)];
				if (idx != 0)
					out[(out_ptr++)] = palette[(idx & 0xFF)];
				else {
					out_ptr++;
				}
			}
			for (int k2 = w; k2 < 0; k2++) {
				byte idx = raster[(in_ptr++)];
				if (idx != 0)
					out[(out_ptr++)] = palette[(idx & 0xFF)];
				else {
					out_ptr++;
				}
			}
			out_ptr += out_ptr_step;
			in_ptr += in_ptr_step;
		}
	}

	private void resize_plot(int[] out, int[] in, int pix, int in_ptr,
			int in_ptr_step, int out_ptr, int out_ptr_step, int w, int h,
			int ratio_w, int ratio_h, int old_w, int step) {
		try {
			int l2 = in_ptr;
			for (int i3 = -h; i3 < 0; i3 += step) {
				int j3 = (in_ptr_step >> 16) * old_w;
				for (int k3 = -w; k3 < 0; k3++) {
					pix = in[((in_ptr >> 16) + j3)];
					if (pix != 0)
						out[(out_ptr++)] = pix;
					else
						out_ptr++;
					in_ptr += ratio_w;
				}

				in_ptr_step += ratio_h;
				in_ptr = l2;
				out_ptr += out_ptr_step;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in plot_scale");
		}
	}

	private void trans_plot(int[] out, int[] in, int pix, int in_ptr,
			int out_ptr, int w, int h, int out_ptr_step, int in_ptr_step,
			int step, int opac) {
		int trans = 256 - opac;
		for (int k2 = -h; k2 < 0; k2 += step) {
			for (int l2 = -w; l2 < 0; l2++) {
				pix = in[(in_ptr++)];
				if (pix != 0) {
					int _pix = out[out_ptr];
					out[(out_ptr++)] = (((pix & 0xFF00FF) * opac
							+ (_pix & 0xFF00FF) * trans & 0xFF00FF00)
							+ ((pix & 0xFF00) * opac + (_pix & 0xFF00) * trans & 0xFF0000) >> 8);
				} else {
					out_ptr++;
				}
			}

			out_ptr += out_ptr_step;
			in_ptr += in_ptr_step;
		}
	}

	private void trans_plot(int[] out, byte[] in, int[] palette, int in_ptr,
			int out_ptr, int w, int h, int out_ptr_step, int in_ptr_step,
			int step, int opac) {
		int trans = 256 - opac;
		for (int j2 = -h; j2 < 0; j2 += step) {
			for (int k2 = -w; k2 < 0; k2++) {
				int pix = in[(in_ptr++)];
				if (pix != 0) {
					pix = palette[(pix & 0xFF)];
					int i3 = out[out_ptr];
					out[(out_ptr++)] = (((pix & 0xFF00FF) * opac
							+ (i3 & 0xFF00FF) * trans & 0xFF00FF00)
							+ ((pix & 0xFF00) * opac + (i3 & 0xFF00) * trans & 0xFF0000) >> 8);
				} else {
					out_ptr++;
				}
			}

			out_ptr += out_ptr_step;
			in_ptr += in_ptr_step;
		}
	}

	private void resize_trans_plot(int[] out, int[] in, int i, int in_ptr,
			int in_ptr_step, int out_ptr, int out_ptr_step, int w, int h,
			int ratio_w, int ratio_h, int old_w, int step, int opac) {
		int trans = 256 - opac;
		try {
			int j3 = in_ptr;
			for (int k3 = -h; k3 < 0; k3 += step) {
				int l3 = (in_ptr_step >> 16) * old_w;
				for (int i4 = -w; i4 < 0; i4++) {
					i = in[((in_ptr >> 16) + l3)];
					if (i != 0) {
						int j4 = out[out_ptr];
						out[(out_ptr++)] = (((i & 0xFF00FF) * opac
								+ (j4 & 0xFF00FF) * trans & 0xFF00FF00)
								+ ((i & 0xFF00) * opac + (j4 & 0xFF00) * trans & 0xFF0000) >> 8);
					} else {
						out_ptr++;
					}
					in_ptr += ratio_w;
				}

				in_ptr_step += ratio_h;
				in_ptr = j3;
				out_ptr += out_ptr_step;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in tran_scale");
		}
	}

	private void resize_tint_plot(int[] out, int[] in, int i, int in_ptr,
			int in_ptr_step, int out_ptr, int out_ptr_step, int w, int h,
			int ratio_w, int ratio_h, int old_w, int step, int col) {
		int r = col >> 16 & 0xFF;
		int g = col >> 8 & 0xFF;
		int b = col & 0xFF;
		try {
			int l3 = in_ptr;
			for (int i4 = -h; i4 < 0; i4 += step) {
				int j4 = (in_ptr_step >> 16) * old_w;
				for (int k4 = -w; k4 < 0; k4++) {
					i = in[((in_ptr >> 16) + j4)];
					if (i != 0) {
						int _r = i >> 16 & 0xFF;
						int _g = i >> 8 & 0xFF;
						int _b = i & 0xFF;
						if ((_r == _g) && (_g == _b))
							out[(out_ptr++)] = ((_r * r >> 8 << 16)
									+ (_g * g >> 8 << 8) + (_b * b >> 8));
						else
							out[(out_ptr++)] = i;
					} else {
						out_ptr++;
					}
					in_ptr += ratio_w;
				}

				in_ptr_step += ratio_h;
				in_ptr = l3;
				out_ptr += out_ptr_step;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in plot_scale");
		}
	}

	public void rotate_sprite_plot(int x, int y, int off, int angle, int scale) {
		int w = this.w;
		int h = this.h;
		if (sin256_tbl == null) {
			sin256_tbl = new int[512];
			for (int i = 0; i < 256; i++) {
				sin256_tbl[i] = ((int) (Math.sin(i * 0.02454369D) * 32768.0D));
				sin256_tbl[(i + 256)] = ((int) (Math.cos(i * 0.02454369D) * 32768.0D));
			}
		}

		int bl_x = -sprite_mask_w[off] / 2;
		int bl_y = -sprite_mask_h[off] / 2;
		if (sprite_trans[off]) {
			bl_x += sprite_trans_x[off];
			bl_y += sprite_trans_y[off];
		}
		int tr_x = bl_x + sprite_width[off];
		int tr_y = bl_y + sprite_height[off];
		int br_x = tr_x;
		int br_y = bl_y;
		int tl_x = bl_x;
		int tl_y = tr_y;
		angle &= 255;
		int sin_angle = sin256_tbl[angle] * scale;
		int cos_angle = sin256_tbl[(angle + 256)] * scale;
		int bl_u = x + (bl_y * sin_angle + bl_x * cos_angle >> 22);
		int bl_v = y + (bl_y * cos_angle - bl_x * sin_angle >> 22);
		int tr_u = x + (br_y * sin_angle + br_x * cos_angle >> 22);
		int tr_v = y + (br_y * cos_angle - br_x * sin_angle >> 22);
		int br_u = x + (tr_y * sin_angle + tr_x * cos_angle >> 22);
		int br_v = y + (tr_y * cos_angle - tr_x * sin_angle >> 22);
		int tl_u = x + (tl_y * sin_angle + tl_x * cos_angle >> 22);
		int tl_v = y + (tl_y * cos_angle - tl_x * sin_angle >> 22);
		int min_v = bl_v;
		int max_v = bl_v;
		if (tr_v < min_v)
			min_v = tr_v;
		else if (tr_v > max_v)
			max_v = tr_v;
		if (br_v < min_v)
			min_v = br_v;
		else if (br_v > max_v)
			max_v = br_v;
		if (tl_v < min_v)
			min_v = tl_v;
		else if (tl_v > max_v)
			max_v = tl_v;
		if (min_v < origin_y)
			min_v = origin_y;
		if (max_v > top_y)
			max_v = top_y;
		if ((sprite_start_x == null)
				|| (sprite_start_x.length != h + 1)) {
			sprite_start_x = new int[h + 1];
			sprite_end_x = new int[h + 1];
			sprite_start_u = new int[h + 1];
			sprite_end_u = new int[h + 1];
			sprite_start_v = new int[h + 1];
			sprite_end_v = new int[h + 1];
		}
		for (int i7 = min_v; i7 <= max_v; i7++) {
			sprite_start_x[i7] = 99999999;
			sprite_end_x[i7] = -99999999;
		}

		int _dx = 0;
		int _du = 0;
		int _dv = 0;
		int sprite_w = sprite_width[off];
		int sprite_h = sprite_height[off];
		bl_x = 0;
		bl_y = 0;
		br_x = sprite_w - 1;
		br_y = 0;
		tr_x = sprite_w - 1;
		tr_y = sprite_h - 1;
		tl_x = 0;
		tl_y = sprite_h - 1;
		if (tl_v != bl_v) {
			_dx = (tl_u - bl_u << 8) / (tl_v - bl_v);
			_dv = (tl_y - bl_y << 8) / (tl_v - bl_v);
		}
		int _x;
		int _v;
		int start_v;
		int end_v;
		if (bl_v > tl_v) {
			_x = tl_u << 8;
			_v = tl_y << 8;
			start_v = tl_v;
			end_v = bl_v;
		} else {
			_x = bl_u << 8;
			_v = bl_y << 8;
			start_v = bl_v;
			end_v = tl_v;
		}
		if (start_v < 0) {
			_x -= _dx * start_v;
			_v -= _dv * start_v;
			start_v = 0;
		}
		if (end_v > h - 1)
			end_v = h - 1;
		for (int l9 = start_v; l9 <= end_v; l9++) {
			int tmp813_811 = _x;
			sprite_end_x[l9] = tmp813_811;
			sprite_start_x[l9] = tmp813_811;
			_x += _dx;
			int tmp836_835 = 0;
			sprite_end_u[l9] = tmp836_835;
			sprite_start_u[l9] = tmp836_835;
			int tmp853_851 = _v;
			sprite_end_v[l9] = tmp853_851;
			sprite_start_v[l9] = tmp853_851;
			_v += _dv;
		}

		if (tr_v != bl_v) {
			_dx = (tr_u - bl_u << 8) / (tr_v - bl_v);
			_du = (br_x - bl_x << 8) / (tr_v - bl_v);
		}
		int _u;
		if (bl_v > tr_v) {
			_x = tr_u << 8;
			_u = br_x << 8;
			start_v = tr_v;
			end_v = bl_v;
		} else {
			_x = bl_u << 8;
			_u = bl_x << 8;
			start_v = bl_v;
			end_v = tr_v;
		}
		if (start_v < 0) {
			_x -= _dx * start_v;
			_u -= _du * start_v;
			start_v = 0;
		}
		if (end_v > h - 1)
			end_v = h - 1;
		for (int ptr = start_v; ptr <= end_v; ptr++) {
			if (_x < sprite_start_x[ptr]) {
				sprite_start_x[ptr] = _x;
				sprite_start_u[ptr] = _u;
				sprite_start_v[ptr] = 0;
			}
			if (_x > sprite_end_x[ptr]) {
				sprite_end_x[ptr] = _x;
				sprite_end_u[ptr] = _u;
				sprite_end_v[ptr] = 0;
			}
			_x += _dx;
			_u += _du;
		}

		if (br_v != tr_v) {
			_dx = (br_u - tr_u << 8) / (br_v - tr_v);
			_dv = (tr_y - br_y << 8) / (br_v - tr_v);
		}
		if (tr_v > br_v) {
			_x = br_u << 8;
			_u = tr_x << 8;
			_v = tr_y << 8;
			start_v = br_v;
			end_v = tr_v;
		} else {
			_x = tr_u << 8;
			_u = br_x << 8;
			_v = br_y << 8;
			start_v = tr_v;
			end_v = br_v;
		}
		if (start_v < 0) {
			_x -= _dx * start_v;
			_v -= _dv * start_v;
			start_v = 0;
		}
		if (end_v > h - 1)
			end_v = h - 1;
		for (int j10 = start_v; j10 <= end_v; j10++) {
			if (_x < sprite_start_x[j10]) {
				sprite_start_x[j10] = _x;
				sprite_start_u[j10] = _u;
				sprite_start_v[j10] = _v;
			}
			if (_x > sprite_end_x[j10]) {
				sprite_end_x[j10] = _x;
				sprite_end_u[j10] = _u;
				sprite_end_v[j10] = _v;
			}
			_x += _dx;
			_v += _dv;
		}

		if (tl_v != br_v) {
			_dx = (tl_u - br_u << 8) / (tl_v - br_v);
			_du = (tl_x - tr_x << 8) / (tl_v - br_v);
		}
		if (br_v > tl_v) {
			_x = tl_u << 8;
			_u = tl_x << 8;
			_v = tl_y << 8;
			start_v = tl_v;
			end_v = br_v;
		} else {
			_x = br_u << 8;
			_u = tr_x << 8;
			_v = tr_y << 8;
			start_v = br_v;
			end_v = tl_v;
		}
		if (start_v < 0) {
			_x -= _dx * start_v;
			_u -= _du * start_v;
			start_v = 0;
		}
		if (end_v > h - 1)
			end_v = h - 1;
		for (int k10 = start_v; k10 <= end_v; k10++) {
			if (_x < sprite_start_x[k10]) {
				sprite_start_x[k10] = _x;
				sprite_start_u[k10] = _u;
				sprite_start_v[k10] = _v;
			}
			if (_x > sprite_end_x[k10]) {
				sprite_end_x[k10] = _x;
				sprite_end_u[k10] = _u;
				sprite_end_v[k10] = _v;
			}
			_x += _dx;
			_u += _du;
		}

		int ptr = min_v * w;
		int[] sprite_pix = sprite_pixels[off];
		for (int _y = min_v; _y < max_v; _y++) {
			int off_x = sprite_start_x[_y] >> 8;
			int end_x = sprite_end_x[_y] >> 8;
			if (end_x - off_x <= 0) {
				ptr += w;
			} else {
				int u = sprite_start_u[_y] << 9;
				int du = ((sprite_end_u[_y] << 9) - u) / (end_x - off_x);
				int v = sprite_start_v[_y] << 9;
				int dv = ((sprite_end_v[_y] << 9) - v) / (end_x - off_x);
				if (off_x < origin_x) {
					u += (origin_x - off_x) * du;
					v += (origin_x - off_x) * dv;
					off_x = origin_x;
				}
				if (end_x > top_x)
					end_x = top_x;
				if ((!skip_lines) || ((_y & 0x1) == 0))
					if (!sprite_trans[off])
						if (sprite_pix != null)
							row_plot(pixels, sprite_pix, 0, ptr + off_x, u, v,
									du, dv, off_x - end_x, sprite_w);
						else
							row_index_plot(pixels, sprite_raster[off],
									sprite_palette[off], 0, ptr + off_x, u, v,
									du, dv, off_x - end_x, sprite_w);
					else if (sprite_pix != null)
						trans_row_plot(pixels, sprite_pix, 0, ptr + off_x, u,
								v, du, dv, off_x - end_x, sprite_w);
					else
						trans_row_index_plot(pixels, sprite_raster[off],
								sprite_palette[off], 0, ptr + off_x, u, v, du,
								dv, off_x - end_x, sprite_w);
				ptr += w;
			}
		}
	}

	private void row_plot(int[] out, int[] in, int i, int out_ptr, int u,
			int v, int du, int dv, int len, int in_w) {
		for (i = len; i < 0; i++) {
			pixels[(out_ptr++)] = in[((u >> 17) + (v >> 17) * in_w)];
			u += du;
			v += dv;
		}
	}

	private void row_index_plot(int[] out, byte[] raster, int[] palette, int i,
			int out_ptr, int u, int v, int du, int dv, int len, int in_w) {
		for (i = len; i < 0; i++) {
			byte idx = raster[((u >> 17) + (v >> 17) * in_w)];
			if (idx != 0)
				pixels[(out_ptr++)] = palette[idx];
			u += du;
			v += dv;
		}
	}

	private void trans_row_plot(int[] out, int[] in, int i, int out_ptr, int u,
			int v, int du, int dv, int len, int in_w) {
		for (int i2 = len; i2 < 0; i2++) {
			i = in[((u >> 17) + (v >> 17) * in_w)];
			if (i != 0)
				pixels[(out_ptr++)] = i;
			else
				out_ptr++;
			u += du;
			v += dv;
		}
	}

	private void trans_row_index_plot(int[] out, byte[] raster, int[] palette,
			int i, int out_ptr, int u, int v, int du, int dv, int len, int in_w) {
		for (int i2 = len; i2 < 0; i2++) {
			byte idx = raster[((u >> 17) + (v >> 17) * in_w)];
			if (idx != 0)
				i = palette[idx];
			if (i != 0)
				pixels[(out_ptr++)] = i;
			else
				out_ptr++;
			u += du;
			v += dv;
		}
	}

	public void sprite_3d_plot(int i, int j, int k, int l, int i1, int j1,
			int k1) {
		resize_sprite_plot(i, j, k, l, i1);
	}

	public void resize_trans_sprite_plot(int x, int y, int new_w, int new_h,
			int off, int grey_tint, int pink_tint, int l1, boolean flag) {
		try {
			if (grey_tint == 0)
				grey_tint = 16777215;
			if (pink_tint == 0)
				pink_tint = 16777215;
			int w = sprite_width[off];
			int h = sprite_height[off];
			int k2 = 0;
			int l2 = 0;
			int out_ptr = l1 << 16;
			int ratio_w = (w << 16) / new_w;
			int ratio_h = (h << 16) / new_h;
			int l3 = -(l1 << 16) / new_h;
			if (sprite_trans[off]) {
				int mask_w = sprite_mask_w[off];
				int mask_h = sprite_mask_h[off];
				ratio_w = (mask_w << 16) / new_w;
				ratio_h = (mask_h << 16) / new_h;
				int trans_x = sprite_trans_x[off];
				int trans_y = sprite_trans_y[off];
				if (flag)
					trans_x = mask_w - sprite_width[off] - trans_x;
				x += (trans_x * new_w + mask_w - 1) / mask_w;
				int dy = (trans_y * new_h + mask_h - 1) / mask_h;
				y += dy;
				out_ptr += dy * l3;
				if (trans_x * new_w % mask_w != 0)
					k2 = (mask_w - trans_x * new_w % mask_w << 16) / new_w;
				if (trans_y * new_h % mask_h != 0)
					l2 = (mask_h - trans_y * new_h % mask_h << 16) / new_h;
				new_w = ((sprite_width[off] << 16) - k2 + ratio_w - 1)
						/ ratio_w;
				new_h = ((sprite_height[off] << 16) - l2 + ratio_h - 1)
						/ ratio_h;
			}
			int j4 = y * w;
			out_ptr += (x << 16);
			if (y < origin_y) {
				int l4 = origin_y - y;
				new_h -= l4;
				y = origin_y;
				j4 += l4 * w;
				l2 += ratio_h * l4;
				out_ptr += l3 * l4;
			}
			if (y + new_h >= top_y)
				new_h -= y + new_h - top_y + 1;
			int i5 = j4 / w & 0x1;
			if (!skip_lines)
				i5 = 2;
			if (pink_tint == 16777215) {
				if (sprite_pixels[off] != null) {
					if (!flag) {
						resize_trans_sprite_plot(pixels,
								sprite_pixels[off], 0, k2, l2, j4, new_w,
								new_h, ratio_w, ratio_h, w, grey_tint, out_ptr,
								l3, i5);
						return;
					}
					resize_trans_sprite_plot(pixels,
							sprite_pixels[off], 0,
							(sprite_width[off] << 16) - k2 - 1, l2, j4,
							new_w, new_h, -ratio_w, ratio_h, w, grey_tint,
							out_ptr, l3, i5);
					return;
				}
				if (!flag) {
					resize_trans_sprite_plot(pixels,
							sprite_raster[off], sprite_palette[off],
							0, k2, l2, j4, new_w, new_h, ratio_w, ratio_h, w,
							grey_tint, out_ptr, l3, i5);
					return;
				}
				resize_trans_sprite_plot(pixels, sprite_raster[off],
						sprite_palette[off], 0,
						(sprite_width[off] << 16) - k2 - 1, l2, j4, new_w,
						new_h, -ratio_w, ratio_h, w, grey_tint, out_ptr, l3, i5);
				return;
			}

			if (sprite_pixels[off] != null) {
				if (!flag) {
					resize_trans_sprite_plot(pixels,
							sprite_pixels[off], 0, k2, l2, j4, new_w,
							new_h, ratio_w, ratio_h, w, grey_tint, pink_tint,
							out_ptr, l3, i5);
					return;
				}
				resize_trans_sprite_plot(pixels, sprite_pixels[off],
						0, (sprite_width[off] << 16) - k2 - 1, l2, j4,
						new_w, new_h, -ratio_w, ratio_h, w, grey_tint,
						pink_tint, out_ptr, l3, i5);
				return;
			}
			if (!flag) {
				resize_trans_sprite_plot(pixels, sprite_raster[off],
						sprite_palette[off], 0, k2, l2, j4, new_w, new_h,
						ratio_w, ratio_h, w, grey_tint, pink_tint, out_ptr, l3,
						i5);
				return;
			}
			resize_trans_sprite_plot(pixels, sprite_raster[off],
					sprite_palette[off], 0, (sprite_width[off] << 16)
							- k2 - 1, l2, j4, new_w, new_h, -ratio_w, ratio_h,
					w, grey_tint, pink_tint, out_ptr, l3, i5);
			return;
		} catch (Exception _ex) {
			System.out.println("error in sprite clipping routine");
		}
	}

	private void resize_trans_sprite_plot(int[] ai, int[] ai1, int i, int j,
			int k, int l, int i1, int j1, int k1, int l1, int i2, int j2,
			int k2, int l2, int i3) {
		int i4 = j2 >> 16 & 0xFF;
		int j4 = j2 >> 8 & 0xFF;
		int k4 = j2 & 0xFF;
		try {
			int l4 = j;
			for (int i5 = -j1; i5 < 0; i5++) {
				int j5 = (k >> 16) * i2;
				int k5 = k2 >> 16;
				int l5 = i1;
				if (k5 < origin_x) {
					int i6 = origin_x - k5;
					l5 -= i6;
					k5 = origin_x;
					j += k1 * i6;
				}
				if (k5 + l5 >= top_x) {
					int j6 = k5 + l5 - top_x;
					l5 -= j6;
				}
				i3 = 1 - i3;
				if (i3 != 0) {
					for (int k6 = k5; k6 < k5 + l5; k6++) {
						i = ai1[((j >> 16) + j5)];
						if (i != 0) {
							int j3 = i >> 16 & 0xFF;
							int k3 = i >> 8 & 0xFF;
							int l3 = i & 0xFF;
							if ((j3 == k3) && (k3 == l3))
								ai[(k6 + l)] = ((j3 * i4 >> 8 << 16)
										+ (k3 * j4 >> 8 << 8) + (l3 * k4 >> 8));
							else
								ai[(k6 + l)] = i;
						}
						j += k1;
					}
				}

				k += l1;
				j = l4;
				l += w;
				k2 += l2;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	private void resize_trans_sprite_plot(int[] ai, int[] ai1, int i, int j,
			int k, int l, int i1, int j1, int k1, int l1, int i2, int j2,
			int k2, int l2, int i3, int j3) {
		int j4 = j2 >> 16 & 0xFF;
		int k4 = j2 >> 8 & 0xFF;
		int l4 = j2 & 0xFF;
		int i5 = k2 >> 16 & 0xFF;
		int j5 = k2 >> 8 & 0xFF;
		int k5 = k2 & 0xFF;
		try {
			int l5 = j;
			for (int i6 = -j1; i6 < 0; i6++) {
				int j6 = (k >> 16) * i2;
				int k6 = l2 >> 16;
				int l6 = i1;
				if (k6 < origin_x) {
					int i7 = origin_x - k6;
					l6 -= i7;
					k6 = origin_x;
					j += k1 * i7;
				}
				if (k6 + l6 >= top_x) {
					int j7 = k6 + l6 - top_x;
					l6 -= j7;
				}
				j3 = 1 - j3;
				if (j3 != 0) {
					for (int k7 = k6; k7 < k6 + l6; k7++) {
						i = ai1[((j >> 16) + j6)];
						if (i != 0) {
							int k3 = i >> 16 & 0xFF;
							int l3 = i >> 8 & 0xFF;
							int i4 = i & 0xFF;
							if ((k3 == l3) && (l3 == i4))
								ai[(k7 + l)] = ((k3 * j4 >> 8 << 16)
										+ (l3 * k4 >> 8 << 8) + (i4 * l4 >> 8));
							else if ((k3 == 255) && (l3 == i4))
								ai[(k7 + l)] = ((k3 * i5 >> 8 << 16)
										+ (l3 * j5 >> 8 << 8) + (i4 * k5 >> 8));
							else
								ai[(k7 + l)] = i;
						}
						j += k1;
					}
				}

				k += l1;
				j = l5;
				l += w;
				l2 += i3;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	private void resize_trans_sprite_plot(int[] ai, byte[] abyte0, int[] ai1,
			int i, int j, int k, int l, int i1, int j1, int k1, int l1, int i2,
			int j2, int k2, int l2, int i3) {
		int i4 = j2 >> 16 & 0xFF;
		int j4 = j2 >> 8 & 0xFF;
		int k4 = j2 & 0xFF;
		try {
			int l4 = j;
			for (int i5 = -j1; i5 < 0; i5++) {
				int j5 = (k >> 16) * i2;
				int k5 = k2 >> 16;
				int l5 = i1;
				if (k5 < origin_x) {
					int i6 = origin_x - k5;
					l5 -= i6;
					k5 = origin_x;
					j += k1 * i6;
				}
				if (k5 + l5 >= top_x) {
					int j6 = k5 + l5 - top_x;
					l5 -= j6;
				}
				i3 = 1 - i3;
				if (i3 != 0) {
					for (int k6 = k5; k6 < k5 + l5; k6++) {
						i = abyte0[((j >> 16) + j5)] & 0xFF;
						if (i != 0) {
							i = ai1[i];
							int j3 = i >> 16 & 0xFF;
							int k3 = i >> 8 & 0xFF;
							int l3 = i & 0xFF;
							if ((j3 == k3) && (k3 == l3))
								ai[(k6 + l)] = ((j3 * i4 >> 8 << 16)
										+ (k3 * j4 >> 8 << 8) + (l3 * k4 >> 8));
							else
								ai[(k6 + l)] = i;
						}
						j += k1;
					}
				}

				k += l1;
				j = l4;
				l += w;
				k2 += l2;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	private void resize_trans_sprite_plot(int[] ai, byte[] abyte0, int[] ai1,
			int i, int j, int k, int l, int i1, int j1, int k1, int l1, int i2,
			int j2, int k2, int l2, int i3, int j3) {
		int j4 = j2 >> 16 & 0xFF;
		int k4 = j2 >> 8 & 0xFF;
		int l4 = j2 & 0xFF;
		int i5 = k2 >> 16 & 0xFF;
		int j5 = k2 >> 8 & 0xFF;
		int k5 = k2 & 0xFF;
		try {
			int l5 = j;
			for (int i6 = -j1; i6 < 0; i6++) {
				int j6 = (k >> 16) * i2;
				int k6 = l2 >> 16;
				int l6 = i1;
				if (k6 < origin_x) {
					int i7 = origin_x - k6;
					l6 -= i7;
					k6 = origin_x;
					j += k1 * i7;
				}
				if (k6 + l6 >= top_x) {
					int j7 = k6 + l6 - top_x;
					l6 -= j7;
				}
				j3 = 1 - j3;
				if (j3 != 0) {
					for (int k7 = k6; k7 < k6 + l6; k7++) {
						i = abyte0[((j >> 16) + j6)] & 0xFF;
						if (i != 0) {
							i = ai1[i];
							int k3 = i >> 16 & 0xFF;
							int l3 = i >> 8 & 0xFF;
							int i4 = i & 0xFF;
							if ((k3 == l3) && (l3 == i4))
								ai[(k7 + l)] = ((k3 * j4 >> 8 << 16)
										+ (l3 * k4 >> 8 << 8) + (i4 * l4 >> 8));
							else if ((k3 == 255) && (l3 == i4))
								ai[(k7 + l)] = ((k3 * i5 >> 8 << 16)
										+ (l3 * j5 >> 8 << 8) + (i4 * k5 >> 8));
							else
								ai[(k7 + l)] = i;
						}
						j += k1;
					}
				}

				k += l1;
				j = l5;
				l += w;
				l2 += i3;
			}

			return;
		} catch (Exception _ex) {
			System.out.println("error in transparent sprite plot routine");
		}
	}

	public static int font_define(byte[] abyte0) {
		fonts[font_cnt] = abyte0;
		return font_cnt++;
	}

	public void precede_text_draw(String s, int i, int j, int k, int l) {
		text_draw(s, i - text_width(s, k), j, k, l);
	}

	public void center_text_draw(String s, int i, int j, int k, int l) {
		text_draw(s, i - text_width(s, k) / 2, j, k, l);
	}

	public void multiline_text_draw(String s, int i, int j, int k, int l, int i1) {
		try {
			int j1 = 0;
			byte[] glyphs = fonts[k];
			int k1 = 0;
			int l1 = 0;
			for (int i2 = 0; i2 < s.length(); i2++) {
				if ((s.charAt(i2) == '@') && (i2 + 4 < s.length())
						&& (s.charAt(i2 + 4) == '@'))
					i2 += 4;
				else if ((s.charAt(i2) == '~') && (i2 + 4 < s.length())
						&& (s.charAt(i2 + 4) == '~'))
					i2 += 4;
				else
					j1 += glyphs[(gylph_idx[s.charAt(i2)] + 7)];
				if (s.charAt(i2) == ' ')
					l1 = i2;
				if (j1 > i1) {
					if (l1 <= k1)
						l1 = i2;
					center_text_draw(s.substring(k1, l1), i, j, k, l);
					j1 = 0;
					k1 = i2 = l1 + 1;
					j += height(k);
				}
			}

			if (j1 > 0) {
				center_text_draw(s.substring(k1), i, j, k, l);
				return;
			}
		} catch (Exception exception) {
			System.out.println("centrepara: " + exception);
			exception.printStackTrace();
		}
	}

	public void text_draw(String s, int x, int y, int font_idx, int col) {
		try {
			byte[] glyphs = fonts[font_idx];
			for (int i1 = 0; i1 < s.length(); i1++) {
				if ((s.charAt(i1) == '@') && (i1 + 4 < s.length())
						&& (s.charAt(i1 + 4) == '@')) {
					if (s.substring(i1 + 1, i1 + 4).equalsIgnoreCase("red"))
						col = 16711680;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("lre"))
						col = 16748608;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("yel"))
						col = 16776960;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("gre"))
						col = 65280;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("blu"))
						col = 255;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("cya"))
						col = 65535;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("mag"))
						col = 16711935;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("whi"))
						col = 16777215;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("bla"))
						col = 0;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("dre"))
						col = 12582912;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("ora"))
						col = 16748608;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("ran"))
						col = (int) (Math.random() * 16777215.0D);
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("or1"))
						col = 16756736;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("or2"))
						col = 16740352;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("or3"))
						col = 16723968;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("gr1"))
						col = 12648192;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("gr2"))
						col = 8453888;
					else if (s.substring(i1 + 1, i1 + 4)
							.equalsIgnoreCase("gr3"))
						col = 4259584;
					i1 += 4;
				} else if ((s.charAt(i1) == '~') && (i1 + 4 < s.length())
						&& (s.charAt(i1 + 4) == '~')) {
					char c = s.charAt(i1 + 1);
					char c1 = s.charAt(i1 + 2);
					char c2 = s.charAt(i1 + 3);
					if ((c >= '0') && (c <= '9') && (c1 >= '0') && (c1 <= '9')
							&& (c2 >= '0') && (c2 <= '9'))
						x = Integer.parseInt(s.substring(i1 + 1, i1 + 4));
					i1 += 4;
				} else {
					int idx = gylph_idx[s.charAt(i1)];
					if ((logged_in) && (col != 0))
						char_plot(idx, x + 1, y, 0, glyphs);
					if ((logged_in) && (col != 0))
						char_plot(idx, x, y + 1, 0, glyphs);
					char_plot(idx, x, y, col, glyphs);
					x += glyphs[(idx + 7)];
				}
			}
			return;
		} catch (Exception exception) {
			System.out.println("drawstring: " + exception);
			exception.printStackTrace();
		}
	}

	private void char_plot(int i, int j, int k, int l, byte[] glyph) {
		int start_x = j + glyph[(i + 5)];
		int start_y = k - glyph[(i + 6)];
		int width = glyph[(i + 3)];
		int height = glyph[(i + 4)];
		int in_ptr = glyph[i] * 16384 + glyph[(i + 1)] * 128 + glyph[(i + 2)];
		int out_ptr = start_x + start_y * w;
		int out_step = w - width;
		int in_step = 0;
		if (start_y < origin_y) {
			int i3 = origin_y - start_y;
			height -= i3;
			start_y = origin_y;
			in_ptr += i3 * width;
			out_ptr += i3 * w;
		}
		if (start_y + height >= top_y)
			height -= start_y + height - top_y + 1;
		if (start_x < origin_x) {
			int j3 = origin_x - start_x;
			width -= j3;
			start_x = origin_x;
			in_ptr += j3;
			out_ptr += j3;
			in_step += j3;
			out_step += j3;
		}
		if (start_x + width >= top_x) {
			int k3 = start_x + width - top_x + 1;
			width -= k3;
			in_step += k3;
			out_step += k3;
		}
		if ((width > 0) && (height > 0))
			char_plot(pixels, glyph, l, in_ptr, out_ptr, width, height,
					out_step, in_step);
	}

	private void char_plot(int[] out, byte[] glyph, int col, int j, int k,
			int w, int h, int j1, int k1) {
		try {
			int blocks = -(w >> 2);
			w = -(w & 0x3);
			for (int i2 = -h; i2 < 0; i2++) {
				for (int j2 = blocks; j2 < 0; j2++) {
					if (glyph[(j++)] != 0)
						out[(k++)] = col;
					else
						k++;
					if (glyph[(j++)] != 0)
						out[(k++)] = col;
					else
						k++;
					if (glyph[(j++)] != 0)
						out[(k++)] = col;
					else
						k++;
					if (glyph[(j++)] != 0)
						out[(k++)] = col;
					else {
						k++;
					}
				}
				for (int k2 = w; k2 < 0; k2++) {
					if (glyph[(j++)] != 0)
						out[(k++)] = col;
					else
						k++;
				}
				k += j1;
				j += k1;
			}

			return;
		} catch (Exception exception) {
			System.out.println("plotletter: " + exception);
			exception.printStackTrace();
		}
	}

	public static int height(int i) {
		if (i == 0) {
			return fonts[i][8] - 2;
		}
		return fonts[i][8] - 1;
	}

	public static int text_width(String s, int i) {
		int j = 0;
		byte[] glyphs = fonts[i];
		for (int k = 0; k < s.length(); k++) {
			if ((s.charAt(k) == '@') && (k + 4 < s.length())
					&& (s.charAt(k + 4) == '@'))
				k += 4;
			else if ((s.charAt(k) == '~') && (k + 4 < s.length())
					&& (s.charAt(k + 4) == '~'))
				k += 4;
			else
				j += glyphs[(gylph_idx[s.charAt(k)] + 7)];
		}
		return j;
	}

	public boolean imageUpdate(Image image, int i, int j, int k, int l, int i1) {
		return true;
	}

	static {
		String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
		gylph_idx = new int[256];
		for (int i = 0; i < 256; i++) {
			int j = s.indexOf(i);
			if (j == -1)
				j = 74;
			gylph_idx[i] = (j * 9);
		}
	}
}