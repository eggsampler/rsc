package jagex.client;

import jagex.Bzip2;
import jagex.DataUtil;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Component;
import java.awt.Event;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.IndexColorModel;
import java.awt.image.MemoryImageSource;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

@SuppressWarnings("serial")
public abstract class Game extends Applet implements Runnable {
	protected int w = 512;
	protected int h = 384;
	private Thread tick_thread;
	protected int period;
	protected int max_updates;
	protected int fps = 0;
	public long[] ticks;
	static Window wnd = null;
	private boolean is_applet;
	private int kill_state;
	private int lag_accum;
	public int mouse_off_y;
	public int idle_cntr;
	public int loading_stage;
	public String additional_text;
	private boolean on_jagex;
	private int bar_perc;
	private String bar_text;
	private Font timesroman;
	private Font bold_helvetica;
	private Font helvetica;
	private Image logo;
	private Graphics graphics;
	public boolean aBoolean23;
	public boolean aBoolean24;
	public boolean aBoolean25;
	public boolean aBoolean26;
	public boolean aBoolean27;
	public boolean aBoolean28;
	public boolean aBoolean29;
	public boolean aBoolean30;
	public int min_delta;
	public int mouse_x;
	public int mouse_y;
	public int mouse_state;
	public int click_state;
	public int pressed_key;
	public int typed_key;
	public boolean reduce_lag;
	public String line_buf_a = "";
	public String line_a = "";
	public String line_buf_b = "";
	public String line_b = "";

	public Game() {
		period = 20;
		max_updates = 1000;
		ticks = new long[10];
		loading_stage = 1;
		bar_text = "Loading";
		timesroman = new Font("TimesRoman", 0, 15);
		bold_helvetica = new Font("Helvetica", 1, 13);
		helvetica = new Font("Helvetica", 0, 12);

		min_delta = 1;
	}

	public abstract void load();

	public abstract void update();

	public abstract void cleanup();

	public abstract void display();

	public abstract void preload_draw();

	public final void start(int w, int h, String title, boolean flag) {
		is_applet = false;
		System.out.println("Started application");
		this.w = w;
		this.h = h;
		wnd = new Window(this, w, h, title, flag, false);
		loading_stage = 1;
		tick_thread = new Thread(this);
		tick_thread.start();
		tick_thread.setPriority(1);
	}

	public final boolean is_applet() {
		return is_applet;
	}

	public final Graphics getGraphics() {
		if (wnd == null) {
			return super.getGraphics();
		}
		return wnd.getGraphics();
	}

	public final int width() {
		return w;
	}

	public final int height() {
		return h;
	}

	public final Image createImage(int i, int j) {
		if (wnd == null) {
			return super.createImage(i, j);
		}
		return wnd.createImage(i, j);
	}

	public Frame window() {
		return wnd;
	}

	public final void set_rate(int i) {
		period = (1000 / i);
	}

	public final void set_max_updates(int i) {
		max_updates = i;
	}

	public final void reset_ticks() {
		for (int i = 0; i < 10; i++)
			ticks[i] = 0L;
	}

	public synchronized boolean keyDown(Event event, int code) {
		on_key(code);
		pressed_key = code;
		typed_key = code;
		idle_cntr = 0;
		if (code == 1006)
			aBoolean25 = true;
		if (code == 1007)
			aBoolean26 = true;
		if (code == 1004)
			aBoolean27 = true;
		if (code == 1005)
			aBoolean28 = true;
		if ((char) code == ' ')
			aBoolean29 = true;
		if (((char) code == 'n') || ((char) code == 'm'))
			aBoolean30 = true;
		if (((char) code == 'N') || ((char) code == 'M'))
			aBoolean30 = true;
		if ((char) code == '{')
			aBoolean23 = true;
		if ((char) code == '}')
			aBoolean24 = true;
		if ((char) code == '\u03F0')
			reduce_lag = (!reduce_lag);
		if (((code >= 97) && (code <= 122)) || ((code >= 65) && (code <= 90))
				|| ((code >= 48) && (code <= 57))
				|| ((code == 32) && (line_buf_a.length() < 20)))
			line_buf_a += (char) code;
		if ((code >= 32) && (code <= 122) && (line_buf_b.length() < 80))
			line_buf_b += (char) code;
		if ((code == 8) && (line_buf_a.length() > 0))
			line_buf_a = line_buf_a.substring(0,
					line_buf_a.length() - 1);
		if ((code == 8) && (line_buf_b.length() > 0))
			line_buf_b = line_buf_b.substring(0,
					line_buf_b.length() - 1);
		if ((code == 10) || (code == 13)) {
			line_a = line_buf_a;
			line_b = line_buf_b;
		}
		return true;
	}

	public void on_key(int i) {
	}

	public synchronized boolean keyUp(Event event, int code) {
		pressed_key = 0;
		if (code == 1006)
			aBoolean25 = false;
		if (code == 1007)
			aBoolean26 = false;
		if (code == 1004)
			aBoolean27 = false;
		if (code == 1005)
			aBoolean28 = false;
		if ((char) code == ' ')
			aBoolean29 = false;
		if (((char) code == 'n') || ((char) code == 'm'))
			aBoolean30 = false;
		if (((char) code == 'N') || ((char) code == 'M'))
			aBoolean30 = false;
		if ((char) code == '{')
			aBoolean23 = false;
		if ((char) code == '}')
			aBoolean24 = false;
		return true;
	}

	public synchronized boolean mouseMove(Event event, int x, int y) {
		mouse_x = x;
		mouse_y = (y + mouse_off_y);
		mouse_state = 0;
		idle_cntr = 0;
		return true;
	}

	public synchronized boolean mouseUp(Event event, int x, int j) {
		mouse_x = x;
		mouse_y = (j + mouse_off_y);
		mouse_state = 0;
		return true;
	}

	public synchronized boolean mouseDown(Event event, int x, int y) {
		mouse_x = x;
		mouse_y = (y + mouse_off_y);
		if (event.metaDown())
			mouse_state = 2;
		else
			mouse_state = 1;
		click_state = mouse_state;
		idle_cntr = 0;
		method15(mouse_state, x, y);
		return true;
	}

	public void method15(int i, int j, int k) {
	}

	public synchronized boolean mouseDrag(Event event, int i, int j) {
		mouse_x = i;
		mouse_y = (j + mouse_off_y);
		if (event.metaDown())
			mouse_state = 2;
		else
			mouse_state = 1;
		return true;
	}

	public final void init() {
		is_applet = true;
		System.out.println("Started applet");
		w = getWidth();
		h = getHeight();
		loading_stage = 1;
		DataUtil.codebase = getCodeBase();
		tick_thread = new Thread(this);
		tick_thread.start();
	}

	public final void start() {
		if (kill_state >= 0)
			kill_state = 0;
	}

	public final void stop() {
		if (kill_state >= 0)
			kill_state = (4000 / period);
	}

	@SuppressWarnings("deprecation")
	public final void destroy() {
		kill_state = -1;
		try {
			Thread.sleep(5000L);
		} catch (Exception _ex) {
		}
		if (kill_state == -1) {
			System.out.println("5 seconds expired, forcing kill");
			close();
			if (tick_thread != null) {
				tick_thread.stop();
				tick_thread = null;
			}
		}
	}

	public final void close() {
		kill_state = -2;
		System.out.println("Closing program");
		cleanup();
		try {
			Thread.sleep(1000L);
		} catch (Exception _ex) {
		}
		if (wnd != null)
			wnd.dispose();
		if (!is_applet)
			System.exit(0);
	}

	public final void run() {
		if (loading_stage == 1) {
			loading_stage = 2;
			graphics = getGraphics();
			load_resources();
			set_game_loading_bar(0, "Loading...");
			load();
			loading_stage = 0;
		}
		int ptr = 0;
		int step = 256;
		int delta = 1;
		int elapsed = 0;
		for (int j1 = 0; j1 < 10; j1++) {
			ticks[j1] = System.currentTimeMillis();
		}
		while (kill_state >= 0) {
			if (kill_state > 0) {
				kill_state -= 1;
				if (kill_state == 0) {
					close();
					tick_thread = null;
					return;
				}
			}
			int prev_step = step;
			int prev_delta = delta;
			step = 300;
			delta = 1;
			long tick = System.currentTimeMillis();
			if (ticks[ptr] == 0L) {
				step = prev_step;
				delta = prev_delta;
			} else if (tick > ticks[ptr]) {
				step = (int) (2560 * period / (tick - ticks[ptr]));
			}
			if (step < 25)
				step = 25;
			if (step > 256) {
				step = 256;
				delta = (int) (period - (tick - ticks[ptr]) / 10L);
				if (delta < min_delta)
					delta = min_delta;
			}
			try {
				Thread.sleep(delta);
			} catch (InterruptedException _ex) {
			}
			ticks[ptr] = tick;
			ptr = (ptr + 1) % 10;
			if (delta > 1) {
				for (int j2 = 0; j2 < 10; j2++) {
					if (ticks[j2] != 0L)
						ticks[j2] += delta;
				}
			}
			int updates = 0;
			while (elapsed < 256) {
				update();
				elapsed += step;
				updates++;
				if (updates > max_updates) {
					elapsed = 0;
					lag_accum += 6;
					if (lag_accum > 25) {
						lag_accum = 0;
						reduce_lag = true;
					}
				}
			}

			lag_accum -= 1;
			elapsed &= 255;
			
			if (period > 0) {
				fps = (1000 * step) / (period * 256);
			}

			display();
		}
		if (kill_state == -1)
			close();
		tick_thread = null;
	}

	public final void update(Graphics g) {
		paint(g);
	}

	public final void paint(Graphics g) {
		if ((loading_stage == 2)) {
			set_game_loading_bar(bar_perc, bar_text);
			return;
		}
		if (loading_stage == 0)
			preload_draw();
	}

	public void load_resources() {
		try {
			byte[] arc = DataUtil.load("jagex.jag");
			byte[] logo_buf = DataUtil.entry_extract("logo.tga", 0, arc);
			logo = tga_decode(logo_buf);
			Surface.font_define(DataUtil.entry_extract("h11p.jf", 0, arc));
			Surface.font_define(DataUtil.entry_extract("h12b.jf", 0, arc));
			Surface.font_define(DataUtil.entry_extract("h12p.jf", 0, arc));
			Surface.font_define(DataUtil.entry_extract("h13b.jf", 0, arc));
			Surface.font_define(DataUtil.entry_extract("h14b.jf", 0, arc));
			Surface.font_define(DataUtil.entry_extract("h16b.jf", 0, arc));
			Surface.font_define(DataUtil.entry_extract("h20b.jf", 0, arc));
			Surface.font_define(DataUtil.entry_extract("h24b.jf", 0, arc));
			return;
		} catch (IOException _ex) {
			System.out.println("Error loading jagex.dat");
		}
	}

	public void set_game_loading_bar(int perc, String text) {
		int x = (w - 281) / 2;
		int y = (h - 148) / 2;
		graphics.setColor(Color.black);
		graphics.fillRect(0, 0, w, h);
		if (!on_jagex)
			graphics.drawImage(logo, x, y, this);
		x += 2;
		y += 90;
		bar_perc = perc;
		bar_text = text;
		graphics.setColor(new Color(132, 132, 132));
		if (on_jagex)
			graphics.setColor(new Color(220, 0, 0));
		graphics.drawRect(x - 2, y - 2, 280, 23);
		graphics.fillRect(x, y, 277 * perc / 100, 20);
		graphics.setColor(new Color(198, 198, 198));
		if (on_jagex)
			graphics.setColor(new Color(255, 255, 255));
		text_draw(graphics, text, timesroman, x + 138, y + 10);
		if (!on_jagex) {
			text_draw(graphics, "Created by JAGeX - visit www.jagex.com",
					bold_helvetica, x + 138, y + 30);
			text_draw(graphics, "��2001-2002 Andrew Gower and Jagex Ltd",
					bold_helvetica, x + 138, y + 44);
		} else {
			graphics.setColor(new Color(132, 132, 152));
			text_draw(graphics, "��2001-2002 Andrew Gower and Jagex Ltd",
					helvetica, x + 138, h - 20);
		}
		if (additional_text != null) {
			graphics.setColor(Color.white);
			text_draw(graphics, additional_text, bold_helvetica,
					x + 138, y - 120);
		}
	}

	public void set_resource_loading_bar(int perc, String text) {
		int x = (w - 281) / 2;
		int y = (h - 148) / 2;
		x += 2;
		y += 90;
		bar_perc = perc;
		bar_text = text;
		int fill_w = 277 * perc / 100;
		graphics.setColor(new Color(132, 132, 132));
		if (on_jagex)
			graphics.setColor(new Color(220, 0, 0));
		graphics.fillRect(x, y, fill_w, 20);
		graphics.setColor(Color.black);
		graphics.fillRect(x + fill_w, y, 277 - fill_w, 20);
		graphics.setColor(new Color(198, 198, 198));
		if (on_jagex)
			graphics.setColor(new Color(255, 255, 255));
		text_draw(graphics, text, timesroman, x + 138, y + 10);
	}

	public void text_draw(Graphics g, String text, Font font, int x, int y) {
		Component obj;
		if (wnd == null)
			obj = this;
		else
			obj = wnd;
		FontMetrics metrics = obj.getFontMetrics(font);
		metrics.stringWidth(text);
		g.setFont(font);
		g.drawString(text, x - metrics.stringWidth(text) / 2,
				y + metrics.getHeight() / 4);
	}

	public Image tga_decode(byte[] buf) {
		int w = buf[13] * 256 + buf[12];
		int h = buf[15] * 256 + buf[14];
		byte[] palette_r = new byte[256];
		byte[] palette_g = new byte[256];
		byte[] palette_b = new byte[256];
		for (int k = 0; k < 256; k++) {
			palette_r[k] = buf[(20 + k * 3)];
			palette_g[k] = buf[(19 + k * 3)];
			palette_b[k] = buf[(18 + k * 3)];
		}

		IndexColorModel model = new IndexColorModel(8, 256, palette_r, palette_g, palette_b);
		byte[] pixels = new byte[w * h];
		int l = 0;
		for (int i1 = h - 1; i1 >= 0; i1--) {
			for (int j1 = 0; j1 < w; j1++) {
				pixels[(l++)] = buf[(786 + j1 + i1 * w)];
			}
		}

		MemoryImageSource source = new MemoryImageSource(w, h, model, pixels, 0, w);
		return createImage(source);
	}

	public byte[] load_file(String file, String desc, int perc)
			throws IOException {
		int attempts = 0;
		int decmp_len = 0;
		int len = 0;
		byte[] buf = null;
		while (attempts < 2)
			try {
				set_resource_loading_bar(perc, "Loading " + desc + " - 0%");
				if (attempts == 1)
					file = file.toUpperCase();
				InputStream stream = DataUtil.open_stream(file);
				DataInputStream _stream = new DataInputStream(stream);
				byte[] header = new byte[6];
				_stream.readFully(header, 0, 6);
				decmp_len = ((header[0] & 0xFF) << 16)
						+ ((header[1] & 0xFF) << 8) + (header[2] & 0xFF);
				len = ((header[3] & 0xFF) << 16) + ((header[4] & 0xFF) << 8)
						+ (header[5] & 0xFF);
				set_resource_loading_bar(perc, "Loading " + desc + " - 5%");
				int read = 0;
				buf = new byte[len];
				while (read < len) {
					int step = len - read;
					if (step > 1000)
						step = 1000;
					_stream.readFully(buf, read, step);
					read += step;
					set_resource_loading_bar(perc, "Loading " + desc + " - "
							+ (5 + read * 95 / len) + "%");
				}
				attempts = 2;
				_stream.close();
			} catch (IOException _ex) {
				attempts++;
			}
		set_resource_loading_bar(perc, "Unpacking " + desc);
		if (len != decmp_len) {
			byte[] abyte1 = new byte[decmp_len];
			Bzip2.decompress(abyte1, decmp_len, buf, len, 0);
			return abyte1;
		}
		return buf;
	}

}