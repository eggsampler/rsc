 package jagex.client;
 
 import java.io.InputStream;
 
 public class AudioStream extends InputStream
 {
   byte[] data;
   int ptr;
   int len;
 
   public AudioStream()
   {
     //AudioPlayer.player.start(this);
   }
 
   public void stop() {
     //AudioPlayer.player.stop(this);
   }
 
   public void set(byte[] data, int off, int len) {
     this.data = data;
     this.ptr = off;
     this.len = (off + len);
   }
 
   public int read(byte[] out, int i, int j) {
     for (int k = 0; k < j; k++) {
       if (this.ptr < this.len)
         out[(i + k)] = this.data[(this.ptr++)];
       else
         out[(i + k)] = -1;
     }
     return j;
   }
 
   public int read() {
     byte[] out = new byte[1];
     read(out, 0, 1);
     return out[0];
   }
 }