package jagex.client;

public final class Scene {

	public static final int anInt97 = 0;
	int ramp_cnt;
	int[] grad_base;
	int[][] grad_ramps;
	int[] ramp;
	public int last_poly_cnt;
	public int clip_near;
	public int clip_far_3d;
	public int clip_far_2d;
	public int fog_falloff;
	public int fog_dist;
	public static int[] sin2048_cache = new int[2048];
	private static int[] sin512_cache = new int[512];
	public boolean wide_band;
	public double aDouble111;
	public int anInt112;
	private boolean picking_active;
	private int click_x;
	private int click_y;
	private int picked_cnt;
	private int max_picked;
	private Model[] picked_models;
	private int[] picked_faces;
	private int width;
	private int clip_x;
	private int clip_y;
	private int base_x;
	private int base_y;
	private int view_dist;
	private int anInt126;
	private int cam_x;
	private int cam_y;
	private int cam_z;
	private int cam_yaw;
	private int cam_pitch;
	private int cam_roll;
	public int model_cnt;
	public int model_cap;
	public Model[] models;
	private int[] model_state;
	private int poly_cnt;
	private Polygon[] polys;
	private int sprite_cnt;
	private int[] sprite_id;
	private int[] sprite_x;
	private int[] sprite_y;
	private int[] sprite_z;
	private int[] sprite_w;
	private int[] sprite_h;
	private int[] sprite_trans_x;
	public Model view;
	int texture_cnt;
	byte[][] texture_raster;
	int[][] texture_palette;
	int[] texture_dim;
	long[] texture_id;
	int[][] texture_pixels;
	boolean[] texture_trans_back;
	private static long next_id;
	int[][] pool64;
	int[][] pool256;
	private static byte[] aByteArray163;
	Surface surface;
	public int[] raster;
	Scanline[] scanlines;
	int min_y;
	int max_y;
	int[] plane_x;
	int[] plane_y;
	int[] vert_shade;
	int[] vert_x;
	int[] vert_y;
	int[] vert_z;
	boolean skip_lines;
	static int frustum_max_x;
	static int frustum_min_x;
	static int frustum_max_y;
	static int frustum_min_y;
	static int frustum_far_z;
	static int frustum_near_z;
	int new_start;
	int new_end;

	public Scene(Surface class5, int model_cap, int polys, int sprites) {
		this.ramp_cnt = 50;
		this.grad_base = new int[this.ramp_cnt];
		this.grad_ramps = new int[this.ramp_cnt][256];
		this.clip_near = 5;
		this.clip_far_3d = 1000;
		this.clip_far_2d = 1000;
		this.fog_falloff = 20;
		this.fog_dist = 10;
		this.wide_band = false;
		this.aDouble111 = 1.1D;
		this.anInt112 = 1;
		this.picking_active = false;
		this.max_picked = 100;
		this.picked_models = new Model[this.max_picked];
		this.picked_faces = new int[this.max_picked];
		this.width = 512;
		this.clip_x = 256;
		this.clip_y = 192;
		this.base_x = 256;
		this.base_y = 256;
		this.view_dist = 8;
		this.anInt126 = 4;
		this.plane_x = new int[40];
		this.plane_y = new int[40];
		this.vert_shade = new int[40];
		this.vert_x = new int[40];
		this.vert_y = new int[40];
		this.vert_z = new int[40];
		this.skip_lines = false;
		this.surface = class5;
		this.clip_x = (class5.w / 2);
		this.clip_y = (class5.h / 2);
		this.raster = class5.pixels;
		this.model_cnt = 0;
		this.model_cap = model_cap;
		this.models = new Model[this.model_cap];
		this.model_state = new int[this.model_cap];
		this.poly_cnt = 0;
		this.polys = new Polygon[polys];
		for (int l = 0; l < polys; l++) {
			this.polys[l] = new Polygon();
		}
		this.sprite_cnt = 0;
		this.view = new Model(sprites * 2, sprites);
		this.sprite_id = new int[sprites];
		this.sprite_w = new int[sprites];
		this.sprite_h = new int[sprites];
		this.sprite_x = new int[sprites];
		this.sprite_y = new int[sprites];
		this.sprite_z = new int[sprites];
		this.sprite_trans_x = new int[sprites];
		if (aByteArray163 == null)
			aByteArray163 = new byte[17691];
		this.cam_x = 0;
		this.cam_y = 0;
		this.cam_z = 0;
		this.cam_yaw = 0;
		this.cam_pitch = 0;
		this.cam_roll = 0;
		for (int i1 = 0; i1 < 256; i1++) {
			sin512_cache[i1] = ((int) (Math.sin(i1 * 0.02454369D) * 32768.0D));
			sin512_cache[(i1 + 256)] = ((int) (Math.cos(i1 * 0.02454369D) * 32768.0D));
		}

		for (int j1 = 0; j1 < 1024; j1++) {
			sin2048_cache[j1] = ((int) (Math.sin(j1 * 0.00613592315D) * 32768.0D));
			sin2048_cache[(j1 + 1024)] = ((int) (Math.cos(j1 * 0.00613592315D) * 32768.0D));
		}
	}

	public void add(Model model) {
		if (model_cnt < model_cap) {
			model_state[model_cnt] = 0;
			models[model_cnt++] = model;
		}
	}

	public void remove(Model model) {
		for (int i = 0; i < this.model_cnt; i++)
			if (this.models[i] == model) {
				this.model_cnt -= 1;
				for (int j = i; j < this.model_cnt; j++) {
					this.models[j] = this.models[(j + 1)];
					this.model_state[j] = this.model_state[(j + 1)];
				}
			}
	}

	public void dispose() {
		clear();
		for (int i = 0; i < this.model_cnt; i++) {
			this.models[i] = null;
		}
		this.model_cnt = 0;
	}

	public void clear() {
		this.sprite_cnt = 0;
		this.view.clear();
	}

	public void reduce(int i) {
		this.sprite_cnt -= i;
		this.view.reduce(i, i * 2);
		if (this.sprite_cnt < 0)
			this.sprite_cnt = 0;
	}

	public int sprite_add(int i, int j, int k, int l, int i1, int j1, int k1) {
		this.sprite_id[this.sprite_cnt] = i;
		this.sprite_x[this.sprite_cnt] = j;
		this.sprite_y[this.sprite_cnt] = k;
		this.sprite_z[this.sprite_cnt] = l;
		this.sprite_w[this.sprite_cnt] = i1;
		this.sprite_h[this.sprite_cnt] = j1;
		this.sprite_trans_x[this.sprite_cnt] = 0;
		int tl = this.view.vert_add(j, k, l);
		int br = this.view.vert_add(j, k - j1, l);
		int[] ab = { tl, br };

		this.view.face_add(2, ab, 0, 0);
		this.view.face_pick_tag[this.sprite_cnt] = k1;
		this.view.is_local_player[(this.sprite_cnt++)] = 0;
		return this.sprite_cnt - 1;
	}

	public void set_local_player(int i) {
		this.view.is_local_player[i] = 1;
	}

	public void set_trans_x(int i, int j) {
		this.sprite_trans_x[i] = j;
	}

	public void consume_click(int i, int j) {
		this.click_x = (i - this.base_x);
		this.click_y = j;
		this.picked_cnt = 0;
		this.picking_active = true;
	}

	public int method199() {
		return this.picked_cnt;
	}

	public int[] picked_faces() {
		return this.picked_faces;
	}

	public Model[] picked_models() {
		return this.picked_models;
	}

	public void set_bounds(int base_x, int base_y, int clip_x, int clip_y, int width, int view_dist) {
		this.clip_x = clip_x;
		this.clip_y = clip_y;
		this.base_x = base_x;
		this.base_y = base_y;
		this.width = width;
		this.view_dist = view_dist;
		this.scanlines = new Scanline[clip_y + base_y];
		for (int i = 0; i < clip_y + base_y; i++)
			this.scanlines[i] = new Scanline();
	}

	private void qsort(Polygon[] entities, int lo, int hi) {
		if (lo < hi) {
			int _low = lo - 1;
			int _hi = hi + 1;
			int pivot = (lo + hi) / 2;
			Polygon mid = entities[pivot];
			entities[pivot] = entities[lo];
			entities[lo] = mid;
			int depth = mid.depth;
			while (_low < _hi) {
				do
					_hi--;
				while (entities[_hi].depth < depth);
				do
					_low++;
				while (entities[_low].depth > depth);
				if (_low < _hi) {
					Polygon class8_1 = entities[_low];
					entities[_low] = entities[_hi];
					entities[_hi] = class8_1;
				}
			}
			qsort(entities, lo, _hi);
			qsort(entities, _hi + 1, hi);
		}
	}

	public void intersect_sort(int step, Polygon[] frags, int cnt) {
		for (int k = 0; k <= cnt; k++) {
			frags[k].handled = false;
			frags[k].key = k;
			frags[k].successor = -1;
		}

		int l = 0;
		while (true)
			if (frags[l].handled) {
				l++;
			} else {
				if (l == cnt)
					return;
				Polygon frag = frags[l];
				frag.handled = true;
				int start = l;
				int end = l + step;
				if (end >= cnt)
					end = cnt - 1;
				for (int term = end; term >= start + 1; term--) {
					Polygon other = frags[term];
					if ((frag.min_plane_x < other.max_plane_x)
							&& (other.min_plane_x < frag.max_plane_x)
							&& (frag.min_plane_y < other.max_plane_y)
							&& (other.min_plane_y < frag.max_plane_y)
							&& (frag.key != other.successor)
							&& (!separate(frag, other))
							&& (heuristic(other, frag))) {
						order(frags, start, term);
						if (frags[term] != other)
							term++;
						start = this.new_start;
						other.successor = frag.key;
					}
				}
			}
	}

	public boolean order(Polygon[] polys, int start, int end) {
		while (true) {
			Polygon poly_a = polys[start];
			for (int pos = start + 1; pos <= end; pos++) {
				Polygon poly_b = polys[pos];
				if (!separate(poly_b, poly_a))
					break;
				polys[start] = poly_b;
				polys[pos] = poly_a;
				start = pos;
				if (start == end) {
					this.new_start = start;
					this.new_end = (start - 1);
					return true;
				}
			}

			Polygon poly_c = polys[end];
			for (int pos = end - 1; pos >= start; pos--) {
				Polygon poly_d = polys[pos];
				if (!separate(poly_c, poly_d))
					break;
				polys[end] = poly_d;
				polys[pos] = poly_c;
				end = pos;
				if (start == end) {
					this.new_start = (end + 1);
					this.new_end = end;
					return true;
				}
			}

			if (start + 1 >= end) {
				this.new_start = start;
				this.new_end = end;
				return false;
			}
			if (!order(polys, start + 1, end)) {
				this.new_start = start;
				return false;
			}
			end = this.new_end;
		}
	}

	public void set_frustum(int i, int j, int k) {
		int l = -this.cam_yaw + 1024 & 0x3FF;
		int i1 = -this.cam_pitch + 1024 & 0x3FF;
		int j1 = -this.cam_roll + 1024 & 0x3FF;
		if (j1 != 0) {
			int k1 = sin2048_cache[j1];
			int j2 = sin2048_cache[(j1 + 1024)];
			int i3 = j * k1 + i * j2 >> 15;
			j = j * j2 - i * k1 >> 15;
			i = i3;
		}
		if (l != 0) {
			int l1 = sin2048_cache[l];
			int k2 = sin2048_cache[(l + 1024)];
			int j3 = j * k2 - k * l1 >> 15;
			k = j * l1 + k * k2 >> 15;
			j = j3;
		}
		if (i1 != 0) {
			int i2 = sin2048_cache[i1];
			int l2 = sin2048_cache[(i1 + 1024)];
			int k3 = k * i2 + i * l2 >> 15;
			k = k * l2 - i * i2 >> 15;
			i = k3;
		}
		if (i < frustum_max_x)
			frustum_max_x = i;
		if (i > frustum_min_x)
			frustum_min_x = i;
		if (j < frustum_max_y)
			frustum_max_y = j;
		if (j > frustum_min_y)
			frustum_min_y = j;
		if (k < frustum_far_z)
			frustum_far_z = k;
		if (k > frustum_near_z)
			frustum_near_z = k;
	}

	public void render() {
		this.skip_lines = this.surface.skip_lines;
		int i3 = this.clip_x * this.clip_far_3d >> this.view_dist;
		int j3 = this.clip_y * this.clip_far_3d >> this.view_dist;
		frustum_max_x = 0;
		frustum_min_x = 0;
		frustum_max_y = 0;
		frustum_min_y = 0;
		frustum_far_z = 0;
		frustum_near_z = 0;
		set_frustum(-i3, -j3, this.clip_far_3d);
		set_frustum(-i3, j3, this.clip_far_3d);
		set_frustum(i3, -j3, this.clip_far_3d);
		set_frustum(i3, j3, this.clip_far_3d);
		set_frustum(-this.clip_x, -this.clip_y, 0);
		set_frustum(-this.clip_x, this.clip_y, 0);
		set_frustum(this.clip_x, -this.clip_y, 0);
		set_frustum(this.clip_x, this.clip_y, 0);
		frustum_max_x += this.cam_x;
		frustum_min_x += this.cam_x;
		frustum_max_y += this.cam_y;
		frustum_min_y += this.cam_y;
		frustum_far_z += this.cam_z;
		frustum_near_z += this.cam_z;
		this.models[this.model_cnt] = this.view;
		this.view.trans_state = 2;
		for (int i = 0; i < this.model_cnt; i++) {
			this.models[i].project(this.cam_x, this.cam_y, this.cam_z,
					this.cam_yaw, this.cam_pitch, this.cam_roll,
					this.view_dist, this.clip_near);
		}
		this.models[this.model_cnt].project(this.cam_x, this.cam_y, this.cam_z,
				this.cam_yaw, this.cam_pitch, this.cam_roll, this.view_dist,
				this.clip_near);
		this.poly_cnt = 0;
		for (int k3 = 0; k3 < this.model_cnt; k3++) {
			Model class7 = this.models[k3];
			if (class7.visible) {
				for (int j = 0; j < class7.face_cnt; j++) {
					int vert_cnt = class7.face_vert_cnt[j];
					int[] ai1 = class7.face_verts[j];
					boolean flag = false;
					for (int k4 = 0; k4 < vert_cnt; k4++) {
						int i1 = class7.project_vert_z[ai1[k4]];
						if ((i1 > this.clip_near) && (i1 < this.clip_far_3d)) {
							flag = true;
							break;
						}
					}
					if (flag) {
						int l1 = 0;
						for (int k5 = 0; k5 < vert_cnt; k5++) {
							int j1 = class7.project_plane_x[ai1[k5]];
							if (j1 > -this.clip_x)
								l1 |= 1;
							if (j1 < this.clip_x)
								l1 |= 2;
							if (l1 == 3) {
								break;
							}
						}
						if (l1 == 3) {
							int i2 = 0;
							for (int l6 = 0; l6 < vert_cnt; l6++) {
								int k1 = class7.project_plane_y[ai1[l6]];
								if (k1 > -this.clip_y)
									i2 |= 1;
								if (k1 < this.clip_y)
									i2 |= 2;
								if (i2 == 3) {
									break;
								}
							}
							if (i2 == 3) {
								Polygon class8_1 = this.polys[this.poly_cnt];
								class8_1.model = class7;
								class8_1.face = j;
								init_poly_3d(this.poly_cnt);
								int l8;
								if (class8_1.visibility < 0)
									l8 = class7.texture_front[j];
								else
									l8 = class7.texture_back[j];
								if (l8 != 12345678) {
									int sum_z = 0;
									for (int l9 = 0; l9 < vert_cnt; l9++)
										sum_z += class7.project_vert_z[ai1[l9]];
									class8_1.depth = (sum_z / vert_cnt + class7.depth);
									class8_1.texture = l8;
									this.poly_cnt += 1;
								}
							}
						}
					}
				}
			}

		}

		Model class7_1 = this.view;
		if (class7_1.visible) {
			for (int k = 0; k < class7_1.face_cnt; k++) {
				int[] ai = class7_1.face_verts[k];
				int j4 = ai[0];
				int l4 = class7_1.project_plane_x[j4];
				int l5 = class7_1.project_plane_y[j4];
				int i7 = class7_1.project_vert_z[j4];
				if ((i7 > this.clip_near) && (i7 < this.clip_far_2d)) {
					int i8 = (this.sprite_w[k] << this.view_dist) / i7;
					int i9 = (this.sprite_h[k] << this.view_dist) / i7;
					if ((l4 - i8 / 2 <= this.clip_x)
							&& (l4 + i8 / 2 >= -this.clip_x)
							&& (l5 - i9 <= this.clip_y) && (l5 >= -this.clip_y)) {
						Polygon class8_2 = this.polys[this.poly_cnt];
						class8_2.model = class7_1;
						class8_2.face = k;
						init_poly_2d(this.poly_cnt);
						class8_2.depth = ((i7 + class7_1.project_vert_z[ai[1]]) / 2);
						this.poly_cnt += 1;
					}
				}
			}
		}

		if (this.poly_cnt == 0)
			return;
		this.last_poly_cnt = this.poly_cnt;
		qsort(this.polys, 0, this.poly_cnt - 1);
		intersect_sort(100, this.polys, this.poly_cnt);
		for (int i4 = 0; i4 < this.poly_cnt; i4++) {
			Polygon poly = this.polys[i4];
			Model model = poly.model;
			int l = poly.face;
			if (model == this.view) {
				int[] verts = model.face_verts[l];
				int vert_a = verts[0];
				int a_x = model.project_plane_x[vert_a];
				int a_y = model.project_plane_y[vert_a];
				int a_depth = model.project_vert_z[vert_a];
				int w = (this.sprite_w[l] << this.view_dist) / a_depth;
				int h = (this.sprite_h[l] << this.view_dist) / a_depth;
				int dy = a_y - model.project_plane_y[verts[1]];
				int dx = (model.project_plane_x[verts[1]] - a_x) * dy / h;
				dx = model.project_plane_x[verts[1]] - a_x;
				int c_x = a_x - w / 2;
				int c_y = this.base_y + a_y - h;
				this.surface.sprite_3d_plot(c_x + this.base_x, c_y, w, h,
						this.sprite_id[l], dx, (256 << this.view_dist)
								/ a_depth);
				if ((this.picking_active)
						&& (this.picked_cnt < this.max_picked)) {
					c_x += (this.sprite_trans_x[l] << this.view_dist) / a_depth;
					if ((this.click_y >= c_y) && (this.click_y <= c_y + h)
							&& (this.click_x >= c_x)
							&& (this.click_x <= c_x + w) && (!model.unpickable)
							&& (model.is_local_player[l] == 0)) {
						this.picked_models[this.picked_cnt] = model;
						this.picked_faces[this.picked_cnt] = l;
						this.picked_cnt += 1;
					}
				}
			} else {
				int cnt = 0;
				int shade = 0;
				int vert_cnt = model.face_vert_cnt[l];
				int[] verts = model.face_verts[l];
				if (model.light_type == Model.use_flat_shade)
					if (poly.visibility < 0)
						shade = model.light_ambient - model.face_shade[l];
					else
						shade = model.light_ambient + model.face_shade[l];
				for (int k11 = 0; k11 < vert_cnt; k11++) {
					int k2 = verts[k11];
					this.vert_x[k11] = model.project_vert_x[k2];
					this.vert_y[k11] = model.project_vert_y[k2];
					this.vert_z[k11] = model.project_vert_z[k2];

					if (model.light_type != Model.use_flat_shade)
						if (poly.visibility < 0)
							shade = model.light_ambient - model.vert_shade[k2] + model.vert_ambient[k2];
						else
							shade = model.light_ambient + model.vert_shade[k2] + model.vert_ambient[k2];

					if (model.project_vert_z[k2] >= this.clip_near) {
						this.plane_x[cnt] = model.project_plane_x[k2];
						this.plane_y[cnt] = model.project_plane_y[k2];
						this.vert_shade[cnt] = shade;
						if (model.project_vert_z[k2] > this.fog_dist)
							this.vert_shade[cnt] += (model.project_vert_z[k2] - this.fog_dist)
									/ this.fog_falloff;
						cnt++;
					} else {
						int k9;
						if (k11 == 0)
							k9 = verts[(vert_cnt - 1)];
						else
							k9 = verts[(k11 - 1)];
						if (model.project_vert_z[k9] >= this.clip_near) {
							int k7 = model.project_vert_z[k2]
									- model.project_vert_z[k9];
							int i5 = model.project_vert_x[k2]
									- (model.project_vert_x[k2] - model.project_vert_x[k9])
									* (model.project_vert_z[k2] - this.clip_near)
									/ k7;
							int j6 = model.project_vert_y[k2]
									- (model.project_vert_y[k2] - model.project_vert_y[k9])
									* (model.project_vert_z[k2] - this.clip_near)
									/ k7;
							this.plane_x[cnt] = ((i5 << this.view_dist) / this.clip_near);
							this.plane_y[cnt] = ((j6 << this.view_dist) / this.clip_near);
							this.vert_shade[cnt] = shade;
							cnt++;
						}
						if (k11 == vert_cnt - 1)
							k9 = verts[0];
						else
							k9 = verts[(k11 + 1)];
						if (model.project_vert_z[k9] >= this.clip_near) {
							int l7 = model.project_vert_z[k2]
									- model.project_vert_z[k9];
							int j5 = model.project_vert_x[k2]
									- (model.project_vert_x[k2] - model.project_vert_x[k9])
									* (model.project_vert_z[k2] - this.clip_near)
									/ l7;
							int k6 = model.project_vert_y[k2]
									- (model.project_vert_y[k2] - model.project_vert_y[k9])
									* (model.project_vert_z[k2] - this.clip_near)
									/ l7;
							this.plane_x[cnt] = ((j5 << this.view_dist) / this.clip_near);
							this.plane_y[cnt] = ((k6 << this.view_dist) / this.clip_near);
							this.vert_shade[cnt] = shade;
							cnt++;
						}
					}
				}

				for (int i12 = 0; i12 < vert_cnt; i12++) {
					if (this.vert_shade[i12] < 0)
						this.vert_shade[i12] = 0;
					else if (this.vert_shade[i12] > 255)
						this.vert_shade[i12] = 255;
					if (poly.texture >= 0) {
						if (this.texture_dim[poly.texture] == 1)
							this.vert_shade[i12] <<= 9;
						else
							this.vert_shade[i12] <<= 6;
					}
				}
				gen_scanlines(0, 0, 0, 0, cnt, this.plane_x, this.plane_y,
						this.vert_shade, model, l);
				if (this.max_y > this.min_y) {
					rasterize(0, 0, vert_cnt, this.vert_x, this.vert_y,
							this.vert_z, poly.texture, model);
				}
			}
		}
		this.picking_active = false;
	}

	private void gen_scanlines(int start_x, int x, int a_ptr, int start_s,
			int vert_cnt, int[] vert_x, int[] vert_y, int[] vert_shade,
			Model model, int face) {
		if (vert_cnt == 3) {
			int a_y = vert_y[0] + this.base_y;
			int b_y = vert_y[1] + this.base_y;
			int c_y = vert_y[2] + this.base_y;
			int a_x = vert_x[0];
			int b_x = vert_x[1];
			int c_x = vert_x[2];
			int a_s = vert_shade[0];
			int b_s = vert_shade[1];
			int c_s = vert_shade[2];
			int max_y = this.base_y + this.clip_y - 1;
			int ac_start_x = 0;
			int ac_dx = 0;
			int ac_start_s = 0;
			int ac_ds = 0;
			int ac_start_y = 12345678;
			int ac_end_y = -12345678;
			if (c_y != a_y) {
				ac_dx = (c_x - a_x << 8) / (c_y - a_y);
				ac_ds = (c_s - a_s << 8) / (c_y - a_y);
				if (a_y < c_y) {
					ac_start_x = a_x << 8;
					ac_start_s = a_s << 8;
					ac_start_y = a_y;
					ac_end_y = c_y;
				} else {
					ac_start_x = c_x << 8;
					ac_start_s = c_s << 8;
					ac_start_y = c_y;
					ac_end_y = a_y;
				}
				if (ac_start_y < 0) {
					ac_start_x -= ac_dx * ac_start_y;
					ac_start_s -= ac_ds * ac_start_y;
					ac_start_y = 0;
				}
				if (ac_end_y > max_y)
					ac_end_y = max_y;
			}
			int ab_start_x = 0;
			int ab_dx = 0;
			int ab_start_s = 0;
			int ab_ds = 0;
			int ab_start_y = 12345678;
			int ab_end_y = -12345678;
			if (b_y != a_y) {
				ab_dx = (b_x - a_x << 8) / (b_y - a_y);
				ab_ds = (b_s - a_s << 8) / (b_y - a_y);
				if (a_y < b_y) {
					ab_start_x = a_x << 8;
					ab_start_s = a_s << 8;
					ab_start_y = a_y;
					ab_end_y = b_y;
				} else {
					ab_start_x = b_x << 8;
					ab_start_s = b_s << 8;
					ab_start_y = b_y;
					ab_end_y = a_y;
				}
				if (ab_start_y < 0) {
					ab_start_x -= ab_dx * ab_start_y;
					ab_start_s -= ab_ds * ab_start_y;
					ab_start_y = 0;
				}
				if (ab_end_y > max_y)
					ab_end_y = max_y;
			}
			int bc_start_x = 0;
			int bc_dx = 0;
			int bc_start_s = 0;
			int bc_ds = 0;
			int bc_start_y = 12345678;
			int bc_end_y = -12345678;
			if (c_y != b_y) {
				bc_dx = (c_x - b_x << 8) / (c_y - b_y);
				bc_ds = (c_s - b_s << 8) / (c_y - b_y);
				if (b_y < c_y) {
					bc_start_x = b_x << 8;
					bc_start_s = b_s << 8;
					bc_start_y = b_y;
					bc_end_y = c_y;
				} else {
					bc_start_x = c_x << 8;
					bc_start_s = c_s << 8;
					bc_start_y = c_y;
					bc_end_y = b_y;
				}
				if (bc_start_y < 0) {
					bc_start_x -= bc_dx * bc_start_y;
					bc_start_s -= bc_ds * bc_start_y;
					bc_start_y = 0;
				}
				if (bc_end_y > max_y)
					bc_end_y = max_y;
			}
			this.min_y = ac_start_y;
			if (ab_start_y < this.min_y)
				this.min_y = ab_start_y;
			if (bc_start_y < this.min_y)
				this.min_y = bc_start_y;
			this.max_y = ac_end_y;
			if (ab_end_y > this.max_y)
				this.max_y = ab_end_y;
			if (bc_end_y > this.max_y)
				this.max_y = bc_end_y;
			int s = 0;
			for (a_ptr = this.min_y; a_ptr < this.max_y; a_ptr++) {
				if ((a_ptr >= ac_start_y) && (a_ptr < ac_end_y)) {
					start_x = x = ac_start_x;
					start_s = s = ac_start_s;
					ac_start_x += ac_dx;
					ac_start_s += ac_ds;
				} else {
					start_x = 655360;
					x = -655360;
				}
				if ((a_ptr >= ab_start_y) && (a_ptr < ab_end_y)) {
					if (ab_start_x < start_x) {
						start_x = ab_start_x;
						start_s = ab_start_s;
					}
					if (ab_start_x > x) {
						x = ab_start_x;
						s = ab_start_s;
					}
					ab_start_x += ab_dx;
					ab_start_s += ab_ds;
				}
				if ((a_ptr >= bc_start_y) && (a_ptr < bc_end_y)) {
					if (bc_start_x < start_x) {
						start_x = bc_start_x;
						start_s = bc_start_s;
					}
					if (bc_start_x > x) {
						x = bc_start_x;
						s = bc_start_s;
					}
					bc_start_x += bc_dx;
					bc_start_s += bc_ds;
				}
				Scanline line = this.scanlines[a_ptr];
				line.start_x = start_x;
				line.end_x = x;
				line.start_s = start_s;
				line.end_s = s;
			}

			if (this.min_y < this.base_y - this.clip_y)
				this.min_y = (this.base_y - this.clip_y);
		} else if (vert_cnt == 4) {
			int a_y = vert_y[0] + this.base_y;
			int b_y = vert_y[1] + this.base_y;
			int c_y = vert_y[2] + this.base_y;
			int d_y = vert_y[3] + this.base_y;
			int a_x = vert_x[0];
			int b_x = vert_x[1];
			int c_x = vert_x[2];
			int d_x = vert_x[3];
			int a_s = vert_shade[0];
			int b_s = vert_shade[1];
			int c_s = vert_shade[2];
			int d_s = vert_shade[3];
			int max_y = this.base_y + this.clip_y - 1;
			int ad_start_x = 0;
			int ad_dx = 0;
			int ad_start_s = 0;
			int ad_ds = 0;
			int ad_start_y = 12345678;
			int ad_end_y = -12345678;
			if (d_y != a_y) {
				ad_dx = (d_x - a_x << 8) / (d_y - a_y);
				ad_ds = (d_s - a_s << 8) / (d_y - a_y);
				if (a_y < d_y) {
					ad_start_x = a_x << 8;
					ad_start_s = a_s << 8;
					ad_start_y = a_y;
					ad_end_y = d_y;
				} else {
					ad_start_x = d_x << 8;
					ad_start_s = d_s << 8;
					ad_start_y = d_y;
					ad_end_y = a_y;
				}
				if (ad_start_y < 0) {
					ad_start_x -= ad_dx * ad_start_y;
					ad_start_s -= ad_ds * ad_start_y;
					ad_start_y = 0;
				}
				if (ad_end_y > max_y)
					ad_end_y = max_y;
			}
			int ab_start_x = 0;
			int ab_dx = 0;
			int ab_start_s = 0;
			int ab_ds = 0;
			int ab_start_y = 12345678;
			int ab_end_y = -12345678;
			if (b_y != a_y) {
				ab_dx = (b_x - a_x << 8) / (b_y - a_y);
				ab_ds = (b_s - a_s << 8) / (b_y - a_y);
				if (a_y < b_y) {
					ab_start_x = a_x << 8;
					ab_start_s = a_s << 8;
					ab_start_y = a_y;
					ab_end_y = b_y;
				} else {
					ab_start_x = b_x << 8;
					ab_start_s = b_s << 8;
					ab_start_y = b_y;
					ab_end_y = a_y;
				}
				if (ab_start_y < 0) {
					ab_start_x -= ab_dx * ab_start_y;
					ab_start_s -= ab_ds * ab_start_y;
					ab_start_y = 0;
				}
				if (ab_end_y > max_y)
					ab_end_y = max_y;
			}
			int bc_start_x = 0;
			int bc_dx = 0;
			int bc_start_s = 0;
			int bc_ds = 0;
			int bc_start_y = 12345678;
			int bc_end_y = -12345678;
			if (c_y != b_y) {
				bc_dx = (c_x - b_x << 8) / (c_y - b_y);
				bc_ds = (c_s - b_s << 8) / (c_y - b_y);
				if (b_y < c_y) {
					bc_start_x = b_x << 8;
					bc_start_s = b_s << 8;
					bc_start_y = b_y;
					bc_end_y = c_y;
				} else {
					bc_start_x = c_x << 8;
					bc_start_s = c_s << 8;
					bc_start_y = c_y;
					bc_end_y = b_y;
				}
				if (bc_start_y < 0) {
					bc_start_x -= bc_dx * bc_start_y;
					bc_start_s -= bc_ds * bc_start_y;
					bc_start_y = 0;
				}
				if (bc_end_y > max_y)
					bc_end_y = max_y;
			}
			int cd_start_x = 0;
			int cd_dx = 0;
			int cd_start_s = 0;
			int cd_ds = 0;
			int cd_start_y = 12345678;
			int cd_end_y = -12345678;
			if (d_y != c_y) {
				cd_dx = (d_x - c_x << 8) / (d_y - c_y);
				cd_ds = (d_s - c_s << 8) / (d_y - c_y);
				if (c_y < d_y) {
					cd_start_x = c_x << 8;
					cd_start_s = c_s << 8;
					cd_start_y = c_y;
					cd_end_y = d_y;
				} else {
					cd_start_x = d_x << 8;
					cd_start_s = d_s << 8;
					cd_start_y = d_y;
					cd_end_y = c_y;
				}
				if (cd_start_y < 0) {
					cd_start_x -= cd_dx * cd_start_y;
					cd_start_s -= cd_ds * cd_start_y;
					cd_start_y = 0;
				}
				if (cd_end_y > max_y)
					cd_end_y = max_y;
			}
			this.min_y = ad_start_y;
			if (ab_start_y < this.min_y)
				this.min_y = ab_start_y;
			if (bc_start_y < this.min_y)
				this.min_y = bc_start_y;
			if (cd_start_y < this.min_y)
				this.min_y = cd_start_y;
			this.max_y = ad_end_y;
			if (ab_end_y > this.max_y)
				this.max_y = ab_end_y;
			if (bc_end_y > this.max_y)
				this.max_y = bc_end_y;
			if (cd_end_y > this.max_y)
				this.max_y = cd_end_y;
			int j24 = 0;
			for (a_ptr = this.min_y; a_ptr < this.max_y; a_ptr++) {
				if ((a_ptr >= ad_start_y) && (a_ptr < ad_end_y)) {
					start_x = x = ad_start_x;
					start_s = j24 = ad_start_s;
					ad_start_x += ad_dx;
					ad_start_s += ad_ds;
				} else {
					start_x = 655360;
					x = -655360;
				}
				if ((a_ptr >= ab_start_y) && (a_ptr < ab_end_y)) {
					if (ab_start_x < start_x) {
						start_x = ab_start_x;
						start_s = ab_start_s;
					}
					if (ab_start_x > x) {
						x = ab_start_x;
						j24 = ab_start_s;
					}
					ab_start_x += ab_dx;
					ab_start_s += ab_ds;
				}
				if ((a_ptr >= bc_start_y) && (a_ptr < bc_end_y)) {
					if (bc_start_x < start_x) {
						start_x = bc_start_x;
						start_s = bc_start_s;
					}
					if (bc_start_x > x) {
						x = bc_start_x;
						j24 = bc_start_s;
					}
					bc_start_x += bc_dx;
					bc_start_s += bc_ds;
				}
				if ((a_ptr >= cd_start_y) && (a_ptr < cd_end_y)) {
					if (cd_start_x < start_x) {
						start_x = cd_start_x;
						start_s = cd_start_s;
					}
					if (cd_start_x > x) {
						x = cd_start_x;
						j24 = cd_start_s;
					}
					cd_start_x += cd_dx;
					cd_start_s += cd_ds;
				}
				Scanline line = this.scanlines[a_ptr];
				line.start_x = start_x;
				line.end_x = x;
				line.start_s = start_s;
				line.end_s = j24;
			}

			if (this.min_y < this.base_y - this.clip_y)
				this.min_y = (this.base_y - this.clip_y);
		} else {
			this.max_y = (this.min_y = vert_y[0] += this.base_y);
			for (a_ptr = 1; a_ptr < vert_cnt; a_ptr++) {
				int val;
				if ((val = vert_y[a_ptr] += this.base_y) < this.min_y)
					this.min_y = val;
				else if (val > this.max_y) {
					this.max_y = val;
				}
			}
			if (this.min_y < this.base_y - this.clip_y)
				this.min_y = (this.base_y - this.clip_y);
			if (this.max_y >= this.base_y + this.clip_y)
				this.max_y = (this.base_y + this.clip_y - 1);
			if (this.min_y >= this.max_y)
				return;
			for (a_ptr = this.min_y; a_ptr < this.max_y; a_ptr++) {
				Scanline line = this.scanlines[a_ptr];
				line.start_x = 655360;
				line.end_x = -655360;
			}

			int last = vert_cnt - 1;
			int u_y = vert_y[0];
			int v_y = vert_y[last];
			if (u_y < v_y) {
				int uv_start_x = vert_x[0] << 8;
				int uv_dx = (vert_x[last] - vert_x[0] << 8) / (v_y - u_y);
				int uv_start_s = vert_shade[0] << 8;
				int uv_ds = (vert_shade[last] - vert_shade[0] << 8)
						/ (v_y - u_y);
				if (u_y < 0) {
					uv_start_x -= uv_dx * u_y;
					uv_start_s -= uv_ds * u_y;
					u_y = 0;
				}
				if (v_y > this.max_y)
					v_y = this.max_y;
				for (a_ptr = u_y; a_ptr <= v_y; a_ptr++) {
					Scanline line = this.scanlines[a_ptr];
					line.start_x = (line.end_x = uv_start_x);
					line.start_s = (line.end_s = uv_start_s);
					uv_start_x += uv_dx;
					uv_start_s += uv_ds;
				}
			} else if (u_y > v_y) {
				int uv_start_x = vert_x[last] << 8;
				int uv_dx = (vert_x[0] - vert_x[last] << 8) / (u_y - v_y);
				int uv_start_s = vert_shade[last] << 8;
				int uv_ds = (vert_shade[0] - vert_shade[last] << 8)
						/ (u_y - v_y);
				if (v_y < 0) {
					uv_start_x -= uv_dx * v_y;
					uv_start_s -= uv_ds * v_y;
					v_y = 0;
				}
				if (u_y > this.max_y)
					u_y = this.max_y;
				for (a_ptr = v_y; a_ptr <= u_y; a_ptr++) {
					Scanline class4_3 = this.scanlines[a_ptr];
					class4_3.start_x = (class4_3.end_x = uv_start_x);
					class4_3.start_s = (class4_3.end_s = uv_start_s);
					uv_start_x += uv_dx;
					uv_start_s += uv_ds;
				}
			}

			for (a_ptr = 0; a_ptr < last; a_ptr++) {
				int b_ptr = a_ptr + 1;
				int a_y = vert_y[a_ptr];
				int b_y = vert_y[b_ptr];
				if (a_y < b_y) {
					int ab_start_x = vert_x[a_ptr] << 8;
					int ab_dx = (vert_x[b_ptr] - vert_x[a_ptr] << 8)
							/ (b_y - a_y);
					int ab_start_s = vert_shade[a_ptr] << 8;
					int ab_ds = (vert_shade[b_ptr] - vert_shade[a_ptr] << 8)
							/ (b_y - a_y);
					if (a_y < 0) {
						ab_start_x -= ab_dx * a_y;
						ab_start_s -= ab_ds * a_y;
						a_y = 0;
					}
					if (b_y > this.max_y)
						b_y = this.max_y;
					for (int l11 = a_y; l11 <= b_y; l11++) {
						Scanline line = this.scanlines[l11];
						if (ab_start_x < line.start_x) {
							line.start_x = ab_start_x;
							line.start_s = ab_start_s;
						}
						if (ab_start_x > line.end_x) {
							line.end_x = ab_start_x;
							line.end_s = ab_start_s;
						}
						ab_start_x += ab_dx;
						ab_start_s += ab_ds;
					}
				} else if (a_y > b_y) {
					int ab_start_x = vert_x[b_ptr] << 8;
					int ab_dx = (vert_x[a_ptr] - vert_x[b_ptr] << 8)
							/ (a_y - b_y);
					int ab_start_s = vert_shade[b_ptr] << 8;
					int ab_ds = (vert_shade[a_ptr] - vert_shade[b_ptr] << 8)
							/ (a_y - b_y);
					if (b_y < 0) {
						ab_start_x -= ab_dx * b_y;
						ab_start_s -= ab_ds * b_y;
						b_y = 0;
					}
					if (a_y > this.max_y)
						a_y = this.max_y;
					for (int i12 = b_y; i12 <= a_y; i12++) {
						Scanline line = this.scanlines[i12];
						if (ab_start_x < line.start_x) {
							line.start_x = ab_start_x;
							line.start_s = ab_start_s;
						}
						if (ab_start_x > line.end_x) {
							line.end_x = ab_start_x;
							line.end_s = ab_start_s;
						}
						ab_start_x += ab_dx;
						ab_start_s += ab_ds;
					}
				}

			}

			if (this.min_y < this.base_y - this.clip_y)
				this.min_y = (this.base_y - this.clip_y);
		}
		if ((this.picking_active) && (this.picked_cnt < this.max_picked)
				&& (this.click_y >= this.min_y) && (this.click_y < this.max_y)) {
			Scanline line = this.scanlines[this.click_y];
			if ((this.click_x >= line.start_x >> 8)
					&& (this.click_x <= line.end_x >> 8)
					&& (line.start_x <= line.end_x) && (!model.unpickable)
					&& (model.is_local_player[face] == 0)) {
				this.picked_models[this.picked_cnt] = model;
				this.picked_faces[this.picked_cnt] = face;
				this.picked_cnt += 1;
			}
		}
	}

	private void rasterize(int i, int start_x, int vert_cnt, int[] vert_x,
			int[] vert_y, int[] vert_z, int texture, Model model) {
		if (texture >= 0) {
			if (texture >= this.texture_cnt)
				texture = 0;
			texture_prepare(texture);
			int a_x = vert_x[0];
			int a_y = vert_y[0];
			int a_z = vert_z[0];
			int b_x = a_x - vert_x[1];
			int b_y = a_y - vert_y[1];
			int b_z = a_z - vert_z[1];
			vert_cnt--;
			int c_x = vert_x[vert_cnt] - a_x;
			int c_y = vert_y[vert_cnt] - a_y;
			int c_z = vert_z[vert_cnt] - a_z;
			if (this.texture_dim[texture] == 1) {
				int a = c_x * a_y - c_y * a_x << 12;
				int h_a = c_y * a_z - c_z * a_y << 5 - this.view_dist + 7 + 4;
				int v_a = c_z * a_x - c_x * a_z << 5 - this.view_dist + 7;
				int b = b_x * a_y - b_y * a_x << 12;
				int h_b = b_y * a_z - b_z * a_y << 5 - this.view_dist + 7 + 4;
				int v_b = b_z * a_x - b_x * a_z << 5 - this.view_dist + 7;
				int c = b_y * c_x - b_x * c_y << 5;
				int h_c = b_z * c_y - b_y * c_z << 5 - this.view_dist + 4;
				int v_c = b_x * c_z - b_z * c_x >> this.view_dist - 5;
				int _h_a = h_a >> 4;
				int _h_b = h_b >> 4;
				int _h_c = h_c >> 4;
				int off_y = this.min_y - this.base_y;
				int width = this.width;
				int out_ptr = this.base_x + this.min_y * width;
				byte step = 1;
				a += v_a * off_y;
				b += v_b * off_y;
				c += v_c * off_y;
				if (this.skip_lines) {
					if ((this.min_y & 0x1) == 1) {
						this.min_y += 1;
						a += v_a;
						b += v_b;
						c += v_c;
						out_ptr += width;
					}
					v_a <<= 1;
					v_b <<= 1;
					v_c <<= 1;
					width <<= 1;
					step = 2;
				}
				if (model.trans_texture) {
					for (i = this.min_y; i < this.max_y; i += step) {
						Scanline class4_3 = this.scanlines[i];
						start_x = class4_3.start_x >> 8;
						int end_x = class4_3.end_x >> 8;
						int dx = end_x - start_x;
						if (dx <= 0) {
							a += v_a;
							b += v_b;
							c += v_c;
							out_ptr += width;
						} else {
							int start_s = class4_3.start_s;
							int ds = (class4_3.end_s - start_s) / dx;
							if (start_x < -this.clip_x) {
								start_s += (-this.clip_x - start_x) * ds;
								start_x = -this.clip_x;
								dx = end_x - start_x;
							}
							if (end_x > this.clip_x) {
								int _end_x = this.clip_x;
								dx = _end_x - start_x;
							}
							trans_tex1_scanline(this.raster,
									this.texture_pixels[texture], 0, 0, a
											+ _h_a * start_x, b + _h_b
											* start_x, c + _h_c * start_x, h_a,
									h_b, h_c, dx, out_ptr + start_x, start_s,
									ds << 2);
							a += v_a;
							b += v_b;
							c += v_c;
							out_ptr += width;
						}
					}

					return;
				}
				if (this.texture_trans_back[texture]) {
					for (i = this.min_y; i < this.max_y; i += step) {
						Scanline class4_4 = this.scanlines[i];
						start_x = class4_4.start_x >> 8;
						int end_x = class4_4.end_x >> 8;
						int dx = end_x - start_x;
						if (dx <= 0) {
							a += v_a;
							b += v_b;
							c += v_c;
							out_ptr += width;
						} else {
							int start_s = class4_4.start_s;
							int ds = (class4_4.end_s - start_s) / dx;
							if (start_x < -this.clip_x) {
								start_s += (-this.clip_x - start_x) * ds;
								start_x = -this.clip_x;
								dx = end_x - start_x;
							}
							if (end_x > this.clip_x) {
								int _end_x = this.clip_x;
								dx = _end_x - start_x;
							}
							tex1_scanline(this.raster,
									this.texture_pixels[texture], 0, 0, a
											+ _h_a * start_x, b + _h_b
											* start_x, c + _h_c * start_x, h_a,
									h_b, h_c, dx, out_ptr + start_x, start_s,
									ds << 2);
							a += v_a;
							b += v_b;
							c += v_c;
							out_ptr += width;
						}
					}

					return;
				}
				for (i = this.min_y; i < this.max_y; i += step) {
					Scanline class4_5 = this.scanlines[i];
					start_x = class4_5.start_x >> 8;
					int end_x = class4_5.end_x >> 8;
					int dx = end_x - start_x;
					if (dx <= 0) {
						a += v_a;
						b += v_b;
						c += v_c;
						out_ptr += width;
					} else {
						int start_s = class4_5.start_s;
						int ds = (class4_5.end_s - start_s) / dx;
						if (start_x < -this.clip_x) {
							start_s += (-this.clip_x - start_x) * ds;
							start_x = -this.clip_x;
							dx = end_x - start_x;
						}
						if (end_x > this.clip_x) {
							int l18 = this.clip_x;
							dx = l18 - start_x;
						}
						trans_back_tex1_scanline(this.raster, 0, 0, 0,
								this.texture_pixels[texture], a + _h_a
										* start_x, b + _h_b * start_x, c + _h_c
										* start_x, h_a, h_b, h_c, dx, out_ptr
										+ start_x, start_s, ds);
						a += v_a;
						b += v_b;
						c += v_c;
						out_ptr += width;
					}
				}

				return;
			}
			int o_a = c_x * a_y - c_y * a_x << 11;
			int h_a = c_y * a_z - c_z * a_y << 5 - this.view_dist + 6 + 4;
			int v_a = c_z * a_x - c_x * a_z << 5 - this.view_dist + 6;
			int o_b = b_x * a_y - b_y * a_x << 11;
			int h_b = b_y * a_z - b_z * a_y << 5 - this.view_dist + 6 + 4;
			int v_b = b_z * a_x - b_x * a_z << 5 - this.view_dist + 6;
			int o_c = b_y * c_x - b_x * c_y << 5;
			int h_c = b_z * c_y - b_y * c_z << 5 - this.view_dist + 4;
			int v_c = b_x * c_z - b_z * c_x >> this.view_dist - 5;
			int _h_a = h_a >> 4;
			int _h_b = h_b >> 4;
			int _h_c = h_c >> 4;
			int j16 = this.min_y - this.base_y;
			int width = this.width;
			int out_ptr = this.base_x + this.min_y * width;
			byte step = 1;
			o_a += v_a * j16;
			o_b += v_b * j16;
			o_c += v_c * j16;
			if (this.skip_lines) {
				if ((this.min_y & 0x1) == 1) {
					this.min_y += 1;
					o_a += v_a;
					o_b += v_b;
					o_c += v_c;
					out_ptr += width;
				}
				v_a <<= 1;
				v_b <<= 1;
				v_c <<= 1;
				width <<= 1;
				step = 2;
			}
			if (model.trans_texture) {
				for (i = this.min_y; i < this.max_y; i += step) {
					Scanline class4_6 = this.scanlines[i];
					start_x = class4_6.start_x >> 8;
					int end_x = class4_6.end_x >> 8;
					int dx = end_x - start_x;
					if (dx <= 0) {
						o_a += v_a;
						o_b += v_b;
						o_c += v_c;
						out_ptr += width;
					} else {
						int start_s = class4_6.start_s;
						int ds = (class4_6.end_s - start_s) / dx;
						if (start_x < -this.clip_x) {
							start_s += (-this.clip_x - start_x) * ds;
							start_x = -this.clip_x;
							dx = end_x - start_x;
						}
						if (end_x > this.clip_x) {
							int j19 = this.clip_x;
							dx = j19 - start_x;
						}
						trans_tex0_scanline(this.raster,
								this.texture_pixels[texture], 0, 0, o_a + _h_a
										* start_x, o_b + _h_b * start_x, o_c
										+ _h_c * start_x, h_a, h_b, h_c, dx,
								out_ptr + start_x, start_s, ds);
						o_a += v_a;
						o_b += v_b;
						o_c += v_c;
						out_ptr += width;
					}
				}

				return;
			}
			if (this.texture_trans_back[texture]) {
				for (i = this.min_y; i < this.max_y; i += step) {
					Scanline class4_7 = this.scanlines[i];
					start_x = class4_7.start_x >> 8;
					int end_x = class4_7.end_x >> 8;
					int dx = end_x - start_x;
					if (dx <= 0) {
						o_a += v_a;
						o_b += v_b;
						o_c += v_c;
						out_ptr += width;
					} else {
						int start_s = class4_7.start_s;
						int ds = (class4_7.end_s - start_s) / dx;
						if (start_x < -this.clip_x) {
							start_s += (-this.clip_x - start_x) * ds;
							start_x = -this.clip_x;
							dx = end_x - start_x;
						}
						if (end_x > this.clip_x) {
							int l19 = this.clip_x;
							dx = l19 - start_x;
						}
						tex0_scanline(this.raster,
								this.texture_pixels[texture], 0, 0, o_a + _h_a
										* start_x, o_b + _h_b * start_x, o_c
										+ _h_c * start_x, h_a, h_b, h_c, dx,
								out_ptr + start_x, start_s, ds);
						o_a += v_a;
						o_b += v_b;
						o_c += v_c;
						out_ptr += width;
					}
				}

				return;
			}
			for (i = this.min_y; i < this.max_y; i += step) {
				Scanline class4_8 = this.scanlines[i];
				start_x = class4_8.start_x >> 8;
				int end_x = class4_8.end_x >> 8;
				int dx = end_x - start_x;
				if (dx <= 0) {
					o_a += v_a;
					o_b += v_b;
					o_c += v_c;
					out_ptr += width;
				} else {
					int start_s = class4_8.start_s;
					int ds = (class4_8.end_s - start_s) / dx;
					if (start_x < -this.clip_x) {
						start_s += (-this.clip_x - start_x) * ds;
						start_x = -this.clip_x;
						dx = end_x - start_x;
					}
					if (end_x > this.clip_x) {
						int j20 = this.clip_x;
						dx = j20 - start_x;
					}
					trans_back_tex0_scanline(this.raster, 0, 0, 0,
							this.texture_pixels[texture], o_a + _h_a * start_x,
							o_b + _h_b * start_x, o_c + _h_c * start_x, h_a,
							h_b, h_c, dx, out_ptr + start_x, start_s, ds);
					o_a += v_a;
					o_b += v_b;
					o_c += v_c;
					out_ptr += width;
				}
			}

			return;
		}
		for (int j1 = 0; j1 < this.ramp_cnt; j1++) {
			if (this.grad_base[j1] == texture) {
				this.ramp = this.grad_ramps[j1];
				break;
			}
			if (j1 == this.ramp_cnt - 1) {
				int ptr = (int) (Math.random() * this.ramp_cnt);
				this.grad_base[ptr] = texture;
				texture = -1 - texture;
				int k2 = (texture >> 10 & 0x1F) * 8;
				int j3 = (texture >> 5 & 0x1F) * 8;
				int l3 = (texture & 0x1F) * 8;
				for (int j4 = 0; j4 < 256; j4++) {
					int j6 = j4 * j4;
					int k7 = k2 * j6 / 65536;
					int l8 = j3 * j6 / 65536;
					int j10 = l3 * j6 / 65536;
					this.grad_ramps[ptr][(255 - j4)] = ((k7 << 16) + (l8 << 8) + j10);
				}

				this.ramp = this.grad_ramps[ptr];
			}
		}

		int width = this.width;
		int out_ptr = this.base_x + this.min_y * width;
		byte step = 1;
		if (this.skip_lines) {
			if ((this.min_y & 0x1) == 1) {
				this.min_y += 1;
				out_ptr += width;
			}
			width <<= 1;
			step = 2;
		}
		if (model.trans) {
			for (i = this.min_y; i < this.max_y; i += step) {
				Scanline class4 = this.scanlines[i];
				start_x = class4.start_x >> 8;
				int end_x = class4.end_x >> 8;
				int dx = end_x - start_x;
				if (dx <= 0) {
					out_ptr += width;
				} else {
					int start_s = class4.start_s;
					int end_s = (class4.end_s - start_s) / dx;
					if (start_x < -this.clip_x) {
						start_s += (-this.clip_x - start_x) * end_s;
						start_x = -this.clip_x;
						dx = end_x - start_x;
					}
					if (end_x > this.clip_x) {
						int l4 = this.clip_x;
						dx = l4 - start_x;
					}
					trans_grad1_scanline(this.raster, -dx, out_ptr + start_x,
							0, this.ramp, start_s, end_s);
					out_ptr += width;
				}
			}

			return;
		}
		if (this.wide_band) {
			for (i = this.min_y; i < this.max_y; i += step) {
				Scanline class4_1 = this.scanlines[i];
				start_x = class4_1.start_x >> 8;
				int end_x = class4_1.end_x >> 8;
				int dx = end_x - start_x;
				if (dx <= 0) {
					out_ptr += width;
				} else {
					int start_s = class4_1.start_s;
					int ds = (class4_1.end_s - start_s) / dx;
					if (start_x < -this.clip_x) {
						start_s += (-this.clip_x - start_x) * ds;
						start_x = -this.clip_x;
						dx = end_x - start_x;
					}
					if (end_x > this.clip_x) {
						int j5 = this.clip_x;
						dx = j5 - start_x;
					}
					grad0_scanline(this.raster, -dx, out_ptr + start_x, 0,
							this.ramp, start_s, ds);
					out_ptr += width;
				}
			}

			return;
		}
		for (i = this.min_y; i < this.max_y; i += step) {
			Scanline class4_2 = this.scanlines[i];
			start_x = class4_2.start_x >> 8;
			int end_x = class4_2.end_x >> 8;
			int dx = end_x - start_x;
			if (dx <= 0) {
				out_ptr += width;
			} else {
				int start_s = class4_2.start_s;
				int ds = (class4_2.end_s - start_s) / dx;
				if (start_x < -this.clip_x) {
					start_s += (-this.clip_x - start_x) * ds;
					start_x = -this.clip_x;
					dx = end_x - start_x;
				}
				if (end_x > this.clip_x) {
					int l5 = this.clip_x;
					dx = l5 - start_x;
				}
				grad1_scanline(this.raster, -dx, out_ptr + start_x, 0,
						this.ramp, start_s, ds);
				out_ptr += width;
			}
		}
	}

	private static void tex1_scanline(int[] out, int[] in, int u, int v, int a,
			int b, int c, int da, int db, int dc, int dx, int ptr, int s, int ds) {
		if (dx <= 0)
			return;
		int end_u = 0;
		int end_v = 0;
		int shade_pow = 0;
		if (c != 0) {
			u = a / c << 7;
			v = b / c << 7;
		}
		if (u < 0)
			u = 0;
		else if (u > 16256)
			u = 16256;
		a += da;
		b += db;
		c += dc;
		if (c != 0) {
			end_u = a / c << 7;
			end_v = b / c << 7;
		}
		if (end_u < 0)
			end_u = 0;
		else if (end_u > 16256)
			end_u = 16256;
		int dv = end_u - u >> 4;
		int du = end_v - v >> 4;
		for (int j4 = dx >> 4; j4 > 0; j4--) {
			u += (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			u = (u & 0x3FFF) + (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			u = (u & 0x3FFF) + (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			u = (u & 0x3FFF) + (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u = end_u;
			v = end_v;
			a += da;
			b += db;
			c += dc;
			if (c != 0) {
				end_u = a / c << 7;
				end_v = b / c << 7;
			}
			if (end_u < 0)
				end_u = 0;
			else if (end_u > 16256)
				end_u = 16256;
			dv = end_u - u >> 4;
			du = end_v - v >> 4;
		}

		for (int k4 = 0; k4 < (dx & 0xF); k4++) {
			if ((k4 & 0x3) == 0) {
				u = (u & 0x3FFF) + (s & 0x600000);
				shade_pow = s >> 23;
				s += ds;
			}
			out[(ptr++)] = (in[((v & 0x3F80) + (u >> 7))] >>> shade_pow);
			u += dv;
			v += du;
		}
	}

	private static void trans_tex1_scanline(int[] out, int[] in, int u, int v,
			int a, int b, int c, int da, int db, int dc, int len, int ptr,
			int s, int ds) {
		if (len <= 0)
			return;
		int end_u = 0;
		int end_v = 0;
		int shade_pow = 0;
		if (c != 0) {
			u = a / c << 7;
			v = b / c << 7;
		}
		if (u < 0)
			u = 0;
		else if (u > 16256)
			u = 16256;
		a += da;
		b += db;
		c += dc;
		if (c != 0) {
			end_u = a / c << 7;
			end_v = b / c << 7;
		}
		if (end_u < 0)
			end_u = 0;
		else if (end_u > 16256)
			end_u = 16256;
		int du = end_u - u >> 4;
		int dv = end_v - v >> 4;
		for (int j4 = len >> 4; j4 > 0; j4--) {
			u += (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			u = (u & 0x3FFF) + (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			u = (u & 0x3FFF) + (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			u = (u & 0x3FFF) + (s & 0x600000);
			shade_pow = s >> 23;
			s += ds;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u = end_u;
			v = end_v;
			a += da;
			b += db;
			c += dc;
			if (c != 0) {
				end_u = a / c << 7;
				end_v = b / c << 7;
			}
			if (end_u < 0)
				end_u = 0;
			else if (end_u > 16256)
				end_u = 16256;
			du = end_u - u >> 4;
			dv = end_v - v >> 4;
		}

		for (int k4 = 0; k4 < (len & 0xF); k4++) {
			if ((k4 & 0x3) == 0) {
				u = (u & 0x3FFF) + (s & 0x600000);
				shade_pow = s >> 23;
				s += ds;
			}
			out[(ptr++)] = ((in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) + (out[ptr] >> 1 & 0x7F7F7F));
			u += du;
			v += dv;
		}
	}

	private static void trans_back_tex1_scanline(int[] out, int texel, int u,
			int v, int[] in, int a, int b, int c, int da, int db, int dc,
			int len, int ptr, int s, int ds) {
		if (len <= 0)
			return;
		int end_u = 0;
		int end_v = 0;
		ds <<= 2;
		if (c != 0) {
			end_u = a / c << 7;
			end_v = b / c << 7;
		}
		if (end_u < 0)
			end_u = 0;
		else if (end_u > 16256)
			end_u = 16256;
		for (int j4 = len; j4 > 0; j4 -= 16) {
			a += da;
			b += db;
			c += dc;
			u = end_u;
			v = end_v;
			if (c != 0) {
				end_u = a / c << 7;
				end_v = b / c << 7;
			}
			if (end_u < 0)
				end_u = 0;
			else if (end_u > 16256)
				end_u = 16256;
			int du = end_u - u >> 4;
			int dv = end_v - v >> 4;
			int shade_pow = s >> 23;
			u += (s & 0x600000);
			s += ds;
			if (j4 < 16) {
				for (int l4 = 0; l4 < j4; l4++) {
					if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
						out[ptr] = texel;
					ptr++;
					u += du;
					v += dv;
					if ((l4 & 0x3) == 3) {
						u = (u & 0x3FFF) + (s & 0x600000);
						shade_pow = s >> 23;
						s += ds;
					}
				}
			} else {
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				u = (u & 0x3FFF) + (s & 0x600000);
				shade_pow = s >> 23;
				s += ds;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				u = (u & 0x3FFF) + (s & 0x600000);
				shade_pow = s >> 23;
				s += ds;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				u = (u & 0x3FFF) + (s & 0x600000);
				shade_pow = s >> 23;
				s += ds;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
				u += du;
				v += dv;
				if ((texel = in[((v & 0x3F80) + (u >> 7))] >>> shade_pow) != 0)
					out[ptr] = texel;
				ptr++;
			}
		}
	}

	private static void tex0_scanline(int[] out, int[] in, int u, int v, int a,
			int b, int c, int da, int db, int dc, int len, int ptr, int s,
			int ds) {
		if (len <= 0)
			return;
		int end_u = 0;
		int end_v = 0;
		ds <<= 2;
		if (c != 0) {
			end_u = a / c << 6;
			end_v = b / c << 6;
		}
		if (end_u < 0)
			end_u = 0;
		else if (end_u > 4032)
			end_u = 4032;
		for (int i4 = len; i4 > 0; i4 -= 16) {
			a += da;
			b += db;
			c += dc;
			u = end_u;
			v = end_v;
			if (c != 0) {
				end_u = a / c << 6;
				end_v = b / c << 6;
			}
			if (end_u < 0)
				end_u = 0;
			else if (end_u > 4032)
				end_u = 4032;
			int du = end_u - u >> 4;
			int dv = end_v - v >> 4;
			int shade_pow = s >> 20;
			u += (s & 0xC0000);
			s += ds;
			if (i4 < 16) {
				for (int k4 = 0; k4 < i4; k4++) {
					out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
					u += du;
					v += dv;
					if ((k4 & 0x3) == 3) {
						u = (u & 0xFFF) + (s & 0xC0000);
						shade_pow = s >> 20;
						s += ds;
					}
				}
			} else {
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
				u += du;
				v += dv;
				out[(ptr++)] = (in[((v & 0xFC0) + (u >> 6))] >>> shade_pow);
			}
		}
	}

	private static void trans_tex0_scanline(int[] out, int[] in, int u, int v,
			int a, int b, int c, int da, int db, int dc, int len, int j2,
			int s, int ds) {
		if (len <= 0)
			return;
		int end_u = 0;
		int end_v = 0;
		ds <<= 2;
		if (c != 0) {
			end_u = a / c << 6;
			end_v = b / c << 6;
		}
		if (end_u < 0)
			end_u = 0;
		else if (end_u > 4032)
			end_u = 4032;
		for (int i4 = len; i4 > 0; i4 -= 16) {
			a += da;
			b += db;
			c += dc;
			u = end_u;
			v = end_v;
			if (c != 0) {
				end_u = a / c << 6;
				end_v = b / c << 6;
			}
			if (end_u < 0)
				end_u = 0;
			else if (end_u > 4032)
				end_u = 4032;
			int du = end_u - u >> 4;
			int dv = end_v - v >> 4;
			int shade_pow = s >> 20;
			u += (s & 0xC0000);
			s += ds;
			if (i4 < 16) {
				for (int k4 = 0; k4 < i4; k4++) {
					out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
					u += du;
					v += dv;
					if ((k4 & 0x3) == 3) {
						u = (u & 0xFFF) + (s & 0xC0000);
						shade_pow = s >> 20;
						s += ds;
					}
				}
			} else {
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
				u += du;
				v += dv;
				out[(j2++)] = ((in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) + (out[j2] >> 1 & 0x7F7F7F));
			}
		}
	}

	private static void trans_back_tex0_scanline(int[] out, int u, int i,
			int v, int[] in, int a, int b, int c, int da, int db, int dc,
			int len, int ptr, int s, int ds) {
		if (len <= 0)
			return;
		int end_u = 0;
		int end_v = 0;
		ds <<= 2;
		if (c != 0) {
			end_u = a / c << 6;
			end_v = b / c << 6;
		}
		if (end_u < 0)
			end_u = 0;
		else if (end_u > 4032)
			end_u = 4032;
		for (int j4 = len; j4 > 0; j4 -= 16) {
			a += da;
			b += db;
			c += dc;
			u = end_u;
			v = end_v;
			if (c != 0) {
				end_u = a / c << 6;
				end_v = b / c << 6;
			}
			if (end_u < 0)
				end_u = 0;
			else if (end_u > 4032)
				end_u = 4032;
			int du = end_u - u >> 4;
			int dv = end_v - v >> 4;
			int shade_pow = s >> 20;
			u += (s & 0xC0000);
			s += ds;
			if (j4 < 16) {
				for (int l4 = 0; l4 < j4; l4++) {
					if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
						out[ptr] = i;
					ptr++;
					u += du;
					v += dv;
					if ((l4 & 0x3) == 3) {
						u = (u & 0xFFF) + (s & 0xC0000);
						shade_pow = s >> 20;
						s += ds;
					}
				}
			} else {
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				u = (u & 0xFFF) + (s & 0xC0000);
				shade_pow = s >> 20;
				s += ds;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
				u += du;
				v += dv;
				if ((i = in[((v & 0xFC0) + (u >> 6))] >>> shade_pow) != 0)
					out[ptr] = i;
				ptr++;
			}
		}
	}

	private static void grad0_scanline(int[] out, int len, int ptr, int col,
			int[] ramp, int r, int dr) {
		if (len >= 0)
			return;
		dr <<= 1;
		col = ramp[(r >> 8 & 0xFF)];
		r += dr;
		int j1 = len / 8;
		for (int k1 = j1; k1 < 0; k1++) {
			out[(ptr++)] = col;
			out[(ptr++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(ptr++)] = col;
			out[(ptr++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(ptr++)] = col;
			out[(ptr++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(ptr++)] = col;
			out[(ptr++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
		}

		j1 = -(len % 8);
		for (int l1 = 0; l1 < j1; l1++) {
			out[(ptr++)] = col;
			if ((l1 & 0x1) == 1) {
				col = ramp[(r >> 8 & 0xFF)];
				r += dr;
			}
		}
	}

	private static void trans_grad1_scanline(int[] out, int len, int ptr,
			int col, int[] ramp, int r, int dr) {
		if (len >= 0)
			return;
		dr <<= 2;
		col = ramp[(r >> 8 & 0xFF)];
		r += dr;
		int j1 = len / 16;
		for (int k1 = j1; k1 < 0; k1++) {
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
		}

		j1 = -(len % 16);
		for (int l1 = 0; l1 < j1; l1++) {
			out[(ptr++)] = (col + (out[ptr] >> 1 & 0x7F7F7F));
			if ((l1 & 0x3) == 3) {
				col = ramp[(r >> 8 & 0xFF)];
				r += dr;
				r += dr;
			}
		}
	}

	private static void grad1_scanline(int[] out, int i, int j, int col,
			int[] ramp, int r, int dr) {
		if (i >= 0)
			return;
		dr <<= 2;
		col = ramp[(r >> 8 & 0xFF)];
		r += dr;
		int j1 = i / 16;
		for (int k1 = j1; k1 < 0; k1++) {
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			out[(j++)] = col;
			col = ramp[(r >> 8 & 0xFF)];
			r += dr;
		}

		j1 = -(i % 16);
		for (int l1 = 0; l1 < j1; l1++) {
			out[(j++)] = col;
			if ((l1 & 0x3) == 3) {
				col = ramp[(r >> 8 & 0xFF)];
				r += dr;
			}
		}
	}

	public void set_camera(int target_x, int target_y, int target_z, int yaw,
			int pitch, int roll, int dist) {
		yaw &= 1023;
		pitch &= 1023;
		roll &= 1023;
		this.cam_yaw = (1024 - yaw & 0x3FF);
		this.cam_pitch = (1024 - pitch & 0x3FF);
		this.cam_roll = (1024 - roll & 0x3FF);
		int dx = 0;
		int dy = 0;
		int dz = dist;
		if (yaw != 0) {
			int sin_yaw = sin2048_cache[yaw];
			int cos_yaw = sin2048_cache[(yaw + 1024)];
			int i4 = dy * cos_yaw - dz * sin_yaw >> 15;
			dz = dy * sin_yaw + dz * cos_yaw >> 15;
			dy = i4;
		}
		if (pitch != 0) {
			int sin_pitch = sin2048_cache[pitch];
			int cos_pitch = sin2048_cache[(pitch + 1024)];
			int j4 = dz * sin_pitch + dx * cos_pitch >> 15;
			dz = dz * cos_pitch - dx * sin_pitch >> 15;
			dx = j4;
		}
		if (roll != 0) {
			int sin_roll = sin2048_cache[roll];
			int cos_roll = sin2048_cache[(roll + 1024)];
			int k4 = dy * sin_roll + dx * cos_roll >> 15;
			dy = dy * cos_roll - dx * sin_roll >> 15;
			dx = k4;
		}
		this.cam_x = (target_x - dx);
		this.cam_y = (target_y - dy);
		this.cam_z = (target_z - dz);
	}

	private void init_poly_3d(int i) {
		Polygon poly = this.polys[i];
		Model model = poly.model;
		int j = poly.face;
		int[] ai = model.face_verts[j];
		int k = model.face_vert_cnt[j];
		int l = model.norm_scale[j];
		int a_x = model.project_vert_x[ai[0]];
		int a_y = model.project_vert_y[ai[0]];
		int a_z = model.project_vert_z[ai[0]];
		int b_x = model.project_vert_x[ai[1]] - a_x;
		int b_y = model.project_vert_y[ai[1]] - a_y;
		int b_z = model.project_vert_z[ai[1]] - a_z;
		int c_x = model.project_vert_x[ai[2]] - a_x;
		int c_y = model.project_vert_y[ai[2]] - a_y;
		int c_z = model.project_vert_z[ai[2]] - a_z;
		int norm_x = b_y * c_z - c_y * b_z;
		int norm_y = b_z * c_x - c_z * b_x;
		int norm_z = b_x * c_y - c_x * b_y;
		if (l == -1) {
			l = 0;
			for (; (norm_x > 25000) || (norm_y > 25000) || (norm_z > 25000)
					|| (norm_x < -25000) || (norm_y < -25000)
					|| (norm_z < -25000); norm_z >>= 1) {
				l++;
				norm_x >>= 1;
				norm_y >>= 1;
			}

			model.norm_scale[j] = l;
			model.norm_mag[j] = ((int) (this.anInt126 * Math.sqrt(norm_x
					* norm_x + norm_y * norm_y + norm_z * norm_z)));
		} else {
			norm_x >>= l;
			norm_y >>= l;
			norm_z >>= l;
		}
		poly.visibility = (a_x * norm_x + a_y * norm_y + a_z * norm_z);
		poly.norm_x = norm_x;
		poly.norm_y = norm_y;
		poly.norm_z = norm_z;
		int min_z = model.project_vert_z[ai[0]];
		int max_z = min_z;
		int min_plane_x = model.project_plane_x[ai[0]];
		int max_plane_x = min_plane_x;
		int min_plane_y = model.project_plane_y[ai[0]];
		int max_plane_y = min_plane_y;
		for (int l5 = 1; l5 < k; l5++) {
			int i1 = model.project_vert_z[ai[l5]];
			if (i1 > max_z)
				max_z = i1;
			else if (i1 < min_z)
				min_z = i1;
			i1 = model.project_plane_x[ai[l5]];
			if (i1 > max_plane_x)
				max_plane_x = i1;
			else if (i1 < min_plane_x)
				min_plane_x = i1;
			i1 = model.project_plane_y[ai[l5]];
			if (i1 > max_plane_y)
				max_plane_y = i1;
			else if (i1 < min_plane_y) {
				min_plane_y = i1;
			}
		}
		poly.min_z = min_z;
		poly.max_z = max_z;
		poly.min_plane_x = min_plane_x;
		poly.max_plane_x = max_plane_x;
		poly.min_plane_y = min_plane_y;
		poly.max_plane_y = max_plane_y;
	}

	private void init_poly_2d(int i) {
		Polygon poly = this.polys[i];
		Model model = poly.model;
		int j = poly.face;
		int[] ai = model.face_verts[j];
		int l = 0;
		int i1 = 0;
		int j1 = 1;
		int k1 = model.project_vert_x[ai[0]];
		int l1 = model.project_vert_y[ai[0]];
		int i2 = model.project_vert_z[ai[0]];
		model.norm_mag[j] = 1;
		model.norm_scale[j] = 0;
		poly.visibility = (k1 * l + l1 * i1 + i2 * j1);
		poly.norm_x = l;
		poly.norm_y = i1;
		poly.norm_z = j1;
		int min_z = model.project_vert_z[ai[0]];
		int max_z = min_z;
		int l2 = model.project_plane_x[ai[0]];
		int i3 = l2;
		if (model.project_plane_x[ai[1]] < l2)
			l2 = model.project_plane_x[ai[1]];
		else
			i3 = model.project_plane_x[ai[1]];
		int j3 = model.project_plane_y[ai[1]];
		int k3 = model.project_plane_y[ai[0]];
		int k = model.project_vert_z[ai[1]];
		if (k > max_z)
			max_z = k;
		else if (k < min_z)
			min_z = k;
		k = model.project_plane_x[ai[1]];
		if (k > i3)
			i3 = k;
		else if (k < l2)
			l2 = k;
		k = model.project_plane_y[ai[1]];
		if (k > k3)
			k3 = k;
		else if (k < j3)
			j3 = k;
		poly.min_z = min_z;
		poly.max_z = max_z;
		poly.min_plane_x = (l2 - 20);
		poly.max_plane_x = (i3 + 20);
		poly.min_plane_y = j3;
		poly.max_plane_y = k3;
	}

	private boolean separate(Polygon poly_a, Polygon poly_b) {
		if (poly_a.min_plane_x >= poly_b.max_plane_x)
			return true;
		if (poly_b.min_plane_x >= poly_a.max_plane_x)
			return true;
		if (poly_a.min_plane_y >= poly_b.max_plane_y)
			return true;
		if (poly_b.min_plane_y >= poly_a.max_plane_y)
			return true;
		if (poly_a.min_z >= poly_b.max_z)
			return true;
		if (poly_b.min_z > poly_a.max_z)
			return false;
		Model model_a = poly_a.model;
		Model model_b = poly_b.model;
		int face_a = poly_a.face;
		int face_b = poly_b.face;
		int[] verts_a = model_a.face_verts[face_a];
		int[] verts_b = model_b.face_verts[face_b];
		int vert_cnt_a = model_a.face_vert_cnt[face_a];
		int vert_cnt_b = model_b.face_vert_cnt[face_b];
		int proj_v_x = model_b.project_vert_x[verts_b[0]];
		int proj_v_y = model_b.project_vert_y[verts_b[0]];
		int proj_v_z = model_b.project_vert_z[verts_b[0]];
		int norm_x = poly_b.norm_x;
		int norm_y = poly_b.norm_y;
		int norm_z = poly_b.norm_z;
		int norm_mag = model_b.norm_mag[face_b];
		int norm_visibility = poly_b.visibility;
		boolean intersect = false;
		for (int k4 = 0; k4 < vert_cnt_a; k4++) {
			int i1 = verts_a[k4];
			int diff = (proj_v_x - model_a.project_vert_x[i1]) * norm_x
					+ (proj_v_y - model_a.project_vert_y[i1]) * norm_y
					+ (proj_v_z - model_a.project_vert_z[i1]) * norm_z;
			if (((diff < -norm_mag) && (norm_visibility < 0))
					|| ((diff > norm_mag) && (norm_visibility > 0))) {
				intersect = true;
				break;
			}
		}
		if (!intersect)
			return true;
		proj_v_x = model_a.project_vert_x[verts_a[0]];
		proj_v_y = model_a.project_vert_y[verts_a[0]];
		proj_v_z = model_a.project_vert_z[verts_a[0]];
		norm_x = poly_a.norm_x;
		norm_y = poly_a.norm_y;
		norm_z = poly_a.norm_z;
		norm_mag = model_a.norm_mag[face_a];
		norm_visibility = poly_a.visibility;
		intersect = false;
		for (int l4 = 0; l4 < vert_cnt_b; l4++) {
			int j1 = verts_b[l4];
			int diff = (proj_v_x - model_b.project_vert_x[j1]) * norm_x
					+ (proj_v_y - model_b.project_vert_y[j1]) * norm_y
					+ (proj_v_z - model_b.project_vert_z[j1]) * norm_z;
			if (((diff < -norm_mag) && (norm_visibility > 0))
					|| ((diff > norm_mag) && (norm_visibility < 0))) {
				intersect = true;
				break;
			}
		}
		if (!intersect)
			return true;
		int[] a_x;
		int[] a_y;
		if (vert_cnt_a == 2) {
			a_x = new int[4];
			a_y = new int[4];
			int i5 = verts_a[0];
			int k1 = verts_a[1];
			a_x[0] = (model_a.project_plane_x[i5] - 20);
			a_x[1] = (model_a.project_plane_x[k1] - 20);
			a_x[2] = (model_a.project_plane_x[k1] + 20);
			a_x[3] = (model_a.project_plane_x[i5] + 20);
			int tmp595_594 = model_a.project_plane_y[i5];
			a_y[3] = tmp595_594;
			a_y[0] = tmp595_594;
			int tmp611_610 = model_a.project_plane_y[k1];
			a_y[2] = tmp611_610;
			a_y[1] = tmp611_610;
		} else {
			a_x = new int[vert_cnt_a];
			a_y = new int[vert_cnt_a];
			for (int j5 = 0; j5 < vert_cnt_a; j5++) {
				int i6 = verts_a[j5];
				a_x[j5] = model_a.project_plane_x[i6];
				a_y[j5] = model_a.project_plane_y[i6];
			}
		}
		int[] b_x;
		int[] b_y;
		if (vert_cnt_b == 2) {
			b_x = new int[4];
			b_y = new int[4];
			int k5 = verts_b[0];
			int l1 = verts_b[1];
			b_x[0] = (model_b.project_plane_x[k5] - 20);
			b_x[1] = (model_b.project_plane_x[l1] - 20);
			b_x[2] = (model_b.project_plane_x[l1] + 20);
			b_x[3] = (model_b.project_plane_x[k5] + 20);
			int tmp778_777 = model_b.project_plane_y[k5];
			b_y[3] = tmp778_777;
			b_y[0] = tmp778_777;
			int tmp795_794 = model_b.project_plane_y[l1];
			b_y[2] = tmp795_794;
			b_y[1] = tmp795_794;
		} else {
			b_x = new int[vert_cnt_b];
			b_y = new int[vert_cnt_b];
			for (int l5 = 0; l5 < vert_cnt_b; l5++) {
				int j6 = verts_b[l5];
				b_x[l5] = model_b.project_plane_x[j6];
				b_y[l5] = model_b.project_plane_y[j6];
			}
		}

		return !intersect(a_x, a_y, b_x, b_y);
	}

	private boolean heuristic(Polygon poly_a, Polygon poly_b) {
		Model class7 = poly_a.model;
		Model class7_1 = poly_b.model;
		int i = poly_a.face;
		int j = poly_b.face;
		int[] ai = class7.face_verts[i];
		int[] ai1 = class7_1.face_verts[j];
		int k = class7.face_vert_cnt[i];
		int l = class7_1.face_vert_cnt[j];
		int i2 = class7_1.project_vert_x[ai1[0]];
		int j2 = class7_1.project_vert_y[ai1[0]];
		int k2 = class7_1.project_vert_z[ai1[0]];
		int l2 = poly_b.norm_x;
		int i3 = poly_b.norm_y;
		int j3 = poly_b.norm_z;
		int k3 = class7_1.norm_mag[j];
		int l3 = poly_b.visibility;
		boolean intersect = false;
		for (int i4 = 0; i4 < k; i4++) {
			int i1 = ai[i4];
			int k1 = (i2 - class7.project_vert_x[i1]) * l2
					+ (j2 - class7.project_vert_y[i1]) * i3
					+ (k2 - class7.project_vert_z[i1]) * j3;
			if (((k1 < -k3) && (l3 < 0)) || ((k1 > k3) && (l3 > 0))) {
				intersect = true;
				break;
			}
		}
		if (!intersect)
			return true;
		i2 = class7.project_vert_x[ai[0]];
		j2 = class7.project_vert_y[ai[0]];
		k2 = class7.project_vert_z[ai[0]];
		l2 = poly_a.norm_x;
		i3 = poly_a.norm_y;
		j3 = poly_a.norm_z;
		k3 = class7.norm_mag[i];
		l3 = poly_a.visibility;
		intersect = false;
		for (int j4 = 0; j4 < l; j4++) {
			int j1 = ai1[j4];
			int l1 = (i2 - class7_1.project_vert_x[j1]) * l2
					+ (j2 - class7_1.project_vert_y[j1]) * i3
					+ (k2 - class7_1.project_vert_z[j1]) * j3;
			if (((l1 < -k3) && (l3 > 0)) || ((l1 > k3) && (l3 < 0))) {
				intersect = true;
				break;
			}
		}
		return !intersect;
	}

	public void textures_allocate(int i, int j, int k) {
		this.texture_cnt = i;
		this.texture_raster = new byte[i][];
		this.texture_palette = new int[i][];
		this.texture_dim = new int[i];
		this.texture_id = new long[i];
		this.texture_trans_back = new boolean[i];
		this.texture_pixels = new int[i][];
		next_id = 0L;
		this.pool64 = new int[j][];
		this.pool256 = new int[k][];
	}

	public void texture_define(int i, byte[] abyte0, int[] ai, int j) {
		this.texture_raster[i] = abyte0;
		this.texture_palette[i] = ai;
		this.texture_dim[i] = j;
		this.texture_id[i] = 0L;
		this.texture_trans_back[i] = false;
		this.texture_pixels[i] = null;
		texture_prepare(i);
	}

	public void texture_prepare(int i) {
		if (i < 0)
			return;
		this.texture_id[i] = (next_id++);
		if (this.texture_pixels[i] != null)
			return;
		if (this.texture_dim[i] == 0) {
			for (int j = 0; j < this.pool64.length; j++) {
				if (this.pool64[j] == null) {
					this.pool64[j] = new int[16384];
					this.texture_pixels[i] = this.pool64[j];
					texture_resolve(i);
					return;
				}
			}
			long l = 1073741824L;
			int i1 = 0;
			for (int k1 = 0; k1 < this.texture_cnt; k1++) {
				if ((k1 != i) && (this.texture_dim[k1] == 0)
						&& (this.texture_pixels[k1] != null)
						&& (this.texture_id[k1] < l)) {
					l = this.texture_id[k1];
					i1 = k1;
				}
			}
			this.texture_pixels[i] = this.texture_pixels[i1];
			this.texture_pixels[i1] = null;
			texture_resolve(i);
			return;
		}
		for (int k = 0; k < this.pool256.length; k++) {
			if (this.pool256[k] == null) {
				this.pool256[k] = new int[65536];
				this.texture_pixels[i] = this.pool256[k];
				texture_resolve(i);
				return;
			}
		}
		long l1 = 1073741824L;
		int j1 = 0;
		for (int i2 = 0; i2 < this.texture_cnt; i2++) {
			if ((i2 != i) && (this.texture_dim[i2] == 1)
					&& (this.texture_pixels[i2] != null)
					&& (this.texture_id[i2] < l1)) {
				l1 = this.texture_id[i2];
				j1 = i2;
			}
		}
		this.texture_pixels[i] = this.texture_pixels[j1];
		this.texture_pixels[j1] = null;
		texture_resolve(i);
	}

	private void texture_resolve(int i) {
		char c;
		if (this.texture_dim[i] == 0)
			c = '@';
		else
			c = '\200';
		int[] ai = this.texture_pixels[i];
		int j = 0;
		for (int k = 0; k < c; k++) {
			for (int l = 0; l < c; l++) {
				int j1 = this.texture_palette[i][(this.texture_raster[i][(l + k
						* c)] & 0xFF)];
				j1 &= 16316671;
				if (j1 == 0) {
					j1 = 1;
				} else if (j1 == 16253183) {
					j1 = 0;
					this.texture_trans_back[i] = true;
				}
				ai[(j++)] = j1;
			}

		}

		for (int i1 = 0; i1 < j; i1++) {
			int k1 = ai[i1];
			ai[(j + i1)] = (k1 - (k1 >>> 3) & 0xF8F8FF);
			ai[(j * 2 + i1)] = (k1 - (k1 >>> 2) & 0xF8F8FF);
			ai[(j * 3 + i1)] = (k1 - (k1 >>> 2) - (k1 >>> 3) & 0xF8F8FF);
		}
	}

	public void method228(int i) {
		if (this.texture_pixels[i] == null)
			return;
		int[] ai = this.texture_pixels[i];
		for (int j = 0; j < 64; j++) {
			int k = j + 4032;
			int l = ai[k];
			for (int j1 = 0; j1 < 63; j1++) {
				ai[k] = ai[(k - 64)];
				k -= 64;
			}

			this.texture_pixels[i][k] = l;
		}

		char c = '\u1000';
		for (int i1 = 0; i1 < c; i1++) {
			int k1 = ai[i1];
			ai[(c + i1)] = (k1 - (k1 >>> 3) & 0xF8F8FF);
			ai[(c * '\002' + i1)] = (k1 - (k1 >>> 2) & 0xF8F8FF);
			ai[(c * '\003' + i1)] = (k1 - (k1 >>> 2) - (k1 >>> 3) & 0xF8F8FF);
		}
	}

	public int method229(int i) {
		if (i == 12345678)
			return 0;
		texture_prepare(i);
		if (i >= 0)
			return this.texture_pixels[i][0];
		if (i < 0) {
			i = -(i + 1);
			int j = i >> 10 & 0x1F;
			int k = i >> 5 & 0x1F;
			int l = i & 0x1F;
			return (j << 19) + (k << 11) + (l << 3);
		}
		return 0;
	}

	public void set_light(int i, int j, int k) {
		if ((i == 0) && (j == 0) && (k == 0))
			i = 32;
		for (int l = 0; l < this.model_cnt; l++)
			this.models[l].set_light(i, j, k);
	}

	public void set_light(int ambient, int falloff, int x, int y, int z) {
		set_light(false, ambient, falloff, x, y, z);
	}

	public void set_light(boolean gourad, int ambient, int falloff, int x, int y, int z) {
		if ((x == 0) && (y == 0) && (z == 0))
			x = 32;
		for (int i = 0; i < model_cnt; i++)
			models[i].set_light(gourad, ambient, falloff, x, y, z);
	}

	
	public static int rgb(int i, int j, int k) {
		return -1 - i / 8 * 1024 - j / 8 * 32 - k / 8;
	}

	public int method233(int a_x1, int a_y1, int a_x0, int a_y0, int b_y0) {
		if (a_y0 == a_y1) {
			return a_x1;
		}
		return a_x1 + (a_x0 - a_x1) * (b_y0 - a_y1) / (a_y0 - a_y1);
	}

	public boolean method234(int i, int j, int k, int l, boolean flag) {
		if (((flag) && (i <= k)) || (i < k)) {
			if (i > l)
				return true;
			if (j > k)
				return true;
			if (j > l)
				return true;
			return !flag;
		}
		if (i < l)
			return true;
		if (j < k)
			return true;
		if (j < l) {
			return true;
		}
		return flag;
	}

	public boolean method235(int i, int j, int k, boolean flag) {
		if (((flag) && (i <= k)) || (i < k)) {
			if (j > k)
				return true;
			return !flag;
		}
		if (j < k) {
			return true;
		}
		return flag;
	}

	public boolean intersect(int[] a_x, int[] a_y, int[] b_x, int[] b_y) {
		int cnt_a = a_x.length;
		int cnt_b = b_x.length;
		byte state = 0;
		int min_y_a;
		int max_y_a = min_y_a = a_y[0];
		int min_y_idx_a = 0;
		int min_y_b;
		int max_y_b = min_y_b = b_y[0];
		int min_y_idx_b = 0;
		for (int ptr = 1; ptr < cnt_a; ptr++)
			if (a_y[ptr] < min_y_a) {
				min_y_a = a_y[ptr];
				min_y_idx_a = ptr;
			} else if (a_y[ptr] > max_y_a) {
				max_y_a = a_y[ptr];
			}
		for (int ptr = 1; ptr < cnt_b; ptr++)
			if (b_y[ptr] < min_y_b) {
				min_y_b = b_y[ptr];
				min_y_idx_b = ptr;
			} else if (b_y[ptr] > max_y_b) {
				max_y_b = b_y[ptr];
			}
		if (min_y_b >= max_y_a)
			return false;
		if (min_y_a >= max_y_b)
			return false;
		int idx_b;
		boolean flag;
		int idx_a;
		if (a_y[min_y_idx_a] < b_y[min_y_idx_b]) {
			for (idx_a = min_y_idx_a; a_y[idx_a] < b_y[min_y_idx_b]; idx_a = (idx_a + 1)
					% cnt_a)
				;
			while (a_y[min_y_idx_a] < b_y[min_y_idx_b])
				min_y_idx_a = (min_y_idx_a - 1 + cnt_a) % cnt_a;
			int k1 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
					a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
					a_y[min_y_idx_a], b_y[min_y_idx_b]);
			int k6 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
					a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a], a_y[idx_a],
					b_y[min_y_idx_b]);
			int l10 = b_x[min_y_idx_b];
			flag = (k1 < l10) | (k6 < l10);
			if (method235(k1, k6, l10, flag))
				return true;
			idx_b = (min_y_idx_b + 1) % cnt_b;
			min_y_idx_b = (min_y_idx_b - 1 + cnt_b) % cnt_b;
			if (min_y_idx_a == idx_a)
				state = 1;
		} else {
			for (idx_b = min_y_idx_b; b_y[idx_b] < a_y[min_y_idx_a]; idx_b = (idx_b + 1)
					% cnt_b)
				;
			while (b_y[min_y_idx_b] < a_y[min_y_idx_a])
				min_y_idx_b = (min_y_idx_b - 1 + cnt_b) % cnt_b;
			int l1 = a_x[min_y_idx_a];
			int i11 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
					b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
					b_y[min_y_idx_b], a_y[min_y_idx_a]);
			int l15 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
					b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b], b_y[idx_b],
					a_y[min_y_idx_a]);
			flag = (l1 < i11) | (l1 < l15);
			if (method235(i11, l15, l1, !flag))
				return true;
			idx_a = (min_y_idx_a + 1) % cnt_a;
			min_y_idx_a = (min_y_idx_a - 1 + cnt_a) % cnt_a;
			if (min_y_idx_b == idx_b)
				state = 2;
		}
		while (state == 0)
			if (a_y[min_y_idx_a] < a_y[idx_a]) {
				if (a_y[min_y_idx_a] < b_y[min_y_idx_b]) {
					if (a_y[min_y_idx_a] < b_y[idx_b]) {
						int i2 = a_x[min_y_idx_a];
						int l6 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
								a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
								a_y[idx_a], a_y[min_y_idx_a]);
						int j11 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
								b_y[((min_y_idx_b + 1) % cnt_b)],
								b_x[min_y_idx_b], b_y[min_y_idx_b],
								a_y[min_y_idx_a]);
						int i16 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
								b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
								b_y[idx_b], a_y[min_y_idx_a]);
						if (method234(i2, l6, j11, i16, flag))
							return true;
						min_y_idx_a = (min_y_idx_a - 1 + cnt_a) % cnt_a;
						if (min_y_idx_a == idx_a)
							state = 1;
					} else {
						int j2 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
								a_y[((min_y_idx_a + 1) % cnt_a)],
								a_x[min_y_idx_a], a_y[min_y_idx_a], b_y[idx_b]);
						int i7 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
								a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
								a_y[idx_a], b_y[idx_b]);
						int k11 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
								b_y[((min_y_idx_b + 1) % cnt_b)],
								b_x[min_y_idx_b], b_y[min_y_idx_b], b_y[idx_b]);
						int j16 = b_x[idx_b];
						if (method234(j2, i7, k11, j16, flag))
							return true;
						idx_b = (idx_b + 1) % cnt_b;
						if (min_y_idx_b == idx_b)
							state = 2;
					}
				} else if (b_y[min_y_idx_b] < b_y[idx_b]) {
					int k2 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
							a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
							a_y[min_y_idx_a], b_y[min_y_idx_b]);
					int j7 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
							a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
							a_y[idx_a], b_y[min_y_idx_b]);
					int l11 = b_x[min_y_idx_b];
					int k16 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
							b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
							b_y[idx_b], b_y[min_y_idx_b]);
					if (method234(k2, j7, l11, k16, flag))
						return true;
					min_y_idx_b = (min_y_idx_b - 1 + cnt_b) % cnt_b;
					if (min_y_idx_b == idx_b)
						state = 2;
				} else {
					int l2 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
							a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
							a_y[min_y_idx_a], b_y[idx_b]);
					int k7 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
							a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
							a_y[idx_a], b_y[idx_b]);
					int i12 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
							b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
							b_y[min_y_idx_b], b_y[idx_b]);
					int l16 = b_x[idx_b];
					if (method234(l2, k7, i12, l16, flag))
						return true;
					idx_b = (idx_b + 1) % cnt_b;
					if (min_y_idx_b == idx_b)
						state = 2;
				}
			} else if (a_y[idx_a] < b_y[min_y_idx_b]) {
				if (a_y[idx_a] < b_y[idx_b]) {
					int i3 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
							a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
							a_y[min_y_idx_a], a_y[idx_a]);
					int l7 = a_x[idx_a];
					int j12 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
							b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
							b_y[min_y_idx_b], a_y[idx_a]);
					int i17 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
							b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
							b_y[idx_b], a_y[idx_a]);
					if (method234(i3, l7, j12, i17, flag))
						return true;
					idx_a = (idx_a + 1) % cnt_a;
					if (min_y_idx_a == idx_a)
						state = 1;
				} else {
					int j3 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
							a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
							a_y[min_y_idx_a], b_y[idx_b]);
					int i8 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
							a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
							a_y[idx_a], b_y[idx_b]);
					int k12 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
							b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
							b_y[min_y_idx_b], b_y[idx_b]);
					int j17 = b_x[idx_b];
					if (method234(j3, i8, k12, j17, flag))
						return true;
					idx_b = (idx_b + 1) % cnt_b;
					if (min_y_idx_b == idx_b)
						state = 2;
				}
			} else if (b_y[min_y_idx_b] < b_y[idx_b]) {
				int k3 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
						a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
						a_y[min_y_idx_a], b_y[min_y_idx_b]);
				int j8 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
						a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
						a_y[idx_a], b_y[min_y_idx_b]);
				int l12 = b_x[min_y_idx_b];
				int k17 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
						b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
						b_y[idx_b], b_y[min_y_idx_b]);
				if (method234(k3, j8, l12, k17, flag))
					return true;
				min_y_idx_b = (min_y_idx_b - 1 + cnt_b) % cnt_b;
				if (min_y_idx_b == idx_b)
					state = 2;
			} else {
				int l3 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
						a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
						a_y[min_y_idx_a], b_y[idx_b]);
				int k8 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
						a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
						a_y[idx_a], b_y[idx_b]);
				int i13 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
						b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
						b_y[min_y_idx_b], b_y[idx_b]);
				int l17 = b_x[idx_b];
				if (method234(l3, k8, i13, l17, flag))
					return true;
				idx_b = (idx_b + 1) % cnt_b;
				if (min_y_idx_b == idx_b)
					state = 2;
			}
		while (state == 1)
			if (a_y[min_y_idx_a] < b_y[min_y_idx_b]) {
				if (a_y[min_y_idx_a] < b_y[idx_b]) {
					int i4 = a_x[min_y_idx_a];
					int j13 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
							b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
							b_y[min_y_idx_b], a_y[min_y_idx_a]);
					int i18 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
							b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
							b_y[idx_b], a_y[min_y_idx_a]);
					return method235(j13, i18, i4, !flag);
				}
				int j4 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
						a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
						a_y[min_y_idx_a], b_y[idx_b]);
				int l8 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
						a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
						a_y[idx_a], b_y[idx_b]);
				int k13 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
						b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
						b_y[min_y_idx_b], b_y[idx_b]);
				int j18 = b_x[idx_b];
				if (method234(j4, l8, k13, j18, flag))
					return true;
				idx_b = (idx_b + 1) % cnt_b;
				if (min_y_idx_b == idx_b)
					state = 0;
			} else if (b_y[min_y_idx_b] < b_y[idx_b]) {
				int k4 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
						a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
						a_y[min_y_idx_a], b_y[min_y_idx_b]);
				int i9 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
						a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
						a_y[idx_a], b_y[min_y_idx_b]);
				int l13 = b_x[min_y_idx_b];
				int k18 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
						b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
						b_y[idx_b], b_y[min_y_idx_b]);
				if (method234(k4, i9, l13, k18, flag))
					return true;
				min_y_idx_b = (min_y_idx_b - 1 + cnt_b) % cnt_b;
				if (min_y_idx_b == idx_b)
					state = 0;
			} else {
				int l4 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
						a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
						a_y[min_y_idx_a], b_y[idx_b]);
				int j9 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
						a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
						a_y[idx_a], b_y[idx_b]);
				int i14 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
						b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
						b_y[min_y_idx_b], b_y[idx_b]);
				int l18 = b_x[idx_b];
				if (method234(l4, j9, i14, l18, flag))
					return true;
				idx_b = (idx_b + 1) % cnt_b;
				if (min_y_idx_b == idx_b)
					state = 0;
			}
		while (state == 2)
			if (b_y[min_y_idx_b] < a_y[min_y_idx_a]) {
				if (b_y[min_y_idx_b] < a_y[idx_a]) {
					int i5 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
							a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
							a_y[min_y_idx_a], b_y[min_y_idx_b]);
					int k9 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
							a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
							a_y[idx_a], b_y[min_y_idx_b]);
					int j14 = b_x[min_y_idx_b];
					return method235(i5, k9, j14, flag);
				}
				int j5 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
						a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
						a_y[min_y_idx_a], a_y[idx_a]);
				int l9 = a_x[idx_a];
				int k14 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
						b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
						b_y[min_y_idx_b], a_y[idx_a]);
				int i19 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
						b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
						b_y[idx_b], a_y[idx_a]);
				if (method234(j5, l9, k14, i19, flag))
					return true;
				idx_a = (idx_a + 1) % cnt_a;
				if (min_y_idx_a == idx_a)
					state = 0;
			} else if (a_y[min_y_idx_a] < a_y[idx_a]) {
				int k5 = a_x[min_y_idx_a];
				int i10 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
						a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a],
						a_y[idx_a], a_y[min_y_idx_a]);
				int l14 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
						b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
						b_y[min_y_idx_b], a_y[min_y_idx_a]);
				int j19 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
						b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
						b_y[idx_b], a_y[min_y_idx_a]);
				if (method234(k5, i10, l14, j19, flag))
					return true;
				min_y_idx_a = (min_y_idx_a - 1 + cnt_a) % cnt_a;
				if (min_y_idx_a == idx_a)
					state = 0;
			} else {
				int l5 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
						a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
						a_y[min_y_idx_a], a_y[idx_a]);
				int j10 = a_x[idx_a];
				int i15 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
						b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
						b_y[min_y_idx_b], a_y[idx_a]);
				int k19 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
						b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b],
						b_y[idx_b], a_y[idx_a]);
				if (method234(l5, j10, i15, k19, flag))
					return true;
				idx_a = (idx_a + 1) % cnt_a;
				if (min_y_idx_a == idx_a)
					state = 0;
			}
		if (a_y[min_y_idx_a] < b_y[min_y_idx_b]) {
			int i6 = a_x[min_y_idx_a];
			int j15 = method233(b_x[((min_y_idx_b + 1) % cnt_b)],
					b_y[((min_y_idx_b + 1) % cnt_b)], b_x[min_y_idx_b],
					b_y[min_y_idx_b], a_y[min_y_idx_a]);
			int l19 = method233(b_x[((idx_b - 1 + cnt_b) % cnt_b)],
					b_y[((idx_b - 1 + cnt_b) % cnt_b)], b_x[idx_b], b_y[idx_b],
					a_y[min_y_idx_a]);
			return method235(j15, l19, i6, !flag);
		}
		int j6 = method233(a_x[((min_y_idx_a + 1) % cnt_a)],
				a_y[((min_y_idx_a + 1) % cnt_a)], a_x[min_y_idx_a],
				a_y[min_y_idx_a], b_y[min_y_idx_b]);
		int k10 = method233(a_x[((idx_a - 1 + cnt_a) % cnt_a)],
				a_y[((idx_a - 1 + cnt_a) % cnt_a)], a_x[idx_a], a_y[idx_a],
				b_y[min_y_idx_b]);
		int k15 = b_x[min_y_idx_b];
		return method235(j6, k10, k15, flag);
	}
}