package jagex.client;

import jagex.DataUtil;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class Model {
	public static final int use_gourad_shade = 0;
	public static final int use_phong_shade = 1;
	public static final int use_flat_shade = 2;

	public int vert_cnt;
	public int[] project_vert_x;
	public int[] project_vert_y;
	public int[] project_vert_z;
	public int[] project_plane_x;
	public int[] project_plane_y;
	public int[] vert_shade;
	public byte[] vert_ambient;
	public int face_cnt;
	public int[] face_vert_cnt;
	public int[][] face_verts;
	public int[] texture_front;
	public int[] texture_back;
	public int[] norm_mag;
	public int[] norm_scale;
	public int[] face_shade;
	public int light_type;
	public int[] face_norm_x;
	public int[] face_norm_y;
	public int[] face_norm_z;
	public int anInt310;
	public int depth;
	public int trans_state;
	public boolean visible;
	public int min_x;
	public int max_x;
	public int min_y;
	public int max_y;
	public int min_z;
	public int max_z;
	public boolean aBoolean320;
	public boolean trans_texture;
	public boolean trans;
	public int anInt323;
	public int[] face_pick_tag;
	public byte[] is_local_player;
	private boolean autocommit;
	public boolean no_bounds;
	public boolean no_shading;
	public boolean unpickable;
	public boolean unrendered;
	private static int[] sin256_cache = new int[512];
	private static int[] sin1024_cache = new int[2048];
	private static byte[] enc_tab = new byte[64];
	private static int[] dec_tab = new int[256];
	public int max_vert_cnt;
	public int[] vert_x;
	public int[] vert_y;
	public int[] vert_z;
	public int[] trans_vert_x;
	public int[] trans_vert_y;
	public int[] trans_vert_z;
	private int max_face_cnt;
	private int[][] anIntArrayArray345;
	private int[] face_min_x;
	private int[] face_max_x;
	private int[] face_min_y;
	private int[] face_max_y;
	private int[] face_min_z;
	private int[] face_max_z;
	private int base_x;
	private int base_y;
	private int base_z;
	private int yaw;
	private int pitch;
	private int roll;
	private int scale_x;
	private int scale_y;
	private int scale_z;
	private int shear_xy;
	private int shear_zy;
	private int shear_xz;
	private int shear_yz;
	private int shear_zx;
	private int shear_yx;
	private int trans_type;
	private int max_dim;
	private int light_x;
	private int light_y;
	private int light_z;
	private int light_mag;
	protected int light_falloff;
	protected int light_ambient;
	private byte[] aByteArray375;
	private int buf_ptr;

	public Model(int vert_cnt, int face_cnt) {
		this.trans_state = 1;
		this.visible = true;
		this.aBoolean320 = true;
		this.trans_texture = false;
		this.trans = false;
		this.anInt323 = -1;
		this.autocommit = false;
		this.no_bounds = false;
		this.no_shading = false;
		this.unpickable = false;
		this.unrendered = false;
		this.max_dim = 12345678;
		this.light_x = 180;
		this.light_y = 155;
		this.light_z = 95;
		this.light_mag = 256;
		this.light_falloff = 512;
		this.light_ambient = 32;
		allocate(vert_cnt, face_cnt);
		this.anIntArrayArray345 = new int[face_cnt][1];
		for (int k = 0; k < face_cnt; k++)
			this.anIntArrayArray345[k][0] = k;
	}

	public Model(int i, int j, boolean flag, boolean flag1, boolean flag2,
			boolean flag3, boolean flag4) {
		this.trans_state = 1;
		this.visible = true;
		this.aBoolean320 = true;
		this.trans_texture = false;
		this.trans = false;
		this.anInt323 = -1;
		this.autocommit = false;
		this.no_bounds = false;
		this.no_shading = false;
		this.unpickable = false;
		this.unrendered = false;
		this.max_dim = 12345678;
		this.light_x = 180;
		this.light_y = 155;
		this.light_z = 95;
		this.light_mag = 256;
		this.light_falloff = 512;
		this.light_ambient = 32;
		this.autocommit = flag;
		this.no_bounds = flag1;
		this.no_shading = flag2;
		this.unpickable = flag3;
		this.unrendered = flag4;
		allocate(i, j);
	}

	private void allocate(int vert_cnt, int face_cnt) {
		this.vert_x = new int[vert_cnt];
		this.vert_y = new int[vert_cnt];
		this.vert_z = new int[vert_cnt];
		this.vert_shade = new int[vert_cnt];
		this.vert_ambient = new byte[vert_cnt];
		this.face_vert_cnt = new int[face_cnt];
		this.face_verts = new int[face_cnt][];
		this.texture_front = new int[face_cnt];
		this.texture_back = new int[face_cnt];
		this.face_shade = new int[face_cnt];
		this.light_type = use_flat_shade;
		this.norm_scale = new int[face_cnt];
		this.norm_mag = new int[face_cnt];
		if (!this.unrendered) {
			this.project_vert_x = new int[vert_cnt];
			this.project_vert_y = new int[vert_cnt];
			this.project_vert_z = new int[vert_cnt];
			this.project_plane_x = new int[vert_cnt];
			this.project_plane_y = new int[vert_cnt];
		}
		if (!this.unpickable) {
			this.is_local_player = new byte[face_cnt];
			this.face_pick_tag = new int[face_cnt];
		}
		if (this.autocommit) {
			this.trans_vert_x = this.vert_x;
			this.trans_vert_y = this.vert_y;
			this.trans_vert_z = this.vert_z;
		} else {
			this.trans_vert_x = new int[vert_cnt];
			this.trans_vert_y = new int[vert_cnt];
			this.trans_vert_z = new int[vert_cnt];
		}
		if ((!this.no_shading) || (!this.no_bounds)) {
			this.face_norm_x = new int[face_cnt];
			this.face_norm_y = new int[face_cnt];
			this.face_norm_z = new int[face_cnt];
		}
		if (!this.no_bounds) {
			this.face_min_x = new int[face_cnt];
			this.face_max_x = new int[face_cnt];
			this.face_min_y = new int[face_cnt];
			this.face_max_y = new int[face_cnt];
			this.face_min_z = new int[face_cnt];
			this.face_max_z = new int[face_cnt];
		}
		this.face_cnt = 0;
		this.vert_cnt = 0;
		this.max_vert_cnt = vert_cnt;
		this.max_face_cnt = face_cnt;
		this.base_x = (this.base_y = this.base_z = 0);
		this.yaw = (this.pitch = this.roll = 0);
		this.scale_x = (this.scale_y = this.scale_z = 256);
		this.shear_xy = (this.shear_zy = this.shear_xz = this.shear_yz = this.shear_zx = this.shear_yx = 256);
		this.trans_type = 0;
	}

	public void project_prepare() {
		this.project_vert_x = new int[this.vert_cnt];
		this.project_vert_y = new int[this.vert_cnt];
		this.project_vert_z = new int[this.vert_cnt];
		this.project_plane_x = new int[this.vert_cnt];
		this.project_plane_y = new int[this.vert_cnt];
	}

	public void clear() {
		this.face_cnt = 0;
		this.vert_cnt = 0;
	}

	public void reduce(int i, int j) {
		this.face_cnt -= i;
		if (this.face_cnt < 0)
			this.face_cnt = 0;
		this.vert_cnt -= j;
		if (this.vert_cnt < 0)
			this.vert_cnt = 0;
	}

	public Model(byte[] buffer, int offset, boolean flag) {
		this.trans_state = 1;
		this.visible = true;
		this.aBoolean320 = true;
		this.trans_texture = false;
		this.trans = false;
		this.anInt323 = -1;
		this.autocommit = false;
		this.no_bounds = false;
		this.no_shading = false;
		this.unpickable = false;
		this.unrendered = false;
		this.max_dim = 12345678;
		this.light_x = 180;
		this.light_y = 155;
		this.light_z = 95;
		this.light_mag = 256;
		this.light_falloff = 512;
		this.light_ambient = 32;
		int verts = DataUtil.short_get(buffer, offset);
		offset += 2;
		int faces = DataUtil.short_get(buffer, offset);
		offset += 2;
		allocate(verts, faces);
		this.anIntArrayArray345 = new int[faces][1];
		for (int l = 0; l < verts; l++) {
			this.vert_x[l] = DataUtil.signed_short_get(buffer, offset);
			offset += 2;
		}

		for (int i = 0; i < verts; i++) {
			this.vert_y[i] = DataUtil.signed_short_get(buffer, offset);
			offset += 2;
		}

		for (int i = 0; i < verts; i++) {
			this.vert_z[i] = DataUtil.signed_short_get(buffer, offset);
			offset += 2;
		}

		this.vert_cnt = verts;

		for (int i = 0; i < faces; i++) {
			this.face_vert_cnt[i] = DataUtil.unsign(buffer[(offset++)]);
		}
		for (int i = 0; i < faces; i++) {
			this.texture_front[i] = DataUtil.signed_short_get(buffer, offset);
			offset += 2;
			if (this.texture_front[i] == 32767) {
				this.texture_front[i] = use_gourad_shade;
			}
		}
		for (int i = 0; i < faces; i++) {
			this.texture_back[i] = DataUtil.signed_short_get(buffer, offset);
			offset += 2;
			if (this.texture_back[i] == 32767) {
				this.texture_back[i] = use_gourad_shade;
			}
		}
		for (int i = 0; i < faces; i++) {
			int shade = buffer[(offset++)] & 0xFF;
			if (shade == 0)
				this.face_shade[i] = 0;
			else {
				this.face_shade[i] = use_gourad_shade;
			}
		}
		for (int i = 0; i < faces; i++) {
			this.face_verts[i] = new int[this.face_vert_cnt[i]];
			for (int j = 0; j < this.face_vert_cnt[i]; j++) {
				if (verts < 256) {
					this.face_verts[i][j] = (buffer[(offset++)] & 0xFF);
				} else {
					this.face_verts[i][j] = DataUtil.short_get(buffer, offset);
					offset += 2;
				}
			}
		}

		this.face_cnt = faces;
		this.trans_state = 1;
	}

	public Model(byte[] abyte0, int i) {
		this.trans_state = 1;
		this.visible = true;
		this.aBoolean320 = true;
		this.trans_texture = false;
		this.trans = false;
		this.anInt323 = -1;
		this.autocommit = false;
		this.no_bounds = false;
		this.no_shading = false;
		this.unpickable = false;
		this.unrendered = false;
		this.max_dim = 12345678;
		this.light_x = 180;
		this.light_y = 155;
		this.light_z = 95;
		this.light_mag = 256;
		this.light_falloff = 512;
		this.light_ambient = 32;
		this.aByteArray375 = abyte0;
		this.buf_ptr = i;
		base64_get(this.aByteArray375);
		int j = base64_get(this.aByteArray375);
		int k = base64_get(this.aByteArray375);
		allocate(j, k);
		this.anIntArrayArray345 = new int[k][];
		for (int l2 = 0; l2 < j; l2++) {
			int l = base64_get(this.aByteArray375);
			int i1 = base64_get(this.aByteArray375);
			int j1 = base64_get(this.aByteArray375);
			vert_get(l, i1, j1);
		}

		for (int i3 = 0; i3 < k; i3++) {
			int k1 = base64_get(this.aByteArray375);
			int l1 = base64_get(this.aByteArray375);
			int i2 = base64_get(this.aByteArray375);
			int j2 = base64_get(this.aByteArray375);
			this.light_falloff = base64_get(this.aByteArray375);
			this.light_ambient = base64_get(this.aByteArray375);
			int k2 = base64_get(this.aByteArray375);
			int[] ai = new int[k1];
			for (int j3 = 0; j3 < k1; j3++) {
				ai[j3] = base64_get(this.aByteArray375);
			}
			int[] ai1 = new int[j2];
			for (int k3 = 0; k3 < j2; k3++) {
				ai1[k3] = base64_get(this.aByteArray375);
			}
			int l3 = face_add(k1, ai, l1, i2);
			this.anIntArrayArray345[i3] = ai1;
			if (k2 == 0)
				this.face_shade[l3] = 0;
			else {
				this.face_shade[l3] = use_gourad_shade;
			}
		}
		this.trans_state = 1;
	}

	public Model(String s) {
		this.trans_state = 1;
		this.visible = true;
		this.aBoolean320 = true;
		this.trans_texture = false;
		this.trans = false;
		this.anInt323 = -1;
		this.autocommit = false;
		this.no_bounds = false;
		this.no_shading = false;
		this.unpickable = false;
		this.unrendered = false;
		this.max_dim = 12345678;
		this.light_x = 180;
		this.light_y = 155;
		this.light_z = 95;
		this.light_mag = 256;
		this.light_falloff = 512;
		this.light_ambient = 32;
		byte[] abyte0 = null;
		try {
			InputStream inputstream = DataUtil.open_stream(s);
			DataInputStream datainputstream = new DataInputStream(inputstream);
			abyte0 = new byte[3];
			this.buf_ptr = 0;
			for (int i = 0; i < 3; i += datainputstream.read(abyte0, i, 3 - i))
				;
			int k = base64_get(abyte0);
			abyte0 = new byte[k];
			this.buf_ptr = 0;
			for (int j = 0; j < k; j += datainputstream.read(abyte0, j, k - j))
				;
			datainputstream.close();
		} catch (IOException _ex) {
			this.vert_cnt = 0;
			this.face_cnt = 0;
			return;
		}
		int l = base64_get(abyte0);
		int i1 = base64_get(abyte0);
		allocate(l, i1);
		this.anIntArrayArray345 = new int[i1][];
		for (int j3 = 0; j3 < l; j3++) {
			int j1 = base64_get(abyte0);
			int k1 = base64_get(abyte0);
			int l1 = base64_get(abyte0);
			vert_get(j1, k1, l1);
		}

		for (int k3 = 0; k3 < i1; k3++) {
			int i2 = base64_get(abyte0);
			int j2 = base64_get(abyte0);
			int k2 = base64_get(abyte0);
			int l2 = base64_get(abyte0);
			this.light_falloff = base64_get(abyte0);
			this.light_ambient = base64_get(abyte0);
			int i3 = base64_get(abyte0);
			int[] ai = new int[i2];
			for (int l3 = 0; l3 < i2; l3++) {
				ai[l3] = base64_get(abyte0);
			}
			int[] ai1 = new int[l2];
			for (int i4 = 0; i4 < l2; i4++) {
				ai1[i4] = base64_get(abyte0);
			}
			int j4 = face_add(i2, ai, j2, k2);
			this.anIntArrayArray345[k3] = ai1;
			if (i3 == 0)
				this.face_shade[j4] = 0;
			else {
				this.face_shade[j4] = use_gourad_shade;
			}
		}
		this.trans_state = 1;
	}

	public Model(Model[] aclass7, int i, boolean flag, boolean flag1,
			boolean flag2, boolean flag3) {
		this.trans_state = 1;
		this.visible = true;
		this.aBoolean320 = true;
		this.trans_texture = false;
		this.trans = false;
		this.anInt323 = -1;
		this.autocommit = false;
		this.no_bounds = false;
		this.no_shading = false;
		this.unpickable = false;
		this.unrendered = false;
		this.max_dim = 12345678;
		this.light_x = 180;
		this.light_y = 155;
		this.light_z = 95;
		this.light_mag = 256;
		this.light_falloff = 512;
		this.light_ambient = 32;
		this.autocommit = flag;
		this.no_bounds = flag1;
		this.no_shading = flag2;
		this.unpickable = flag3;
		join(aclass7, i, false);
	}

	public Model(Model[] aclass7, int i) {
		this.trans_state = 1;
		this.visible = true;
		this.aBoolean320 = true;
		this.trans_texture = false;
		this.trans = false;
		this.anInt323 = -1;
		this.autocommit = false;
		this.no_bounds = false;
		this.no_shading = false;
		this.unpickable = false;
		this.unrendered = false;
		this.max_dim = 12345678;
		this.light_x = 180;
		this.light_y = 155;
		this.light_z = 95;
		this.light_mag = 256;
		this.light_falloff = 512;
		this.light_ambient = 32;
		join(aclass7, i, true);
	}

	public void join(Model[] aclass7, int i, boolean flag) {
		int j = 0;
		int k = 0;
		for (int l = 0; l < i; l++) {
			j += aclass7[l].face_cnt;
			k += aclass7[l].vert_cnt;
		}

		allocate(k, j);
		if (flag)
			this.anIntArrayArray345 = new int[j][];
		for (int i1 = 0; i1 < i; i1++) {
			Model class7 = aclass7[i1];
			class7.commit();
			this.light_ambient = class7.light_ambient;
			this.light_falloff = class7.light_falloff;
			this.light_x = class7.light_x;
			this.light_y = class7.light_y;
			this.light_z = class7.light_z;
			this.light_mag = class7.light_mag;
			for (int j1 = 0; j1 < class7.face_cnt; j1++) {
				int[] ai = new int[class7.face_vert_cnt[j1]];
				int[] ai1 = class7.face_verts[j1];
				for (int k1 = 0; k1 < class7.face_vert_cnt[j1]; k1++) {
					ai[k1] = vert_get(class7.vert_x[ai1[k1]],
							class7.vert_y[ai1[k1]], class7.vert_z[ai1[k1]]);
				}
				int l1 = face_add(class7.face_vert_cnt[j1], ai,
						class7.texture_front[j1], class7.texture_back[j1]);
				this.face_shade[l1] = class7.face_shade[j1];
				this.light_type = class7.light_type;
				this.norm_scale[l1] = class7.norm_scale[j1];
				this.norm_mag[l1] = class7.norm_mag[j1];
				if (flag) {
					if (i > 1) {
						this.anIntArrayArray345[l1] = new int[class7.anIntArrayArray345[j1].length + 1];
						this.anIntArrayArray345[l1][0] = i1;
						for (int i2 = 0; i2 < class7.anIntArrayArray345[j1].length; i2++)
							this.anIntArrayArray345[l1][(i2 + 1)] = class7.anIntArrayArray345[j1][i2];
					} else {
						this.anIntArrayArray345[l1] = new int[class7.anIntArrayArray345[j1].length];
						for (int j2 = 0; j2 < class7.anIntArrayArray345[j1].length; j2++) {
							this.anIntArrayArray345[l1][j2] = class7.anIntArrayArray345[j1][j2];
						}
					}
				}
			}
		}

		this.trans_state = 1;
	}

	public Model(int i, int[] ai, int[] ai1, int[] ai2, int j, int k) {
		this(i, 1);
		this.vert_cnt = i;
		for (int l = 0; l < i; l++) {
			this.vert_x[l] = ai[l];
			this.vert_y[l] = ai1[l];
			this.vert_z[l] = ai2[l];
		}

		this.face_cnt = 1;
		this.face_vert_cnt[0] = i;
		int[] ai3 = new int[i];
		for (int i1 = 0; i1 < i; i1++) {
			ai3[i1] = i1;
		}
		this.face_verts[0] = ai3;
		this.texture_front[0] = j;
		this.texture_back[0] = k;
		this.trans_state = 1;
	}

	public int vert_get(int i, int j, int k) {
		for (int l = 0; l < this.vert_cnt; l++) {
			if ((this.vert_x[l] == i) && (this.vert_y[l] == j)
					&& (this.vert_z[l] == k))
				return l;
		}
		if (this.vert_cnt >= this.max_vert_cnt) {
			return -1;
		}
		this.vert_x[this.vert_cnt] = i;
		this.vert_y[this.vert_cnt] = j;
		this.vert_z[this.vert_cnt] = k;
		return this.vert_cnt++;
	}

	public int vert_add(int x, int y, int z) {
		if (vert_cnt >= max_vert_cnt) {
			return -1;
		}
		vert_x[vert_cnt] = x;
		vert_y[vert_cnt] = y;
		vert_z[vert_cnt] = z;
		return vert_cnt++;
	}

	public int face_add(int vert_cnt, int[] vert, int front, int back) {
		if (this.face_cnt >= this.max_face_cnt) {
			return -1;
		}
		this.face_vert_cnt[this.face_cnt] = vert_cnt;
		this.face_verts[this.face_cnt] = vert;
		this.texture_front[this.face_cnt] = front;
		this.texture_back[this.face_cnt] = back;
		this.trans_state = 1;
		return this.face_cnt++;
	}

	public Model[] split(int bound_a, int bound_b, int row, int cnt,
			int max_verts, boolean unpickable) {
		commit();
		int[] vert_cnts = new int[cnt];
		int[] face_cnts = new int[cnt];
		for (int l1 = 0; l1 < cnt; l1++) {
			vert_cnts[l1] = 0;
			face_cnts[l1] = 0;
		}

		for (int ptr = 0; ptr < this.face_cnt; ptr++) {
			int sum_x = 0;
			int sum_z = 0;
			int vert_cnt = this.face_vert_cnt[ptr];
			int[] verts = this.face_verts[ptr];
			for (int i4 = 0; i4 < vert_cnt; i4++) {
				sum_x += this.vert_x[verts[i4]];
				sum_z += this.vert_z[verts[i4]];
			}

			int k4 = sum_x / (vert_cnt * bound_a) + sum_z
					/ (vert_cnt * bound_b) * row;
			vert_cnts[k4] += vert_cnt;
			face_cnts[k4] += 1;
		}

		Model[] aclass7 = new Model[cnt];
		for (int l2 = 0; l2 < cnt; l2++) {
			if (vert_cnts[l2] > max_verts)
				vert_cnts[l2] = max_verts;
			aclass7[l2] = new Model(vert_cnts[l2], face_cnts[l2], true, true,
					true, unpickable, true);
			aclass7[l2].light_falloff = this.light_falloff;
			aclass7[l2].light_ambient = this.light_ambient;
		}

		for (int j3 = 0; j3 < this.face_cnt; j3++) {
			int k3 = 0;
			int j4 = 0;
			int l4 = this.face_vert_cnt[j3];
			int[] ai3 = this.face_verts[j3];
			for (int i5 = 0; i5 < l4; i5++) {
				k3 += this.vert_x[ai3[i5]];
				j4 += this.vert_z[ai3[i5]];
			}

			int j5 = k3 / (l4 * bound_a) + j4 / (l4 * bound_b) * row;
			copy_face(aclass7[j5], ai3, l4, j3);
		}

		for (int l3 = 0; l3 < cnt; l3++) {
			aclass7[l3].project_prepare();
		}
		return aclass7;
	}

	public void copy_face(Model class7, int[] ai, int i, int j) {
		int[] ai1 = new int[i];
		for (int k = 0; k < i; k++) {
			int l = ai1[k] = class7.vert_get(this.vert_x[ai[k]],
					this.vert_y[ai[k]], this.vert_z[ai[k]]);
			class7.vert_shade[l] = this.vert_shade[ai[k]];
			class7.vert_ambient[l] = this.vert_ambient[ai[k]];
		}

		int i1 = class7.face_add(i, ai1, this.texture_front[j],
				this.texture_back[j]);
		if ((!class7.unpickable) && (!this.unpickable))
			class7.face_pick_tag[i1] = this.face_pick_tag[j];
		class7.face_shade[i1] = this.face_shade[j];
		class7.light_type = this.light_type;
		class7.norm_scale[i1] = this.norm_scale[j];
		class7.norm_mag[i1] = this.norm_mag[j];
	}
	
	public void set_light(int light, int ambient, int falloff, int light_x, int light_y, int light_z) {
		light_ambient = (256 - ambient * 4);
		light_falloff = ((64 - falloff) * 16 + 128);
		if (no_shading)
			return;

		this.light_type = light;
		this.light_x = light_x;
		this.light_y = light_y;
		this.light_z = light_z;
		this.light_mag = ((int) Math.sqrt(light_x * light_x + light_y * light_y + light_z * light_z));
		calc_shading();
	}

	public void set_light(boolean gourad, int ambient, int falloff,
			int light_x, int light_y, int light_z) {
		this.light_ambient = (256 - ambient * 4);
		this.light_falloff = ((64 - falloff) * 16 + 128);
		if (this.no_shading)
			return;
		light_type = gourad ? use_gourad_shade : use_flat_shade;
		this.light_x = light_x;
		this.light_y = light_y;
		this.light_z = light_z;
		this.light_mag = ((int) Math.sqrt(light_x * light_x + light_y * light_y
				+ light_z * light_z));
		calc_shading();
	}

	public void set_light(int ambient, int falloff, int x, int y, int z) {
		this.light_ambient = (256 - ambient * 4);
		this.light_falloff = ((64 - falloff) * 16 + 128);
		if (this.no_shading) {
			return;
		}
		this.light_x = x;
		this.light_y = y;
		this.light_z = z;
		this.light_mag = ((int) Math.sqrt(x * x + y * y + z * z));
		calc_shading();
	}

	public void set_light(int i, int j, int k) {
		if (this.no_shading) {
			return;
		}
		this.light_x = i;
		this.light_y = j;
		this.light_z = k;
		this.light_mag = ((int) Math.sqrt(i * i + j * j + k * k));
		calc_shading();
	}

	public void set_vert_ambient(int i, int j) {
		this.vert_ambient[i] = ((byte) j);
	}

	public void rotate(int yaw, int pitch, int roll) {
		this.yaw = (this.yaw + yaw & 0xFF);
		this.pitch = (this.pitch + pitch & 0xFF);
		this.roll = (this.roll + roll & 0xFF);
		determine_trans();
		this.trans_state = 1;
	}

	public void orient(int yaw, int pitch, int roll) {
		this.yaw = (yaw & 0xFF);
		this.pitch = (pitch & 0xFF);
		this.roll = (roll & 0xFF);
		determine_trans();
		this.trans_state = 1;
	}

	public void translate(int i, int j, int k) {
		this.base_x += i;
		this.base_y += j;
		this.base_z += k;
		determine_trans();
		this.trans_state = 1;
	}

	public void position(int x, int y, int z) {
		this.base_x = x;
		this.base_y = y;
		this.base_z = z;
		determine_trans();
		this.trans_state = 1;
	}

	public int base_x() {
		return this.base_x;
	}

	public void scale(int i, int j, int k) {
		this.scale_x = i;
		this.scale_y = j;
		this.scale_z = k;
		determine_trans();
		this.trans_state = 1;
	}

	public void shear(int i, int j, int k, int l, int i1, int j1) {
		this.shear_xy = i;
		this.shear_zy = j;
		this.shear_xz = k;
		this.shear_yz = l;
		this.shear_zx = i1;
		this.shear_yx = j1;
		determine_trans();
		this.trans_state = 1;
	}

	private void determine_trans() {
		if ((this.shear_xy != 256) || (this.shear_zy != 256)
				|| (this.shear_xz != 256) || (this.shear_yz != 256)
				|| (this.shear_zx != 256) || (this.shear_yx != 256)) {
			this.trans_type = 4;
			return;
		}
		if ((this.scale_x != 256) || (this.scale_y != 256)
				|| (this.scale_z != 256)) {
			this.trans_type = 3;
			return;
		}
		if ((this.yaw != 0) || (this.pitch != 0) || (this.roll != 0)) {
			this.trans_type = 2;
			return;
		}
		if ((this.base_x != 0) || (this.base_y != 0) || (this.base_z != 0)) {
			this.trans_type = 1;
			return;
		}
		this.trans_type = 0;
	}

	private void translate_apply(int i, int j, int k) {
		for (int l = 0; l < this.vert_cnt; l++) {
			this.trans_vert_x[l] += i;
			this.trans_vert_y[l] += j;
			this.trans_vert_z[l] += k;
		}
	}

	private void rotate_apply(int i, int j, int k) {
		for (int i3 = 0; i3 < this.vert_cnt; i3++) {
			if (k != 0) {
				int l = sin256_cache[k];
				int k1 = sin256_cache[(k + 256)];
				int j2 = this.trans_vert_y[i3] * l + this.trans_vert_x[i3] * k1 >> 15;
				this.trans_vert_y[i3] = (this.trans_vert_y[i3] * k1
						- this.trans_vert_x[i3] * l >> 15);
				this.trans_vert_x[i3] = j2;
			}
			if (i != 0) {
				int i1 = sin256_cache[i];
				int l1 = sin256_cache[(i + 256)];
				int k2 = this.trans_vert_y[i3] * l1 - this.trans_vert_z[i3]
						* i1 >> 15;
				this.trans_vert_z[i3] = (this.trans_vert_y[i3] * i1
						+ this.trans_vert_z[i3] * l1 >> 15);
				this.trans_vert_y[i3] = k2;
			}
			if (j != 0) {
				int j1 = sin256_cache[j];
				int i2 = sin256_cache[(j + 256)];
				int l2 = this.trans_vert_z[i3] * j1 + this.trans_vert_x[i3]
						* i2 >> 15;
				this.trans_vert_z[i3] = (this.trans_vert_z[i3] * i2
						- this.trans_vert_x[i3] * j1 >> 15);
				this.trans_vert_x[i3] = l2;
			}
		}
	}

	private void shear_apply(int xy, int zy, int xz, int yz, int zx, int yx) {
		for (int k1 = 0; k1 < this.vert_cnt; k1++) {
			if (xy != 0)
				this.trans_vert_x[k1] += (this.trans_vert_y[k1] * xy >> 8);
			if (zy != 0)
				this.trans_vert_z[k1] += (this.trans_vert_y[k1] * zy >> 8);
			if (xz != 0)
				this.trans_vert_x[k1] += (this.trans_vert_z[k1] * xz >> 8);
			if (yz != 0)
				this.trans_vert_y[k1] += (this.trans_vert_z[k1] * yz >> 8);
			if (zx != 0)
				this.trans_vert_z[k1] += (this.trans_vert_x[k1] * zx >> 8);
			if (yx != 0)
				this.trans_vert_y[k1] += (this.trans_vert_x[k1] * yx >> 8);
		}
	}

	private void scale_apply(int i, int j, int k) {
		for (int l = 0; l < this.vert_cnt; l++) {
			this.trans_vert_x[l] = (this.trans_vert_x[l] * i >> 8);
			this.trans_vert_y[l] = (this.trans_vert_y[l] * j >> 8);
			this.trans_vert_z[l] = (this.trans_vert_z[l] * k >> 8);
		}
	}

	private void calc_bounds() {
		this.min_x = (this.min_y = this.min_z = 999999);
		this.max_dim = (this.max_x = this.max_y = this.max_z = -999999);
		for (int i = 0; i < this.face_cnt; i++) {
			int[] face_v = this.face_verts[i];
			int vert = face_v[0];
			int face_v_cnt = this.face_vert_cnt[i];
			int min_x;
			int max_x = min_x = this.trans_vert_x[vert];
			int min_y;
			int max_y = min_y = this.trans_vert_y[vert];
			int min_z;
			int max_z = min_z = this.trans_vert_z[vert];
			for (int j = 0; j < face_v_cnt; j++) {
				int l = face_v[j];
				if (this.trans_vert_x[l] < min_x)
					min_x = this.trans_vert_x[l];
				else if (this.trans_vert_x[l] > max_x)
					max_x = this.trans_vert_x[l];
				if (this.trans_vert_y[l] < min_y)
					min_y = this.trans_vert_y[l];
				else if (this.trans_vert_y[l] > max_y)
					max_y = this.trans_vert_y[l];
				if (this.trans_vert_z[l] < min_z)
					min_z = this.trans_vert_z[l];
				else if (this.trans_vert_z[l] > max_z) {
					max_z = this.trans_vert_z[l];
				}
			}
			if (!this.no_bounds) {
				this.face_min_x[i] = min_x;
				this.face_max_x[i] = max_x;
				this.face_min_y[i] = min_y;
				this.face_max_y[i] = max_y;
				this.face_min_z[i] = min_z;
				this.face_max_z[i] = max_z;
			}
			if (max_x - min_x > this.max_dim)
				this.max_dim = (max_x - min_x);
			if (max_y - min_y > this.max_dim)
				this.max_dim = (max_y - min_y);
			if (max_z - min_z > this.max_dim)
				this.max_dim = (max_z - min_z);
			if (min_x < this.min_x)
				this.min_x = min_x;
			if (max_x > this.max_x)
				this.max_x = max_x;
			if (min_y < this.min_y)
				this.min_y = min_y;
			if (max_y > this.max_y)
				this.max_y = max_y;
			if (min_z < this.min_z)
				this.min_z = min_z;
			if (max_z > this.max_z)
				this.max_z = max_z;
		}
	}

	public void calc_shading() {
		if (no_shading)
			return;
		
		switch (light_type) {
		case use_gourad_shade:
			calc_gourad_shade();
			break;
		case use_phong_shade:
			calc_phong_shade();
			break;
		case use_flat_shade:
			calc_default_shade();
			break;
		}
	}
	
	private void calc_gourad_shade() {
		int radiometry = light_falloff * light_mag >> 8;
		int[] x = new int[vert_cnt];
		int[] y = new int[vert_cnt];
		int[] z = new int[vert_cnt];
		int[] cnt = new int[vert_cnt];

		for (int i = 0; i < face_cnt; i++) {
			for (int j = 0; j < face_vert_cnt[i]; j++) {
				int vert = face_verts[i][j];
				x[vert] += face_norm_x[i];
				y[vert] += face_norm_y[i];
				z[vert] += face_norm_z[i];
				cnt[vert] += 1;
			}
		}
		for (int i = 0; i < vert_cnt; i++) {
			if (cnt[i] > 0) {
				vert_shade[i] = (((x[i] * light_x)
								+ (y[i] * light_y)
								+ (z[i] * light_z)) / (radiometry * cnt[i]));
			}
		}
	}
	
	private void calc_phong_shade() {
		int radiometry = light_falloff * light_mag >> 8;
		int[] x = new int[vert_cnt];
		int[] y = new int[vert_cnt];
		int[] z = new int[vert_cnt];
		int[] cnt = new int[vert_cnt];

		for (int i = 0; i < face_cnt; i++) {
			for (int j = 0; j < face_vert_cnt[i]; j++) {
				int vert = face_verts[i][j];
				x[vert] += face_norm_x[i] >> 1;
				y[vert] += face_norm_y[i] >> 1;
				z[vert] += face_norm_z[i] >> 1;
				cnt[vert] += 1;
			}
		}
		for (int i = 0; i < vert_cnt; i++) {
			if (cnt[i] > 0) {
				vert_shade[i] = (((x[i] * light_x)
								+ (y[i] * light_y)
								+ (z[i] * light_z)) / (radiometry * cnt[i]));
			}
		}
	}
	
	private void calc_default_shade() {
		int radiometry = light_falloff * light_mag >> 8;

		for (int i = 0; i < face_cnt; i++) {
			face_shade[i] = (((face_norm_x[i] * light_x)
							+ (face_norm_y[i] * light_y)
							+ (face_norm_z[i] * light_z)) / radiometry);
		}
	}

	public void calc_norms() {
		if ((this.no_shading) && (this.no_bounds))
			return;
		for (int i = 0; i < this.face_cnt; i++) {
			int[] ai = this.face_verts[i];
			int j = this.trans_vert_x[ai[0]];
			int k = this.trans_vert_y[ai[0]];
			int l = this.trans_vert_z[ai[0]];
			int i1 = this.trans_vert_x[ai[1]] - j;
			int j1 = this.trans_vert_y[ai[1]] - k;
			int k1 = this.trans_vert_z[ai[1]] - l;
			int l1 = this.trans_vert_x[ai[2]] - j;
			int i2 = this.trans_vert_y[ai[2]] - k;
			int j2 = this.trans_vert_z[ai[2]] - l;
			int k2 = j1 * j2 - i2 * k1;
			int l2 = k1 * l1 - j2 * i1;

			int i3;
			for (i3 = i1 * i2 - l1 * j1; (k2 > 8192) || (l2 > 8192)
					|| (i3 > 8192) || (k2 < -8192) || (l2 < -8192)
					|| (i3 < -8192); i3 >>= 1) {
				k2 >>= 1;
				l2 >>= 1;
			}

			int mag = (int) (256.0D * Math.sqrt(k2 * k2 + l2 * l2 + i3 * i3));
			if (mag <= 0)
				mag = 1;
			this.face_norm_x[i] = (k2 * 65536 / mag);
			this.face_norm_y[i] = (l2 * 65536 / mag);
			this.face_norm_z[i] = (i3 * 65535 / mag);
			this.norm_scale[i] = -1;
		}

		calc_shading();
	}

	public void trans_apply() {
		if (this.trans_state == 2) {
			this.trans_state = 0;
			for (int i = 0; i < this.vert_cnt; i++) {
				this.trans_vert_x[i] = this.vert_x[i];
				this.trans_vert_y[i] = this.vert_y[i];
				this.trans_vert_z[i] = this.vert_z[i];
			}

			this.min_x = (this.min_y = this.min_z = -9999999);
			this.max_dim = (this.max_x = this.max_y = this.max_z = 9999999);
			return;
		}
		if (this.trans_state == 1) {
			this.trans_state = 0;
			for (int j = 0; j < this.vert_cnt; j++) {
				this.trans_vert_x[j] = this.vert_x[j];
				this.trans_vert_y[j] = this.vert_y[j];
				this.trans_vert_z[j] = this.vert_z[j];
			}

			if (this.trans_type >= 2)
				rotate_apply(this.yaw, this.pitch, this.roll);
			if (this.trans_type >= 3)
				scale_apply(this.scale_x, this.scale_y, this.scale_z);
			if (this.trans_type >= 4)
				shear_apply(this.shear_xy, this.shear_zy, this.shear_xz,
						this.shear_yz, this.shear_zx, this.shear_yx);
			if (this.trans_type >= 1)
				translate_apply(this.base_x, this.base_y, this.base_z);
			calc_bounds();
			calc_norms();
		}
	}

	public void project(int cam_x, int cam_y, int cam_z, int cam_roll,
			int cam_pitch, int cam_yaw, int plane_dist, int near_dist) {
		trans_apply();
		if ((this.min_z > Scene.frustum_near_z)
				|| (this.max_z < Scene.frustum_far_z)
				|| (this.min_x > Scene.frustum_min_x)
				|| (this.max_x < Scene.frustum_max_x)
				|| (this.min_y > Scene.frustum_min_y)
				|| (this.max_y < Scene.frustum_max_y)) {
			this.visible = false;
			return;
		}
		this.visible = true;
		int l2 = 0;
		int i3 = 0;
		int j3 = 0;
		int k3 = 0;
		int l3 = 0;
		int i4 = 0;
		if (cam_yaw != 0) {
			l2 = sin1024_cache[cam_yaw];
			i3 = sin1024_cache[(cam_yaw + 1024)];
		}
		if (cam_pitch != 0) {
			l3 = sin1024_cache[cam_pitch];
			i4 = sin1024_cache[(cam_pitch + 1024)];
		}
		if (cam_roll != 0) {
			j3 = sin1024_cache[cam_roll];
			k3 = sin1024_cache[(cam_roll + 1024)];
		}
		for (int j4 = 0; j4 < this.vert_cnt; j4++) {
			int k4 = this.trans_vert_x[j4] - cam_x;
			int l4 = this.trans_vert_y[j4] - cam_y;
			int i5 = this.trans_vert_z[j4] - cam_z;
			if (cam_yaw != 0) {
				int i2 = l4 * l2 + k4 * i3 >> 15;
				l4 = l4 * i3 - k4 * l2 >> 15;
				k4 = i2;
			}
			if (cam_pitch != 0) {
				int j2 = i5 * l3 + k4 * i4 >> 15;
				i5 = i5 * i4 - k4 * l3 >> 15;
				k4 = j2;
			}
			if (cam_roll != 0) {
				int k2 = l4 * k3 - i5 * j3 >> 15;
				i5 = l4 * j3 + i5 * k3 >> 15;
				l4 = k2;
			}
			if (i5 >= near_dist)
				this.project_plane_x[j4] = ((k4 << plane_dist) / i5);
			else
				this.project_plane_x[j4] = (k4 << plane_dist);
			if (i5 >= near_dist)
				this.project_plane_y[j4] = ((l4 << plane_dist) / i5);
			else
				this.project_plane_y[j4] = (l4 << plane_dist);
			this.project_vert_x[j4] = k4;
			this.project_vert_y[j4] = l4;
			this.project_vert_z[j4] = i5;
		}
	}

	public void commit() {
		trans_apply();
		for (int i = 0; i < this.vert_cnt; i++) {
			this.vert_x[i] = this.trans_vert_x[i];
			this.vert_y[i] = this.trans_vert_y[i];
			this.vert_z[i] = this.trans_vert_z[i];
		}

		this.base_x = (this.base_y = this.base_z = 0);
		this.yaw = (this.pitch = this.roll = 0);
		this.scale_x = (this.scale_y = this.scale_z = 256);
		this.shear_xy = (this.shear_zy = this.shear_xz = this.shear_yz = this.shear_zx = this.shear_yx = 256);
		this.trans_type = 0;
	}

	public Model copy() {
		Model[] aclass7 = new Model[1];
		aclass7[0] = this;
		Model class7 = new Model(aclass7, 1);
		class7.depth = this.depth;
		class7.trans = this.trans;
		return class7;
	}

	public Model copy(boolean flag, boolean flag1, boolean flag2, boolean flag3) {
		Model[] other = new Model[1];
		other[0] = this;
		Model class7 = new Model(other, 1, flag, flag1, flag2, flag3);
		class7.depth = this.depth;
		return class7;
	}

	public void copy_trans(Model other) {
		this.yaw = other.yaw;
		this.pitch = other.pitch;
		this.roll = other.roll;
		this.base_x = other.base_x;
		this.base_y = other.base_y;
		this.base_z = other.base_z;
		determine_trans();
		this.trans_state = 1;
	}

	public int base64_get(byte[] buf) {
		while ((buf[this.buf_ptr] == 10) || (buf[this.buf_ptr] == 13))
			this.buf_ptr += 1;
		int i = dec_tab[(buf[(this.buf_ptr++)] & 0xFF)];
		int j = dec_tab[(buf[(this.buf_ptr++)] & 0xFF)];
		int k = dec_tab[(buf[(this.buf_ptr++)] & 0xFF)];
		int l = i * 4096 + j * 64 + k - 131072;
		if (l == 123456)
			l = use_gourad_shade;
		return l;
	}

	static {
		for (int i = 0; i < 256; i++) {
			sin256_cache[i] = ((int) (Math.sin(i * 0.02454369D) * 32768.0D));
			sin256_cache[(i + 256)] = ((int) (Math.cos(i * 0.02454369D) * 32768.0D));
		}

		for (int j = 0; j < 1024; j++) {
			sin1024_cache[j] = ((int) (Math.sin(j * 0.00613592315D) * 32768.0D));
			sin1024_cache[(j + 1024)] = ((int) (Math.cos(j * 0.00613592315D) * 32768.0D));
		}

		for (int k = 0; k < 10; k++) {
			enc_tab[k] = ((byte) (48 + k));
		}
		for (int l = 0; l < 26; l++) {
			enc_tab[(l + 10)] = ((byte) (65 + l));
		}
		for (int i1 = 0; i1 < 26; i1++) {
			enc_tab[(i1 + 36)] = ((byte) (97 + i1));
		}
		enc_tab[62] = -93;
		enc_tab[63] = 36;
		for (int j1 = 0; j1 < 10; j1++) {
			dec_tab[(48 + j1)] = j1;
		}
		for (int k1 = 0; k1 < 26; k1++) {
			dec_tab[(65 + k1)] = (k1 + 10);
		}
		for (int l1 = 0; l1 < 26; l1++) {
			dec_tab[(97 + l1)] = (l1 + 36);
		}
		dec_tab[163] = 62;
		dec_tab[36] = 63;
	}
}