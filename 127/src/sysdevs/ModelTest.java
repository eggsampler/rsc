package sysdevs;

import jagex.DataUtil;
import jagex.client.Game;
import jagex.client.Model;
import jagex.client.Scene;
import jagex.client.Surface;

import java.awt.Graphics;
import java.io.IOException;

public final class ModelTest extends Game {

	private static final long serialVersionUID = -7785287890455810407L;

	public static void main(String[] args) {
		ModelTest test = new ModelTest("Test", 400, 300);
		test.show();
	}

	private final String title;

	private final int w, h;
			
	private final Surface surface;
	
	private final Scene scene;

	private Graphics gfx;
	
	protected ModelTest(String title, int w, int h) {
		this.title = title;
		this.w = w;
		this.h = h;
		surface = new Surface(w, h, 0, this);
		scene = new Scene(surface, 2, 100, 1);
	}

	@Override
	public void preload_draw() {
		
	}
	
	@Override
	public void load() {
		gfx = getGraphics();
		scene.clip_far_3d = 1500;
		scene.clip_far_2d = 150;
		scene.fog_falloff = 1;
		scene.fog_dist = 1200;
        scene.set_bounds(w / 2, h / 2, w, w / 2, 400, h / 2);
		scene.set_camera(0, 8, -50, 0, 0, 0, 10);
		
		try {
			byte[] model_arc = load_file("models.jag", "3d models", 50);
			
			int off = DataUtil.offset_entry("anvil.ob3", model_arc);
			//Model mdl = new Model(model_arc, off, true);
			//mdl.scale(20, 20, 20);
			//scene.add(mdl);
			//mdl.position(0, 0, 0);
			
			Model plane = create_plane();
			scene.add(plane);
			plane.position(0, 0, 0);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		scene.set_light(true, 56, 32, -10, -10, -10);
		set_rate(60);
	}

	@Override
	public void update() {
		
	}

	boolean slept = false;
	
	@Override
	public void display() {
		surface.rect_fill(0, 0, w, h, Surface.DARK_GRAY);
		scene.render();
		draw_sys_info();
		surface.copy(gfx, 0, 0);
		surface.clear();
		
		if (!slept) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			slept = true;
		}
	}
	
	@Override
	public void cleanup() {
		
	}
	
	public void show() {
		start(w, h, title, false);
	}
	
	private void draw_sys_info() {
		Runtime r = Runtime.getRuntime();
		int total = (int) (r.totalMemory() / 1024L / 1024L);
		int avail = (int) ((r.totalMemory() - r.freeMemory()) / 1024L / 1024L);

		int y = 15;
		surface.text_draw("fps: " + fps, 10, y, Surface.FONT_BOLD, Surface.YELLOW);
		y += Surface.height(Surface.FONT_BOLD);
		surface.text_draw("mem: " + avail + "MB/" + total + "MB", 10, y, Surface.FONT_BOLD, Surface.YELLOW);
	}
	
	Model create_plane() {
		int front = Scene.rgb(255, 0, 0);
		int back = Scene.rgb(0, 255, 0);
		
		Model plane = new Model(6, 2);
		
		plane.vert_add(-64, -64, 0);
		plane.vert_add(-64, 64, 0);
		plane.vert_add(64, 64, 0);

		plane.vert_add(-64, -64, 0);
		plane.vert_add(64, -64, 0);
		plane.vert_add(64, 64, 0);
		
		plane.face_add(3, new int[] { 0, 1, 2 }, front, back);
		plane.face_add(3, new int[] { 3, 4, 5 }, back, front);
		
		plane.scale(20, 20, 20);
		return plane;
	}

}
