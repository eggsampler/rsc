import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Transferable;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;

final class CachePackageManager implements Runnable {

    static Method methodFocusCycle;
    static String javaVendor;
    static Method methodFocusTraversal;
    static String javaVersion;
    static Class aClass1077;
    static Class aClass1078;
    static Class aClass1079;
    private static volatile long aLong1053 = 0L;
    private static String osName;
    private static String gameName;
    private static String homeDir;
    private static String osNameLc;
    private static int gameModeId;
    EventQueue systemEventQueue;
    CacheFile cacheRandom = null;
    CacheFile cacheMainDat2 = null;
    CacheFile cacheMainIdx255 = null;
    private Callback_Sub1 aCallback_Sub1_1055;
    private Thread aThread1056;
    private boolean usingMsJava = false;
    private boolean applicationMode = false;
    private Object anObject1064;
    private CacheFile[] cacheMainIdxs;
    private CacheState aCacheState_1072 = null;
    private CacheState aCacheState_1073 = null;
    private Object msJavaSound;
    private boolean aBoolean1075 = false;


    CachePackageManager(int modeId, String gamename, int cacheCount, boolean applicationMode) throws Exception {
        javaVendor = "Unknown";
        gameName = gamename;
        gameModeId = modeId;
        this.applicationMode = applicationMode;
        javaVersion = "1.1";

        try {
            javaVendor = System.getProperty("java.vendor");
            javaVersion = System.getProperty("java.version");
        } catch (Exception var15) {
            ;
        }

        if (javaVendor.toLowerCase().indexOf("microsoft") != -1) {
            this.usingMsJava = true;
        }

        try {
            osName = System.getProperty("os.name");
        } catch (Exception var14) {
            osName = "Unknown";
        }

        osNameLc = osName.toLowerCase();

        try {
            System.getProperty("os.arch").toLowerCase();
        } catch (Exception var13) {
            ;
        }

        try {
            System.getProperty("os.version").toLowerCase();
        } catch (Exception var12) {
            ;
        }

        try {
            homeDir = System.getProperty("user.home");
            if (homeDir != null) {
                homeDir = homeDir + "/";
            }
        } catch (Exception var11) {
            ;
        }

        if (homeDir == null) {
            homeDir = "~/";
        }

        try {
            this.systemEventQueue = Toolkit.getDefaultToolkit().getSystemEventQueue();
        } catch (Throwable var10) {
            ;
        }

        if (!this.usingMsJava) {
            try {
                methodFocusTraversal = Class.forName("java.awt.Component").getDeclaredMethod("setFocusTraversalKeysEnabled", new Class[]{Boolean.TYPE});
            } catch (Exception var9) {
                ;
            }

            try {
                methodFocusCycle = Class.forName("java.awt.Container").getDeclaredMethod("setFocusCycleRoot", new Class[]{Boolean.TYPE});
            } catch (Exception var8) {
                ;
            }
        }

        CacheContainerManager.initialize(gameName, gameModeId);
        if (this.applicationMode) {
            this.cacheRandom = new CacheFile(CacheContainerManager.getFile("random.dat", (String) null, gameModeId, 0), "rw", 25L);
            this.cacheMainDat2 = new CacheFile(CacheContainerManager.getFile("main_file_cache.dat2", 0), "rw", 314572800L);
            this.cacheMainIdx255 = new CacheFile(CacheContainerManager.getFile("main_file_cache.idx255", 0), "rw", 1048576L);
            this.cacheMainIdxs = new CacheFile[cacheCount];

            for (int var5 = 0; cacheCount > var5; ++var5) {
                this.cacheMainIdxs[var5] = new CacheFile(CacheContainerManager.getFile("main_file_cache.idx" + var5, 0), "rw", 1048576L);
            }

            if (this.usingMsJava) {
                try {
                    this.msJavaSound = Class.forName("DirectSoundImpl").newInstance();
                } catch (Throwable var7) {
                    ;
                }
            }

            try {
                if (!this.usingMsJava) {
                    this.anObject1064 = Class.forName("GameCursor").newInstance();
                } else {
                    this.aCallback_Sub1_1055 = new Callback_Sub1();
                }
            } catch (Throwable var6) {
                ;
            }
        }

        this.aBoolean1075 = false;
        this.aThread1056 = new Thread(this);
        this.aThread1056.setPriority(10);
        this.aThread1056.setDaemon(true);
        this.aThread1056.start();
    }

    private static CacheFile openPrefs(int var1, String var2, String var3) {
        String filename;
        if (var1 == 33) {
            filename = "jagex_" + var3 + "_preferences" + var2 + "_rc.dat";
        } else if (var1 == 34) {
            filename = "jagex_" + var3 + "_preferences" + var2 + "_wip.dat";
        } else {
            filename = "jagex_" + var3 + "_preferences" + var2 + ".dat";
        }

        String[] var5 = new String[]{"c:/rscache/", "/rscache/", homeDir, "c:/windows/", "c:/winnt/", "c:/", "/tmp/", ""};

        for (int var6 = 0; var5.length > var6; ++var6) {
            String path = var5[var6];
            if (path.length() <= 0 || (new File(path)).exists()) {
                try {
                    CacheFile prefs = new CacheFile(new File(path, filename), "rw", 10000L);
                    return prefs;
                } catch (Exception var9) {
                    ;
                }
            }
        }

        return null;
    }

    static Class method593(String var0) {
        try {
            return Class.forName(var0);
        } catch (ClassNotFoundException var2) {
            throw new NoClassDefFoundError(var2.getMessage());
        }
    }

    final CacheState method585(int var1, byte var2, Runnable var3) {
        return var2 != -128 ? null : this.method588(2, var3, var1, 0, (byte) -78);
    }

    public final void run() {
        while (true) {
            CacheState var2;
            synchronized (this) {
                while (true) {
                    if (aBoolean1075) {
                        /*
                        was:
                        while(!aBoolean1075) { ... }
                         */
                        return;
                    }
                    if (this.aCacheState_1073 != null) {
                        var2 = this.aCacheState_1073;
                        this.aCacheState_1073 = this.aCacheState_1073.aCacheState_177;
                        if (this.aCacheState_1073 == null) {
                            this.aCacheState_1072 = null;
                        }
                        break;
                    }

                    try {
                        this.wait();
                    } catch (InterruptedException var20) {

                    }
                }
            }

            try {
                int var1 = var2.anInt178;
                if (var1 != 1) {
                    if (var1 == 22) {
                        if (Utility.currentTimeMillis(-73) < aLong1053) {
                            throw new IOException();
                        }

                        try {
                            var2.stateObject = ClientProxyIndirect.create((String) var2.anObject174, var2.anInt176).getSystemProxy(0);
                        } catch (ProxyAuthenticationException var19) {
                            var2.stateObject = var19.getMessage();
                            throw var19;
                        }
                    } else if (var1 == 2) {
                        Thread var3 = new Thread((Runnable) var2.anObject174);
                        var3.setDaemon(true);
                        var3.start();
                        var3.setPriority(var2.anInt176);
                        var2.stateObject = var3;
                    } else if (var1 != 4) {
                        Object[] var24;
                        if (var1 == 8) {
                            var24 = (Object[]) var2.anObject174;
                            if (this.applicationMode && ((Class) var24[0]).getClassLoader() == null) {
                                throw new SecurityException();
                            }

                            var2.stateObject = ((Class) var24[0]).getDeclaredMethod((String) var24[1], (Class[]) var24[2]);
                        } else if (var1 == 9) {
                            var24 = (Object[]) var2.anObject174;
                            if (this.applicationMode && ((Class) var24[0]).getClassLoader() == null) {
                                throw new SecurityException();
                            }

                            var2.stateObject = ((Class) var24[0]).getDeclaredField((String) var24[1]);
                        } else if (var1 != 18) {
                            if (var1 != 19) {
                                if (!this.applicationMode) {
                                    throw new Exception("");
                                }

                                String var25;
                                if (var1 != 3) {
                                    if (var1 == 21) {
                                        if (Utility.currentTimeMillis(-32) < aLong1053) {
                                            throw new IOException();
                                        }

                                        var2.stateObject = InetAddress.getByName((String) var2.anObject174).getAddress();
                                    } else {
                                        CacheFile var29;
                                        if (var1 != 12) {
                                            if (var1 != 13) {
                                                if (this.applicationMode && var1 == 14) {
                                                    int var30 = var2.anInt176;
                                                    int var28 = var2.anInt175;
                                                    if (!this.usingMsJava) {
                                                        Class.forName("GameCursor").getDeclaredMethod("mouseMove", new Class[]{Integer.TYPE, Integer.TYPE}).invoke(this.anObject1064, new Object[]{new Integer(var30), new Integer(var28)});
                                                    } else {
                                                        this.aCallback_Sub1_1055.method7(var30, 113, var28);
                                                    }
                                                } else if (this.applicationMode && var1 == 15) {
                                                    boolean var27 = var2.anInt176 != 0;
                                                    Component var26 = (Component) var2.anObject174;
                                                    if (!this.usingMsJava) {
                                                        Class.forName("GameCursor").getDeclaredMethod("setComponent", new Class[]{aClass1077 != null ? aClass1077 : (aClass1077 = method593("java.awt.Component")), Boolean.TYPE}).invoke(this.anObject1064, new Object[]{var26, new Boolean(var27)});
                                                    } else {
                                                        this.aCallback_Sub1_1055.method6(var26, -113, var27);
                                                    }
                                                } else if (!this.usingMsJava && var1 == 17) {
                                                    var24 = (Object[]) var2.anObject174;
                                                    Class.forName("GameCursor").getDeclaredMethod("setCursor", new Class[]{aClass1077 != null ? aClass1077 : (aClass1077 = method593("java.awt.Component")), aClass1078 != null ? aClass1078 : (aClass1078 = method593("[I")), Integer.TYPE, Integer.TYPE, aClass1079 != null ? aClass1079 : (aClass1079 = method593("java.awt.Point"))}).invoke(this.anObject1064, new Object[]{(Component) var24[0], (int[]) var24[1], new Integer(var2.anInt176), new Integer(var2.anInt175), (Point) var24[2]});
                                                } else {
                                                    if (var1 != 16) {
                                                        throw new Exception("");
                                                    }

                                                    try {
                                                        if (!osNameLc.startsWith("win")) {
                                                            throw new Exception();
                                                        }

                                                        var25 = (String) var2.anObject174;
                                                        if (!var25.startsWith("http://") && !var25.startsWith("https://")) {
                                                            throw new Exception();
                                                        }

                                                        String var4 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789?&=,.%+-_#:/*";

                                                        for (int var5 = 0; var5 < var25.length(); ++var5) {
                                                            if (var4.indexOf(var25.charAt(var5)) == -1) {
                                                                throw new Exception();
                                                            }
                                                        }

                                                        Runtime.getRuntime().exec("cmd /c start \"j\" \"" + var25 + "\"");
                                                        var2.stateObject = null;
                                                    } catch (Exception var21) {
                                                        var2.stateObject = var21;
                                                        throw var21;
                                                    }
                                                }
                                            } else {
                                                var29 = openPrefs(gameModeId, (String) var2.anObject174, "");
                                                var2.stateObject = var29;
                                            }
                                        } else {
                                            var29 = openPrefs(gameModeId, (String) var2.anObject174, gameName);
                                            var2.stateObject = var29;
                                        }
                                    }
                                } else {
                                    if (Utility.currentTimeMillis(-63) < aLong1053) {
                                        throw new IOException();
                                    }

                                    var25 = (var2.anInt176 >> 24 & 255) + "." + (255 & var2.anInt176 >> 16) + "." + (('\uff8e' & var2.anInt176) >> 8) + "." + (var2.anInt176 & 255);
                                    var2.stateObject = InetAddress.getByName(var25).getHostName();
                                }
                            } else {
                                Transferable var33 = (Transferable) var2.anObject174;
                                Clipboard var31 = Toolkit.getDefaultToolkit().getSystemClipboard();
                                var31.setContents(var33, (ClipboardOwner) null);
                            }
                        } else {
                            Clipboard var32 = Toolkit.getDefaultToolkit().getSystemClipboard();
                            var2.stateObject = var32.getContents((Object) null);
                        }
                    } else {
                        if (aLong1053 > Utility.currentTimeMillis(114)) {
                            throw new IOException();
                        }

                        var2.stateObject = new DataInputStream(((URL) var2.anObject174).openStream());
                    }
                } else {
                    if (aLong1053 > Utility.currentTimeMillis(-81)) {
                        throw new IOException();
                    }

                    var2.stateObject = new Socket(InetAddress.getByName((String) var2.anObject174), var2.anInt176);
                }

                var2.state = 1;
            } catch (Throwable var22) {
                var2.state = 2;
            }

            synchronized (var2) {
                var2.notify();
            }
        }
    }

    final CacheState method586(int var1, int var2) {
        if (var1 != 20757) {
            this.cacheRandom = null;
        }

        return this.method588(3, (Object) null, var2, 0, (byte) -78);
    }

    private CacheState method587(int var1, byte var2, String var3, boolean var4) {
        if (var2 != -43) {
            this.method590(73, 81, (String) null);
        }

        return this.method588(var4 ? 22 : 1, var3, var1, 0, (byte) -78);
    }

    private CacheState method588(int var1, Object url, int var3, int var4, byte var5) {
        CacheState var6 = new CacheState();
        var6.anInt175 = var4;
        var6.anInt178 = var1;
        var6.anInt176 = var3;
        if (var5 != -78) {
            this.method588(107, (Object) null, 45, -111, (byte) 12);
        }

        var6.anObject174 = url;
        synchronized (this) {
            if (this.aCacheState_1072 != null) {
                this.aCacheState_1072.aCacheState_177 = var6;
                this.aCacheState_1072 = var6;
            } else {
                this.aCacheState_1072 = this.aCacheState_1073 = var6;
            }

            this.notify();
        }

        return var6;
    }

    final Object method589(byte var1) {
        if (var1 != 127) {
            this.applicationMode = false;
        }

        return this.msJavaSound;
    }

    final CacheState method590(int var1, int var2, String var3) {
        return var2 != 837317288 ? null : this.method587(var1, (byte) -43, var3, false);
    }

    final CacheState openURL(URL url) {
        return this.method588(4, url, 0, 0, (byte) -78);
    }

}
