import java.io.IOException;

class ClientStreamBase {

    static int anInt739;
    static int[] anIntArray810 = new int[256];
    private static long[] aLongArray724 = new long[256];
    int maxReadTries = 0;
    String socketExceptionMessage = "";
    boolean socketException = false;
    BufferBase_Sub3_Sub1 writeBuffer;
    private int packetMaxLength = 5000;
    private int length = 0;
    private Isaac incomingIsaac;
    private Isaac isaacOutgoing;
    private int readTries = 0;
    private int anInt733 = 0;
    private int packetStart = 0;

    public ClientStreamBase() {
        this.writeBuffer = new BufferBase_Sub3_Sub1(this.packetMaxLength);
        this.writeBuffer.offset = 3;
    }

    static {
        for (int var0 = 0; var0 < 256; ++var0) {
            long var1 = (long) var0;

            for (int var3 = 0; var3 < 8; ++var3) {
                if ((var1 & 1L) != 1) {
                    var1 >>>= 1;
                } else {
                    var1 = var1 >>> 1 ^ -3932672073523589310L;
                }
            }

            aLongArray724[var0] = var1;
        }

        anInt739 = 114;
    }

    static String method352(BufferBase_Sub3 var0, int var1, byte var2) {
        try {
            int var3 = var0.method220();
            if (var2 != -109) {
                method352((BufferBase_Sub3) null, 121, (byte) -68);
            }

            if (var3 > var1) {
                var3 = var1;
            }

            byte[] var4 = new byte[var3];
            var0.offset += client.aClass11_1110.method240(var0.buffer, 0, var4, true, var0.offset, var3);
            String var5 = Utility.readUnicodeString(var4, 0, var3);
            return var5;
        } catch (Exception var6) {
            return "Cabbage";
        }
    }

    static String cabbage(BufferBase_Sub3 var1) {
        return method352(var1, 32767, (byte) -109);
    }

    final void flushPacket() throws IOException {
        this.sendPacket();
        this.writePacket(0);
    }

    final void newPacket(int id) {
        if (this.packetMaxLength * 4 / 5 < this.packetStart) {
            try {
                this.writePacket(0);
            } catch (IOException var5) {
                this.socketException = true;
                this.socketExceptionMessage = var5.getMessage();
            }
        }

        this.writeBuffer.offset = this.packetStart + 2;
        this.writeBuffer.putByte(id);
    }

    final int isaacIncoming(int var1) {
        return 255 & var1 - this.incomingIsaac.next();
    }

    final boolean method465() {
        return this.packetStart > 0;
    }

    int availableStream() throws IOException {
        return 0;
    }

    int read() throws IOException {
        return 0;
    }

    void closeStream() {
    }

    final void init_isaac(int[] var2) {
        this.incomingIsaac = new Isaac(var2);
        this.isaacOutgoing = new Isaac(var2);
    }

    final void writePacket(int var1) throws IOException {
        if (this.socketException) {
            this.socketException = false;
            this.writeBuffer.offset = 3;
            this.packetStart = 0;
            throw new IOException(this.socketExceptionMessage);
        } else {
            ++this.anInt733;
            if (var1 <= this.anInt733) {
                if (this.packetStart > 0) {
                    this.anInt733 = 0;
                    this.method476(this.packetStart, this.writeBuffer.buffer, 0);
                }

                this.packetStart = 0;
                this.writeBuffer.offset = 3;
            }
        }
    }

    void method471(int var1, int var2, byte[] var3) throws IOException {
    }

    private void readBytes(byte[] var2, int var3) throws IOException {
        this.method471(0, var3, var2);
    }

    final int readPacket(BufferBase_Sub3_Sub1 var2) {
        var2.offset = 0;
        return this.readData(var2.buffer);
    }

    final void sendPacket() {
        int n;
        if (this.isaacOutgoing != null) {
            n = this.writeBuffer.buffer[2 + this.packetStart] & 255;
            this.writeBuffer.buffer[2 + this.packetStart] = (byte) (this.isaacOutgoing.next() + n);
        }

        n = -this.packetStart + this.writeBuffer.offset - 2;
        if (n >= 160) {
            this.writeBuffer.buffer[this.packetStart] = (byte) (n / 256 + 160);
            this.writeBuffer.buffer[this.packetStart + 1] = (byte) Utility.bitwiseAnd(255, n);
        } else {
            this.writeBuffer.buffer[this.packetStart] = (byte) n;
            --this.writeBuffer.offset;
            this.writeBuffer.buffer[this.packetStart + 1] = this.writeBuffer.buffer[this.writeBuffer.offset];
        }

        if (this.packetMaxLength <= 10000) {
            int var3 = 255 & this.writeBuffer.buffer[this.packetStart + 2];
            ++Isaac.anIntArray444[var3];
            anIntArray810[var3] += this.writeBuffer.offset - this.packetStart;
        }

        this.packetStart = this.writeBuffer.offset;
    }

    void method476(int var1, byte[] var2, int var4) throws IOException {
    }

    private int readData(byte[] buff) {
        try {
            ++this.readTries;
            if (this.maxReadTries > 0 && this.readTries > this.maxReadTries) {
                this.socketExceptionMessage = "time-out";
                this.maxReadTries += this.maxReadTries;
                this.socketException = true;
                return 0;
            }

            if (this.length == 0 && this.availableStream() >= 2) {
                this.length = this.read();
                if (this.length >= 160) {
                    this.length = 256 * this.length - ('\ua000' + -this.read());
                }
            }

            if (this.length > 0 && this.availableStream() >= this.length) {
                if (this.length < 160) {
                    buff[-1 + this.length] = (byte) this.read();
                    if (this.length > 1) {
                        this.readBytes(buff, this.length + -1);
                    }
                } else {
                    this.readBytes(buff, this.length);
                }

                int var3 = this.length;
                this.length = 0;
                this.readTries = 0;
                return var3;
            }
        } catch (IOException var4) {
            this.socketException = true;
            this.socketExceptionMessage = var4.getMessage();
        }

        return 0;
    }
}
