import java.io.IOException;

final class ProxyAuthenticationException extends IOException {

    ProxyAuthenticationException(String message) {
        super(message);
    }

}
