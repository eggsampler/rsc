import java.awt.*;

final class SoundPlayerDirectX extends SoundPlayer {

    private static DirectSound directSound;
    private int anInt1706;


    SoundPlayerDirectX(CachePackageManager var1, int var2) {
        directSound = (DirectSound) var1.method589((byte) 127);
        this.anInt1706 = var2;
    }

    final void close() {
        directSound.method3((byte) 93, this.anInt1706);
    }

    final void start(int var1) throws Exception {
        if (var1 > '\u8000') {
            throw new IllegalArgumentException();
        } else {
            directSound.method4(-23386, this.anInt1706, var1);
        }
    }

    final int getPosition() {
        return directSound.method5(this.anInt1706, -6108);
    }

    final void init(Component var1) throws Exception {
        directSound.method1(var1, 126, sampleRate, stereo);
    }

    final void flush() {
        directSound.method2(this.anInt1706, super.anIntArray767);
    }
}
