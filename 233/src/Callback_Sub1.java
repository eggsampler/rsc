import com.ms.awt.WComponentPeer;
import com.ms.dll.Callback;
import com.ms.dll.Root;
import com.ms.win32.User32;

import java.awt.*;

final class Callback_Sub1 extends Callback {

    private volatile int anInt1;
    private volatile int anInt2;
    private int anInt3;
    private boolean aBoolean4;
    private volatile boolean aBoolean5 = true;


    final void method6(Component var1, int var2, boolean var3) {
        WComponentPeer var4 = (WComponentPeer) var1.getPeer();
        int var5 = var4.getTopHwnd();
        if (var5 != this.anInt1 || !var3 == this.aBoolean5) {
            if (!this.aBoolean4) {
                this.anInt3 = User32.LoadCursor(0, 32512);
                Root.alloc(this);
                this.aBoolean4 = true;
            }

            if (this.anInt1 != var5) {
                if (this.anInt1 != 0) {
                    this.aBoolean5 = true;
                    User32.SendMessage(var5, 101024, 0, 0);
                    synchronized (this) {
                        User32.SetWindowLong(this.anInt1, -4, this.anInt2);
                    }
                }

                synchronized (this) {
                    this.anInt1 = var5;
                    this.anInt2 = User32.SetWindowLong(this.anInt1, -4, this);
                }
            }

            this.aBoolean5 = var3;
            if (var2 > -72) {
                this.callback(98, 127, 46, -55);
            }

            User32.SendMessage(var5, 101024, 0, 0);
        }
    }

    final void method7(int var1, int var2, int var3) {
        if (var2 >= 59) {
            User32.SetCursorPos(var1, var3);
        }
    }

    final synchronized int callback(int var1, int var2, int var3, int var4) {
        int var5;
        if (var1 != this.anInt1) {
            var5 = User32.GetWindowLong(var1, -4);
            return User32.CallWindowProc(var5, var1, var2, var3, var4);
        } else {
            if (var2 == 32) {
                var5 = '\uffff' & var4;
                if (var5 == 1) {
                    User32.SetCursor(this.aBoolean5 ? this.anInt3 : 0);
                    return 0;
                }
            }

            if (var2 == 101024) {
                User32.SetCursor(this.aBoolean5 ? this.anInt3 : 0);
                return 0;
            } else {
                if (var2 == 1) {
                    this.anInt1 = 0;
                    this.aBoolean5 = true;
                }

                return User32.CallWindowProc(this.anInt2, var1, var2, var3, var4);
            }
        }
    }

}
