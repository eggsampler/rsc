import java.awt.image.ColorModel;

final class Scene {

    static int anInt762;
    static int anInt465;
    static int[] anIntArray215 = new int[512];
    static int[] anIntArray823 = new int[2048];
    static byte[] aByteArray798;
    static String aString378;
    static ColorModel aColorModel384;
    static boolean[] aBooleanArray418 = new boolean[12];
    int anInt317 = 1000;
    int fogZFalloff = 20;
    int anInt367 = 1000;
    int fogZDistance = 10;
    GameModel view;
    private int[] anIntArray319;
    private GameModel[] aGameModelArray321;
    private int[] anIntArray325;
    private int anInt326 = 0;
    private int[][] anIntArrayArray327;
    private int[][] anIntArrayArray328;
    private int[] anIntArray329;
    private int[] anIntArray330 = new int[40];
    private int anInt332;
    private Polygon[] visibleModelPolygons;
    private int[][] anIntArrayArray335;
    private int[] anIntArray336;
    private int anInt337;
    private int[] anIntArray339;
    private GameModel[] aGameModelArray340;
    private boolean[] aBooleanArray341;
    private int[] anIntArray342 = new int[40];
    private int anInt343;
    private int anInt344;
    private int anInt345;
    private int anInt349;
    private byte[][] aByteArrayArray350;
    private int anInt351 = 8;
    private int anInt356;
    private int anInt357 = 256;
    private int[] anIntArray358;
    private int anInt359;
    private int[] anIntArray360 = new int[40];
    private int[] anIntArray363 = new int[40];
    private int anInt364;
    private int anInt365;
    private int anInt368 = 256;
    private int[] anIntArray370;
    private int anInt371;
    private int anInt373 = 0;
    private int anInt374;
    private int[] anIntArray375;
    private int[] anIntArray377;
    private boolean aBoolean379;
    private int anInt380;
    private int anInt381;
    private int[] anIntArray383;
    private Surface aSurface_385;
    private boolean aBoolean390 = false;
    private int[] anIntArray392;
    private int anInt394 = 100;
    private int anInt395 = 0;
    private Scanline[] scanlines;
    private int anInt401;
    private int anInt404;
    private int anInt407 = 512;
    private int anInt408;
    private int[] anIntArray412 = new int[40];
    private int[][] anIntArrayArray415;
    private int[] anIntArray417;
    private int anInt420;
    private int[] anIntArray421;
    private long[] aLongArray423;
    private int[][] anIntArrayArray424;
    private int[] anIntArray425;
    private boolean aBoolean426;
    private int anInt427;
    private int anInt428;

    Scene(Surface var1, int var2, int var3, int var4) {
        this.aGameModelArray321 = new GameModel[this.anInt394];
        this.aBoolean379 = false;
        this.anInt408 = 50;
        this.anInt349 = 4;
        this.anInt374 = 5;
        this.anIntArray329 = new int[this.anInt394];
        this.anIntArray421 = new int[40];
        this.anInt371 = 256;
        this.anIntArray375 = new int[this.anInt408];
        this.aBoolean426 = false;
        this.anInt420 = 192;
        this.anIntArrayArray424 = new int[this.anInt408][256];

        this.anIntArray319 = var1.pixels;
        this.aSurface_385 = var1;
        this.anInt365 = 0;
        this.anInt356 = var2;
        this.anInt420 = var1.height2 / 2;
        this.anInt357 = var1.width2 / 2;
        this.aGameModelArray340 = new GameModel[this.anInt356];
        this.visibleModelPolygons = new Polygon[var3];
        this.anIntArray377 = new int[this.anInt356];
        this.anInt359 = 0;

        for (int var5 = 0; var5 < var3; ++var5) {
            this.visibleModelPolygons[var5] = new Polygon();
        }

        this.anInt395 = 0;
        this.view = new GameModel(var4 * 2, var4);
        this.anInt345 = 0;
        this.anInt332 = 0;
        this.anIntArray325 = new int[var4];
        this.anIntArray336 = new int[var4];
        this.anInt337 = 0;
        this.anIntArray425 = new int[var4];
        if (aByteArray798 == null) {
            aByteArray798 = new byte[17691];
        }

        this.anIntArray383 = new int[var4];
        this.anInt404 = 0;
        this.anInt364 = 0;
        this.anIntArray370 = new int[var4];
        this.anIntArray392 = new int[var4];
        this.anIntArray339 = new int[var4];
        this.anInt381 = 0;

        for (int var6 = 0; var6 < 256; ++var6) {
            anIntArray215[var6] = (int) (Math.sin((double) var6 * 0.02454369D) * 32768.0D);
            anIntArray215[var6 - -256] = (int) (32768.0D * Math.cos((double) var6 * 0.02454369D));
        }

        for (int var7 = 0; var7 < 1024; ++var7) {
            anIntArray823[var7] = (int) (32768.0D * Math.sin(0.00613592315D * (double) var7));
            anIntArray823[var7 + 1024] = (int) (Math.cos((double) var7 * 0.00613592315D) * 32768.0D);
        }
    }

    static void method607(int var0, int var1, int[] var2, int var3, int var4, int[] var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, int var15) {
        if (var15 > 0) {
            int var16 = 0;
            if (var7 == -13651) {
                int var17 = 0;
                if (var12 != 0) {
                    var17 = var11 / var12 << 7;
                    var16 = var4 / var12 << 7;
                }

                var10 <<= 2;
                if (var16 < 0) {
                    var16 = 0;
                } else if (var16 > 16256) {
                    var16 = 16256;
                }

                for (int var18 = var15; var18 > 0; var18 -= 16) {
                    var0 = var17;
                    var12 += var14;
                    var3 = var16;
                    var11 += var13;
                    var4 += var1;
                    if (var12 != 0) {
                        var16 = var4 / var12 << 7;
                        var17 = var11 / var12 << 7;
                    }

                    if (var16 >= 0) {
                        if (var16 > 16256) {
                            var16 = 16256;
                        }
                    } else {
                        var16 = 0;
                    }

                    int var19 = var16 - var3 >> 4;
                    int var20 = var17 + -var0 >> 4;
                    int var21 = var9 >> 23;
                    var3 += var9 & 6291456;
                    var9 += var10;
                    if (var18 < 16) {
                        for (int var22 = 0; var22 < var18; ++var22) {
                            if ((var8 = var5[(var3 >> 7) + (var0 & 16256)] >>> var21) != 0) {
                                var2[var6] = var8;
                            }

                            ++var6;
                            var3 += var19;
                            var0 += var20;
                            if ((3 & var22) == 3) {
                                var3 = (6291456 & var9) + (16383 & var3);
                                var21 = var9 >> 23;
                                var9 += var10;
                            }
                        }
                    } else {
                        if ((var8 = var5[(var3 >> 7) + (var0 & 16256)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var3 += var19;
                        var0 += var20;
                        ++var6;
                        if ((var8 = var5[(var3 >> 7) + (16256 & var0)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        ++var6;
                        var3 += var19;
                        var0 += var20;
                        if ((var8 = var5[(16256 & var0) + (var3 >> 7)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var0 += var20;
                        ++var6;
                        var3 += var19;
                        if ((var8 = var5[(var0 & 16256) - -(var3 >> 7)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var0 += var20;
                        ++var6;
                        var3 += var19;
                        var21 = var9 >> 23;
                        var3 = (var9 & 6291456) + (16383 & var3);
                        if ((var8 = var5[(16256 & var0) + (var3 >> 7)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var9 += var10;
                        ++var6;
                        var0 += var20;
                        var3 += var19;
                        if ((var8 = var5[(16256 & var0) + (var3 >> 7)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var0 += var20;
                        ++var6;
                        var3 += var19;
                        if ((var8 = var5[(var3 >> 7) + (16256 & var0)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        ++var6;
                        var0 += var20;
                        var3 += var19;
                        if ((var8 = var5[(var3 >> 7) + (var0 & 16256)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        ++var6;
                        var3 += var19;
                        var0 += var20;
                        var3 = (var9 & 6291456) + (var3 & 16383);
                        var21 = var9 >> 23;
                        if ((var8 = var5[(var0 & 16256) + (var3 >> 7)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var9 += var10;
                        ++var6;
                        var0 += var20;
                        var3 += var19;
                        if ((var8 = var5[(var3 >> 7) + (16256 & var0)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        ++var6;
                        var0 += var20;
                        var3 += var19;
                        if ((var8 = var5[(16256 & var0) - -(var3 >> 7)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        ++var6;
                        var0 += var20;
                        var3 += var19;
                        if ((var8 = var5[(var3 >> 7) + (var0 & 16256)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        ++var6;
                        var3 += var19;
                        var0 += var20;
                        var3 = (var3 & 16383) + (var9 & 6291456);
                        var21 = var9 >> 23;
                        var9 += var10;
                        if ((var8 = var5[(var3 >> 7) + (16256 & var0)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var3 += var19;
                        var0 += var20;
                        ++var6;
                        if ((var8 = var5[(16256 & var0) + (var3 >> 7)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var0 += var20;
                        ++var6;
                        var3 += var19;
                        if ((var8 = var5[(var3 >> 7) + (16256 & var0)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        var0 += var20;
                        var3 += var19;
                        ++var6;
                        if ((var8 = var5[(var3 >> 7) + (16256 & var0)] >>> var21) != 0) {
                            var2[var6] = var8;
                        }

                        ++var6;
                    }
                }

            }
        }
    }

    static void method581(int[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int[] var10, int var11, int var12, int var13, int var14) {
        if (var12 > 0) {
            int var15 = 0;
            if (var2 >= 113) {
                int var16 = 0;
                if (var9 != 0) {
                    var16 = var7 / var9 << 6;
                    var15 = var5 / var9 << 6;
                }

                var1 <<= 2;
                if (var15 >= 0) {
                    if (var15 > 4032) {
                        var15 = 4032;
                    }
                } else {
                    var15 = 0;
                }

                for (int var17 = var12; var17 > 0; var17 -= 16) {
                    var14 = var15;
                    var9 += var6;
                    var11 = var16;
                    var7 += var13;
                    var5 += var3;
                    if (var9 != 0) {
                        var15 = var5 / var9 << 6;
                        var16 = var7 / var9 << 6;
                    }

                    if (var15 < 0) {
                        var15 = 0;
                    } else if (var15 > 4032) {
                        var15 = 4032;
                    }

                    int var18 = var16 - var11 >> 4;
                    int var19 = var15 - var14 >> 4;
                    int var20 = var8 >> 20;
                    var14 += var8 & 786432;
                    var8 += var1;
                    if (var17 < 16) {
                        for (int var21 = 0; var21 < var17; ++var21) {
                            var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(var11, 4032)] >>> var20;
                            var14 += var19;
                            var11 += var18;
                            if ((var21 & 3) == 3) {
                                var14 = (var14 & 4095) + (var8 & 786432);
                                var20 = var8 >> 20;
                                var8 += var1;
                            }
                        }
                    } else {
                        var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(4032, var11)] >>> var20;
                        var14 += var19;
                        var11 += var18;
                        var10[var4++] = var0[Utility.bitwiseAnd(var11, 4032) + (var14 >> 6)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(4032, var11)] >>> var20;
                        var14 += var19;
                        var11 += var18;
                        var10[var4++] = var0[Utility.bitwiseAnd(var11, 4032) + (var14 >> 6)] >>> var20;
                        var14 += var19;
                        var11 += var18;
                        var20 = var8 >> 20;
                        var14 = (4095 & var14) + (var8 & 786432);
                        var8 += var1;
                        var10[var4++] = var0[Utility.bitwiseAnd(4032, var11) - -(var14 >> 6)] >>> var20;
                        var14 += var19;
                        var11 += var18;
                        var10[var4++] = var0[Utility.bitwiseAnd(4032, var11) - -(var14 >> 6)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(var11, 4032)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var10[var4++] = var0[Utility.bitwiseAnd(var11, 4032) + (var14 >> 6)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var14 = (4095 & var14) + (786432 & var8);
                        var20 = var8 >> 20;
                        var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(var11, 4032)] >>> var20;
                        var8 += var1;
                        var11 += var18;
                        var14 += var19;
                        var10[var4++] = var0[Utility.bitwiseAnd(var11, 4032) + (var14 >> 6)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var10[var4++] = var0[Utility.bitwiseAnd(4032, var11) - -(var14 >> 6)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var10[var4++] = var0[Utility.bitwiseAnd(4032, var11) + (var14 >> 6)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var20 = var8 >> 20;
                        var14 = (var8 & 786432) + (4095 & var14);
                        var8 += var1;
                        var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(var11, 4032)] >>> var20;
                        var14 += var19;
                        var11 += var18;
                        var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(4032, var11)] >>> var20;
                        var14 += var19;
                        var11 += var18;
                        var10[var4++] = var0[(var14 >> 6) + Utility.bitwiseAnd(4032, var11)] >>> var20;
                        var11 += var18;
                        var14 += var19;
                        var10[var4++] = var0[Utility.bitwiseAnd(var11, 4032) - -(var14 >> 6)] >>> var20;
                    }
                }

            }
        }
    }

    static void method487(int var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int[] var8, int var9, int var10, int var11, int var12, int[] var13, int var14) {
        if (var4 > ~var1) {
            int var15 = 0;
            int var16 = 0;
            if (var2 != 0) {
                var6 = var9 / var2 << 7;
                var0 = var11 / var2 << 7;
            }

            int var17 = 0;
            var11 += var3;
            var9 += var5;
            var2 += var12;
            if (var6 < 0) {
                var6 = 0;
            } else if (var6 > 16256) {
                var6 = 16256;
            }

            if (var2 != 0) {
                var16 = var11 / var2 << 7;
                var15 = var9 / var2 << 7;
            }

            if (var15 < 0) {
                var15 = 0;
            } else if (var15 > 16256) {
                var15 = 16256;
            }

            int var18 = var15 + -var6 >> 4;
            int var19 = var16 + -var0 >> 4;

            for (int var20 = var1 >> 4; var20 > 0; --var20) {
                var6 += 6291456 & var10;
                var17 = var10 >> 23;
                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) - -(var6 >> 7)] >>> var17;
                var10 += var7;
                var6 += var18;
                var0 += var19;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(var0, 16256)] >>> var17;
                var6 += var18;
                var0 += var19;
                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) + (var6 >> 7)] >>> var17;
                var6 += var18;
                var0 += var19;
                var8[var14++] = var13[Utility.bitwiseAnd(var0, 16256) + (var6 >> 7)] >>> var17;
                var6 += var18;
                var0 += var19;
                var6 = (var10 & 6291456) + (var6 & 16383);
                var17 = var10 >> 23;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(16256, var0)] >>> var17;
                var10 += var7;
                var0 += var19;
                var6 += var18;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(var0, 16256)] >>> var17;
                var6 += var18;
                var0 += var19;
                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) + (var6 >> 7)] >>> var17;
                var0 += var19;
                var6 += var18;
                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) - -(var6 >> 7)] >>> var17;
                var6 += var18;
                var0 += var19;
                var6 = (var10 & 6291456) + (16383 & var6);
                var17 = var10 >> 23;
                var10 += var7;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(16256, var0)] >>> var17;
                var0 += var19;
                var6 += var18;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(var0, 16256)] >>> var17;
                var6 += var18;
                var0 += var19;
                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) - -(var6 >> 7)] >>> var17;
                var6 += var18;
                var0 += var19;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(var0, 16256)] >>> var17;
                var6 += var18;
                var0 += var19;
                var6 = (6291456 & var10) + (var6 & 16383);
                var17 = var10 >> 23;
                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) + (var6 >> 7)] >>> var17;
                var10 += var7;
                var0 += var19;
                var6 += var18;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(16256, var0)] >>> var17;
                var6 += var18;
                var0 += var19;
                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) - -(var6 >> 7)] >>> var17;
                var0 += var19;
                var6 += var18;
                var8[var14++] = var13[(var6 >> 7) + Utility.bitwiseAnd(16256, var0)] >>> var17;
                var2 += var12;
                var0 = var16;
                var11 += var3;
                var9 += var5;
                var6 = var15;
                if (var2 != 0) {
                    var16 = var11 / var2 << 7;
                    var15 = var9 / var2 << 7;
                }

                if (var15 < 0) {
                    var15 = 0;
                } else if (var15 > 16256) {
                    var15 = 16256;
                }

                var18 = var15 + -var6 >> 4;
                var19 = -var16 + var16 >> 4;
            }

            for (int var21 = 0; (var1 & 15) > var21; ++var21) {
                if ((var21 & 3) == 0) {
                    var17 = var10 >> 23;
                    var6 = (16383 & var6) + (6291456 & var10);
                    var10 += var7;
                }

                var8[var14++] = var13[Utility.bitwiseAnd(16256, var0) + (var6 >> 7)] >>> var17;
                var0 += var19;
                var6 += var18;
            }

        }
    }

    static void method353(int var0, int var1, int var2, int var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int[] var10, int var11, int var12, int var13, int var14) {
        if (var2 > 0) {
            int var15 = 0;
            if (var14 != -27223) {
                method353(100, -80, 0, -111, (int[]) null, 72, 124, 124, -14, 8, (int[]) null, -8, 59, 68, 23);
            }

            int var16 = 0;
            var5 <<= 2;
            if (var1 != 0) {
                var15 = var8 / var1 << 6;
                var16 = var0 / var1 << 6;
            }

            if (var15 >= 0) {
                if (var15 > 4032) {
                    var15 = 4032;
                }
            } else {
                var15 = 0;
            }

            for (int var17 = var2; var17 > 0; var17 -= 16) {
                var0 += var3;
                var9 = var15;
                var8 += var13;
                var6 = var16;
                var1 += var7;
                if (var1 != 0) {
                    var15 = var8 / var1 << 6;
                    var16 = var0 / var1 << 6;
                }

                if (var15 >= 0) {
                    if (var15 > 4032) {
                        var15 = 4032;
                    }
                } else {
                    var15 = 0;
                }

                int var18 = var15 - var9 >> 4;
                int var19 = var16 - var6 >> 4;
                var9 += 786432 & var11;
                int var20 = var11 >> 20;
                var11 += var5;
                if (var17 >= 16) {
                    var4[var12++] = Utility.bitwiseAnd(8355711, var4[var12] >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20);
                    var6 += var19;
                    var9 += var18;
                    var4[var12++] = Utility.bitwiseAnd(8355711, var4[var12] >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(4032, var6)] >>> var20);
                    var6 += var19;
                    var9 += var18;
                    var4[var12++] = (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20) + Utility.bitwiseAnd(var4[var12] >> 1, 8355711);
                    var9 += var18;
                    var6 += var19;
                    var4[var12++] = (Utility.bitwiseAnd(var4[var12], 16711422) >> 1) + (var10[Utility.bitwiseAnd(4032, var6) - -(var9 >> 6)] >>> var20);
                    var9 += var18;
                    var6 += var19;
                    var20 = var11 >> 20;
                    var9 = (var11 & 786432) + (var9 & 4095);
                    var4[var12++] = (var10[(var9 >> 6) + Utility.bitwiseAnd(4032, var6)] >>> var20) + (Utility.bitwiseAnd(var4[var12], 16711423) >> 1);
                    var11 += var5;
                    var9 += var18;
                    var6 += var19;
                    var4[var12++] = (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20) + (Utility.bitwiseAnd(var4[var12], 16711423) >> 1);
                    var6 += var19;
                    var9 += var18;
                    var4[var12++] = Utility.bitwiseAnd(8355711, var4[var12] >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(4032, var6)] >>> var20);
                    var9 += var18;
                    var6 += var19;
                    var4[var12++] = Utility.bitwiseAnd(var4[var12] >> 1, 8355711) + (var10[Utility.bitwiseAnd(var6, 4032) + (var9 >> 6)] >>> var20);
                    var9 += var18;
                    var6 += var19;
                    var20 = var11 >> 20;
                    var9 = (var9 & 4095) + (786432 & var11);
                    var11 += var5;
                    var4[var12++] = (var10[Utility.bitwiseAnd(var6, 4032) + (var9 >> 6)] >>> var20) - -(Utility.bitwiseAnd(16711423, var4[var12]) >> 1);
                    var9 += var18;
                    var6 += var19;
                    var4[var12++] = (Utility.bitwiseAnd(var4[var12], 16711422) >> 1) + (var10[Utility.bitwiseAnd(4032, var6) - -(var9 >> 6)] >>> var20);
                    var6 += var19;
                    var9 += var18;
                    var4[var12++] = (Utility.bitwiseAnd(var4[var12], 16711422) >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20);
                    var6 += var19;
                    var9 += var18;
                    var4[var12++] = (var10[Utility.bitwiseAnd(4032, var6) + (var9 >> 6)] >>> var20) + (Utility.bitwiseAnd(var4[var12], 16711422) >> 1);
                    var6 += var19;
                    var9 += var18;
                    var20 = var11 >> 20;
                    var9 = (var9 & 4095) + (var11 & 786432);
                    var4[var12++] = (Utility.bitwiseAnd(var4[var12], 16711423) >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20);
                    var11 += var5;
                    var9 += var18;
                    var6 += var19;
                    var4[var12++] = (Utility.bitwiseAnd(16711422, var4[var12]) >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20);
                    var6 += var19;
                    var9 += var18;
                    var4[var12++] = Utility.bitwiseAnd(8355711, var4[var12] >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20);
                    var6 += var19;
                    var9 += var18;
                    var4[var12++] = (Utility.bitwiseAnd(16711422, var4[var12]) >> 1) + (var10[(var9 >> 6) + Utility.bitwiseAnd(4032, var6)] >>> var20);
                } else {
                    for (int var21 = 0; var21 < var17; ++var21) {
                        var4[var12++] = (var10[(var9 >> 6) + Utility.bitwiseAnd(var6, 4032)] >>> var20) - -Utility.bitwiseAnd(var4[var12] >> 1, 8355711);
                        var6 += var19;
                        var9 += var18;
                        if ((var21 & 3) == 3) {
                            var9 = (4095 & var9) + (786432 & var11);
                            var20 = var11 >> 20;
                            var11 += var5;
                        }
                    }
                }
            }

        }
    }

    static void method295(int[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        if (var6 < 0) {
            var7 <<= 2;
            var3 = var0[255 & var5 >> 8];
            var5 += var7;
            int var8 = var6 / 16;

            for (int var9 = var8; var9 < 0; ++var9) {
                var1[var4++] = var3 + (Utility.bitwiseAnd(var1[var4], 16711423) >> 1);
                var1[var4++] = var3 + Utility.bitwiseAnd(8355711, var1[var4] >> 1);
                var1[var4++] = var3 + (Utility.bitwiseAnd(var1[var4], 16711422) >> 1);
                var1[var4++] = Utility.bitwiseAnd(8355711, var1[var4] >> 1) + var3;
                var3 = var0[('\uff49' & var5) >> 8];
                var1[var4++] = var3 + Utility.bitwiseAnd(var1[var4] >> 1, 8355711);
                var5 += var7;
                var1[var4++] = (Utility.bitwiseAnd(var1[var4], 16711422) >> 1) + var3;
                var1[var4++] = var3 + Utility.bitwiseAnd(8355711, var1[var4] >> 1);
                var1[var4++] = (Utility.bitwiseAnd(var1[var4], 16711422) >> 1) + var3;
                var3 = var0[255 & var5 >> 8];
                var5 += var7;
                var1[var4++] = Utility.bitwiseAnd(8355711, var1[var4] >> 1) + var3;
                var1[var4++] = Utility.bitwiseAnd(var1[var4] >> 1, 8355711) + var3;
                var1[var4++] = var3 + (Utility.bitwiseAnd(var1[var4], 16711423) >> 1);
                var1[var4++] = Utility.bitwiseAnd(var1[var4] >> 1, 8355711) + var3;
                var3 = var0[('\uffc4' & var5) >> 8];
                var1[var4++] = var3 - -Utility.bitwiseAnd(8355711, var1[var4] >> 1);
                var5 += var7;
                var1[var4++] = var3 + Utility.bitwiseAnd(var1[var4] >> 1, 8355711);
                var1[var4++] = var3 + Utility.bitwiseAnd(var1[var4] >> 1, 8355711);
                var1[var4++] = Utility.bitwiseAnd(8355711, var1[var4] >> 1) + var3;
                var3 = var0[var5 >> 8 & 255];
                var5 += var7;
            }

            var8 = -(var6 % 16);
            if (var2 == -747942559) {
                for (int var10 = 0; var8 > var10; ++var10) {
                    var1[var4++] = Utility.bitwiseAnd(8355711, var1[var4] >> 1) + var3;
                    if ((3 & var10) == 3) {
                        var3 = var0[(var5 & '\uff65') >> 8];
                        var5 += var7;
                        var5 += var7;
                    }
                }

            }
        }
    }

    static void method148(int[] var0, int var1, int var2, int var3, int[] var4, int var5, int var6, int var7) {
        if (var6 < 0) {
            var5 <<= 2;
            var7 = var0[(var3 & '\uff59') >> 8];
            var3 += var5;
            int var8 = var6 / 16;
            if (var2 >= 67) {
                for (int var9 = var8; var9 < 0; ++var9) {
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var7 = var0[var3 >> 8 & 255];
                    var4[var1++] = var7;
                    var3 += var5;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var7 = var0[(var3 & '\uffbf') >> 8];
                    var4[var1++] = var7;
                    var3 += var5;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var7 = var0[var3 >> 8 & 255];
                    var4[var1++] = var7;
                    var3 += var5;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var4[var1++] = var7;
                    var7 = var0[255 & var3 >> 8];
                    var3 += var5;
                }

                var8 = -(var6 % 16);

                for (int var10 = 0; var8 > var10; ++var10) {
                    var4[var1++] = var7;
                    if ((3 & var10) == 3) {
                        var7 = var0[('\uff4f' & var3) >> 8];
                        var3 += var5;
                    }
                }

            }
        }
    }

    static void method202(int[] var0, int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int[] var9, int var10, int var11, int var12, int var13, int var14) {
        if (var14 > 0) {
            int var15 = 0;
            int var16 = 0;
            int var17 = 0;
            if (var7 != 0) {
                var13 = var8 / var7 << 7;
                var6 = var12 / var7 << 7;
            }

            var12 += var5;
            var8 += var2;
            if (var6 < 0) {
                var6 = 0;
            } else if (var6 > 16256) {
                var6 = 16256;
            }

            var7 += var4;
            if (var3 != var7) {
                var15 = var12 / var7 << 7;
                var16 = var8 / var7 << 7;
            }

            if (var15 >= 0) {
                if (var15 > 16256) {
                    var15 = 16256;
                }
            } else {
                var15 = 0;
            }

            int var18 = var16 - var13 >> 4;
            int var19 = -var6 + var15 >> 4;

            for (int var20 = var14 >> 4; var20 > 0; --var20) {
                var6 += var1 & 6291456;
                var17 = var1 >> 23;
                var9[var10++] = Utility.bitwiseAnd(var9[var10] >> 1, 8355711) + (var0[Utility.bitwiseAnd(16256, var13) - -(var6 >> 7)] >>> var17);
                var1 += var11;
                var13 += var18;
                var6 += var19;
                var9[var10++] = (Utility.bitwiseAnd(16711422, var9[var10]) >> 1) + (var0[Utility.bitwiseAnd(var13, 16256) - -(var6 >> 7)] >>> var17);
                var13 += var18;
                var6 += var19;
                var9[var10++] = Utility.bitwiseAnd(8355711, var9[var10] >> 1) + (var0[(var6 >> 7) + Utility.bitwiseAnd(16256, var13)] >>> var17);
                var13 += var18;
                var6 += var19;
                var9[var10++] = Utility.bitwiseAnd(8355711, var9[var10] >> 1) + (var0[Utility.bitwiseAnd(16256, var13) - -(var6 >> 7)] >>> var17);
                var13 += var18;
                var6 += var19;
                var6 = (16383 & var6) - -(var1 & 6291456);
                var17 = var1 >> 23;
                var1 += var11;
                var9[var10++] = (var0[Utility.bitwiseAnd(var13, 16256) + (var6 >> 7)] >>> var17) - -Utility.bitwiseAnd(8355711, var9[var10] >> 1);
                var13 += var18;
                var6 += var19;
                var9[var10++] = Utility.bitwiseAnd(var9[var10] >> 1, 8355711) + (var0[(var6 >> 7) + Utility.bitwiseAnd(var13, 16256)] >>> var17);
                var6 += var19;
                var13 += var18;
                var9[var10++] = (var0[Utility.bitwiseAnd(var13, 16256) - -(var6 >> 7)] >>> var17) + Utility.bitwiseAnd(8355711, var9[var10] >> 1);
                var6 += var19;
                var13 += var18;
                var9[var10++] = (var0[Utility.bitwiseAnd(16256, var13) + (var6 >> 7)] >>> var17) + Utility.bitwiseAnd(var9[var10] >> 1, 8355711);
                var13 += var18;
                var6 += var19;
                var17 = var1 >> 23;
                var6 = (var6 & 16383) - -(6291456 & var1);
                var9[var10++] = (Utility.bitwiseAnd(var9[var10], 16711422) >> 1) + (var0[Utility.bitwiseAnd(var13, 16256) - -(var6 >> 7)] >>> var17);
                var1 += var11;
                var13 += var18;
                var6 += var19;
                var9[var10++] = (var0[(var6 >> 7) + Utility.bitwiseAnd(16256, var13)] >>> var17) - -(Utility.bitwiseAnd(var9[var10], 16711423) >> 1);
                var6 += var19;
                var13 += var18;
                var9[var10++] = (Utility.bitwiseAnd(var9[var10], 16711423) >> 1) + (var0[(var6 >> 7) + Utility.bitwiseAnd(var13, 16256)] >>> var17);
                var13 += var18;
                var6 += var19;
                var9[var10++] = (var0[(var6 >> 7) + Utility.bitwiseAnd(16256, var13)] >>> var17) + (Utility.bitwiseAnd(var9[var10], 16711423) >> 1);
                var6 += var19;
                var13 += var18;
                var6 = (var6 & 16383) + (var1 & 6291456);
                var17 = var1 >> 23;
                var9[var10++] = (var0[(var6 >> 7) + Utility.bitwiseAnd(var13, 16256)] >>> var17) - -Utility.bitwiseAnd(8355711, var9[var10] >> 1);
                var1 += var11;
                var13 += var18;
                var6 += var19;
                var9[var10++] = Utility.bitwiseAnd(var9[var10] >> 1, 8355711) + (var0[(var6 >> 7) + Utility.bitwiseAnd(var13, 16256)] >>> var17);
                var6 += var19;
                var13 += var18;
                var9[var10++] = (var0[Utility.bitwiseAnd(16256, var13) + (var6 >> 7)] >>> var17) + Utility.bitwiseAnd(8355711, var9[var10] >> 1);
                var6 += var19;
                var13 += var18;
                var9[var10++] = (var0[Utility.bitwiseAnd(16256, var13) + (var6 >> 7)] >>> var17) + Utility.bitwiseAnd(var9[var10] >> 1, 8355711);
                var8 += var2;
                var13 = var16;
                var12 += var5;
                var7 += var4;
                var6 = var15;
                if (var7 != 0) {
                    var16 = var8 / var7 << 7;
                    var15 = var12 / var7 << 7;
                }

                if (var15 < 0) {
                    var15 = 0;
                } else if (var15 > 16256) {
                    var15 = 16256;
                }

                var18 = var16 - var13 >> 4;
                var19 = -var6 + var15 >> 4;
            }

            for (int var21 = 0; (var14 & 15) > var21; ++var21) {
                if ((3 & var21) == 0) {
                    var17 = var1 >> 23;
                    var6 = (var1 & 6291456) + (16383 & var6);
                    var1 += var11;
                }

                var9[var10++] = (var0[(var6 >> 7) + Utility.bitwiseAnd(var13, 16256)] >>> var17) - -(Utility.bitwiseAnd(16711422, var9[var10]) >> 1);
                var13 += var18;
                var6 += var19;
            }

        }
    }

    static void method228(int var0, int var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int[] var12, int var13, int var14, int var15) {
        if (var6 > 0) {
            int var16 = 0;
            int var17 = var10;
            var1 <<= 2;
            if (var7 != 0) {
                var16 = var5 / var7 << 6;
                var17 = var15 / var7 << 6;
            }

            if (var16 >= 0) {
                if (var16 > 4032) {
                    var16 = 4032;
                }
            } else {
                var16 = 0;
            }

            for (int var18 = var6; var18 > 0; var18 -= 16) {
                var5 += var13;
                var15 += var11;
                var7 += var0;
                var3 = var16;
                var4 = var17;
                if (var7 != 0) {
                    var16 = var5 / var7 << 6;
                    var17 = var15 / var7 << 6;
                }

                if (var16 >= 0) {
                    if (var16 > 4032) {
                        var16 = 4032;
                    }
                } else {
                    var16 = 0;
                }

                int var19 = -var17 + var17 >> 4;
                int var20 = var16 + -var3 >> 4;
                var3 += 786432 & var8;
                int var21 = var8 >> 20;
                var8 += var1;
                if (var18 < 16) {
                    for (int var22 = 0; var22 < var18; ++var22) {
                        if ((var14 = var2[(4032 & var4) + (var3 >> 6)] >>> var21) != 0) {
                            var12[var9] = var14;
                        }

                        var3 += var20;
                        var4 += var19;
                        ++var9;
                        if ((var22 & 3) == 3) {
                            var21 = var8 >> 20;
                            var3 = (4095 & var3) + (786432 & var8);
                            var8 += var1;
                        }
                    }
                } else {
                    if ((var14 = var2[(4032 & var4) + (var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                    var4 += var19;
                    var3 += var20;
                    if ((var14 = var2[(4032 & var4) - -(var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                    var4 += var19;
                    var3 += var20;
                    if ((var14 = var2[(var3 >> 6) + (var4 & 4032)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var3 += var20;
                    ++var9;
                    var4 += var19;
                    if ((var14 = var2[(var3 >> 6) + (4032 & var4)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                    var3 += var20;
                    var4 += var19;
                    var21 = var8 >> 20;
                    var3 = (786432 & var8) + (4095 & var3);
                    if ((var14 = var2[(var3 >> 6) + (4032 & var4)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var8 += var1;
                    var4 += var19;
                    ++var9;
                    var3 += var20;
                    if ((var14 = var2[(4032 & var4) + (var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var3 += var20;
                    var4 += var19;
                    ++var9;
                    if ((var14 = var2[(var3 >> 6) + (4032 & var4)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                    var4 += var19;
                    var3 += var20;
                    if ((var14 = var2[(var3 >> 6) + (var4 & 4032)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                    var3 += var20;
                    var4 += var19;
                    var3 = (4095 & var3) + (786432 & var8);
                    var21 = var8 >> 20;
                    var8 += var1;
                    if ((var14 = var2[(var3 >> 6) + (4032 & var4)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var3 += var20;
                    ++var9;
                    var4 += var19;
                    if ((var14 = var2[(var4 & 4032) - -(var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var3 += var20;
                    ++var9;
                    var4 += var19;
                    if ((var14 = var2[(var3 >> 6) + (var4 & 4032)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var4 += var19;
                    ++var9;
                    var3 += var20;
                    if ((var14 = var2[(var3 >> 6) + (var4 & 4032)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                    var4 += var19;
                    var3 += var20;
                    var3 = (4095 & var3) + (786432 & var8);
                    var21 = var8 >> 20;
                    if ((var14 = var2[(var4 & 4032) + (var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var8 += var1;
                    var3 += var20;
                    var4 += var19;
                    ++var9;
                    if ((var14 = var2[(4032 & var4) - -(var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    var3 += var20;
                    ++var9;
                    var4 += var19;
                    if ((var14 = var2[(var4 & 4032) - -(var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                    var4 += var19;
                    var3 += var20;
                    if ((var14 = var2[(4032 & var4) + (var3 >> 6)] >>> var21) != 0) {
                        var12[var9] = var14;
                    }

                    ++var9;
                }
            }

        }
    }

    static int method307(int var0, int var1, int var2, int var3) {
        if (var1 >= -106) {
            method307(-105, -46, -65, -106);
        }

        return -(var0 / 8) + -(var2 / 8 * 1024) + (-1 - var3 / 8 * 32);
    }

    static int method311(int var0, boolean var1) {
        var0 = (var0 & 1431655765) - -(1431655765 & var0 >>> 1);
        var0 = (858993459 & var0 >>> 2) + (var0 & 858993459);
        if (!var1) {
            return 13;
        } else {
            var0 = var0 + (var0 >>> 4) & 252645135;
            var0 += var0 >>> 8;
            var0 += var0 >>> 16;
            return var0 & 255;
        }
    }

    private void method296(GameModel var1, int[] var2, int var3, int[] var4, int[] var5, int var6, boolean var7, int var8, int var9) {
        if (!var7) {
            if (var9 != -2) {
                int var10;
                int var11;
                int var12;
                int var13;
                int var14;
                int var15;
                int var17;
                int var16;
                int var19;
                int var18;
                if (var9 >= 0) {
                    if (var9 >= this.anInt326) {
                        var9 = 0;
                    }

                    this.method333(false, var9);
                    var10 = var5[0];
                    var11 = var4[0];
                    var12 = var2[0];
                    var13 = var10 + -var5[1];
                    var14 = -var4[1] + var11;
                    var15 = -var2[1] + var12;
                    --var6;
                    var16 = var5[var6] + -var10;
                    var17 = var4[var6] - var11;
                    var18 = -var12 + var2[var6];
                    int var21;
                    int var20;
                    int var23;
                    int var22;
                    int var25;
                    int var24;
                    int var27;
                    int var26;
                    int var29;
                    int var28;
                    int var31;
                    int var30;
                    byte var34;
                    Scanline var35;
                    int var32;
                    int var33;
                    int var38;
                    int var39;
                    int var36;
                    int var37;
                    if (this.anIntArray417[var9] == 1) {
                        var19 = var16 * var11 + -(var10 * var17) << 12;
                        var20 = var12 * var17 - var18 * var11 << 5 + -this.anInt351 + 11;
                        var21 = -(var16 * var12) + var18 * var10 << -this.anInt351 + 5 + 7;
                        var22 = var13 * var11 - var10 * var14 << 12;
                        var23 = -(var15 * var11) + var12 * var14 << 11 + (5 - this.anInt351);
                        var24 = -(var13 * var12) + var15 * var10 << 7 + -this.anInt351 + 5;
                        var25 = var16 * var14 + -(var17 * var13) << 5;
                        var26 = var15 * var17 + -(var18 * var14) << 4 + -this.anInt351 + 5;
                        var27 = var13 * var18 - var15 * var16 >> -5 + this.anInt351;
                        var28 = var20 >> 4;
                        var29 = var23 >> 4;
                        var30 = var26 >> 4;
                        var31 = this.anInt343 + -this.anInt368;
                        var32 = this.anInt407;
                        var33 = this.anInt371 + var32 * this.anInt343;
                        var25 += var27 * var31;
                        var22 += var24 * var31;
                        var34 = 1;
                        var19 += var31 * var21;
                        if (this.aBoolean390) {
                            if ((this.anInt343 & 1) == 1) {
                                var19 += var21;
                                var33 += var32;
                                var25 += var27;
                                var22 += var24;
                                ++this.anInt343;
                            }

                            var34 = 2;
                            var32 <<= 1;
                            var24 <<= 1;
                            var21 <<= 1;
                            var27 <<= 1;
                        }

                        if (!var1.aBoolean938) {
                            if (this.aBooleanArray341[var9]) {
                                for (var8 = this.anInt343; this.anInt380 > var8; var8 += var34) {
                                    var35 = this.scanlines[var8];
                                    var3 = var35.lowx >> 8;
                                    var36 = var35.highx >> 8;
                                    var37 = -var3 + var36;
                                    if (var37 <= 0) {
                                        var22 += var24;
                                        var25 += var27;
                                        var19 += var21;
                                        var33 += var32;
                                    } else {
                                        var38 = var35.anInt1092;
                                        var39 = (var35.anInt1090 + -var38) / var37;
                                        if (var3 < -this.anInt357) {
                                            var38 += var39 * (-this.anInt357 - var3);
                                            var3 = -this.anInt357;
                                            var37 = -var3 + var36;
                                        }

                                        if (var36 > this.anInt357) {
                                            var36 = this.anInt357;
                                            var37 = -var3 + var36;
                                        }

                                        method607(0, var20, this.anIntArray319, 0, var28 * var3 + var19, this.anIntArrayArray335[var9], var3 + var33, -13651, 0, var38, var39, var3 * var29 + var22, var30 * var3 + var25, var23, var26, var37);
                                        var33 += var32;
                                        var19 += var21;
                                        var22 += var24;
                                        var25 += var27;
                                    }
                                }

                            } else {
                                for (var8 = this.anInt343; var8 < this.anInt380; var8 += var34) {
                                    var35 = this.scanlines[var8];
                                    var3 = var35.lowx >> 8;
                                    var36 = var35.highx >> 8;
                                    var37 = var36 - var3;
                                    if (var37 <= 0) {
                                        var19 += var21;
                                        var33 += var32;
                                        var25 += var27;
                                        var22 += var24;
                                    } else {
                                        var38 = var35.anInt1092;
                                        var39 = (var35.anInt1090 + -var38) / var37;
                                        if (var3 < -this.anInt357) {
                                            var38 += var39 * (-this.anInt357 - var3);
                                            var3 = -this.anInt357;
                                            var37 = var36 - var3;
                                        }

                                        if (this.anInt357 < var36) {
                                            var36 = this.anInt357;
                                            var37 = -var3 + var36;
                                        }

                                        method487(0, var37, var3 * var30 + var25, var23, -1, var20, 0, var39 << 2, this.anIntArray319, var19 - -(var28 * var3), var38, var22 + var29 * var3, var26, this.anIntArrayArray335[var9], var3 + var33);
                                        var25 += var27;
                                        var33 += var32;
                                        var22 += var24;
                                        var19 += var21;
                                    }
                                }

                            }
                        } else {
                            for (var8 = this.anInt343; var8 < this.anInt380; var8 += var34) {
                                var35 = this.scanlines[var8];
                                var3 = var35.lowx >> 8;
                                var36 = var35.highx >> 8;
                                var37 = -var3 + var36;
                                if (var37 <= 0) {
                                    var33 += var32;
                                    var19 += var21;
                                    var25 += var27;
                                    var22 += var24;
                                } else {
                                    var38 = var35.anInt1092;
                                    var39 = (var35.anInt1090 + -var38) / var37;
                                    if (var3 < (-this.anInt357)) {
                                        var38 += (-var3 + -this.anInt357) * var39;
                                        var3 = -this.anInt357;
                                        var37 = var36 - var3;
                                    }

                                    if (this.anInt357 < var36) {
                                        var36 = this.anInt357;
                                        var37 = var36 - var3;
                                    }

                                    method202(this.anIntArrayArray335[var9], var38, var23, 0, var26, var20, 0, var30 * var3 + var25, var22 - -(var29 * var3), this.anIntArray319, var33 + var3, var39 << 2, var3 * var28 + var19, 0, var37);
                                    var33 += var32;
                                    var19 += var21;
                                    var22 += var24;
                                    var25 += var27;
                                }
                            }

                        }
                    } else {
                        var19 = var16 * var11 - var10 * var17 << 11;
                        var20 = -(var18 * var11) + var17 * var12 << 5 - (this.anInt351 + -6 + -4);
                        var21 = var10 * var18 + -(var16 * var12) << 6 + -this.anInt351 + 5;
                        var22 = -(var10 * var14) + var11 * var13 << 11;
                        var23 = -(var11 * var15) + var14 * var12 << 10 + -this.anInt351 + 5;
                        var24 = var10 * var15 + -(var12 * var13) << 6 + -this.anInt351 + 5;
                        var25 = -(var17 * var13) + var14 * var16 << 5;
                        var26 = -(var14 * var18) + var15 * var17 << -this.anInt351 + 9;
                        var27 = var18 * var13 + -(var16 * var15) >> this.anInt351 - 5;
                        var28 = var20 >> 4;
                        var29 = var23 >> 4;
                        var30 = var26 >> 4;
                        var31 = -this.anInt368 + this.anInt343;
                        var32 = this.anInt407;
                        var33 = this.anInt343 * var32 + this.anInt371;
                        var34 = 1;
                        var25 += var31 * var27;
                        var19 += var21 * var31;
                        var22 += var24 * var31;
                        if (this.aBoolean390) {
                            if ((1 & this.anInt343) == 1) {
                                var25 += var27;
                                var19 += var21;
                                var22 += var24;
                                var33 += var32;
                                ++this.anInt343;
                            }

                            var27 <<= 1;
                            var24 <<= 1;
                            var34 = 2;
                            var32 <<= 1;
                            var21 <<= 1;
                        }

                        if (var1.aBoolean938) {
                            for (var8 = this.anInt343; this.anInt380 > var8; var8 += var34) {
                                var35 = this.scanlines[var8];
                                var3 = var35.lowx >> 8;
                                var36 = var35.highx >> 8;
                                var37 = -var3 + var36;
                                if (var37 <= 0) {
                                    var33 += var32;
                                    var22 += var24;
                                    var19 += var21;
                                    var25 += var27;
                                } else {
                                    var38 = var35.anInt1092;
                                    var39 = (var35.anInt1090 + -var38) / var37;
                                    if ((-this.anInt357) > var3) {
                                        var38 += var39 * (-var3 + -this.anInt357);
                                        var3 = -this.anInt357;
                                        var37 = var36 - var3;
                                    }

                                    if (var36 > this.anInt357) {
                                        var36 = this.anInt357;
                                        var37 = -var3 + var36;
                                    }

                                    method353(var29 * var3 + var22, var3 * var30 + var25, var37, var23, this.anIntArray319, var39, 0, var26, var28 * var3 + var19, 0, this.anIntArrayArray335[var9], var38, var3 + var33, var20, -27223);
                                    var19 += var21;
                                    var25 += var27;
                                    var33 += var32;
                                    var22 += var24;
                                }
                            }

                        } else if (!this.aBooleanArray341[var9]) {
                            for (var8 = this.anInt343; var8 < this.anInt380; var8 += var34) {
                                var35 = this.scanlines[var8];
                                var3 = var35.lowx >> 8;
                                var36 = var35.highx >> 8;
                                var37 = var36 + -var3;
                                if (var37 <= 0) {
                                    var33 += var32;
                                    var22 += var24;
                                    var25 += var27;
                                    var19 += var21;
                                } else {
                                    var38 = var35.anInt1092;
                                    var39 = (var35.anInt1090 + -var38) / var37;
                                    if (var3 < (-this.anInt357)) {
                                        var38 += (-var3 + -this.anInt357) * var39;
                                        var3 = -this.anInt357;
                                        var37 = var36 + -var3;
                                    }

                                    if (this.anInt357 < var36) {
                                        var36 = this.anInt357;
                                        var37 = -var3 + var36;
                                    }

                                    method581(this.anIntArrayArray335[var9], var39, 122, var20, var3 + var33, var3 * var28 + var19, var26, var22 + var3 * var29, var38, var3 * var30 + var25, this.anIntArray319, 0, var37, var23, 0);
                                    var25 += var27;
                                    var19 += var21;
                                    var33 += var32;
                                    var22 += var24;
                                }
                            }

                        } else {
                            for (var8 = this.anInt343; var8 < this.anInt380; var8 += var34) {
                                var35 = this.scanlines[var8];
                                var3 = var35.lowx >> 8;
                                var36 = var35.highx >> 8;
                                var37 = var36 + -var3;
                                if (var37 <= 0) {
                                    var25 += var27;
                                    var33 += var32;
                                    var22 += var24;
                                    var19 += var21;
                                } else {
                                    var38 = var35.anInt1092;
                                    var39 = (-var38 + var35.anInt1090) / var37;
                                    if (var3 < (-this.anInt357)) {
                                        var38 += (-this.anInt357 + -var3) * var39;
                                        var3 = -this.anInt357;
                                        var37 = var36 + -var3;
                                    }

                                    if (this.anInt357 < var36) {
                                        var36 = this.anInt357;
                                        var37 = -var3 + var36;
                                    }

                                    method228(var26, var39, this.anIntArrayArray335[var9], 0, 0, var19 - -(var28 * var3), var37, var25 + var3 * var30, var38, var33 - -var3, 0, var23, this.anIntArray319, var20, 0, var3 * var29 + var22);
                                    var22 += var24;
                                    var19 += var21;
                                    var33 += var32;
                                    var25 += var27;
                                }
                            }

                        }
                    }
                } else {
                    for (var10 = 0; this.anInt408 > var10; ++var10) {
                        if (this.anIntArray375[var10] == var9) {
                            this.anIntArray358 = this.anIntArrayArray424[var10];
                            break;
                        }

                        if (-1 + this.anInt408 == var10) {
                            var11 = (int) ((double) this.anInt408 * Math.random());
                            this.anIntArray375[var11] = var9;
                            var9 = -1 - var9;
                            var12 = (var9 >> 10 & 31) * 8;
                            var13 = 8 * (var9 >> 5 & 31);
                            var14 = (31 & var9) * 8;

                            for (var15 = 0; var15 < 256; ++var15) {
                                var16 = var15 * var15;
                                var17 = var12 * var16 / 65536;
                                var18 = var16 * var13 / 65536;
                                var19 = var14 * var16 / 65536;
                                this.anIntArrayArray424[var11][255 + -var15] = var19 + (var17 << 16) + (var18 << 8);
                            }

                            this.anIntArray358 = this.anIntArrayArray424[var11];
                        }
                    }

                    var11 = this.anInt407;
                    var12 = this.anInt371 - -(this.anInt343 * var11);
                    byte var41 = 1;
                    if (this.aBoolean390) {
                        if ((this.anInt343 & 1) == 1) {
                            ++this.anInt343;
                            var12 += var11;
                        }

                        var41 = 2;
                        var11 <<= 1;
                    }

                    Scanline var42;
                    if (var1.transparent) {
                        for (var8 = this.anInt343; var8 < this.anInt380; var8 += var41) {
                            var42 = this.scanlines[var8];
                            var3 = var42.lowx >> 8;
                            var15 = var42.highx >> 8;
                            var16 = var15 + -var3;
                            if (var16 <= 0) {
                                var12 += var11;
                            } else {
                                var17 = var42.anInt1092;
                                var18 = (-var17 + var42.anInt1090) / var16;
                                if ((-this.anInt357) > var3) {
                                    var17 += (-this.anInt357 - var3) * var18;
                                    var3 = -this.anInt357;
                                    var16 = var15 + -var3;
                                }

                                if (this.anInt357 < var15) {
                                    var15 = this.anInt357;
                                    var16 = var15 + -var3;
                                }

                                method295(this.anIntArray358, this.anIntArray319, -747942559, 0, var12 + var3, var17, -var16, var18);
                                var12 += var11;
                            }
                        }

                    } else if (this.aBoolean379) {
                        for (var8 = this.anInt343; this.anInt380 > var8; var8 += var41) {
                            var42 = this.scanlines[var8];
                            var3 = var42.lowx >> 8;
                            var15 = var42.highx >> 8;
                            var16 = -var3 + var15;
                            if (var16 <= 0) {
                                var12 += var11;
                            } else {
                                var17 = var42.anInt1092;
                                var18 = (-var17 + var42.anInt1090) / var16;
                                if (var3 < -this.anInt357) {
                                    var17 += (-var3 + -this.anInt357) * var18;
                                    var3 = -this.anInt357;
                                    var16 = -var3 + var15;
                                }

                                if (var15 > this.anInt357) {
                                    var15 = this.anInt357;
                                    var16 = -var3 + var15;
                                }

                                GameApplet.method14(-var16, 1, this.anIntArray358, 0, var17, this.anIntArray319, var3 + var12, var18);
                                var12 += var11;
                            }
                        }

                    } else {
                        for (var8 = this.anInt343; this.anInt380 > var8; var8 += var41) {
                            var42 = this.scanlines[var8];
                            var3 = var42.lowx >> 8;
                            var15 = var42.highx >> 8;
                            var16 = -var3 + var15;
                            if (var16 <= 0) {
                                var12 += var11;
                            } else {
                                var17 = var42.anInt1092;
                                var18 = (-var17 + var42.anInt1090) / var16;
                                if (-this.anInt357 > var3) {
                                    var17 += var18 * (-this.anInt357 + -var3);
                                    var3 = -this.anInt357;
                                    var16 = var15 + -var3;
                                }

                                if (var15 > this.anInt357) {
                                    var15 = this.anInt357;
                                    var16 = var15 + -var3;
                                }

                                method148(this.anIntArray358, var12 - -var3, 68, var17, this.anIntArray319, var18, -var16, 0);
                                var12 += var11;
                            }
                        }

                    }
                }
            }
        }
    }

    private boolean method297(int[] var1, int[] var2, int var3, int[] var4, int[] var5) {
        int var6 = var5.length;
        int var7 = var4.length;
        byte var8 = 0;
        int var9 = 0;
        int var10;
        int var11 = var10 = var1[0];
        int var12 = 0;

        for (int var13 = 1; var13 < var6; ++var13) {
            if (var1[var13] >= var10) {
                if (var11 < var1[var13]) {
                    var11 = var1[var13];
                }
            } else {
                var9 = var13;
                var10 = var1[var13];
            }
        }

        int var14;
        int var15 = var14 = var2[0];

        for (int var16 = 1; var7 > var16; ++var16) {
            if (var14 > var2[var16]) {
                var12 = var16;
                var14 = var2[var16];
            } else if (var2[var16] > var15) {
                var15 = var2[var16];
            }
        }

        if (var3 != 20021) {
            this.anIntArray421 = null;
        }

        if (var11 <= var14) {
            return false;
        } else if (var15 <= var10) {
            return false;
        } else {
            int var17;
            int var19;
            int var18;
            boolean var21;
            int var20;
            int var23;
            int var22;
            if (var1[var9] >= var2[var12]) {
                for (var17 = var12; var1[var9] > var2[var12]; var12 = (var7 + -1 + var12) % var7) {
                    ;
                }

                while (var1[var9] > var2[var17]) {
                    var17 = (1 + var17) % var7;
                }

                var18 = var5[var9];
                var19 = this.method330(var2[(1 + var12) % var7], 1, var4[var12], var4[(1 + var12) % var7], var1[var9], var2[var12]);
                var20 = this.method330(var2[(-1 + var17 + var7) % var7], 1, var4[var17], var4[(var7 + var17 + -1) % var7], var1[var9], var2[var17]);
                var21 = var18 < var20 | var19 > var18;
                if (this.method315(var20, !var21, var19, var18, -50)) {
                    return true;
                }

                var22 = (var9 + 1) % var6;
                if (var17 == var12) {
                    var8 = 2;
                }

                var9 = (var9 - 1 - -var6) % var6;
            } else {
                for (var22 = var9; var2[var12] > var1[var22]; var22 = (var22 - -1) % var6) {
                    ;
                }

                while (var2[var12] > var1[var9]) {
                    var9 = (var6 + var9 + -1) % var6;
                }

                var18 = this.method330(var1[(var9 + 1) % var6], 1, var5[var9], var5[(1 + var9) % var6], var2[var12], var1[var9]);
                var23 = this.method330(var1[(-1 + var22 - -var6) % var6], var3 + -20020, var5[var22], var5[(var22 - (1 - var6)) % var6], var2[var12], var1[var22]);
                var19 = var4[var12];
                var21 = var18 < var19 | var23 < var19;
                if (this.method315(var23, var21, var18, var19, -81)) {
                    return true;
                }

                var17 = (var12 - -1) % var7;
                if (var9 == var22) {
                    var8 = 1;
                }

                var12 = (var7 + var12 + -1) % var7;
            }

            while (var8 == 0) {
                if (var1[var9] < var1[var22]) {
                    if (var1[var9] < var2[var12]) {
                        if (var1[var9] >= var2[var17]) {
                            var18 = this.method330(var1[(1 + var9) % var6], 1, var5[var9], var5[(var9 - -1) % var6], var2[var17], var1[var9]);
                            var23 = this.method330(var1[(-1 + var22 + var6) % var6], var3 + -20020, var5[var22], var5[(var22 + -1 - -var6) % var6], var2[var17], var1[var22]);
                            var19 = this.method330(var2[(var12 + 1) % var7], 1, var4[var12], var4[(1 + var12) % var7], var2[var17], var2[var12]);
                            var20 = var4[var17];
                            if (this.method308(var21, -69, var20, var18, var23, var19)) {
                                return true;
                            }

                            var17 = (var17 + 1) % var7;
                            if (var17 == var12) {
                                var8 = 2;
                            }
                        } else {
                            var18 = var5[var9];
                            var23 = this.method330(var1[(var6 + -1 + var22) % var6], 1, var5[var22], var5[(var22 + (-1 - -var6)) % var6], var1[var9], var1[var22]);
                            var19 = this.method330(var2[(1 + var12) % var7], 1, var4[var12], var4[(1 + var12) % var7], var1[var9], var2[var12]);
                            var20 = this.method330(var2[(var7 + -1 + var17) % var7], var3 + -20020, var4[var17], var4[(var7 + (var17 - 1)) % var7], var1[var9], var2[var17]);
                            if (this.method308(var21, var3 + -20140, var20, var18, var23, var19)) {
                                return true;
                            }

                            var9 = (var6 + (var9 - 1)) % var6;
                            if (var9 == var22) {
                                var8 = 1;
                            }
                        }
                    } else if (var2[var17] <= var2[var12]) {
                        var18 = this.method330(var1[(var9 + 1) % var6], 1, var5[var9], var5[(1 + var9) % var6], var2[var17], var1[var9]);
                        var23 = this.method330(var1[(var6 + -1 + var22) % var6], 1, var5[var22], var5[(var22 - 1 + var6) % var6], var2[var17], var1[var22]);
                        var19 = this.method330(var2[(var12 - -1) % var7], 1, var4[var12], var4[(1 + var12) % var7], var2[var17], var2[var12]);
                        var20 = var4[var17];
                        if (this.method308(var21, var3 + -20113, var20, var18, var23, var19)) {
                            return true;
                        }

                        var17 = (var17 + 1) % var7;
                        if (var12 == var17) {
                            var8 = 2;
                        }
                    } else {
                        var18 = this.method330(var1[(1 + var9) % var6], 1, var5[var9], var5[(var9 - -1) % var6], var2[var12], var1[var9]);
                        var23 = this.method330(var1[(-1 + var22 + var6) % var6], var3 + -20020, var5[var22], var5[(-1 + (var22 - -var6)) % var6], var2[var12], var1[var22]);
                        var19 = var4[var12];
                        var20 = this.method330(var2[(var17 + -1 + var7) % var7], 1, var4[var17], var4[(var17 + -1 + var7) % var7], var2[var12], var2[var17]);
                        if (this.method308(var21, -89, var20, var18, var23, var19)) {
                            return true;
                        }

                        var12 = (var12 + -1 + var7) % var7;
                        if (var17 == var12) {
                            var8 = 2;
                        }
                    }
                } else if (var1[var22] < var2[var12]) {
                    if (var1[var22] >= var2[var17]) {
                        var18 = this.method330(var1[(1 + var9) % var6], var3 + -20020, var5[var9], var5[(var9 + 1) % var6], var2[var17], var1[var9]);
                        var23 = this.method330(var1[(var6 + -1 + var22) % var6], 1, var5[var22], var5[(var6 + -1 + var22) % var6], var2[var17], var1[var22]);
                        var19 = this.method330(var2[(1 + var12) % var7], 1, var4[var12], var4[(1 + var12) % var7], var2[var17], var2[var12]);
                        var20 = var4[var17];
                        if (this.method308(var21, -96, var20, var18, var23, var19)) {
                            return true;
                        }

                        var17 = (var17 - -1) % var7;
                        if (var17 == var12) {
                            var8 = 2;
                        }
                    } else {
                        var18 = this.method330(var1[(var9 - -1) % var6], 1, var5[var9], var5[(var9 - -1) % var6], var1[var22], var1[var9]);
                        var23 = var5[var22];
                        var19 = this.method330(var2[(1 + var12) % var7], 1, var4[var12], var4[(var12 - -1) % var7], var1[var22], var2[var12]);
                        var20 = this.method330(var2[(var7 + var17 + -1) % var7], var3 + -20020, var4[var17], var4[(var7 + -1 + var17) % var7], var1[var22], var2[var17]);
                        if (this.method308(var21, var3 + -20149, var20, var18, var23, var19)) {
                            return true;
                        }

                        var22 = (var22 - -1) % var6;
                        if (var22 == var9) {
                            var8 = 1;
                        }
                    }
                } else if (var2[var17] <= var2[var12]) {
                    var18 = this.method330(var1[(1 + var9) % var6], 1, var5[var9], var5[(var9 - -1) % var6], var2[var17], var1[var9]);
                    var23 = this.method330(var1[(-1 + var22 + var6) % var6], 1, var5[var22], var5[(var6 + (var22 - 1)) % var6], var2[var17], var1[var22]);
                    var19 = this.method330(var2[(1 + var12) % var7], 1, var4[var12], var4[(1 + var12) % var7], var2[var17], var2[var12]);
                    var20 = var4[var17];
                    if (this.method308(var21, -123, var20, var18, var23, var19)) {
                        return true;
                    }

                    var17 = (var17 - -1) % var7;
                    if (var12 == var17) {
                        var8 = 2;
                    }
                } else {
                    var18 = this.method330(var1[(1 + var9) % var6], 1, var5[var9], var5[(1 + var9) % var6], var2[var12], var1[var9]);
                    var23 = this.method330(var1[(var6 + -1 + var22) % var6], 1, var5[var22], var5[(var6 + (var22 - 1)) % var6], var2[var12], var1[var22]);
                    var19 = var4[var12];
                    var20 = this.method330(var2[(var7 + var17 + -1) % var7], 1, var4[var17], var4[(var7 + -1 + var17) % var7], var2[var12], var2[var17]);
                    if (this.method308(var21, -113, var20, var18, var23, var19)) {
                        return true;
                    }

                    var12 = (var12 - 1 - -var7) % var7;
                    if (var17 == var12) {
                        var8 = 2;
                    }
                }
            }

            while (var8 == 1) {
                if (var1[var9] < var2[var12]) {
                    if (var2[var17] > var1[var9]) {
                        var18 = var5[var9];
                        var19 = this.method330(var2[(var12 - -1) % var7], var3 ^ 20020, var4[var12], var4[(1 + var12) % var7], var1[var9], var2[var12]);
                        var20 = this.method330(var2[(var17 + -1 + var7) % var7], var3 + -20020, var4[var17], var4[(var7 + var17 + -1) % var7], var1[var9], var2[var17]);
                        if (this.method315(var20, !var21, var19, var18, var3 + -20141)) {
                            return true;
                        }

                        return false;
                    }

                    var18 = this.method330(var1[(var9 + 1) % var6], var3 + -20020, var5[var9], var5[(1 + var9) % var6], var2[var17], var1[var9]);
                    var23 = this.method330(var1[(var22 + -1 + var6) % var6], var3 ^ 20020, var5[var22], var5[(var6 + var22 + -1) % var6], var2[var17], var1[var22]);
                    var19 = this.method330(var2[(1 + var12) % var7], 1, var4[var12], var4[(1 + var12) % var7], var2[var17], var2[var12]);
                    var20 = var4[var17];
                    if (this.method308(var21, -79, var20, var18, var23, var19)) {
                        return true;
                    }

                    var17 = (1 + var17) % var7;
                    if (var12 == var17) {
                        var8 = 0;
                    }
                } else if (var2[var12] < var2[var17]) {
                    var18 = this.method330(var1[(var9 + 1) % var6], 1, var5[var9], var5[(1 + var9) % var6], var2[var12], var1[var9]);
                    var23 = this.method330(var1[(var22 - (1 + -var6)) % var6], 1, var5[var22], var5[(var6 + var22 - 1) % var6], var2[var12], var1[var22]);
                    var19 = var4[var12];
                    var20 = this.method330(var2[(var7 + -1 + var17) % var7], var3 + -20020, var4[var17], var4[(var7 + -1 + var17) % var7], var2[var12], var2[var17]);
                    if (this.method308(var21, var3 + -20115, var20, var18, var23, var19)) {
                        return true;
                    }

                    var12 = (var7 + var12 - 1) % var7;
                    if (var17 == var12) {
                        var8 = 0;
                    }
                } else {
                    var18 = this.method330(var1[(var9 - -1) % var6], 1, var5[var9], var5[(1 + var9) % var6], var2[var17], var1[var9]);
                    var23 = this.method330(var1[(var6 + -1 + var22) % var6], 1, var5[var22], var5[(var6 + -1 + var22) % var6], var2[var17], var1[var22]);
                    var19 = this.method330(var2[(var12 - -1) % var7], var3 ^ 20020, var4[var12], var4[(1 + var12) % var7], var2[var17], var2[var12]);
                    var20 = var4[var17];
                    if (this.method308(var21, -110, var20, var18, var23, var19)) {
                        return true;
                    }

                    var17 = (1 + var17) % var7;
                    if (var12 == var17) {
                        var8 = 0;
                    }
                }
            }

            while (var8 == 2) {
                if (var1[var9] <= var2[var12]) {
                    if (var1[var9] >= var1[var22]) {
                        var18 = this.method330(var1[(var9 + 1) % var6], 1, var5[var9], var5[(var9 - -1) % var6], var1[var22], var1[var9]);
                        var23 = var5[var22];
                        var19 = this.method330(var2[(1 + var12) % var7], 1, var4[var12], var4[(1 + var12) % var7], var1[var22], var2[var12]);
                        var20 = this.method330(var2[(var7 + -1 + var17) % var7], 1, var4[var17], var4[(-1 + (var17 - -var7)) % var7], var1[var22], var2[var17]);
                        if (this.method308(var21, var3 + -20107, var20, var18, var23, var19)) {
                            return true;
                        }

                        var22 = (1 + var22) % var6;
                        if (var22 == var9) {
                            var8 = 0;
                        }
                    } else {
                        var18 = var5[var9];
                        var23 = this.method330(var1[(var6 + -1 + var22) % var6], 1, var5[var22], var5[(var6 + -1 + var22) % var6], var1[var9], var1[var22]);
                        var19 = this.method330(var2[(var12 - -1) % var7], 1, var4[var12], var4[(1 + var12) % var7], var1[var9], var2[var12]);
                        var20 = this.method330(var2[(var7 + -1 + var17) % var7], 1, var4[var17], var4[(var7 + -1 + var17) % var7], var1[var9], var2[var17]);
                        if (this.method308(var21, -103, var20, var18, var23, var19)) {
                            return true;
                        }

                        var9 = (var6 + -1 + var9) % var6;
                        if (var22 == var9) {
                            var8 = 0;
                        }
                    }
                } else {
                    if (var2[var12] < var1[var22]) {
                        var18 = this.method330(var1[(1 + var9) % var6], 1, var5[var9], var5[(1 + var9) % var6], var2[var12], var1[var9]);
                        var23 = this.method330(var1[(var6 + var22 + -1) % var6], 1, var5[var22], var5[(-1 + var22 + var6) % var6], var2[var12], var1[var22]);
                        var19 = var4[var12];
                        if (this.method315(var23, var21, var18, var19, -82)) {
                            return true;
                        }

                        return false;
                    }

                    var18 = this.method330(var1[(1 + var9) % var6], 1, var5[var9], var5[(var9 - -1) % var6], var1[var22], var1[var9]);
                    var23 = var5[var22];
                    var19 = this.method330(var2[(var12 - -1) % var7], 1, var4[var12], var4[(var12 + 1) % var7], var1[var22], var2[var12]);
                    var20 = this.method330(var2[(-1 + var17 - -var7) % var7], var3 + -20020, var4[var17], var4[(var7 + -1 + var17) % var7], var1[var22], var2[var17]);
                    if (this.method308(var21, var3 + -20137, var20, var18, var23, var19)) {
                        return true;
                    }

                    var22 = (1 + var22) % var6;
                    if (var22 == var9) {
                        var8 = 0;
                    }
                }
            }

            if (var2[var12] <= var1[var9]) {
                var18 = this.method330(var1[(var9 - -1) % var6], 1, var5[var9], var5[(var9 + 1) % var6], var2[var12], var1[var9]);
                var23 = this.method330(var1[(-1 + var22 + var6) % var6], 1, var5[var22], var5[(var22 + -1 + var6) % var6], var2[var12], var1[var22]);
                var19 = var4[var12];
                if (this.method315(var23, var21, var18, var19, -120)) {
                    return true;
                } else {
                    return false;
                }
            } else {
                var18 = var5[var9];
                var19 = this.method330(var2[(1 + var12) % var7], var3 + -20020, var4[var12], var4[(1 + var12) % var7], var1[var9], var2[var12]);
                var20 = this.method330(var2[(-1 + var17 + var7) % var7], 1, var4[var17], var4[(var17 - (1 + -var7)) % var7], var1[var9], var2[var17]);
                if (this.method315(var20, !var21, var19, var18, -96)) {
                    return true;
                } else {
                    return false;
                }
            }
        }
    }

    final void loadTexture(int var1, int var2, byte[] var3, int[] var4, int var5) {
        this.aByteArrayArray350[var2] = var3;
        this.anIntArrayArray415[var2] = var4;
        this.anIntArray417[var2] = var5;
        if (var1 <= 116) {
            this.aSurface_385 = null;
        }

        this.aLongArray423[var2] = 0L;
        this.aBooleanArray341[var2] = false;
        this.anIntArrayArray335[var2] = null;
        this.method333(false, var2);
    }

    final int method299(int var1, int var2) {
        if (var1 == 12345678) {
            return 0;
        } else {
            this.method333(false, var1);
            if (var2 != -18255) {
                return -103;
            } else if (var1 >= 0) {
                return this.anIntArrayArray335[var1][0];
            } else if (var1 < 0) {
                var1 = -(1 + var1);
                int var3 = (31820 & var1) >> 10;
                int var4 = 31 & var1 >> 5;
                int var5 = 31 & var1;
                return (var3 << 19) - (-(var4 << 11) - (var5 << 3));
            } else {
                return 0;
            }
        }
    }

    final GameModel[] getMousePickedModels(byte var1) {
        int var2 = -34 % ((var1 - 5) / 48);
        return this.aGameModelArray321;
    }

    private boolean method301(Polygon[] var1, int var2, int var3, int var4) {
        int var5 = 74 / ((var3 - 36) / 48);

        while (true) {
            Polygon var6 = var1[var4];
            int var7 = 1 + var4;

            while (true) {
                Polygon var8;
                if (var2 >= var7) {
                    var8 = var1[var7];
                    if (this.method317(-1, var8, var6)) {
                        var1[var4] = var8;
                        var4 = var7;
                        var1[var7] = var6;
                        if (var2 == var7) {
                            this.anInt428 = var7 + -1;
                            this.anInt427 = var7;
                            return true;
                        }

                        ++var7;
                        continue;
                    }
                }

                var8 = var1[var2];

                for (int var9 = var2 + -1; var9 >= var4; --var9) {
                    Polygon var10 = var1[var9];
                    if (!this.method317(-1, var8, var10)) {
                        break;
                    }

                    var1[var2] = var10;
                    var1[var9] = var8;
                    var2 = var9;
                    if (var9 == var4) {
                        this.anInt428 = var9;
                        this.anInt427 = 1 + var9;
                        return true;
                    }
                }

                if (var4 - -1 >= var2) {
                    this.anInt427 = var4;
                    this.anInt428 = var2;
                    return false;
                }

                if (!this.method301(var1, var2, 102, 1 + var4)) {
                    this.anInt427 = var4;
                    return false;
                }

                var2 = this.anInt428;
                break;
            }
        }
    }

    final void allocateTextures(int var1, int var2, byte var3, int var4) {
        this.aLongArray423 = new long[var4];
        this.anIntArrayArray328 = new int[var1][];
        this.anIntArrayArray415 = new int[var4][];
        this.anIntArrayArray327 = new int[var2][];
        this.anIntArray417 = new int[var4];
        JagGrab.aLong1131 = 0L;
        this.anInt326 = var4;
        this.anIntArrayArray335 = new int[var4][];
        if (var3 < 31) {
            this.method308(false, 2, 101, -41, -65, -6);
        }

        this.aBooleanArray341 = new boolean[var4];
        this.aByteArrayArray350 = new byte[var4][];
    }

    private void method303(int var1, int var2) {
        if (var1 == 31545) {
            short var3;
            if (this.anIntArray417[var2] != 0) {
                var3 = 128;
            } else {
                var3 = 64;
            }

            int[] var4 = this.anIntArrayArray335[var2];
            int var5 = 0;

            int var7;
            int var8;
            for (int var6 = 0; var3 > var6; ++var6) {
                for (var7 = 0; var7 < var3; ++var7) {
                    var8 = this.anIntArrayArray415[var2][this.aByteArrayArray350[var2][var7 + var6 * var3] & 255];
                    var8 &= 16316671;
                    if (var8 == 0) {
                        var8 = 1;
                    } else if (var8 == 16253183) {
                        var8 = 0;
                        this.aBooleanArray341[var2] = true;
                    }

                    var4[var5++] = var8;
                }
            }

            for (var7 = 0; var5 > var7; ++var7) {
                var8 = var4[var7];
                var4[var5 - -var7] = Utility.bitwiseAnd(-(var8 >>> 3) + var8, 16316671);
                var4[var5 * 2 - -var7] = Utility.bitwiseAnd(16316671, -(var8 >>> 2) + var8);
                var4[3 * var5 + var7] = Utility.bitwiseAnd(-(var8 >>> 3) + var8 - (var8 >>> 2), 16316671);
            }

        }
    }

    final void endscene(boolean var1) {
        this.aBoolean390 = this.aSurface_385.interlace;
        int var2 = this.anInt357 * this.anInt317 >> this.anInt351;
        GameModel.anInt1031 = 0;
        BufferBase_Sub3_Sub1.anInt1741 = 0;
        World.anInt273 = 0;
        anInt465 = 0;
        anInt762 = 0;
        SoundPlayer.anInt769 = 0;
        int var3 = this.anInt420 * this.anInt317 >> this.anInt351;
        this.method324(this.anInt317, false, -var2, -var3);
        this.method324(this.anInt317, false, -var2, var3);
        this.method324(this.anInt317, var1, var2, -var3);
        this.method324(this.anInt317, var1, var2, var3);
        this.method324(0, false, -this.anInt357, -this.anInt420);
        this.method324(0, false, -this.anInt357, this.anInt420);
        this.method324(0, var1, this.anInt357, -this.anInt420);
        this.method324(0, var1, this.anInt357, this.anInt420);
        anInt465 += this.anInt364;
        GameModel.anInt1031 += this.anInt404;
        anInt762 += this.anInt381;
        SoundPlayer.anInt769 += this.anInt364;
        BufferBase_Sub3_Sub1.anInt1741 += this.anInt381;
        World.anInt273 += this.anInt404;
        this.aGameModelArray340[this.anInt365] = this.view;
        this.view.numFaces = 2;

        int var4;
        for (var4 = 0; var4 < this.anInt365; ++var4) {
            this.aGameModelArray340[var4].method548(this.anInt374, this.anInt404, (byte) 76, this.anInt381, this.anInt364, this.anInt332, this.anInt337, this.anInt345, this.anInt351);
        }

        this.aGameModelArray340[this.anInt365].method548(this.anInt374, this.anInt404, (byte) 76, this.anInt381, this.anInt364, this.anInt332, this.anInt337, this.anInt345, this.anInt351);
        this.anInt359 = 0;

        GameModel var6;
        int var7;
        int var10;
        int var12;
        int var13;
        int var15;
        int var17;
        int var16;
        for (int var5 = 0; var5 < this.anInt365; ++var5) {
            var6 = this.aGameModelArray340[var5];
            if (var6.aBoolean936) {
                var4 = 0;

                while (var6.transformState > var4) {
                    var7 = var6.faceNumVertices[var4];
                    boolean var8 = false;
                    int[] var9 = var6.faceVertices[var4];
                    var10 = 0;

                    while (true) {
                        int var11;
                        if (var7 > var10) {
                            var11 = var6.anIntArray973[var9[var10]];
                            if (this.anInt374 >= var11 || this.anInt317 <= var11) {
                                ++var10;
                                continue;
                            }

                            var8 = true;
                        }

                        if (var8) {
                            int var26 = 0;

                            for (var12 = 0; var7 > var12; ++var12) {
                                var11 = var6.anIntArray1001[var9[var12]];
                                if ((-this.anInt357) < var11) {
                                    var26 |= 1;
                                }

                                if (this.anInt357 > var11) {
                                    var26 |= 2;
                                }

                                if (var26 == 3) {
                                    break;
                                }
                            }

                            if (var26 == 3) {
                                var26 = 0;

                                for (var13 = 0; var7 > var13; ++var13) {
                                    var11 = var6.anIntArray994[var9[var13]];
                                    if ((-this.anInt420) < var11) {
                                        var26 |= 1;
                                    }

                                    if (this.anInt420 > var11) {
                                        var26 |= 2;
                                    }

                                    if (var26 == 3) {
                                        break;
                                    }
                                }

                                if (var26 == 3) {
                                    Polygon var14 = this.visibleModelPolygons[this.anInt359];
                                    var14.anInt169 = var4;
                                    var14.model = var6;
                                    this.method331(this.anInt359, 1);
                                    if (var14.anInt158 >= 0) {
                                        var15 = var6.faceFillBack[var4];
                                    } else {
                                        var15 = var6.faceFillFront[var4];
                                    }

                                    if (var15 != 12345678) {
                                        var16 = 0;

                                        for (var17 = 0; var17 < var7; ++var17) {
                                            var16 += var6.anIntArray973[var9[var17]];
                                        }

                                        var14.anInt160 = var6.anInt1009 + var16 / var7;
                                        ++this.anInt359;
                                        var14.anInt157 = var15;
                                    }
                                }
                            }
                        }

                        ++var4;
                        break;
                    }
                }
            }
        }

        var6 = this.view;
        int var29;
        if (var6.aBoolean936) {
            for (var4 = 0; var4 < var6.transformState; ++var4) {
                int[] var24 = var6.faceVertices[var4];
                int var25 = var24[0];
                var10 = var6.anIntArray1001[var25];
                var12 = var6.anIntArray994[var25];
                var13 = var6.anIntArray973[var25];
                if (this.anInt374 < var13 && this.anInt367 > var13) {
                    var29 = (this.anIntArray370[var4] << this.anInt351) / var13;
                    var15 = (this.anIntArray325[var4] << this.anInt351) / var13;
                    if (this.anInt357 >= (-(var29 / 2) + var10) && (var10 - -(var29 / 2)) >= (-this.anInt357) && this.anInt420 >= (-var15 + var12) && var12 >= (-this.anInt420)) {
                        Polygon var30 = this.visibleModelPolygons[this.anInt359];
                        var30.model = var6;
                        var30.anInt169 = var4;
                        this.method326(false, this.anInt359);
                        var30.anInt160 = (var13 + var6.anIntArray973[var24[1]]) / 2;
                        ++this.anInt359;
                    }
                }
            }
        }

        if (this.anInt359 != 0) {
            this.method310(-1 + this.anInt359, -97, this.visibleModelPolygons, 0);
            this.method309(22692, 100, this.anInt359, this.visibleModelPolygons);

            for (var7 = 0; this.anInt359 > var7; ++var7) {
                Polygon var27 = this.visibleModelPolygons[var7];
                var6 = var27.model;
                var4 = var27.anInt169;
                int var18;
                int var21;
                int var20;
                if (var6 == this.view) {
                    int[] var28 = var6.faceVertices[var4];
                    var12 = var28[0];
                    var13 = var6.anIntArray1001[var12];
                    var29 = var6.anIntArray994[var12];
                    var15 = var6.anIntArray973[var12];
                    var17 = (this.anIntArray370[var4] << this.anInt351) / var15;
                    var18 = (this.anIntArray325[var4] << this.anInt351) / var15;
                    int var19 = -var6.anIntArray994[var28[1]] + var29;
                    var20 = (-var13 + var6.anIntArray1001[var28[1]]) * var19 / var18;
                    var20 = var6.anIntArray1001[var28[1]] - var13;
                    var21 = -(var17 / 2) + var13;
                    int var22 = var29 + this.anInt368 + -var18;
                    this.aSurface_385.method420(this.anInt371 + var21, var17, var18, this.anIntArray339[var4], var20, (byte) -90, (256 << this.anInt351) / var15, var22);
                    if (this.aBoolean426 && this.anInt394 > this.anInt373) {
                        var21 += (this.anIntArray425[var4] << this.anInt351) / var15;
                        if (this.anInt401 >= var22 && this.anInt401 <= (var22 + var18) && this.anInt344 >= var21 && this.anInt344 <= (var17 + var21) && !var6.aBoolean922 && var6.aByteArray956[var4] == 0) {
                            this.aGameModelArray321[this.anInt373] = var6;
                            this.anIntArray329[this.anInt373] = var4;
                            ++this.anInt373;
                        }
                    }
                } else {
                    var29 = 0;
                    var17 = 0;
                    var18 = var6.faceNumVertices[var4];
                    if (var6.faceIntensity[var4] != 12345678) {
                        if (var27.anInt158 >= 0) {
                            var17 = var6.faceIntensity[var4] + var6.anInt985;
                        } else {
                            var17 = -var6.faceIntensity[var4] + var6.anInt985;
                        }
                    }

                    int[] var31 = var6.faceVertices[var4];

                    for (var20 = 0; var18 > var20; ++var20) {
                        var16 = var31[var20];
                        this.anIntArray342[var20] = var6.anIntArray961[var16];
                        this.anIntArray412[var20] = var6.anIntArray992[var16];
                        this.anIntArray421[var20] = var6.anIntArray973[var16];
                        if (var6.faceIntensity[var4] == 12345678) {
                            if (var27.anInt158 >= 0) {
                                var17 = var6.aByteArray942[var16] + (var6.anInt985 - -var6.anIntArray939[var16]);
                            } else {
                                var17 = -var6.anIntArray939[var16] + (var6.anInt985 - -var6.aByteArray942[var16]);
                            }
                        }

                        if (this.anInt374 > var6.anIntArray973[var16]) {
                            if (var20 != 0) {
                                var15 = var31[-1 + var20];
                            } else {
                                var15 = var31[var18 - 1];
                            }

                            if (this.anInt374 <= var6.anIntArray973[var15]) {
                                var13 = var6.anIntArray973[var16] + -var6.anIntArray973[var15];
                                var10 = var6.anIntArray961[var16] - (-var6.anIntArray961[var15] + var6.anIntArray961[var16]) * (var6.anIntArray973[var16] - this.anInt374) / var13;
                                var12 = -((-this.anInt374 + var6.anIntArray973[var16]) * (var6.anIntArray992[var16] + -var6.anIntArray992[var15]) / var13) + var6.anIntArray992[var16];
                                this.anIntArray363[var29] = (var10 << this.anInt351) / this.anInt374;
                                this.anIntArray330[var29] = (var12 << this.anInt351) / this.anInt374;
                                this.anIntArray360[var29] = var17;
                                ++var29;
                            }

                            if (-1 + var18 != var20) {
                                var15 = var31[1 + var20];
                            } else {
                                var15 = var31[0];
                            }

                            if (this.anInt374 <= var6.anIntArray973[var15]) {
                                var13 = var6.anIntArray973[var16] - var6.anIntArray973[var15];
                                var10 = -((-this.anInt374 + var6.anIntArray973[var16]) * (-var6.anIntArray961[var15] + var6.anIntArray961[var16]) / var13) + var6.anIntArray961[var16];
                                var12 = -((var6.anIntArray992[var16] - var6.anIntArray992[var15]) * (var6.anIntArray973[var16] - this.anInt374) / var13) + var6.anIntArray992[var16];
                                this.anIntArray363[var29] = (var10 << this.anInt351) / this.anInt374;
                                this.anIntArray330[var29] = (var12 << this.anInt351) / this.anInt374;
                                this.anIntArray360[var29] = var17;
                                ++var29;
                            }
                        } else {
                            this.anIntArray363[var29] = var6.anIntArray1001[var16];
                            this.anIntArray330[var29] = var6.anIntArray994[var16];
                            this.anIntArray360[var29] = var17;
                            if (this.fogZDistance < var6.anIntArray973[var16]) {
                                this.anIntArray360[var29] += (var6.anIntArray973[var16] + -this.fogZDistance) / this.fogZFalloff;
                            }

                            ++var29;
                        }
                    }

                    for (var21 = 0; var18 > var21; ++var21) {
                        if (this.anIntArray360[var21] >= 0) {
                            if (this.anIntArray360[var21] > 255) {
                                this.anIntArray360[var21] = 255;
                            }
                        } else {
                            this.anIntArray360[var21] = 0;
                        }

                        if (var27.anInt157 >= 0) {
                            if (this.anIntArray417[var27.anInt157] != 1) {
                                this.anIntArray360[var21] <<= 6;
                            } else {
                                this.anIntArray360[var21] <<= 9;
                            }
                        }
                    }

                    this.method305(0, var29, 0, 0, this.anIntArray330, (byte) 55, this.anIntArray363, var6, var4, this.anIntArray360, 0);
                    if (this.anInt343 < this.anInt380) {
                        this.method296(var6, this.anIntArray421, 0, this.anIntArray412, this.anIntArray342, var18, false, 0, var27.anInt157);
                    }
                }
            }

            this.aBoolean426 = false;
        }
        ;
    }

    private void method305(int var1, int var2, int var3, int var4, int[] var5, byte var6, int[] var7, GameModel var8, int var9, int[] var10, int var11) {
        int var12 = 49 / ((-29 - var6) / 39);
        int var13;
        int var14;
        int var15;
        int var17;
        int var16;
        int var19;
        int var18;
        int var21;
        int var20;
        int var23;
        int var22;
        int var25;
        int var24;
        int var27;
        int var26;
        int var29;
        int var28;
        int var31;
        int var30;
        int var34;
        int var35;
        int var32;
        int var33;
        int var38;
        int var39;
        int var36;
        int var37;
        int var40;
        int var41;
        Scanline var53;
        if (var2 == 3) {
            var13 = var5[0] + this.anInt368;
            var14 = var5[1] - -this.anInt368;
            var15 = this.anInt368 + var5[2];
            var16 = var7[0];
            var17 = var7[1];
            var18 = var7[2];
            var19 = var10[0];
            var20 = var10[1];
            var21 = var10[2];
            var22 = -1 + this.anInt420 + this.anInt368;
            var23 = 0;
            var24 = 0;
            var25 = 0;
            var26 = 0;
            var27 = 12345678;
            var28 = -12345678;
            if (var15 != var13) {
                if (var15 > var13) {
                    var27 = var13;
                    var28 = var15;
                    var25 = var19 << 8;
                    var23 = var16 << 8;
                } else {
                    var23 = var18 << 8;
                    var25 = var21 << 8;
                    var28 = var13;
                    var27 = var15;
                }

                var26 = (-var19 + var21 << 8) / (var15 - var13);
                var24 = (var18 - var16 << 8) / (-var13 + var15);
                if (var27 < 0) {
                    var23 -= var24 * var27;
                    var25 -= var27 * var26;
                    var27 = 0;
                }

                if (var22 < var28) {
                    var28 = var22;
                }
            }

            var29 = 0;
            var30 = 0;
            var31 = 0;
            var32 = 0;
            var33 = 12345678;
            var34 = -12345678;
            if (var14 != var13) {
                if (var14 > var13) {
                    var34 = var14;
                    var29 = var16 << 8;
                    var33 = var13;
                    var31 = var19 << 8;
                } else {
                    var33 = var14;
                    var29 = var17 << 8;
                    var34 = var13;
                    var31 = var20 << 8;
                }

                var30 = (-var16 + var17 << 8) / (var14 + -var13);
                var32 = (-var19 + var20 << 8) / (-var13 + var14);
                if (var33 < 0) {
                    var29 -= var30 * var33;
                    var31 -= var33 * var32;
                    var33 = 0;
                }

                if (var22 < var34) {
                    var34 = var22;
                }
            }

            var35 = 0;
            var36 = 0;
            var37 = 0;
            var38 = 0;
            var39 = 12345678;
            var40 = -12345678;
            this.anInt343 = var27;
            if (var14 != var15) {
                if (var14 < var15) {
                    var37 = var20 << 8;
                    var35 = var17 << 8;
                    var39 = var14;
                    var40 = var15;
                } else {
                    var39 = var15;
                    var35 = var18 << 8;
                    var40 = var14;
                    var37 = var21 << 8;
                }

                var36 = (var18 - var17 << 8) / (-var14 + var15);
                var38 = (-var20 + var21 << 8) / (-var14 + var15);
                if (var22 < var40) {
                    var40 = var22;
                }

                if (var39 < 0) {
                    var37 -= var38 * var39;
                    var35 -= var39 * var36;
                    var39 = 0;
                }
            }

            if (this.anInt343 > var33) {
                this.anInt343 = var33;
            }

            if (var39 < this.anInt343) {
                this.anInt343 = var39;
            }

            this.anInt380 = var28;
            if (var34 > this.anInt380) {
                this.anInt380 = var34;
            }

            if (this.anInt380 < var40) {
                this.anInt380 = var40;
            }

            var41 = 0;

            for (var1 = this.anInt343; var1 < this.anInt380; ++var1) {
                if (var27 <= var1 && var28 > var1) {
                    var41 = var25;
                    var11 = var25;
                    var3 = var23;
                    var4 = var23;
                    var25 += var26;
                    var23 += var24;
                } else {
                    var3 = -655360;
                    var4 = 655360;
                }

                if (var33 <= var1 && var1 < var34) {
                    if (var29 > var3) {
                        var3 = var29;
                        var41 = var31;
                    }

                    if (var4 > var29) {
                        var4 = var29;
                        var11 = var31;
                    }

                    var31 += var32;
                    var29 += var30;
                }

                if (var39 <= var1 && var40 > var1) {
                    if (var4 > var35) {
                        var4 = var35;
                        var11 = var37;
                    }

                    if (var3 < var35) {
                        var41 = var37;
                        var3 = var35;
                    }

                    var35 += var36;
                    var37 += var38;
                }

                Scanline var42 = this.scanlines[var1];
                var42.lowx = var4;
                var42.anInt1092 = var11;
                var42.highx = var3;
                var42.anInt1090 = var41;
            }

            if (this.anInt343 < (-this.anInt420 + this.anInt368)) {
                this.anInt343 = this.anInt368 + -this.anInt420;
            }
        } else if (var2 != 4) {
            this.anInt380 = this.anInt343 = var5[0] += this.anInt368;

            for (var1 = 1; var2 > var1; ++var1) {
                if ((var13 = var5[var1] += this.anInt368) >= this.anInt343) {
                    if (this.anInt380 < var13) {
                        this.anInt380 = var13;
                    }
                } else {
                    this.anInt343 = var13;
                }
            }

            if (this.anInt343 < -this.anInt420 + this.anInt368) {
                this.anInt343 = this.anInt368 + -this.anInt420;
            }

            if (this.anInt380 >= (this.anInt420 + this.anInt368)) {
                this.anInt380 = -1 + this.anInt368 + this.anInt420;
            }

            if (this.anInt380 <= this.anInt343) {
                return;
            }

            for (var1 = this.anInt343; this.anInt380 > var1; ++var1) {
                var53 = this.scanlines[var1];
                var53.lowx = 655360;
                var53.highx = -655360;
            }

            var13 = var2 + -1;
            var14 = var5[0];
            var15 = var5[var13];
            Scanline var54;
            if (var14 >= var15) {
                if (var15 < var14) {
                    var16 = var7[var13] << 8;
                    var17 = (-var7[var13] + var7[0] << 8) / (var14 - var15);
                    var18 = var10[var13] << 8;
                    var19 = (var10[0] + -var10[var13] << 8) / (-var15 + var14);
                    if (var15 < 0) {
                        var16 -= var15 * var17;
                        var18 -= var19 * var15;
                        var15 = 0;
                    }

                    if (this.anInt380 < var14) {
                        var14 = this.anInt380;
                    }

                    for (var1 = var15; var14 >= var1; ++var1) {
                        var54 = this.scanlines[var1];
                        var54.anInt1092 = var54.anInt1090 = var18;
                        var54.lowx = var54.highx = var16;
                        var18 += var19;
                        var16 += var17;
                    }
                }
            } else {
                var16 = var7[0] << 8;
                var17 = (var7[var13] - var7[0] << 8) / (-var14 + var15);
                var18 = var10[0] << 8;
                var19 = (var10[var13] - var10[0] << 8) / (var15 - var14);
                if (var14 < 0) {
                    var18 -= var19 * var14;
                    var16 -= var14 * var17;
                    var14 = 0;
                }

                if (this.anInt380 < var15) {
                    var15 = this.anInt380;
                }

                for (var1 = var14; var1 <= var15; ++var1) {
                    var54 = this.scanlines[var1];
                    var54.lowx = var54.highx = var16;
                    var54.anInt1092 = var54.anInt1090 = var18;
                    var16 += var17;
                    var18 += var19;
                }
            }

            for (var1 = 0; var1 < var13; ++var1) {
                var14 = var5[var1];
                var16 = var1 + 1;
                var15 = var5[var16];
                Scanline var55;
                if (var14 >= var15) {
                    if (var15 < var14) {
                        var17 = var7[var16] << 8;
                        var18 = (-var7[var16] + var7[var1] << 8) / (-var15 + var14);
                        var19 = var10[var16] << 8;
                        var20 = (var10[var1] + -var10[var16] << 8) / (var14 + -var15);
                        if (var15 < 0) {
                            var17 -= var18 * var15;
                            var19 -= var15 * var20;
                            var15 = 0;
                        }

                        if (this.anInt380 < var14) {
                            var14 = this.anInt380;
                        }

                        for (var21 = var15; var14 >= var21; ++var21) {
                            var55 = this.scanlines[var21];
                            if (var17 > var55.highx) {
                                var55.highx = var17;
                                var55.anInt1090 = var19;
                            }

                            if (var55.lowx > var17) {
                                var55.anInt1092 = var19;
                                var55.lowx = var17;
                            }

                            var19 += var20;
                            var17 += var18;
                        }
                    }
                } else {
                    var17 = var7[var1] << 8;
                    var18 = (var7[var16] - var7[var1] << 8) / (var15 + -var14);
                    var19 = var10[var1] << 8;
                    var20 = (var10[var16] + -var10[var1] << 8) / (-var14 + var15);
                    if (var15 > this.anInt380) {
                        var15 = this.anInt380;
                    }

                    if (var14 < 0) {
                        var19 -= var20 * var14;
                        var17 -= var18 * var14;
                        var14 = 0;
                    }

                    for (var21 = var14; var15 >= var21; ++var21) {
                        var55 = this.scanlines[var21];
                        if (var55.highx < var17) {
                            var55.anInt1090 = var19;
                            var55.highx = var17;
                        }

                        if (var55.lowx > var17) {
                            var55.lowx = var17;
                            var55.anInt1092 = var19;
                        }

                        var17 += var18;
                        var19 += var20;
                    }
                }
            }

            if (this.anInt343 < -this.anInt420 + this.anInt368) {
                this.anInt343 = this.anInt368 - this.anInt420;
            }
        } else {
            var13 = this.anInt368 + var5[0];
            var14 = var5[1] - -this.anInt368;
            var15 = this.anInt368 + var5[2];
            var16 = var5[3] - -this.anInt368;
            var17 = var7[0];
            var18 = var7[1];
            var19 = var7[2];
            var20 = var7[3];
            var21 = var10[0];
            var22 = var10[1];
            var23 = var10[2];
            var24 = var10[3];
            var25 = this.anInt368 - -this.anInt420 - 1;
            var26 = 0;
            var27 = 0;
            var28 = 0;
            var29 = 0;
            var30 = 12345678;
            var31 = -12345678;
            if (var13 != var16) {
                var29 = (-var21 + var24 << 8) / (-var13 + var16);
                if (var16 > var13) {
                    var26 = var17 << 8;
                    var31 = var16;
                    var28 = var21 << 8;
                    var30 = var13;
                } else {
                    var28 = var24 << 8;
                    var30 = var16;
                    var31 = var13;
                    var26 = var20 << 8;
                }

                var27 = (var20 - var17 << 8) / (-var13 + var16);
                if (var25 < var31) {
                    var31 = var25;
                }

                if (var30 < 0) {
                    var28 -= var30 * var29;
                    var26 -= var30 * var27;
                    var30 = 0;
                }
            }

            var32 = 0;
            var33 = 0;
            var34 = 0;
            var35 = 0;
            var36 = 12345678;
            var37 = -12345678;
            if (var14 != var13) {
                var35 = (var22 - var21 << 8) / (-var13 + var14);
                if (var14 <= var13) {
                    var34 = var22 << 8;
                    var37 = var13;
                    var36 = var14;
                    var32 = var18 << 8;
                } else {
                    var37 = var14;
                    var32 = var17 << 8;
                    var36 = var13;
                    var34 = var21 << 8;
                }

                var33 = (var18 + -var17 << 8) / (var14 - var13);
                if (var36 < 0) {
                    var32 -= var36 * var33;
                    var34 -= var36 * var35;
                    var36 = 0;
                }

                if (var37 > var25) {
                    var37 = var25;
                }
            }

            var38 = 0;
            var39 = 0;
            var40 = 0;
            var41 = 0;
            int var56 = 12345678;
            int var43 = -12345678;
            if (var15 != var14) {
                var39 = (var19 - var18 << 8) / (var15 + -var14);
                if (var15 <= var14) {
                    var40 = var23 << 8;
                    var38 = var19 << 8;
                    var56 = var15;
                    var43 = var14;
                } else {
                    var38 = var18 << 8;
                    var43 = var15;
                    var56 = var14;
                    var40 = var22 << 8;
                }

                var41 = (var23 + -var22 << 8) / (-var14 + var15);
                if (var56 < 0) {
                    var40 -= var41 * var56;
                    var38 -= var39 * var56;
                    var56 = 0;
                }

                if (var43 > var25) {
                    var43 = var25;
                }
            }

            int var44 = 0;
            int var45 = 0;
            int var46 = 0;
            int var47 = 0;
            int var48 = 12345678;
            int var49 = -12345678;
            this.anInt343 = var30;
            if (var16 != var15) {
                if (var15 >= var16) {
                    var44 = var20 << 8;
                    var49 = var15;
                    var48 = var16;
                    var46 = var24 << 8;
                } else {
                    var49 = var16;
                    var48 = var15;
                    var46 = var23 << 8;
                    var44 = var19 << 8;
                }

                var47 = (var24 + -var23 << 8) / (var16 - var15);
                var45 = (var20 - var19 << 8) / (-var15 + var16);
                if (var25 < var49) {
                    var49 = var25;
                }

                if (var48 < 0) {
                    var44 -= var48 * var45;
                    var46 -= var47 * var48;
                    var48 = 0;
                }
            }

            if (this.anInt343 > var36) {
                this.anInt343 = var36;
            }

            if (var56 < this.anInt343) {
                this.anInt343 = var56;
            }

            this.anInt380 = var31;
            if (this.anInt343 > var48) {
                this.anInt343 = var48;
            }

            if (this.anInt380 < var37) {
                this.anInt380 = var37;
            }

            if (this.anInt380 < var43) {
                this.anInt380 = var43;
            }

            if (var49 > this.anInt380) {
                this.anInt380 = var49;
            }

            int var50 = 0;

            for (var1 = this.anInt343; this.anInt380 > var1; ++var1) {
                if (var1 >= var30 && var31 > var1) {
                    var3 = var26;
                    var4 = var26;
                    var50 = var28;
                    var11 = var28;
                    var28 += var29;
                    var26 += var27;
                } else {
                    var3 = -655360;
                    var4 = 655360;
                }

                if (var1 >= var36 && var1 < var37) {
                    if (var3 < var32) {
                        var50 = var34;
                        var3 = var32;
                    }

                    if (var32 < var4) {
                        var11 = var34;
                        var4 = var32;
                    }

                    var32 += var33;
                    var34 += var35;
                }

                if (var56 <= var1 && var43 > var1) {
                    if (var4 > var38) {
                        var4 = var38;
                        var11 = var40;
                    }

                    if (var38 > var3) {
                        var50 = var40;
                        var3 = var38;
                    }

                    var40 += var41;
                    var38 += var39;
                }

                if (var1 >= var48 && var1 < var49) {
                    if (var3 < var44) {
                        var50 = var46;
                        var3 = var44;
                    }

                    if (var44 < var4) {
                        var11 = var46;
                        var4 = var44;
                    }

                    var44 += var45;
                    var46 += var47;
                }

                Scanline var51 = this.scanlines[var1];
                var51.anInt1092 = var11;
                var51.anInt1090 = var50;
                var51.lowx = var4;
                var51.highx = var3;
            }

            if (this.anInt343 < this.anInt368 + -this.anInt420) {
                this.anInt343 = this.anInt368 - this.anInt420;
            }
        }

        if (this.aBoolean426 && this.anInt394 > this.anInt373 && this.anInt401 >= this.anInt343 && this.anInt401 < this.anInt380) {
            var53 = this.scanlines[this.anInt401];
            if (var53.lowx >> 8 <= this.anInt344 && this.anInt344 <= (var53.highx >> 8) && var53.highx >= var53.lowx && !var8.aBoolean922 && var8.aByteArray956[var9] == 0) {
                this.aGameModelArray321[this.anInt373] = var8;
                this.anIntArray329[this.anInt373] = var9;
                ++this.anInt373;
                return;
            }
        }
        ;
    }

    private boolean method306(Polygon var1, int var2, Polygon var3) {
        GameModel var4 = var1.model;
        GameModel var5 = var3.model;
        int var6 = var1.anInt169;
        int var7 = var3.anInt169;
        int[] var8 = var4.faceVertices[var6];
        int[] var9 = var5.faceVertices[var7];
        int var10 = var4.faceNumVertices[var6];
        int var11 = var5.faceNumVertices[var7];
        int var12 = var5.anIntArray961[var9[0]];
        int var13 = var5.anIntArray992[var9[0]];
        int var14 = var5.anIntArray973[var9[0]];
        int var15 = var3.anInt166;
        int var16 = var3.anInt161;
        int var17 = var3.anInt163;
        int var18 = var5.anIntArray1008[var7];
        byte var19 = 0;
        int var20 = var3.anInt158;

        int var23;
        int var22;
        for (int var21 = 0; var21 < var10; ++var21) {
            var22 = var8[var21];
            var23 = var17 * (-var4.anIntArray973[var22] + var14) + ((-var4.anIntArray961[var22] + var12) * var15 - -(var16 * (-var4.anIntArray992[var22] + var13)));
            if (-var18 > var23 && var20 < 0 || var18 < var23 && var20 > 0) {
                var19 = 1;
                break;
            }
        }

        if (var19 == 0) {
            return true;
        } else {
            var18 = var4.anIntArray1008[var6];
            var13 = var4.anIntArray992[var8[0]];
            var15 = var1.anInt166;
            var14 = var4.anIntArray973[var8[0]];
            boolean var26 = false;
            var20 = var1.anInt158;
            var16 = var1.anInt161;
            var12 = var4.anIntArray961[var8[var2]];
            var17 = var1.anInt163;

            for (int var24 = 0; var11 > var24; ++var24) {
                var22 = var9[var24];
                var23 = var15 * (var12 + -var5.anIntArray961[var22]) - (-((-var5.anIntArray992[var22] + var13) * var16) + -(var17 * (-var5.anIntArray973[var22] + var14)));
                if (var23 < (-var18) && var20 > 0 || var23 > var18 && var20 < 0) {
                    var26 = true;
                    break;
                }
            }

            return !var26;
        }
    }

    private boolean method308(boolean var1, int var2, int var3, int var4, int var5, int var6) {
        if ((!var1 || var4 > var6) && var6 <= var4) {
            if (var4 < var3) {
                return true;
            } else if (var6 > var5) {
                return true;
            } else if (var5 < var3) {
                return true;
            } else {
                if (var2 >= -67) {
                    this.scanlines = null;
                }

                return var1;
            }
        } else {
            return var3 < var4 ? true : (var5 > var6 ? true : (var5 > var3 ? true : !var1));
        }
    }

    private void method309(int var1, int var2, int var3, Polygon[] var4) {
        for (int var5 = 0; var3 >= var5; ++var5) {
            var4[var5].aBoolean154 = false;
            var4[var5].anInt152 = var5;
            var4[var5].anInt168 = -1;
        }

        if (var1 != 22692) {
            this.method324(48, false, -12, 127);
        }

        int var6 = 0;

        while (true) {
            while (var4[var6].aBoolean154) {
                ++var6;
            }

            if (var6 == var3) {
                return;
            }

            Polygon var7 = var4[var6];
            var7.aBoolean154 = true;
            int var8 = var6;
            int var9 = var6 + var2;
            if (var3 <= var9) {
                var9 = var3 - 1;
            }

            for (int var10 = var9; var10 >= 1 + var8; --var10) {
                Polygon var11 = var4[var10];
                if (var7.anInt167 < var11.anInt156 && var7.anInt156 > var11.anInt167 && var7.anInt164 < var11.anInt159 && var7.anInt159 > var11.anInt164 && var7.anInt152 != var11.anInt168 && !this.method317(-1, var7, var11) && this.method306(var11, 0, var7)) {
                    this.method301(var4, var10, -31, var8);
                    var8 = this.anInt427;
                    if (var11 != var4[var10]) {
                        ++var10;
                    }

                    var11.anInt168 = var7.anInt152;
                }
            }
        }
    }

    private void method310(int var1, int var2, Polygon[] var3, int var4) {
        if (var4 < var1) {
            int var5 = var4 - 1;
            int var6 = var1 + 1;
            int var7 = (var4 + var1) / 2;
            Polygon var8 = var3[var7];
            var3[var7] = var3[var4];
            var3[var4] = var8;
            int var9 = var8.anInt160;

            while (var5 < var6) {
                do {
                    --var6;
                } while (var3[var6].anInt160 < var9);

                do {
                    ++var5;
                } while (var9 < var3[var5].anInt160);

                if (var6 > var5) {
                    Polygon var10 = var3[var5];
                    var3[var5] = var3[var6];
                    var3[var6] = var10;
                }
            }

            this.method310(var6, -75, var3, var4);
            this.method310(var1, -102, var3, 1 + var6);
        }

        if (var2 > -70) {
            this.method321(123, -59, -81, 122);
        }
    }

    final int[] getMousePickedFaces(int var1) {
        if (var1 != -26772) {
            this.anInt380 = 36;
        }

        return this.anIntArray329;
    }

    final void freeModel(byte var1, GameModel var2) {
        for (int var3 = 0; var3 < this.anInt365; ++var3) {
            if (this.aGameModelArray340[var3] == var2) {
                --this.anInt365;

                for (int var4 = var3; var4 < this.anInt365; ++var4) {
                    this.aGameModelArray340[var4] = this.aGameModelArray340[var4 - -1];
                    this.anIntArray377[var4] = this.anIntArray377[var4 - -1];
                }
            }
        }

        if (var1 <= 97) {
            this.anIntArray329 = null;
        }
    }

    final void setFaceSpriteLocalPlayer(int var1, int var2) {
        this.view.aByteArray956[var1] = 1;
        if (var2 != -32653) {
            this.anIntArray339 = null;
        }
    }

    private boolean method315(int var1, boolean var2, int var3, int var4, int var5) {
        return (!var2 || var4 < var3) && var4 <= var3 ? (var5 > -38 ? true : (var4 > var1 ? true : var2)) : (var1 > var4 ? true : !var2);
    }

    final void setMidpoints(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        this.anInt371 = var4;
        this.anInt407 = var3;
        this.anInt351 = var5;
        this.anInt420 = var2;
        this.anInt357 = var1;
        this.scanlines = new Scanline[var6 + var2];
        this.anInt368 = var6;

        for (int var8 = var7; var2 + var6 > var8; ++var8) {
            this.scanlines[var8] = new Scanline();
        }
    }

    private boolean method317(int var1, Polygon var2, Polygon var3) {
        if (var3.anInt156 <= var2.anInt167) {
            return true;
        } else if (var2.anInt156 <= var3.anInt167) {
            return true;
        } else if (var2.anInt164 >= var3.anInt159) {
            return true;
        } else if (var3.anInt164 >= var2.anInt159) {
            return true;
        } else if (var2.anInt162 >= var3.anInt171) {
            return true;
        } else if (var2.anInt171 < var3.anInt162) {
            return false;
        } else {
            GameModel var4 = var2.model;
            GameModel var5 = var3.model;
            int var6 = var2.anInt169;
            int var7 = var3.anInt169;
            int[] var8 = var4.faceVertices[var6];
            int[] var9 = var5.faceVertices[var7];
            int var10 = var4.faceNumVertices[var6];
            int var11 = var5.faceNumVertices[var7];
            int var12 = var5.anIntArray961[var9[0]];
            int var13 = var5.anIntArray992[var9[0]];
            int var14 = var5.anIntArray973[var9[0]];
            int var15 = var3.anInt166;
            int var16 = var3.anInt161;
            int var17 = var3.anInt163;
            int var18 = var5.anIntArray1008[var7];
            int var19 = var3.anInt158;
            boolean var20 = false;

            int var23;
            int var22;
            for (int var21 = 0; var21 < var10; ++var21) {
                var22 = var8[var21];
                var23 = var15 * (var12 + -var4.anIntArray961[var22]) - -(var16 * (var13 - var4.anIntArray992[var22])) + (-var4.anIntArray973[var22] + var14) * var17;
                if (-var18 > var23 && var19 < 0 || var23 > var18 && var19 > 0) {
                    var20 = true;
                    break;
                }
            }

            if (!var20) {
                return true;
            } else {
                byte var32 = 0;
                var12 = var4.anIntArray961[var8[0]];
                var13 = var4.anIntArray992[var8[0]];
                var17 = var2.anInt163;
                var16 = var2.anInt161;
                var15 = var2.anInt166;
                var19 = var2.anInt158;
                var18 = var4.anIntArray1008[var6];
                var14 = var4.anIntArray973[var8[0]];

                for (int var24 = 0; var11 > var24; ++var24) {
                    var22 = var9[var24];
                    var23 = (var14 - var5.anIntArray973[var22]) * var17 + var16 * (var13 - var5.anIntArray992[var22]) + var15 * (var12 + -var5.anIntArray961[var22]);
                    if (-var18 > var23 && var19 > 0 || var23 > var18 && var19 < 0) {
                        var32 = 1;
                        break;
                    }
                }

                if (~var32 == var1) {
                    return true;
                } else {
                    int[] var25;
                    int var27;
                    int[] var26;
                    int var28;
                    if (var10 != 2) {
                        var25 = new int[var10];
                        var26 = new int[var10];

                        for (var27 = 0; var10 > var27; ++var27) {
                            var28 = var8[var27];
                            var26[var27] = var4.anIntArray1001[var28];
                            var25[var27] = var4.anIntArray994[var28];
                        }
                    } else {
                        var26 = new int[4];
                        var25 = new int[4];
                        var27 = var8[0];
                        var22 = var8[1];
                        var26[0] = var4.anIntArray1001[var27] - 20;
                        var26[1] = var4.anIntArray1001[var22] - 20;
                        var26[2] = 20 + var4.anIntArray1001[var22];
                        var26[3] = 20 + var4.anIntArray1001[var27];
                        var25[0] = var25[3] = var4.anIntArray994[var27];
                        var25[1] = var25[2] = var4.anIntArray994[var22];
                    }

                    int[] var29;
                    int[] var30;
                    if (var11 != 2) {
                        var29 = new int[var11];
                        var30 = new int[var11];

                        for (var27 = 0; var27 < var11; ++var27) {
                            var28 = var9[var27];
                            var30[var27] = var5.anIntArray1001[var28];
                            var29[var27] = var5.anIntArray994[var28];
                        }
                    } else {
                        var29 = new int[4];
                        var30 = new int[4];
                        var22 = var9[1];
                        var27 = var9[0];
                        var30[0] = -20 + var5.anIntArray1001[var27];
                        var30[1] = -20 + var5.anIntArray1001[var22];
                        var30[2] = var5.anIntArray1001[var22] + 20;
                        var30[3] = 20 + var5.anIntArray1001[var27];
                        var29[0] = var29[3] = var5.anIntArray994[var27];
                        var29[1] = var29[2] = var5.anIntArray994[var22];
                    }

                    return !this.method297(var25, var29, var1 ^ -20022, var30, var26);
                }
            }
        }
    }

    final void setMouseLoc(int var1, int var2, boolean var3) {
        this.aBoolean426 = true;
        if (var3) {
            this.anIntArray339 = null;
        }

        this.anInt373 = 0;
        this.anInt344 = -this.anInt371 + var2;
        this.anInt401 = var1;
    }

    final void setCamera(int var1, int var2, int var3, byte var4, int var5, int var6, int var7, int var8) {
        var3 &= 1023;
        var2 &= 1023;
        var8 &= 1023;
        this.anInt345 = 1023 & 1024 - var8;
        this.anInt332 = 1024 + -var2 & 1023;
        this.anInt337 = -var3 + 1024 & 1023;
        int var9 = 0;
        int var10 = 0;
        int var11 = var5;
        if (var4 != 98) {
            this.anIntArrayArray328 = null;
        }

        int var12;
        int var13;
        int var14;
        if (var8 != 0) {
            var12 = anIntArray823[var8 + 1024];
            var13 = anIntArray823[var8];
            var14 = var12 * var10 + -(var5 * var13) >> 15;
            var11 = var13 * var10 + var5 * var12 >> 15;
            var10 = var14;
        }

        if (var2 != 0) {
            var13 = anIntArray823[var2];
            var12 = anIntArray823[var2 + 1024];
            var14 = var11 * var13 - -(var12 * var9) >> 15;
            var11 = var11 * var12 + -(var9 * var13) >> 15;
            var9 = var14;
        }

        if (var3 != 0) {
            var12 = anIntArray823[1024 + var3];
            var13 = anIntArray823[var3];
            var14 = var13 * var10 - -(var12 * var9) >> 15;
            var10 = -(var9 * var13) + var12 * var10 >> 15;
            var9 = var14;
        }

        this.anInt381 = var1 + -var9;
        this.anInt364 = -var10 + var6;
        this.anInt404 = var7 - var11;
    }

    final void setCombatXOffset(int var1, byte var2, int var3) {
        if (var2 != -30) {
            this.aBooleanArray341 = null;
        }

        this.anIntArray425[var1] = var3;
    }

    final void method321(int var1, int var2, int var3, int var4) {
        if (var2 == 0 && var3 == 0 && var1 == 0) {
            var2 = 32;
        }

        if (var4 != 1) {
            this.aBoolean379 = false;
        }


        for (int var5 = 0; this.anInt365 > var5; ++var5) {
            this.aGameModelArray340[var5].method554(var3, var1, -243635764, var2);
        }
    }

    private void method322(boolean var1) {
        if (!var1) {
            this.method315(-116, true, -107, 55, -13);
        }

        this.anInt395 = 0;
        this.view.method580((byte) 109);
    }

    final void method323(boolean var1) {
        this.method322(var1);

        for (int var2 = 0; this.anInt365 > var2; ++var2) {
            this.aGameModelArray340[var2] = null;
        }

        if (!var1) {
            method311(-60, true);
        }

        this.anInt365 = 0;
    }

    private void method324(int var1, boolean var2, int var3, int var4) {
        int var5 = 1023 & 1024 + -this.anInt345;
        int var6 = 1023 & -this.anInt332 + 1024;
        int var7 = 1023 & -this.anInt337 - -1024;
        int var8;
        int var9;
        int var10;
        if (var7 != 0) {
            var8 = anIntArray823[var7 + 1024];
            var9 = anIntArray823[var7];
            var10 = var8 * var3 + var4 * var9 >> 15;
            var4 = -(var9 * var3) + var8 * var4 >> 15;
            var3 = var10;
        }

        if (var5 != 0) {
            var8 = anIntArray823[var5 + 1024];
            var9 = anIntArray823[var5];
            var10 = var8 * var4 + -(var9 * var1) >> 15;
            var1 = var8 * var1 + var9 * var4 >> 15;
            var4 = var10;
        }

        if (var6 != 0) {
            var9 = anIntArray823[var6];
            var8 = anIntArray823[1024 + var6];
            var10 = var9 * var1 + var3 * var8 >> 15;
            var1 = var1 * var8 + -(var9 * var3) >> 15;
            var3 = var10;
        }

        if (anInt762 > var3) {
            anInt762 = var3;
        }

        if (SoundPlayer.anInt769 < var4) {
            SoundPlayer.anInt769 = var4;
        }

        if (var1 > World.anInt273) {
            World.anInt273 = var1;
        }

        if (GameModel.anInt1031 > var1) {
            GameModel.anInt1031 = var1;
        }

        if (anInt465 > var4) {
            anInt465 = var4;
        }

        if (!var2) {
            if (BufferBase_Sub3_Sub1.anInt1741 < var3) {
                BufferBase_Sub3_Sub1.anInt1741 = var3;
            }
        }
        ;
    }

    final void method325(GameModel var1, byte var2) {
        if (var1 == null) {
            System.out.println("Warning tried to add null object!");
        }

        if (var2 == 6) {
            if (this.anInt356 > this.anInt365) {
                this.anIntArray377[this.anInt365] = 0;
                this.aGameModelArray340[this.anInt365++] = var1;
            }
        }
    }

    private void method326(boolean var1, int var2) {
        Polygon var3 = this.visibleModelPolygons[var2];
        GameModel var4 = var3.model;
        int var5 = var3.anInt169;
        int[] var6 = var4.faceVertices[var5];
        if (!var1) {
            byte var7 = 0;
            byte var8 = 0;
            byte var9 = 1;
            int var10 = var4.anIntArray961[var6[0]];
            int var11 = var4.anIntArray992[var6[0]];
            int var12 = var4.anIntArray973[var6[0]];
            var4.anIntArray1008[var5] = 1;
            var4.anIntArray1003[var5] = 0;
            var3.anInt166 = var7;
            var3.anInt158 = var9 * var12 + var8 * var11 + var10 * var7;
            var3.anInt163 = var9;
            var3.anInt161 = var8;
            int var13 = var4.anIntArray973[var6[0]];
            int var14 = var13;
            int var15 = var4.anIntArray1001[var6[0]];
            int var16 = var15;
            if (var4.anIntArray1001[var6[1]] >= var15) {
                var16 = var4.anIntArray1001[var6[1]];
            } else {
                var15 = var4.anIntArray1001[var6[1]];
            }

            int var17 = var4.anIntArray994[var6[1]];
            int var18 = var4.anIntArray973[var6[1]];
            int var19 = var4.anIntArray994[var6[0]];
            if (var18 > var13) {
                var14 = var18;
            } else if (var18 < var13) {
                var13 = var18;
            }

            var18 = var4.anIntArray1001[var6[1]];
            if (var16 >= var18) {
                if (var18 < var15) {
                    var15 = var18;
                }
            } else {
                var16 = var18;
            }

            var18 = var4.anIntArray994[var6[1]];
            if (var19 >= var18) {
                if (var17 > var18) {
                    var17 = var18;
                }
            } else {
                var19 = var18;
            }

            var3.anInt162 = var13;
            var3.anInt156 = 20 + var16;
            var3.anInt167 = var15 - 20;
            var3.anInt171 = var14;
            var3.anInt164 = var17;
            var3.anInt159 = var19;
        }
        ;
    }

    final void method327(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (var2 == ~var3 && var4 == 0 && var1 == 0) {
            var3 = 32;
        }


        for (int var7 = 0; this.anInt365 > var7; ++var7) {
            this.aGameModelArray340[var7].method567(var4, true, var1, var6, var3, var5);
        }
    }

    final void method328(boolean var1, int var2) {
        if (this.anIntArrayArray335[var2] != null) {
            int[] var3 = this.anIntArrayArray335[var2];
            if (var1) {
                int var6;
                int var7;
                for (int var4 = 0; var4 < 64; ++var4) {
                    int var5 = var4 + 4032;
                    var6 = var3[var5];

                    for (var7 = 0; var7 < 63; ++var7) {
                        var3[var5] = var3[-64 + var5];
                        var5 -= 64;
                    }

                    this.anIntArrayArray335[var2][var5] = var6;
                }

                short var9 = 4096;

                for (var6 = 0; var9 > var6; ++var6) {
                    var7 = var3[var6];
                    var3[var6 + var9] = Utility.bitwiseAnd(16316671, var7 + -(var7 >>> 3));
                    var3[var6 + 2 * var9] = Utility.bitwiseAnd(16316671, var7 - (var7 >>> 2));
                    var3[var6 + 3 * var9] = Utility.bitwiseAnd(-(var7 >>> 2) + var7 + -(var7 >>> 3), 16316671);
                }

            }
        }
    }

    final void reduceSprites(int var1, byte var2) {
        if (var2 != -39) {
            this.anInt371 = 64;
        }

        this.anInt395 -= var1;
        this.view.method570(var1, 2 * var1, -23817);
        if (this.anInt395 < 0) {
            this.anInt395 = 0;
        }
    }

    private int method330(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (var2 != 1) {
            this.setCamera(30, -109, -28, (byte) 26, 31, 91, 123, -40);
        }

        return var1 == var6 ? var4 : var4 + (-var1 + var5) * (var3 - var4) / (-var1 + var6);
    }

    private void method331(int var1, int var2) {
        Polygon var3 = this.visibleModelPolygons[var1];
        GameModel var4 = var3.model;
        int var5 = var3.anInt169;
        int[] var6 = var4.faceVertices[var5];
        int var7 = var4.faceNumVertices[var5];
        int var8 = var4.anIntArray1003[var5];
        int var9 = var4.anIntArray961[var6[0]];
        int var10 = var4.anIntArray992[var6[0]];
        int var11 = var4.anIntArray973[var6[0]];
        int var12 = var4.anIntArray961[var6[1]] + -var9;
        int var13 = var4.anIntArray992[var6[1]] - var10;
        int var14 = -var11 + var4.anIntArray973[var6[1]];
        int var15 = var4.anIntArray961[var6[2]] + -var9;
        int var16 = var4.anIntArray992[var6[2]] + -var10;
        int var17 = -var11 + var4.anIntArray973[var6[2]];
        int var18 = -(var16 * var14) + var17 * var13;
        int var19 = -(var17 * var12) + var15 * var14;
        int var20 = var12 * var16 + -(var13 * var15);
        if (var8 == -1) {
            for (var8 = 0; var18 > 25000 || var19 > 25000 || var20 > 25000 || var18 < -25000 || var19 < -25000 || var20 < -25000; var20 >>= 1) {
                var18 >>= 1;
                ++var8;
                var19 >>= 1;
            }

            var4.anIntArray1003[var5] = var8;
            var4.anIntArray1008[var5] = (int) ((double) this.anInt349 * Math.sqrt((double) (var20 * var20 + var19 * var19 + var18 * var18)));
        } else {
            var19 >>= var8;
            var20 >>= var8;
            var18 >>= var8;
        }

        var3.anInt163 = var20;
        var3.anInt161 = var19;
        var3.anInt166 = var18;
        var3.anInt158 = var20 * var11 + var10 * var19 + var18 * var9;
        int var21 = var4.anIntArray973[var6[0]];
        int var22 = var21;
        int var23 = var4.anIntArray1001[var6[0]];
        int var24 = var23;
        int var25 = var4.anIntArray994[var6[0]];
        int var26 = var25;

        for (int var27 = var2; var27 < var7; ++var27) {
            int var28 = var4.anIntArray973[var6[var27]];
            if (var28 <= var22) {
                if (var28 < var21) {
                    var21 = var28;
                }
            } else {
                var22 = var28;
            }

            var28 = var4.anIntArray1001[var6[var27]];
            if (var28 <= var24) {
                if (var23 > var28) {
                    var23 = var28;
                }
            } else {
                var24 = var28;
            }

            var28 = var4.anIntArray994[var6[var27]];
            if (var26 < var28) {
                var26 = var28;
            } else if (var28 < var25) {
                var25 = var28;
            }
        }

        var3.anInt164 = var25;
        var3.anInt171 = var22;
        var3.anInt162 = var21;
        var3.anInt156 = var24;
        var3.anInt167 = var23;
        var3.anInt159 = var26;
    }

    final int drawSprite(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        this.anIntArray339[this.anInt395] = var4;
        this.anIntArray336[this.anInt395] = var7;
        this.anIntArray392[this.anInt395] = var8;
        this.anIntArray383[this.anInt395] = var6;
        this.anIntArray370[this.anInt395] = var3;
        this.anIntArray325[this.anInt395] = var1;
        this.anIntArray425[this.anInt395] = 0;
        int var9 = this.view.method574(var6, var7, var8, -1);
        int var10 = this.view.method574(var6, var7, var8 + -var1, -1);
        int[] var11 = new int[]{var9, var10};
        this.view.method549(var2, 0, (byte) -118, var11, 0);
        this.view.faceTag[this.anInt395] = var5;
        this.view.aByteArray956[this.anInt395++] = 0;
        return this.anInt395 + -1;
    }

    private void method333(boolean var1, int var2) {
        if (!var1) {
            if (var2 >= 0) {
                this.aLongArray423[var2] = (long) (JagGrab.aLong1131++);
                if (this.anIntArrayArray335[var2] == null) {
                    int var3;
                    long var4;
                    int var6;
                    int var7;
                    if (this.anIntArray417[var2] != 0) {
                        for (var3 = 0; var3 < this.anIntArrayArray327.length; ++var3) {
                            if (this.anIntArrayArray327[var3] == null) {
                                this.anIntArrayArray327[var3] = new int[65536];
                                this.anIntArrayArray335[var2] = this.anIntArrayArray327[var3];
                                this.method303(31545, var2);
                                return;
                            }
                        }

                        var4 = 1073741824L;
                        var6 = 0;

                        for (var7 = 0; var7 < this.anInt326; ++var7) {
                            if (var7 != var2 && this.anIntArray417[var7] == 1 && this.anIntArrayArray335[var7] != null && var4 > this.aLongArray423[var7]) {
                                var4 = this.aLongArray423[var7];
                                var6 = var7;
                            }
                        }

                        this.anIntArrayArray335[var2] = this.anIntArrayArray335[var6];
                        this.anIntArrayArray335[var6] = null;
                        this.method303(31545, var2);
                    } else {
                        for (var3 = 0; this.anIntArrayArray328.length > var3; ++var3) {
                            if (this.anIntArrayArray328[var3] == null) {
                                this.anIntArrayArray328[var3] = new int[16384];
                                this.anIntArrayArray335[var2] = this.anIntArrayArray328[var3];
                                this.method303(31545, var2);
                                return;
                            }
                        }

                        var4 = 1073741824L;
                        var6 = 0;

                        for (var7 = 0; this.anInt326 > var7; ++var7) {
                            if (var2 != var7 && this.anIntArray417[var7] == 0 && this.anIntArrayArray335[var7] != null && this.aLongArray423[var7] < var4) {
                                var6 = var7;
                                var4 = this.aLongArray423[var7];
                            }
                        }

                        this.anIntArrayArray335[var2] = this.anIntArrayArray335[var6];
                        this.anIntArrayArray335[var6] = null;
                        this.method303(31545, var2);
                    }
                }
            }
        }
    }

    final int getMousePickedCount(int var1) {
        if (var1 <= 24) {
            this.method326(true, -22);
        }

        return this.anInt373;
    }

}
