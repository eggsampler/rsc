import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

final class GameFrame extends Frame {

    static char[] aCharArray1033 = new char[]{'[', ']', '#'};
    static String[] aStringArray103 = new String[]{"Enter number of items to stake and press enter"};
    static byte[][] aByteArrayArray105 = new byte[12][];
    static int anInt108 = 0;
    private int anInt101 = 28;
    private int anInt106;
    private int anInt107;
    private GameApplet anGameApplet__111;
    private int anInt113 = 0;


    GameFrame(GameApplet var1, int var2, int var3, String var4, boolean var5, boolean var6) {
        this.anInt107 = var3;
        this.anGameApplet__111 = var1;
        if (!var6) {
            this.anInt101 = 28;
        } else {
            this.anInt101 = 48;
        }

        this.anInt106 = var2;
        this.setTitle(var4);
        this.setResizable(var5);
        this.show();
        this.toFront();
        this.resize(this.anInt106, this.anInt107);
        this.getGraphics();

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                anGameApplet__111.destroy();
            }
        });
    }

    static boolean method145(GameModeWhere var0, boolean var1) {
        if (!var1) {
            aStringArray103 = null;
        }

        return var0 == GameModeWhere.WTRC || var0 == GameModeWhere.WTQA || GameModeWhere.WTWIP == var0 || GameModeWhere.WTI == var0 || GameModeWhere.INTBETA == var0;
    }

    static boolean method147(char var0, int var1) {
        if (java.lang.Character.isISOControl(var0)) {
            return false;
        } else if (World.method282(var0, (byte) -115)) {
            return true;
        } else {
            char[] var2 = Menu.aCharArray706;

            for (int var3 = var1; var3 < var2.length; ++var3) {
                char var4 = var2[var3];
                if (var4 == var0) {
                    return true;
                }
            }

            char[] var8 = aCharArray1033;

            for (int var5 = 0; var8.length > var5; ++var5) {
                char var6 = var8[var5];
                if (var6 == var0) {
                    return true;
                }
            }

            return false;
        }
    }

    public final void paint(Graphics var1) {
        this.anGameApplet__111.paint(var1);
    }

    public final void resize(int var1, int var2) {
        super.resize(var1, var2 - -this.anInt101);
    }

    protected final void processEvent(AWTEvent var1) {
        if (var1 instanceof MouseEvent) {
            MouseEvent var2 = (MouseEvent) var1;
            var1 = new MouseEvent(var2.getComponent(), var2.getID(), var2.getWhen(), var2.getModifiers(), var2.getX(), -24 + var2.getY(), var2.getClickCount(), var2.isPopupTrigger());
        }

        super.processEvent((AWTEvent) var1);
    }

    public final Graphics getGraphics() {
        Graphics var1 = super.getGraphics();
        if (this.anInt113 != 0) {
            var1.translate(-5, 0);
        } else {
            var1.translate(0, 24);
        }

        return var1;
    }

}
