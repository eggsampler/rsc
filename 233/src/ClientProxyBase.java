import java.io.IOException;
import java.net.Socket;

abstract class ClientProxyBase {

    String address;
    int port;

    abstract Socket getSystemProxy(int skip) throws IOException;

    final Socket openDirectConnection() throws IOException {
        return new Socket(this.address, this.port);
    }

}
