import java.io.EOFException;
import java.io.File;
import java.io.IOException;

final class CacheFileManager {

    static GameApplet anGameApplet__299;
    private int read;
    private byte[] aByteArray294;
    private long length;
    private long aLong298 = -1L;
    private long length2;
    private long position;
    private int anInt303 = 0;
    private byte[] buffer;
    private CacheFile cacheFile;
    private long position2;
    private long aLong311 = -1L;


    CacheFileManager(CacheFile cacheFile, int var2, int var3) throws IOException {
        this.cacheFile = cacheFile;
        this.length = this.length2 = cacheFile.getLength();
        this.buffer = new byte[var2];
        this.position = 0L;
        this.aByteArray294 = new byte[var3];
    }

    final void seek(long position, byte var3) throws IOException {
        if (var3 > -105) {
            this.aLong311 = 19L;
        }

        if (position < 0) {
            throw new IOException("Invalid seek to " + position + " in file " + this.getCacheFile(119));
        } else {
            this.position = position;
        }
    }

    final void write(byte[] var1, int var2) throws IOException {
        this.write(0, var1, var1.length);
    }

    final void write(int var1, byte[] var3, int var4) throws IOException {// METHOD IS TOO COMPLEX TO ANALYSE BY yourstruly
        try {
            if (var3.length < var1 + var4) {
                throw new ArrayIndexOutOfBoundsException(var4 + var1 + -var3.length);
            }

            if (this.aLong298 != -1L && this.position >= this.aLong298 && ((long) var4 + this.position) <= (this.aLong298 + (long) this.anInt303)) {
                Utility.insertBytes(this.aByteArray294, (int) (this.position + -this.aLong298), var3, var1, var4);
                this.position += (long) var4;
                return;
            }

            long var5 = this.position;
            int var8 = var4;
            int var9;
            if (this.aLong311 <= this.position && (this.aLong311 + (long) this.read) > this.position) {
                var9 = (int) (-this.position - -this.aLong311 + (long) this.read);
                if (var4 < var9) {
                    var9 = var4;
                }

                Utility.insertBytes(this.buffer, (int) (-this.aLong311 + this.position), var3, var1, var9);
                var1 += var9;
                this.position += (long) var9;
                var4 -= var9;
            }

            if (this.buffer.length >= var4) {
                if (var4 > 0) {
                    this.read();
                    var9 = var4;
                    if (this.read < var4) {
                        var9 = this.read;
                    }

                    Utility.insertBytes(this.buffer, 0, var3, var1, var9);
                    var4 -= var9;
                    var1 += var9;
                    this.position += (long) var9;
                }
            } else {
                this.cacheFile.seek(this.position);

                for (this.position2 = this.position; var4 > 0; this.position += (long) var9) {
                    var9 = this.cacheFile.read(var3, var4, var1);
                    if (var9 == -1) {
                        break;
                    }

                    var1 += var9;
                    this.position2 += (long) var9;
                    var4 -= var9;
                }
            }

            if (this.aLong298 != -1L) {
                if (this.aLong298 > this.position && var4 > 0) {
                    var9 = var1 - -((int) (this.aLong298 - this.position));
                    if (var4 + var1 < var9) {
                        var9 = var4 + var1;
                    }

                    while (var9 > var1) {
                        var3[var1++] = 0;
                        --var4;
                        ++this.position;
                    }
                }

                long var10 = -1L;
                if (this.aLong298 >= var5 && this.aLong298 < var5 + (long) var8) {
                    var10 = this.aLong298;
                } else if (var5 >= this.aLong298 && var5 < (long) this.anInt303 + this.aLong298) {
                    var10 = var5;
                }

                long var12 = -1L;
                if (var5 < this.aLong298 + (long) this.anInt303 && (this.aLong298 + (long) this.anInt303) <= ((long) var8 + var5)) {
                    var12 = this.aLong298 - -((long) this.anInt303);
                } else if (this.aLong298 < (long) var8 + var5 && ((long) this.anInt303 + this.aLong298) >= (var5 + (long) var8)) {
                    var12 = (long) var8 + var5;
                }

                if (var10 > -1L && var10 < var12) {
                    int var14 = (int) (-var10 + var12);
                    Utility.insertBytes(this.aByteArray294, (int) (-this.aLong298 + var10), var3, var1 + (int) (-var5 + var10), var14);
                    if (this.position < var12) {
                        var4 = (int) ((long) var4 - (-this.position + var12));
                        this.position = var12;
                    }
                }
            }
        } catch (IOException var16) {
            this.position2 = -1L;
            throw var16;
        }

        if (var4 > 0) {
            throw new EOFException();
        }
    }

    private File getCacheFile(int var1) {
        if (var1 <= 109) {
            return null;
        } else {
            return this.cacheFile.getFile();
        }
    }

    private void flush() throws IOException {
        if (this.aLong298 != -1L) {
            if (this.position2 != this.aLong298) {
                this.cacheFile.seek(this.aLong298);
                this.position2 = this.aLong298;
            }

            this.cacheFile.write(this.aByteArray294, this.anInt303, 0);
            this.position2 += (long) this.anInt303;
            if (this.position2 > this.length2) {
                this.length2 = this.position2;
            }

            long var2 = -1L;
            long var4 = -1L;
            if (this.aLong298 >= this.aLong311 && this.aLong298 < ((long) this.read + this.aLong311)) {
                var2 = this.aLong298;
            } else if (this.aLong311 >= this.aLong298 && (this.aLong298 - -((long) this.anInt303)) > this.aLong311) {
                var2 = this.aLong311;
            }

            if (this.aLong298 + (long) this.anInt303 > this.aLong311 && (long) this.read + this.aLong311 >= this.aLong298 - -((long) this.anInt303)) {
                var4 = (long) this.anInt303 + this.aLong298;
            } else if (this.aLong298 < ((long) this.read + this.aLong311) && (long) this.anInt303 + this.aLong298 >= this.aLong311 + (long) this.read) {
                var4 = (long) this.read + this.aLong311;
            }

            if (var2 > -1 && var4 > var2) {
                int var6 = (int) (var4 + -var2);
                Utility.insertBytes(this.aByteArray294, (int) (-this.aLong298 + var2), this.buffer, (int) (-this.aLong311 + var2), var6);
            }

            this.aLong298 = -1L;
            this.anInt303 = 0;
        }
    }

    final void write(byte[] buf, int offfset, int length) throws IOException {
        try {
            if (((long) length + this.position) > this.length) {
                this.length = this.position + (long) length;
            }

            if (this.aLong298 != -1L && (this.position < this.aLong298 || this.aLong298 - -((long) this.anInt303) < this.position)) {
                this.flush();
            }

            if (this.aLong298 != -1 && (this.position - -((long) length)) > ((long) this.aByteArray294.length + this.aLong298)) {
                int var5 = (int) ((long) this.aByteArray294.length + -this.position + this.aLong298);
                Utility.insertBytes(buf, offfset, this.aByteArray294, (int) (-this.aLong298 + this.position), var5);
                this.position += (long) var5;
                offfset += var5;
                length -= var5;
                this.anInt303 = this.aByteArray294.length;
                this.flush();
            }

            if (this.aByteArray294.length < length) {
                if (this.position != this.position2) {
                    this.cacheFile.seek(this.position);
                    this.position2 = this.position;
                }

                this.cacheFile.write(buf, length, offfset);
                this.position2 += (long) length;
                if (this.position2 > this.length2) {
                    this.length2 = this.position2;
                }

                long var6 = -1L;
                if (this.aLong311 <= this.position && this.position < ((long) this.read + this.aLong311)) {
                    var6 = this.position;
                } else if (this.position <= this.aLong311 && this.position + (long) length > this.aLong311) {
                    var6 = this.aLong311;
                }

                long var8 = -1L;
                if (this.aLong311 < (this.position - -((long) length)) && (long) length + this.position <= (long) this.read + this.aLong311) {
                    var8 = (long) length + this.position;
                } else if ((this.aLong311 - -((long) this.read)) > this.position && (long) length + this.position >= this.aLong311 - -((long) this.read)) {
                    var8 = this.aLong311 - -((long) this.read);
                }

                if (var6 > -1 && var6 < var8) {
                    int var10 = (int) (var8 - var6);
                    Utility.insertBytes(buf, (int) (-this.position + ((long) offfset - -var6)), this.buffer, (int) (var6 - this.aLong311), var10);
                }

                this.position += (long) length;
                return;
            }

            if (length > 0) {
                if (this.aLong298 == -1L) {
                    this.aLong298 = this.position;
                }

                Utility.insertBytes(buf, offfset, this.aByteArray294, (int) (this.position + -this.aLong298), length);
                this.position += (long) length;
                if (((long) this.anInt303) < (this.position + -this.aLong298)) {
                    this.anInt303 = (int) (this.position - this.aLong298);
                }

                return;
            }
        } catch (IOException var11) {
            this.position2 = -1L;
            throw var11;
        }
    }

    private void read() throws IOException {
        this.read = 0;
        if (this.position != this.position2) {
            this.cacheFile.seek(this.position);
            this.position2 = this.position;
        }

        this.aLong311 = this.position;
        while (this.buffer.length > this.read) {
            int var2 = -this.read + this.buffer.length;
            if (var2 > 200000000) {
                var2 = 200000000;
                // 200 000 000
            }

            int read = this.cacheFile.read(this.buffer, var2, this.read);
            if (read == -1) {
                break;
            }

            this.position2 += (long) read;
            this.read += read;
        }
    }

    final long method294(boolean var1) {
        return this.length;
    }

}
