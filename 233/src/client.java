import java.awt.*;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.Socket;

public final class client extends GameApplet {

    static int[] friendListOnline = new int[200];
    static String[] aStringArray138 = new String[]{"Enter number of items to offer and press enter"};
    static byte[] aByteArray65 = method155(128);
    static String[] friendListServer = new String[200];
    static String[] aStringArray544 = new String[]{"Enter number of items to remove and press enter"};
    static String[] messageColors = new String[]{"@whi@", "@cya@", "@cya@", "@whi@", "@yel@", "@cya@", "@whi@", "@whi@"};
    static GameModeWhere modewhere;
    static BigInteger exponent = new BigInteger("58778699976184461502525193738213253649000149147835990136706041084440742975821");
    static String[] aStringArray1034 = new String[]{"Please enter the number of items to deposit", "and press enter"};
    static int friendListCount = 0;
    static String[] messageColor = new String[100];
    static String[] ignoreListOldNames = new String[100];
    static int nodeid;
    static int[] messageCrowns = new int[100];
    static String[] ignoreListAccNames = new String[100];
    static int maxReadTries = 0;
    static GameModeWhat modewhat;
    static Class11 aClass11_315 = new Class11(new byte[]{(byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 21, (byte) 22, (byte) 22, (byte) 20, (byte) 22, (byte) 22, (byte) 22, (byte) 21, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 3, (byte) 8, (byte) 22, (byte) 16, (byte) 22, (byte) 16, (byte) 17, (byte) 7, (byte) 13, (byte) 13, (byte) 13, (byte) 16, (byte) 7, (byte) 10, (byte) 6, (byte) 16, (byte) 10, (byte) 11, (byte) 12, (byte) 12, (byte) 12, (byte) 12, (byte) 13, (byte) 13, (byte) 14, (byte) 14, (byte) 11, (byte) 14, (byte) 19, (byte) 15, (byte) 17, (byte) 8, (byte) 11, (byte) 9, (byte) 10, (byte) 10, (byte) 10, (byte) 10, (byte) 11, (byte) 10, (byte) 9, (byte) 7, (byte) 12, (byte) 11, (byte) 10, (byte) 10, (byte) 9, (byte) 10, (byte) 10, (byte) 12, (byte) 10, (byte) 9, (byte) 8, (byte) 12, (byte) 12, (byte) 9, (byte) 14, (byte) 8, (byte) 12, (byte) 17, (byte) 16, (byte) 17, (byte) 22, (byte) 13, (byte) 21, (byte) 4, (byte) 7, (byte) 6, (byte) 5, (byte) 3, (byte) 6, (byte) 6, (byte) 5, (byte) 4, (byte) 10, (byte) 7, (byte) 5, (byte) 6, (byte) 4, (byte) 4, (byte) 6, (byte) 10, (byte) 5, (byte) 4, (byte) 4, (byte) 5, (byte) 7, (byte) 6, (byte) 10, (byte) 6, (byte) 10, (byte) 22, (byte) 19, (byte) 22, (byte) 14, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 22, (byte) 21, (byte) 22, (byte) 21, (byte) 22, (byte) 22, (byte) 22, (byte) 21, (byte) 22, (byte) 22});
    static int[] messageHistoryTimeout = new int[100];
    static String[] aStringArray214 = new String[]{"Please enter the number of items to withdraw", "and press enter"};
    static BigInteger modulus = new BigInteger("7162900525229798032761816791230527296329313291232324290237849263501208207972894053929065636522363163621000728841182238772712427862772219676577293600221789");
    static Class11 aClass11_1110;
    static int baseSpriteStart = 0;
    static String[] friendListNames = new String[200];
    static String[] friendListOldNames = new String[200];
    static String[] ignoreListServers = new String[100];
    static String[] ignoreListNames = new String[100];
    static String[] aStringArray1659 = new String[]{"Are you sure you wish to skip the tutorial", "and teleport to Lumbridge?"};

    static String[] aStringArray819 = new String[]{"Type the number of items to buy and press enter"};
    static String[] messageSenders = new String[100];
    static String[] messageSenderClans = new String[100];
    static String[] messageMessages = new String[100];
    private static int ignoreListCount = 0;
    private static int[] messageTypes = new int[100];
    private BufferBase_Sub3_Sub1 packetsIncoming = new BufferBase_Sub3_Sub1(5000);
    private int serverJagGrabPort;
    private int serverPort;
    private ClientStream clientStream;
    private String serverAddress;
    private int anInt1260;
    private int worldFullTimeout = 0;
    private int cameraAngle = 1;
    private int settingsBlockPrivate = 0;
    private int anInt1265 = 0;
    private Character[] players = new Character[500];
    private int objectAnimationNumberFireLightningSpell = 0;
    private long packetLastRead;
    private int spriteTexture;
    private int objectAnimationNumberClaw = 0;
    private boolean errorLoadingCodebase = false;
    private int anInt1275 = 0;
    private int spriteMedia;
    private boolean errorLoadingMemory = false;
    private int cameraAutoRotatePlayerX = 0;
    private int planeIndex = -1;
    private boolean veterans = false;
    private boolean members = false;
    private int anInt1285 = 9;
    private int projectileMaxRange = 40;
    private int spriteTextureWorld;
    private int settingsBlockDuel = 0;
    private int anInt1289 = 0;
    private int autoLoginTimeout = 0;
    private Scene scene;
    private int anInt1292;
    private int objectAnimationNumberTorch = 0;
    private int anInt1295 = 0;
    private Graphics graphics;
    private int anInt1298;
    private int cameraZoom = 550;
    private int modlevel1 = 0;
    private int regionX;
    private int settingsBlockChat = 0;
    private int planeWidth = 0;
    private int gameHeight = 334;
    private int spriteProjectile;
    private int regionY;
    private int lastOjectAnimationNumberClaw = -1;
    private Character[] knownPlayers = new Character[500];
    private int cameraRotation = 128;
    private int gameWidth = 512;
    private int mouseButtonClick = 0;
    private int settingsBlockTrade = 0;
    private int mouseClickXStep = 0;
    private int lastObjectAnimationNumberFireLightningSpell = -1;
    private int[] anIntArray1318 = new int[8192];
    private int spriteItem;
    private int anInt1321 = 2;
    private int spriteCrowns;
    private int lastObjectAnimationNumberTorch = -1;
    private int anInt1324 = 0;
    private boolean fogOfWar = false;
    private int cameraRotationX = 0;
    private int spriteUtil;
    private int knownPlayerCount = 0;
    private int[] anIntArray1330 = new int[8000];
    private int planeMultiplier = 0;
    private long[] friendListUnknown = new long[100];
    private int lastHeightOffset = -1;
    private int playerCount = 0;
    private int[] anIntArray1336 = new int[8000];
    private int objectAnimationCount = 0;
    private int planeHeight = 0;
    private World world;
    private int[] anIntArray1343 = new int[8192];
    private int cameraRotationY = 0;
    private int friendListUnknown2 = 0;
    private boolean errorLoadingData = false;
    private int anInt1348;
    private SurfaceSprite surface;
    private int magicLoc = 128;
    private int anInt1353 = 2;
    private int cameraAutoRotatePlayerY = 0;
    private int spriteLogo;
    private Character[] playerServer = new Character[4000];
    private int modlevel2 = 0;
    private int spriteCount = 0;
    private int anInt1360 = 0;
    private Character localPlayer = new Character();
    private int loggedIn = 0;
    private int[] shopItemCount = new int[256];
    private boolean optionSoundDisabled = false;
    private int anInt1365 = 0;
    private int localPlayerServerIndex = -1;
    private int anInt1367 = 0;
    private int shopSellPriceMod = 0;
    private boolean inputPopupSubmit = false;
    private int anInt1370;
    private int[] wallObjectId = new int[500];
    private String username = "";
    private int welcomeLastLoggedInDays = 0;
    private Panel panelPlayerinfo;
    private boolean showRightClickMenu = false;
    private boolean serverMessageBoxTop = false;
    private int welcomeRecoverySetDays = 0;
    private BufferBase_Sub2_Sub1 aClass8_Sub2_Sub1_1378;
    private int anInt1379 = 0;
    private int anInt1380;
    private int anInt1381;
    private boolean showDialogBank = false;
    private int anInt1383;
    private int anInt1384 = 0;
    private String tradeRecipientConfirmName;
    private String[] inputPopupText = null;
    private int shopBuyPriceMod = 0;
    private Character[] npcsServer = new Character[5000];
    private int[] objectX = new int[1500];
    private int anInt1390;
    private int[] shopItemPrice = new int[256];
    private int inventoryItemsCount;
    private int anInt1393;
    private int[] shopItem = new int[256];
    private int controlTextListQuest;
    private int duelItemsCount = 0;
    private int messageTabSelected = 0;
    private int[] duelItems = new int[8];
    private int selectedItemInventoryIndex = -1;
    private int[] inventoryItemEquipped = new int[35];
    private int shopPriceMultiplier = 0;
    private int controlButtonAppearanceHead1;
    private int anInt1403;
    private boolean tradeRecipientAccepted = false;
    private int[] anIntArray1405 = new int[]{0, 1, 2, 1};
    private int bankActivePage = 0;
    private int anInt1407 = 2;
    private int anInt1408;
    private int statFatigue = 0;
    private int[] objectDirection = new int[1500];
    private Panel panelMagic;
    private int playerMessagesCount = 0;
    private int uiTabPlayerInfoSubTab = 0;
    private boolean[] questComplete = new boolean[50];
    private int[] groundItemX = new int[5000];
    private boolean sleepWordDelay = true;
    private int anInt1417;
    private Character[] npcs = new Character[500];
    private int systemUpdate = 0;
    private boolean showAppearanceChange = false;
    private int controlListSocialPlayers;
    private int[] objectId = new int[1500];
    private int anInt1423 = 0;
    private int anInt1424;
    private int controlLoginStatus1;
    private String[] aStringArray1426 = new String[]{"Black knight\'s fortress", "Cook\'s assistant", "Demon slayer", "Doric\'s quest", "The restless ghost", "Goblin diplomacy", "Ernest the chicken", "Imp catcher", "Pirate\'s treasure", "Prince Ali rescue", "Romeo & Juliet", "Sheep shearer", "Shield of Arrav", "The knight\'s sword", "Vampire slayer", "Witch\'s potion", "Dragon slayer", "Witch\'s house (members)", "Lost city (members)", "Hero\'s quest (members)", "Druidic ritual (members)", "Merlin\'s crystal (members)", "Scorpion catcher (members)", "Family crest (members)", "Tribal totem (members)", "Fishing contest (members)", "Monk\'s friend (members)", "Temple of Ikov (members)", "Clock tower (members)", "The Holy Grail (members)", "Fight Arena (members)", "Tree Gnome Village (members)", "The Hazeel Cult (members)", "Sheep Herder (members)", "Plague City (members)", "Sea Slug (members)", "Waterfall quest (members)", "Biohazard (members)", "Jungle potion (members)", "Grand tree (members)", "Shilo village (members)", "Underground pass (members)", "Observatory quest (members)", "Tourist trap (members)", "Watchtower (members)", "Dwarf Cannon (members)", "Murder Mystery (members)", "Digsite (members)", "Gertrude\'s Cat (members)", "Legend\'s Quest (members)"};
    private int reportReason = 0;
    private int localRegionY;
    private int shopSelectedItemIndex = -1;
    private int inputPopupHeight = 0;
    private Menu menuDuel;
    private int controlPlayerinfopanel;
    private int newBankItemCount = 0;
    private boolean[] prayerOn = new boolean[50];
    private int anInt1435 = -1;
    private String duelOpponentName;
    private boolean showDialogTradeConfirm = false;
    private int groundItemCount = 0;
    private int minimapRandom_1 = 0;
    private int[] objectY = new int[1500];
    private int[] playerActionBubbleItem = new int[50];
    private boolean loadingArea = false;
    private boolean reportMutePlayer = false;
    private String password = "";
    private int[] anIntArray1445 = new int[]{0, 0, 0, 0, 0, 1, 2, 1};
    private int messageTabFlashPrivate = 0;
    private int anInt1447 = 1;
    private int[] teleportBubbleX = new int[50];
    private int mouseButtonItemCountIncrement = 0;
    private boolean showDialogDuelConfirm = false;
    private Panel panelAppearance;
    private int anInt1452 = 0;
    private boolean aBoolean1453 = false;
    private boolean duelSettingsRetreat = false;
    private int showDialogSocialInput = 0;
    private boolean tradeConfirmAccepted = false;
    private int messageTabFlashQuest = 0;
    private int[] wallObjectY = new int[500];
    private boolean duelSettingsWeapons = false;
    private int anInt1460;
    private int anInt1461 = 0;
    private int tradeRecipientConfirmItemsCount = 0;
    private int tradeItemsCount = 0;
    private String sleepingStatusText = null;
    private int[] anIntArray1465 = new int[]{16760880, 16752704, 8409136, 6307872, 0x303030, 16736288, 16728064, 0xffffff, '\uff00', '\uffff'};
    private int controlLoginStatus2;
    private int messageTabFlashHistory = 0;
    private int duelOptionPrayer;
    private boolean showDialogDuel = false;
    private Panel panelLoginWelcome;
    private boolean welcomeScreenShown = false;
    private String[] skillNameLong = new String[]{"Attack", "Defense", "Strength", "Hits", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblaw", "Agility", "Thieving"};
    private int anInt1473;
    private boolean duelSettingsPrayer = false;
    private boolean optionCameraModeAuto = true;
    private boolean inputPopupShowInput = true;
    private Panel panelSocial;
    private int[] traceRecipientConfirmItemCount = new int[14];
    private int controlLoginPass;
    private int[] duelOpponentItemCount = new int[8];
    private int[] playerHealthBarMissing = new int[50];
    private int minimapRandom_2 = 0;
    private int[] playerActionBubbleX = new int[50];
    private int[] playerMessageX = new int[50];
    private int anInt1485;
    private String tradeRecipientName = "";
    private int[] playerMessageMidPoint = new int[50];
    private int anInt1488;
    private int shopSelectedItemType = -2;
    private int[] traceRecipientConfirmItems = new int[14];
    private int[] inventoryItemId = new int[35];
    private int[] newBankItemsCount = new int[256];
    private int anInt1493;
    private int duelOfferItemCount = 0;
    private String serverMessage = "";
    private int[] tradeRecipientItems = new int[14];
    private int anInt1497;
    private int[] duelOfferItemStack = new int[8];
    private int[] playerStatCurrent = new int[18];
    private Menu menuCommon;
    private int[] wallObjectDirection = new int[500];
    private int duelOptionWeapons;
    private int npcCacheCount = 0;
    private int teleportBubbleCount = 0;
    private String[] skillNameShort = new String[]{"Attack", "Defense", "Strength", "Hits", "Ranged", "Prayer", "Magic", "Cooking", "Woodcut", "Fletching", "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblaw", "Agility", "Thieving"};
    private int playerQuestPoints = 0;
    private boolean duelOfferAccepted = false;
    private int npcCount = 0;
    private int referid = 0;
    private GameModel[] gameModels = new GameModel[1000];
    private boolean isSleeping = false;
    private int[] playerMessageY = new int[50];
    private int tradeRecipientItemsCount = 0;
    private int showDialogReportAbuseStep = 0;
    private int anInt1515;
    private int bankSelectedItemSlot = -1;
    private boolean showDialogTrade = false;
    private int[] playerMessageHeight = new int[50];
    private int[] groundItemZ = new int[5000];
    private int localRegionX;
    private byte[] soundData = null;
    private boolean cameraAutoAngleDebug = false;
    private int[] tradeConfirmItemCount = new int[14];
    private int[][] anIntArrayArray1524 = new int[][]{{11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3, 4}, {11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3, 4}, {11, 3, 2, 9, 7, 1, 6, 10, 0, 5, 8, 4}, {3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5}, {3, 4, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5}, {4, 3, 2, 9, 7, 1, 6, 10, 8, 11, 0, 5}, {11, 4, 2, 9, 7, 1, 6, 10, 0, 5, 8, 3}, {11, 2, 9, 7, 1, 6, 10, 0, 5, 8, 4, 3}};
    private int[] anIntArray1525 = new int[]{0, 1, 2, 1, 0, 0, 0, 0};
    private Character[] npcsCache = new Character[500];
    private int[] teleportBubbleY = new int[50];
    private int[] tradeRecipientItemCount = new int[14];
    private int[] playerStatBase = new int[18];
    private int[] tradeItemCount = new int[14];
    private int[] groundItemY = new int[5000];
    private int welcomeUnreadMessages = 0;
    private int anInt1533 = 0;
    private int[] playerActionBubbleScale = new int[50];
    private int anInt1535 = 30;
    private int[] duelOfferItemId = new int[8];
    private int anInt1537;
    private GameModel[] wallObjectModel = new GameModel[500];
    private int[] bankItemsCount = new int[256];
    private int appearanceHeadType = 0;
    private int fatigueSleeping = 0;
    private int optionMenuCount = 0;
    private String welcomeLastLoggedInHost = null;
    private String[] equipmentStatNames = new String[]{"Armour", "WeaponAim", "WeaponPower", "Magic", "Prayer"};
    private Panel panelMessageTabs;
    private int messageTabFlashAll = 0;
    private boolean menu_visible = false;
    private int[] anIntArray1548 = new int[]{15523536, 13415270, 11766848, 10056486, 9461792};
    private int inputPopupWidth = 0;
    private int[] inventoryItemStackCount = new int[35];
    private int combatStyle = 0;
    private int duelOptionRetreat;
    private boolean[] objectAlreadyInMenu = new boolean[1500];
    private String privateMessageTarget;
    private int anInt1555 = 8;
    private int anInt1556;
    private int[] tradeItems = new int[14];
    private boolean duelSettingsMagic = false;
    private int[] duelOpponentItems = new int[8];
    private int wallObjectCount = 0;
    private int controlLoginUser;
    private int[] duelItemCount = new int[8];
    private int anInt1563;
    private int[] duelOfferOpponentItemStack = new int[8];
    private int deathScreenTimeout = 0;
    private SoundPlayer soundPlayer;
    private boolean inTutorial = false;
    private String duelConfirmOpponentName = "";
    private int[] playerHealthBarY = new int[50];
    private String selectedItemName = "";
    private int anInt1571;
    private int welcomeLastLoggedInIp = 0;
    private int anInt1573 = 14;
    private int[] bankItems = new int[256];
    private boolean showDialogShop = false;
    private boolean duelAccepted = false;
    private int anInt1577;
    private int controlTextListPrivate;
    private int mouseButtonDownTime = 0;
    private int selectedSpell = -1;
    private int[] wallObjectX = new int[500];
    private boolean showOptionMenu = false;
    private int bankSelectedItem = -2;
    private int[] teleportBubbleType = new int[50];
    private int controlMagicPanel;
    private boolean aBoolean1586 = false;
    private int showUiWildWarn = 0;
    private int[] teleportBubbleTime = new int[50];
    private int[] playerActionBubbleY = new int[50];
    private boolean[] wallObjectAlreadyInMenu = new boolean[500];
    private int duelOfferOpponentItemCount = 0;
    private int[] anIntArray1592 = new int[]{0xff0000, 0xff8000, 16769024, 10543104, '\ue000', '\u8000', '\ua080', '\ub0ff', '\u80ff', 12528, 14680288, 0x303030, 6307840, 8409088, 0xffffff};
    private int[] playerExperience = new int[18];
    private int controlTextListAll;
    private int controlTextListChat;
    private int[] playerStatEquipment = new int[5];
    private int sleepWordDelayTimer = 0;
    private int duelOpponentItemsCount = 0;
    private boolean showDialogServermessage = false;
    private boolean showDialogMessage = false;
    private int duelOptionMagic;
    private int logoutTimeout = 0;
    private int anInt1603;
    private int[] newBankItems = new int[256];
    private boolean optionMouseButtonOne = false;
    private int[] groundItemId = new int[5000];
    private GameModel[] objectModel = new GameModel[1500];
    private int anInt1608;
    private int[] duelOpponentItemId = new int[8];
    private int[] playerHealthBarX = new int[50];
    private int anInt1611 = 2;
    private int[] experienceArray = new int[99];
    private String[] optionMenuEntry = new String[5];
    private int appearanceHeadGender = 1;
    private Menu menuTrade;
    private int bankItemCount = 0;
    private int anInt1617 = 0;
    private int objectCount = 0;
    private String[] playerMessages = new String[50];
    private int inputPopupType = 0;
    private int combatTimeout = 0;
    private int[] tradeConfirmItems = new int[14];
    private int anInt1623;
    private boolean duelOfferOpponentAccepted = false;
    private int anInt1625 = 0;
    private int anInt1626 = -1;
    private int showUiTab = 0;
    private int tradeConfirmItemsCount = 0;
    private int bankItemsMax = 48;
    private boolean tradeAccepted = false;
    private Panel paneLogin;
    private String reportName = "";

    static byte[] method429(int var0, int var2, byte[] var3) {
        byte[] var4 = new byte[var2];

        for (int var5 = 0; var2 > var5; ++var5) {
            var4[var5] = aByteArray65[Utility.bitwiseAnd(var3[var5 + var0], 255)];
        }

        return var4;
    }

    static byte[] method155(int var0) {
        if (var0 != 128) {
            return null;
        } else {
            byte[] var1 = new byte[256];

            for (int var2 = -128; var2 < 127; ++var2) {
                byte var3 = (byte) var2;
                int var9 = ~var3;
                int var4 = 128 & var9;
                int var5 = (123 & var9) >> 4;
                int var6 = var9 & 15;
                var6 |= 16;
                var6 = (var6 << 1) + 1;
                int var7 = var6 << 2 + var5;
                var7 += -132;
                if (var4 != 0) {
                    var7 = -var7;
                }

                var1[Utility.bitwiseAnd(255, var2)] = (byte) (var7 / 256);
            }

            return var1;
        }
    }

    static int method161(byte var0, BufferBase_Sub3 var1, String var2) {
        int var3 = var1.offset;
        byte[] var4 = Utility.stringToUnicode(var2);
        var1.addShort2(var4.length);
        int var5 = 39 / ((84 - var0) / 40);
        var1.offset += aClass11_1110.method241(0, var4.length, var1.buffer, var4, 18695, var1.offset);
        return var1.offset - var3;
    }

    static int calculateShopItemPrice(int basePrice, int itemCount, boolean buying, int priceModifier, int loopCycles, int itemPrice, int priceMultiplier) {
        int price = 0;

        for (int var9 = 0; loopCycles > var9; ++var9) {
            int var10 = priceMultiplier * (-itemCount + (buying ? var9 : -var9) + itemPrice);
            if (var10 < -100) {
                var10 = -100;
            } else if (var10 > 100) {
                var10 = 100;
            }

            int var11 = priceModifier + var10;
            if (var11 < 10) {
                var11 = 10;
            }

            price += basePrice * var11 / 100;
        }

        return price;
    }

    static void writeCache(int offset, BufferBase_Sub3 var2) {
        if (Isaac.cacheRandomDat != null) {
            try {
                Isaac.cacheRandomDat.seek(0L, (byte) -118);
                Isaac.cacheRandomDat.write(var2.buffer, offset, 24);
            } catch (Exception var4) {
            }
        }
    }

    static void putRandom(int var0, BufferBase_Sub3 data) {
        byte[] randomdat = new byte[24];
        if (Isaac.cacheRandomDat != null) {
            int var3;
            try {
                Isaac.cacheRandomDat.seek(0L, (byte) -118);
                Isaac.cacheRandomDat.write(randomdat, var0 + 302);

                for (var3 = 0; var3 < 24 && randomdat[var3] == 0; ++var3) {
                    ;
                }

                if (var3 >= 24) {
                    throw new IOException();
                }
            } catch (Exception var4) {
                for (var3 = 0; var3 < 24; ++var3) {
                    randomdat[var3] = -1;
                }
            }
        }

        data.addBytes(24, randomdat, 0);
        if (var0 != -257) {
            GameModeWhere.LOCAL = null;
        }
    }

    static void method339(byte var0, Class11 var1) {
        if (var0 != -96) {
            putRandom(58, (BufferBase_Sub3) null);
        }

        aClass11_1110 = var1;
    }

    static SoundPlayer method437(Component var0, int var2, CachePackageManager var3, int var4) {
        if (SoundPlayer.sampleRate == 0) {
            throw new IllegalStateException();
        } else if (var4 >= 0 && var4 < 2) {
            if (var2 < 256) {
                var2 = 256;
            }

            try {
                SoundPlayer var10 = (SoundPlayer) Class.forName("SoundPlayerJava").newInstance();
                var10.anIntArray767 = new int[(!SoundPlayer.stereo ? 1 : 2) * 256];
                var10.anInt796 = var2;
                var10.init(var0);
                var10.anInt789 = (-1024 & var2) + 1024;
                if (var10.anInt789 > 16384) {
                    var10.anInt789 = 16384;
                }

                var10.start(var10.anInt789);
                if (SoundPlayer.channels > 0 && SoundPlayer.aSoundWhat_490 == null) {
                    SoundPlayer.aSoundWhat_490 = new SoundWhat();
                    SoundPlayer.aSoundWhat_490.cpm = var3;
                    var3.method585(SoundPlayer.channels, (byte) -128, SoundPlayer.aSoundWhat_490);
                }

                if (SoundPlayer.aSoundWhat_490 != null) {
                    if (SoundPlayer.aSoundWhat_490.soundPlayers[var4] != null) {
                        throw new IllegalArgumentException();
                    }

                    SoundPlayer.aSoundWhat_490.soundPlayers[var4] = var10;
                }

                return var10;
            } catch (Throwable var8) {
                try {
                    SoundPlayerDirectX var5 = new SoundPlayerDirectX(var3, var4);
                    var5.anInt796 = var2;
                    var5.anIntArray767 = new int[256 * (!SoundPlayer.stereo ? 1 : 2)];
                    var5.init(var0);
                    var5.anInt789 = 16384;
                    var5.start(var5.anInt789);
                    if (SoundPlayer.channels > 0 && SoundPlayer.aSoundWhat_490 == null) {
                        SoundPlayer.aSoundWhat_490 = new SoundWhat();
                        SoundPlayer.aSoundWhat_490.cpm = var3;
                        var3.method585(SoundPlayer.channels, (byte) -128, SoundPlayer.aSoundWhat_490);
                    }

                    if (SoundPlayer.aSoundWhat_490 != null) {
                        if (SoundPlayer.aSoundWhat_490.soundPlayers[var4] != null) {
                            throw new IllegalArgumentException();
                        }

                        SoundPlayer.aSoundWhat_490.soundPlayers[var4] = var5;
                    }

                    return var5;
                } catch (Throwable var7) {
                    return new SoundPlayer();
                }
            }
        } else {
            throw new IllegalArgumentException();
        }
    }

    static String formatMessage(String message, String sender, int type) {
        if (type == 5 || sender == null || sender.length() == 0) {
            return message;
        }
        switch (type) {
            case 0:
            case 3:
            case 4:
            case 7:
                return sender + ": " + message;
            case 1:
                return sender + " tells you: " + message;
            case 2:
                return "You tell " + sender + ": " + message;
            case 6:
                return sender + " wishes to trade with you.";
            default:
                return "";
        }
    }

    public static void main(String[] args) {
        if (args == null || args.length < 3) {
            args = new String[]{"5002", "live", "members"};
        }

        try {
            modewhere = GameModeWhere.LOCAL;
            nodeid = Integer.parseInt(args[0]);
            if (args[1].equals("live")) {
                modewhat = GameModeWhat.LIVE;
            } else if (!args[1].equals("rc")) {
                if (args[1].equals("wip")) {
                    modewhat = GameModeWhat.WIP;
                }
            } else {
                modewhat = GameModeWhat.RC;
            }

            client cl = new client();
            cl.appletMode = false;

            for (int var2 = 2; args.length > var2; ++var2) {
                if (args[var2].equals("members")) {
                    cl.members = true;
                }

                if (args[var2].equals("veterans")) {
                    cl.veterans = true;
                }
            }

            cl.startApplication(12 + cl.gameHeight, "RuneScape Classic", cl.gameWidth, false, nodeid + 7000, modewhat.id + 32, 0, "classic", ClientStream.version);
            cl.threadSleep = 10;
        } catch (Exception var3) {
            Utility.sendClientError((String) null, -257, var3);
        }
    }

    private void resetGame(int var1) {
        this.loggedIn = 1;
        this.combatStyle = 0;
        this.logoutTimeout = 0;
        this.anInt1617 = 0;
        this.systemUpdate = 0;
        this.method115((byte) 127);
        this.surface.blackScreen((byte) 127);
        this.surface.draw(-2020315800, this.graphics, super.anInt58, super.anInt7);

        for (int var2 = var1; this.objectCount > var2; ++var2) {
            this.scene.freeModel((byte) 124, this.objectModel[var2]);
            this.world.removeObject(this.objectY[var2], -2106, this.objectId[var2], this.objectX[var2]);
        }

        for (int var3 = 0; this.wallObjectCount > var3; ++var3) {
            this.scene.freeModel((byte) 105, this.wallObjectModel[var3]);
            this.world.removeWallObject(var1 ^ -125, this.wallObjectY[var3], this.wallObjectX[var3], this.wallObjectDirection[var3], this.wallObjectId[var3]);
        }

        this.playerCount = 0;
        this.wallObjectCount = 0;
        this.objectCount = 0;
        this.groundItemCount = 0;

        for (int var4 = 0; var4 < 4000; ++var4) {
            this.playerServer[var4] = null;
        }

        for (int var5 = 0; var5 < 500; ++var5) {
            this.players[var5] = null;
        }

        this.npcCount = 0;

        for (int var6 = 0; var6 < 5000; ++var6) {
            this.npcsServer[var6] = null;
        }

        for (int var7 = 0; var7 < 500; ++var7) {
            this.npcs[var7] = null;
        }

        for (int var8 = 0; var8 < 50; ++var8) {
            this.prayerOn[var8] = false;
        }

        friendListCount = 0;
        this.showDialogReportAbuseStep = 0;
        super.lastMouseButtonDown = 0;
        super.anInt84 = 0;
        this.isSleeping = false;
        this.showDialogShop = false;
        this.showDialogBank = false;
        this.mouseButtonClick = 0;

        for (int var9 = 0; var9 < 100; ++var9) {
            messageMessages[var9] = null;
            messageHistoryTimeout[var9] = 0;
            messageSenders[var9] = null;
            messageCrowns[var9] = 0;
            messageSenderClans[var9] = null;
            messageColor[var9] = null;
            messageTypes[var9] = 0;
        }

        this.panelMessageTabs.clearList(this.controlTextListChat);
        this.panelMessageTabs.clearList(this.controlTextListQuest);
        this.panelMessageTabs.clearList(this.controlTextListPrivate);
    }

    private void autorotateCamera(byte var1) {
        if ((this.cameraAngle & 1) != 1 || !this.method138(this.cameraAngle, 77)) {
            if ((this.cameraAngle & 1) == 0 && this.method138(this.cameraAngle, 89)) {
                if (this.method138(7 & 1 + this.cameraAngle, 91)) {
                    this.cameraAngle = 1 + this.cameraAngle & 7;
                } else {
                    if (this.method138(7 & this.cameraAngle + 7, 115)) {
                        this.cameraAngle = 7 & 7 + this.cameraAngle;
                    }

                }
            } else {
                int[] var2 = new int[]{1, -1, 2, -2, 3, -3, 4};
                int var3 = 0;

                for (int var4 = -95 / ((var1 - -23) / 56); var3 < 7; ++var3) {
                    if (this.method138(7 & 8 + this.cameraAngle + var2[var3], 94)) {
                        this.cameraAngle = 8 + this.cameraAngle + var2[var3] & 7;
                        break;
                    }
                }

                if ((this.cameraAngle & 1) == 0 && this.method138(this.cameraAngle, 41)) {
                    if (!this.method138(this.cameraAngle - -1 & 7, 51)) {
                        if (this.method138(7 + this.cameraAngle & 7, 31)) {
                            this.cameraAngle = 7 + this.cameraAngle & 7;
                            return;
                        }
                    } else {
                        this.cameraAngle = 7 & this.cameraAngle + 1;
                    }

                }
            }
        }
    }

    final void draw(int var1) {
        if (super.aBoolean6) {
            this.method42(25);
            super.aBoolean6 = false;
        }

        Graphics g;
        if (this.errorLoadingData) {
            g = this.getGraphics();
            if (g != null) {
                g.translate(super.anInt58, super.anInt7);
                g.setColor(Color.black);
                g.fillRect(0, 0, 512, 356);
                g.setFont(new Font("Helvetica", 1, 16));
                g.setColor(Color.yellow);
                byte var10 = 35;
                g.drawString("Sorry, an error has occured whilst loading RuneScape", 30, var10);
                g.setColor(Color.white);
                int var11 = var10 + 50;
                g.drawString("To fix this try the following (in order):", 30, var11);
                var11 += 50;
                g.setColor(Color.white);
                g.setFont(new Font("Helvetica", 1, 12));
                g.drawString("1: Try closing ALL open web-browser windows, and reloading", 30, var11);
                var11 += 30;
                g.drawString("2: Try clearing your web-browsers cache from tools->internet options", 30, var11);
                var11 += 30;
                g.drawString("3: Try using a different game-world", 30, var11);
                var11 += 30;
                g.drawString("4: Try rebooting your computer", 30, var11);
                var11 += 30;
                g.drawString("5: Try selecting a different version of Java from the play-game menu", 30, var11);
                this.setFPS((byte) 126, 1);
            }
        } else if (this.errorLoadingCodebase) {
            g = this.getGraphics();
            if (g != null) {
                g.translate(super.anInt58, super.anInt7);
                g.setColor(Color.black);
                g.fillRect(0, 0, 512, 356);
                g.setFont(new Font("Helvetica", 1, 20));
                g.setColor(Color.white);
                g.drawString("Error - unable to load game!", 50, 50);
                g.drawString("To play RuneScape make sure you play from", 50, 100);
                g.drawString("http://www.runescape.com", 50, 150);
                this.setFPS((byte) 126, 1);
            }
        } else if (this.errorLoadingMemory) {
            g = this.getGraphics();
            if (g != null) {
                g.translate(super.anInt58, super.anInt7);
                g.setColor(Color.black);
                g.fillRect(0, 0, 512, 356);
                g.setFont(new Font("Helvetica", 1, 20));
                g.setColor(Color.white);
                g.drawString("Error - out of memory!", 50, 50);
                g.drawString("Close ALL unnecessary programs", 50, 100);
                g.drawString("and windows before loading the game", 50, 150);
                g.drawString("RuneScape needs about 48meg of spare RAM", 50, 200);
                this.setFPS((byte) 126, 1);
            }
        } else {
            try {
                if (var1 != 16586) {
                    this.playerMessageMidPoint = null;
                }

                if (this.surface != null) {
                    if (this.loggedIn == 0) {
                        this.surface.loggedIn = false;
                        this.method48(64);
                    }

                    if (this.loggedIn == 1) {
                        this.surface.loggedIn = true;
                        this.drawGame(17918);
                    }

                    if (GameModeWhere.LIVE != modewhere) {
                        try {
                            g = this.getGraphics();
                            if (g != null) {
                                Runtime var3 = Runtime.getRuntime();
                                int var4 = (int) (var3.totalMemory() / 1024L);
                                int var5 = (int) ((var3.totalMemory() - var3.freeMemory()) / 1024L);
                                int var6 = this.getSize().height;
                                g.setColor(Color.black);
                                g.fillRect(0, var6 - 50, 400, 20);
                                g.setColor(Color.yellow);
                                g.drawString("Used:" + var5 + "k Tot:" + var4 + "k", 10, var6 + -35);
                            }
                        } catch (NullPointerException var7) {
                            ;
                        }
                    }
                }
            } catch (OutOfMemoryError var8) {
                this.errorLoadingMemory = true;
            }
        }
        ;
    }

    private void loadEntities(int var1) {
        Object var2 = null;
        byte[] var14 = this.readDataFile(1, 30, "people and monsters", -10);
        Object var3 = null;
        if (var14 == null) {
            this.errorLoadingData = true;
        } else {
            byte[] indexDat = Utility.loadData(var14, 0, "index.dat");
            byte[] var4 = null;
            byte[] var5 = null;
            if (this.members) {
                var4 = this.readDataFile(2, 45, "member graphics", -10);
                if (var4 == null) {
                    this.errorLoadingData = true;
                    return;
                }

                var5 = Utility.loadData(var4, 0, "index.dat");
            }

            if (var1 != -25763) {
                this.objectAnimationCount = 124;
            }

            int frameCount = 0;
            this.anInt1533 = 0;
            this.anInt1452 = this.anInt1533;
            int var7 = 0;

            while (GameData.animationCount > var7) {
                String animName = GameData.animationName[var7];
                int var9 = 0;

                while (true) {
                    if (var7 <= var9) {
                        byte[] dat = Utility.loadData(var14, 0, animName + ".dat");
                        byte[] indexData = indexDat;
                        if (dat == null && this.members) {
                            dat = Utility.loadData(var4, 0, animName + ".dat");
                            indexData = var5;
                        }

                        if (dat != null) {
                            frameCount += 15;
                            this.surface.parseSprite(indexData, this.anInt1452, (byte) 108, dat, 15);
                            if (GameData.animationHasA[var7] == 1) {
                                byte[] datA = Utility.loadData(var14, 0, animName + "a.dat");
                                indexData = indexDat;
                                if (datA == null && this.members) {
                                    datA = Utility.loadData(var4, 0, animName + "a.dat");
                                    indexData = var5;
                                }

                                frameCount += 3;
                                this.surface.parseSprite(indexData, this.anInt1452 - -15, (byte) 82, datA, 3);
                            }

                            if (GameData.animationHasF[var7] == 1) {
                                indexData = indexDat;
                                byte[] datF = Utility.loadData(var14, 0, animName + "f.dat");
                                if (datF == null && this.members) {
                                    indexData = var5;
                                    datF = Utility.loadData(var4, 0, animName + "f.dat");
                                }

                                this.surface.parseSprite(indexData, this.anInt1452 - -18, (byte) 77, datF, 9);
                                frameCount += 9;
                            }

                            if (GameData.animationSomething[var7] != 0) {
                                for (int var16 = this.anInt1452; (27 + this.anInt1452) > var16; ++var16) {
                                    this.surface.loadSprite(1, var16);
                                }
                            }
                        }

                        GameData.animationNumber[var7] = this.anInt1452;
                        this.anInt1452 += 27;
                    } else {
                        if (!GameData.animationName[var9].equalsIgnoreCase(animName)) {
                            ++var9;
                            continue;
                        }

                        GameData.animationNumber[var7] = GameData.animationNumber[var9];
                    }

                    ++var7;
                    break;
                }
            }

            System.out.println("Loaded: " + frameCount + " frames of animation");
        }
        ;
    }

    private void showInputPopup(int type, String[] text, boolean showInput) {
        this.showInputPopup(type, text, 0, "", showInput);
    }

    private void showMessage(int messageType, String message, String sender, String senderClan, int crownId, String colorOverride, boolean forceShow) {
        if ((messageType == 1 || messageType == 4 || messageType == 6) && senderClan != null && !forceShow) {
            String var9 = Utility.formatName(senderClan);
            if (var9 == null) {
                return;
            }

            for (int i = 0; ignoreListCount > i; ++i) {
                if (var9.equals(Utility.formatName(ignoreListAccNames[i]))) {
                    return;
                }
            }
        }

        String color = messageColors[messageType];
        if (colorOverride != null) {
            color = colorOverride;
        }

        if (this.messageTabSelected != 0) {
            if (messageType == 0 || messageType == 7) {
                this.messageTabFlashAll = 200;
            }

            if ((messageType == 5 || messageType == 1 || messageType == 2) && this.messageTabSelected != 3) {
                this.messageTabFlashPrivate = 200;
            }

            if (messageType == 3 && this.messageTabSelected != 2) {
                this.messageTabFlashQuest = 200;
            }

            if (messageType == 4 && this.messageTabSelected != 1) {
                this.messageTabFlashHistory = 200;
            }

            if (messageType == 0 && this.messageTabSelected != 0) {
                this.messageTabSelected = 0;
            }

            if ((messageType == 5 || messageType == 1 || messageType == 2) && this.messageTabSelected != 3 && this.messageTabSelected != 0) {
                this.messageTabSelected = 0;
            }
        }

        for (int i = 99; i > 0; --i) {
            messageTypes[i] = messageTypes[-1 + i];
            messageHistoryTimeout[i] = messageHistoryTimeout[i + -1];
            messageCrowns[i] = messageCrowns[i - 1];
            messageSenders[i] = messageSenders[i - 1];
            messageSenderClans[i] = messageSenderClans[-1 + i];
            messageMessages[i] = messageMessages[-1 + i];
            messageColor[i] = messageColor[-1 + i];
        }

        messageTypes[0] = messageType;
        messageHistoryTimeout[0] = 300;
        messageSenders[0] = sender;
        messageCrowns[0] = crownId;
        messageSenderClans[0] = senderClan;
        messageMessages[0] = message;
        messageColor[0] = color;

        String fullMessage = color + formatMessage(message, sender, messageType);
        if (messageType == 4) {
            if ((-4 + this.panelMessageTabs.controlListEntryCount[this.controlTextListChat]) != this.panelMessageTabs.controlFlashText[this.controlTextListChat]) {
                this.panelMessageTabs.removeListEntry(this.controlTextListChat, fullMessage, false, senderClan, sender, crownId);
            } else {
                this.panelMessageTabs.removeListEntry(this.controlTextListChat, fullMessage, true, senderClan, sender, crownId);
            }
        }

        if (messageType == 3) {
            if (this.panelMessageTabs.controlFlashText[this.controlTextListQuest] != (-4 + this.panelMessageTabs.controlListEntryCount[this.controlTextListQuest])) {
                this.panelMessageTabs.removeListEntry(this.controlTextListQuest, fullMessage, false, (String) null, (String) null, 0);
            } else {
                this.panelMessageTabs.removeListEntry(this.controlTextListQuest, fullMessage, true, (String) null, (String) null, 0);
            }
        }

        if (messageType == 1 || messageType == 2) {
            if (messageType != 1) {
                crownId = 0;
            }

            if (this.panelMessageTabs.controlFlashText[this.controlTextListPrivate] != (-4 + this.panelMessageTabs.controlListEntryCount[this.controlTextListPrivate])) {
                this.panelMessageTabs.removeListEntry(this.controlTextListPrivate, fullMessage, false, senderClan, sender, crownId);
                return;
            }

            this.panelMessageTabs.removeListEntry(this.controlTextListPrivate, fullMessage, true, senderClan, sender, crownId);
        }
        ;
    }

    private void sendPrivacySettings(int var1, int var2, int var3, int var4, int var5) {
        if (var1 != -18896) {
            this.sendPrivateMessage((String) null, (String) null, (byte) 80);
        }

        this.clientStream.newPacket(64);
        this.clientStream.writeBuffer.putByte(var3);
        this.clientStream.writeBuffer.putByte(var2);
        this.clientStream.writeBuffer.putByte(var4);
        this.clientStream.writeBuffer.putByte(var5);
        this.clientStream.sendPacket();
    }

    private void drawDialogReportAbuse(byte var1) {
        this.reportReason = 0;
        boolean var2 = true;
        if (super.mouseX >= 36 && super.mouseX < 176) {
            this.reportReason = 1;
        } else if (super.mouseX >= 186 && super.mouseX < 326) {
            this.reportReason = 7;
        } else if (super.mouseX >= 336 && super.mouseX < 476) {
            this.reportReason = 12;
        } else {
            var2 = false;
        }

        int y = 156;
        int var4;
        if (var2) {
            var2 = false;

            for (var4 = 0; var4 < 6; ++var4) {
                int var5 = var4 == 0 ? 30 : 18;
                if (super.mouseY > -12 + y && super.mouseY < -12 + y - -var5) {
                    if (this.reportReason == 1) {
                        var2 = true;
                        this.reportReason += var4;
                        break;
                    }

                    if (this.reportReason == 7) {
                        if (var4 < 5) {
                            var2 = true;
                            this.reportReason += var4;
                        }
                        break;
                    }

                    if (this.reportReason == 12) {
                        if (var4 < 3) {
                            this.reportReason += var4;
                            var2 = true;
                        }
                        break;
                    }
                }

                y += 2 + var5;
            }
        }

        if (!var2) {
            this.reportReason = 0;
        }

        if (this.mouseButtonClick != 0 && this.reportReason != 0) {
            this.clientStream.newPacket(206);
            this.clientStream.writeBuffer.pjstr2(this.reportName);
            this.clientStream.writeBuffer.putByte(this.reportReason);
            this.clientStream.writeBuffer.putByte(!this.reportMutePlayer ? 0 : 1);
            this.clientStream.sendPacket();
            super.inputTextCurrent = "";
            super.inputTextFinal = "";
            this.mouseButtonClick = 0;
            this.showDialogReportAbuseStep = 0;
        } else {
            y += 15;
            if (this.mouseButtonClick != 0) {
                this.mouseButtonClick = 0;
                if (super.mouseX < 31 || super.mouseY < 35 || super.mouseX > 481 || super.mouseY > 310) {
                    this.showDialogReportAbuseStep = 0;
                    return;
                }

                if (super.mouseX > 66 && super.mouseX < 446 && super.mouseY >= (-15 + y) && 5 + y > super.mouseY) {
                    this.showDialogReportAbuseStep = 0;
                    return;
                }
            }

            this.surface.drawBox(31, 35, 450, 275, 0);
            byte var7 = 50;
            this.surface.drawBoxEdge(31, 35, 450, 275, 0xffffff);
            this.surface.drawstringCenter("This form is for reporting players who are breaking our rules", 1, var7, 256, 0xffffff);
            y = var7 + 15;
            this.surface.drawstringCenter("Using it sends a snapshot of the last 60 seconds of activity to us", 1, y, 256, 0xffffff);
            y += 15;
            this.surface.drawstringCenter("If you misuse this form, you will be banned.", 1, y, 256, 0xff8000);
            y += 15;
            y += 10;
            this.surface.drawstringCenter("Click on the most suitable option from the Rules of RuneScape.", 1, y, 256, 0xffff00);
            y += 15;
            this.surface.drawstringCenter("This will send a report to our Player Support team for investigation.", 1, y, 256, 0xffff00);
            y += 18;
            this.surface.drawstringCenter("Honour", 4, y, 106, 0xff0000);
            this.surface.drawstringCenter("Respect", 4, y, 256, 0xff0000);
            this.surface.drawstringCenter("Security", 4, y, 406, 0xff0000);
            y += 18;
            if (this.reportReason == 1) {
                this.surface.drawBox(36, y - 12, 140, 30, 0x303030);
            }

            this.surface.drawBoxEdge(36, y - 12, 140, 30, 0x404040);
            if (this.reportReason == 7) {
                this.surface.drawBox(186, y + -12, 140, 30, 0x303030);
            }

            this.surface.drawBoxEdge(186, -12 + y, 140, 30, 0x404040);
            if (this.reportReason == 12) {
                this.surface.drawBox(336, y - 12, 140, 30, 0x303030);
            }

            this.surface.drawBoxEdge(336, -12 + y, 140, 30, 0x404040);
            if (this.reportReason != 1) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("Buying or", 0, y, 106, var4);
            if (this.reportReason == 7) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("Seriously offensive", 0, y, 256, var4);
            if (this.reportReason != 12) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("Asking for or providing", 0, y, 406, var4);
            if (this.reportReason != 1) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            y += 12;
            this.surface.drawstringCenter("selling an account", 0, y, 106, var4);
            if (var1 != 58) {
                this.anInt1298 = 3;
            }

            if (this.reportReason == 7) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("language", 0, y, 256, var4);
            if (this.reportReason != 12) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("contact information", 0, y, 406, var4);
            y += 20;
            if (this.reportReason == 2) {
                this.surface.drawBox(36, -12 + y, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(36, y - 12, 140, 18, 0x404040);
            if (this.reportReason == 8) {
                this.surface.drawBox(186, y + -12, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(186, -12 + y, 140, 18, 0x404040);
            if (this.reportReason == 13) {
                this.surface.drawBox(336, -12 + y, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(336, -12 + y, 140, 18, 0x404040);
            if (this.reportReason == 2) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("Encouraging rule-breaking", 0, y, 106, var4);
            if (this.reportReason != 8) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("Solicitation", 0, y, 256, var4);
            if (this.reportReason != 13) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("Breaking real-world laws", 0, y, 406, var4);
            y += 20;
            if (this.reportReason == 3) {
                this.surface.drawBox(36, -12 + y, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(36, y + -12, 140, 18, 0x404040);
            if (this.reportReason == 9) {
                this.surface.drawBox(186, y - 12, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(186, y + -12, 140, 18, 0x404040);
            if (this.reportReason == 14) {
                this.surface.drawBox(336, -12 + y, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(336, -12 + y, 140, 18, 0x404040);
            if (this.reportReason != 3) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("Staff impersonation", 0, y, 106, var4);
            if (this.reportReason == 9) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("Disruptive behaviour", 0, y, 256, var4);
            if (this.reportReason == 14) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("Advertising websites", 0, y, 406, var4);
            y += 20;
            if (this.reportReason == 4) {
                this.surface.drawBox(36, y - 12, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(36, y + -12, 140, 18, 0x404040);
            if (this.reportReason == 10) {
                this.surface.drawBox(186, -12 + y, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(186, y + -12, 140, 18, 0x404040);
            if (this.reportReason == 4) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("Macroing or use of bots", 0, y, 106, var4);
            if (this.reportReason != 10) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("Offensive account name", 0, y, 256, var4);
            y += 20;
            if (this.reportReason == 5) {
                this.surface.drawBox(36, -12 + y, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(36, -12 + y, 140, 18, 0x404040);
            if (this.reportReason == 11) {
                this.surface.drawBox(186, y - 12, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(186, -12 + y, 140, 18, 0x404040);
            if (this.reportReason != 5) {
                var4 = 0xffffff;
            } else {
                var4 = 0xff8000;
            }

            this.surface.drawstringCenter("Scamming", 0, y, 106, var4);
            if (this.reportReason == 11) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("Real-life threats", 0, y, 256, var4);
            y += 20;
            if (this.reportReason == 6) {
                this.surface.drawBox(36, -12 + y, 140, 18, 0x303030);
            }

            this.surface.drawBoxEdge(36, y + -12, 140, 18, 0x404040);
            if (this.reportReason == 6) {
                var4 = 0xff8000;
            } else {
                var4 = 0xffffff;
            }

            this.surface.drawstringCenter("Exploiting a bug", 0, y, 106, var4);
            y += 18;
            var4 = 0xffffff;
            y += 15;
            if (super.mouseX > 196 && super.mouseX < 316 && super.mouseY > y + -15 && y - -5 > super.mouseY) {
                var4 = 0xffff00;
            }

            this.surface.drawstringCenter("Click here to cancel", 1, y, 256, var4);
        }
        ;
    }

    final void handleInputs(int var1) {
        if (var1 <= -30) {
            if (!this.errorLoadingCodebase) {
                if (!this.errorLoadingMemory) {
                    if (!this.errorLoadingData) {
                        if (this.soundPlayer != null) {
                            this.soundPlayer.method494(24);
                        }

                        try {
                            ++this.anInt1360;
                            if (this.loggedIn == 0) {
                                super.lastMouseAction = 0;
                                this.handleLoginScreenInput(32767);
                            }

                            if (this.loggedIn == 1) {
                                ++super.lastMouseAction;
                                this.handleGameInput(-18771);
                            }

                            super.lastMouseButtonDown = 0;
                            ++this.anInt1324;
                            if (this.anInt1324 > 500) {
                                this.anInt1324 = 0;
                                int var2 = (int) (4.0D * Math.random());
                                if ((2 & var2) == 2) {
                                    this.cameraRotationX += this.anInt1353;
                                }

                                if ((1 & var2) == 1) {
                                    this.cameraRotationY += this.anInt1321;
                                }
                            }

                            if (this.cameraRotationY < -50) {
                                this.anInt1321 = 2;
                            }

                            if (this.cameraRotationY > 50) {
                                this.anInt1321 = -2;
                            }

                            if (this.cameraRotationX < -50) {
                                this.anInt1353 = 2;
                            }

                            if (this.messageTabFlashPrivate > 0) {
                                --this.messageTabFlashPrivate;
                            }

                            if (this.messageTabFlashHistory > 0) {
                                --this.messageTabFlashHistory;
                            }

                            if (this.cameraRotationX > 50) {
                                this.anInt1353 = -2;
                            }

                            if (this.messageTabFlashAll > 0) {
                                --this.messageTabFlashAll;
                            }

                            if (this.messageTabFlashQuest > 0) {
                                --this.messageTabFlashQuest;
                            }
                        } catch (OutOfMemoryError var3) {
                            this.errorLoadingMemory = true;
                        }
                    }
                }
            }
        }
    }

    private void method42(int var1) {
        Object var2;
        if (this.appletMode) {
            if (Isaac.anApplet445 == null) {
                var2 = this;
            } else {
                var2 = Isaac.anApplet445;
            }
        } else {
            var2 = gameFrameReference;
        }

        this.anInt1417 = ((Component) var2).getSize().width;
        this.anInt1556 = ((Component) var2).getSize().height;
        super.anInt58 = (this.anInt1417 - this.gameWidth) / 2;
        int var3 = 113 % ((-53 - var1) / 62);
        super.anInt7 = 0;
        this.method69(275);
    }

    private void handleLoginScreenInput(int var1) {
        if (this.worldFullTimeout > 0) {
            --this.worldFullTimeout;
        }

        if (this.anInt1617 == 0) {
            this.panelLoginWelcome.handleMouse(-107, super.lastMouseButtonDown, super.mouseX, super.anInt84, super.mouseY);
            if (this.panelLoginWelcome.isClicked(3, this.anInt1515)) {
                this.anInt1617 = 2;
                this.paneLogin.updateText(this.controlLoginStatus1, "", var1 ^ -32692);
                this.paneLogin.updateText(this.controlLoginStatus2, "Please enter your username and password", -111);
                this.paneLogin.updateText(this.controlLoginUser, "", -128);
                this.paneLogin.updateText(this.controlLoginPass, "", 85);
                this.paneLogin.setFocus((byte) 96, this.controlLoginUser);
            }
        } else if (this.anInt1617 == 2) {
            this.paneLogin.handleMouse(-121, super.lastMouseButtonDown, super.mouseX, super.anInt84, super.mouseY);
            if (this.paneLogin.isClicked(3, this.anInt1537)) {
                this.anInt1617 = 0;
            }

            if (this.paneLogin.isClicked(3, this.controlLoginUser)) {
                this.paneLogin.setFocus((byte) 70, this.controlLoginPass);
            }

            if (this.paneLogin.isClicked(3, this.controlLoginPass) || this.paneLogin.isClicked(3, this.anInt1473)) {
                this.username = this.paneLogin.method523(-17054, this.controlLoginUser);
                this.password = this.paneLogin.method523(-17054, this.controlLoginPass);
                this.autoLoginTimeout = 2;
                this.login(false, this.username, this.password, (byte) -82);
            }
        }

        if (var1 != 32767) {
            this.duelOptionPrayer = -90;
        }
    }

    private void drawDialogSocialInput(int var1) {
        if (this.mouseButtonClick != 0) {
            this.mouseButtonClick = 0;
            if (this.showDialogSocialInput == 1 && (super.mouseX < 106 || super.mouseY < 145 || super.mouseX > 406 || super.mouseY > 215)) {
                this.showDialogSocialInput = 0;
                return;
            }

            if (this.showDialogSocialInput == 2 && (super.mouseX < 6 || super.mouseY < 145 || super.mouseX > 506 || super.mouseY > 215)) {
                this.showDialogSocialInput = 0;
                return;
            }

            if (this.showDialogSocialInput == 3 && (super.mouseX < 106 || super.mouseY < 145 || super.mouseX > 406 || super.mouseY > 215)) {
                this.showDialogSocialInput = 0;
                return;
            }

            if (super.mouseX > 236 && super.mouseX < 276 && super.mouseY > 193 && super.mouseY < 213) {
                this.showDialogSocialInput = 0;
                return;
            }
        }

        int var2 = 145;
        if (var1 != 20) {
            this.drawAboveHeadStuff(-112);
        }

        String var3;
        String var4;
        if (this.showDialogSocialInput == 1) {
            this.surface.drawBox(106, var2, 300, 70, 0);
            this.surface.drawBoxEdge(106, var2, 300, 70, 0xffffff);
            var2 += 20;
            this.surface.drawstringCenter("Enter name to add to friends list", 4, var2, 256, 0xffffff);
            var2 += 20;
            this.surface.drawstringCenter(super.inputTextCurrent + "*", 4, var2, 256, 0xffffff);
            var3 = Utility.formatName(this.localPlayer.accountName);
            if (var3 != null && super.inputTextFinal.length() > 0) {
                var4 = super.inputTextFinal.trim();
                super.inputTextFinal = "";
                this.showDialogSocialInput = 0;
                super.inputTextCurrent = "";
                if (var4.length() > 0 && !var3.equals(Utility.formatName(var4))) {
                    this.friendAdd(4, var4);
                }
            }
        }

        if (this.showDialogSocialInput == 2) {
            this.surface.drawBox(6, var2, 500, 70, 0);
            this.surface.drawBoxEdge(6, var2, 500, 70, 0xffffff);
            var2 += 20;
            this.surface.drawstringCenter("Enter message to send to " + this.privateMessageTarget, 4, var2, 256, 0xffffff);
            var2 += 20;
            this.surface.drawstringCenter(super.inputPmCurrent + "*", 4, var2, 256, 0xffffff);
            if (super.inputPmFinal.length() > 0) {
                var3 = super.inputPmFinal;
                super.inputPmCurrent = "";
                super.inputPmFinal = "";
                this.showDialogSocialInput = 0;
                this.sendPrivateMessage(var3, this.privateMessageTarget, (byte) -19);
            }
        }

        if (this.showDialogSocialInput == 3) {
            this.surface.drawBox(106, var2, 300, 70, 0);
            this.surface.drawBoxEdge(106, var2, 300, 70, 0xffffff);
            var2 += 20;
            this.surface.drawstringCenter("Enter name to add to ignore list", 4, var2, 256, 0xffffff);
            var2 += 20;
            this.surface.drawstringCenter(super.inputTextCurrent + "*", 4, var2, 256, 0xffffff);
            var3 = Utility.formatName(this.localPlayer.accountName);
            if (var3 != null && super.inputTextFinal.length() > 0) {
                var4 = super.inputTextFinal.trim();
                super.inputTextCurrent = "";
                super.inputTextFinal = "";
                this.showDialogSocialInput = 0;
                if (var4.length() > 0 && !var3.equals(Utility.formatName(var4))) {
                    this.ignoreAdd(var4);
                }
            }
        }

        int var6 = 0xffffff;
        if (super.mouseX > 236 && super.mouseX < 276 && super.mouseY > 193 && super.mouseY < 213) {
            var6 = 0xffff00;
        }

        this.surface.drawstringCenter("Cancel", 1, 208, 256, var6);
    }

    private void drawUiTabMagic(int var1, boolean var2) {
        int var3 = this.surface.width2 + -199;
        byte var4 = 36;
        this.surface.drawSprite((byte) 61, 3, 4 + this.spriteMedia, -49 + var3);
        short var5 = 196;
        short var6 = 182;
        int var7;
        int var8 = var7 = Surface.rgb2long(160, 160, 160);
        if (this.anInt1384 == 0) {
            var8 = Surface.rgb2long(220, 220, 220);
        } else {
            var7 = Surface.rgb2long(220, 220, 220);
        }

        this.surface.drawBoxAlpha(var3, var4, var5 / 2, 24, var8, 128);
        this.surface.drawBoxAlpha(var5 / 2 + var3, var4, var5 / 2, 24, var7, 128);
        this.surface.drawBoxAlpha(var3, 24 + var4, var5, 90, Surface.rgb2long(220, 220, 220), 128);
        this.surface.drawBoxAlpha(var3, 114 + var4, var5, -114 + var6, Surface.rgb2long(160, 160, 160), 128);
        this.surface.drawLineHoriz(var3, var4 - -24, var5, 0);
        this.surface.drawLineVert(var3 - -(var5 / 2), var4, 24, 0);
        this.surface.drawLineHoriz(var3, 113 + var4, var5, 0);
        this.surface.drawstringCenter("Magic", 4, 16 + var4, var5 / 4 + var3, 0);
        this.surface.drawstringCenter("Prayers", 4, var4 - -16, var5 / 2 + var3 + var5 / 4, 0);
        int var9;
        int var10;
        String var11;
        int var12;
        int var19;
        if (this.anInt1384 == 0) {
            this.panelMagic.clearList(this.controlMagicPanel);
            var9 = 0;

            int var13;
            for (var10 = 0; GameData.spellCount > var10; ++var10) {
                var11 = "@yel@";

                for (var12 = 0; var12 < GameData.spellRunesRequired[var10]; ++var12) {
                    var13 = GameData.spellRunesId[var10][var12];
                    if (!this.method65(var13, 0, GameData.spellRunesCount[var10][var12])) {
                        var11 = "@whi@";
                        break;
                    }
                }

                var13 = this.playerStatCurrent[6];
                if (GameData.spellLevel[var10] > var13) {
                    var11 = "@bla@";
                }

                this.panelMagic.addListEntry(this.controlMagicPanel, var9++, var11 + "Level " + GameData.spellLevel[var10] + ": " + GameData.spellName[var10], (String) null, (String) null, 0);
            }

            this.panelMagic.drawPanel(111);
            var19 = this.panelMagic.setFocus(this.controlMagicPanel);
            if (var19 == -1) {
                this.surface.drawstring("Point at a spell for a description", 2 + var3, 124 + var4, 1, 0);
            } else {
                this.surface.drawstring("Level " + GameData.spellLevel[var19] + ": " + GameData.spellName[var19], 2 + var3, 124 + var4, 1, 0xffff00);
                this.surface.drawstring(GameData.spellDescription[var19], 2 + var3, 136 + var4, 0, 0xffffff);

                for (var12 = 0; GameData.spellRunesRequired[var19] > var12; ++var12) {
                    var13 = GameData.spellRunesId[var19][var12];
                    this.surface.drawSprite((byte) 61, var4 - -150, GameData.itemPicture[var13] + this.spriteItem, 44 * var12 + var3 - -2);
                    int var14 = this.getInventoryCount(var13);
                    int var15 = GameData.spellRunesCount[var19][var12];
                    String var16 = "@red@";
                    if (this.method65(var13, 0, var15)) {
                        var16 = "@gre@";
                    }

                    this.surface.drawstring(var16 + var14 + "/" + var15, 44 * var12 + var3 + 2, var4 - -150, 1, 0xffffff);
                }
            }
        }

        if (this.anInt1384 == 1) {
            this.panelMagic.clearList(this.controlMagicPanel);
            var9 = 0;

            for (var10 = 0; var10 < GameData.prayerCount; ++var10) {
                var11 = "@whi@";
                if (GameData.prayerLevel[var10] > this.playerStatBase[5]) {
                    var11 = "@bla@";
                }

                if (this.prayerOn[var10]) {
                    var11 = "@gre@";
                }

                this.panelMagic.addListEntry(this.controlMagicPanel, var9++, var11 + "Level " + GameData.prayerLevel[var10] + ": " + GameData.prayerName[var10], (String) null, (String) null, 0);
            }

            this.panelMagic.drawPanel(94);
            var19 = this.panelMagic.setFocus(this.controlMagicPanel);
            if (var19 != -1) {
                this.surface.drawstringCenter("Level " + GameData.prayerLevel[var19] + ": " + GameData.prayerName[var19], 1, var4 - -130, var3 + var5 / 2, 0xffff00);
                this.surface.drawstringCenter(GameData.prayerDescription[var19], 0, var4 + 145, var5 / 2 + var3, 0xffffff);
                this.surface.drawstringCenter("Drain rate: " + GameData.prayerDrain[var19], 1, 160 + var4, var3 + var5 / 2, 0);
            } else {
                this.surface.drawstring("Point at a prayer for a description", 2 + var3, 124 + var4, 1, 0);
            }
        }

        if (var2) {
            int var18 = super.mouseY - 36;
            var3 = -this.surface.width2 - (-199 - super.mouseX);
            if (var1 != 40) {
                this.addTradeOffer(-125, 42, 123);
            }

            if (var3 >= 0 && var18 >= 0 && var3 < 196 && var18 < 182) {
                this.panelMagic.handleMouse(-89, super.lastMouseButtonDown, var3 - (199 + -this.surface.width2), super.anInt84, var18 + 36);
                if (var18 <= 24 && this.mouseButtonClick == 1) {
                    if (var3 < 98 && this.anInt1384 == 1) {
                        this.anInt1384 = 0;
                        this.panelMagic.method506(-22757, this.controlMagicPanel);
                    } else if (var3 > 98 && this.anInt1384 == 0) {
                        this.anInt1384 = 1;
                        this.panelMagic.method506(-22757, this.controlMagicPanel);
                    }
                }

                if (this.mouseButtonClick == 1 && this.anInt1384 == 0) {
                    var9 = this.panelMagic.setFocus(this.controlMagicPanel);
                    if (var9 != -1) {
                        var10 = this.playerStatCurrent[6];
                        if (GameData.spellLevel[var9] <= var10) {
                            for (var19 = 0; var19 < GameData.spellRunesRequired[var9]; ++var19) {
                                var12 = GameData.spellRunesId[var9][var19];
                                if (!this.method65(var12, 0, GameData.spellRunesCount[var9][var19])) {
                                    this.showMessage(0, "You don\'t have all the reagents you need for this spell", (String) null, (String) null, 0, (String) null, false);
                                    var19 = -1;
                                    break;
                                }
                            }

                            if (GameData.spellRunesRequired[var9] == var19) {
                                this.selectedItemInventoryIndex = -1;
                                this.selectedSpell = var9;
                            }
                        } else {
                            this.showMessage(0, "Your magic ability is not high enough for this spell", (String) null, (String) null, 0, (String) null, false);
                        }
                    }
                }

                if (this.mouseButtonClick == 1 && this.anInt1384 == 1) {
                    var9 = this.panelMagic.setFocus(this.controlMagicPanel);
                    if (var9 != -1) {
                        var10 = this.playerStatBase[5];
                        if (var10 >= GameData.prayerLevel[var9]) {
                            if (this.playerStatCurrent[5] == 0) {
                                this.showMessage(0, "You have run out of prayer points. Return to a church to recharge", (String) null, (String) null, 0, (String) null, false);
                            } else if (this.prayerOn[var9]) {
                                this.clientStream.newPacket(254);
                                this.clientStream.writeBuffer.putByte(var9);
                                this.clientStream.sendPacket();
                                this.prayerOn[var9] = false;
                                this.playSoundFile("prayeroff");
                            } else {
                                this.clientStream.newPacket(60);
                                this.clientStream.writeBuffer.putByte(var9);
                                this.clientStream.sendPacket();
                                this.prayerOn[var9] = true;
                                this.playSoundFile("prayeron");
                            }
                        } else {
                            this.showMessage(0, "Your prayer ability is not high enough for this prayer", (String) null, (String) null, 0, (String) null, false);
                        }
                    }
                }

                this.mouseButtonClick = 0;
            }
        }
        ;
    }

    private void drawDialogTrade(int var1) {
        int var2 = -1;
        if (this.mouseButtonClick != 0 && this.aBoolean1453) {
            var2 = this.menuTrade.drawNoBg(this.anInt1497, this.anInt1408, super.mouseX, super.mouseY);
        }

        int var3;
        int var4;
        int var5;
        int var6;
        int var7;
        int var8;
        int var9;
        int var10;
        if (var2 < 0) {
            if (this.inputPopupType == 0) {
                if (this.mouseButtonClick == 1 && this.mouseButtonItemCountIncrement == 0) {
                    this.mouseButtonItemCountIncrement = 1;
                }

                var3 = -22 + super.mouseX;
                var4 = super.mouseY - 36;
                if (var3 >= 0 && var4 >= 0 && var3 < 468 && var4 < 262) {
                    if (this.mouseButtonItemCountIncrement > 0) {
                        if (var3 > 216 && var4 > 30 && var3 < 462 && var4 < 235) {
                            var5 = 5 * ((var4 - 31) / 34) + (var3 + -217) / 49;
                            if (var5 >= 0 && this.inventoryItemsCount > var5) {
                                this.addTradeOffer(var5, 68, -1);
                            }
                        }

                        if (var3 > 8 && var4 > 30 && var3 < 205 && var4 < 133) {
                            var5 = 4 * ((var4 - 31) / 34) + (-9 + var3) / 49;
                            if (var5 >= 0 && var5 < this.tradeItemsCount) {
                                this.updateTradeItems(var5, -1, var1 + 107);
                            }
                        }

                        if (var3 >= 217 && var4 >= 238 && var3 <= 286 && var4 <= 259) {
                            this.tradeAccepted = true;
                            this.clientStream.newPacket(55);
                            this.clientStream.sendPacket();
                        }

                        if (var3 >= 394 && var4 >= 238 && var3 < 463 && var4 < 259) {
                            this.showDialogTrade = false;
                            this.clientStream.newPacket(230);
                            this.clientStream.sendPacket();
                        }

                        this.mouseButtonItemCountIncrement = 0;
                        this.mouseButtonClick = 0;
                    }

                    if (this.mouseButtonClick == 2) {
                        if (var3 > 216 && var4 > 30 && var3 < 462 && var4 < 235) {
                            var5 = this.menuCommon.getWidth();
                            var6 = this.menuCommon.getHeight();
                            this.anInt1379 = -(var5 / 2) + super.mouseX;
                            this.anInt1625 = super.mouseY - 7;
                            this.showRightClickMenu = true;
                            if (this.anInt1625 < 0) {
                                this.anInt1625 = 0;
                            }

                            if (this.anInt1379 < 0) {
                                this.anInt1379 = 0;
                            }

                            if (this.anInt1379 + var5 > 510) {
                                this.anInt1379 = 510 + -var5;
                            }

                            if (var6 + this.anInt1625 > 315) {
                                this.anInt1625 = -var6 + 315;
                            }

                            var7 = (-217 + var3) / 49 + 5 * ((var4 - 31) / 34);
                            if (var7 >= 0 && this.inventoryItemsCount > var7) {
                                var8 = this.inventoryItemId[var7];
                                this.aBoolean1453 = true;
                                this.menuTrade.recalculateSize();
                                this.menuTrade.addItem(1, 1, "Offer 1", "@lre@" + GameData.itemName[var8], var8);
                                this.menuTrade.addItem(5, 1, "Offer 5", "@lre@" + GameData.itemName[var8], var8);
                                this.menuTrade.addItem(10, 1, "Offer 10", "@lre@" + GameData.itemName[var8], var8);
                                this.menuTrade.addItem(-1, 1, "Offer All", "@lre@" + GameData.itemName[var8], var8);
                                this.menuTrade.addItem(-2, 1, "Offer X", "@lre@" + GameData.itemName[var8], var8);
                                var9 = this.menuTrade.getWidth();
                                var10 = this.menuTrade.getHeight();
                                this.anInt1497 = -7 + super.mouseY;
                                this.anInt1408 = -(var9 / 2) + super.mouseX;
                                if (this.anInt1497 < 0) {
                                    this.anInt1497 = 0;
                                }

                                if (this.anInt1408 < 0) {
                                    this.anInt1408 = 0;
                                }

                                if ((var10 + this.anInt1497) > 315) {
                                    this.anInt1497 = -var10 + 315;
                                }

                                if (this.anInt1408 + var9 > 510) {
                                    this.anInt1408 = 510 - var9;
                                }
                            }
                        }

                        if (var3 > 8 && var4 > 30 && var3 < 205 && var4 < 133) {
                            var5 = 4 * ((-31 + var4) / 34) + (-9 + var3) / 49;
                            if (var5 >= 0 && this.tradeItemsCount > var5) {
                                var6 = this.tradeItems[var5];
                                this.aBoolean1453 = true;
                                this.menuTrade.recalculateSize();
                                this.menuTrade.addItem(1, 2, "Remove 1", "@lre@" + GameData.itemName[var6], var6);
                                this.menuTrade.addItem(5, 2, "Remove 5", "@lre@" + GameData.itemName[var6], var6);
                                this.menuTrade.addItem(10, 2, "Remove 10", "@lre@" + GameData.itemName[var6], var6);
                                this.menuTrade.addItem(-1, 2, "Remove All", "@lre@" + GameData.itemName[var6], var6);
                                this.menuTrade.addItem(-2, 2, "Remove X", "@lre@" + GameData.itemName[var6], var6);
                                var7 = this.menuTrade.getWidth();
                                var8 = this.menuTrade.getHeight();
                                this.anInt1408 = super.mouseX + -(var7 / 2);
                                this.anInt1497 = -7 + super.mouseY;
                                if (this.anInt1408 < 0) {
                                    this.anInt1408 = 0;
                                }

                                if (this.anInt1497 < 0) {
                                    this.anInt1497 = 0;
                                }

                                if (this.anInt1408 + var7 > 510) {
                                    this.anInt1408 = 510 + -var7;
                                }

                                if (this.anInt1497 + var8 > 315) {
                                    this.anInt1497 = 315 - var8;
                                }
                            }
                        }

                        this.mouseButtonClick = 0;
                    }

                    if (this.aBoolean1453) {
                        var5 = this.menuTrade.getWidth();
                        var6 = this.menuTrade.getHeight();
                        if (super.mouseX < (this.anInt1408 - 10) || super.mouseY < -10 + this.anInt1497 || super.mouseX > this.anInt1408 + var5 + 10 || 10 + (this.anInt1497 - -var6) < super.mouseY) {
                            this.aBoolean1453 = false;
                        }
                    }
                } else if (this.mouseButtonClick != 0) {
                    this.showDialogTrade = false;
                    this.clientStream.newPacket(230);
                    this.clientStream.sendPacket();
                }
            }
        } else {
            this.mouseButtonClick = 0;
            this.aBoolean1453 = false;
            var3 = this.menuTrade.getItemMenuIndex(var2);
            var4 = this.menuTrade.getMenuItemX(var2);
            var5 = -1;
            var6 = 0;
            if (var3 == 1) {
                for (var7 = 0; var7 < this.inventoryItemsCount; ++var7) {
                    if (var4 == this.inventoryItemId[var7]) {
                        if (var5 < 0) {
                            var5 = var7;
                        }

                        if (GameData.itemStackable[var4] == 0) {
                            var6 = this.inventoryItemStackCount[var7];
                            break;
                        }

                        ++var6;
                    }
                }
            } else {
                for (var7 = 0; var7 < this.tradeItemsCount; ++var7) {
                    if (var4 == this.tradeItems[var7]) {
                        if (var5 < 0) {
                            var5 = var7;
                        }

                        if (GameData.itemStackable[var4] == 0) {
                            var6 = this.tradeItemCount[var7];
                            break;
                        }

                        ++var6;
                    }
                }
            }

            if (var5 >= 0) {
                var7 = this.menuTrade.getMenuItemY(var2);
                if (var7 == -2) {
                    this.anInt1393 = var5;
                    if (var3 != 1) {
                        this.showInputPopup(2, ClientStream.aStringArray1683, true);
                    } else {
                        this.showInputPopup(1, aStringArray138, true);
                    }
                } else {
                    if (var7 == -1) {
                        var7 = var6;
                    }

                    if (var3 == 1) {
                        this.addTradeOffer(var5, 59, var7);
                    } else {
                        this.updateTradeItems(var5, var7, var1 ^ -102);
                    }
                }
            }
        }

        if (this.showDialogTrade) {
            byte var17 = 22;
            byte var18 = 36;
            this.surface.drawBox(var17, var18, 468, 12, 192);
            var5 = 10000536;
            this.surface.drawBoxAlpha(var17, 12 + var18, 468, 18, var5, 160);
            this.surface.drawBoxAlpha(var17, 30 + var18, 8, 248, var5, 160);
            this.surface.drawBoxAlpha(205 + var17, var18 - -30, 11, 248, var5, 160);
            this.surface.drawBoxAlpha(462 + var17, var18 - -30, 6, 248, var5, 160);
            this.surface.drawBoxAlpha(8 + var17, var18 - -133, 197, 22, var5, 160);
            this.surface.drawBoxAlpha(8 + var17, var18 + 258, 197, 20, var5, 160);
            this.surface.drawBoxAlpha(var17 - -216, var18 + 235, 246, 43, var5, 160);
            var6 = 13684944;
            this.surface.drawBoxAlpha(8 + var17, 30 + var18, 197, 103, var6, 160);
            this.surface.drawBoxAlpha(8 + var17, 155 + var18, 197, 103, var6, 160);
            this.surface.drawBoxAlpha(var17 - -216, var18 + 30, 246, 205, var6, 160);

            for (var7 = 0; var7 < 4; ++var7) {
                this.surface.drawLineHoriz(8 + var17, 34 * var7 + 30 + var18, 197, 0);
            }

            if (var1 != -2) {
                this.spriteProjectile = -6;
            }

            for (var8 = 0; var8 < 4; ++var8) {
                this.surface.drawLineHoriz(var17 + 8, 155 + var18 + var8 * 34, 197, 0);
            }

            for (var9 = 0; var9 < 7; ++var9) {
                this.surface.drawLineHoriz(216 + var17, var9 * 34 + 30 + var18, 246, 0);
            }

            for (var10 = 0; var10 < 6; ++var10) {
                if (var10 < 5) {
                    this.surface.drawLineVert(var17 - -8 - -(49 * var10), 30 + var18, 103, 0);
                }

                if (var10 < 5) {
                    this.surface.drawLineVert(var17 - (-8 - 49 * var10), var18 + 155, 103, 0);
                }

                this.surface.drawLineVert(var17 + (216 - -(var10 * 49)), var18 + 30, 205, 0);
            }

            this.surface.drawstring("Trading with: " + this.tradeRecipientName, 1 + var17, var18 + 10, 1, 0xffffff);
            this.surface.drawstring("Your Offer", 9 + var17, var18 + 27, 4, 0xffffff);
            this.surface.drawstring("Opponent\'s Offer", var17 - -9, var18 - -152, 4, 0xffffff);
            this.surface.drawstring("Your Inventory", var17 - -216, var18 - -27, 4, 0xffffff);
            if (!this.tradeAccepted) {
                this.surface.drawSprite((byte) 61, var18 - -238, this.spriteMedia - -25, var17 - -217);
            }

            this.surface.drawSprite((byte) 61, 238 + var18, 26 + this.spriteMedia, var17 - -394);
            if (this.tradeRecipientAccepted) {
                this.surface.drawstringCenter("Other player", 1, 246 + var18, 341 + var17, 0xffffff);
                this.surface.drawstringCenter("has accepted", 1, 256 + var18, 341 + var17, 0xffffff);
            }

            if (this.tradeAccepted) {
                this.surface.drawstringCenter("Waiting for", 1, 246 + var18, var17 - -217 - -35, 0xffffff);
                this.surface.drawstringCenter("other player", 1, 256 + var18, var17 - -217 - -35, 0xffffff);
            }

            int var12;
            int var13;
            for (int var11 = 0; var11 < this.inventoryItemsCount; ++var11) {
                var12 = var17 + 217 + 49 * (var11 % 5);
                var13 = var18 + 31 + 34 * (var11 / 5);
                this.surface.method409(GameData.itemMask[this.inventoryItemId[var11]], GameData.itemPicture[this.inventoryItemId[var11]] + this.spriteItem, var13, -56, 32, 0, 48, var12, false, 0);
                if (GameData.itemStackable[this.inventoryItemId[var11]] == 0) {
                    this.surface.drawstring(String.valueOf(this.inventoryItemStackCount[var11]), 1 + var12, var13 - -10, 1, 0xffff00);
                }
            }

            int var14;
            for (var12 = 0; this.tradeItemsCount > var12; ++var12) {
                var13 = var17 + 9 + var12 % 4 * 49;
                var14 = 34 * (var12 / 4) + 31 + var18;
                this.surface.method409(GameData.itemMask[this.tradeItems[var12]], GameData.itemPicture[this.tradeItems[var12]] + this.spriteItem, var14, -66, 32, 0, 48, var13, false, 0);
                if (GameData.itemStackable[this.tradeItems[var12]] == 0) {
                    this.surface.drawstring(String.valueOf(this.tradeItemCount[var12]), 1 + var13, 10 + var14, 1, 0xffff00);
                }

                if (super.mouseX > var13 && super.mouseX < (var13 + 48) && var14 < super.mouseY && var14 + 32 > super.mouseY) {
                    this.surface.drawstring(GameData.itemName[this.tradeItems[var12]] + ": @whi@" + GameData.itemDescription[this.tradeItems[var12]], 8 + var17, var18 - -273, 1, 0xffff00);
                }
            }

            for (var13 = 0; this.tradeRecipientItemsCount > var13; ++var13) {
                var14 = 9 - (-var17 + -(49 * (var13 % 4)));
                int var15 = 34 * (var13 / 4) + 156 + var18;
                this.surface.method409(GameData.itemMask[this.tradeRecipientItems[var13]], this.spriteItem - -GameData.itemPicture[this.tradeRecipientItems[var13]], var15, -93, 32, 0, 48, var14, false, 0);
                if (GameData.itemStackable[this.tradeRecipientItems[var13]] == 0) {
                    this.surface.drawstring(String.valueOf(this.tradeRecipientItemCount[var13]), var14 + 1, var15 - -10, 1, 0xffff00);
                }

                if (var14 < super.mouseX && var14 + 48 > super.mouseX && var15 < super.mouseY && (var15 + 32) > super.mouseY) {
                    this.surface.drawstring(GameData.itemName[this.tradeRecipientItems[var13]] + ": @whi@" + GameData.itemDescription[this.tradeRecipientItems[var13]], var17 + 8, var18 + 273, 1, 0xffff00);
                }
            }

            if (this.aBoolean1453) {
                this.menuTrade.draw(this.anInt1497, super.mouseX, this.anInt1408, super.mouseY);
            }
        }
    }

    private void handlePacket(int opcode, int len) {
        opcode = this.clientStream.isaacIncoming(opcode);
        if (opcode == 131) {
            int type = this.packetsIncoming.getUnsignedByte();
            int var17 = this.packetsIncoming.getUnsignedByte();
            String message = this.packetsIncoming.gjstr2();
            String sender = null;
            String clan = null;
            if ((var17 & 1) != 0) {
                sender = this.packetsIncoming.gjstr2();
            }

            String color = null;
            if ((1 & var17) != 0) {// ??
                clan = this.packetsIncoming.gjstr2();
            }

            if ((var17 & 2) != 0) {
                color = this.packetsIncoming.gjstr2();
            }

            this.showMessage(type, message, sender, clan, 0, color, false);
        } else if (opcode == 4) {
            this.closeConnection(true, -10);
        } else if (opcode == 183) {
            this.cantLogout((byte) 59);
            return;
        } else if (opcode == 165) {
            this.closeConnection(false, -10);
            return;
        } else if (opcode == 149) {
            String friendName = this.packetsIncoming.gjstr2();
            String friendOldName = this.packetsIncoming.gjstr2();
            int onlineStatus = this.packetsIncoming.getUnsignedByte();
            boolean var18 = (onlineStatus & 1) != 0;
            boolean loggedIn = (4 & onlineStatus) != 0;
            String friendServer = null;
            if (loggedIn) {
                friendServer = this.packetsIncoming.gjstr2();
            }

            for (int index = 0; friendListCount > index; ++index) {
                if (!var18) {
                    if (friendListNames[index].equals(friendName)) {
                        if (friendListServer[index] == null && loggedIn) {
                            this.showMessage(5, friendName + " has logged in", (String) null, (String) null, 0, (String) null, false);
                        }

                        if (friendListServer[index] != null && !loggedIn) {
                            this.showMessage(5, friendName + " has logged out", (String) null, (String) null, 0, (String) null, false);
                        }

                        friendListOldNames[index] = friendOldName;
                        friendListServer[index] = friendServer;
                        friendListOnline[index] = onlineStatus;
                        this.sortFriendList((byte) -128);
                        return;
                    }
                } else if (friendListNames[index].equals(friendOldName)) {
                    if (friendListServer[index] == null && loggedIn) {
                        this.showMessage(5, friendName + " has logged in", (String) null, (String) null, 0, (String) null, false);
                    }

                    if (friendListServer[index] != null && !loggedIn) {
                        this.showMessage(5, friendName + " has logged out", (String) null, (String) null, 0, (String) null, false);
                    }

                    friendListNames[index] = friendName;
                    friendListOldNames[index] = friendOldName;
                    friendListServer[index] = friendServer;
                    friendListOnline[index] = onlineStatus;
                    this.sortFriendList((byte) -128);
                    return;
                }
            }

            if (var18) {
                System.out.println("Error: friend display name change packet received, but old name \'" + friendOldName + "\' is not on friend list");
                return;
            }

            friendListNames[friendListCount] = friendName;
            friendListOldNames[friendListCount] = friendOldName;
            friendListServer[friendListCount] = friendServer;
            friendListOnline[friendListCount] = onlineStatus;
            ++friendListCount;
            this.sortFriendList((byte) -128);
            return;
        } else if (opcode == 237) {
            String var4 = this.packetsIncoming.gjstr2();
            String var5 = this.packetsIncoming.gjstr2();
            if (var5.length() == 0) {
                var5 = var4;
            }

            String var16 = this.packetsIncoming.gjstr2();
            String var7 = this.packetsIncoming.gjstr2();
            if (var7.length() == 0) {
                var7 = var4;
            }

            boolean var8 = this.packetsIncoming.getUnsignedByte() == 1;

            for (int index = 0; index < ignoreListCount; ++index) {
                if (var8) {
                    if (ignoreListAccNames[index].equals(var7)) {
                        ignoreListNames[index] = var4;
                        ignoreListAccNames[index] = var5;
                        ignoreListOldNames[index] = var16;
                        ignoreListServers[index] = var7;
                        return;
                    }
                } else if (ignoreListAccNames[index].equals(var5)) {
                    return;
                }
            }

            if (var8) {
                System.out.println("Error: ignore display name change packet received, but old name \'" + var7 + "\' is not on ignore list");
                return;
            }

            ignoreListNames[ignoreListCount] = var4;
            ignoreListAccNames[ignoreListCount] = var5;
            ignoreListOldNames[ignoreListCount] = var16;
            ignoreListServers[ignoreListCount] = var7;
            return;
        } else if (opcode == 109) {
            ignoreListCount = this.packetsIncoming.getUnsignedByte();

            for (int var15 = 0; var15 < ignoreListCount; ++var15) {
                ignoreListNames[var15] = this.packetsIncoming.gjstr2();
                ignoreListAccNames[var15] = this.packetsIncoming.gjstr2();
                ignoreListOldNames[var15] = this.packetsIncoming.gjstr2();
                ignoreListServers[var15] = this.packetsIncoming.gjstr2();
            }

            return;
        } else if (opcode == 51) {
            this.settingsBlockChat = this.packetsIncoming.getUnsignedByte();
            this.settingsBlockPrivate = this.packetsIncoming.getUnsignedByte();
            this.settingsBlockTrade = this.packetsIncoming.getUnsignedByte();
            this.settingsBlockDuel = this.packetsIncoming.getUnsignedByte();
            return;
        } else if (opcode == 120) {
            String sender = this.packetsIncoming.gjstr2();
            String senderClan = this.packetsIncoming.gjstr2();
            int modStatus = this.packetsIncoming.getUnsignedByte();
            long unknown = this.packetsIncoming.method214(-121);
            String message = ClientStreamBase.cabbage(this.packetsIncoming);

            for (int index = 0; index < 100; ++index) {
                if (this.friendListUnknown[index] == unknown) {
                    return;
                }
            }

            this.friendListUnknown[this.friendListUnknown2] = unknown;
            this.friendListUnknown2 = (this.friendListUnknown2 + 1) % 100;

            this.showMessage(1, message, sender, senderClan, modStatus, (String) null, modStatus == 2);
            return;
        } else if (opcode == 87) {
            String sender = this.packetsIncoming.gjstr2();
            String message = ClientStreamBase.cabbage(this.packetsIncoming);
            this.showMessage(2, message, sender, sender, 0, (String) null, false);
            return;
        } else if (opcode == 189) {
            this.packetsIncoming.offset += 28;
            if (this.packetsIncoming.value_same(0)) {
                writeCache(-28 + this.packetsIncoming.offset, this.packetsIncoming);
                return;
            }
        } else {
            this.handleIncomingPacket(opcode, len);
        }
    }

    private void method48(int var1) {
        this.welcomeScreenShown = false;
        this.surface.interlace = false;
        this.surface.blackScreen((byte) 127);
        if (this.anInt1617 == 0 || this.anInt1617 == 1 || this.anInt1617 == 2 || this.anInt1617 == 3) {
            int var2 = this.anInt1360 * 2 % 3072;
            if (var2 < 1024) {
                this.surface.drawSprite((byte) 61, 10, this.spriteLogo, 0);
                if (var2 > 768) {
                    this.surface.drawSpriteAlpha(0, 10, this.spriteLogo - -1, -768 + var2);
                }
            } else if (var2 < 2048) {
                this.surface.drawSprite((byte) 61, 10, 1 + this.spriteLogo, 0);
                if (var2 > 1792) {
                    this.surface.drawSpriteAlpha(0, 10, 10 + this.spriteMedia, -1792 + var2);
                }
            } else {
                this.surface.drawSprite((byte) 61, 10, this.spriteMedia - -10, 0);
                if (var2 > 2816) {
                    this.surface.drawSpriteAlpha(0, 10, this.spriteLogo, var2 - 2816);
                }
            }
        }

        if (this.anInt1617 == 0) {
            this.panelLoginWelcome.drawPanel(118);
        }

        if (this.anInt1617 == 2) {
            String var4 = this.paneLogin.method523(-17054, this.controlLoginStatus1);
            if (var4 != null && var4.length() > 0) {
                this.surface.drawBoxAlpha(0, 185, this.gameWidth, 30, 0, 100);
            }

            this.paneLogin.drawPanel(var1 ^ 29);
        }

        this.surface.drawSprite((byte) 61, this.gameHeight, this.spriteMedia - -22, 0);
        this.surface.draw(-2020315800, this.graphics, super.anInt58, super.anInt7);
        if (var1 != 64) {
            this.controlLoginPass = -18;
        }
    }

    private void checkConnection(int var1) {
        long var2 = Utility.currentTimeMillis(-55);
        if (this.clientStream.method465()) {
            this.packetLastRead = var2;
        }

        if ((-this.packetLastRead + var2) > 5000) {
            this.packetLastRead = var2;
            this.clientStream.newPacket(67);
            this.clientStream.sendPacket();
        }

        try {
            this.clientStream.writePacket(20);
        } catch (IOException var6) {
            this.lostConnection(var1 + -12);
            return;
        }

        if (this.method105(var1 + 123)) {
            int len = this.clientStream.readPacket(this.packetsIncoming);
            if (var1 < len) {
                this.handlePacket(this.packetsIncoming.getUnsignedByte(), len);
            }
        }
    }

    private void createRightClickMenuPlayer(int var1, int var2) {
        Character player = this.players[var2];
        if (var1 != -8242) {
            this.controlPlayerinfopanel = 32;
        }

        String playerName = player.displayName;
        int var5 = -this.regionY + -this.localRegionY - (this.planeHeight - 2203);
        if (this.regionX + this.localRegionX + this.planeWidth >= 2640) {
            var5 = -50;
        }

        String levelDiffText = "";
        int levelDiff = 0;
        if (this.localPlayer.level > 0 && player.level > 0) {
            levelDiff = this.localPlayer.level - player.level;
        }

        if (levelDiff < 0) {
            levelDiffText = "@or1@";
        }

        if (levelDiff < -3) {
            levelDiffText = "@or2@";
        }

        if (levelDiff < -6) {
            levelDiffText = "@or3@";
        }

        if (levelDiff < -9) {
            levelDiffText = "@red@";
        }

        if (levelDiff > 0) {
            levelDiffText = "@gr1@";
        }

        if (levelDiff > 3) {
            levelDiffText = "@gr2@";
        }

        if (levelDiff > 6) {
            levelDiffText = "@gr3@";
        }

        if (levelDiff > 9) {
            levelDiffText = "@gre@";
        }

        levelDiffText = " " + levelDiffText + "(level-" + player.level + ")";
        if (this.selectedSpell >= 0) {
            if (GameData.spellType[this.selectedSpell] == 1 || GameData.spellType[this.selectedSpell] == 2) {
                this.menuCommon.addItem(this.selectedSpell, 800, "Cast " + GameData.spellName[this.selectedSpell] + " on", "@whi@" + playerName + levelDiffText, player.serverIndex);
            }

        } else if (this.selectedItemInventoryIndex >= 0) {
            this.menuCommon.addItem(this.selectedItemInventoryIndex, 810, "Use " + this.selectedItemName + " with", "@whi@" + playerName + levelDiffText, player.serverIndex);
        } else {
            if (var5 > 0 && (-64 + player.currentY) / this.magicLoc + (this.planeHeight - -this.regionY) < 2203) {
                this.menuCommon.addItem(levelDiff >= 0 && levelDiff < 5 ? 805 : 2805, "Attack", player.serverIndex, "@whi@" + playerName + levelDiffText);
            } else if (this.members) {
                this.menuCommon.addItem(2806, "Duel with", player.serverIndex, "@whi@" + playerName + levelDiffText);
            }

            this.menuCommon.addItem(2810, "Trade with", player.serverIndex, "@whi@" + playerName + levelDiffText);
            this.menuCommon.addItem(2820, "Follow", player.serverIndex, "@whi@" + playerName + levelDiffText);
            this.menuCommon.addItem("Report abuse", player.accountName, 2833, 0, "@whi@" + playerName + levelDiffText, player.displayName);
        }
    }

    private void drawMinimapEntity(boolean var1, int var2, int var3, int var4) {
        if (var1) {
            this.method115((byte) -42);
        }

        this.surface.method401((byte) 118, var3, var4, var2);
        this.surface.method401((byte) 118, var3, var4 - 1, var2);
        this.surface.method401((byte) 118, var3, var4 - -1, var2);
        this.surface.method401((byte) 118, -1 + var3, var4, var2);
        this.surface.method401((byte) 118, 1 + var3, var4, var2);
    }

    private void drawDialogDuelConfirm(boolean var1) {
        byte var2 = 22;
        byte var3 = 36;
        this.surface.drawBox(var2, var3, 468, 16, 192);
        int var4 = 10000536;
        if (!var1) {
            this.drawGame(-60);
        }

        this.surface.drawBoxAlpha(var2, 16 + var3, 468, 246, var4, 160);
        this.surface.drawstringCenter("Please confirm your duel with @yel@" + this.duelOpponentName, 1, 12 + var3, var2 - -234, 0xffffff);
        this.surface.drawstringCenter("Your stake:", 1, 30 + var3, var2 + 117, 0xffff00);

        for (int var5 = 0; this.duelItemsCount > var5; ++var5) {
            String var6 = GameData.itemName[this.duelItems[var5]];
            if (GameData.itemStackable[this.duelItems[var5]] == 0) {
                var6 = var6 + " x " + Character.method363(this.duelItemCount[var5], 0);
            }

            this.surface.drawstringCenter(var6, 1, var3 - -42 + 12 * var5, var2 + 117, 0xffffff);
        }

        if (this.duelItemsCount == 0) {
            this.surface.drawstringCenter("Nothing!", 1, 42 + var3, 117 + var2, 0xffffff);
        }

        this.surface.drawstringCenter("Your opponent\'s stake:", 1, 30 + var3, var2 + 351, 0xffff00);

        for (int var9 = 0; this.duelOpponentItemsCount > var9; ++var9) {
            String var7 = GameData.itemName[this.duelOpponentItems[var9]];
            if (GameData.itemStackable[this.duelOpponentItems[var9]] == 0) {
                var7 = var7 + " x " + Character.method363(this.duelOpponentItemCount[var9], 0);
            }

            this.surface.drawstringCenter(var7, 1, 42 + var3 + var9 * 12, 351 + var2, 0xffffff);
        }

        if (this.duelOpponentItemsCount == 0) {
            this.surface.drawstringCenter("Nothing!", 1, var3 + 42, 351 + var2, 0xffffff);
        }

        if (this.duelOptionRetreat != 0) {
            this.surface.drawstringCenter("No retreat is possible!", 1, var3 + 180, 234 + var2, 0xff0000);
        } else {
            this.surface.drawstringCenter("You can retreat from this duel", 1, var3 - -180, 234 + var2, '\uff00');
        }

        if (this.duelOptionMagic != 0) {
            this.surface.drawstringCenter("Magic cannot be used", 1, 192 + var3, var2 - -234, 0xff0000);
        } else {
            this.surface.drawstringCenter("Magic may be used", 1, 192 + var3, 234 + var2, '\uff00');
        }

        if (this.duelOptionPrayer != 0) {
            this.surface.drawstringCenter("Prayer cannot be used", 1, 204 + var3, 234 + var2, 0xff0000);
        } else {
            this.surface.drawstringCenter("Prayer may be used", 1, 204 + var3, 234 + var2, '\uff00');
        }

        if (this.duelOptionWeapons == 0) {
            this.surface.drawstringCenter("Weapons may be used", 1, var3 - -216, var2 - -234, '\uff00');
        } else {
            this.surface.drawstringCenter("Weapons cannot be used", 1, 216 + var3, var2 - -234, 0xff0000);
        }

        this.surface.drawstringCenter("If you are sure click \'Accept\' to begin the duel", 1, var3 + 230, 234 + var2, 0xffffff);
        if (this.duelAccepted) {
            this.surface.drawstringCenter("Waiting for other player...", 1, 250 + var3, var2 - -234, 0xffff00);
        } else {
            this.surface.drawSprite((byte) 61, 238 + var3, 25 + this.spriteMedia, -35 + var2 - -118);
            this.surface.drawSprite((byte) 61, 238 + var3, this.spriteMedia - -26, var2 - -317);
        }

        if (this.mouseButtonClick == 1) {
            if (super.mouseX < var2 || var3 > super.mouseY || (468 + var2) < super.mouseX || super.mouseY > 262 + var3) {
                this.showDialogDuelConfirm = false;
                this.clientStream.newPacket(230);
                this.clientStream.sendPacket();
            }

            if ((-35 + 118 + var2) <= super.mouseX && 70 + (var2 - -118) >= super.mouseX && (238 + var3) <= super.mouseY && var3 + 238 + 21 >= super.mouseY) {
                this.duelAccepted = true;
                this.clientStream.newPacket(77);
                this.clientStream.sendPacket();
            }

            if (super.mouseX >= (var2 + 352 + -35) && (var2 + 423) >= super.mouseX && super.mouseY >= (var3 + 238) && super.mouseY <= var3 + 238 + 21) {
                this.showDialogDuelConfirm = false;
                this.clientStream.newPacket(197);
                this.clientStream.sendPacket();
            }

            this.mouseButtonClick = 0;
        }
        ;
    }

    private boolean method53(byte var1, int var2) {
        if (var1 > -69) {
            this.playerCount = 45;
        }


        for (int var3 = 0; var3 < this.inventoryItemsCount; ++var3) {
            if (this.inventoryItemId[var3] == var2 && this.inventoryItemEquipped[var3] == 1) {
                return true;
            }
        }

        return false;
    }

    final void handleKeyPress(byte var1, int var2) {
        if (this.loggedIn == 0) {
            if (this.anInt1617 == 0 && this.panelLoginWelcome != null) {
                this.panelLoginWelcome.keyPress(0, var2);
            }

            if (this.anInt1617 == 2 && this.paneLogin != null) {
                this.paneLogin.keyPress(0, var2);
            }
        }

        if (this.loggedIn == 1) {
            if (this.showAppearanceChange) {
                this.panelAppearance.keyPress(0, var2);
                return;
            }

            if (this.showDialogSocialInput == 0 && this.showDialogReportAbuseStep == 0 && !this.isSleeping && this.inputPopupType == 0) {
                this.panelMessageTabs.keyPress(0, var2);
            }
        }

        int var3 = -18 / ((-68 - var1) / 58);
    }

    private boolean walkToSource(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7, boolean var8, int var9) {
        int var10 = this.world.method263(this.anIntArray1330, var9, var3, var5, var7, this.anIntArray1336, var1, var2, var6, 3);
        if (var10 == -1) {
            if (!var8) {
                return false;
            }

            this.anIntArray1336[0] = var1;
            var10 = 1;
            this.anIntArray1330[0] = var3;
        }

        --var10;
        int var11 = 15 / ((17 - var4) / 63);
        var6 = this.anIntArray1336[var10];
        var2 = this.anIntArray1330[var10];
        --var10;
        if (var8) {
            this.clientStream.newPacket(16);
        } else {
            this.clientStream.newPacket(187);
        }

        this.clientStream.writeBuffer.putShort(this.regionX + var6);
        this.clientStream.writeBuffer.putShort(this.regionY + var2);
        if (var8 && var10 == -1 && ((this.regionX + var6) % 5) == 0) {
            var10 = 0;
        }

        for (int var12 = var10; var12 >= 0 && var12 > (var10 - 25); --var12) {
            this.clientStream.writeBuffer.putByte(this.anIntArray1336[var12] - var6);
            this.clientStream.writeBuffer.putByte(-var2 + this.anIntArray1330[var12]);
        }

        this.clientStream.sendPacket();
        this.anInt1265 = super.mouseY;
        this.mouseClickXStep = -24;
        this.anInt1289 = super.mouseX;
        return true;
    }

    private void renderLoginScreenViewports(boolean var1) {
        byte var2 = 0;
        byte var3 = 50;
        byte var4 = 50;
        this.world.method269(23 + 48 * var3, var4 * 48 - -23, 5280, var2);
        this.world.method251(this.gameModels, false);
        short var5 = 6400;
        short var6 = 1100;
        short var7 = 9728;
        this.scene.anInt367 = 4100;
        this.scene.fogZDistance = 4000;
        this.scene.anInt317 = 4100;
        short var8 = 888;
        this.scene.fogZFalloff = 1;
        this.scene.setCamera(var7, var8, 0, (byte) 98, var6 * 2, -this.world.getElevation(var7, (byte) -56, var5), var5, 912);
        this.scene.endscene(false);
        this.surface.fade2black(-915682524);
        this.surface.fade2black(-915682524);
        this.surface.drawBox(0, 0, 512, 6, 0);

        for (int var9 = 6; var9 >= 1; --var9) {
            this.surface.method398(-122, 0, var9, var9, 0, 8, 512);
        }

        this.surface.drawBox(0, 194, 512, 20, 0);

        for (int var10 = 6; var10 >= 1; --var10) {
            this.surface.method398(22, 0, var10, 194 + -var10, 0, 8, 512);
        }

        this.surface.drawSprite((byte) 61, 15, this.spriteMedia - -10, 15);
        this.surface.drawSprite((byte) 107, 200, this.spriteLogo, 512, 0, 0);
        var8 = 888;
        var5 = 9216;
        var7 = 9216;
        var6 = 1100;
        this.surface.drawWorld(-257, this.spriteLogo);
        this.scene.fogZFalloff = 1;
        this.scene.fogZDistance = 4000;
        this.scene.anInt317 = 4100;
        this.scene.anInt367 = 4100;
        this.scene.setCamera(var7, var8, 0, (byte) 98, 2 * var6, -this.world.getElevation(var7, (byte) -56, var5), var5, 912);
        this.scene.endscene(false);
        this.surface.fade2black(-915682524);
        this.surface.fade2black(-915682524);
        this.surface.drawBox(0, 0, 512, 6, 0);

        for (int var11 = 6; var11 >= 1; --var11) {
            this.surface.method398(102, 0, var11, var11, 0, 8, 512);
        }

        this.surface.drawBox(0, 194, 512, 20, 0);

        for (int var12 = 6; var12 >= 1; --var12) {
            this.surface.method398(94, 0, var12, -var12 + 194, 0, 8, 512);
        }

        this.surface.drawSprite((byte) 61, 15, this.spriteMedia - -10, 15);
        this.surface.drawSprite((byte) -120, 200, 1 + this.spriteLogo, 512, 0, 0);
        this.surface.drawWorld(-257, this.spriteLogo + 1);
        var6 = 500;
        var5 = 10368;
        var8 = 376;
        var7 = 11136;

        for (int var13 = 0; var13 < 64; ++var13) {
            this.scene.freeModel((byte) 100, this.world.aGameModelArrayArray236[0][var13]);
            this.scene.freeModel((byte) 112, this.world.aGameModelArrayArray246[1][var13]);
            this.scene.freeModel((byte) 120, this.world.aGameModelArrayArray236[1][var13]);
            this.scene.freeModel((byte) 100, this.world.aGameModelArrayArray246[2][var13]);
            this.scene.freeModel((byte) 119, this.world.aGameModelArrayArray236[2][var13]);
        }

        this.scene.fogZFalloff = 1;
        this.scene.fogZDistance = 4000;
        this.scene.anInt367 = 4100;
        this.scene.anInt317 = 4100;
        this.scene.setCamera(var7, var8, 0, (byte) 98, var6 * 2, -this.world.getElevation(var7, (byte) -56, var5), var5, 912);
        this.scene.endscene(!var1);
        this.surface.fade2black(-915682524);
        this.surface.fade2black(-915682524);
        this.surface.drawBox(0, 0, 512, 6, 0);

        for (int var14 = 6; var14 >= 1; --var14) {
            this.surface.method398(-103, 0, var14, var14, 0, 8, 512);
        }

        this.surface.drawBox(0, 194, 512, 20, 0);

        for (int var15 = 6; var15 >= 1; --var15) {
            this.surface.method398(102, 0, var15, 194, 0, 8, 512);
        }

        this.surface.drawSprite((byte) 61, 15, 10 + this.spriteMedia, 15);
        this.surface.drawSprite((byte) 103, 200, this.spriteMedia - -10, 512, 0, 0);
        this.surface.drawWorld(-257, this.spriteMedia - -10);
    }

    private void sendLogout(int var1) {
        if (this.loggedIn != 0) {
            if (var1 == 2) {
                if (this.combatTimeout > 450) {
                    this.showMessage(0, "You can\'t logout during combat!", (String) null, (String) null, 0, "@cya@", false);
                } else if (this.combatTimeout > 0) {
                    this.showMessage(0, "You can\'t logout for 10 seconds after combat", (String) null, (String) null, 0, "@cya@", false);
                } else {
                    this.clientStream.newPacket(102);
                    this.clientStream.sendPacket();
                    this.logoutTimeout = 1000;
                }
            }
        }
    }

    private void drawDialogTradeConfirm(int var1) {
        byte var2 = 22;
        byte var3 = 36;
        this.surface.drawBox(var2, var3, 468, 16, 192);
        int var4 = 10000536;
        this.surface.drawBoxAlpha(var2, var3 - -16, 468, 246, var4, 160);
        this.surface.drawstringCenter("Please confirm your trade with @yel@" + this.tradeRecipientConfirmName, 1, var3 - -12, var2 - -234, 0xffffff);
        this.surface.drawstringCenter("You are about to give:", 1, var3 + 30, var2 - -117, 0xffff00);

        for (int var5 = 0; var5 < this.tradeConfirmItemsCount; ++var5) {
            String var6 = GameData.itemName[this.tradeConfirmItems[var5]];
            if (GameData.itemStackable[this.tradeConfirmItems[var5]] == 0) {
                var6 = var6 + " x " + Character.method363(this.tradeConfirmItemCount[var5], 0);
            }

            this.surface.drawstringCenter(var6, 1, 12 * var5 + 42 + var3, 117 + var2, 0xffffff);
        }

        if (this.tradeConfirmItemsCount == 0) {
            this.surface.drawstringCenter("Nothing!", 1, var3 + 42, var2 + 117, 0xffffff);
        }

        this.surface.drawstringCenter("In return you will receive:", 1, var3 + 30, 351 + var2, 0xffff00);

        for (int var9 = 0; this.tradeRecipientConfirmItemsCount > var9; ++var9) {
            String var7 = GameData.itemName[this.traceRecipientConfirmItems[var9]];
            if (GameData.itemStackable[this.traceRecipientConfirmItems[var9]] == 0) {
                var7 = var7 + " x " + Character.method363(this.traceRecipientConfirmItemCount[var9], 0);
            }

            this.surface.drawstringCenter(var7, 1, var9 * 12 + var3 + 42, var2 + 351, 0xffffff);
        }

        if (this.tradeRecipientConfirmItemsCount == 0) {
            this.surface.drawstringCenter("Nothing!", 1, 42 + var3, 351 + var2, 0xffffff);
        }

        this.surface.drawstringCenter("Are you sure you want to do this?", 4, var3 - -200, var2 - -234, '\uffff');
        this.surface.drawstringCenter("There is NO WAY to reverse a trade if you change your mind.", 1, var3 + 215, 234 + var2, 0xffffff);
        this.surface.drawstringCenter("Remember that not all players are trustworthy", 1, var3 - -230, var2 - -234, 0xffffff);
        if (!this.tradeConfirmAccepted) {
            this.surface.drawSprite((byte) 61, var3 - -238, this.spriteMedia - -25, -35 + (var2 - -118));
            this.surface.drawSprite((byte) 61, 238 + var3, 26 + this.spriteMedia, 352 + (var2 - 35));
        } else {
            this.surface.drawstringCenter("Waiting for other player...", 1, var3 - -250, 234 + var2, 0xffff00);
        }

        if (var1 != -28437) {
            this.createPlayer(-4, 88, -124, -45, -75);
        }

        if (this.mouseButtonClick == 1) {
            if (var2 > super.mouseX || super.mouseY < var3 || (468 + var2) < super.mouseX || super.mouseY > (var3 - -262)) {
                this.showDialogTradeConfirm = false;
                this.clientStream.newPacket(230);
                this.clientStream.sendPacket();
            }

            if ((-35 + var2 - -118) <= super.mouseX && super.mouseX <= 188 + var2 && super.mouseY >= (238 + var3) && super.mouseY <= 21 + var3 + 238) {
                this.tradeConfirmAccepted = true;
                this.clientStream.newPacket(104);
                this.clientStream.sendPacket();
            }

            if (super.mouseX >= -35 + var2 - -352 && (var2 + 423) >= super.mouseX && super.mouseY >= (238 + var3) && 21 + var3 + 238 >= super.mouseY) {
                this.showDialogTradeConfirm = false;
                this.clientStream.newPacket(230);
                this.clientStream.sendPacket();
            }

            this.mouseButtonClick = 0;
        }
        ;
    }

    private void drawDialogBank(int var1) {
        short var2 = 408;
        short var3 = 334;
        if (this.bankActivePage > 0 && this.bankItemCount <= 48) {
            this.bankActivePage = 0;
        }

        if (this.bankActivePage > 1 && this.bankItemCount <= 96) {
            this.bankActivePage = 1;
        }

        if (this.bankSelectedItemSlot >= this.bankItemCount || this.bankSelectedItemSlot < 0) {
            this.bankSelectedItemSlot = -1;
        }

        if (this.bankActivePage > 2 && this.bankItemCount <= 144) {
            this.bankActivePage = 2;
        }

        if (this.bankSelectedItemSlot != -1 && this.bankItems[this.bankSelectedItemSlot] != this.bankSelectedItem) {
            this.bankSelectedItemSlot = -1;
            this.bankSelectedItem = -2;
        }

        int var4;
        int var5;
        int var6;
        int xOff;
        int colour;
        int var9;
        int itemCount;
        if (this.inputPopupType == 0 && this.mouseButtonClick != 0) {
            this.mouseButtonClick = 0;
            int mouseX = var2 / 2 - 256 + super.mouseX;
            int mouseY = super.mouseY + var3 / 2 + -170;
            if (mouseX >= 0 && mouseY >= 12 && mouseX < 408 && mouseY < 280) {
                var6 = this.bankActivePage * 48;

                for (xOff = 0; xOff < 6; ++xOff) {
                    for (int i = 0; i < 8; ++i) {
                        var9 = 49 * i + 7;
                        itemCount = 28 + xOff * 34;
                        if (var9 < mouseX && mouseX < var9 - -49 && itemCount < mouseY && mouseY < (34 + itemCount) && this.bankItemCount > var6 && this.bankItems[var6] != -1) {
                            this.bankSelectedItem = this.bankItems[var6];
                            this.bankSelectedItemSlot = var6;
                        }

                        ++var6;
                    }
                }

                mouseX = 256 + -(var2 / 2);
                mouseY = 170 - var3 / 2;
                int slot;
                if (this.bankSelectedItemSlot >= 0) {
                    slot = this.bankItems[this.bankSelectedItemSlot];
                } else {
                    slot = -1;
                }

                if (slot != -1) {
                    int bankCount = this.bankItemsCount[this.bankSelectedItemSlot];
                    if (bankCount >= 1 && super.mouseX >= mouseX + 220 && mouseY - -238 <= super.mouseY && 250 + mouseX > super.mouseX && super.mouseY <= (249 + mouseY)) {
                        this.clientStream.newPacket(22);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(1);
                        this.clientStream.writeBuffer.putInt(0x12345678);
                        this.clientStream.sendPacket();
                    }

                    if (bankCount >= 5 && super.mouseX >= mouseX + 250 && super.mouseY >= 238 + mouseY && (mouseX + 280) > super.mouseX && (mouseY - -249) >= super.mouseY) {
                        this.clientStream.newPacket(22);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(5);
                        this.clientStream.writeBuffer.putInt(0x12345678);
                        this.clientStream.sendPacket();
                    }

                    if (bankCount >= 10 && super.mouseX >= (280 + mouseX) && (mouseY - -238) <= super.mouseY && super.mouseX < (mouseX - -305) && super.mouseY <= (mouseY - -249)) {
                        this.clientStream.newPacket(22);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(10);
                        this.clientStream.writeBuffer.putInt(0x12345678);
                        this.clientStream.sendPacket();
                    }

                    if (bankCount >= 50 && super.mouseX >= mouseX - -305 && super.mouseY >= 238 + mouseY && 335 + mouseX > super.mouseX && super.mouseY <= (mouseY - -249)) {
                        this.clientStream.newPacket(22);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(50);
                        this.clientStream.writeBuffer.putInt(0x12345678);
                        this.clientStream.sendPacket();
                    }

                    if (super.mouseX >= (335 + mouseX) && mouseY - -238 <= super.mouseY && super.mouseX < 368 + mouseX && super.mouseY <= 249 + mouseY) {
                        this.showInputPopup(3, aStringArray214, true);
                    }

                    if (super.mouseX >= (370 + mouseX) && super.mouseY >= (mouseY + 238) && 400 + mouseX > super.mouseX && super.mouseY <= 249 + mouseY) {
                        this.clientStream.newPacket(22);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(bankCount);
                        this.clientStream.writeBuffer.putInt(0x12345678);
                        this.clientStream.sendPacket();
                    }

                    if (this.getInventoryCount(slot) >= 1 && super.mouseX >= 220 + mouseX && super.mouseY >= mouseY + 263 && mouseX + 250 > super.mouseX && super.mouseY <= (274 + mouseY)) {
                        this.clientStream.newPacket(23);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(1);
                        this.clientStream.writeBuffer.putInt(-0x789abcdf);
                        this.clientStream.sendPacket();
                    }

                    if (this.getInventoryCount(slot) >= 5 && super.mouseX >= (250 + mouseX) && (263 + mouseY) <= super.mouseY && mouseX - -280 > super.mouseX && super.mouseY <= mouseY + 274) {
                        this.clientStream.newPacket(23);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(5);
                        this.clientStream.writeBuffer.putInt(-0x789abcdf);
                        this.clientStream.sendPacket();
                    }

                    if (this.getInventoryCount(slot) >= 10 && super.mouseX >= 280 + mouseX && (mouseY - -263) <= super.mouseY && (305 + mouseX) > super.mouseX && (mouseY + 274) >= super.mouseY) {
                        this.clientStream.newPacket(23);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(10);
                        this.clientStream.writeBuffer.putInt(-0x789abcdf);
                        this.clientStream.sendPacket();
                    }

                    if (this.getInventoryCount(slot) >= 50 && super.mouseX >= mouseX + 305 && super.mouseY >= 263 + mouseY && super.mouseX < (mouseX - -335) && (mouseY - -274) >= super.mouseY) {
                        this.clientStream.newPacket(23);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(50);
                        this.clientStream.writeBuffer.putInt(-0x789abcdf);
                        this.clientStream.sendPacket();
                    }

                    if (mouseX + 335 <= super.mouseX && mouseY - -263 <= super.mouseY && 368 + mouseX > super.mouseX && super.mouseY <= mouseY + 274) {
                        this.showInputPopup(4, aStringArray1034, true);
                    }

                    if (super.mouseX >= (370 + mouseX) && mouseY + 263 <= super.mouseY && mouseX + 400 > super.mouseX && mouseY + 274 >= super.mouseY) {
                        this.clientStream.newPacket(23);
                        this.clientStream.writeBuffer.putShort(slot);
                        this.clientStream.writeBuffer.putInt(this.getInventoryCount(slot));
                        this.clientStream.writeBuffer.putInt(-0x789abcdf);
                        this.clientStream.sendPacket();
                    }
                }
            } else if (this.bankItemCount > 48 && mouseX >= 50 && mouseX <= 115 && mouseY <= 12) {
                this.bankActivePage = 0;
            } else if (this.bankItemCount > 48 && mouseX >= 115 && mouseX <= 180 && mouseY <= 12) {
                this.bankActivePage = 1;
            } else if (this.bankItemCount > 96 && mouseX >= 180 && mouseX <= 245 && mouseY <= 12) {
                this.bankActivePage = 2;
            } else {
                if (this.bankItemCount <= 144 || mouseX < 245 || mouseX > 310 || mouseY > 12) {
                    this.clientStream.newPacket(212);
                    this.clientStream.sendPacket();
                    this.showDialogBank = false;
                    return;
                }

                this.bankActivePage = 3;
            }
        }

        var4 = 256 - var2 / 2;
        var5 = -(var3 / 2) + 170;
        this.surface.drawBox(var4, var5, 408, 12, 192);
        var6 = 10000536;
        this.surface.drawBoxAlpha(var4, var5 - -12, 408, 17, var6, 160);
        this.surface.drawBoxAlpha(var4, var5 + 29, 8, 204, var6, 160);
        this.surface.drawBoxAlpha(var4 + 399, var5 - -29, 9, 204, var6, 160);
        this.surface.drawBoxAlpha(var4, var5 + 233, 408, 47, var6, 160);
        this.surface.drawstring("Bank", 1 + var4, 10 + var5, 1, 0xffffff);
        xOff = 50;
        if (this.bankItemCount > 48) {
            colour = 0xffffff;
            if (this.bankActivePage != 0) {
                if (xOff + var4 < super.mouseX && var5 <= super.mouseY && var4 + (xOff - -65) > super.mouseX && (var5 - -12) > super.mouseY) {
                    colour = 0xffff00;
                }
            } else {
                colour = 0xff0000;
            }

            this.surface.drawstring("<page 1>", var4 - -xOff, var5 - -10, 1, colour);
            xOff += 65;
            colour = 0xffffff;
            if (this.bankActivePage != 1) {
                if (super.mouseX > var4 + xOff && super.mouseY >= var5 && 65 + xOff + var4 > super.mouseX && 12 + var5 > super.mouseY) {
                    colour = 0xffff00;
                }
            } else {
                colour = 0xff0000;
            }

            this.surface.drawstring("<page 2>", var4 - -xOff, var5 - -10, 1, colour);
            xOff += 65;
        }

        if (this.bankItemCount > 96) {
            colour = 0xffffff;
            if (this.bankActivePage == 2) {
                colour = 0xff0000;
            } else if ((var4 + xOff) < super.mouseX && var5 <= super.mouseY && super.mouseX < (xOff + var4 + 65) && var5 - -12 > super.mouseY) {
                colour = 0xffff00;
            }

            this.surface.drawstring("<page 3>", var4 + xOff, var5 - -10, 1, colour);
            xOff += 65;
        }

        if (this.bankItemCount > 144) {
            colour = 0xffffff;
            if (this.bankActivePage != 3) {
                if (super.mouseX > (xOff + var4) && super.mouseY >= var5 && super.mouseX < (xOff + var4 + 65) && (12 + var5) > super.mouseY) {
                    colour = 0xffff00;
                }
            } else {
                colour = 0xff0000;
            }

            this.surface.drawstring("<page 4>", xOff + var4, 10 + var5, 1, colour);
            xOff += 65;
        }

        colour = 0xffffff;
        if (super.mouseX > var4 + 320 && super.mouseY >= var5 && (var4 - -408) > super.mouseX && 12 + var5 > super.mouseY) {
            colour = 0xff0000;
        }

        this.surface.drawstringRight(var5 - -10, colour, 406 + var4, 1, "Close window", -5169);
        this.surface.drawstring("Number in bank in green", 7 + var4, var5 - -24, 1, '\uff00');
        this.surface.drawstring("Number held in blue", var4 - -289, 24 + var5, 1, '\uffff');
        var9 = 13684944;
        if (var1 != -19428) {
            this.mouseButtonClick = -108;
        }

        itemCount = this.bankActivePage * 48;

        int item;
        for (int var11 = 0; var11 < 6; ++var11) {
            for (item = 0; item < 8; ++item) {
                int var13 = 49 * item + 7 + var4;
                int var14 = 34 * var11 + (var5 - -28);
                if (this.bankSelectedItemSlot != itemCount) {
                    this.surface.drawBoxAlpha(var13, var14, 49, 34, var9, 160);
                } else {
                    this.surface.drawBoxAlpha(var13, var14, 49, 34, 0xff0000, 160);
                }

                this.surface.drawBoxEdge(var13, var14, 50, 35, 0);
                if (this.bankItemCount > itemCount && this.bankItems[itemCount] != -1) {
                    this.surface.method409(GameData.itemMask[this.bankItems[itemCount]], this.spriteItem + GameData.itemPicture[this.bankItems[itemCount]], var14, var1 + 19308, 32, 0, 48, var13, false, 0);
                    this.surface.drawstring(String.valueOf(this.bankItemsCount[itemCount]), var13 + 1, 10 + var14, 1, '\uff00');
                    this.surface.drawstringRight(var14 + 29, '\uffff', var13 - -47, 1, String.valueOf(this.getInventoryCount(this.bankItems[itemCount])), -5169);
                }

                ++itemCount;
            }
        }

        this.surface.drawLineHoriz(var4 + 5, var5 - -256, 398, 0);
        if (this.bankSelectedItemSlot != -1) {
            if (this.bankSelectedItemSlot >= 0) {
                item = this.bankItems[this.bankSelectedItemSlot];
            } else {
                item = -1;
            }

            if (item != -1) {
                itemCount = this.bankItemsCount[this.bankSelectedItemSlot];
                if (GameData.itemStackable[item] == 1 && itemCount > 1) {
                    itemCount = 1;
                }

                if (itemCount > 0) {
                    this.surface.drawstring("Withdraw " + GameData.itemName[item], var4 + 2, 248 + var5, 1, 0xffffff);
                    colour = 0xffffff;
                    if (super.mouseX >= var4 + 220 && super.mouseY >= (238 + var5) && super.mouseX < (var4 + 250) && super.mouseY <= 249 + var5) {
                        colour = 0xff0000;
                    }

                    this.surface.drawstring("One", 222 + var4, var5 - -248, 1, colour);
                    if (itemCount >= 5) {
                        colour = 0xffffff;
                        if (250 + var4 <= super.mouseX && super.mouseY >= 238 + var5 && 280 + var4 > super.mouseX && super.mouseY <= (var5 - -249)) {
                            colour = 0xff0000;
                        }

                        this.surface.drawstring("Five", var4 - -252, var5 + 248, 1, colour);
                    }

                    if (itemCount >= 10) {
                        colour = 0xffffff;
                        if (super.mouseX >= 280 + var4 && super.mouseY >= (var5 + 238) && (var4 - -305) > super.mouseX && super.mouseY <= (var5 - -249)) {
                            colour = 0xff0000;
                        }

                        this.surface.drawstring("10", 282 + var4, var5 + 248, 1, colour);
                    }

                    if (itemCount >= 50) {
                        colour = 0xffffff;
                        if (305 + var4 <= super.mouseX && super.mouseY >= 238 + var5 && (var4 + 335) > super.mouseX && (var5 - -249) >= super.mouseY) {
                            colour = 0xff0000;
                        }

                        this.surface.drawstring("50", var4 - -307, var5 + 248, 1, colour);
                    }

                    colour = 0xffffff;
                    if ((var4 + 335) <= super.mouseX && 238 + var5 <= super.mouseY && 368 + var4 > super.mouseX && super.mouseY <= (249 + var5)) {
                        colour = 0xff0000;
                    }

                    this.surface.drawstring("X", var4 + 337, 248 + var5, 1, colour);
                    colour = 0xffffff;
                    if (super.mouseX >= var4 - -370 && (238 + var5) <= super.mouseY && super.mouseX < (var4 + 400) && super.mouseY <= (249 + var5)) {
                        colour = 0xff0000;
                    }

                    this.surface.drawstring("All", 370 + var4, 248 + var5, 1, colour);
                }

                if (this.getInventoryCount(item) > 0) {
                    this.surface.drawstring("Deposit " + GameData.itemName[item], 2 + var4, var5 - -273, 1, 0xffffff);
                    colour = 0xffffff;
                    if ((var4 + 220) <= super.mouseX && super.mouseY >= var5 + 263 && super.mouseX < (250 + var4) && (274 + var5) >= super.mouseY) {
                        colour = 0xff0000;
                    }

                    this.surface.drawstring("One", var4 + 222, 273 + var5, 1, colour);
                    if (this.getInventoryCount(item) >= 5) {
                        colour = 0xffffff;
                        if (super.mouseX >= (250 + var4) && super.mouseY >= (var5 + 263) && super.mouseX < (280 + var4) && (var5 + 274) >= super.mouseY) {
                            colour = 0xff0000;
                        }

                        this.surface.drawstring("Five", 252 + var4, var5 + 273, 1, colour);
                    }

                    if (this.getInventoryCount(item) >= 10) {
                        colour = 0xffffff;
                        if (super.mouseX >= var4 + 280 && super.mouseY >= (var5 - -263) && super.mouseX < 305 + var4 && (var5 + 274) >= super.mouseY) {
                            colour = 0xff0000;
                        }

                        this.surface.drawstring("10", 282 + var4, var5 - -273, 1, colour);
                    }

                    if (this.getInventoryCount(item) >= 50) {
                        colour = 0xffffff;
                        if ((305 + var4) <= super.mouseX && var5 + 263 <= super.mouseY && (var4 - -335) > super.mouseX && var5 - -274 >= super.mouseY) {
                            colour = 0xff0000;
                        }

                        this.surface.drawstring("50", var4 + 307, 273 + var5, 1, colour);
                    }

                    colour = 0xffffff;
                    if ((335 + var4) <= super.mouseX && var5 + 263 <= super.mouseY && super.mouseX < 368 + var4 && var5 - -274 >= super.mouseY) {
                        colour = 0xff0000;
                    }

                    this.surface.drawstring("X", var4 + 337, var5 - -273, 1, colour);
                    colour = 0xffffff;
                    if (370 + var4 <= super.mouseX && (var5 + 263) <= super.mouseY && super.mouseX < var4 + 400 && var5 - -274 >= super.mouseY) {
                        colour = 0xff0000;
                    }

                    this.surface.drawstring("All", var4 - -370, var5 + 273, 1, colour);
                    return;
                }
            }
        } else {
            this.surface.drawstringCenter("Select an object to withdraw or deposit", 3, 248 + var5, 204 + var4, 0xffff00);
        }
        ;
    }

    private void showLoginScreenStatus(String var1, String var2, int var3) {
        if (this.anInt1617 == 2) {
            if (var1 != null && var1.length() >= 1) {
                this.paneLogin.updateText(this.controlLoginStatus1, var2, 121);
                this.paneLogin.updateText(this.controlLoginStatus2, var1, 49);
            } else {
                this.paneLogin.updateText(this.controlLoginStatus2, var2, -91);
            }
        }

        int var4 = -40 / ((-35 - var3) / 36);
        this.method48(64);
        this.resetTimings(-10);
    }

    private void drawDialogDuel(int var1) {
        int var2 = -1;
        if (this.mouseButtonClick != 0 && this.aBoolean1586) {
            var2 = this.menuDuel.drawNoBg(this.anInt1608, this.anInt1563, super.mouseX, super.mouseY);
        }

        int var3;
        int var4;
        int var5;
        int var6;
        int var7;
        int var8;
        int var9;
        int var10;
        if (var2 >= 0) {
            this.mouseButtonClick = 0;
            this.aBoolean1586 = false;
            var3 = this.menuDuel.getItemMenuIndex(var2);
            var4 = this.menuDuel.getMenuItemX(var2);
            var5 = -1;
            var6 = 0;
            if (var3 != 3) {
                for (var7 = 0; this.duelOfferItemCount > var7; ++var7) {
                    if (this.duelOfferItemId[var7] == var4) {
                        if (var5 < 0) {
                            var5 = var7;
                        }

                        if (GameData.itemStackable[var4] == 0) {
                            var6 = this.duelOfferItemStack[var7];
                            break;
                        }

                        ++var6;
                    }
                }
            } else {
                for (var7 = 0; var7 < this.inventoryItemsCount; ++var7) {
                    if (this.inventoryItemId[var7] == var4) {
                        if (var5 < 0) {
                            var5 = var7;
                        }

                        if (GameData.itemStackable[var4] == 0) {
                            var6 = this.inventoryItemStackCount[var7];
                            break;
                        }

                        ++var6;
                    }
                }
            }

            if (var5 >= 0) {
                var7 = this.menuDuel.getMenuItemY(var2);
                if (var7 != -2) {
                    if (var7 == -1) {
                        var7 = var6;
                    }

                    if (var3 != 3) {
                        this.updateDuelItems(var5, 5, var7);
                    } else {
                        this.drawDialogDuel(var7, var5);
                    }
                } else {
                    this.anInt1460 = var5;
                    if (var3 == 3) {
                        this.showInputPopup(7, GameFrame.aStringArray103, true);
                    } else {
                        this.showInputPopup(8, aStringArray544, true);
                    }
                }
            }
        } else if (this.inputPopupType == 0) {
            if (this.mouseButtonClick == 1 && this.mouseButtonItemCountIncrement == 0) {
                this.mouseButtonItemCountIncrement = 1;
            }

            var3 = super.mouseX + -22;
            var4 = super.mouseY + -36;
            if (var3 >= 0 && var4 >= 0 && var3 < 468 && var4 < 262) {
                if (this.mouseButtonItemCountIncrement > 0) {
                    if (var3 > 216 && var4 > 30 && var3 < 462 && var4 < 235) {
                        var5 = (var4 + -31) / 34 * 5 + (-217 + var3) / 49;
                        if (var5 >= 0 && var5 < this.inventoryItemsCount) {
                            this.drawDialogDuel(-1, var5);
                        }
                    }

                    if (var3 > 8 && var4 > 30 && var3 < 205 && var4 < 129) {
                        var5 = (var4 + -31) / 34 * 4 + (var3 - 9) / 49;
                        if (var5 >= 0 && var5 < this.duelOfferItemCount) {
                            this.updateDuelItems(var5, 5, -1);
                        }
                    }

                    boolean var20 = false;
                    if (var3 >= 93 && var4 >= 221 && var3 <= 104 && var4 <= 232) {
                        this.duelSettingsRetreat = !this.duelSettingsRetreat;
                        var20 = true;
                    }

                    if (var3 >= 93 && var4 >= 240 && var3 <= 104 && var4 <= 251) {
                        this.duelSettingsMagic = !this.duelSettingsMagic;
                        var20 = true;
                    }

                    if (var3 >= 191 && var4 >= 221 && var3 <= 202 && var4 <= 232) {
                        this.duelSettingsPrayer = !this.duelSettingsPrayer;
                        var20 = true;
                    }

                    if (var3 >= 191 && var4 >= 240 && var3 <= 202 && var4 <= 251) {
                        var20 = true;
                        this.duelSettingsWeapons = !this.duelSettingsWeapons;
                    }

                    if (var20) {
                        this.clientStream.newPacket(8);
                        this.clientStream.writeBuffer.putByte(!this.duelSettingsRetreat ? 0 : 1);
                        this.clientStream.writeBuffer.putByte(this.duelSettingsMagic ? 1 : 0);
                        this.clientStream.writeBuffer.putByte(!this.duelSettingsPrayer ? 0 : 1);
                        this.clientStream.writeBuffer.putByte(!this.duelSettingsWeapons ? 0 : 1);
                        this.clientStream.sendPacket();
                        this.duelOfferOpponentAccepted = false;
                        this.duelOfferAccepted = false;
                    }

                    if (var3 >= 217 && var4 >= 238 && var3 <= 286 && var4 <= 259) {
                        this.duelOfferAccepted = true;
                        this.clientStream.newPacket(176);
                        this.clientStream.sendPacket();
                    }

                    if (var3 >= 394 && var4 >= 238 && var3 < 463 && var4 < 259) {
                        this.showDialogDuel = false;
                        this.clientStream.newPacket(197);
                        this.clientStream.sendPacket();
                    }

                    this.mouseButtonItemCountIncrement = 0;
                    this.mouseButtonClick = 0;
                }

                if (this.mouseButtonClick == 2) {
                    if (var3 > 216 && var4 > 30 && var3 < 462 && var4 < 235) {
                        var5 = this.menuCommon.getWidth();
                        var6 = this.menuCommon.getHeight();
                        this.anInt1379 = super.mouseX - var5 / 2;
                        this.showRightClickMenu = true;
                        this.anInt1625 = super.mouseY + -7;
                        if (this.anInt1625 < 0) {
                            this.anInt1625 = 0;
                        }

                        if (this.anInt1379 < 0) {
                            this.anInt1379 = 0;
                        }

                        if ((var6 + this.anInt1625) > 315) {
                            this.anInt1625 = 315 + -var6;
                        }

                        if (var5 + this.anInt1379 > 510) {
                            this.anInt1379 = -var5 + 510;
                        }

                        var7 = 5 * ((-31 + var4) / 34) + (var3 + -217) / 49;
                        if (var7 >= 0 && var7 < this.inventoryItemsCount) {
                            var8 = this.inventoryItemId[var7];
                            this.aBoolean1586 = true;
                            this.menuDuel.recalculateSize();
                            this.menuDuel.addItem(1, 3, "Stake 1", "@lre@" + GameData.itemName[var8], var8);
                            this.menuDuel.addItem(5, 3, "Stake 5", "@lre@" + GameData.itemName[var8], var8);
                            this.menuDuel.addItem(10, 3, "Stake 10", "@lre@" + GameData.itemName[var8], var8);
                            this.menuDuel.addItem(-1, 3, "Stake All", "@lre@" + GameData.itemName[var8], var8);
                            this.menuDuel.addItem(-2, 3, "Stake X", "@lre@" + GameData.itemName[var8], var8);
                            var9 = this.menuDuel.getWidth();
                            var10 = this.menuDuel.getHeight();
                            this.anInt1608 = -7 + super.mouseY;
                            this.anInt1563 = super.mouseX - var9 / 2;
                            if (this.anInt1563 < 0) {
                                this.anInt1563 = 0;
                            }

                            if (this.anInt1608 < 0) {
                                this.anInt1608 = 0;
                            }

                            if ((var9 + this.anInt1563) > 510) {
                                this.anInt1563 = -var9 + 510;
                            }

                            if (var10 + this.anInt1608 > 315) {
                                this.anInt1608 = -var10 + 315;
                            }
                        }
                    }

                    if (var3 > 8 && var4 > 30 && var3 < 205 && var4 < 133) {
                        var5 = (-31 + var4) / 34 * 4 + (-9 + var3) / 49;
                        if (var5 >= 0 && var5 < this.duelOfferItemCount) {
                            var6 = this.duelOfferItemId[var5];
                            this.aBoolean1586 = true;
                            this.menuDuel.recalculateSize();
                            this.menuDuel.addItem(1, 4, "Remove 1", "@lre@" + GameData.itemName[var6], var6);
                            this.menuDuel.addItem(5, 4, "Remove 5", "@lre@" + GameData.itemName[var6], var6);
                            this.menuDuel.addItem(10, 4, "Remove 10", "@lre@" + GameData.itemName[var6], var6);
                            this.menuDuel.addItem(-1, 4, "Remove All", "@lre@" + GameData.itemName[var6], var6);
                            this.menuDuel.addItem(-2, 4, "Remove X", "@lre@" + GameData.itemName[var6], var6);
                            var7 = this.menuDuel.getWidth();
                            var8 = this.menuDuel.getHeight();
                            this.anInt1563 = super.mouseX - var7 / 2;
                            this.anInt1608 = -7 + super.mouseY;
                            if (this.anInt1563 < 0) {
                                this.anInt1563 = 0;
                            }

                            if (this.anInt1608 < 0) {
                                this.anInt1608 = 0;
                            }

                            if (this.anInt1608 + var8 > 315) {
                                this.anInt1608 = -var8 + 315;
                            }

                            if (this.anInt1563 - -var7 > 510) {
                                this.anInt1563 = 510 - var7;
                            }
                        }
                    }

                    this.mouseButtonClick = 0;
                }

                if (this.aBoolean1586) {
                    var5 = this.menuDuel.getWidth();
                    var6 = this.menuDuel.getHeight();
                    if (super.mouseX < (this.anInt1563 + -10) || (this.anInt1608 + -10) > super.mouseY || (10 + this.anInt1563 + var5) < super.mouseX || var6 + this.anInt1608 + 10 < super.mouseY) {
                        this.aBoolean1586 = false;
                    }
                }
            } else if (this.mouseButtonClick != 0) {
                this.showDialogDuel = false;
                this.clientStream.newPacket(197);
                this.clientStream.sendPacket();
            }
        }

        if (this.showDialogDuel) {
            byte var18 = 22;
            byte var19 = 36;
            this.surface.drawBox(var18, var19, 468, 12, 13175581);
            var5 = 10000536;
            this.surface.drawBoxAlpha(var18, var19 + 12, 468, 18, var5, 160);
            this.surface.drawBoxAlpha(var18, 30 + var19, 8, 248, var5, 160);
            this.surface.drawBoxAlpha(205 + var18, 30 + var19, 11, 248, var5, 160);
            this.surface.drawBoxAlpha(var18 + 462, var19 - -30, 6, 248, var5, 160);
            this.surface.drawBoxAlpha(8 + var18, 99 + var19, 197, 24, var5, 160);
            this.surface.drawBoxAlpha(8 + var18, var19 - -192, 197, 23, var5, 160);
            this.surface.drawBoxAlpha(8 + var18, var19 + 258, 197, 20, var5, 160);
            this.surface.drawBoxAlpha(216 + var18, var19 + 235, 246, 43, var5, 160);
            var6 = 13684944;
            this.surface.drawBoxAlpha(var18 - -8, 30 + var19, 197, 69, var6, 160);
            this.surface.drawBoxAlpha(var18 - -8, var19 - -123, 197, 69, var6, 160);
            this.surface.drawBoxAlpha(8 + var18, var19 - -215, 197, 43, var6, 160);
            this.surface.drawBoxAlpha(216 + var18, var19 - -30, 246, 205, var6, 160);

            for (var7 = 0; var7 < 3; ++var7) {
                this.surface.drawLineHoriz(8 + var18, 30 + var19 - -(34 * var7), 197, 0);
            }

            for (var8 = 0; var8 < 3; ++var8) {
                this.surface.drawLineHoriz(8 + var18, 34 * var8 + (var19 - -123), 197, 0);
            }

            for (var9 = 0; var9 < 7; ++var9) {
                this.surface.drawLineHoriz(var18 + 216, 30 + var19 + 34 * var9, 246, 0);
            }

            for (var10 = 0; var10 < 6; ++var10) {
                if (var10 < 5) {
                    this.surface.drawLineVert(var18 - (-8 - var10 * 49), var19 + 30, 69, 0);
                }

                if (var10 < 5) {
                    this.surface.drawLineVert(49 * var10 + var18 + 8, var19 + 123, 69, 0);
                }

                this.surface.drawLineVert(var18 - (-216 - 49 * var10), var19 - -30, 205, 0);
            }

            this.surface.drawLineHoriz(var18 - -8, var19 - -215, 197, 0);
            this.surface.drawLineHoriz(8 + var18, var19 - -257, 197, 0);
            this.surface.drawLineVert(var18 + 8, var19 + 215, 43, 0);
            this.surface.drawLineVert(204 + var18, 215 + var19, 43, 0);
            this.surface.drawstring("Preparing to duel with: " + this.duelConfirmOpponentName, 1 + var18, 10 + var19, 1, 0xffffff);
            this.surface.drawstring("Your Stake", var18 - -9, 27 + var19, 4, 0xffffff);
            this.surface.drawstring("Opponent\'s Stake", var18 - -9, var19 + 120, 4, 0xffffff);
            this.surface.drawstring("Duel Options", var18 - -9, var19 - -212, 4, 0xffffff);
            this.surface.drawstring("Your Inventory", var18 + 216, var19 - -27, 4, 0xffffff);
            this.surface.drawstring("No retreating", 1 + 8 + var18, 16 + 215 + var19, 3, 0xffff00);
            this.surface.drawstring("No magic", 1 + 8 + var18, 215 + (var19 - -35), 3, 0xffff00);
            this.surface.drawstring("No prayer", 102 + 8 + var18, 16 + 215 + var19, 3, 0xffff00);
            this.surface.drawstring("No weapons", 110 + var18, 215 + var19 + 35, 3, 0xffff00);
            this.surface.drawBoxEdge(93 + var18, 221 + var19, 11, 11, 0xffff00);
            if (this.duelSettingsRetreat) {
                this.surface.drawBox(95 + var18, 8 + 215 + var19, 7, 7, 0xffff00);
            }

            this.surface.drawBoxEdge(var18 - -93, 215 + var19 + 25, 11, 11, 0xffff00);
            if (this.duelSettingsMagic) {
                this.surface.drawBox(95 + var18, 27 + 215 + var19, 7, 7, 0xffff00);
            }

            this.surface.drawBoxEdge(191 + var18, var19 + 215 - -6, 11, 11, 0xffff00);
            if (this.duelSettingsPrayer) {
                this.surface.drawBox(var18 + 193, var19 + 215 + 8, 7, 7, 0xffff00);
            }

            this.surface.drawBoxEdge(191 + var18, 25 + (var19 - -215), 11, 11, 0xffff00);
            if (this.duelSettingsWeapons) {
                this.surface.drawBox(193 + var18, 242 + var19, 7, 7, 0xffff00);
            }

            if (!this.duelOfferAccepted) {
                this.surface.drawSprite((byte) 61, var19 - -238, 25 + this.spriteMedia, 217 + var18);
            }

            this.surface.drawSprite((byte) 61, var19 - -238, this.spriteMedia - -26, 394 + var18);
            if (this.duelOfferOpponentAccepted) {
                this.surface.drawstringCenter("Other player", 1, 246 + var19, 341 + var18, 0xffffff);
                this.surface.drawstringCenter("has accepted", 1, var19 - -256, 341 + var18, 0xffffff);
            }

            if (this.duelOfferAccepted) {
                this.surface.drawstringCenter("Waiting for", 1, 246 + var19, 217 + var18 + 35, 0xffffff);
                this.surface.drawstringCenter("other player", 1, var19 + 256, 35 + var18 + 217, 0xffffff);
            }

            int var12;
            int var13;
            for (int var11 = 0; this.inventoryItemsCount > var11; ++var11) {
                var12 = var18 + 217 + 49 * (var11 % 5);
                var13 = 34 * (var11 / 5) + 31 + var19;
                this.surface.method409(GameData.itemMask[this.inventoryItemId[var11]], this.spriteItem + GameData.itemPicture[this.inventoryItemId[var11]], var13, -90, 32, 0, 48, var12, false, 0);
                if (GameData.itemStackable[this.inventoryItemId[var11]] == 0) {
                    this.surface.drawstring(String.valueOf(this.inventoryItemStackCount[var11]), var12 + 1, 10 + var13, 1, 0xffff00);
                }
            }

            int var14;
            for (var12 = 0; var12 < this.duelOfferItemCount; ++var12) {
                var13 = 9 + var18 - -(var12 % 4 * 49);
                var14 = var19 + 31 + 34 * (var12 / 4);
                this.surface.method409(GameData.itemMask[this.duelOfferItemId[var12]], this.spriteItem + GameData.itemPicture[this.duelOfferItemId[var12]], var14, -113, 32, 0, 48, var13, false, 0);
                if (GameData.itemStackable[this.duelOfferItemId[var12]] == 0) {
                    this.surface.drawstring(String.valueOf(this.duelOfferItemStack[var12]), 1 + var13, var14 - -10, 1, 0xffff00);
                }

                if (super.mouseX > var13 && super.mouseX < 48 + var13 && super.mouseY > var14 && super.mouseY < var14 - -32) {
                    this.surface.drawstring(GameData.itemName[this.duelOfferItemId[var12]] + ": @whi@" + GameData.itemDescription[this.duelOfferItemId[var12]], 8 + var18, var19 + 273, 1, 0xffff00);
                }
            }

            var14 = -40 / ((var1 - -19) / 47);

            for (var13 = 0; this.duelOfferOpponentItemCount > var13; ++var13) {
                int var15 = var18 + (9 - -(49 * (var13 % 4)));
                int var16 = var13 / 4 * 34 + 124 + var19;
                this.surface.method409(GameData.itemMask[this.duelOpponentItemId[var13]], GameData.itemPicture[this.duelOpponentItemId[var13]] + this.spriteItem, var16, -92, 32, 0, 48, var15, false, 0);
                if (GameData.itemStackable[this.duelOpponentItemId[var13]] == 0) {
                    this.surface.drawstring(String.valueOf(this.duelOfferOpponentItemStack[var13]), var15 + 1, var16 - -10, 1, 0xffff00);
                }

                if (var15 < super.mouseX && super.mouseX < 48 + var15 && var16 < super.mouseY && var16 - -32 > super.mouseY) {
                    this.surface.drawstring(GameData.itemName[this.duelOpponentItemId[var13]] + ": @whi@" + GameData.itemDescription[this.duelOpponentItemId[var13]], var18 - -8, 273 + var19, 1, 0xffff00);
                }
            }

            if (this.aBoolean1586) {
                this.menuDuel.draw(this.anInt1608, super.mouseX, this.anInt1563, super.mouseY);
            }
        }
    }

    private void drawTextBox(String var1, boolean var2, String var3) {
        if (!var2) {
            this.loadGameConfig(true);
        }

        Graphics var4 = this.getGraphics();
        if (var4 != null) {
            var4.translate(super.anInt58, super.anInt7);
            Font var5 = new Font("Helvetica", 1, 15);
            short var6 = 512;
            short var7 = 344;
            var4.setColor(Color.black);
            var4.fillRect(var6 / 2 + -140, var7 / 2 - 25, 280, 50);
            var4.setColor(Color.white);
            var4.drawRect(-140 + var6 / 2, var7 / 2 + -25, 280, 50);
            this.method9(var6 / 2, -10 + var7 / 2, 5061, var4, var5, var3);
            this.method9(var6 / 2, 10 + var7 / 2, 5061, var4, var5, var1);
        }
        ;
    }

    private void updateDuelItems(int var1, int var2, int var3) {
        int var4 = this.duelOfferItemId[var1];
        int var5 = var3 >= 0 ? var3 : this.mouseButtonItemCountIncrement;
        int var6;
        if (GameData.itemStackable[var4] != 0) {
            var6 = 0;

            for (int var7 = 0; this.duelOfferItemCount > var7 && var5 > var6; ++var7) {
                if (this.duelOfferItemId[var7] == var4) {
                    ++var6;
                    --this.duelOfferItemCount;

                    for (int var8 = var7; this.duelOfferItemCount > var8; ++var8) {
                        this.duelOfferItemId[var8] = this.duelOfferItemId[1 + var8];
                        this.duelOfferItemStack[var8] = this.duelOfferItemStack[var8 - -1];
                    }

                    --var7;
                }
            }
        } else {
            this.duelOfferItemStack[var1] -= var5;
            if (this.duelOfferItemStack[var1] <= 0) {
                --this.duelOfferItemCount;

                for (var6 = var1; this.duelOfferItemCount > var6; ++var6) {
                    this.duelOfferItemId[var6] = this.duelOfferItemId[1 + var6];
                    this.duelOfferItemStack[var6] = this.duelOfferItemStack[1 + var6];
                }
            }
        }

        this.clientStream.newPacket(33);
        this.clientStream.writeBuffer.putByte(this.duelOfferItemCount);

        for (var6 = 0; this.duelOfferItemCount > var6; ++var6) {
            this.clientStream.writeBuffer.putShort(this.duelOfferItemId[var6]);
            this.clientStream.writeBuffer.putInt(this.duelOfferItemStack[var6]);
        }

        this.clientStream.sendPacket();
        this.duelOfferOpponentAccepted = false;
        if (var2 == 5) {
            this.duelOfferAccepted = false;
        }
        ;
    }

    private void loadMaps(byte var1) {
        this.world.mapPack = this.readDataFile(4, 70, "map", var1 ^ 50);
        if (var1 != -60) {
            this.drawDialogReportAbuse((byte) -81);
        }

        if (this.members) {
            this.world.memberMapPack = this.readDataFile(5, 75, "members map", -10);
        }

        this.world.landscapePack = this.readDataFile(6, 80, "landscape", -10);
        if (this.members) {
            this.world.memberLandscapePack = this.readDataFile(7, 85, "members landscape", var1 ^ 50);
        }
    }

    private void drawAppearancePanelCharacterSprites(byte var1) {
        this.surface.interlace = false;
        this.surface.blackScreen((byte) 127);
        this.panelAppearance.drawPanel(66);
        short var2 = 140;
        byte var3 = 50;
        int var5 = var2 + 116;
        int var6 = var3 - 25;
        this.surface.method395(this.anIntArray1592[this.anInt1573], 64, GameData.animationNumber[this.anInt1611], 1536, 102, var6, var5 + -87);
        this.surface.method409(this.anIntArray1592[this.anInt1555], GameData.animationNumber[this.anInt1447], var6, -90, 102, this.anIntArray1548[this.anInt1423], 64, -55 + var5 - 32, false, 0);
        this.surface.method409(this.anIntArray1465[this.anInt1407], GameData.animationNumber[this.appearanceHeadType], var6, var1 ^ 74, 102, this.anIntArray1548[this.anInt1423], 64, -32 + (var5 - 55), false, 0);
        this.surface.method395(this.anIntArray1592[this.anInt1573], 64, GameData.animationNumber[this.anInt1611] - -6, 1536, 102, var6, -32 + var5);
        this.surface.method409(this.anIntArray1592[this.anInt1555], 6 + GameData.animationNumber[this.anInt1447], var6, -60, 102, this.anIntArray1548[this.anInt1423], 64, -32 + var5, false, 0);
        this.surface.method409(this.anIntArray1465[this.anInt1407], 6 + GameData.animationNumber[this.appearanceHeadType], var6, -62, 102, this.anIntArray1548[this.anInt1423], 64, -32 + var5, false, 0);
        this.surface.method395(this.anIntArray1592[this.anInt1573], 64, 12 + GameData.animationNumber[this.anInt1611], 1536, 102, var6, 55 + (var5 - 32));
        this.surface.method409(this.anIntArray1592[this.anInt1555], GameData.animationNumber[this.anInt1447] + 12, var6, var1 ^ 63, 102, this.anIntArray1548[this.anInt1423], 64, var5 + 23, false, 0);
        this.surface.method409(this.anIntArray1465[this.anInt1407], 12 + GameData.animationNumber[this.appearanceHeadType], var6, var1 + -37, 102, this.anIntArray1548[this.anInt1423], 64, var5 + 23, false, 0);
        if (var1 != -28) {
            this.anIntArray1592 = null;
        }

        this.surface.drawSprite((byte) 61, this.gameHeight, 22 + this.spriteMedia, 0);
        this.surface.draw(-2020315800, this.graphics, super.anInt58, super.anInt7);
    }

    private boolean method65(int var1, int var2, int var3) {
        if (var1 == 31 && (this.method53((byte) -90, 197) || this.method53((byte) -77, 615) || this.method53((byte) -99, 682))) {
            return true;
        } else if (var1 == 32 && (this.method53((byte) -86, 102) || this.method53((byte) -121, 616) || this.method53((byte) -95, 683))) {
            return true;
        } else if (var1 == 33 && (this.method53((byte) -121, 101) || this.method53((byte) -98, 617) || this.method53((byte) -81, 684))) {
            return true;
        } else if (var1 == 34 && (this.method53((byte) -103, 103) || this.method53((byte) -83, 618) || this.method53((byte) -92, 685))) {
            return true;
        } else if (var3 <= this.getInventoryCount(var1)) {
            return true;
        } else {
            if (var2 != 0) {
                this.createPlayer(30, 104, 68, -5, 51);
            }

            return false;
        }
    }

    private void drawDialogReportAbuseInput(int var1) {
        if (super.inputTextFinal.length() > 0) {
            this.reportName = super.inputTextFinal.trim();
            this.showDialogReportAbuseStep = 2;
            this.reportReason = 0;
        } else {
            byte admin = 0;
            if (this.modlevel2 < 2 && this.modlevel1 < 7) {
                if (this.modlevel1 >= 5) {
                    admin = 1;
                }
            } else {
                admin = 2;
            }

            int textHeight1 = this.surface.textHeight(1, true);
            int textHeight4 = this.surface.textHeight(4, true);
            short w = 400;
            int h = (admin > 0 ? textHeight1 + 5 : 0) + 70;
            int x = -(w / 2) + 256;
            int y = -(h / 2) + 180;
            this.surface.drawBox(x, y, w, h, 0);
            this.surface.drawBoxEdge(x, y, w, h, 0xffffff);
            this.surface.drawstringCenter("Enter the name of the player you wish to report:", 1, textHeight1 + 5 + y, var1, 0xffff00);
            int textHeight12 = 2 + textHeight1;
            this.surface.drawstringCenter(super.inputTextCurrent + "*", 4, textHeight4 + 3 + textHeight12 + (y - -5), 256, 0xffffff);
            int textHeightAyy = 2 + textHeight4 + textHeight12 + 5 + y - -3 + textHeight1;
            int color = 0xffffff;
            if (admin > 0) {
                String var12 = this.reportMutePlayer ? "[X]" : "[ ]";
                if (admin > 1) {
                    var12 = var12 + " Mute player";
                } else {
                    var12 = var12 + " Suggest mute";
                }

                int textWidth1 = this.surface.textWidth(1, var1 ^ -383, var12);
                if (super.mouseX > (256 + -(textWidth1 / 2)) && super.mouseX < 256 - -(textWidth1 / 2) && super.mouseY > (textHeightAyy + -textHeight1) && super.mouseY < textHeightAyy) {
                    color = 0xffff00;
                    if (this.mouseButtonClick != 0) {
                        this.mouseButtonClick = 0;
                        this.reportMutePlayer = !this.reportMutePlayer;
                    }
                }

                this.surface.drawstringCenter(var12, 1, textHeightAyy, 256, color);
                textHeightAyy += 10 + textHeight1;
            }

            color = 0xffffff;
            if (super.mouseX > 210 && super.mouseX < 228 && super.mouseY > (textHeightAyy + -textHeight1) && textHeightAyy > super.mouseY) {
                if (this.mouseButtonClick != 0) {
                    super.inputTextFinal = super.inputTextCurrent;
                    this.mouseButtonClick = 0;
                }

                color = 0xffff00;
            }

            this.surface.drawstring("OK", 210, textHeightAyy, 1, color);
            color = 0xffffff;
            if (super.mouseX > 264 && super.mouseX < 304 && super.mouseY > textHeightAyy + -textHeight1 && textHeightAyy > super.mouseY) {
                if (this.mouseButtonClick != 0) {
                    this.showDialogReportAbuseStep = 0;
                    this.mouseButtonClick = 0;
                }

                color = 0xffff00;
            }

            this.surface.drawstring("Cancel", 264, textHeightAyy, 1, color);
            if (this.mouseButtonClick == 1) {
                if (x > super.mouseX || super.mouseX > w + x || y > super.mouseY || super.mouseY > h + y) {
                    this.showDialogReportAbuseStep = 0;
                    this.mouseButtonClick = 0;
                }

            }
        }
    }

    private void drawDialogDuel(int var2, int var3) {
        boolean sendUpdate = false;
        int var5 = 0;
        int var6 = this.inventoryItemId[var3];

        int var8;
        for (int var7 = 0; var7 < this.duelOfferItemCount; ++var7) {
            if (var6 == this.duelOfferItemId[var7]) {
                if (GameData.itemStackable[var6] != 0) {
                    ++var5;
                } else if (var2 < 0) {
                    for (var8 = 0; this.mouseButtonItemCountIncrement > var8; ++var8) {
                        sendUpdate = true;
                        if (this.inventoryItemStackCount[var3] > this.duelOfferItemStack[var7]) {
                            ++this.duelOfferItemStack[var7];
                        }
                    }
                } else {
                    this.duelOfferItemStack[var7] += var2;
                    if (this.duelOfferItemStack[var7] > this.inventoryItemStackCount[var3]) {
                        this.duelOfferItemStack[var7] = this.inventoryItemStackCount[var3];
                    }

                    sendUpdate = true;
                }
            }
        }

        var8 = this.getInventoryCount(var6);
        if (var8 <= var5) {
            sendUpdate = true;
        }

        if (GameData.itemSpecial[var6] == 1) {
            sendUpdate = true;
            this.showMessage(0, "This object cannot be added to a duel offer", (String) null, (String) null, 0, (String) null, false);
        }

        int var9;
        if (!sendUpdate) {
            if (var2 < 0) {
                if (this.duelOfferItemCount < 8) {
                    this.duelOfferItemId[this.duelOfferItemCount] = var6;
                    this.duelOfferItemStack[this.duelOfferItemCount] = 1;
                    ++this.duelOfferItemCount;
                    sendUpdate = true;
                }
            } else {
                for (var9 = 0; var9 < var2 && this.duelOfferItemCount < 8 && var5 < var8; ++var9) {
                    this.duelOfferItemId[this.duelOfferItemCount] = var6;
                    this.duelOfferItemStack[this.duelOfferItemCount] = 1;
                    ++this.duelOfferItemCount;
                    ++var5;
                    sendUpdate = true;
                    if (var9 == 0 && GameData.itemStackable[var6] == 0) {
                        this.duelOfferItemStack[-1 + this.duelOfferItemCount] = var2 <= this.inventoryItemStackCount[var3] ? var2 : this.inventoryItemStackCount[var3];
                        break;
                    }
                }
            }
        }

        if (sendUpdate) {
            this.clientStream.newPacket(33);
            this.clientStream.writeBuffer.putByte(this.duelOfferItemCount);

            for (var9 = 0; this.duelOfferItemCount > var9; ++var9) {
                this.clientStream.writeBuffer.putShort(this.duelOfferItemId[var9]);
                this.clientStream.writeBuffer.putInt(this.duelOfferItemStack[var9]);
            }

            this.clientStream.sendPacket();
            this.duelOfferAccepted = false;
            this.duelOfferOpponentAccepted = false;
        }
        ;
    }

    private boolean createRightClickMenusSocial(String var1, int var2, String var3) {
        String var4 = Utility.formatName(var1);
        if (var4 == null) {
            return false;
        } else if (var4.equals(Utility.formatName(this.localPlayer.accountName))) {
            return false;
        } else {
            boolean isFriend = false;
            boolean isOnline = false;

            for (int var7 = var2; var7 < friendListCount; ++var7) {
                if (var4.equals(Utility.formatName(friendListNames[var7]))) {
                    isFriend = true;
                    if ((4 & friendListOnline[var7]) != 0) {
                        isOnline = true;
                    }
                    break;
                }
            }

            if (isFriend) {
                if (isOnline) {
                    this.menuCommon.addItem("Message", var1, 2830, 0, "@whi@" + var3, var3);
                }
            } else {
                boolean isIgnored = false;

                for (int var9 = 0; ignoreListCount > var9; ++var9) {
                    if (var4.equals(Utility.formatName(ignoreListAccNames[var9]))) {
                        isIgnored = true;
                        break;
                    }
                }

                if (!isIgnored) {
                    this.menuCommon.addItem("Add friend", var1, 2831, 0, "@whi@" + var3, var3);
                    this.menuCommon.addItem("Add ignore", var1, 2832, var2, "@whi@" + var3, var3);
                }
            }

            this.menuCommon.addItem("Report abuse", var1, 2833, var2, "@whi@" + var3, var3);
            return true;
        }
    }

    private void method69(int var1) {
        int var2 = super.anInt58;
        int var3 = super.anInt7;
        int var4 = -var2 + -this.gameWidth + this.anInt1417;
        int var5 = -var3 + this.anInt1556 - (12 + this.gameHeight);
        if (var1 != 275) {
            this.createModel(-104, -50, 96, -5, -78, -47);
        }

        if (var2 > 0 || var4 > 0 || var3 > 0 || var5 > 0) {
            try {
                Object var6;
                if (this.appletMode) {
                    if (Isaac.anApplet445 == null) {
                        var6 = this;
                    } else {
                        var6 = Isaac.anApplet445;
                    }
                } else {
                    var6 = gameFrameReference;
                }

                Graphics var7 = ((Component) var6).getGraphics();
                if (var7 == null) {
                    return;
                }

                var7.setColor(Color.black);
                if (var2 > 0) {
                    var7.fillRect(0, 0, var2, this.anInt1556);
                }

                if (var3 > 0) {
                    var7.fillRect(0, 0, this.anInt1417, var3);
                }

                if (var4 > 0) {
                    var7.fillRect(this.anInt1417 - var4, 0, var4, this.anInt1556);
                }

                if (var5 > 0) {
                    var7.fillRect(0, -var5 + this.anInt1556, this.anInt1417, var5);
                    return;
                }
            } catch (Exception var8) {
                return;
            }
        }
        ;
    }

    private boolean walkTo(int var1, int var2, int var3, int var4, int var5, int var6, boolean var7, boolean var8, byte var9) {
        if (var9 < 97) {
            this.sendChatMessage((byte) 8, (String) null);
        }

        int var10 = this.world.method263(this.anIntArray1330, var4, var2, var3, var8, this.anIntArray1336, var5, var1, var6, 3);
        if (var10 == -1) {
            return false;
        } else {
            --var10;
            var6 = this.anIntArray1336[var10];
            var1 = this.anIntArray1330[var10];
            --var10;
            if (var7) {
                this.clientStream.newPacket(16);
            } else {
                this.clientStream.newPacket(187);
            }

            this.clientStream.writeBuffer.putShort(var6 - -this.regionX);
            this.clientStream.writeBuffer.putShort(var1 + this.regionY);
            if (var7 && var10 == -1 && (var6 + this.regionX) % 5 == 0) {
                var10 = 0;
            }

            for (int var11 = var10; var11 >= 0 && var10 - 25 < var11; --var11) {
                this.clientStream.writeBuffer.putByte(-var6 + this.anIntArray1336[var11]);
                this.clientStream.writeBuffer.putByte(this.anIntArray1330[var11] - var1);
            }

            this.clientStream.sendPacket();
            this.anInt1265 = super.mouseY;
            this.anInt1289 = super.mouseX;
            this.mouseClickXStep = -24;
            return true;
        }
    }

    private void createLoginPanels(int var1) {
        this.panelLoginWelcome = new Panel(this.surface, 50);
        byte var2 = 40;
        this.panelLoginWelcome.addText(true, false, 4, var2 + 200, 256, "Welcome to RuneScape Classic");
        String var3 = null;
        if (this.members) {
            if (!this.veterans) {
                var3 = "You need a members account to use this server";
            } else {
                var3 = "You need a veteran Classic members account to use this server";
            }
        } else if (this.veterans) {
            var3 = "You need a veteran Classic account to use this server";
        }

        if (var3 != null) {
            this.panelLoginWelcome.addText(true, false, 4, var2 + 215, 256, var3);
        }

        this.panelLoginWelcome.addButtonBackground(256, 200, 250 + var2, 35, (byte) -8);
        this.panelLoginWelcome.addText(false, false, 5, 250 - -var2, 256, "Click here to login");
        this.anInt1515 = this.panelLoginWelcome.addButton(200, 35, var2 + 250, 256, var1 + 84);
        panelLoginWelcome.setFocus((byte) -60, anInt1515);
        short var5 = 230;
        this.paneLogin = new Panel(this.surface, 50);
        this.controlLoginStatus1 = this.paneLogin.addText(true, false, 4, var5 - 30, 256, "");
        this.controlLoginStatus2 = this.paneLogin.addText(true, false, 4, -10 + var5, 256, "Please enter your username and password");
        int var6 = var5 + 28;
        this.paneLogin.addButtonBackground(140, 200, var6, 40, (byte) -8);
        this.paneLogin.addText(false, false, 4, var6 + -10, 140, "Username:");
        this.controlLoginUser = this.paneLogin.method535(140, -53, false, false, 200, 4, 320, var1, 10 + var6);
        var6 += 47;
        this.paneLogin.addButtonBackground(190, 200, var6, 40, (byte) -8);
        this.paneLogin.addText(false, false, 4, -10 + var6, 190, "Password:");
        this.controlLoginPass = this.paneLogin.method535(190, -108, true, false, 200, 4, 20, 40, var6 + 10);
        var6 -= 55;
        this.paneLogin.addButtonBackground(410, 120, var6, 25, (byte) -8);
        this.paneLogin.addText(false, false, 4, var6, 410, "Ok");
        this.anInt1473 = this.paneLogin.addButton(120, 25, var6, 410, 123);
        var6 += 30;
        this.paneLogin.addButtonBackground(410, 120, var6, 25, (byte) -8);
        this.paneLogin.addText(false, false, 4, var6, 410, "Cancel");
        this.anInt1537 = this.paneLogin.addButton(120, 25, var6, 410, var1 ^ 85);
        var6 += 30;
        this.paneLogin.setFocus((byte) -60, this.controlLoginUser);
    }

    private void drawUiTabMinimap(boolean nomenus, int var2) {
        if (var2 != -1033933134) {
            this.settingsBlockChat = -97;
        }

        int var3 = -199 + this.surface.width2;
        short var4 = 156;
        this.surface.drawSprite((byte) 61, 3, this.spriteMedia - -2, -49 + var3);
        short var5 = 152;
        var3 += 40;
        this.surface.drawBox(var3, 36, var4, var5, 0);
        this.surface.setBounds(var3, 36, var3 + var4, var5 + 36);
        int var6 = this.minimapRandom_2 + 192;
        int var7 = 255 & this.minimapRandom_1 + this.cameraRotation;
        int var8 = var6 * (-18120 + 3 * this.localPlayer.currentX) / 2048;
        int var9 = (-18120 + this.localPlayer.currentY * 3) * var6 / 2048;
        int var10 = Scene.anIntArray823[-(var7 * 4) + 1024 & 1023];
        int var11 = Scene.anIntArray823[(1023 & 1024 - 4 * var7) - -1024];
        int var12 = var10 * var9 - -(var8 * var11) >> 18;
        var9 = -(var10 * var8) + var11 * var9 >> 18;
        this.surface.drawMinimapSprite(-var12 + var4 / 2 + var3, var6, -1 + this.spriteMedia, 36 + (var5 / 2 - -var9), true, var7 + 64 & 255);

        for (int var13 = 0; var13 < this.objectCount; ++var13) {
            var9 = (this.magicLoc * this.objectX[var13] - (-64 + this.localPlayer.currentY)) * 3 * var6 / 2048;
            var8 = 3 * (-this.localPlayer.currentX + this.magicLoc * this.objectY[var13] + 64) * var6 / 2048;
            var12 = var8 * var11 + var9 * var10 >> 18;
            var9 = -(var10 * var8) + var9 * var11 >> 18;
            this.drawMinimapEntity(false, '\uffff', -var9 + var5 / 2 + 36, var12 + var4 / 2 + var3);
        }

        for (int var14 = 0; var14 < this.groundItemCount; ++var14) {
            var8 = 3 * (-this.localPlayer.currentX + 64 + this.groundItemX[var14] * this.magicLoc) * var6 / 2048;
            var9 = var6 * (-this.localPlayer.currentY + this.magicLoc * this.groundItemY[var14] - -64) * 3 / 2048;
            var12 = var9 * var10 - -(var11 * var8) >> 18;
            var9 = var9 * var11 + -(var8 * var10) >> 18;
            this.drawMinimapEntity(false, 0xff0000, var5 / 2 + 36 - var9, var12 + var3 + var4 / 2);
        }

        for (int var15 = 0; this.npcCount > var15; ++var15) {
            Character var16 = this.npcs[var15];
            var9 = (-this.localPlayer.currentY + var16.currentY) * 3 * var6 / 2048;
            var8 = (-this.localPlayer.currentX + var16.currentX) * 3 * var6 / 2048;
            var12 = var11 * var8 + var9 * var10 >> 18;
            var9 = -(var10 * var8) + var11 * var9 >> 18;
            this.drawMinimapEntity(false, 0xffff00, 36 - (-(var5 / 2) - -var9), var3 + (var4 / 2 - -var12));
        }

        for (int var22 = 0; this.playerCount > var22; ++var22) {
            Character var17 = this.players[var22];
            var8 = 3 * (-this.localPlayer.currentX + var17.currentX) * var6 / 2048;
            var9 = (-this.localPlayer.currentY + var17.currentY) * 3 * var6 / 2048;
            var12 = var11 * var8 + var9 * var10 >> 18;
            var9 = var11 * var9 - var8 * var10 >> 18;
            int var18 = 0xffffff;
            String var19 = Utility.formatName(var17.accountName);
            if (var19 != null) {
                for (int var20 = 0; friendListCount > var20; ++var20) {
                    if (var19.equals(Utility.formatName(friendListNames[var20])) && (2 & friendListOnline[var20]) != 0) {
                        var18 = '\uff00';
                        break;
                    }
                }
            }

            this.drawMinimapEntity(false, var18, var5 / 2 + 36 - var9, var12 + var4 / 2 + var3);
        }

        this.surface.drawCircle(2, var4 / 2 + var3, 36 + var5 / 2, -1385529104, 0xffffff, 255);
        this.surface.drawMinimapSprite(19 + var3, 128, this.spriteMedia - -24, 55, true, this.cameraRotation - -128 & 255);
        this.surface.setBounds(0, 0, this.gameWidth, this.gameHeight - -12);
        if (nomenus) {
            var3 = 199 + -this.surface.width2 + super.mouseX;
            int var23 = -36 + super.mouseY;
            if (var3 >= 40 && var23 >= 0 && var3 < 196 && var23 < 152) {
                var3 = -199 + this.surface.width2;
                var4 = 156;
                var5 = 152;
                var6 = 192 - -this.minimapRandom_2;
                var7 = 255 & this.cameraRotation + this.minimapRandom_1;
                var3 += 40;
                var11 = Scene.anIntArray823[1024 + (1024 - 4 * var7 & 1023)];
                var10 = Scene.anIntArray823[-(4 * var7) + 1024 & 1023];
                var9 = 16384 * (super.mouseY - var5 / 2 - 36) / (3 * var6);
                var8 = 16384 * (super.mouseX + (-var3 - var4 / 2)) / (var6 * 3);
                var12 = var9 * var10 - -(var11 * var8) >> 15;
                var9 = var11 * var9 - var8 * var10 >> 15;
                var8 = var12 + this.localPlayer.currentX;
                var9 = -var9 + this.localPlayer.currentY;
                mouseMinimapX = var8 / 128;
                mouseMinimapY = var9 / 128;
                if (this.mouseButtonClick == 1) {
                    this.walkToActionSource(false, var8 / 128, var9 / 128, this.localRegionX, this.localRegionY, (byte) 102);
                }

                this.mouseButtonClick = 0;
            }
        }
        ;
    }

    int mouseMinimapX, mouseMinimapY;

    private void handleGameInput(int var1) {
        if (this.systemUpdate > 1) {
            --this.systemUpdate;
        }

        this.checkConnection(0);
        if (this.logoutTimeout > 0) {
            --this.logoutTimeout;
        }

        if (super.lastMouseAction > 15000 && this.combatTimeout == 0 && this.logoutTimeout == 0) {
            super.lastMouseAction -= 15000;
            this.sendLogout(var1 ^ -18769);
            return;
        }
        if (this.localPlayer.animationCurrent == 8 || this.localPlayer.animationCurrent == 9) {
            this.combatTimeout = 500;
        }

        if (this.combatTimeout > 0) {
            --this.combatTimeout;
        }

        if (this.showAppearanceChange) {
            this.handleAppearancePanelControls(var1 + 18774);
            return;
        }
        int var4;
        int var6;
        int var7;
        int var8;
        for (int var2 = 0; var2 < this.playerCount; ++var2) {
            Character var3 = this.players[var2];
            var4 = (var3.waypointCurrent - -1) % 10;
            if (var3.movingStep == var4) {
                var3.animationCurrent = var3.animationNext;
            } else {
                byte var5 = -1;
                var6 = var3.movingStep;
                if (var4 > var6) {
                    var7 = var4 - var6;
                } else {
                    var7 = 10 + var4 + -var6;
                }

                var8 = 4;
                if (var7 > 2) {
                    var8 = -4 + 4 * var7;
                }

                if (var3.waypointsX[var6] - var3.currentX <= this.magicLoc * 3 && (3 * this.magicLoc) >= (-var3.currentY + var3.waypointsY[var6]) && -this.magicLoc * 3 <= var3.waypointsX[var6] + -var3.currentX && var3.waypointsY[var6] - var3.currentY >= -this.magicLoc * 3 && var7 <= 8) {
                    if (var3.waypointsX[var6] > var3.currentX) {
                        var5 = 2;
                        var3.currentX += var8;
                        ++var3.stepCount;
                    } else if (var3.waypointsX[var6] < var3.currentX) {
                        var3.currentX -= var8;
                        var5 = 6;
                        ++var3.stepCount;
                    }

                    if (var3.currentX - var3.waypointsX[var6] < var8 && var3.currentX + -var3.waypointsX[var6] > -var8) {
                        var3.currentX = var3.waypointsX[var6];
                    }

                    if (var3.waypointsY[var6] <= var3.currentY) {
                        if (var3.currentY > var3.waypointsY[var6]) {
                            if (var5 != -1) {
                                if (var5 == 2) {
                                    var5 = 1;
                                } else {
                                    var5 = 7;
                                }
                            } else {
                                var5 = 0;
                            }

                            var3.currentY -= var8;
                            ++var3.stepCount;
                        }
                    } else {
                        if (var5 == -1) {
                            var5 = 4;
                        } else if (var5 != 2) {
                            var5 = 5;
                        } else {
                            var5 = 3;
                        }

                        var3.currentY += var8;
                        ++var3.stepCount;
                    }

                    if (var8 > var3.currentY - var3.waypointsY[var6] && -var3.waypointsY[var6] + var3.currentY > -var8) {
                        var3.currentY = var3.waypointsY[var6];
                    }
                } else {
                    var3.currentX = var3.waypointsX[var6];
                    var3.currentY = var3.waypointsY[var6];
                }

                if (var5 != -1) {
                    var3.animationCurrent = var5;
                }

                if (var3.waypointsX[var6] == var3.currentX && var3.waypointsY[var6] == var3.currentY) {
                    var3.movingStep = (1 + var6) % 10;
                }
            }

            if (var3.combatTimeout > 0) {
                --var3.combatTimeout;
            }

            if (var3.messageTimeout > 0) {
                --var3.messageTimeout;
            }

            if (var3.bubbleTimeout > 0) {
                --var3.bubbleTimeout;
            }

            if (this.deathScreenTimeout > 0) {
                --this.deathScreenTimeout;
                if (this.deathScreenTimeout == 0) {
                    this.showMessage(0, "You have been granted another life. Be more careful this time!", (String) null, (String) null, 0, (String) null, false);
                }

                if (this.deathScreenTimeout == 0) {
                    this.showMessage(0, "You retain your skills. Your objects land where you died", (String) null, (String) null, 0, (String) null, false);
                }
            }
        }

        int var16;
        for (int var11 = 0; this.npcCount > var11; ++var11) {
            Character c = this.npcs[var11];
            var16 = (c.waypointCurrent - -1) % 10;
            if (var16 == c.movingStep) {
                c.animationCurrent = c.animationNext;
                if (c.npcId == 43) {
                    ++c.stepCount;
                }
            } else {
                byte var13 = -1;
                var7 = c.movingStep;
                if (var16 <= var7) {
                    var8 = var16 + 10 + -var7;
                } else {
                    var8 = -var7 + var16;
                }

                int var9 = 4;
                if (var8 > 2) {
                    var9 = -4 + var8 * 4;
                }

                if ((c.waypointsX[var7] + -c.currentX) <= (3 * this.magicLoc) && -c.currentY + c.waypointsY[var7] <= 3 * this.magicLoc && -c.currentX + c.waypointsX[var7] >= -this.magicLoc * 3 && (-this.magicLoc * 3) <= (c.waypointsY[var7] - c.currentY) && var8 <= 8) {
                    if (c.waypointsX[var7] > c.currentX) {
                        ++c.stepCount;
                        var13 = 2;
                        c.currentX += var9;
                    } else if (c.currentX > c.waypointsX[var7]) {
                        var13 = 6;
                        ++c.stepCount;
                        c.currentX -= var9;
                    }

                    if (var9 > c.currentX - c.waypointsX[var7] && (-var9) < (-c.waypointsX[var7] + c.currentX)) {
                        c.currentX = c.waypointsX[var7];
                    }

                    if (c.currentY >= c.waypointsY[var7]) {
                        if (c.waypointsY[var7] < c.currentY) {
                            if (var13 != -1) {
                                if (var13 == 2) {
                                    var13 = 1;
                                } else {
                                    var13 = 7;
                                }
                            } else {
                                var13 = 0;
                            }

                            ++c.stepCount;
                            c.currentY -= var9;
                        }
                    } else {
                        ++c.stepCount;
                        c.currentY += var9;
                        if (var13 == -1) {
                            var13 = 4;
                        } else if (var13 != 2) {
                            var13 = 5;
                        } else {
                            var13 = 3;
                        }
                    }

                    if (var9 > c.currentY + -c.waypointsY[var7] && -var9 < -c.waypointsY[var7] + c.currentY) {
                        c.currentY = c.waypointsY[var7];
                    }
                } else {
                    c.currentX = c.waypointsX[var7];
                    c.currentY = c.waypointsY[var7];
                }

                if (var13 != -1) {
                    c.animationCurrent = var13;
                }

                if (c.waypointsX[var7] == c.currentX && c.waypointsY[var7] == c.currentY) {
                    c.movingStep = (var7 - -1) % 10;
                }
            }

            if (c.bubbleTimeout > 0) {
                --c.bubbleTimeout;
            }

            if (c.combatTimeout > 0) {
                --c.combatTimeout;
            }

            if (c.messageTimeout > 0) {
                --c.messageTimeout;
            }
        }

        if (this.showUiTab != 2) {
            if (Surface.anInt1129 > 0) {
                ++this.sleepWordDelayTimer;
            }

            if (Surface.anInt751 > 0) {
                this.sleepWordDelayTimer = 0;
            }

            Surface.anInt1129 = 0;
            Surface.anInt751 = 0;
        }

        for (var4 = 0; var4 < this.playerCount; ++var4) {
            Character var15 = this.players[var4];
            if (var15.projectileRange > 0) {
                --var15.projectileRange;
            }
        }

        if (this.sleepWordDelayTimer > 20) {
            this.sleepWordDelayTimer = 0;
            this.sleepWordDelay = false;
        }

        if (!this.cameraAutoAngleDebug) {
            if ((this.cameraAutoRotatePlayerY - this.localPlayer.currentX) < -500 || this.cameraAutoRotatePlayerY - this.localPlayer.currentX > 500 || this.cameraAutoRotatePlayerX + -this.localPlayer.currentY < -500 || (this.cameraAutoRotatePlayerX + -this.localPlayer.currentY) > 500) {
                this.cameraAutoRotatePlayerY = this.localPlayer.currentX;
                this.cameraAutoRotatePlayerX = this.localPlayer.currentY;
            }

            if (this.localPlayer.currentY != this.cameraAutoRotatePlayerX) {
                this.cameraAutoRotatePlayerX += (this.localPlayer.currentY - this.cameraAutoRotatePlayerX) / (16 - -((-500 + this.cameraZoom) / 15));
            }

            if (this.optionCameraModeAuto) {
                var16 = 32 * this.cameraAngle;
                var6 = -this.cameraRotation + var16;
                byte var14 = 1;
                if (var6 != 0) {
                    ++this.anInt1275;
                    if (var6 > 128) {
                        var14 = -1;
                        var6 = 256 + -var6;
                    } else if (var6 <= 0) {
                        if (var6 < -128) {
                            var14 = 1;
                            var6 += 256;
                        } else if (var6 < 0) {
                            var6 = -var6;
                            var14 = -1;
                        }
                    } else {
                        var14 = 1;
                    }

                    this.cameraRotation += (var6 * this.anInt1275 - -255) / 256 * var14;
                    this.cameraRotation &= 255;
                } else {
                    this.anInt1275 = 0;
                }
            }

            if (this.localPlayer.currentX != this.cameraAutoRotatePlayerY) {
                this.cameraAutoRotatePlayerY += (-this.cameraAutoRotatePlayerY + this.localPlayer.currentX) / (16 + (this.cameraZoom - 500) / 15);
            }
        } else if ((this.cameraAutoRotatePlayerY - this.localPlayer.currentX) < -500 || (-this.localPlayer.currentX + this.cameraAutoRotatePlayerY) > 500 || -this.localPlayer.currentY + this.cameraAutoRotatePlayerX < -500 || -this.localPlayer.currentY + this.cameraAutoRotatePlayerX > 500) {
            this.cameraAutoRotatePlayerX = this.localPlayer.currentY;
            this.cameraAutoRotatePlayerY = this.localPlayer.currentX;
        }

        if (this.isSleeping) {
            if (super.inputTextFinal.length() > 0) {
                if (super.inputTextFinal.equalsIgnoreCase("::lostcon") && !this.appletMode) {
                    this.clientStream.closeStream();
                } else if (super.inputTextFinal.equalsIgnoreCase("::closecon") && !this.appletMode) {
                    this.closeConnection(true, -10);
                } else {
                    this.clientStream.newPacket(45);
                    if (!this.sleepWordDelay) {
                        this.clientStream.writeBuffer.putByte(0);
                        this.sleepWordDelay = true;
                    } else {
                        this.clientStream.writeBuffer.putByte(1);
                    }

                    this.clientStream.writeBuffer.pjstr2(super.inputTextFinal);
                    this.clientStream.sendPacket();
                    super.inputTextCurrent = "";
                    this.sleepingStatusText = "Please wait...";
                    super.inputTextFinal = "";
                }
            }

            if (super.lastMouseButtonDown == 1 && super.mouseY > 275 && super.mouseY < 310 && super.mouseX > 56 && super.mouseX < 456) {
                this.clientStream.newPacket(45);
                if (this.sleepWordDelay) {
                    this.clientStream.writeBuffer.putByte(1);
                } else {
                    this.clientStream.writeBuffer.putByte(0);
                    this.sleepWordDelay = true;
                }

                this.clientStream.writeBuffer.pjstr2("-null-");
                this.clientStream.sendPacket();
                this.sleepingStatusText = "Please wait...";
                super.inputTextFinal = "";
                super.inputTextCurrent = "";
            }

            super.lastMouseButtonDown = 0;
        } else {
            if (this.gameHeight - 4 < super.mouseY) {
                if (super.mouseX > 15 && super.mouseX < 96 && super.lastMouseButtonDown == 1) {
                    this.messageTabSelected = 0;
                }

                if (super.mouseX > 110 && super.mouseX < 194 && super.lastMouseButtonDown == 1) {
                    this.messageTabSelected = 1;
                    this.panelMessageTabs.controlFlashText[this.controlTextListChat] = 999999;
                }

                if (super.mouseX > 215 && super.mouseX < 295 && super.lastMouseButtonDown == 1) {
                    this.messageTabSelected = 2;
                    this.panelMessageTabs.controlFlashText[this.controlTextListQuest] = 999999;
                }

                if (super.mouseX > 315 && super.mouseX < 395 && super.lastMouseButtonDown == 1) {
                    this.messageTabSelected = 3;
                    this.panelMessageTabs.controlFlashText[this.controlTextListPrivate] = 999999;
                }

                if (super.mouseX > 417 && super.mouseX < 497 && super.lastMouseButtonDown == 1) {
                    this.showDialogReportAbuseStep = 1;
                    super.inputTextCurrent = "";
                    super.inputTextFinal = "";
                }

                super.lastMouseButtonDown = 0;
                super.anInt84 = 0;
            }

            this.panelMessageTabs.handleMouse(-111, super.lastMouseButtonDown, super.mouseX, super.anInt84, super.mouseY);
            if (this.messageTabSelected > 0 && super.mouseX >= 494 && (this.gameHeight - 66) <= super.mouseY) {
                super.lastMouseButtonDown = 0;
            }

            if (var1 != -18771) {
                this.resetGame(-110);
            }

            if (this.panelMessageTabs.isClicked(3, this.controlTextListAll)) {
                String var17 = this.panelMessageTabs.method523(-17054, this.controlTextListAll);
                this.panelMessageTabs.updateText(this.controlTextListAll, "", 29);
                if (!var17.startsWith("::")) {
                    this.sendChatMessage((byte) 120, var17);
                } else if (var17.equalsIgnoreCase("::closecon") && !this.appletMode) {
                    this.clientStream.closeStream();
                } else if (var17.equalsIgnoreCase("::logout") && !this.appletMode) {
                    this.closeConnection(true, -10);
                } else if (var17.equalsIgnoreCase("::lostcon") && !this.appletMode) {
                    this.lostConnection(-12);
                } else {
                    this.sendCommandString(var17.substring(2));
                }
            }

            for (var16 = 0; var16 < 100; ++var16) {
                if (messageHistoryTimeout[var16] > 0) {
                    --messageHistoryTimeout[var16];
                }
            }

            if (!this.showDialogTrade && !this.showDialogDuel) {
                this.mouseButtonItemCountIncrement = 0;
                this.mouseButtonDownTime = 0;
            } else {
                if (super.anInt84 == 0) {
                    this.mouseButtonDownTime = 0;
                } else {
                    ++this.mouseButtonDownTime;
                }

                if (this.mouseButtonDownTime <= 600) {
                    if (this.mouseButtonDownTime > 450) {
                        this.mouseButtonItemCountIncrement += 500;
                    } else if (this.mouseButtonDownTime <= 300) {
                        if (this.mouseButtonDownTime > 150) {
                            this.mouseButtonItemCountIncrement += 5;
                        } else if (this.mouseButtonDownTime <= 50) {
                            if (this.mouseButtonDownTime > 20 && (this.mouseButtonDownTime & 5) == 0) {
                                ++this.mouseButtonItemCountIncrement;
                            }
                        } else {
                            ++this.mouseButtonItemCountIncrement;
                        }
                    } else {
                        this.mouseButtonItemCountIncrement += 50;
                    }
                } else {
                    this.mouseButtonItemCountIncrement += 5000;
                }
            }

            if (this.deathScreenTimeout != 0) {
                super.lastMouseButtonDown = 0;
            }

            if (super.lastMouseButtonDown != 1) {
                if (super.lastMouseButtonDown == 2) {
                    this.mouseButtonClick = 2;
                }
            } else {
                this.mouseButtonClick = 1;
            }

            this.scene.setMouseLoc(super.mouseY, super.mouseX, false);
            super.lastMouseButtonDown = 0;
            if (this.optionCameraModeAuto) {
                if (this.anInt1275 == 0 || this.cameraAutoAngleDebug) {
                    if (super.keyLeft) {
                        this.cameraAngle = 1 + this.cameraAngle & 7;
                        super.keyLeft = false;
                        if (!this.fogOfWar) {
                            if ((this.cameraAngle & 1) == 0) {
                                this.cameraAngle = 7 & 1 + this.cameraAngle;
                            }

                            for (var6 = 0; var6 < 8 && !this.method138(this.cameraAngle, 38); ++var6) {
                                this.cameraAngle = 1 + this.cameraAngle & 7;
                            }
                        }
                    }

                    if (super.keyRight) {
                        this.cameraAngle = 7 & 7 + this.cameraAngle;
                        super.keyRight = false;
                        if (!this.fogOfWar) {
                            if ((this.cameraAngle & 1) == 0) {
                                this.cameraAngle = 7 + this.cameraAngle & 7;
                            }

                            for (var6 = 0; var6 < 8 && !this.method138(this.cameraAngle, var1 + 18838); ++var6) {
                                this.cameraAngle = this.cameraAngle - -7 & 7;
                            }
                        }
                    }
                }
            } else if (!super.keyLeft) {
                if (super.keyRight) {
                    this.cameraRotation = 255 & this.cameraRotation - 2;
                }
            } else {
                this.cameraRotation = 2 + this.cameraRotation & 255;
            }

            if (this.mouseClickXStep > 0) {
                --this.mouseClickXStep;
            } else if (this.mouseClickXStep < 0) {
                ++this.mouseClickXStep;
            }

            if (this.fogOfWar && this.cameraZoom > 550) {
                this.cameraZoom -= 4;
            } else if (!this.fogOfWar && this.cameraZoom < 750) {
                this.cameraZoom += 4;
            }

            this.scene.method328(true, 17);
            ++this.objectAnimationCount;
            if (this.objectAnimationCount > 5) {
                this.objectAnimationCount = 0;
                this.objectAnimationNumberTorch = (this.objectAnimationNumberTorch + 1) % 4;
                this.objectAnimationNumberFireLightningSpell = (1 + this.objectAnimationNumberFireLightningSpell) % 3;
                this.objectAnimationNumberClaw = (1 + this.objectAnimationNumberClaw) % 5;
            }

            for (var6 = 0; var6 < this.objectCount; ++var6) {
                var7 = this.objectY[var6];
                var8 = this.objectX[var6];
                if (var7 >= 0 && var8 >= 0 && var7 < 96 && var8 < 96 && this.objectId[var6] == 74) {
                    this.objectModel[var6].rotate(57, 1, 0, 0);
                }
            }

            for (var7 = 0; this.teleportBubbleCount > var7; ++var7) {
                ++this.teleportBubbleTime[var7];
                if (this.teleportBubbleTime[var7] > 50) {
                    --this.teleportBubbleCount;

                    for (var8 = var7; this.teleportBubbleCount > var8; ++var8) {
                        this.teleportBubbleX[var8] = this.teleportBubbleX[var8 + 1];
                        this.teleportBubbleY[var8] = this.teleportBubbleY[1 + var8];
                        this.teleportBubbleTime[var8] = this.teleportBubbleTime[1 + var8];
                        this.teleportBubbleType[var8] = this.teleportBubbleType[1 + var8];
                    }
                }
            }

        }
    }

    private void sendCommandString(String var2) {
        this.clientStream.newPacket(38);
        this.clientStream.writeBuffer.pjstr2(var2);
        this.clientStream.sendPacket();
    }

    private void resetLoginScreenVariables(int var1) {
        this.password = "";
        this.username = "";
        this.anInt1617 = 0;
        this.loggedIn = 0;
        this.npcCount = var1;
        this.playerCount = 0;
    }

    private void drawRightClickMenu(int var1) {
        int var2;
        if (this.mouseButtonClick != 0) {
            var2 = this.menuCommon.drawNoBg(this.anInt1625, this.anInt1379, super.mouseX, super.mouseY);
            if (var2 >= 0) {
                this.menuItemClickl(var2, (byte) -14);
            }

            this.showRightClickMenu = false;
            this.mouseButtonClick = 0;
        } else if (var1 <= -36) {
            var2 = this.menuCommon.getWidth();
            int var3 = this.menuCommon.getHeight();
            if ((this.anInt1379 - 10) <= super.mouseX && super.mouseY >= -10 + this.anInt1625 && super.mouseX <= (10 + var2 + this.anInt1379) && 10 + var3 + this.anInt1625 >= super.mouseY) {
                this.menuCommon.draw(this.anInt1625, super.mouseX, this.anInt1379, super.mouseY);
            } else {
                this.showRightClickMenu = false;
            }
        }
    }

    private Character method77(int var1, int var2) {
        for (int var3 = var2; this.npcCount > var3; ++var3) {
            if (this.npcs[var3].serverIndex == var1) {
                return this.npcs[var3];
            }
        }

        return null;
    }

    private void updateBankItems(int var1) {
        this.bankItemCount = this.newBankItemCount;

        for (int i = 0; i < this.newBankItemCount; ++i) {
            this.bankItems[i] = this.newBankItems[i];
            this.bankItemsCount[i] = this.newBankItemsCount[i];
        }

        for (int invidx = 0; this.inventoryItemsCount > invidx && this.bankItemsMax > this.bankItemCount; ++invidx) {
            int invid = this.inventoryItemId[invidx];
            boolean hasitemininv = false;

            for (int var6 = 0; this.bankItemCount > var6; ++var6) {
                if (invid == this.bankItems[var6]) {
                    hasitemininv = true;
                    break;
                }
            }

            if (!hasitemininv) {
                this.bankItems[this.bankItemCount] = invid;
                this.bankItemsCount[this.bankItemCount] = 0;
                ++this.bankItemCount;
            }
        }
    }

    private void drawInputPopup() {
        if (super.inputTextFinal.length() <= 0 && !this.inputPopupSubmit) {
            if (this.inputPopupType >= 1 && this.inputPopupType <= 8) {
                String validInput = "";

                for (int i = 0; super.inputTextCurrent.length() > i; ++i) {
                    char chr = super.inputTextCurrent.charAt(i);
                    if (java.lang.Character.isDigit(chr)) {
                        validInput = validInput + chr;
                    }
                }

                super.inputTextCurrent = validInput;
            }

            int dialogX = 256 - this.inputPopupWidth / 2;
            int dialogY = 180 - this.inputPopupHeight / 2;
            this.surface.drawBox(dialogX, dialogY, this.inputPopupWidth, this.inputPopupHeight, 0);
            this.surface.drawBoxEdge(dialogX, dialogY, this.inputPopupWidth, this.inputPopupHeight, 0xffffff);
            int font1Height = this.surface.textHeight(1, true);
            int font4Height = this.surface.textHeight(4, true);
            int font1HeightWithSomeFreeSpace = 2 + font1Height;

            for (int var7 = 0; var7 < this.inputPopupText.length; ++var7) {
                this.surface.drawstringCenter(this.inputPopupText[var7], 1, 5 + dialogY + font1Height + var7 * font1HeightWithSomeFreeSpace, 256, 0xffff00);
            }

            if (this.inputPopupShowInput) {
                this.surface.drawstringCenter(super.inputTextCurrent + "*", 4, font1HeightWithSomeFreeSpace * this.inputPopupText.length + dialogY + 5 + 3 + font4Height, 256, 0xffffff);
            }

            int color = 0xffffff;
            int y = font4Height + dialogY - (-5 + (-(font1HeightWithSomeFreeSpace * this.inputPopupText.length) + -3 - 2) - font1Height);
            if (super.mouseX > 230 && super.mouseX < 248 && super.mouseY > (y + -font1Height) && super.mouseY < y) {
                if (this.mouseButtonClick != 0) {
                    this.mouseButtonClick = 0;
                    super.inputTextFinal = super.inputTextCurrent;
                    this.inputPopupSubmit = true;
                }

                color = 0xffff00;
            }

            this.surface.drawstring("OK", 230, y, 1, color);
            color = 0xffffff;
            if (super.mouseX > 264 && super.mouseX < 304 && (-font1Height + y) < super.mouseY && super.mouseY < y) {
                color = 0xffff00;
                if (this.mouseButtonClick != 0) {
                    this.mouseButtonClick = 0;
                    this.inputPopupType = 0;
                }
            }

            this.surface.drawstring("Cancel", 264, y, 1, color);
            if (this.mouseButtonClick == 1) {
                if (super.mouseX < dialogX || (this.inputPopupWidth + dialogX) < super.mouseX || dialogY > super.mouseY || super.mouseY > (this.inputPopupHeight + dialogY)) {
                    this.inputPopupType = 0;
                    this.mouseButtonClick = 0;
                }

            }
        } else {
            int var1 = 0;
            String input = super.inputTextFinal.trim();
            super.inputTextFinal = "";
            super.inputTextCurrent = "";
            if (this.inputPopupType != 1) {
                if (this.inputPopupType != 2) {
                    if (this.inputPopupType != 3) {
                        if (this.inputPopupType != 4) {
                            if (this.inputPopupType == 5) {
                                try {
                                    int var3 = this.shopItem[this.shopSelectedItemIndex];
                                    if (var3 != -1) {
                                        int var4 = Integer.parseInt(input);
                                        this.clientStream.newPacket(236);
                                        this.clientStream.writeBuffer.putShort(var3);
                                        this.clientStream.writeBuffer.putShort(this.shopItemCount[this.shopSelectedItemIndex]);
                                        this.clientStream.writeBuffer.putShort(var4);
                                        this.clientStream.sendPacket();
                                    }
                                } catch (NumberFormatException var17) {
                                    ;
                                }
                            } else if (this.inputPopupType == 6) {
                                try {
                                    int var3 = this.shopItem[this.shopSelectedItemIndex];
                                    if (var3 != -1) {
                                        int var4 = Integer.parseInt(input);
                                        this.clientStream.newPacket(221);
                                        this.clientStream.writeBuffer.putShort(this.shopItem[this.shopSelectedItemIndex]);
                                        this.clientStream.writeBuffer.putShort(this.shopItemCount[this.shopSelectedItemIndex]);
                                        this.clientStream.writeBuffer.putShort(var4);
                                        this.clientStream.sendPacket();
                                    }
                                } catch (NumberFormatException var16) {
                                    ;
                                }
                            } else if (this.inputPopupType == 7) {
                                try {
                                    this.drawDialogDuel(Integer.parseInt(input), this.anInt1460);
                                } catch (NumberFormatException var15) {
                                    ;
                                }
                            } else if (this.inputPopupType == 8) {
                                try {
                                    this.updateDuelItems(this.anInt1460, 5, Integer.parseInt(input));
                                } catch (NumberFormatException var14) {
                                    ;
                                }
                            } else if (this.inputPopupType == 9) {
                                this.clientStream.newPacket(84);
                                this.clientStream.sendPacket();
                            }
                        } else {
                            try {
                                int var3;
                                if (this.bankSelectedItemSlot < 0) {
                                    var3 = -1;
                                } else {
                                    var3 = this.bankItems[this.bankSelectedItemSlot];
                                }

                                int var4 = Integer.parseInt(input);
                                this.clientStream.newPacket(23);
                                this.clientStream.writeBuffer.putShort(var3);
                                this.clientStream.writeBuffer.putInt(var4);
                                this.clientStream.writeBuffer.putInt(-0x789abcdf);
                                this.clientStream.sendPacket();
                            } catch (NumberFormatException var13) {
                                ;
                            }
                        }
                    } else {
                        try {
                            int var3;
                            if (this.bankSelectedItemSlot >= 0) {
                                var3 = this.bankItems[this.bankSelectedItemSlot];
                            } else {
                                var3 = -1;
                            }

                            int var4 = Integer.parseInt(input);
                            this.clientStream.newPacket(22);
                            this.clientStream.writeBuffer.putShort(var3);
                            this.clientStream.writeBuffer.putInt(var4);
                            this.clientStream.writeBuffer.putInt(0x12345678);
                            this.clientStream.sendPacket();
                        } catch (NumberFormatException var12) {
                            ;
                        }
                    }
                } else {
                    try {
                        this.updateTradeItems(this.anInt1393, Integer.parseInt(input), var1 + 29853);
                    } catch (NumberFormatException var11) {
                        ;
                    }
                }
            } else {
                try {
                    this.addTradeOffer(this.anInt1393, var1 + 29843, Integer.parseInt(input));
                } catch (NumberFormatException var10) {
                    ;
                }
            }

            this.inputPopupType = 0;
        }
        ;
    }

    private void drawOptionsMenu(int var1) {
        if (var1 != 11) {
            this.drawDialogReportAbuse((byte) -63);
        }

        int var2;
        if (this.mouseButtonClick == 0) {
            for (var2 = 0; this.optionMenuCount > var2; ++var2) {
                int var3 = '\uffff';
                if (super.mouseX < this.surface.textWidth(1, -127, this.optionMenuEntry[var2]) && (var2 * 12) < super.mouseY && super.mouseY < (12 + 12 * var2)) {
                    var3 = 0xff0000;
                }

                this.surface.drawstring(this.optionMenuEntry[var2], 6, var2 * 12 + 12, 1, var3);
            }

        } else {
            for (var2 = 0; this.optionMenuCount > var2; ++var2) {
                if (super.mouseX < this.surface.textWidth(1, -127, this.optionMenuEntry[var2]) && super.mouseY > 12 * var2 && var2 * 12 + 12 > super.mouseY) {
                    this.clientStream.newPacket(116);
                    this.clientStream.writeBuffer.putByte(var2);
                    this.clientStream.sendPacket();
                    break;
                }
            }

            this.mouseButtonClick = 0;
            this.showOptionMenu = false;
        }
        ;
    }

    private void drawGame(int var1) {
        if (this.deathScreenTimeout != 0) {
            this.surface.fade2black(var1 ^ -915697958);
            this.surface.drawstringCenter("Oh dear! You are dead...", 7, this.gameHeight / 2, this.gameWidth / 2, 0xff0000);
            this.drawChatMessageTabs(false);
            this.surface.draw(-2020315800, this.graphics, super.anInt58, super.anInt7);
        } else if (this.showAppearanceChange) {
            this.drawAppearancePanelCharacterSprites((byte) -28);
        } else if (this.isSleeping) {
            this.surface.fade2black(var1 ^ -915697958);
            if (Math.random() < 0.15D) {
                this.surface.drawstringCenter("ZZZ", 5, (int) (334.0D * Math.random()), (int) (80.0D * Math.random()), (int) (1.6777215E7D * Math.random()));
            }

            if (Math.random() < 0.15D) {
                this.surface.drawstringCenter("ZZZ", 5, (int) (334.0D * Math.random()), -((int) (Math.random() * 80.0D)) + 512, (int) (1.6777215E7D * Math.random()));
            }

            this.surface.drawBox(-100 + this.gameWidth / 2, 160, 200, 40, 0);
            this.surface.drawstringCenter("You are sleeping", 7, 50, this.gameWidth / 2, 0xffff00);
            this.surface.drawstringCenter("Fatigue: " + 100 * this.fatigueSleeping / 750 + "%", 7, 90, this.gameWidth / 2, 0xffff00);
            this.surface.drawstringCenter("When you want to wake up just use your", 5, 140, this.gameWidth / 2, 0xffffff);
            this.surface.drawstringCenter("keyboard to type the word in the box below", 5, 160, this.gameWidth / 2, 0xffffff);
            this.surface.drawstringCenter(super.inputTextCurrent + "*", 5, 180, this.gameWidth / 2, '\uffff');
            if (this.sleepingStatusText != null) {
                this.surface.drawstringCenter(this.sleepingStatusText, 5, 260, this.gameWidth / 2, 0xff0000);
            } else {
                this.surface.drawSprite((byte) 61, 230, 1 + this.spriteTexture, this.gameWidth / 2 + -127);
            }

            this.surface.drawBoxEdge(this.gameWidth / 2 + -128, 229, 257, 42, 0xffffff);
            this.drawChatMessageTabs(false);
            this.surface.drawstringCenter("If you can\'t read the word", 1, 290, this.gameWidth / 2, 0xffffff);
            this.surface.drawstringCenter("@yel@click here@whi@ to get a different one", 1, 305, this.gameWidth / 2, 0xffffff);
            this.surface.draw(-2020315800, this.graphics, super.anInt58, super.anInt7);
        } else if (this.world.playerAlive) {
            for (int var2 = 0; var2 < 64; ++var2) {
                this.scene.freeModel((byte) 102, this.world.aGameModelArrayArray236[this.lastHeightOffset][var2]);
                if (this.lastHeightOffset == 0) {
                    this.scene.freeModel((byte) 110, this.world.aGameModelArrayArray246[1][var2]);
                    this.scene.freeModel((byte) 120, this.world.aGameModelArrayArray236[1][var2]);
                    this.scene.freeModel((byte) 108, this.world.aGameModelArrayArray246[2][var2]);
                    this.scene.freeModel((byte) 101, this.world.aGameModelArrayArray236[2][var2]);
                }

                this.fogOfWar = true;
                if (this.lastHeightOffset == 0 && (128 & this.world.anIntArrayArray276[this.localPlayer.currentX / 128][this.localPlayer.currentY / 128]) == 0) {
                    this.scene.method325(this.world.aGameModelArrayArray236[this.lastHeightOffset][var2], (byte) 6);
                    if (this.lastHeightOffset == 0) {
                        this.scene.method325(this.world.aGameModelArrayArray246[1][var2], (byte) 6);
                        this.scene.method325(this.world.aGameModelArrayArray236[1][var2], (byte) 6);
                        this.scene.method325(this.world.aGameModelArrayArray246[2][var2], (byte) 6);
                        this.scene.method325(this.world.aGameModelArrayArray236[2][var2], (byte) 6);
                    }

                    this.fogOfWar = false;
                }
            }

            int var3;
            if (this.objectAnimationNumberFireLightningSpell != this.lastObjectAnimationNumberFireLightningSpell) {
                this.lastObjectAnimationNumberFireLightningSpell = this.objectAnimationNumberFireLightningSpell;

                for (var3 = 0; var3 < this.objectCount; ++var3) {
                    if (this.objectId[var3] == 97) {
                        this.updateObjectAnimation(var3, false, "firea" + (1 + this.objectAnimationNumberFireLightningSpell));
                    }

                    if (this.objectId[var3] == 274) {
                        this.updateObjectAnimation(var3, false, "fireplacea" + (1 + this.objectAnimationNumberFireLightningSpell));
                    }

                    if (this.objectId[var3] == 1031) {
                        this.updateObjectAnimation(var3, false, "lightning" + (this.objectAnimationNumberFireLightningSpell + 1));
                    }

                    if (this.objectId[var3] == 1036) {
                        this.updateObjectAnimation(var3, false, "firespell" + (this.objectAnimationNumberFireLightningSpell + 1));
                    }

                    if (this.objectId[var3] == 1147) {
                        this.updateObjectAnimation(var3, false, "spellcharge" + (1 + this.objectAnimationNumberFireLightningSpell));
                    }
                }
            }

            if (this.lastObjectAnimationNumberTorch != this.objectAnimationNumberTorch) {
                this.lastObjectAnimationNumberTorch = this.objectAnimationNumberTorch;

                for (var3 = 0; var3 < this.objectCount; ++var3) {
                    if (this.objectId[var3] == 51) {
                        this.updateObjectAnimation(var3, false, "torcha" + (1 + this.objectAnimationNumberTorch));
                    }

                    if (this.objectId[var3] == 143) {
                        this.updateObjectAnimation(var3, false, "skulltorcha" + (1 + this.objectAnimationNumberTorch));
                    }
                }
            }

            if (this.lastOjectAnimationNumberClaw != this.objectAnimationNumberClaw) {
                this.lastOjectAnimationNumberClaw = this.objectAnimationNumberClaw;

                for (var3 = 0; var3 < this.objectCount; ++var3) {
                    if (this.objectId[var3] == 1142) {
                        this.updateObjectAnimation(var3, false, "clawspell" + (1 + this.objectAnimationNumberClaw));
                    }
                }
            }

            this.scene.reduceSprites(this.spriteCount, (byte) -39);
            this.spriteCount = 0;

            int var5;
            int var6;
            int var7;
            int var8;
            for (var3 = 0; var3 < this.playerCount; ++var3) {
                Character var4 = this.players[var3];
                if (var4.colourBottom != 255) {
                    var5 = var4.currentX;
                    var6 = var4.currentY;
                    var7 = -this.world.getElevation(var5, (byte) -56, var6);
                    var8 = this.scene.drawSprite(220, 2, 145, 5000 - -var3, 10000 + var3, var6, var5, var7);
                    ++this.spriteCount;
                    if (var4 == this.localPlayer) {
                        this.scene.setFaceSpriteLocalPlayer(var8, -32653);
                    }

                    if (var4.animationCurrent == 8) {
                        this.scene.setCombatXOffset(var8, (byte) -30, -30);
                    }

                    if (var4.animationCurrent == 9) {
                        this.scene.setCombatXOffset(var8, (byte) -30, 30);
                    }
                }
            }

            int var9;
            int var10;
            Character var19;
            for (int var17 = 0; var17 < this.playerCount; ++var17) {
                Character var18 = this.players[var17];
                if (var18.projectileRange > 0) {
                    var19 = null;
                    if (var18.attackingNpcServerIndex != -1) {
                        var19 = this.npcsServer[var18.attackingNpcServerIndex];
                    } else if (var18.attackingPlayerServerIndex != -1) {
                        var19 = this.playerServer[var18.attackingPlayerServerIndex];
                    }

                    if (var19 != null) {
                        var7 = var18.currentX;
                        var8 = var18.currentY;
                        var9 = -this.world.getElevation(var7, (byte) -56, var8) + -110;
                        var10 = var19.currentX;
                        int var11 = var19.currentY;
                        int var12 = -this.world.getElevation(var10, (byte) -56, var11) - GameData.npcHeight[var19.npcId] / 2;
                        int var13 = ((-var18.projectileRange + this.projectileMaxRange) * var10 + var7 * var18.projectileRange) / this.projectileMaxRange;
                        int var14 = (var12 * (-var18.projectileRange + this.projectileMaxRange) + var9 * var18.projectileRange) / this.projectileMaxRange;
                        int var15 = (var8 * var18.projectileRange + var11 * (-var18.projectileRange + this.projectileMaxRange)) / this.projectileMaxRange;
                        this.scene.drawSprite(32, 2, 32, this.spriteProjectile - -var18.incomingProjectileSprite, 0, var15, var13, var14);
                        ++this.spriteCount;
                    }
                }
            }

            for (var5 = 0; var5 < this.npcCount; ++var5) {
                var19 = this.npcs[var5];
                var7 = var19.currentX;
                var8 = var19.currentY;
                var9 = -this.world.getElevation(var7, (byte) -56, var8);
                var10 = this.scene.drawSprite(GameData.npcHeight[var19.npcId], 2, GameData.npcWidth[var19.npcId], 20000 - -var5, var5 + 30000, var8, var7, var9);
                ++this.spriteCount;
                if (var19.animationCurrent == 8) {
                    this.scene.setCombatXOffset(var10, (byte) -30, -30);
                }

                if (var19.animationCurrent == 9) {
                    this.scene.setCombatXOffset(var10, (byte) -30, 30);
                }
            }

            for (var6 = 0; this.groundItemCount > var6; ++var6) {
                var7 = this.groundItemX[var6] * this.magicLoc - -64;
                var8 = 64 + this.groundItemY[var6] * this.magicLoc;
                this.scene.drawSprite(64, var1 + -17916, 96, this.groundItemId[var6] + '\u9c40', 20000 + var6, var8, var7, -this.world.getElevation(var7, (byte) -56, var8) + -this.groundItemZ[var6]);
                ++this.spriteCount;
            }

            for (var7 = 0; var7 < this.teleportBubbleCount; ++var7) {
                var8 = 64 + this.magicLoc * this.teleportBubbleX[var7];
                var9 = 64 + this.teleportBubbleY[var7] * this.magicLoc;
                var10 = this.teleportBubbleType[var7];
                if (var10 == 0) {
                    this.scene.drawSprite(256, 2, 128, '\uc350' - -var7, var7 + '\uc350', var9, var8, -this.world.getElevation(var8, (byte) -56, var9));
                    ++this.spriteCount;
                }

                if (var10 == 1) {
                    this.scene.drawSprite(64, var1 + -17916, 128, '\uc350' - -var7, '\uc350' + var7, var9, var8, -this.world.getElevation(var8, (byte) -56, var9));
                    ++this.spriteCount;
                }
            }

            this.surface.interlace = false;
            this.surface.blackScreen((byte) 127);
            this.surface.interlace = super.interlace;
            if (this.lastHeightOffset == 3) {
                var8 = (int) (Math.random() * 3.0D) + 40;
                var9 = 40 + (int) (Math.random() * 7.0D);
                this.scene.method327(-50, -1, -50, -10, var9, var8);
            }

            this.playerMessagesCount = 0;
            this.anInt1461 = 0;
            this.anInt1365 = 0;
            if (this.cameraAutoAngleDebug) {
                if (this.optionCameraModeAuto && !this.fogOfWar) {
                    var8 = this.cameraAngle;
                    this.autorotateCamera((byte) 57);
                    if (var8 != this.cameraAngle) {
                        this.cameraAutoRotatePlayerX = this.localPlayer.currentY;
                        this.cameraAutoRotatePlayerY = this.localPlayer.currentX;
                    }
                }

                this.scene.anInt317 = 3000;
                this.scene.fogZFalloff = 1;
                this.scene.anInt367 = 3000;
                this.cameraRotation = 32 * this.cameraAngle;
                this.scene.fogZDistance = 2800;
                var8 = this.cameraAutoRotatePlayerY - -this.cameraRotationY;
                var9 = this.cameraRotationX + this.cameraAutoRotatePlayerX;
                this.scene.setCamera(var8, this.cameraRotation * 4, 0, (byte) 98, 2000, -this.world.getElevation(var8, (byte) -56, var9), var9, 912);
            } else {
                if (this.optionCameraModeAuto && !this.fogOfWar) {
                    this.autorotateCamera((byte) 33);
                }

                if (super.interlace) {
                    this.scene.fogZDistance = 2100;
                    this.scene.anInt317 = 2200;
                    this.scene.anInt367 = 2200;
                    this.scene.fogZFalloff = 1;
                } else {
                    this.scene.fogZFalloff = 1;
                    this.scene.fogZDistance = 2300;
                    this.scene.anInt367 = 2400;
                    this.scene.anInt317 = 2400;
                }

                var8 = this.cameraRotationY + this.cameraAutoRotatePlayerY;
                var9 = this.cameraAutoRotatePlayerX - -this.cameraRotationX;
                this.scene.setCamera(var8, 4 * this.cameraRotation, 0, (byte) 98, 2 * this.cameraZoom, -this.world.getElevation(var8, (byte) -56, var9), var9, 912);
            }

            this.scene.endscene(false);
            this.drawAboveHeadStuff(var1 ^ -17866);
            if (this.mouseClickXStep > 0) {
                this.surface.drawSprite((byte) 61, this.anInt1265 - 8, 14 + this.spriteMedia + (24 - this.mouseClickXStep) / 6, this.anInt1289 - 8);
            }

            if (this.mouseClickXStep < 0) {
                this.surface.drawSprite((byte) 61, -8 + this.anInt1265, (this.mouseClickXStep + 24) / 6 + 18 + this.spriteMedia, this.anInt1289 - 8);
            }

            if (var1 != 17918) {
                this.inputPopupShowInput = true;
            }

            if (this.systemUpdate != 0) {
                var8 = this.systemUpdate / 50;
                var9 = var8 / 60;
                var8 %= 60;
                if (var8 < 10) {
                    this.surface.drawstringCenter("System update in: " + var9 + ":0" + var8, 1, -7 + this.gameHeight, 256, 0xffff00);
                } else {
                    this.surface.drawstringCenter("System update in: " + var9 + ":" + var8, 1, -7 + this.gameHeight, 256, 0xffff00);
                }
            }

            if (!this.loadingArea) {
                var8 = -this.regionY - (this.localRegionY - -this.planeHeight) + 2203;
                if (this.regionX + this.localRegionX - -this.planeWidth >= 2640) {
                    var8 = -50;
                }

                if (var8 > 0) {
                    this.surface.drawSprite((byte) 61, this.gameHeight + -56, this.spriteMedia - -13, 453);
                    var9 = 1 + var8 / 6;
                    this.surface.drawstringCenter("Wilderness", 1, this.gameHeight - 20, 465, 0xffff00);
                    this.surface.drawstringCenter("Level: " + var9, 1, -7 + this.gameHeight, 465, 0xffff00);
                    if (this.showUiWildWarn == 0) {
                        this.showUiWildWarn = 2;
                    }
                }

                if (this.showUiWildWarn == 0 && var8 > -10 && var8 <= 0) {
                    this.showUiWildWarn = 1;
                }
            }

            if (this.messageTabSelected == 0) {
                for (var8 = 0; var8 < 100; ++var8) {
                    if (messageHistoryTimeout[var8] > 0) {
                        String var20 = messageColor[var8] + formatMessage(messageMessages[var8], messageSenders[var8], messageTypes[var8]);
                        this.surface.drawstring(var20, 7, -18 + this.gameHeight + -(12 * var8), 1, 0xffff00, messageCrowns[var8]);
                    }
                }
            }

            this.panelMessageTabs.hide(this.controlTextListChat, var1 + -17930);
            this.panelMessageTabs.hide(this.controlTextListQuest, -12);
            this.panelMessageTabs.hide(this.controlTextListPrivate, -12);
            if (this.messageTabSelected != 1) {
                if (this.messageTabSelected == 2) {
                    this.panelMessageTabs.show(this.controlTextListQuest, (byte) -73);
                } else if (this.messageTabSelected == 3) {
                    this.panelMessageTabs.show(this.controlTextListPrivate, (byte) -112);
                }
            } else {
                this.panelMessageTabs.show(this.controlTextListChat, (byte) -77);
            }

            Panel.textListEntryHeightMod = 2;
            this.panelMessageTabs.drawPanel(114);
            Panel.textListEntryHeightMod = 0;
            this.surface.drawSpriteAlpha(-197 + this.surface.width2 + -3, 3, this.spriteMedia, 128);
            this.drawUi();
            this.surface.loggedIn = false;
            this.drawChatMessageTabs(false);
            drawhud();
            this.surface.draw(var1 ^ -2020333418, this.graphics, super.anInt58, super.anInt7);
        }
    }

    private void drawhud() {
        int y = 100;
        this.surface.drawstring(String.format("regionX %d, regionY %d", regionX, regionY), 20, y, 1, 0xffffff);
        y += 15;
        this.surface.drawstring(String.format("localRegionX %d, localRegionY %d", localRegionX, localRegionY), 20, y, 1, 0xffffff);
        y += 15;
        this.surface.drawstring(String.format("x %d, y %d", regionX + localRegionX, regionY + localRegionY), 20, y, 1, 0xffffff);
        y += 15;
        this.surface.drawstring(String.format("w %d, h %d", planeWidth, planeHeight), 20, y, 1, 0xffffff);
        y += 15;
        if (showUiTab == 2) {
            this.surface.drawstring(String.format("lmx %d, lmy %d", mouseMinimapX, mouseMinimapY), 20, y, 1, 0xffffff);
            y += 15;
            this.surface.drawstring(String.format("mx %d, my %d", mouseMinimapX + regionX, mouseMinimapY + regionY), 20, y, 1, 0xffffff);
        } else {
            this.surface.drawstring(String.format("lmx %d, lmy %d", this.menuCommon.getMenuItemX(0),
                    this.menuCommon.getMenuItemY(0)), 20, y, 1, 0xffffff);
            y += 15;
            this.surface.drawstring(String.format("mx %d, my %d", this.menuCommon.getMenuItemX(0) + regionX,
                    this.menuCommon.getMenuItemY(0) + regionY), 20, y, 1, 0xffffff);
        }
    }

    private void drawDialogLogout(int var1) {
        if (var1 > -103) {
            this.teleportBubbleY = null;
        }

        this.surface.drawBox(126, 137, 260, 60, 0);
        this.surface.drawBoxEdge(126, 137, 260, 60, 0xffffff);
        this.surface.drawstringCenter("Logging out...", 5, 173, 256, 0xffffff);
    }

    private void loadTextures(int var1) {
        byte[] buffTextures = this.readDataFile(11, 50, "Textures", -10);
        if (buffTextures == null) {
            this.errorLoadingData = true;
        } else {
            byte[] buffIndex = Utility.loadData(buffTextures, 0, "index.dat");
            this.scene.allocateTextures(7, var1, (byte) 46, GameData.textureCount);

            for (int i = 0; i < GameData.textureCount; ++i) {
                String name = GameData.textureName[i];
                byte[] buff = Utility.loadData(buffTextures, 0, name + ".dat");
                this.surface.parseSprite(buffIndex, this.spriteTexture, (byte) 118, buff, 1);
                this.surface.drawBox(0, 0, 128, 128, 16711935);
                this.surface.drawSprite((byte) 61, 0, this.spriteTexture, 0);
                int wh = this.surface.spriteWidthFull[this.spriteTexture];
                String nameSub = GameData.textureSubtypeName[i];
                if (nameSub != null && nameSub.length() > 0) {
                    buff = Utility.loadData(buffTextures, 0, nameSub + ".dat");
                    this.surface.parseSprite(buffIndex, this.spriteTexture, (byte) 80, buff, 1);
                    this.surface.drawSprite((byte) 61, 0, this.spriteTexture, 0);
                }

                this.surface.drawSprite((byte) 117, wh, this.spriteTextureWorld - -i, wh, 0, 0);
                int var9 = wh * wh;

                for (int var10 = 0; var9 > var10; ++var10) {
                    if (this.surface.surfacePixels[i + this.spriteTextureWorld][var10] == '\uff00') {
                        this.surface.surfacePixels[i + this.spriteTextureWorld][var10] = 16711935;
                    }
                }

                this.surface.drawWorld(var1 ^ -268, i + this.spriteTextureWorld);
                this.scene.loadTexture(117, i, this.surface.spriteColoursUsed[i + this.spriteTextureWorld], this.surface.spriteColourList[i + this.spriteTextureWorld], wh / 64 + -1);
            }

        }
    }

    private void ignoreRemove(String name) {
        String formattedName = Utility.formatName(name);
        if (formattedName != null) {
            for (int i = 0; ignoreListCount > i; ++i) {
                if (formattedName.equals(Utility.formatName(ignoreListAccNames[i]))) {
                    --ignoreListCount;

                    for (int j = i; ignoreListCount > j; ++j) {
                        ignoreListNames[j] = ignoreListNames[1 + j];
                        ignoreListAccNames[j] = ignoreListAccNames[j - -1];
                        ignoreListOldNames[j] = ignoreListOldNames[1 + j];
                        ignoreListServers[j] = ignoreListServers[j];
                    }

                    this.clientStream.newPacket(241);
                    this.clientStream.writeBuffer.pjstr2(name);
                    this.clientStream.sendPacket();
                    return;
                }
            }

        }
    }

    private void method85(boolean var1, int var2) {
        int var3 = this.surface.width2 - 248;
        this.surface.drawSprite((byte) 61, 3, this.spriteMedia + 1, var3);

        int var5;
        int var6;
        for (int var4 = 0; var4 < this.anInt1535; ++var4) {
            var5 = var3 - -(var4 % 5 * 49);
            var6 = 34 * (var4 / 5) + 36;
            if (var4 < this.inventoryItemsCount && this.inventoryItemEquipped[var4] == 1) {
                this.surface.drawBoxAlpha(var5, var6, 49, 34, 0xff0000, 128);
            } else {
                this.surface.drawBoxAlpha(var5, var6, 49, 34, Surface.rgb2long(181, 181, 181), 128);
            }

            if (this.inventoryItemsCount > var4) {
                this.surface.method409(GameData.itemMask[this.inventoryItemId[var4]], this.spriteItem - -GameData.itemPicture[this.inventoryItemId[var4]], var6, -36, 32, 0, 48, var5, false, 0);
                if (GameData.itemStackable[this.inventoryItemId[var4]] == 0) {
                    this.surface.drawstring(String.valueOf(this.inventoryItemStackCount[var4]), var5 + 1, 10 + var6, 1, 0xffff00);
                }
            }
        }

        for (var5 = 1; var5 <= 4; ++var5) {
            this.surface.drawLineVert(49 * var5 + var3, 36, 34 * (this.anInt1535 / 5), 0);
        }

        for (var6 = 1; var6 <= (-1 + this.anInt1535 / 5); ++var6) {
            this.surface.drawLineHoriz(var3, var6 * 34 + 36, 245, 0);
        }

        if (var2 != -6135) {
            this.method91(-54, -102, -92, true, -47, -111, -110);
        }

        if (var1) {
            var3 = super.mouseX - -248 - this.surface.width2;
            int var7 = -36 + super.mouseY;
            if (var3 >= 0 && var7 >= 0 && var3 < 248 && (this.anInt1535 / 5 * 34) > var7) {
                int var8 = var3 / 49 - -(var7 / 34 * 5);
                if (this.inventoryItemsCount > var8) {
                    int var9 = this.inventoryItemId[var8];
                    if (this.selectedSpell >= 0) {
                        if (GameData.spellType[this.selectedSpell] == 3) {
                            this.menuCommon.addItem(this.selectedSpell, 600, "Cast " + GameData.spellName[this.selectedSpell] + " on", "@lre@" + GameData.itemName[var9], var8);
                            return;
                        }
                    } else {
                        if (this.selectedItemInventoryIndex >= 0) {
                            this.menuCommon.addItem(this.selectedItemInventoryIndex, 610, "Use " + this.selectedItemName + " with", "@lre@" + GameData.itemName[var9], var8);
                            return;
                        }

                        if (this.inventoryItemEquipped[var8] != 1) {
                            if (GameData.itemWearable[var9] != 0) {
                                String var10;
                                if ((24 & GameData.itemWearable[var9]) == 0) {
                                    var10 = "Wear";
                                } else {
                                    var10 = "Wield";
                                }

                                this.menuCommon.addItem(630, var10, var8, "@lre@" + GameData.itemName[var9]);
                            }
                        } else {
                            this.menuCommon.addItem(620, "Remove", var8, "@lre@" + GameData.itemName[var9]);
                        }

                        if (!GameData.itemCommand[var9].equals("")) {
                            this.menuCommon.addItem(640, GameData.itemCommand[var9], var8, "@lre@" + GameData.itemName[var9]);
                        }

                        this.menuCommon.addItem(650, "Use", var8, "@lre@" + GameData.itemName[var9]);
                        this.menuCommon.addItem(660, "Drop", var8, "@lre@" + GameData.itemName[var9]);
                        this.menuCommon.addItem(3600, "Examine", var9, "@lre@" + GameData.itemName[var9]);
                    }

                    return;
                }
            }

        }
    }

    private void sortFriendList(byte var1) {
        boolean go = true;

        while (go) {
            go = false;

            for (int index = 0; index < (friendListCount - 1); ++index) {
                if ((friendListOnline[index] & 2) == 0 && (friendListOnline[index + 1] & 2) != 0 || (friendListOnline[index] & 4) == 0 && (friendListOnline[index + 1] & 4) != 0) {
                    String server = friendListServer[index];
                    friendListServer[index] = friendListServer[index + 1];
                    friendListServer[index + 1] = server;
                    String name = friendListNames[index];
                    friendListNames[index] = friendListNames[index + 1];
                    friendListNames[index + 1] = name;
                    String oldName = friendListOldNames[index];
                    friendListOldNames[index] = friendListOldNames[index + 1];
                    friendListOldNames[index + 1] = oldName;
                    int online = friendListOnline[index];
                    friendListOnline[index] = friendListOnline[index + 1];
                    friendListOnline[index + 1] = online;
                    go = true;
                }
            }
        }
        ;
    }

    private void drawDialogWelcome(byte var1) {
        int var2 = 65;
        if (this.welcomeRecoverySetDays != 201) {
            var2 += 60;
        }

        if (this.welcomeUnreadMessages > 0) {
            var2 += 30;
        }

        if (this.welcomeLastLoggedInIp != 0) {
            var2 += 45;
        }

        int var3 = -(var2 / 2) + 167;
        this.surface.drawBox(56, 167 + -(var2 / 2), 400, var2, 0);
        this.surface.drawBoxEdge(56, 167 - var2 / 2, 400, var2, 0xffffff);
        var3 += 20;
        this.surface.drawstringCenter("Welcome to RuneScape " + this.localPlayer.accountName, 4, var3, 256, 0xffff00);
        var3 += 30;
        String var4;
        if (this.welcomeLastLoggedInDays != 0) {
            if (this.welcomeLastLoggedInDays != 1) {
                var4 = this.welcomeLastLoggedInDays + " days ago";
            } else {
                var4 = "yesterday";
            }
        } else {
            var4 = "earlier today";
        }

        if (this.welcomeLastLoggedInIp != 0) {
            this.surface.drawstringCenter("You last logged in " + var4, 1, var3, 256, 0xffffff);
            var3 += 15;
            if (this.welcomeLastLoggedInHost == null) {
                this.welcomeLastLoggedInHost = this.getHostnameIp(this.welcomeLastLoggedInIp, 114);
            }

            this.surface.drawstringCenter("from: " + this.welcomeLastLoggedInHost, 1, var3, 256, 0xffffff);
            var3 += 15;
            var3 += 15;
        }

        if (this.welcomeUnreadMessages > 0) {
            if (this.welcomeUnreadMessages == 1) {
                this.surface.drawstringCenter("You have @yel@0@whi@ unread messages in your message-centre", 1, var3, 256, 0xffffff);
            } else {
                this.surface.drawstringCenter("You have @gre@" + (this.welcomeUnreadMessages + -1) + " unread messages @whi@in your message-centre", 1, var3, 256, 0xffffff);
            }

            var3 += 15;
            var3 += 15;
        }

        if (this.welcomeRecoverySetDays != 201) {
            if (this.welcomeRecoverySetDays == 200) {
                this.surface.drawstringCenter("You have not yet set any password recovery questions.", 1, var3, 256, 0xff8000);
                var3 += 15;
                this.surface.drawstringCenter("We strongly recommend you do so now to secure your account.", 1, var3, 256, 0xff8000);
                var3 += 15;
                this.surface.drawstringCenter("Do this from the \'account management\' area on our front webpage", 1, var3, 256, 0xff8000);
                var3 += 15;
            } else {
                if (this.welcomeRecoverySetDays != 0) {
                    if (this.welcomeRecoverySetDays == 1) {
                        var4 = "Yesterday";
                    } else {
                        var4 = this.welcomeRecoverySetDays + " days ago";
                    }
                } else {
                    var4 = "Earlier today";
                }

                this.surface.drawstringCenter(var4 + " you changed your recovery questions", 1, var3, 256, 0xff8000);
                var3 += 15;
                this.surface.drawstringCenter("If you do not remember making this change then cancel it immediately", 1, var3, 256, 0xff8000);
                var3 += 15;
                this.surface.drawstringCenter("Do this from the \'account management\' area on our front webpage", 1, var3, 256, 0xff8000);
                var3 += 15;
            }

            var3 += 15;
        }

        int var5 = 0xffffff;
        if (var1 < 8) {
            this.loadEntities(-104);
        }

        if (-12 + var3 < super.mouseY && super.mouseY <= var3 && super.mouseX > 106 && super.mouseX < 406) {
            var5 = 0xff0000;
        }

        this.surface.drawstringCenter("Click here to close window", 1, var3, 256, var5);
        if (this.mouseButtonClick == 1) {
            if (var5 == 0xff0000) {
                this.showDialogMessage = false;
            }

            if ((super.mouseX < 86 || super.mouseX > 426) && (super.mouseY < (-(var2 / 2) + 167) || super.mouseY > 167 - -(var2 / 2))) {
                this.showDialogMessage = false;
            }
        }

        this.mouseButtonClick = 0;
    }

    private void lostConnection(int var1) {
        if (var1 != -12) {
            this.showDialogTradeConfirm = false;
        }

        this.systemUpdate = 0;
        if (this.logoutTimeout != 0) {
            this.resetLoginVars((byte) 51);
        } else {
            System.out.println("Lost connection");
            this.autoLoginTimeout = 10;
            this.login(true, this.username, this.password, (byte) -57);
        }
    }

    private void cantLogout(byte var1) {
        this.logoutTimeout = 0;
        if (var1 == 59) {
            this.showMessage(0, "Sorry, you can\'t logout at the moment", (String) null, (String) null, 0, "@cya@", false);
        }
    }

    private void updateTradeItems(int var1, int var2, int var3) {
        int var4 = this.tradeItems[var1];
        int var5 = var2 < 0 ? this.mouseButtonItemCountIncrement : var2;
        int var6;
        if (GameData.itemStackable[var4] != 0) {
            var6 = 0;

            for (int var7 = 0; this.tradeItemsCount > var7 && var6 < var5; ++var7) {
                if (var4 == this.tradeItems[var7]) {
                    ++var6;
                    --this.tradeItemsCount;

                    for (int var8 = var7; var8 < this.tradeItemsCount; ++var8) {
                        this.tradeItems[var8] = this.tradeItems[var8 - -1];
                        this.tradeItemCount[var8] = this.tradeItemCount[var8 + 1];
                    }

                    --var7;
                }
            }
        } else {
            this.tradeItemCount[var1] -= var5;
            if (this.tradeItemCount[var1] <= 0) {
                --this.tradeItemsCount;

                for (var6 = var1; var6 < this.tradeItemsCount; ++var6) {
                    this.tradeItems[var6] = this.tradeItems[1 + var6];
                    this.tradeItemCount[var6] = this.tradeItemCount[1 + var6];
                }
            }
        }

        this.clientStream.newPacket(46);
        this.clientStream.writeBuffer.putByte(this.tradeItemsCount);

        for (var6 = 0; var6 < this.tradeItemsCount; ++var6) {
            this.clientStream.writeBuffer.putShort(this.tradeItems[var6]);
            this.clientStream.writeBuffer.putInt(this.tradeItemCount[var6]);
        }

        this.clientStream.sendPacket();
        if (var3 >= 96) {
            this.tradeAccepted = false;
            this.tradeRecipientAccepted = false;
        }
        ;
    }

    final void method91(int var1, int var2, int var3, boolean var4, int var5, int var6, int var7) {
        if (var4) {
            int var8 = this.spriteItem + GameData.itemPicture[var5];
            int var9 = GameData.itemMask[var5];
            this.surface.method409(var9, var8, var1, -41, var3, 0, var2, var6, false, 0);
        }
    }

    private void friendAdd(int var1, String name) {
        if (friendListCount >= (this.members ? 200 : 100)) {
            this.showMessage(0, "Friend list is full", (String) null, (String) null, 0, (String) null, false);
        } else {
            String formattedName = Utility.formatName(name);
            if (formattedName != null) {
                for (int i = 0; i < friendListCount; ++i) {
                    if (formattedName.equals(Utility.formatName(friendListNames[i]))) {
                        this.showMessage(0, name + " is already on your friend list.", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }

                    if (friendListOldNames[i] != null && formattedName.equals(Utility.formatName(friendListOldNames[i]))) {
                        this.showMessage(0, name + " is already on your friend list.", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }
                }

                for (int i = 0; i < ignoreListCount; ++i) {
                    if (formattedName.equals(Utility.formatName(ignoreListNames[i]))) {
                        this.showMessage(0, "Please remove " + name + " from your ignore list first.", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }

                    if (ignoreListOldNames[i] != null && formattedName.equals(Utility.formatName(ignoreListOldNames[i]))) {
                        this.showMessage(0, "Please remove " + name + " from your ignore list first.", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }
                }

                if (formattedName.equals(Utility.formatName(this.localPlayer.accountName))) {
                    this.showMessage(0, "You can\'t add yourself to your own friend list.", (String) null, (String) null, 0, (String) null, false);
                } else {
                    this.clientStream.newPacket(195);
                    this.clientStream.writeBuffer.pjstr2(name);
                    this.clientStream.sendPacket();
                }
            }
        }
    }

    private void walkToActionSource(boolean var1, int var2, int var3, int var4, int var5, byte var6) {
        if (var6 != 102) {
            this.cameraAngle = -88;
        }

        this.walkToSource(var2, var5, var3, -59, var2, var4, false, var1, var3);
    }

    public final void init() {
        nodeid = Integer.parseInt(this.getParameter("nodeid"));
        modewhere = GameModeWhere.method239(-102, Integer.parseInt(this.getParameter("modewhere")));
        if (modewhere == null) {
            modewhere = GameModeWhere.LIVE;
        }

        modewhat = GameModeWhat.method337(Integer.parseInt(this.getParameter("modewhat")), true);
        if (modewhat == null) {
            modewhat = GameModeWhat.LIVE;
        }

        super.startApplet(32 - -modewhat.id, 12 + this.gameHeight, 23527, ClientStream.version, this.gameWidth);
    }

    private void closeConnection(boolean var1, int var2) {
        if (var1 && this.clientStream != null) {
            try {
                this.clientStream.newPacket(31);
                this.clientStream.flushPacket();
            } catch (IOException var4) {
                ;
            }
        }

        this.password = "";
        this.username = "";
        this.resetLoginVars((byte) 75);
        if (var2 != -10) {
            this.walkTo(51, 38, -106, -113, -16, 118, false, true, (byte) 35);
        }
    }

    private void method95(int var1, int var2, int var3, int var4, int var5) {
        if (var1 != 5) {
            this.handleKeyPress((byte) -106, 127);
        }

        int var6;
        int var7;
        if (var4 != 0 && var4 != 4) {
            var7 = GameData.objectHeight[var5];
            var6 = GameData.objectWidth[var5];
        } else {
            var6 = GameData.objectHeight[var5];
            var7 = GameData.objectWidth[var5];
        }

        if (GameData.objectType[var5] != 2 && GameData.objectType[var5] != 3) {
            this.walkToSource(var2, this.localRegionY, var3, 124, -1 + var7 + var2, this.localRegionX, true, true, -1 + var3 + var6);
        } else {
            if (var4 == 0) {
                --var2;
                ++var7;
            }

            if (var4 == 2) {
                ++var6;
            }

            if (var4 == 6) {
                --var3;
                ++var6;
            }

            if (var4 == 4) {
                ++var7;
            }

            this.walkToSource(var2, this.localRegionY, var3, var1 + 88, var2 + var7 - 1, this.localRegionX, false, true, var6 + var3 + -1);
        }
        ;
    }

    private void menuItemClickl(int var1, byte var2) {
        int mitemid = this.menuCommon.getItemMenuIndex(var1);
        int mx = this.menuCommon.getMenuItemX(var1);
        if (var2 != -14) {
            this.duelItemCount = null;
        }

        int my = this.menuCommon.getMenuItemY(var1);
        int midx = this.menuCommon.getItemParam2(var1);
        int msrcidx = this.menuCommon.getItemParam3(var1);
        int var8 = this.menuCommon.getItemParam4(var1);
        String var9 = this.menuCommon.getItemText3(var1);
        if (mitemid == 200) {
            this.walkToGroundItem(my, this.localRegionY, this.localRegionX, true, 0, mx);
            this.clientStream.newPacket(249);
            this.clientStream.writeBuffer.putShort(this.regionX + mx);
            this.clientStream.writeBuffer.putShort(my + this.regionY);
            this.clientStream.writeBuffer.putShort(midx);
            this.clientStream.writeBuffer.putShort(msrcidx);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 210) {
            this.walkToGroundItem(my, this.localRegionY, this.localRegionX, true, 0, mx);
            this.clientStream.newPacket(53);
            this.clientStream.writeBuffer.putShort(mx - -this.regionX);
            this.clientStream.writeBuffer.putShort(this.regionY + my);
            this.clientStream.writeBuffer.putShort(midx);
            this.clientStream.writeBuffer.putShort(msrcidx);
            this.clientStream.sendPacket();
            this.selectedItemInventoryIndex = -1;
        }

        if (mitemid == 220) {
            this.walkToGroundItem(my, this.localRegionY, this.localRegionX, true, 0, mx);
            this.clientStream.newPacket(247);
            this.clientStream.writeBuffer.putShort(this.regionX + mx);
            this.clientStream.writeBuffer.putShort(my + this.regionY);
            this.clientStream.writeBuffer.putShort(midx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 3600 || mitemid == 3200) {
            this.showMessage(0, GameData.itemDescription[mx], (String) null, (String) null, 0, (String) null, false);
        }

        if (mitemid == 300) {
            this.method131(mx, my, 56, midx);
            this.clientStream.newPacket(180);
            this.clientStream.writeBuffer.putShort(this.regionX + mx);
            this.clientStream.writeBuffer.putShort(my - -this.regionY);
            this.clientStream.writeBuffer.putByte(midx);
            this.clientStream.writeBuffer.putShort(msrcidx);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 310) {
            this.method131(mx, my, 56, midx);
            this.clientStream.newPacket(161);
            this.clientStream.writeBuffer.putShort(mx - -this.regionX);
            this.clientStream.writeBuffer.putShort(my + this.regionY);
            this.clientStream.writeBuffer.putByte(midx);
            this.clientStream.writeBuffer.putShort(msrcidx);
            this.clientStream.sendPacket();
            this.selectedItemInventoryIndex = -1;
        }

        if (mitemid == 320) {
            this.method131(mx, my, 56, midx);
            this.clientStream.newPacket(14);
            this.clientStream.writeBuffer.putShort(mx - -this.regionX);
            this.clientStream.writeBuffer.putShort(my + this.regionY);
            this.clientStream.writeBuffer.putByte(midx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 2300) {
            this.method131(mx, my, 56, midx);
            this.clientStream.newPacket(127);
            this.clientStream.writeBuffer.putShort(mx + this.regionX);
            this.clientStream.writeBuffer.putShort(my + this.regionY);
            this.clientStream.writeBuffer.putByte(midx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 3300) {
            this.showMessage(0, GameData.wallObjectDescription[mx], (String) null, (String) null, 0, (String) null, false);
        }

        if (mitemid == 400) {
            this.method95(5, mx, my, midx, msrcidx);
            this.clientStream.newPacket(99);
            this.clientStream.writeBuffer.putShort(mx - -this.regionX);
            this.clientStream.writeBuffer.putShort(my + this.regionY);
            this.clientStream.writeBuffer.putShort(var8);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 410) {
            this.method95(5, mx, my, midx, msrcidx);
            this.clientStream.newPacket(115);
            this.clientStream.writeBuffer.putShort(this.regionX + mx);
            this.clientStream.writeBuffer.putShort(this.regionY + my);
            this.clientStream.writeBuffer.putShort(var8);
            this.clientStream.sendPacket();
            this.selectedItemInventoryIndex = -1;
        }

        if (mitemid == 420) {
            this.method95(5, mx, my, midx, msrcidx);
            this.clientStream.newPacket(136);
            this.clientStream.writeBuffer.putShort(mx + this.regionX);
            this.clientStream.writeBuffer.putShort(my - -this.regionY);
            this.clientStream.sendPacket();
        }

        if (mitemid == 2400) {
            this.method95(5, mx, my, midx, msrcidx);
            this.clientStream.newPacket(79);
            this.clientStream.writeBuffer.putShort(mx + this.regionX);
            this.clientStream.writeBuffer.putShort(this.regionY + my);
            this.clientStream.sendPacket();
        }

        if (mitemid == 3400) {
            this.showMessage(0, GameData.objectDescription[mx], (String) null, (String) null, 0, (String) null, false);
        }

        if (mitemid == 600) {
            this.clientStream.newPacket(4);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.writeBuffer.putShort(my);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 610) {
            this.clientStream.newPacket(91);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.writeBuffer.putShort(my);
            this.clientStream.sendPacket();
            this.selectedItemInventoryIndex = -1;
        }

        if (mitemid == 620) {
            this.clientStream.newPacket(170);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 630) {
            this.clientStream.newPacket(169);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 640) {
            this.clientStream.newPacket(90);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 650) {
            this.showUiTab = 0;
            this.selectedItemInventoryIndex = mx;
            this.selectedItemName = GameData.itemName[this.inventoryItemId[this.selectedItemInventoryIndex]];
        }

        if (mitemid == 660) {
            this.clientStream.newPacket(246);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
            this.showUiTab = 0;
            this.selectedItemInventoryIndex = -1;
            this.showMessage(7, "Dropping " + GameData.itemName[this.inventoryItemId[mx]], (String) null, (String) null, 0, (String) null, false);
        }

        Character var10;
        int var11;
        int var12;
        if (mitemid == 700) {
            var10 = this.method77(mx, 0);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (var10.currentY - 64) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(50);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.writeBuffer.putShort(my);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 710) {
            var10 = this.method77(mx, 0);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (var10.currentY - 64) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(135);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.writeBuffer.putShort(my);
            this.clientStream.sendPacket();
            this.selectedItemInventoryIndex = -1;
        }

        if (mitemid == 720) {
            var10 = this.method77(mx, 0);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (-64 + var10.currentY) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(153);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 725) {
            var10 = this.method77(mx, 0);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (var10.currentY - 64) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(202);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 2715 || mitemid == 715) {
            var10 = this.method77(mx, 0);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (-64 + var10.currentY) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(190);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 3700) {
            this.showMessage(0, GameData.npcDescription[mx], (String) null, (String) null, 0, (String) null, false);
        }

        if (mitemid == 800) {
            var10 = this.getPlayer(true, mx);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (var10.currentY + -64) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(229);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.writeBuffer.putShort(my);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 810) {
            var10 = this.getPlayer(true, mx);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (var10.currentY + -64) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(113);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.writeBuffer.putShort(my);
            this.clientStream.sendPacket();
            this.selectedItemInventoryIndex = -1;
        }

        if (mitemid == 2805 || mitemid == 805) {
            var10 = this.getPlayer(true, mx);
            var11 = (-64 + var10.currentX) / this.magicLoc;
            var12 = (-64 + var10.currentY) / this.magicLoc;
            this.walkToActionSource(true, var11, var12, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(171);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 2806) {
            this.clientStream.newPacket(103);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 2810) {
            this.clientStream.newPacket(142);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 2820) {
            this.clientStream.newPacket(165);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
        }

        if (mitemid == 2833) {
            super.inputTextCurrent = var9;
            super.inputTextFinal = "";
            this.showDialogReportAbuseStep = 1;
        }

        if (mitemid == 2831) {
            this.friendAdd(4, var9);
        }

        if (mitemid == 2832) {
            this.ignoreAdd(var9);
        }

        if (mitemid == 2830) {
            super.inputPmFinal = "";
            super.inputPmCurrent = "";
            this.privateMessageTarget = var9;
            this.showDialogSocialInput = 2;
        }

        if (mitemid == 900) {
            this.walkToActionSource(true, mx, my, this.localRegionX, this.localRegionY, (byte) 102);
            this.clientStream.newPacket(158);
            this.clientStream.writeBuffer.putShort(mx + this.regionX);
            this.clientStream.writeBuffer.putShort(my + this.regionY);
            this.clientStream.writeBuffer.putShort(midx);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 920) {
            this.walkToActionSource(false, mx, my, this.localRegionX, this.localRegionY, (byte) 102);
            if (this.mouseClickXStep == -24) {
                this.mouseClickXStep = 24;
            }
        }

        if (mitemid == 1000) {
            this.clientStream.newPacket(137);
            this.clientStream.writeBuffer.putShort(mx);
            this.clientStream.sendPacket();
            this.selectedSpell = -1;
        }

        if (mitemid == 4000) {
            this.selectedItemInventoryIndex = -1;
            this.selectedSpell = -1;
        }
        ;
    }

    private void showInputPopup(int type, String[] text, int textStartIndex, String defaultInput, boolean showInput) {
        this.inputPopupText = text;
        this.inputPopupWidth = 400;

        for (int i = textStartIndex; text.length > i; ++i) {
            int var7 = this.surface.textWidth(1, -127, text[i]) - -10;
            if (var7 > this.inputPopupWidth) {
                this.inputPopupWidth = var7;
            }
        }

        this.inputPopupHeight = 15 - -((this.surface.textHeight(1, true) + 2) * (text.length - -1)) - -this.surface.textHeight(4, true);
        this.inputPopupShowInput = showInput;
        this.inputPopupType = type;
        this.inputPopupSubmit = false;
        super.inputTextCurrent = defaultInput;
        super.inputTextFinal = "";
    }

    private GameModel createModel(int var1, int var2, int var3, int var4, int var5, int var6) {
        int var7 = var4;
        int var9 = -115 / ((37 - var2) / 48);
        int var10 = var4;
        int var11 = var3;
        int var12 = GameData.wallObjectTextureFront[var6];
        int var13 = GameData.wallObjectTextureBack[var6];
        int var14 = GameData.wallObjectHeight[var6];
        if (var5 == 1) {
            var11 = var3 + 1;
        }

        GameModel var15 = new GameModel(4, 1);
        if (var5 == 0) {
            var10 = 1 + var4;
        }

        if (var5 == 2) {
            var11 = 1 + var3;
            var7 = var4 + 1;
        }

        int var8 = var3 * this.magicLoc;
        var7 *= this.magicLoc;
        if (var5 == 3) {
            var11 = 1 + var3;
            var10 = 1 + var4;
        }

        var10 *= this.magicLoc;
        var11 *= this.magicLoc;
        int var16 = var15.method561(var7, (byte) 127, -this.world.getElevation(var7, (byte) -56, var8), var8);
        int var17 = var15.method561(var7, (byte) 112, -this.world.getElevation(var7, (byte) -56, var8) - var14, var8);
        int var18 = var15.method561(var10, (byte) 120, -var14 + -this.world.getElevation(var10, (byte) -56, var11), var11);
        int var19 = var15.method561(var10, (byte) 112, -this.world.getElevation(var10, (byte) -56, var11), var11);
        int[] var20 = new int[]{var16, var17, var18, var19};
        var15.method549(4, var13, (byte) -101, var20, var12);
        var15.setLight(-10, 60, false, (byte) -38, 24, -50, -50);
        if (var4 >= 0 && var3 >= 0 && var4 < 96 && var3 < 96) {
            this.scene.method325(var15, (byte) 6);
        }

        var15.key = var1 + 10000;
        return var15;
    }

    private void drawUiTabSocial(int var1, boolean var2) {
        int var3 = -199 + this.surface.width2;
        byte var4 = 36;
        this.surface.drawSprite((byte) 61, 3, this.spriteMedia - -5, -49 + var3);
        short var5 = 196;
        short var6 = 182;
        int var7;
        int var8 = var7 = Surface.rgb2long(160, 160, 160);
        if (this.anInt1367 == 0) {
            var8 = Surface.rgb2long(220, 220, 220);
        } else {
            var7 = Surface.rgb2long(220, 220, 220);
        }

        this.surface.drawBoxAlpha(var3, var4, var5 / 2, 24, var8, 128);
        this.surface.drawBoxAlpha(var5 / 2 + var3, var4, var5 / 2, 24, var7, 128);
        this.surface.drawBoxAlpha(var3, var4 + 24, var5, var6 + -24, Surface.rgb2long(220, 220, 220), 128);
        this.surface.drawLineHoriz(var3, 24 + var4, var5, 0);
        this.surface.drawLineVert(var5 / 2 + var3, var4, 24, 0);
        this.surface.drawLineHoriz(var3, var6 + var4 + -16, var5, 0);
        this.surface.drawstringCenter("Friends", 4, 16 + var4, var3 - -(var5 / 4), 0);
        this.surface.drawstringCenter("Ignore", 4, 16 + var4, var5 / 2 + var5 / 4 + var3, 0);
        this.panelSocial.clearList(this.controlListSocialPlayers);
        int index;
        String var10;
        int var12;
        if (this.anInt1367 == 0) {
            for (index = 0; friendListCount > index; ++index) {
                if ((2 & friendListOnline[index]) != 0) {
                    var10 = "@gre@";
                } else if ((4 & friendListOnline[index]) != 0) {
                    var10 = "@yel@";
                } else {
                    var10 = "@red@";
                }

                String name = friendListNames[index];
                var12 = 0;

                for (int var13 = friendListNames[index].length(); this.surface.textWidth(1, -127, name) > 120; name = friendListNames[index].substring(0, var13 - var12) + "...") {
                    ++var12;
                }

                this.panelSocial.addListEntry(this.controlListSocialPlayers, index, var10 + name + "~439~@whi@Remove         WWWWWWWWWW", null, null, 0);
            }
        }

        if (this.anInt1367 == 1) {
            for (index = 0; index < ignoreListCount; ++index) {
                var10 = ignoreListNames[index];
                int var16 = 0;

                for (var12 = ignoreListNames[index].length(); this.surface.textWidth(1, -127, var10) > 120; var10 = ignoreListNames[index].substring(0, -var16 + var12) + "...") {
                    ++var16;
                }

                this.panelSocial.addListEntry(this.controlListSocialPlayers, index, "@yel@" + var10 + "~439~@whi@Remove         WWWWWWWWWW", (String) null, (String) null, 0);
            }
        }

        this.panelSocial.drawPanel(63);
        this.anInt1435 = -1;
        this.anInt1626 = -1;
        int var17;
        if (this.anInt1367 == 0) {
            index = this.panelSocial.setFocus(this.controlListSocialPlayers);
            if (index >= 0 && super.mouseX < 489) {
                if (super.mouseX <= 429) {
                    this.anInt1626 = index;
                } else {
                    this.anInt1626 = -(2 - -index);
                }
            }

            this.surface.drawstringCenter("Click a name to send a message", 1, var4 + 35, var3 + var5 / 2, 0xffffff);
            if (var3 < super.mouseX && var3 + var5 > super.mouseX && super.mouseY > var4 + var6 + -16 && super.mouseY < (var6 + var4)) {
                var17 = 0xffff00;
            } else {
                var17 = 0xffffff;
            }

            this.surface.drawstringCenter("Click here to add a friend", 1, var6 + var4 - 3, var5 / 2 + var3, var17);
        }

        if (this.anInt1367 == 1) {
            index = this.panelSocial.setFocus(this.controlListSocialPlayers);
            if (index >= 0 && super.mouseX < 489) {
                if (super.mouseX > 429) {
                    this.anInt1435 = -(2 + index);
                } else {
                    this.anInt1435 = index;
                }
            }

            this.surface.drawstringCenter("Blocking messages from:", 1, var4 + 35, var5 / 2 + var3, 0xffffff);
            if (var3 < super.mouseX && var3 - -var5 > super.mouseX && (-16 + var6 + var4) < super.mouseY && (var4 - -var6) > super.mouseY) {
                var17 = 0xffff00;
            } else {
                var17 = 0xffffff;
            }

            this.surface.drawstringCenter("Click here to add a name", 1, -3 + var6 + var4, var5 / 2 + var3, var17);
        }

        if (var1 != -3998) {
            this.createMessageTabPanel(-74);
        }

        if (var2) {
            var3 = super.mouseX + -this.surface.width2 + 199;
            int var15 = -36 + super.mouseY;
            if (var3 >= 0 && var15 >= 0 && var3 < 196 && var15 < 182) {
                this.panelSocial.handleMouse(var1 + 3904, super.lastMouseButtonDown, var3 + -199 + this.surface.width2, super.anInt84, 36 + var15);
                if (var15 <= 24 && this.mouseButtonClick == 1) {
                    if (var3 < 98 && this.anInt1367 == 1) {
                        this.anInt1367 = 0;
                        this.panelSocial.method506(var1 + -18759, this.controlListSocialPlayers);
                    } else if (var3 > 98 && this.anInt1367 == 0) {
                        this.anInt1367 = 1;
                        this.panelSocial.method506(-22757, this.controlListSocialPlayers);
                    }
                }

                if (this.mouseButtonClick == 1 && this.anInt1367 == 0) {
                    index = this.panelSocial.setFocus(this.controlListSocialPlayers);
                    if (index >= 0 && super.mouseX < 489) {
                        if (super.mouseX <= 429) {
                            if ((4 & friendListOnline[index]) != 0) {
                                this.showDialogSocialInput = 2;
                                this.privateMessageTarget = friendListNames[index];
                                super.inputPmCurrent = "";
                                super.inputPmFinal = "";
                            }
                        } else {
                            this.friendRemove(friendListNames[index]);
                        }
                    }
                }

                if (this.mouseButtonClick == 1 && this.anInt1367 == 1) {
                    index = this.panelSocial.setFocus(this.controlListSocialPlayers);
                    if (index >= 0 && super.mouseX < 489 && super.mouseX > 429) {
                        this.ignoreRemove(ignoreListAccNames[index]);
                    }
                }

                if (var15 > 166 && this.mouseButtonClick == 1 && this.anInt1367 == 0) {
                    super.inputTextFinal = "";
                    super.inputTextCurrent = "";
                    this.showDialogSocialInput = 1;
                }

                if (var15 > 166 && this.mouseButtonClick == 1 && this.anInt1367 == 1) {
                    this.showDialogSocialInput = 3;
                    super.inputTextCurrent = "";
                    super.inputTextFinal = "";
                }

                this.mouseButtonClick = 0;
            }
        }
        ;
    }

    private void drawAboveHeadStuff(int var1) {
        int var3;
        int var4;
        int var5;
        int var6;
        int var7;
        int var9;
        for (int var2 = 0; var2 < this.playerMessagesCount; ++var2) {
            var3 = this.surface.textHeight(1, true);
            var4 = this.playerMessageX[var2];
            var5 = this.playerMessageY[var2];
            var6 = this.playerMessageMidPoint[var2];
            var7 = this.playerMessageHeight[var2];
            boolean var8 = true;

            while (var8) {
                var8 = false;

                for (var9 = 0; var2 > var9; ++var9) {
                    if (var7 + var5 > this.playerMessageY[var9] - var3 && (-var3 + var5) < (this.playerMessageHeight[var9] + this.playerMessageY[var9]) && (var4 - var6) < (this.playerMessageX[var9] + this.playerMessageMidPoint[var9]) && (var6 + var4) > (-this.playerMessageMidPoint[var9] + this.playerMessageX[var9]) && var5 > (-var7 + (this.playerMessageY[var9] - var3))) {
                        var8 = true;
                        var5 = -var3 + this.playerMessageY[var9] + -var7;
                    }
                }
            }

            this.playerMessageY[var2] = var5;
            this.surface.centrepara(var4, false, false, 1, 0xffff00, this.playerMessages[var2], 300, var5);
        }


        for (var3 = 0; var3 < this.anInt1461; ++var3) {
            var4 = this.playerActionBubbleX[var3];
            var5 = this.playerActionBubbleY[var3];
            var6 = this.playerActionBubbleScale[var3];
            var7 = this.playerActionBubbleItem[var3];
            int var14 = 39 * var6 / 100;
            var9 = 27 * var6 / 100;
            int var10 = var5 + -var9;
            this.surface.method425(var14, var4 + -(var14 / 2), 85, 9 + this.spriteMedia, -117, var9, var10);
            int var11 = var6 * 36 / 100;
            int var12 = var6 * 24 / 100;
            this.surface.method409(GameData.itemMask[var7], this.spriteItem + GameData.itemPicture[var7], -(var12 / 2) + var10 - -(var9 / 2), -124, var12, 0, var11, -(var11 / 2) + var4, false, 0);
        }

        if (var1 >= -43) {
            this.friendListUnknown2 = -14;
        }

        for (var4 = 0; this.anInt1365 > var4; ++var4) {
            var5 = this.playerHealthBarX[var4];
            var6 = this.playerHealthBarY[var4];
            var7 = this.playerHealthBarMissing[var4];
            this.surface.drawBoxAlpha(-15 + var5, var6 + -3, var7, 5, '\uff00', 192);
            this.surface.drawBoxAlpha(var5 + (-15 - -var7), -3 + var6, -var7 + 30, 5, 0xff0000, 192);
        }
        ;
    }

    private boolean loadNextRegion(int var1, int var2, byte var3) {
        if (this.deathScreenTimeout != 0) {
            this.world.playerAlive = false;
            return false;
        } else {
            this.loadingArea = false;
            var1 += this.planeWidth;
            var2 += this.planeHeight;
            if (this.planeIndex == this.lastHeightOffset && var1 > this.anInt1348 && var1 < this.anInt1260 && this.anInt1298 < var2 && this.anInt1292 > var2) {
                this.world.playerAlive = true;
                return false;
            } else {
                this.surface.drawstringCenter("Loading... Please wait", 1, 192, 256, 0xffffff);
                this.drawChatMessageTabs(false);
                this.surface.draw(-2020315800, this.graphics, super.anInt58, super.anInt7);
                int var4 = this.regionX;
                int var5 = this.regionY;
                int var6 = (var1 - -24) / 48;
                this.lastHeightOffset = this.planeIndex;
                this.regionX = -48 + var6 * 48;
                int var7 = (24 + var2) / 48;
                this.anInt1260 = var6 * 48 + 32;
                this.regionY = -48 + 48 * var7;
                this.anInt1348 = var6 * 48 + -32;
                this.anInt1298 = 48 * var7 - 32;
                if (var3 != 43) {
                    return true;
                } else {
                    this.anInt1292 = 32 + 48 * var7;
                    this.world.method269(var1, var2, 5280, this.lastHeightOffset);
                    this.regionX -= this.planeWidth;
                    this.regionY -= this.planeHeight;
                    int var8 = this.regionX + -var4;
                    int var9 = this.regionY + -var5;

                    int var11;
                    int var12;
                    int var13;
                    int var15;
                    int var17;
                    for (int objidx = 0; objidx < this.objectCount; ++objidx) {
                        this.objectY[objidx] -= var8;
                        this.objectX[objidx] -= var9;
                        var11 = this.objectY[objidx];
                        var12 = this.objectX[objidx];
                        var13 = this.objectId[objidx];
                        GameModel gameModel = this.objectModel[objidx];

                        try {
                            var15 = this.objectDirection[objidx];
                            int var16;
                            if (var15 != 0 && var15 != 4) {
                                var17 = GameData.objectHeight[var13];
                                var16 = GameData.objectWidth[var13];
                            } else {
                                var16 = GameData.objectHeight[var13];
                                var17 = GameData.objectWidth[var13];
                            }

                            int var18 = this.magicLoc * (var17 + (var11 - -var11)) / 2;
                            int var19 = (var16 + var12 - -var12) * this.magicLoc / 2;
                            if (var11 >= 0 && var12 >= 0 && var11 < 96 && var12 < 96) {
                                this.scene.method325(gameModel, (byte) 6);
                                gameModel.method573(var19, var18, -this.world.getElevation(var18, (byte) -56, var19), (byte) -83);
                                this.world.method274(var13, (byte) -113, var11, var12);
                                if (var13 == 74) {
                                    gameModel.translate(-480, 0, (byte) -120, 0);
                                }
                            }
                        } catch (RuntimeException var21) {
                            System.out.println("Loc Error: " + var21.getMessage());
                            System.out.println("i:" + objidx + " obj:" + gameModel);
                            var21.printStackTrace();
                        }
                    }

                    int var23;
                    for (var11 = 0; var11 < this.wallObjectCount; ++var11) {
                        this.wallObjectX[var11] -= var8;
                        this.wallObjectY[var11] -= var9;
                        var12 = this.wallObjectX[var11];
                        var13 = this.wallObjectY[var11];
                        var23 = this.wallObjectId[var11];
                        var15 = this.wallObjectDirection[var11];

                        try {
                            this.world.setObjectAdjacency(var23, var13, var12, -85, var15);
                            GameModel var26 = this.createModel(var11, var3 + -101, var13, var12, var15, var23);
                            this.wallObjectModel[var11] = var26;
                        } catch (RuntimeException var20) {
                            System.out.println("Bound Error: " + var20.getMessage());
                            var20.printStackTrace();
                        }
                    }

                    for (var12 = 0; var12 < this.groundItemCount; ++var12) {
                        this.groundItemX[var12] -= var8;
                        this.groundItemY[var12] -= var9;
                    }

                    for (var13 = 0; var13 < this.playerCount; ++var13) {
                        Character var24 = this.players[var13];
                        var24.currentY -= this.magicLoc * var9;
                        var24.currentX -= var8 * this.magicLoc;

                        for (var15 = 0; var15 <= var24.waypointCurrent; ++var15) {
                            var24.waypointsX[var15] -= var8 * this.magicLoc;
                            var24.waypointsY[var15] -= this.magicLoc * var9;
                        }
                    }

                    for (var23 = 0; this.npcCount > var23; ++var23) {
                        Character var25 = this.npcs[var23];
                        var25.currentY -= var9 * this.magicLoc;
                        var25.currentX -= var8 * this.magicLoc;

                        for (var17 = 0; var17 <= var25.waypointCurrent; ++var17) {
                            var25.waypointsX[var17] -= var8 * this.magicLoc;
                            var25.waypointsY[var17] -= this.magicLoc * var9;
                        }
                    }

                    this.world.playerAlive = true;
                    return true;
                }
            }
        }
    }

    private Character createNpc(int serverIndex, int x, int sprite, byte var4, int type, int y) {
        if (this.npcsServer[serverIndex] == null) {
            this.npcsServer[serverIndex] = new Character();
            this.npcsServer[serverIndex].serverIndex = serverIndex;
        }

        Character character = this.npcsServer[serverIndex];
        boolean foundNpc = false;

        for (int var9 = 0; this.npcCacheCount > var9; ++var9) {
            if (serverIndex == this.npcsCache[var9].serverIndex) {
                foundNpc = true;
                break;
            }
        }

        if (var4 >= -68) {
            this.walkToSource(14, 112, -36, 90, -78, 102, true, false, 31);
        }

        if (foundNpc) {
            character.animationNext = sprite;
            character.npcId = type;
            int waypointIdx = character.waypointCurrent;
            if (character.waypointsX[waypointIdx] != x || y != character.waypointsY[waypointIdx]) {
                character.waypointCurrent = waypointIdx = (1 + waypointIdx) % 10;
                character.waypointsX[waypointIdx] = x;
                character.waypointsY[waypointIdx] = y;
            }
        } else {
            character.waypointsX[0] = character.currentX = x;
            character.waypointCurrent = 0;
            character.serverIndex = serverIndex;
            character.movingStep = 0;
            character.stepCount = 0;
            character.npcId = type;
            character.waypointsY[0] = character.currentY = y;
            character.animationNext = character.animationCurrent = sprite;
        }

        this.npcs[this.npcCount++] = character;
        return character;
    }

    private void addTradeOffer(int var1, int var2, int var3) {
        boolean var4 = false;
        int var5 = 0;
        int var6 = this.inventoryItemId[var1];
        if (var2 <= 57) {
            this.mouseButtonDownTime = 36;
        }

        int var8;
        for (int var7 = 0; var7 < this.tradeItemsCount; ++var7) {
            if (this.tradeItems[var7] == var6) {
                if (GameData.itemStackable[var6] != 0) {
                    ++var5;
                } else if (var3 < 0) {
                    for (var8 = 0; this.mouseButtonItemCountIncrement > var8; ++var8) {
                        if (this.inventoryItemStackCount[var1] > this.tradeItemCount[var7]) {
                            ++this.tradeItemCount[var7];
                        }

                        var4 = true;
                    }
                } else {
                    this.tradeItemCount[var7] += var3;
                    if (this.tradeItemCount[var7] > this.inventoryItemStackCount[var1]) {
                        this.tradeItemCount[var7] = this.inventoryItemStackCount[var1];
                    }

                    var4 = true;
                }
            }
        }

        var8 = this.getInventoryCount(var6);
        if (var8 <= var5) {
            var4 = true;
        }

        if (GameData.itemSpecial[var6] == 1) {
            var4 = true;
            this.showMessage(0, "This object cannot be traded with other players", (String) null, (String) null, 0, (String) null, false);
        }

        int var9;
        if (!var4) {
            if (var3 >= 0) {
                for (var9 = 0; var9 < var3 && this.tradeItemsCount < 12 && var5 < var8; ++var9) {
                    this.tradeItems[this.tradeItemsCount] = var6;
                    this.tradeItemCount[this.tradeItemsCount] = 1;
                    ++var5;
                    var4 = true;
                    ++this.tradeItemsCount;
                    if (var9 == 0 && GameData.itemStackable[var6] == 0) {
                        this.tradeItemCount[-1 + this.tradeItemsCount] = this.inventoryItemStackCount[var1] >= var3 ? var3 : this.inventoryItemStackCount[var1];
                        break;
                    }
                }
            } else if (this.tradeItemsCount < 12) {
                this.tradeItems[this.tradeItemsCount] = var6;
                this.tradeItemCount[this.tradeItemsCount] = 1;
                ++this.tradeItemsCount;
                var4 = true;
            }
        }

        if (var4) {
            this.clientStream.newPacket(46);
            this.clientStream.writeBuffer.putByte(this.tradeItemsCount);

            for (var9 = 0; var9 < this.tradeItemsCount; ++var9) {
                this.clientStream.writeBuffer.putShort(this.tradeItems[var9]);
                this.clientStream.writeBuffer.putInt(this.tradeItemCount[var9]);
            }

            this.clientStream.sendPacket();
            this.tradeRecipientAccepted = false;
            this.tradeAccepted = false;
        }
        ;
    }

    private void login(boolean reconnecting, String user, String pass, byte var4) {
        if (this.worldFullTimeout > 0) {
            this.showLoginScreenStatus("Connecting to server", "Please wait...", 105);

            try {
                sleep(2000L, -17239);
            } catch (Exception var13) {
                ;
            }

            this.showLoginScreenStatus("Please try again later", "Sorry! The server is currently full.", 94);
        } else {
            int var6;
            while (this.autoLoginTimeout > 0) {
                try {
                    this.password = pass;
                    this.username = user;
                    String formatpass = Utility.format(20, pass, (byte) -127);
                    if (this.username.trim().length() == 0) {
                        this.showLoginScreenStatus("and a password - Please try again", "You must enter both a username", -118);
                        return;
                    }

                    if (!reconnecting) {
                        this.showLoginScreenStatus("Connecting to server", "Please wait...", 113);
                    } else {
                        this.drawTextBox("Attempting to re-establish", true, "Connection lost! Please wait...");
                    }

                    var6 = this.autoLoginTimeout <= 1 ? this.serverJagGrabPort : this.serverPort;
                    System.out.println("CONNECTING TO " + serverAddress + ":" + var6);
                    this.clientStream = new ClientStream(this.method121((byte) -127, this.serverAddress, var6), this);
                    this.clientStream.maxReadTries = maxReadTries;
                    byte limit30 = 0;

                    try {
                        if (gameFrameReference == null) {
                            String var8 = this.getParameter("limit30");
                            if (var8.equals("1")) {
                                limit30 = 1;
                            }
                        }
                    } catch (Exception var15) {
                        ;
                    }

                    int[] keys = new int[]{(int) (9.9999999E7D * Math.random()), (int) (9.9999999E7D * Math.random()), (int) (Math.random() * 9.9999999E7D), (int) (9.9999999E7D * Math.random())};
                    this.clientStream.newPacket(0); // login packet id 0
                    if (!reconnecting) {
                        this.clientStream.writeBuffer.putByte(0);
                    } else {
                        this.clientStream.writeBuffer.putByte(1);
                    }

                    this.clientStream.writeBuffer.putInt(ClientStream.version);
                    BufferBase_Sub3 loginBlock = new BufferBase_Sub3(500);
                    loginBlock.putByte(10); // login packet encrypted id 10
                    loginBlock.putInt(keys[0]); // isaac/xtea keys
                    loginBlock.putInt(keys[1]);
                    loginBlock.putInt(keys[2]);
                    loginBlock.putInt(keys[3]);
                    loginBlock.pjstr(formatpass); // password

                    for (int var10 = 0; var10 < 5; ++var10) {
                        loginBlock.putInt((int) (9.9999999E7D * Math.random())); // nonces ?
                    }

                    loginBlock.addInt3Byte((int) (9.9999999E7D * Math.random())); // nonce, was originally the linkuid thing ?
                    loginBlock.encrypt(exponent, modulus); // encrypt the block
                    this.clientStream.writeBuffer.addBytes(loginBlock.offset, loginBlock.buffer, 0); // add the block
                    this.clientStream.writeBuffer.putShort(0); // xtea block offset holder
                    int xtea_start = this.clientStream.writeBuffer.offset;
                    this.clientStream.writeBuffer.putByte(limit30); // ya
                    putRandom(-257, this.clientStream.writeBuffer); // writes 24 bytes to the buffer? wtf are they?
                    this.clientStream.writeBuffer.pjstr(this.username); // username NOT INSIDE LOGIN BLOCK
                    this.clientStream.writeBuffer.xtea_encrypt(xtea_start, keys, this.clientStream.writeBuffer.offset); // xtea the block
                    this.clientStream.writeBuffer.setLengthShort(2, this.clientStream.writeBuffer.offset - xtea_start); // sets xtea block offset at the placeholder
                    this.clientStream.flushPacket(); // flush
                    this.clientStream.init_isaac(keys); // init isaac
                    int loginResponse = this.clientStream.read();
                    System.out.println("login response:" + loginResponse);
                    if ((loginResponse & 64) != 0) {
                        this.autoLoginTimeout = 0;
                        this.modlevel1 = loginResponse >> 2 & 15;
                        this.modlevel2 = 3 & loginResponse;
                        this.resetGame(0);
                        return;
                    }

                    if (loginResponse == 1) {
                        this.autoLoginTimeout = 0;
                        this.method134(false);
                        return;
                    }

                    if (reconnecting) {
                        formatpass = "";
                        this.username = "";
                        this.resetLoginVars((byte) -82);
                        return;
                    }

                    if (loginResponse == -1) {
                        this.showLoginScreenStatus("Server timed out", "Error unable to login.", 107);
                        return;
                    }

                    if (loginResponse == 3) {
                        this.showLoginScreenStatus("Try again, or create a new account", "Invalid username or password.", 15);
                        return;
                    }

                    if (loginResponse == 4) {
                        this.showLoginScreenStatus("Wait 60 seconds then retry", "That username is already logged in.", 87);
                        return;
                    }

                    if (loginResponse == 5) {
                        this.showLoginScreenStatus("Please reload this page", "The client has been updated.", -71);
                        return;
                    }

                    if (loginResponse == 6) {
                        this.showLoginScreenStatus("Your ip-address is already in use", "You may only use 1 character at once.", -89);
                        return;
                    }

                    if (loginResponse == 7) {
                        this.showLoginScreenStatus("Please try again in 5 minutes", "Login attempts exceeded!", 72);
                        return;
                    }

                    if (loginResponse == 8) {
                        this.showLoginScreenStatus("Server rejected session", "Error unable to login.", 71);
                        return;
                    }

                    if (loginResponse == 9) {
                        this.showLoginScreenStatus("Under 13 accounts cannot access RuneScape Classic", "Error unable to login.", 65);
                        return;
                    }

                    if (loginResponse == 10) {
                        this.showLoginScreenStatus("Wait 60 seconds then retry", "That username is already in use.", 97);
                        return;
                    }

                    if (loginResponse == 11) {
                        this.showLoginScreenStatus("Check your message inbox for details", "Account temporarily disabled.", 89);
                        return;
                    }

                    if (loginResponse == 12) {
                        this.showLoginScreenStatus("Check your message inbox for details", "Account permanently disabled.", -99);
                        return;
                    }

                    if (loginResponse == 14) {
                        this.showLoginScreenStatus("Please try a different world", "Sorry! This world is currently full.", -118);
                        this.worldFullTimeout = 1500;
                        return;
                    }

                    if (loginResponse == 15) {
                        this.showLoginScreenStatus("to login to this world", "You need a members account", 91);
                        return;
                    }

                    if (loginResponse == 16) {
                        this.showLoginScreenStatus("Please try again", "Error - no reply from loginserver.", 5);
                        return;
                    }

                    if (loginResponse == 17) {
                        this.showLoginScreenStatus("Contact customer support", "Error - failed to decode profile.", 24);
                        return;
                    }

                    if (loginResponse == 18) {
                        this.showLoginScreenStatus("Press \'recover a locked account\' on front page.", "Account suspected stolen.", -73);
                        return;
                    }

                    if (loginResponse == 20) {
                        this.showLoginScreenStatus("Please try a different world", "Error - loginserver mismatch", -97);
                        return;
                    }

                    if (loginResponse == 21) {
                        this.showLoginScreenStatus("Please try a non-veterans world.", "That is not a veteran RS-Classic account.", 22);
                        return;
                    }

                    if (loginResponse == 22) {
                        this.showLoginScreenStatus("Press \'change your password\' on front page.", "Password suspected stolen.", -81);
                        return;
                    }

                    if (loginResponse == 23) {
                        this.showLoginScreenStatus("Please go to the Account Management page to do this.", "You need to set your display name.", 96);
                        return;
                    }

                    if (loginResponse == 24) {
                        this.showLoginScreenStatus("Please see the launch page for help", "This world does not accept new players.", -95);
                        return;
                    }

                    if (loginResponse != 25) {
                        this.showLoginScreenStatus("Unrecognised response code", "Error unable to login.", 15);
                        return;
                    }

                    this.showLoginScreenStatus("Contact customer support", "None of your characters can log in.", 74);
                    return;
                } catch (Exception var16) {
                    System.out.println(String.valueOf(var16));
                    if (this.autoLoginTimeout > 0) {
                        try {
                            sleep(5000L, -17239);
                        } catch (Exception var14) {
                            ;
                        }

                        --this.autoLoginTimeout;
                    } else if (reconnecting) {
                        this.username = "";
                        this.password = "";
                        this.resetLoginVars((byte) -109);
                    } else {
                        Utility.sendClientError("Error while connecting", -257, var16);
                        this.showLoginScreenStatus("Check internet settings or try another world", "Sorry! Unable to connect.", 103);
                    }
                }
            }

            var6 = 114 / ((20 - var4) / 40);
        }
    }

    private boolean method105(int var1) {
        if (var1 < 54) {
            this.duelItems = null;
        }

        return true;
    }

    final void method27(int var1, int var2, int var3, int var4) {
        this.anIntArray1318[this.anInt1295] = var4;
        this.anIntArray1343[this.anInt1295] = var1;
        this.anInt1295 = var2 & this.anInt1295 + 1;

        for (int var5 = 10; var5 < 4000; ++var5) {
            int var6 = this.anInt1295 - var5 & 8191;
            if (var4 == this.anIntArray1318[var6] && this.anIntArray1343[var6] == var1) {
                boolean var7 = false;

                for (int var8 = 1; var8 < var5; ++var8) {
                    int var9 = 8191 & this.anInt1295 + -var8;
                    int var10 = 8191 & var6 + -var8;
                    if (this.anIntArray1318[var10] != var4 || this.anIntArray1343[var10] != var1) {
                        var7 = true;
                    }

                    if (this.anIntArray1318[var9] != this.anIntArray1318[var10] || this.anIntArray1343[var10] != this.anIntArray1343[var9]) {
                        break;
                    }

                    if (var8 == (-1 + var5) && var7 && this.combatTimeout == 0 && this.logoutTimeout == 0) {
                        this.sendLogout(2);
                        return;
                    }
                }
            }
        }
    }

    private void drawUiTabPlayerInfo(boolean var1, boolean var2) {
        int var3 = this.surface.width2 + -199;
        byte var4 = 36;
        this.surface.drawSprite((byte) 61, 3, 3 + this.spriteMedia, -49 + var3);
        short var5 = 196;
        if (!var1) {
            this.menuDuel = null;
        }

        short var6 = 275;
        int var7;
        int var8 = var7 = Surface.rgb2long(160, 160, 160);
        if (this.uiTabPlayerInfoSubTab == 0) {
            var8 = Surface.rgb2long(220, 220, 220);
        } else {
            var7 = Surface.rgb2long(220, 220, 220);
        }

        this.surface.drawBoxAlpha(var3, var4, var5 / 2, 24, var8, 128);
        this.surface.drawBoxAlpha(var5 / 2 + var3, var4, var5 / 2, 24, var7, 128);
        this.surface.drawBoxAlpha(var3, var4 + 24, var5, -24 + var6, Surface.rgb2long(220, 220, 220), 128);
        this.surface.drawLineHoriz(var3, var4 + 24, var5, 0);
        this.surface.drawLineVert(var3 - -(var5 / 2), var4, 24, 0);
        this.surface.drawstringCenter("Stats", 4, var4 + 16, var5 / 4 + var3, 0);
        this.surface.drawstringCenter("Quests", 4, var4 - -16, var3 + var5 / 4 + var5 / 2, 0);
        int var17;
        if (this.uiTabPlayerInfoSubTab == 0) {
            byte var9 = 72;
            this.surface.drawstring("Skills", var3 + 5, var9, 3, 0xffff00);
            int var10 = -1;
            var17 = var9 + 13;

            int var12;
            for (int var11 = 0; var11 < 9; ++var11) {
                var12 = 0xffffff;
                if (super.mouseX > var3 - -3 && (var17 - 11) <= super.mouseY && (var17 + 2) > super.mouseY && var3 - -90 > super.mouseX) {
                    var10 = var11;
                    var12 = 0xff0000;
                }

                this.surface.drawstring(this.skillNameShort[var11] + ":@yel@" + this.playerStatCurrent[var11] + "/" + this.playerStatBase[var11], 5 + var3, var17, 1, var12);
                var12 = 0xffffff;
                if (90 + var3 <= super.mouseX && -11 + (var17 - 13) <= super.mouseY && (var17 + -11) > super.mouseY && (var3 + 196) > super.mouseX) {
                    var12 = 0xff0000;
                    var10 = var11 + 9;
                }

                this.surface.drawstring(this.skillNameShort[var11 + 9] + ":@yel@" + this.playerStatCurrent[9 + var11] + "/" + this.playerStatBase[var11 - -9], -5 + var5 / 2 + var3, -13 + var17, 1, var12);
                var17 += 13;
            }

            this.surface.drawstring("Quest Points:@yel@" + this.playerQuestPoints, -5 + var5 / 2 + var3, -13 + var17, 1, 0xffffff);
            var17 += 12;
            this.surface.drawstring("Fatigue: @yel@" + 100 * this.statFatigue / 750 + "%", var3 - -5, var17 - 13, 1, 0xffffff);
            var17 += 8;
            this.surface.drawstring("Equipment Status", 5 + var3, var17, 3, 0xffff00);
            var17 += 12;

            for (var12 = 0; var12 < 3; ++var12) {
                this.surface.drawstring(this.equipmentStatNames[var12] + ":@yel@" + this.playerStatEquipment[var12], var3 - -5, var17, 1, 0xffffff);
                if (var12 < 2) {
                    this.surface.drawstring(this.equipmentStatNames[var12 - -3] + ":@yel@" + this.playerStatEquipment[var12 - -3], 25 + var5 / 2 + var3, var17, 1, 0xffffff);
                }

                var17 += 13;
            }

            var17 += 6;
            this.surface.drawLineHoriz(var3, var17 + -15, var5, 0);
            int var13;
            int var14;
            if (var10 == -1) {
                this.surface.drawstring("Overall levels", 5 + var3, var17, 1, 0xffff00);
                var17 += 12;
                var13 = 0;

                for (var14 = 0; var14 < 18; ++var14) {
                    var13 += this.playerStatBase[var14];
                }

                this.surface.drawstring("Skill total: " + var13, 5 + var3, var17, 1, 0xffffff);
                var17 += 12;
                this.surface.drawstring("Combat level: " + this.localPlayer.level, var3 - -5, var17, 1, 0xffffff);
                var17 += 12;
            } else {
                this.surface.drawstring(this.skillNameLong[var10] + " skill", var3 + 5, var17, 1, 0xffff00);
                var17 += 12;
                var13 = this.experienceArray[0];//ie experience array

                for (var14 = 0; var14 < 98; ++var14) {
                    if (this.playerExperience[var10] >= this.experienceArray[var14]) {
                        var13 = this.experienceArray[var14 - -1];
                    }
                }

                this.surface.drawstring("Total xp: " + this.playerExperience[var10] / 4, var3 + 5, var17, 1, 0xffffff);
                var17 += 12;
                this.surface.drawstring("Next level at: " + var13 / 4, 5 + var3, var17, 1, 0xffffff);
            }
        }

        if (this.uiTabPlayerInfoSubTab == 1) {
            this.panelPlayerinfo.clearList(this.controlPlayerinfopanel);
            this.panelPlayerinfo.addListEntry(this.controlPlayerinfopanel, 0, "@whi@Quest-list (green=completed)", (String) null, (String) null, 0);

            for (var17 = 0; var17 < 50; ++var17) {
                this.panelPlayerinfo.addListEntry(this.controlPlayerinfopanel, 1 + var17, (this.questComplete[var17] ? "@gre@" : "@red@") + this.aStringArray1426[var17], (String) null, (String) null, 0);
            }

            this.panelPlayerinfo.drawPanel(122);
        }

        if (var2) {
            var3 = super.mouseX + 199 + -this.surface.width2;
            int var16 = super.mouseY - 36;
            if (var3 >= 0 && var16 >= 0 && var3 < var5 && var16 < var6) {
                if (this.uiTabPlayerInfoSubTab == 1) {
                    this.panelPlayerinfo.handleMouse(-109, super.lastMouseButtonDown, var3 - -this.surface.width2 - 199, super.anInt84, var16 - -36);
                }

                if (var16 <= 24 && this.mouseButtonClick == 1) {
                    if (var3 >= 98) {
                        if (var3 > 98) {
                            this.uiTabPlayerInfoSubTab = 1;
                            return;
                        }
                    } else {
                        this.uiTabPlayerInfoSubTab = 0;
                    }

                    return;
                }
            }

        }
        ;
    }

    final void method107(byte var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        Character var9 = this.npcs[var8];
        int var10 = (this.cameraRotation + 16) / 32 + var9.animationCurrent & 7;
        boolean var11 = false;
        int var12 = var10;
        if (var10 == 5) {
            var11 = true;
            var12 = 3;
        } else if (var10 == 6) {
            var11 = true;
            var12 = 2;
        } else if (var10 == 7) {
            var11 = true;
            var12 = 1;
        }

        int var13 = var12 * 3 + this.anIntArray1405[var9.stepCount / GameData.npcWalkModel[var9.npcId] % 4];
        if (var9.animationCurrent != 8) {
            if (var9.animationCurrent == 9) {
                var12 = 5;
                var10 = 2;
                var4 += var6 * GameData.npcCombatAnimation[var9.npcId] / 100;
                var11 = true;
                var13 = 3 * var12 + this.anIntArray1445[this.anInt1360 / GameData.npcCombatModel[var9.npcId] % 8];
            }
        } else {
            var4 -= var6 * GameData.npcCombatAnimation[var9.npcId] / 100;
            var12 = 5;
            var10 = 2;
            var11 = false;
            var13 = this.anIntArray1525[this.anInt1360 / (-1 + GameData.npcCombatModel[var9.npcId]) % 8] + 3 * var12;
        }

        int var15;
        int var16;
        for (int var14 = 0; var14 < 12; ++var14) {
            var15 = this.anIntArrayArray1524[var10][var14];
            var16 = GameData.npcSprite[var9.npcId][var15];
            if (var16 >= 0) {
                byte var17 = 0;
                byte var18 = 0;
                int var19 = var13;
                if (var11 && var12 >= 1 && var12 <= 3 && GameData.animationHasF[var16] == 1) {
                    var19 = var13 + 15;
                }

                if (var12 != 5 || GameData.animationHasA[var16] == 1) {
                    int var20 = var19 + GameData.animationNumber[var16];
                    int var21 = this.surface.spriteWidthFull[var20];
                    int var22 = this.surface.spriteHeightFull[var20];
                    int var23 = this.surface.spriteWidthFull[GameData.animationNumber[var16]];
                    if (var21 != 0 && var22 != 0 && var23 != 0) {
                        int var29 = var18 * var3 / var22;
                        int var28 = var17 * var5 / var21;
                        int var24 = this.surface.spriteWidthFull[var20] * var5 / var23;
                        var28 -= (-var5 + var24) / 2;
                        int var25 = GameData.animationCharacterColour[var16];
                        int var26 = 0;
                        if (var25 == 1) {
                            var26 = GameData.npcColourSkin[var9.npcId];
                            var25 = GameData.npcColourHair[var9.npcId];
                        } else if (var25 == 2) {
                            var25 = GameData.npcColourTop[var9.npcId];
                            var26 = GameData.npcColourSkin[var9.npcId];
                        } else if (var25 == 3) {
                            var25 = GameData.npcColourBottom[var9.npcId];
                            var26 = GameData.npcColourSkin[var9.npcId];
                        }

                        this.surface.method409(var25, var20, var2 - -var29, -103, var3, var26, var24, var28 + var4, var11, var7);
                    }
                }
            }
        }

        if (var1 != 66) {
            this.resetLoginVars((byte) 42);
        }

        if (var9.messageTimeout > 0) {
            this.playerMessageMidPoint[this.playerMessagesCount] = this.surface.textWidth(1, Utility.bitwiseXor(var1, -61), var9.message) / 2;
            if (this.playerMessageMidPoint[this.playerMessagesCount] > 150) {
                this.playerMessageMidPoint[this.playerMessagesCount] = 150;
            }

            this.playerMessageHeight[this.playerMessagesCount] = this.surface.textWidth(1, -127, var9.message) / 300 * this.surface.textHeight(1, true);
            this.playerMessageX[this.playerMessagesCount] = var4 - -(var5 / 2);
            this.playerMessageY[this.playerMessagesCount] = var2;
            this.playerMessages[this.playerMessagesCount++] = var9.message;
        }

        if (var9.animationCurrent == 8 || var9.animationCurrent == 9 || var9.combatTimeout != 0) {
            if (var9.combatTimeout > 0) {
                var15 = var4;
                if (var9.animationCurrent == 8) {
                    var15 = var4 - 20 * var6 / 100;
                } else if (var9.animationCurrent == 9) {
                    var15 = var4 + var6 * 20 / 100;
                }

                var16 = 30 * var9.healthCurrent / var9.healthMax;
                this.playerHealthBarX[this.anInt1365] = var15 - -(var5 / 2);
                this.playerHealthBarY[this.anInt1365] = var2;
                this.playerHealthBarMissing[this.anInt1365++] = var16;
            }

            if (var9.combatTimeout <= 150) {
                return;
            }

            var15 = var4;
            if (var9.animationCurrent == 8) {
                var15 = var4 - 10 * var6 / 100;
            } else if (var9.animationCurrent == 9) {
                var15 = var4 + var6 * 10 / 100;
            }

            this.surface.drawSprite((byte) 61, -12 + var3 / 2 + var2, this.spriteMedia - -12, -12 + var15 + var5 / 2);
            this.surface.drawstringCenter(String.valueOf(var9.damageTaken), 3, 5 + var3 / 2 + var2, var5 / 2 + (var15 - 1), 0xffffff);
        }
        ;
    }

    private void ignoreAdd(String name) {
        if (ignoreListCount >= 100) {
            this.showMessage(0, "Ignore list full", (String) null, (String) null, 0, (String) null, false);
        } else {
            String formattedName = Utility.formatName(name);
            if (formattedName != null) {
                for (int i = 0; i < ignoreListCount; ++i) {
                    if (formattedName.equals(Utility.formatName(ignoreListNames[i]))) {
                        this.showMessage(0, name + " is already on your ignore list", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }

                    if (ignoreListOldNames[i] != null && formattedName.equals(Utility.formatName(ignoreListOldNames[i]))) {
                        this.showMessage(0, name + " is already on your ignore list", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }
                }

                for (int i = 0; friendListCount > i; ++i) {
                    if (formattedName.equals(Utility.formatName(friendListNames[i]))) {
                        this.showMessage(0, "Please remove " + name + " from your friends list first", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }

                    if (friendListOldNames[i] != null && formattedName.equals(Utility.formatName(friendListOldNames[i]))) {
                        this.showMessage(0, "Please remove " + name + " from your friends list first", (String) null, (String) null, 0, (String) null, false);
                        return;
                    }
                }

                if (formattedName.equals(Utility.formatName(this.localPlayer.accountName))) {
                    this.showMessage(0, "You can\'t add yourself to your ignore list", (String) null, (String) null, 0, (String) null, false);
                } else {
                    this.clientStream.newPacket(132);
                    this.clientStream.writeBuffer.pjstr2(name);
                    this.clientStream.sendPacket();
                }
            }
        }
    }

    final void method109(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8) {
        Character var9 = this.players[var6];
        if (var9.colourBottom != 255) {
            int var10 = var9.animationCurrent + (16 + this.cameraRotation) / 32 & 7;
            boolean var11 = false;
            int var12 = var10;
            if (var10 == 5) {
                var12 = 3;
                var11 = true;
            } else if (var10 == 6) {
                var12 = 2;
                var11 = true;
            } else if (var10 == 7) {
                var11 = true;
                var12 = 1;
            }

            int var13 = 3 * var12 + this.anIntArray1405[var9.stepCount / 6 % 4];
            if (var1 < -45) {
                if (var9.animationCurrent != 8) {
                    if (var9.animationCurrent == 9) {
                        var12 = 5;
                        var11 = true;
                        var3 += 5 * var7 / 100;
                        var10 = 2;
                        var13 = this.anIntArray1445[this.anInt1360 / 6 % 8] + var12 * 3;
                    }
                } else {
                    var3 -= 5 * var7 / 100;
                    var11 = false;
                    var12 = 5;
                    var10 = 2;
                    var13 = var12 * 3 - -this.anIntArray1525[this.anInt1360 / 5 % 8];
                }

                int var15;
                int var16;
                int var29;
                for (int var14 = 0; var14 < 12; ++var14) {
                    var15 = this.anIntArrayArray1524[var10][var14];
                    var16 = -1 + var9.equippedItem[var15];
                    if (var16 >= 0) {
                        byte var17 = 0;
                        byte var18 = 0;
                        int var19 = var13;
                        if (var11 && var12 >= 1 && var12 <= 3) {
                            if (GameData.animationHasF[var16] != 1) {
                                if (var15 == 4 && var12 == 1) {
                                    var19 = var12 * 3 + this.anIntArray1405[(2 + var9.stepCount / 6) % 4];
                                    var17 = -22;
                                    var18 = -3;
                                } else if (var15 == 4 && var12 == 2) {
                                    var19 = this.anIntArray1405[(var9.stepCount / 6 + 2) % 4] + 3 * var12;
                                    var17 = 0;
                                    var18 = -8;
                                } else if (var15 == 4 && var12 == 3) {
                                    var18 = -5;
                                    var17 = 26;
                                    var19 = 3 * var12 + this.anIntArray1405[(2 - -(var9.stepCount / 6)) % 4];
                                } else if (var15 == 3 && var12 == 1) {
                                    var18 = 3;
                                    var17 = 22;
                                    var19 = this.anIntArray1405[(var9.stepCount / 6 + 2) % 4] + var12 * 3;
                                } else if (var15 == 3 && var12 == 2) {
                                    var18 = 8;
                                    var19 = this.anIntArray1405[(2 + var9.stepCount / 6) % 4] + var12 * 3;
                                    var17 = 0;
                                } else if (var15 == 3 && var12 == 3) {
                                    var19 = var12 * 3 + this.anIntArray1405[(2 + var9.stepCount / 6) % 4];
                                    var18 = 5;
                                    var17 = -26;
                                }
                            } else {
                                var19 = var13 + 15;
                            }
                        }

                        if (var12 != 5 || GameData.animationHasA[var16] == 1) {
                            int var20 = GameData.animationNumber[var16] + var19;
                            int var21 = this.surface.spriteWidthFull[var20];
                            int var22 = this.surface.spriteHeightFull[var20];
                            int var23 = this.surface.spriteWidthFull[GameData.animationNumber[var16]];
                            if (var21 != 0 && var22 != 0 && var23 != 0) {
                                var29 = var17 * var2 / var21;
                                int var28 = var18 * var4 / var22;
                                int var24 = var21 * var2 / var23;
                                var29 -= (-var2 + var24) / 2;
                                int var25 = GameData.animationCharacterColour[var16];
                                if (var25 != 1) {
                                    if (var25 == 2) {
                                        var25 = this.anIntArray1592[var9.colourTop];
                                    } else if (var25 == 3) {
                                        var25 = this.anIntArray1592[var9.colourBottom];
                                    }
                                } else {
                                    var25 = this.anIntArray1465[var9.colourHair];
                                }

                                int var26 = this.anIntArray1548[var9.colourSkin];
                                this.surface.method409(var25, var20, var28 + var5, -110, var4, var26, var24, var29 + var3, var11, var8);
                            }
                        }
                    }
                }

                if (var9.messageTimeout > 0) {
                    this.playerMessageMidPoint[this.playerMessagesCount] = this.surface.textWidth(1, -127, var9.message) / 2;
                    if (this.playerMessageMidPoint[this.playerMessagesCount] > 150) {
                        this.playerMessageMidPoint[this.playerMessagesCount] = 150;
                    }

                    this.playerMessageHeight[this.playerMessagesCount] = this.surface.textWidth(1, -127, var9.message) / 300 * this.surface.textHeight(1, true);
                    this.playerMessageX[this.playerMessagesCount] = var2 / 2 + var3;
                    this.playerMessageY[this.playerMessagesCount] = var5;
                    this.playerMessages[this.playerMessagesCount++] = var9.message;
                }

                if (var9.bubbleTimeout > 0) {
                    this.playerActionBubbleX[this.anInt1461] = var3 + var2 / 2;
                    this.playerActionBubbleY[this.anInt1461] = var5;
                    this.playerActionBubbleScale[this.anInt1461] = var7;
                    this.playerActionBubbleItem[this.anInt1461++] = var9.bubbleItem;
                }

                if (var9.animationCurrent == 8 || var9.animationCurrent == 9 || var9.combatTimeout != 0) {
                    if (var9.combatTimeout > 0) {
                        var15 = var3;
                        if (var9.animationCurrent != 8) {
                            if (var9.animationCurrent == 9) {
                                var15 = var3 + var7 * 20 / 100;
                            }
                        } else {
                            var15 = var3 - var7 * 20 / 100;
                        }

                        var16 = 30 * var9.healthCurrent / var9.healthMax;
                        this.playerHealthBarX[this.anInt1365] = var2 / 2 + var15;
                        this.playerHealthBarY[this.anInt1365] = var5;
                        this.playerHealthBarMissing[this.anInt1365++] = var16;
                    }

                    if (var9.combatTimeout > 150) {
                        var15 = var3;
                        if (var9.animationCurrent != 8) {
                            if (var9.animationCurrent == 9) {
                                var15 = var3 + 10 * var7 / 100;
                            }
                        } else {
                            var15 = var3 - 10 * var7 / 100;
                        }

                        this.surface.drawSprite((byte) 61, -12 + var4 / 2 + var5, this.spriteMedia - -11, var2 / 2 + var15 + -12);
                        this.surface.drawstringCenter(String.valueOf(var9.damageTaken), 3, var5 - (-(var4 / 2) - 5), var2 / 2 + (var15 - 1), 0xffffff);
                    }
                }

                if (var9.skullVisible == 1 && var9.bubbleTimeout == 0) {
                    var15 = var2 / 2 + (var8 - -var3);
                    if (var9.animationCurrent != 8) {
                        if (var9.animationCurrent == 9) {
                            var15 += var7 * 20 / 100;
                        }
                    } else {
                        var15 -= 20 * var7 / 100;
                    }

                    var16 = 16 * var7 / 100;
                    var29 = var7 * 16 / 100;
                    this.surface.method426(-(var29 / 2) + (var5 - 10 * var7 / 100), var29, -(var16 / 2) + var15, 13 + this.spriteMedia, (byte) -85, var16);
                }
            }
        }
    }

    private void drawDialogWildWarn(int var1) {
        byte var2 = 97;
        this.surface.drawBox(86, 77, 340, 180, 0);
        this.surface.drawBoxEdge(86, 77, 340, 180, 0xffffff);
        this.surface.drawstringCenter("Warning! Proceed with caution", 4, var2, 256, 0xff0000);
        int var5 = var2 + 26;
        this.surface.drawstringCenter("If you go much further north you will enter the", 1, var5, 256, 0xffffff);
        var5 += 13;
        this.surface.drawstringCenter("wilderness. This a very dangerous area where", 1, var5, 256, 0xffffff);
        var5 += 13;
        this.surface.drawstringCenter("other players can attack you!", 1, var5, 256, 0xffffff);
        var5 += 22;
        if (var1 == -2109) {
            this.surface.drawstringCenter("The further north you go the more dangerous it", 1, var5, 256, 0xffffff);
            var5 += 13;
            this.surface.drawstringCenter("becomes, but the more treasure you will find.", 1, var5, 256, 0xffffff);
            var5 += 22;
            this.surface.drawstringCenter("In the wilderness an indicator at the bottom-right", 1, var5, 256, 0xffffff);
            var5 += 13;
            this.surface.drawstringCenter("of the screen will show the current level of danger", 1, var5, 256, 0xffffff);
            var5 += 22;
            int var3 = 0xffffff;
            if (-12 + var5 < super.mouseY && super.mouseY <= var5 && super.mouseX > 181 && super.mouseX < 331) {
                var3 = 0xff0000;
            }

            this.surface.drawstringCenter("Click here to close window", 1, var5, 256, var3);
            if (this.mouseButtonClick != 0) {
                if ((var5 + -12) < super.mouseY && super.mouseY <= var5 && super.mouseX > 181 && super.mouseX < 331) {
                    this.showUiWildWarn = 2;
                }

                if (super.mouseX < 86 || super.mouseX > 426 || super.mouseY < 77 || super.mouseY > 257) {
                    this.showUiWildWarn = 2;
                }

                this.mouseButtonClick = 0;
            }
        }
    }

    private void updateObjectAnimation(int var1, boolean var2, String var3) {
        int var4 = this.objectY[var1];
        int var5 = this.objectX[var1];
        int var6 = -(this.localPlayer.currentX / 128) + var4;
        if (!var2) {
            int var7 = var5 - this.localPlayer.currentY / 128;
            byte var8 = 7;
            if (var4 >= 0 && var5 >= 0 && var4 < 96 && var5 < 96 && var6 > (-var8) && var8 > var6 && var7 > (-var8) && var7 < var8) {
                this.scene.freeModel((byte) 127, this.objectModel[var1]);
                int var9 = GameData.getModelIndex((byte) 56, var3);
                GameModel var10 = this.gameModels[var9].copy(1);
                this.scene.method325(var10, (byte) 6);
                var10.setLight(-10, 48, true, (byte) -47, 48, -50, -50);
                var10.method578(false, this.objectModel[var1]);
                var10.key = var1;
                this.objectModel[var1] = var10;
            }
        }
        ;
    }

    private void createMessageTabPanel(int var1) {
        this.panelMessageTabs = new Panel(this.surface, 10);
        this.controlTextListChat = this.panelMessageTabs.addTextList(5, 269, 502, 56, 1, 20, true, false);
        this.controlTextListAll = this.panelMessageTabs.addTextListInput(7, 324, 498, 14, 1, 80, false, true);
        this.controlTextListQuest = this.panelMessageTabs.addTextList(5, 269, 502, 56, 1, 20, true, false);
        this.controlTextListPrivate = this.panelMessageTabs.addTextList(var1, 269, 502, 56, 1, 20, true, false);
        this.panelMessageTabs.setFocus((byte) 75, this.controlTextListAll);
    }

    private void handleIncomingPacket(int opcode, int psize) {
        try {
            if (opcode == 191) {
                this.knownPlayerCount = this.playerCount;
                for (int index = 0; index < this.knownPlayerCount; ++index) {
                    this.knownPlayers[index] = this.players[index];
                }

                this.packetsIncoming.setBitmaskOffset();
                this.localRegionX = this.packetsIncoming.getBitMask(11);
                this.localRegionY = this.packetsIncoming.getBitMask(13);
                int anim = this.packetsIncoming.getBitMask(4);
                boolean var29 = this.loadNextRegion(this.localRegionX, this.localRegionY, (byte) 43);
                this.localRegionX -= this.regionX;
                this.localRegionY -= this.regionY;
                int var7 = 64 + this.magicLoc * this.localRegionX;
                int var8 = this.localRegionY * this.magicLoc + 64;
                this.playerCount = 0;
                if (var29) {
                    this.localPlayer.waypointCurrent = 0;
                    this.localPlayer.currentX = this.localPlayer.waypointsX[0] = var7;
                    this.localPlayer.movingStep = 0;
                    this.localPlayer.currentY = this.localPlayer.waypointsY[0] = var8;
                }

                this.localPlayer = this.createPlayer(1, this.localPlayerServerIndex, var8, var7, anim);
                int playerCount = this.packetsIncoming.getBitMask(8);

                for (int index = 0; index < playerCount; ++index) {
                    Character player = this.knownPlayers[1 + index];
                    int reqUpdate = this.packetsIncoming.getBitMask(1);
                    if (reqUpdate != 0) {
                        int updateType = this.packetsIncoming.getBitMask(1);
                        if (updateType != 0) {
                            int var14 = this.packetsIncoming.getBitMask(2);
                            if (var14 == 3) {
                                continue;
                            }

                            player.animationNext = this.packetsIncoming.getBitMask(2) + (var14 << 2);
                        } else {
                            int nextAnim = this.packetsIncoming.getBitMask(3);
                            int wp = player.waypointCurrent;
                            int x = player.waypointsX[wp];
                            int y = player.waypointsY[wp];
                            if (nextAnim == 2 || nextAnim == 1 || nextAnim == 3) {
                                x += this.magicLoc;
                            }

                            if (nextAnim == 6 || nextAnim == 5 || nextAnim == 7) {
                                x -= this.magicLoc;
                            }

                            if (nextAnim == 4 || nextAnim == 3 || nextAnim == 5) {
                                y += this.magicLoc;
                            }

                            player.animationNext = nextAnim;
                            if (nextAnim == 0 || nextAnim == 1 || nextAnim == 7) {
                                y -= this.magicLoc;
                            }

                            player.waypointCurrent = wp = (1 + wp) % 10;
                            player.waypointsX[wp] = x;
                            player.waypointsY[wp] = y;
                        }
                    }

                    this.players[this.playerCount++] = player;
                }

                while (psize * 8 > this.packetsIncoming.getBitmaskOffset() - -24) {
                    int serverIndex = this.packetsIncoming.getBitMask(11);
                    int areaX = this.packetsIncoming.getBitMask(5);
                    if (areaX > 15) {
                        areaX -= 32;
                    }

                    int areaY = this.packetsIncoming.getBitMask(5);
                    if (areaY > 15) {
                        areaY -= 32;
                    }

                    anim = this.packetsIncoming.getBitMask(4);
                    int x = this.magicLoc * (this.localRegionY + areaY) + 64;
                    int y = (this.localRegionX + areaX) * this.magicLoc + 64;
                    this.createPlayer(1, serverIndex, x, y, anim);
                }

                this.packetsIncoming.updateOffset();
                return;
            }

            if (opcode == 99) {
                while (this.packetsIncoming.offset < psize) {
                    if (this.packetsIncoming.getUnsignedByte() == 255) {
                        int newIndex = 0;
                        int x = this.localRegionX + this.packetsIncoming.getByte() >> 3;
                        int y = this.localRegionY - -this.packetsIncoming.getByte() >> 3;

                        for (int oldIndex = 0; oldIndex < this.groundItemCount; ++oldIndex) {
                            int var8 = -x + (this.groundItemX[oldIndex] >> 3);
                            int var9 = (this.groundItemY[oldIndex] >> 3) + -y;
                            if (var8 != 0 || var9 != 0) {
                                if (newIndex != oldIndex) {
                                    this.groundItemX[newIndex] = this.groundItemX[oldIndex];
                                    this.groundItemY[newIndex] = this.groundItemY[oldIndex];
                                    this.groundItemId[newIndex] = this.groundItemId[oldIndex];
                                    this.groundItemZ[newIndex] = this.groundItemZ[oldIndex];
                                }

                                ++newIndex;
                            }
                        }

                        //if (this.groundItemCount > newIndex) {
                        //    System.out.printf("Far 99 removed %d items\n", groundItemCount - newIndex);
                        //}
                        this.groundItemCount = newIndex;
                    } else {
                        --this.packetsIncoming.offset;
                        int mod = this.packetsIncoming.getUnsignedShort();
                        int x = this.localRegionX + this.packetsIncoming.getByte();
                        int y = this.localRegionY + this.packetsIncoming.getByte();
                        if (('\u8000' & mod) == 0) {
                            //System.out.printf("Adding item %s (id:%d) to %d,%d (%d, %d)\n",
                            //        GameData.itemName[mod], mod, x, y, x >> 3, y >> 3);
                            this.groundItemX[this.groundItemCount] = x;
                            this.groundItemY[this.groundItemCount] = y;
                            this.groundItemId[this.groundItemCount] = mod;
                            this.groundItemZ[this.groundItemCount] = 0;

                            for (int index = 0; this.objectCount > index; ++index) {
                                if (this.objectY[index] == x && this.objectX[index] == y) {
                                    this.groundItemZ[this.groundItemCount] = GameData.objectElevation[this.objectId[index]];
                                    break;
                                }
                            }

                            ++this.groundItemCount;
                        } else {
                            mod &= 32767;
                            //System.out.printf("Removing item %s (id:%d) from %d,%d (%d, %d)\n",
                            //        GameData.itemName[mod], mod, x, y, x >> 3, y >> 3);
                            int newIndex = 0;

                            for (int oldIndex = 0; oldIndex < this.groundItemCount; ++oldIndex) {
                                if (x == this.groundItemX[oldIndex] && y == this.groundItemY[oldIndex] && mod == this.groundItemId[oldIndex]) {
                                    mod = -123;
                                } else {
                                    if (newIndex != oldIndex) {
                                        this.groundItemX[newIndex] = this.groundItemX[oldIndex];
                                        this.groundItemY[newIndex] = this.groundItemY[oldIndex];
                                        this.groundItemId[newIndex] = this.groundItemId[oldIndex];
                                        this.groundItemZ[newIndex] = this.groundItemZ[oldIndex];
                                    }

                                    ++newIndex;
                                }
                            }

                            this.groundItemCount = newIndex;
                        }
                    }
                }

                return;
            }

            if (opcode == 48) {
                while (psize > this.packetsIncoming.offset) {
                    if (this.packetsIncoming.getUnsignedByte() == 255) {
                        int newIndex = 0;
                        int x = this.localRegionX - -this.packetsIncoming.getByte() >> 3;
                        int y = this.localRegionY + this.packetsIncoming.getByte() >> 3;

                        for (int oldIndex = 0; this.objectCount > oldIndex; ++oldIndex) {
                            int var8 = (this.objectY[oldIndex] >> 3) - x;
                            int var9 = -y + (this.objectX[oldIndex] >> 3);
                            if (var8 == 0 && var9 == 0) {
                                this.scene.freeModel((byte) 104, this.objectModel[oldIndex]);
                                this.world.removeObject(this.objectY[oldIndex], -2106, this.objectId[oldIndex], this.objectX[oldIndex]);
                            } else {
                                if (newIndex != oldIndex) {
                                    this.objectModel[newIndex] = this.objectModel[oldIndex];
                                    this.objectModel[newIndex].key = newIndex;
                                    this.objectY[newIndex] = this.objectY[oldIndex];
                                    this.objectX[newIndex] = this.objectX[oldIndex];
                                    this.objectId[newIndex] = this.objectId[oldIndex];
                                    this.objectDirection[newIndex] = this.objectDirection[oldIndex];
                                }

                                ++newIndex;
                            }
                        }

                        this.objectCount = newIndex;
                    } else {
                        --this.packetsIncoming.offset;
                        int type = this.packetsIncoming.getUnsignedShort();
                        int x = this.localRegionX + this.packetsIncoming.getByte();
                        int y = this.localRegionY + this.packetsIncoming.getByte();
                        int oldIndex = 0;

                        for (int newIndex = 0; this.objectCount > newIndex; ++newIndex) {
                            if (this.objectY[newIndex] == x && this.objectX[newIndex] == y) {
                                this.scene.freeModel((byte) 118, this.objectModel[newIndex]);
                                this.world.removeObject(this.objectY[newIndex], -2106, this.objectId[newIndex], this.objectX[newIndex]);
                            } else {
                                if (newIndex != oldIndex) {
                                    this.objectModel[oldIndex] = this.objectModel[newIndex];
                                    this.objectModel[oldIndex].key = oldIndex;
                                    this.objectY[oldIndex] = this.objectY[newIndex];
                                    this.objectX[oldIndex] = this.objectX[newIndex];
                                    this.objectId[oldIndex] = this.objectId[newIndex];
                                    this.objectDirection[oldIndex] = this.objectDirection[newIndex];
                                }

                                ++oldIndex;
                            }
                        }

                        this.objectCount = oldIndex;
                        if (type != 60000) {
                            int direction = this.world.getTileDirection(x, true, y);
                            int width, height;
                            if (direction != 0 && direction != 4) {
                                width = GameData.objectWidth[type];
                                height = GameData.objectHeight[type];
                            } else {
                                width = GameData.objectHeight[type];
                                height = GameData.objectWidth[type];
                            }

                            int modelX = (height + x - -x) * this.magicLoc / 2;
                            int modelY = this.magicLoc * (width + y + y) / 2;
                            int modelIndex = GameData.objectModelIndex[type];
                            GameModel model = this.gameModels[modelIndex].copy(1);
                            this.scene.method325(model, (byte) 6);
                            model.key = this.objectCount;
                            model.rotate(68, 0, 0, direction * 32);
                            model.translate(-this.world.getElevation(modelX, (byte) -56, modelY), modelY, (byte) 59, modelX);
                            model.setLight(-10, 48, true, (byte) -109, 48, -50, -50);
                            this.world.method274(type, (byte) -113, x, y);
                            if (type == 74) {
                                model.translate(-480, 0, (byte) 36, 0);
                            }

                            this.objectY[this.objectCount] = x;
                            this.objectX[this.objectCount] = y;
                            this.objectId[this.objectCount] = type;
                            this.objectDirection[this.objectCount] = direction;
                            this.objectModel[this.objectCount++] = model;
                        }
                    }
                }

                return;
            }

            if (opcode == 111) {
                this.inTutorial = this.packetsIncoming.getUnsignedByte() != 0;
                return;
            }

            if (opcode == 53) {
                this.inventoryItemsCount = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; this.inventoryItemsCount > index; ++index) {
                    int mod = this.packetsIncoming.getUnsignedShort();
                    this.inventoryItemId[index] = Utility.bitwiseAnd(mod, 32767);
                    this.inventoryItemEquipped[index] = mod / '\u8000';
                    if (GameData.itemStackable[mod & 32767] == 0) {
                        this.inventoryItemStackCount[index] = this.packetsIncoming.getUnsignedInt3();
                    } else {
                        this.inventoryItemStackCount[index] = 1;
                    }
                }

                return;
            }

            if (opcode == 234) {
                int playerCount = this.packetsIncoming.getUnsignedShort();

                for (int _i = 0; _i < playerCount; ++_i) {
                    int playerServerIndex = this.packetsIncoming.getUnsignedShort();
                    Character player = this.playerServer[playerServerIndex];
                    byte updateType = this.packetsIncoming.getByte();
                    if (updateType == 0) {
                        int itemType = this.packetsIncoming.getUnsignedShort();
                        if (player != null) {
                            player.bubbleItem = itemType;
                            player.bubbleTimeout = 150;
                        }
                    } else if (updateType == 1 && player != null) {
                        int modStatus = this.packetsIncoming.getUnsignedByte();
                        String message = ClientStreamBase.cabbage(this.packetsIncoming);
                        boolean ignored = false;
                        String name = Utility.formatName(player.accountName);// or account name idk
                        if (name != null) {
                            for (int index = 0; ignoreListCount > index; ++index) {
                                if (name.equals(Utility.formatName(ignoreListAccNames[index]))) {
                                    ignored = true;
                                    break;
                                }
                            }
                        }

                        if (!ignored) {
                            player.messageTimeout = 150;
                            player.message = message;
                            this.showMessage(4, player.message, player.displayName, player.accountName, modStatus, (String) null, modStatus == 2);
                        }
                    } else if (updateType == 2) {
                        int damage = this.packetsIncoming.getUnsignedByte();
                        int curhp = this.packetsIncoming.getUnsignedByte();
                        int maxhp = this.packetsIncoming.getUnsignedByte();
                        if (player != null) {
                            player.healthMax = maxhp;
                            if (player == this.localPlayer) {
                                this.playerStatCurrent[3] = curhp;
                                this.showDialogServermessage = false;
                                this.showDialogMessage = false;
                                this.playerStatBase[3] = maxhp;
                            }

                            player.healthCurrent = curhp;
                            player.damageTaken = damage;
                            player.combatTimeout = 200;
                        }
                    } else if (updateType == 3) {
                        int sprite = this.packetsIncoming.getUnsignedShort();
                        int shooterServerIndex = this.packetsIncoming.getUnsignedShort();
                        if (player != null) {
                            player.projectileRange = this.projectileMaxRange;
                            player.incomingProjectileSprite = sprite;
                            player.attackingNpcServerIndex = shooterServerIndex;
                            player.attackingPlayerServerIndex = -1;
                        }
                    } else if (updateType == 4) {
                        int sprite = this.packetsIncoming.getUnsignedShort();
                        int shooterServerIndex = this.packetsIncoming.getUnsignedShort();
                        if (player != null) {
                            player.attackingPlayerServerIndex = shooterServerIndex;
                            player.attackingNpcServerIndex = -1;
                            player.projectileRange = this.projectileMaxRange;
                            player.incomingProjectileSprite = sprite;
                        }
                    } else if (updateType == 5 && player != null) {
                        this.packetsIncoming.getUnsignedShort();// lol y u do this tho, it's ignoring the player server id cuz who cares
                        player.displayName = this.packetsIncoming.gjstr2();
                        player.accountName = this.packetsIncoming.gjstr2();
                        int equipCount = this.packetsIncoming.getUnsignedByte();

                        for (int index = 0; equipCount > index; ++index) {
                            player.equippedItem[index] = this.packetsIncoming.getUnsignedByte();
                        }

                        for (int index = equipCount; index < 12; ++index) {
                            player.equippedItem[index] = 0;
                        }

                        player.colourHair = this.packetsIncoming.getUnsignedByte();
                        player.colourTop = this.packetsIncoming.getUnsignedByte();
                        player.colourBottom = this.packetsIncoming.getUnsignedByte();
                        player.colourSkin = this.packetsIncoming.getUnsignedByte();
                        player.level = this.packetsIncoming.getUnsignedByte();
                        player.skullVisible = this.packetsIncoming.getUnsignedByte();
                    } else if (updateType == 6 && player != null) {
                        String message = ClientStreamBase.cabbage(this.packetsIncoming);
                        player.message = message;
                        player.messageTimeout = 150;
                        if (player == this.localPlayer) {
                            this.showMessage(3, player.message, player.displayName, player.accountName, 0, (String) null, false);
                        }
                    } else {
                        // todo ?????????????????????
                        int i = this.packetsIncoming.getUnsignedShort();
                        String s = this.packetsIncoming.gjstr2();
                        String s1 = this.packetsIncoming.gjstr2();
                        int var9 = this.packetsIncoming.getUnsignedByte();
                        this.packetsIncoming.offset += 6 + var9;
                        System.out.println("????? " + i + " \"" + s + "\" \"" + s1 + "\" " + var9);
                    }
                }

                return;
            }

            if (opcode == 91) {
                while (psize > this.packetsIncoming.offset) {
                    if (this.packetsIncoming.getUnsignedByte() != 255) {
                        --this.packetsIncoming.offset;
                        int type = this.packetsIncoming.getUnsignedShort();
                        int x = this.localRegionX + this.packetsIncoming.getByte();
                        int y = this.localRegionY - -this.packetsIncoming.getByte();
                        byte direction = this.packetsIncoming.getByte();
                        int newIndex = 0;

                        for (int oldIndex = 0; oldIndex < this.wallObjectCount; ++oldIndex) {
                            if (x == this.wallObjectX[oldIndex] && y == this.wallObjectY[oldIndex] && this.wallObjectDirection[oldIndex] == direction) {
                                this.scene.freeModel((byte) 123, this.wallObjectModel[oldIndex]);
                                this.world.removeWallObject(54, this.wallObjectY[oldIndex], this.wallObjectX[oldIndex], this.wallObjectDirection[oldIndex], this.wallObjectId[oldIndex]);
                            } else {
                                if (oldIndex != newIndex) {
                                    this.wallObjectModel[newIndex] = this.wallObjectModel[oldIndex];
                                    this.wallObjectModel[newIndex].key = 10000 + newIndex;
                                    this.wallObjectX[newIndex] = this.wallObjectX[oldIndex];
                                    this.wallObjectY[newIndex] = this.wallObjectY[oldIndex];
                                    this.wallObjectDirection[newIndex] = this.wallObjectDirection[oldIndex];
                                    this.wallObjectId[newIndex] = this.wallObjectId[oldIndex];
                                }

                                ++newIndex;
                            }
                        }

                        this.wallObjectCount = newIndex;
                        if (type != '\uffff') {
                            this.world.setObjectAdjacency(type, y, x, -36, direction);
                            GameModel model = this.createModel(this.wallObjectCount, 94, y, x, direction, type);
                            this.wallObjectModel[this.wallObjectCount] = model;
                            this.wallObjectX[this.wallObjectCount] = x;
                            this.wallObjectY[this.wallObjectCount] = y;
                            this.wallObjectId[this.wallObjectCount] = type;
                            this.wallObjectDirection[this.wallObjectCount++] = direction;
                        }
                    } else {
                        int newIndex = 0;
                        int x = this.localRegionX - -this.packetsIncoming.getByte() >> 3;
                        int y = this.localRegionY + this.packetsIncoming.getByte() >> 3;

                        for (int oldIndex = 0; this.wallObjectCount > oldIndex; ++oldIndex) {
                            int var8 = (this.wallObjectX[oldIndex] >> 3) + -x;
                            int var9 = (this.wallObjectY[oldIndex] >> 3) - y;
                            if (var8 == 0 && var9 == 0) {
                                this.scene.freeModel((byte) 99, this.wallObjectModel[oldIndex]);
                                this.world.removeWallObject(-127, this.wallObjectY[oldIndex], this.wallObjectX[oldIndex], this.wallObjectDirection[oldIndex], this.wallObjectId[oldIndex]);
                            } else {
                                if (oldIndex != newIndex) {
                                    this.wallObjectModel[newIndex] = this.wallObjectModel[oldIndex];
                                    this.wallObjectModel[newIndex].key = newIndex + 10000;
                                    this.wallObjectX[newIndex] = this.wallObjectX[oldIndex];
                                    this.wallObjectY[newIndex] = this.wallObjectY[oldIndex];
                                    this.wallObjectDirection[newIndex] = this.wallObjectDirection[oldIndex];
                                    this.wallObjectId[newIndex] = this.wallObjectId[oldIndex];
                                }

                                ++newIndex;
                            }
                        }

                        this.wallObjectCount = newIndex;
                    }
                }

                return;
            }

            if (opcode == 79) {
                this.npcCacheCount = this.npcCount;
                this.npcCount = 0;
                for (int index = 0; index < this.npcCacheCount; ++index) {
                    this.npcsCache[index] = this.npcs[index];
                }

                this.packetsIncoming.setBitmaskOffset();
                int npcCount = this.packetsIncoming.getBitMask(8);

                for (int index = 0; index < npcCount; ++index) {
                    Character npc = this.npcsCache[index];
                    int reqUpdate = this.packetsIncoming.getBitMask(1);
                    if (reqUpdate != 0) {
                        int updateType = this.packetsIncoming.getBitMask(1);
                        if (updateType != 0) {
                            int var26 = this.packetsIncoming.getBitMask(2);
                            if (var26 == 3) {
                                continue;
                            }

                            npc.animationNext = (var26 << 2) + this.packetsIncoming.getBitMask(2);
                        } else {
                            int nextAnim = this.packetsIncoming.getBitMask(3);
                            int var11 = npc.waypointCurrent;
                            int var12 = npc.waypointsX[var11];
                            if (nextAnim == 2 || nextAnim == 1 || nextAnim == 3) {
                                var12 += this.magicLoc;
                            }

                            int var13 = npc.waypointsY[var11];
                            if (nextAnim == 6 || nextAnim == 5 || nextAnim == 7) {
                                var12 -= this.magicLoc;
                            }

                            if (nextAnim == 4 || nextAnim == 3 || nextAnim == 5) {
                                var13 += this.magicLoc;
                            }

                            if (nextAnim == 0 || nextAnim == 1 || nextAnim == 7) {
                                var13 -= this.magicLoc;
                            }

                            npc.waypointCurrent = var11 = (var11 - -1) % 10;
                            npc.animationNext = nextAnim;
                            npc.waypointsX[var11] = var12;
                            npc.waypointsY[var11] = var13;
                        }
                    }

                    this.npcs[this.npcCount++] = npc;
                }

                while (this.packetsIncoming.getBitmaskOffset() - -34 < psize * 8) {
                    int serverIndex = this.packetsIncoming.getBitMask(12);
                    int areaX = this.packetsIncoming.getBitMask(5);
                    if (areaX > 15) {
                        areaX -= 32;
                    }

                    int areaY = this.packetsIncoming.getBitMask(5);
                    if (areaY > 15) {
                        areaY -= 32;
                    }

                    int sprite = this.packetsIncoming.getBitMask(4);
                    int x = 64 + (areaX + this.localRegionX) * this.magicLoc;
                    int y = (areaY + this.localRegionY) * this.magicLoc - -64;
                    int type = this.packetsIncoming.getBitMask(10);
                    if (GameData.npcCount <= type) {
                        type = 24;
                    }
                    this.createNpc(serverIndex, x, sprite, (byte) -70, type, y);
                }

                this.packetsIncoming.updateOffset();
                return;
            }

            if (opcode == 104) {
                int npcCount = this.packetsIncoming.getUnsignedShort();

                for (int index = 0; npcCount > index; ++index) {
                    int serverIndex = this.packetsIncoming.getUnsignedShort();
                    Character npc = this.npcsServer[serverIndex];
                    int updateType = this.packetsIncoming.getUnsignedByte();
                    if (updateType != 1) {
                        if (updateType == 2) {
                            int damage = this.packetsIncoming.getUnsignedByte();
                            int curhp = this.packetsIncoming.getUnsignedByte();
                            int maxhp = this.packetsIncoming.getUnsignedByte();
                            if (npc != null) {
                                npc.damageTaken = damage;
                                npc.healthMax = maxhp;
                                npc.healthCurrent = curhp;
                                npc.combatTimeout = 200;
                            }
                        }
                    } else {
                        int playerServerIndex = this.packetsIncoming.getUnsignedShort();
                        if (npc != null) {
                            String message = ClientStreamBase.cabbage(this.packetsIncoming);
                            npc.message = message;
                            npc.messageTimeout = 150;
                            if (playerServerIndex == this.localPlayer.serverIndex) {
                                this.showMessage(3, GameData.npcName[npc.npcId] + ": " + npc.message, (String) null, (String) null, 0, "@yel@", false);
                            }
                        }
                    }
                }

                return;
            }

            if (opcode == 245) {
                this.showOptionMenu = true;
                int optionMenuCount = this.packetsIncoming.getUnsignedByte();
                this.optionMenuCount = optionMenuCount;

                for (int index = 0; optionMenuCount > index; ++index) {
                    this.optionMenuEntry[index] = this.packetsIncoming.gjstr2();
                }

                return;
            }

            if (opcode == 252) {
                this.showOptionMenu = false;
                return;
            }

            if (opcode == 25) {
                this.loadingArea = true;
                this.localPlayerServerIndex = this.packetsIncoming.getUnsignedShort();
                this.planeWidth = this.packetsIncoming.getUnsignedShort();
                this.planeHeight = this.packetsIncoming.getUnsignedShort();
                this.planeIndex = this.packetsIncoming.getUnsignedShort();
                this.planeMultiplier = this.packetsIncoming.getUnsignedShort();
                this.planeHeight -= this.planeIndex * this.planeMultiplier;
                return;
            }

            if (opcode == 156) {
                for (int skill = 0; skill < 18; ++skill) {
                    this.playerStatCurrent[skill] = this.packetsIncoming.getUnsignedByte();
                }

                for (int skill = 0; skill < 18; ++skill) {
                    this.playerStatBase[skill] = this.packetsIncoming.getUnsignedByte();
                }

                for (int skill = 0; skill < 18; ++skill) {
                    this.playerExperience[skill] = this.packetsIncoming.getUnsignedInt();
                }

                this.playerQuestPoints = this.packetsIncoming.getUnsignedByte();
                return;
            }

            if (opcode == 153) {
                for (int equip = 0; equip < 5; ++equip) {
                    this.playerStatEquipment[equip] = this.packetsIncoming.getUnsignedByte();
                }

                return;
            }

            if (opcode == 83) {
                this.deathScreenTimeout = 250;
                return;
            }

            if (opcode == 211) {
                int entityCount = (psize - 1) / 4;

                for (int index = 0; entityCount > index; ++index) {
                    int x = this.localRegionX - -this.packetsIncoming.getSignedShort() >> 3;
                    int y = this.localRegionY + this.packetsIncoming.getSignedShort() >> 3;
                    int newIndex = 0;

                    for (int oldIndex = 0; this.groundItemCount > oldIndex; ++oldIndex) {
                        int gx = (this.groundItemX[oldIndex] >> 3) - x;
                        int gy = -y + (this.groundItemY[oldIndex] >> 3);
                        if (gx != 0 || gy != 0) {
                            if (newIndex != oldIndex) {
                                this.groundItemX[newIndex] = this.groundItemX[oldIndex];
                                this.groundItemY[newIndex] = this.groundItemY[oldIndex];
                                this.groundItemId[newIndex] = this.groundItemId[oldIndex];
                                this.groundItemZ[newIndex] = this.groundItemZ[oldIndex];
                            }

                            ++newIndex;
                        }
                    }

                    //if (this.groundItemCount > newIndex) {
                    //    System.out.printf("Far 211 removed %d items\n", groundItemCount - newIndex);
                    //}
                    this.groundItemCount = newIndex;
                    newIndex = 0;

                    for (int oldIndex = 0; oldIndex < this.objectCount; ++oldIndex) {
                        int var11 = (this.objectY[oldIndex] >> 3) - x;
                        int var12 = -y + (this.objectX[oldIndex] >> 3);
                        if (var11 == 0 && var12 == 0) {
                            this.scene.freeModel((byte) 102, this.objectModel[oldIndex]);
                            this.world.removeObject(this.objectY[oldIndex], -2106, this.objectId[oldIndex], this.objectX[oldIndex]);
                        } else {
                            if (oldIndex != newIndex) {
                                this.objectModel[newIndex] = this.objectModel[oldIndex];
                                this.objectModel[newIndex].key = newIndex;
                                this.objectY[newIndex] = this.objectY[oldIndex];
                                this.objectX[newIndex] = this.objectX[oldIndex];
                                this.objectId[newIndex] = this.objectId[oldIndex];
                                this.objectDirection[newIndex] = this.objectDirection[oldIndex];
                            }

                            ++newIndex;
                        }
                    }

                    this.objectCount = newIndex;
                    newIndex = 0;

                    for (int oldIndex = 0; oldIndex < this.wallObjectCount; ++oldIndex) {
                        int var12 = (this.wallObjectX[oldIndex] >> 3) - x;
                        int var13 = (this.wallObjectY[oldIndex] >> 3) - y;
                        if (var12 == 0 && var13 == 0) {
                            this.scene.freeModel((byte) 108, this.wallObjectModel[oldIndex]);
                            this.world.removeWallObject(70, this.wallObjectY[oldIndex], this.wallObjectX[oldIndex], this.wallObjectDirection[oldIndex], this.wallObjectId[oldIndex]);
                        } else {
                            if (oldIndex != newIndex) {
                                this.wallObjectModel[newIndex] = this.wallObjectModel[oldIndex];
                                this.wallObjectModel[newIndex].key = newIndex + 10000;
                                this.wallObjectX[newIndex] = this.wallObjectX[oldIndex];
                                this.wallObjectY[newIndex] = this.wallObjectY[oldIndex];
                                this.wallObjectDirection[newIndex] = this.wallObjectDirection[oldIndex];
                                this.wallObjectId[newIndex] = this.wallObjectId[oldIndex];
                            }

                            ++newIndex;
                        }
                    }

                    this.wallObjectCount = newIndex;
                }

                return;
            }

            if (opcode == 59) {
                this.showAppearanceChange = true;
                return;
            }

            if (opcode == 92) {
                int serverIndex = this.packetsIncoming.getUnsignedShort();
                if (this.playerServer[serverIndex] != null) {
                    this.tradeRecipientName = this.playerServer[serverIndex].displayName;
                }

                this.showDialogTrade = true;
                this.tradeAccepted = false;
                this.tradeRecipientItemsCount = 0;
                this.tradeRecipientAccepted = false;
                this.tradeItemsCount = 0;
                return;
            }

            if (opcode == 128) {
                this.showDialogTrade = false;
                this.showDialogTradeConfirm = false;
                return;
            }

            if (opcode == 97) {
                this.tradeRecipientItemsCount = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; this.tradeRecipientItemsCount > index; ++index) {
                    this.tradeRecipientItems[index] = this.packetsIncoming.getUnsignedShort();
                    this.tradeRecipientItemCount[index] = this.packetsIncoming.getUnsignedInt();
                }

                this.tradeRecipientAccepted = false;
                this.tradeAccepted = false;
                return;
            }

            if (opcode == 162) {
                int accepted = this.packetsIncoming.getUnsignedByte();
                if (accepted != 1) {
                    this.tradeRecipientAccepted = false;
                    return;
                }

                this.tradeRecipientAccepted = true;
                return;
            }

            if (opcode == 101) {
                this.showDialogShop = true;
                int shopItemCount = this.packetsIncoming.getUnsignedByte();
                byte shopType = this.packetsIncoming.getByte();
                this.shopSellPriceMod = this.packetsIncoming.getUnsignedByte();
                this.shopBuyPriceMod = this.packetsIncoming.getUnsignedByte();
                this.shopPriceMultiplier = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; index < 40; ++index) {
                    this.shopItem[index] = -1;
                }

                for (int index = 0; shopItemCount > index; ++index) {
                    this.shopItem[index] = this.packetsIncoming.getUnsignedShort();
                    this.shopItemCount[index] = this.packetsIncoming.getUnsignedShort();
                    this.shopItemPrice[index] = this.packetsIncoming.getUnsignedShort();
                }

                if (shopType == 1) {
                    int var8 = 39;

                    for (int inventoryIndex = 0; inventoryIndex < this.inventoryItemsCount && shopItemCount <= var8; ++inventoryIndex) {
                        boolean var10 = false;

                        for (int shopIndex = 0; shopIndex < 40; ++shopIndex) {
                            if (this.inventoryItemId[inventoryIndex] == this.shopItem[shopIndex]) {
                                var10 = true;
                                break;
                            }
                        }

                        if (this.inventoryItemId[inventoryIndex] == 10) {
                            var10 = true;
                        }

                        if (!var10) {
                            this.shopItem[var8] = Utility.bitwiseAnd(32767, this.inventoryItemId[inventoryIndex]);
                            this.shopItemCount[var8] = 0;
                            this.shopItemPrice[var8] = 0;
                            --var8;
                        }
                    }
                }

                if (this.shopSelectedItemIndex >= 0 && this.shopSelectedItemIndex < 40 && this.shopSelectedItemType != this.shopItem[this.shopSelectedItemIndex]) {
                    this.shopSelectedItemIndex = -1;
                    this.shopSelectedItemType = -2;
                }

                return;
            }

            if (opcode == 137) {
                this.showDialogShop = false;
                return;
            }

            if (opcode == 15) {
                int accepted = this.packetsIncoming.getByte();
                if (accepted != 1) {
                    this.tradeAccepted = false;
                    return;
                }

                this.tradeAccepted = true;
                return;
            }

            if (opcode == 240) {
                this.optionCameraModeAuto = this.packetsIncoming.getUnsignedByte() == 1;
                this.optionMouseButtonOne = this.packetsIncoming.getUnsignedByte() == 1;
                this.optionSoundDisabled = this.packetsIncoming.getUnsignedByte() == 1;
                return;
            }

            if (opcode == 206) {
                for (int index = 0; index < psize + -1; ++index) {
                    boolean on = this.packetsIncoming.getByte() == 1;
                    if (!this.prayerOn[index] && on) {
                        this.playSoundFile("prayeron");
                    }

                    if (this.prayerOn[index] && !on) {
                        this.playSoundFile("prayeroff");
                    }

                    this.prayerOn[index] = on;
                }

                return;
            }

            if (opcode == 5) {
                for (int index = 0; index < 50; ++index) {
                    this.questComplete[index] = this.packetsIncoming.getByte() == 1;
                }

                return;
            }

            if (opcode == 42) {
                this.showDialogBank = true;
                this.newBankItemCount = this.packetsIncoming.getUnsignedByte();
                this.bankItemsMax = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; index < this.newBankItemCount; ++index) {
                    this.newBankItems[index] = this.packetsIncoming.getUnsignedShort();
                    this.newBankItemsCount[index] = this.packetsIncoming.getUnsignedInt3();
                }

                this.updateBankItems(-1129);
                return;
            }

            if (opcode == 203) {
                this.showDialogBank = false;
                return;
            }

            if (opcode == 33) {
                int skill = this.packetsIncoming.getUnsignedByte();
                this.playerExperience[skill] = this.packetsIncoming.getUnsignedInt();
                return;
            }

            if (opcode == 176) {
                int serverIndex = this.packetsIncoming.getUnsignedShort();
                if (this.playerServer[serverIndex] != null) {
                    this.duelConfirmOpponentName = this.playerServer[serverIndex].displayName;
                }

                this.duelOfferAccepted = false;
                this.duelOfferItemCount = 0;
                this.showDialogDuel = true;
                this.duelSettingsPrayer = false;
                this.duelSettingsMagic = false;
                this.duelOfferOpponentAccepted = false;
                this.duelOfferOpponentItemCount = 0;
                this.duelSettingsRetreat = false;
                this.duelSettingsWeapons = false;
                return;
            }

            if (opcode == 225) {
                this.showDialogDuelConfirm = false;
                this.showDialogDuel = false;
                return;
            }

            if (opcode == 20) {
                this.showDialogTrade = false;
                this.showDialogTradeConfirm = true;
                this.tradeConfirmAccepted = false;
                this.tradeRecipientConfirmName = this.packetsIncoming.gjstr2();
                this.tradeRecipientConfirmItemsCount = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; index < this.tradeRecipientConfirmItemsCount; ++index) {
                    this.traceRecipientConfirmItems[index] = this.packetsIncoming.getUnsignedShort();
                    this.traceRecipientConfirmItemCount[index] = this.packetsIncoming.getUnsignedInt();
                }

                this.tradeConfirmItemsCount = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; this.tradeConfirmItemsCount > index; ++index) {
                    this.tradeConfirmItems[index] = this.packetsIncoming.getUnsignedShort();
                    this.tradeConfirmItemCount[index] = this.packetsIncoming.getUnsignedInt();
                }

                return;
            }

            if (opcode == 6) {
                this.duelOfferOpponentItemCount = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; this.duelOfferOpponentItemCount > index; ++index) {
                    this.duelOpponentItemId[index] = this.packetsIncoming.getUnsignedShort();
                    this.duelOfferOpponentItemStack[index] = this.packetsIncoming.getUnsignedInt();
                }

                this.duelOfferAccepted = false;
                this.duelOfferOpponentAccepted = false;
                return;
            }

            if (opcode == 30) {
                if (this.packetsIncoming.getUnsignedByte() == 1) {
                    this.duelSettingsRetreat = true;
                } else {
                    this.duelSettingsRetreat = false;
                }

                if (this.packetsIncoming.getUnsignedByte() == 1) {
                    this.duelSettingsMagic = true;
                } else {
                    this.duelSettingsMagic = false;
                }

                if (this.packetsIncoming.getUnsignedByte() != 1) {
                    this.duelSettingsPrayer = false;
                } else {
                    this.duelSettingsPrayer = true;
                }

                if (this.packetsIncoming.getUnsignedByte() == 1) {
                    this.duelSettingsWeapons = true;
                } else {
                    this.duelSettingsWeapons = false;
                }

                this.duelOfferOpponentAccepted = false;
                this.duelOfferAccepted = false;
                return;
            }

            if (opcode == 249) {
                int slot = this.packetsIncoming.getUnsignedByte();
                int item = this.packetsIncoming.getUnsignedShort();
                int itemCount = this.packetsIncoming.getUnsignedInt3();
                if (itemCount == 0) {
                    --this.newBankItemCount;

                    for (int index = slot; this.newBankItemCount > index; ++index) {
                        this.newBankItems[index] = this.newBankItems[index + 1];
                        this.newBankItemsCount[index] = this.newBankItemsCount[1 + index];
                    }
                } else {
                    this.newBankItems[slot] = item;
                    this.newBankItemsCount[slot] = itemCount;
                    if (slot >= this.newBankItemCount) {
                        this.newBankItemCount = 1 + slot;
                    }
                }

                this.updateBankItems(-1129);
                return;
            }

            if (opcode == 90) {
                int stackSize = 1;
                int slot = this.packetsIncoming.getUnsignedByte();
                int mod = this.packetsIncoming.getUnsignedShort();
                if (GameData.itemStackable[mod & 32767] == 0) {
                    stackSize = this.packetsIncoming.getUnsignedInt3();
                }

                this.inventoryItemId[slot] = Utility.bitwiseAnd(mod, 32767);
                this.inventoryItemEquipped[slot] = mod / '\u8000';
                this.inventoryItemStackCount[slot] = stackSize;
                if (this.inventoryItemsCount <= slot) {
                    this.inventoryItemsCount = slot - -1;
                }

                return;
            }

            if (opcode == 123) {
                int slot = this.packetsIncoming.getUnsignedByte();
                --this.inventoryItemsCount;

                for (int index = slot; index < this.inventoryItemsCount; ++index) {
                    this.inventoryItemId[index] = this.inventoryItemId[1 + index];
                    this.inventoryItemStackCount[index] = this.inventoryItemStackCount[index - -1];
                    this.inventoryItemEquipped[index] = this.inventoryItemEquipped[index + 1];
                }

                return;
            }

            if (opcode == 159) {
                int skill = this.packetsIncoming.getUnsignedByte();
                this.playerStatCurrent[skill] = this.packetsIncoming.getUnsignedByte();
                this.playerStatBase[skill] = this.packetsIncoming.getUnsignedByte();
                this.playerExperience[skill] = this.packetsIncoming.getUnsignedInt();
                return;
            }

            if (opcode == 253) {
                int accepted = this.packetsIncoming.getByte();
                if (accepted != 1) {
                    this.duelOfferOpponentAccepted = false;
                    return;
                }

                this.duelOfferOpponentAccepted = true;
                return;
            }

            if (opcode == 210) {
                int accepted = this.packetsIncoming.getByte();
                if (accepted == 1) {
                    this.duelOfferAccepted = true;
                    return;
                }

                this.duelOfferAccepted = false;
                return;
            }

            if (opcode == 172) {
                this.duelAccepted = false;
                this.showDialogDuelConfirm = true;
                this.showDialogDuel = false;
                this.duelOpponentName = this.packetsIncoming.gjstr2();
                this.duelOpponentItemsCount = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; index < this.duelOpponentItemsCount; ++index) {
                    this.duelOpponentItems[index] = this.packetsIncoming.getUnsignedShort();
                    this.duelOpponentItemCount[index] = this.packetsIncoming.getUnsignedInt();
                }

                this.duelItemsCount = this.packetsIncoming.getUnsignedByte();

                for (int index = 0; this.duelItemsCount > index; ++index) {
                    this.duelItems[index] = this.packetsIncoming.getUnsignedShort();
                    this.duelItemCount[index] = this.packetsIncoming.getUnsignedInt();
                }

                this.duelOptionRetreat = this.packetsIncoming.getUnsignedByte();
                this.duelOptionMagic = this.packetsIncoming.getUnsignedByte();
                this.duelOptionPrayer = this.packetsIncoming.getUnsignedByte();
                this.duelOptionWeapons = this.packetsIncoming.getUnsignedByte();
                return;
            }

            if (opcode == 204) {
                String filename = this.packetsIncoming.gjstr2();
                this.playSoundFile(filename);
                return;
            }

            if (opcode == 36) {
                if (this.teleportBubbleCount < 50) {
                    int type = this.packetsIncoming.getUnsignedByte();
                    int x = this.packetsIncoming.getByte() + this.localRegionX;
                    int y = this.packetsIncoming.getByte() + this.localRegionY;
                    this.teleportBubbleType[this.teleportBubbleCount] = type;
                    this.teleportBubbleTime[this.teleportBubbleCount] = 0;
                    this.teleportBubbleX[this.teleportBubbleCount] = x;
                    this.teleportBubbleY[this.teleportBubbleCount] = y;
                    ++this.teleportBubbleCount;
                }

                return;
            }

            if (opcode == 182) {
                if (!this.welcomeScreenShown) {
                    this.welcomeLastLoggedInIp = this.packetsIncoming.getUnsignedInt();
                    this.welcomeLastLoggedInDays = this.packetsIncoming.getUnsignedShort();
                    this.welcomeRecoverySetDays = this.packetsIncoming.getUnsignedByte();
                    this.welcomeUnreadMessages = this.packetsIncoming.getUnsignedShort();
                    this.welcomeLastLoggedInHost = null;
                    this.welcomeScreenShown = true;
                    this.showDialogMessage = true;
                }

                return;
            }

            if (opcode == 89) {
                this.serverMessage = this.packetsIncoming.gjstr2();
                this.showDialogServermessage = true;
                this.serverMessageBoxTop = false;
                return;
            }

            if (opcode == 222) {
                this.serverMessage = this.packetsIncoming.gjstr2();
                this.showDialogServermessage = true;
                this.serverMessageBoxTop = true;
                return;
            }

            if (opcode == 114) {
                this.statFatigue = this.packetsIncoming.getUnsignedShort();
                return;
            }

            if (opcode == 117) {
                if (!this.isSleeping) {
                    this.fatigueSleeping = this.statFatigue;
                }

                super.inputTextCurrent = "";
                super.inputTextFinal = "";
                this.isSleeping = true;
                this.surface.readSleepWord(1 + this.spriteTexture, (byte) -120, this.packetsIncoming.buffer);
                this.sleepingStatusText = null;
                return;
            }

            if (opcode == 244) {
                this.fatigueSleeping = this.packetsIncoming.getUnsignedShort();
                return;
            }

            if (opcode == 84) {
                this.isSleeping = false;
                return;
            }

            if (opcode == 194) {
                this.sleepingStatusText = "Incorrect - Please wait...";
                return;
            }

            if (opcode == 52) {
                this.systemUpdate = 32 * this.packetsIncoming.getUnsignedShort();
                return;
            }

            if (opcode == 213) {
                System.out.println("!!!! opcode = 213");
                return;
            }
        } catch (RuntimeException var18) {
            String var5 = "T2 - " + opcode + " - " + psize + " rx:" + this.localRegionX + " ry:" + this.localRegionY + " num3l:" + this.objectCount + " - ";

            for (int i = 0; psize > i && i < 50; ++i) {
                var5 = var5 + this.packetsIncoming.buffer[i] + ",";
            }

            Utility.sendClientError(var5, -257, var18);
            this.closeConnection(true, -10);
            return;
        }

        Utility.sendClientError("T1 - " + opcode + " - " + psize, -257, (Throwable) null);
        this.closeConnection(true, -10);
    }

    private void sendChatMessage(byte var1, String var2) {
        this.clientStream.newPacket(216);
        if (var1 <= 54) {
            this.panelSocial = null;
        }

        method161((byte) 127, this.clientStream.writeBuffer, var2);
        this.clientStream.sendPacket();
    }

    private void method115(byte var1) {
        super.inputPmCurrent = "";
        if (var1 > 120) {
            super.inputPmFinal = "";
        }
    }

    final void drawTeleportBubble(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        int type = this.teleportBubbleType[var7];
        int time = this.teleportBubbleTime[var7];
        int var10;
        if (type == 0) {
            var10 = 255 + 5 * time * 256;
            this.surface.drawCircle(time * 2 + 20, var4 - -(var5 / 2), var2 - -(var1 / 2), -1385529104, var10, -(time * 5) + 255);
        }

        if (type == 1) {
            var10 = time * 5 * 256 + 0xff0000;
            this.surface.drawCircle(time + 10, var5 / 2 + var4, var2 - -(var1 / 2), -1385529104, var10, 255 + -(5 * time));
        }

        if (var3 > -63) {
            this.anInt1292 = -43;
        }
        ;
    }

    private void friendRemove(String name) {
        String formattedName = Utility.formatName(name);
        if (formattedName != null) {
            for (int i = 0; friendListCount > i; ++i) {
                if (formattedName.equals(Utility.formatName(friendListNames[i]))) {
                    --friendListCount;

                    for (int j = i; j < friendListCount; ++j) {
                        friendListNames[j] = friendListNames[1 + j];
                        friendListOldNames[j] = friendListOldNames[j - -1];
                        friendListServer[j] = friendListServer[j + 1];
                        friendListOnline[j] = friendListOnline[1 + j];
                    }

                    this.clientStream.newPacket(167);
                    this.clientStream.writeBuffer.pjstr2(name);
                    this.clientStream.sendPacket();
                    break;
                }
            }
        }
    }

    private void loadGameConfig(boolean var1) {
        byte[] var2 = this.readDataFile(0, 10, "Configuration", -10);
        if (var2 == null) {
            this.errorLoadingData = true;
        } else if (var1) {
            GameData.method41((byte) 121, this.members, var2);
        }
    }

    final void startThread(boolean var1, Runnable var2) {
        cachePackageManager.method585(1, (byte) -128, var2);
        if (var1) {
            this.method138(-100, -77);
        }
    }

    private void resetLoginVars(byte var1) {
        this.systemUpdate = 0;
        this.anInt1617 = 0;
        int var2 = 74 % ((-12 - var1) / 45);
        this.loggedIn = 0;
        this.logoutTimeout = 0;
    }

    private void loadSounds(int var1) {
        try {
            this.soundData = this.readDataFile(var1, 90, "Sound effects", var1 ^ -4);
            SoundPlayer.setSettings(50000, 1, false, 22050);
            Object gameContainer;
            if (gameFrameReference == null) {
                if (Isaac.anApplet445 != null) {
                    gameContainer = Isaac.anApplet445;
                } else {
                    gameContainer = this;
                }
            } else {
                gameContainer = gameFrameReference;
            }

            this.soundPlayer = method437((Component) gameContainer, 22050, cachePackageManager, 0);
            this.aClass8_Sub2_Sub1_1378 = new BufferBase_Sub2_Sub1();
            this.soundPlayer.method490(this.aClass8_Sub2_Sub1_1378, 16385);
        } catch (Throwable var3) {
            System.out.println("Unable to init sounds:" + var3);
        }
    }

    private Socket method121(byte var1, String var2, int var3) throws IOException {
        Socket var4;
        if (gameFrameReference == null && Isaac.anApplet445 != null) {
            CacheState var5 = cachePackageManager.method590(var3, 837317288, var2);

            while (var5.state == 0) {
                sleep(50L, -17239);
            }

            if (var5.state != 1) {
                throw new IOException();
            }

            var4 = (Socket) var5.stateObject;
            if (var4 == null) {
                throw new IOException();
            }
        } else if (gameFrameReference != null) {
            var4 = new Socket(InetAddress.getByName(var2), var3);
        } else {
            var4 = new Socket(InetAddress.getByName(this.getCodeBase().getHost()), var3);
        }

        var4.setSoTimeout(30000);
        int var7 = -118 % ((var1 - -2) / 63);
        var4.setTcpNoDelay(true);
        return var4;
    }

    private void drawDialogShop(int var1) {
        int var4;
        int var5;
        int shopItemId;
        int shopItemCount;
        int itemCount;
        if (this.mouseButtonClick != 0 && this.inputPopupType == 0) {
            this.mouseButtonClick = 0;
            int mouseX = -52 + super.mouseX;
            int mouseY = -44 + super.mouseY;
            if (mouseX < 0 || mouseY < 12 || mouseX >= 408 || mouseY >= 246) {
                this.clientStream.newPacket(166);
                this.clientStream.sendPacket();
                this.showDialogShop = false;
                return;
            }

            int itemIndex = 0;

            for (int row = 0; row < 5; ++row) {
                for (int col = 0; col < 8; ++col) {
                    shopItemCount = col * 49 + 7;
                    itemCount = 34 * row + 28;
                    if (mouseX > shopItemCount && mouseX < shopItemCount + 49 && itemCount < mouseY && (34 + itemCount) > mouseY && this.shopItem[itemIndex] != -1) {
                        this.shopSelectedItemIndex = itemIndex;
                        this.shopSelectedItemType = this.shopItem[itemIndex];
                    }

                    ++itemIndex;
                }
            }

            if (this.shopSelectedItemIndex >= 0) {
                int itemType = this.shopItem[this.shopSelectedItemIndex];
                if (itemType != -1) {
                    int shopCount = this.shopItemCount[this.shopSelectedItemIndex];
                    if (shopCount > 0 && mouseY >= 204 && mouseY <= 215) {
                        byte count = 0;
                        if (mouseX > 318 && mouseX < 330) {
                            count = 1;
                        }

                        if (shopCount >= 5 && mouseX > 333 && mouseX < 345) {
                            count = 5;
                        }

                        if (shopCount >= 10 && mouseX > 348 && mouseX < 365) {
                            count = 10;
                        }

                        if (shopCount >= 50 && mouseX > 368 && mouseX < 385) {
                            count = 50;
                        }

                        if (mouseX > 388 && mouseX < 400) {
                            this.showInputPopup(5, aStringArray819, true);
                        }

                        if (count > 0) {
                            this.clientStream.newPacket(236);
                            this.clientStream.writeBuffer.putShort(this.shopItem[this.shopSelectedItemIndex]);
                            this.clientStream.writeBuffer.putShort(shopCount);
                            this.clientStream.writeBuffer.putShort(count);
                            this.clientStream.sendPacket();
                        }
                    }

                    int invCount = this.getInventoryCount(itemType);
                    if (invCount > 0 && mouseY >= 229 && mouseY <= 240) {
                        byte count = 0;
                        if (mouseX > 318 && mouseX < 330) {
                            count = 1;
                        }

                        if (invCount >= 5 && mouseX > 333 && mouseX < 345) {
                            count = 5;
                        }

                        if (invCount >= 10 && mouseX > 348 && mouseX < 365) {
                            count = 10;
                        }

                        if (mouseX > 388 && mouseX < 400) {
                            this.showInputPopup(6, World.aStringArray237, true);
                        }

                        if (invCount >= 50 && mouseX > 368 && mouseX < 385) {
                            count = 50;
                        }

                        if (count > 0) {
                            this.clientStream.newPacket(221);
                            this.clientStream.writeBuffer.putShort(this.shopItem[this.shopSelectedItemIndex]);
                            this.clientStream.writeBuffer.putShort(shopCount);
                            this.clientStream.writeBuffer.putShort(count);
                            this.clientStream.sendPacket();
                        }
                    }
                }
            }
        }

        byte dialogX = 52;
        if (var1 == 27103) {
            byte dialogY = 44;
            this.surface.drawBox(dialogX, dialogY, 408, 12, 192);
            var4 = 10000536;
            this.surface.drawBoxAlpha(dialogX, dialogY - -12, 408, 17, var4, 160);
            this.surface.drawBoxAlpha(dialogX, dialogY - -29, 8, 170, var4, 160);
            this.surface.drawBoxAlpha(dialogX - -399, 29 + dialogY, 9, 170, var4, 160);
            this.surface.drawBoxAlpha(dialogX, 199 + dialogY, 408, 47, var4, 160);
            this.surface.drawstring("Buying and selling items", dialogX + 1, 10 + dialogY, 1, 0xffffff);
            var5 = 0xffffff;
            if (dialogX - -320 < super.mouseX && super.mouseY >= dialogY && (408 + dialogX) > super.mouseX && super.mouseY < (dialogY + 12)) {
                var5 = 0xff0000;
            }

            this.surface.drawstringRight(10 + dialogY, var5, 406 + dialogX, 1, "Close window", -5169);
            this.surface.drawstring("Shops stock in green", dialogX - -2, dialogY - -24, 1, '\uff00');
            this.surface.drawstring("Number you own in blue", 135 + dialogX, 24 + dialogY, 1, '\uffff');
            this.surface.drawstring("Your money: " + this.getInventoryCount(10) + "gp", 280 + dialogX, dialogY - -24, 1, 0xffff00);
            shopItemId = 13684944;
            shopItemCount = 0;

            int var18;
            for (itemCount = 0; itemCount < 5; ++itemCount) {
                for (var18 = 0; var18 < 8; ++var18) {
                    int sumthan = var18 * 49 + 7 + dialogX;
                    int var11 = 28 + (dialogY - -(itemCount * 34));
                    if (shopItemCount != this.shopSelectedItemIndex) {
                        this.surface.drawBoxAlpha(sumthan, var11, 49, 34, shopItemId, 160);
                    } else {
                        this.surface.drawBoxAlpha(sumthan, var11, 49, 34, 0xff0000, 160);
                    }

                    this.surface.drawBoxEdge(sumthan, var11, 50, 35, 0);
                    if (this.shopItem[shopItemCount] != -1) {
                        this.surface.method409(GameData.itemMask[this.shopItem[shopItemCount]], GameData.itemPicture[this.shopItem[shopItemCount]] + this.spriteItem, var11, -113, 32, 0, 48, sumthan, false, 0);
                        this.surface.drawstring(String.valueOf(this.shopItemCount[shopItemCount]), 1 + sumthan, 10 + var11, 1, '\uff00');
                        this.surface.drawstringRight(10 + var11, '\uffff', 47 + sumthan, 1, String.valueOf(this.getInventoryCount(this.shopItem[shopItemCount])), -5169);
                    }

                    ++shopItemCount;
                }
            }

            this.surface.drawLineHoriz(dialogX - -5, 222 + dialogY, 398, 0);
            if (this.shopSelectedItemIndex == -1) {
                this.surface.drawstringCenter("Select an object to buy or sell", 3, dialogY + 214, 204 + dialogX, 0xffff00);
            } else {
                int selectedItemType = this.shopItem[this.shopSelectedItemIndex];
                if (selectedItemType != -1) {
                    int count = this.shopItemCount[this.shopSelectedItemIndex];
                    if (count > 0) {
                        int var11 = calculateShopItemPrice(GameData.itemBasePrice[selectedItemType], count, true, this.shopBuyPriceMod, 1, this.shopItemPrice[this.shopSelectedItemIndex], this.shopPriceMultiplier);
                        this.surface.drawstring(GameData.itemName[selectedItemType] + ": buy for " + var11 + "gp each", 2 + dialogX, dialogY + 214, 1, 0xffff00);
                        boolean var12 = dialogY - -204 <= super.mouseY && super.mouseY <= (215 + dialogY);
                        var5 = 0xffffff;
                        this.surface.drawstring("Buy:", 285 + dialogX, 214 + dialogY, 3, 0xffffff);
                        if (var12 && dialogX - -318 < super.mouseX && super.mouseX < (330 + dialogX)) {
                            var5 = 0xff0000;
                        }

                        this.surface.drawstring("1", 320 + dialogX, dialogY + 214, 3, var5);
                        if (count >= 5) {
                            var5 = 0xffffff;
                            if (var12 && 333 + dialogX < super.mouseX && super.mouseX < dialogX + 345) {
                                var5 = 0xff0000;
                            }

                            this.surface.drawstring("5", 335 + dialogX, 214 + dialogY, 3, var5);
                        }

                        if (count >= 10) {
                            var5 = 0xffffff;
                            if (var12 && super.mouseX > 348 + dialogX && super.mouseX < (dialogX + 365)) {
                                var5 = 0xff0000;
                            }

                            this.surface.drawstring("10", dialogX - -350, dialogY + 214, 3, var5);
                        }

                        if (count >= 50) {
                            var5 = 0xffffff;
                            if (var12 && super.mouseX > (dialogX - -368) && super.mouseX < dialogX - -385) {
                                var5 = 0xff0000;
                            }

                            this.surface.drawstring("50", 370 + dialogX, dialogY + 214, 3, var5);
                        }

                        var5 = 0xffffff;
                        if (var12 && (388 + dialogX) < super.mouseX && dialogX - -400 > super.mouseX) {
                            var5 = 0xff0000;
                        }

                        this.surface.drawstring("X", 390 + dialogX, 214 + dialogY, 3, var5);
                    } else {
                        this.surface.drawstringCenter("This item is not currently available to buy", 3, 214 + dialogY, dialogX - -204, 0xffff00);
                    }

                    int countInv = this.getInventoryCount(selectedItemType);
                    if (countInv <= 0) {
                        this.surface.drawstringCenter("You do not have any of this item to sell", 3, dialogY - -239, dialogX - -204, 0xffff00);
                    } else {
                        int var19 = calculateShopItemPrice(GameData.itemBasePrice[selectedItemType], count, false, this.shopSellPriceMod, 1, this.shopItemPrice[this.shopSelectedItemIndex], this.shopPriceMultiplier);
                        this.surface.drawstring(GameData.itemName[selectedItemType] + ": sell for " + var19 + "gp each", 2 + dialogX, dialogY - -239, 1, 0xffff00);
                        boolean var13 = super.mouseY >= dialogY - -229 && super.mouseY <= (240 + dialogY);
                        this.surface.drawstring("Sell:", dialogX + 285, dialogY + 239, 3, 0xffffff);
                        var5 = 0xffffff;
                        if (var13 && (dialogX - -318) < super.mouseX && super.mouseX < (330 + dialogX)) {
                            var5 = 0xff0000;
                        }

                        this.surface.drawstring("1", dialogX + 320, dialogY + 239, 3, var5);
                        if (countInv >= 5) {
                            var5 = 0xffffff;
                            if (var13 && 333 + dialogX < super.mouseX && super.mouseX < 345 + dialogX) {
                                var5 = 0xff0000;
                            }

                            this.surface.drawstring("5", 335 + dialogX, dialogY + 239, 3, var5);
                        }

                        if (countInv >= 10) {
                            var5 = 0xffffff;
                            if (var13 && super.mouseX > dialogX + 348 && super.mouseX < dialogX - -365) {
                                var5 = 0xff0000;
                            }

                            this.surface.drawstring("10", dialogX - -350, dialogY + 239, 3, var5);
                        }

                        if (countInv >= 50) {
                            var5 = 0xffffff;
                            if (var13 && super.mouseX > 368 + dialogX && super.mouseX < (dialogX - -385)) {
                                var5 = 0xff0000;
                            }

                            this.surface.drawstring("50", 370 + dialogX, 239 + dialogY, 3, var5);
                        }

                        var5 = 0xffffff;
                        if (var13 && 388 + dialogX < super.mouseX && (dialogX + 400) > super.mouseX) {
                            var5 = 0xff0000;
                        }

                        this.surface.drawstring("X", dialogX - -390, 239 + dialogY, 3, var5);
                    }
                }
            }
        }
        ;
    }

    private void sendPrivateMessage(String var1, String var2, byte var3) {
        this.clientStream.newPacket(218);
        this.clientStream.writeBuffer.pjstr2(var2);
        if (var3 != -19) {
            this.anInt1577 = -58;
        }

        method161((byte) 125, this.clientStream.writeBuffer, var1);
        this.clientStream.sendPacket();
    }

    private void setActiveUiTab(int var1) {
        if (this.showUiTab == 0 && (this.surface.width2 + -35) <= super.mouseX && super.mouseY >= 3 && super.mouseX < (-3 + this.surface.width2) && super.mouseY < 35) {
            this.showUiTab = 1;
        }

        if (this.showUiTab == 0 && super.mouseX >= (-35 + this.surface.width2 - 33) && super.mouseY >= 3 && (-33 + -3 + this.surface.width2) > super.mouseX && super.mouseY < 35) {
            this.minimapRandom_1 = -6 + (int) (Math.random() * 13.0D);
            this.showUiTab = 2;
            this.minimapRandom_2 = (int) (Math.random() * 23.0D) + -11;
        }

        if (this.showUiTab == 0 && super.mouseX >= this.surface.width2 + -101 && super.mouseY >= 3 && (this.surface.width2 - 3 - 66) > super.mouseX && super.mouseY < 35) {
            this.showUiTab = 3;
        }

        if (this.showUiTab == 0 && super.mouseX >= -99 + -35 + this.surface.width2 && super.mouseY >= 3 && super.mouseX < (-99 + this.surface.width2 + -3) && super.mouseY < 35) {
            this.showUiTab = 4;
        }

        if (this.showUiTab == 0 && super.mouseX >= -132 + -35 + this.surface.width2 && super.mouseY >= 3 && -132 + (this.surface.width2 - 3) > super.mouseX && super.mouseY < 35) {
            this.showUiTab = 5;
        }

        if (this.showUiTab == 0 && super.mouseX >= (-35 + this.surface.width2 + -165) && super.mouseY >= 3 && (this.surface.width2 - 3 - 165) > super.mouseX && super.mouseY < 35) {
            this.showUiTab = 6;
        }

        if (this.showUiTab != 0 && (this.surface.width2 + -35) <= super.mouseX && super.mouseY >= 3 && super.mouseX < (-3 + this.surface.width2) && super.mouseY < 26) {
            this.showUiTab = 1;
        }

        if (this.showUiTab != 0 && this.showUiTab != 2 && super.mouseX >= (-35 + (this.surface.width2 - 33)) && super.mouseY >= 3 && super.mouseX < -36 + this.surface.width2 && super.mouseY < 26) {
            this.minimapRandom_2 = (int) (Math.random() * 23.0D) + -11;
            this.showUiTab = 2;
            this.minimapRandom_1 = (int) (Math.random() * 13.0D) - 6;
        }

        if (this.showUiTab != 0 && -66 + -35 + this.surface.width2 <= super.mouseX && super.mouseY >= 3 && -3 + this.surface.width2 + -66 > super.mouseX && super.mouseY < 26) {
            this.showUiTab = 3;
        }

        if (this.showUiTab != 0 && super.mouseX >= (-99 + this.surface.width2 - 35) && super.mouseY >= 3 && (-102 + this.surface.width2) > super.mouseX && super.mouseY < 26) {
            this.showUiTab = 4;
        }

        if (this.showUiTab != 0 && super.mouseX >= -132 + -35 + this.surface.width2 && super.mouseY >= 3 && -3 + this.surface.width2 + -132 > super.mouseX && super.mouseY < 26) {
            this.showUiTab = 5;
        }

        if (this.showUiTab != 0 && (-165 + this.surface.width2 + -35) <= super.mouseX && super.mouseY >= 3 && super.mouseX < -165 + (this.surface.width2 - 3) && super.mouseY < 26) {
            this.showUiTab = 6;
        }

        if (this.showUiTab == 1 && (super.mouseX < (this.surface.width2 + -248) || (36 + 34 * (this.anInt1535 / 5)) < super.mouseY)) {
            this.showUiTab = 0;
        }

        int var2 = -36 % ((-35 - var1) / 58);
        if (this.showUiTab == 3 && ((-199 + this.surface.width2) > super.mouseX || super.mouseY > 316)) {
            this.showUiTab = 0;
        }

        if ((this.showUiTab == 2 || this.showUiTab == 4 || this.showUiTab == 5) && (super.mouseX < (-199 + this.surface.width2) || super.mouseY > 240)) {
            this.showUiTab = 0;
        }

        if (this.showUiTab == 6) {
            if ((this.surface.width2 - 199) > super.mouseX || super.mouseY > 311) {
                this.showUiTab = 0;
            }

        }
    }

    private void drawDialogCombatStyle(int var1) {
        byte var2 = 7;
        byte var3 = 15;
        short var4 = 175;
        int var5;
        if (this.mouseButtonClick != 0) {
            for (var5 = 0; var5 < 5; ++var5) {
                if (var5 > 0 && super.mouseX > var2 && (var2 - -var4) > super.mouseX && super.mouseY > (var3 + 20 * var5) && super.mouseY < 20 + var3 + var5 * 20) {
                    this.mouseButtonClick = 0;
                    this.combatStyle = -1 + var5;
                    this.clientStream.newPacket(29);
                    this.clientStream.writeBuffer.putByte(this.combatStyle);
                    this.clientStream.sendPacket();
                    break;
                }
            }
        }

        for (var5 = 0; var5 < 5; ++var5) {
            if ((this.combatStyle - -1) != var5) {
                this.surface.drawBoxAlpha(var2, 20 * var5 + var3, var4, 20, Surface.rgb2long(190, 190, 190), 128);
            } else {
                this.surface.drawBoxAlpha(var2, var5 * 20 + var3, var4, 20, Surface.rgb2long(0, 0, 255), 128);
            }

            this.surface.drawLineHoriz(var2, var3 + 20 * var5, var4, 0);
            this.surface.drawLineHoriz(var2, 20 + var3 - -(var5 * 20), var4, 0);
        }

        this.surface.drawstringCenter("Select combat style", 3, var3 + 16, var4 / 2 + var2, 0xffffff);
        this.surface.drawstringCenter("Controlled (+1 of each)", 3, 36 + var3, var2 - -(var4 / 2), 0);
        this.surface.drawstringCenter("Aggressive (+3 strength)", 3, 56 + var3, var2 - -(var4 / 2), 0);
        this.surface.drawstringCenter("Accurate   (+3 attack)", 3, 76 + var3, var4 / 2 + var2, 0);
        this.surface.drawstringCenter("Defensive  (+3 defense)", 3, 96 + var3, var2 + var4 / 2, 0);
        if (var1 != -1) {
            this.setActiveUiTab(-114);
        }
        ;
    }

    private Character getPlayer(boolean var1, int serverIndeex) {
        for (int var3 = 0; this.playerCount > var3; ++var3) {
            if (serverIndeex == this.players[var3].serverIndex) {
                return this.players[var3];
            }
        }

        return !var1 ? null : null;
    }

    private void createRightClickMenu(int var1) {
        if (this.messageTabSelected == 1 && this.panelMessageTabs.isClicked(3, this.controlTextListChat) || this.messageTabSelected == 3 && this.panelMessageTabs.isClicked(3, this.controlTextListPrivate)) {
            int control = this.messageTabSelected == 1 ? this.controlTextListChat : this.controlTextListPrivate;
            int mouseThing = this.panelMessageTabs.isMouseButtonDown(control);
            if (mouseThing >> 16 == 2 || this.optionMouseButtonOne && (mouseThing >> 16) == 1) {
                mouseThing = '\uffff' & mouseThing;
                String extra1 = this.panelMessageTabs.getListEntryExtra1(control, (byte) -93, mouseThing);
                String extra2 = this.panelMessageTabs.getListEntryExtra2(-94, control, mouseThing);
                if (this.createRightClickMenusSocial(extra1, 0, extra2)) {
                    return;
                }
            }
        }
        int var2;
        int var3;
        int var4;

        if (this.messageTabSelected == 0) {
            for (var2 = 0; var2 < 100; ++var2) {
                if (messageHistoryTimeout[var2] > 0 && (messageTypes[var2] == 4 || messageTypes[var2] == 1 || messageTypes[var2] == 5 || messageTypes[var2] == 6)) {
                    String var19 = messageColor[var2] + formatMessage(messageMessages[var2], messageSenders[var2], messageTypes[var2]);
                    if (super.mouseX > 7 && super.mouseX < (7 - -this.surface.textWidth(1, -127, var19)) && super.mouseY > (-(var2 * 12) + -30 + this.gameHeight) && this.gameHeight + (-18 - 12 * var2) > super.mouseY && (this.mouseButtonClick == 2 || this.optionMouseButtonOne && this.mouseButtonClick == 1) && this.createRightClickMenusSocial(messageSenderClans[var2], 0, messageSenders[var2])) {
                        return;
                    }
                }
            }
        }

        this.menu_visible = false;
        var2 = -1;

        for (var3 = 0; var3 < this.objectCount; ++var3) {
            this.objectAlreadyInMenu[var3] = false;
        }

        for (var4 = 0; var4 < this.wallObjectCount; ++var4) {
            this.wallObjectAlreadyInMenu[var4] = false;
        }

        int var20 = this.scene.getMousePickedCount(28);
        GameModel[] var21 = this.scene.getMousePickedModels((byte) 96);
        int[] plyrs = this.scene.getMousePickedFaces(var1 + -25855);

        for (int var8 = 0; var20 > var8 && this.menuCommon.getItemCount() <= 200; ++var8) {
            int var9 = plyrs[var8];
            GameModel gameModel = var21[var8];
            if (gameModel.faceTag[var9] <= 65535 || gameModel.faceTag[var9] >= 200000 && gameModel.faceTag[var9] <= 300000) {
                int var11;
                int var12;
                if (this.scene.view == gameModel) {
                    var11 = gameModel.faceTag[var9] % 10000;
                    var12 = gameModel.faceTag[var9] / 10000;
                    if (var12 == 1) {
                        this.createRightClickMenuPlayer(var1 + -7325, var11);
                    } else if (var12 != 2) {
                        if (var12 == 3) {
                            String var13 = "";
                            int var14 = -1;
                            int var15 = this.npcs[var11].npcId;
                            if (GameData.npcAttackable[var15] > 0) {
                                int var16 = (GameData.npcHits[var15] + GameData.npcDefense[var15] + GameData.npcAttack[var15] - -GameData.npcStrength[var15]) / 4;
                                int var17 = (27 + this.playerStatBase[3] + this.playerStatBase[1] + this.playerStatBase[0] + this.playerStatBase[2]) / 4;
                                var13 = "@yel@";
                                var14 = -var16 + var17;
                                if (var14 < 0) {
                                    var13 = "@or1@";
                                }

                                if (var14 < -3) {
                                    var13 = "@or2@";
                                }

                                if (var14 < -6) {
                                    var13 = "@or3@";
                                }

                                if (var14 < -9) {
                                    var13 = "@red@";
                                }

                                if (var14 > 0) {
                                    var13 = "@gr1@";
                                }

                                if (var14 > 3) {
                                    var13 = "@gr2@";
                                }

                                if (var14 > 6) {
                                    var13 = "@gr3@";
                                }

                                if (var14 > 9) {
                                    var13 = "@gre@";
                                }

                                var13 = " " + var13 + "(level-" + var16 + ")";
                            }

                            if (this.selectedSpell >= 0) {
                                if (GameData.spellType[this.selectedSpell] == 2) {
                                    this.menuCommon.addItem(this.selectedSpell, 700, "Cast " + GameData.spellName[this.selectedSpell] + " on", "@yel@" + GameData.npcName[this.npcs[var11].npcId], this.npcs[var11].serverIndex);
                                }
                            } else if (this.selectedItemInventoryIndex >= 0) {
                                this.menuCommon.addItem(this.selectedItemInventoryIndex, 710, "Use " + this.selectedItemName + " with", "@yel@" + GameData.npcName[this.npcs[var11].npcId], this.npcs[var11].serverIndex);
                            } else {
                                if (GameData.npcAttackable[var15] > 0) {
                                    this.menuCommon.addItem(var14 < 0 ? 2715 : 715, "Attack", this.npcs[var11].serverIndex, "@yel@" + GameData.npcName[this.npcs[var11].npcId] + var13);
                                }

                                this.menuCommon.addItem(720, "Talk-to", this.npcs[var11].serverIndex, "@yel@" + GameData.npcName[this.npcs[var11].npcId]);
                                if (!GameData.npcCommand[var15].equals("")) {
                                    this.menuCommon.addItem(725, GameData.npcCommand[var15], this.npcs[var11].serverIndex, "@yel@" + GameData.npcName[this.npcs[var11].npcId]);
                                }

                                this.menuCommon.addItem(3700, "Examine", this.npcs[var11].npcId, "@yel@" + GameData.npcName[this.npcs[var11].npcId]);
                            }
                        }
                    } else if (this.selectedSpell < 0) {
                        if (this.selectedItemInventoryIndex >= 0) {
                            this.menuCommon.addItem(210, this.groundItemX[var11], "@lre@" + GameData.itemName[this.groundItemId[var11]], this.groundItemY[var11], this.groundItemId[var11], this.selectedItemInventoryIndex, "Use " + this.selectedItemName + " with");
                        } else {
                            this.menuCommon.addItem("@lre@" + GameData.itemName[this.groundItemId[var11]], 220, "Take", this.groundItemY[var11], this.groundItemId[var11], this.groundItemX[var11]);
                            this.menuCommon.addItem(3200, "Examine", this.groundItemId[var11], "@lre@" + GameData.itemName[this.groundItemId[var11]]);
                        }
                    } else if (GameData.spellType[this.selectedSpell] == 3) {
                        this.menuCommon.addItem(200, this.groundItemX[var11], "@lre@" + GameData.itemName[this.groundItemId[var11]], this.groundItemY[var11], this.groundItemId[var11], this.selectedSpell, "Cast " + GameData.spellName[this.selectedSpell] + " on");
                    }
                } else if (gameModel != null && gameModel.key >= 10000) {
                    var11 = gameModel.key - 10000;
                    var12 = this.wallObjectId[var11];
                    if (!this.wallObjectAlreadyInMenu[var11]) {
                        if (this.selectedSpell < 0) {
                            if (this.selectedItemInventoryIndex >= 0) {
                                this.menuCommon.addItem(310, this.wallObjectX[var11], "@cya@" + GameData.wallObjectName[var12], this.wallObjectY[var11], this.wallObjectDirection[var11], this.selectedItemInventoryIndex, "Use " + this.selectedItemName + " with");
                            } else {
                                if (!GameData.wallObjectCommand1[var12].equalsIgnoreCase("WalkTo")) {
                                    this.menuCommon.addItem("@cya@" + GameData.wallObjectName[var12], 320, GameData.wallObjectCommand1[var12], this.wallObjectY[var11], this.wallObjectDirection[var11], this.wallObjectX[var11]);
                                }

                                if (!GameData.wallObjectCommand2[var12].equalsIgnoreCase("Examine")) {
                                    this.menuCommon.addItem("@cya@" + GameData.wallObjectName[var12], 2300, GameData.wallObjectCommand2[var12], this.wallObjectY[var11], this.wallObjectDirection[var11], this.wallObjectX[var11]);
                                }

                                this.menuCommon.addItem(3300, "Examine", var12, "@cya@" + GameData.wallObjectName[var12]);
                            }
                        } else if (GameData.spellType[this.selectedSpell] == 4) {
                            this.menuCommon.addItem(300, this.wallObjectX[var11], "@cya@" + GameData.wallObjectName[var12], this.wallObjectY[var11], this.wallObjectDirection[var11], this.selectedSpell, "Cast " + GameData.spellName[this.selectedSpell] + " on");
                        }

                        this.wallObjectAlreadyInMenu[var11] = true;
                    }
                } else if (gameModel != null && gameModel.key >= 0) {
                    var11 = gameModel.key;
                    var12 = this.objectId[var11];
                    if (!this.objectAlreadyInMenu[var11]) {
                        if (this.selectedSpell < 0) {
                            if (this.selectedItemInventoryIndex >= 0) {
                                this.menuCommon.addItem(this.objectDirection[var11], this.objectX[var11], this.objectY[var11], "@cya@" + GameData.objectName[var12], this.objectId[var11], this.selectedItemInventoryIndex, "Use " + this.selectedItemName + " with", 410);
                            } else {
                                if (!GameData.objectCommand1[var12].equalsIgnoreCase("WalkTo")) {
                                    this.menuCommon.addItem(420, this.objectY[var11], "@cya@" + GameData.objectName[var12], this.objectX[var11], this.objectDirection[var11], this.objectId[var11], GameData.objectCommand1[var12]);
                                }

                                if (!GameData.objectCommand2[var12].equalsIgnoreCase("Examine")) {
                                    this.menuCommon.addItem(2400, this.objectY[var11], "@cya@" + GameData.objectName[var12], this.objectX[var11], this.objectDirection[var11], this.objectId[var11], GameData.objectCommand2[var12]);
                                }

                                this.menuCommon.addItem(3400, "Examine", var12, "@cya@" + GameData.objectName[var12]);
                            }
                        } else if (GameData.spellType[this.selectedSpell] == 5) {
                            this.menuCommon.addItem(this.objectDirection[var11], this.objectX[var11], this.objectY[var11], "@cya@" + GameData.objectName[var12], this.objectId[var11], this.selectedSpell, "Cast " + GameData.spellName[this.selectedSpell] + " on", 400);
                        }

                        this.objectAlreadyInMenu[var11] = true;
                    }
                } else {
                    if (var9 >= 0) {
                        var9 = gameModel.faceTag[var9] + -200000;
                    }

                    if (var9 >= 0) {
                        var2 = var9;
                    }
                }
            }
        }

        if (this.selectedSpell >= 0 && GameData.spellType[this.selectedSpell] <= 1) {
            this.menuCommon.addItem(1000, "Cast " + GameData.spellName[this.selectedSpell] + " on self", this.selectedSpell, "");
        }

        if (var1 == -917) {
            if (var2 != -1) {
                this.menu_visible = true;
                this.anInt1383 = this.world.anIntArray244[var2] - -this.regionX;
                this.anInt1488 = this.regionY + this.world.anIntArray266[var2];
                if (this.selectedSpell < 0) {
                    if (this.selectedItemInventoryIndex < 0) {
                        this.menuCommon.addItem(this.world.anIntArray266[var2], 920, "Walk here", "", this.world.anIntArray244[var2]);
                        return;
                    }
                } else {
                    if (GameData.spellType[this.selectedSpell] != 6) {
                        return;
                    }

                    this.menuCommon.addItem("", 900, "Cast " + GameData.spellName[this.selectedSpell] + " on ground", this.world.anIntArray266[var2], this.selectedSpell, this.world.anIntArray244[var2]);
                }

            }
        }
        ;
    }

    private void drawUiTabOptions(boolean var1, boolean var2) {
        int uiX = -199 + this.surface.width2;
        byte uiY = 36;
        this.surface.drawSprite((byte) 61, 3, 6 + this.spriteMedia, uiX + -49);
        short uiWidth = 196;
        this.surface.drawBoxAlpha(uiX, 36, uiWidth, 65, Surface.rgb2long(181, 181, 181), 160);
        this.surface.drawBoxAlpha(uiX, 101, uiWidth, 65, Surface.rgb2long(201, 201, 201), 160);
        this.surface.drawBoxAlpha(uiX, 166, uiWidth, 95, Surface.rgb2long(181, 181, 181), 160);
        this.surface.drawBoxAlpha(uiX, 261, uiWidth, this.inTutorial ? 55 : 40, Surface.rgb2long(201, 201, 201), 160);
        int x = 3 + uiX;
        int y = uiY + 15;
        this.surface.drawstring("Game options - click to toggle", x, y, 1, 0);
        y += 15;
        if (!this.optionCameraModeAuto) {
            this.surface.drawstring("Camera angle mode - @red@Manual", x, y, 1, 0xffffff);
        } else {
            this.surface.drawstring("Camera angle mode - @gre@Auto", x, y, 1, 0xffffff);
        }

        y += 15;
        if (!this.optionMouseButtonOne) {
            this.surface.drawstring("Mouse buttons - @gre@Two", x, y, 1, 0xffffff);
        } else {
            this.surface.drawstring("Mouse buttons - @red@One", x, y, 1, 0xffffff);
        }

        y += 15;
        if (this.members) {
            if (this.optionSoundDisabled) {
                this.surface.drawstring("Sound effects - @red@off", x, y, 1, 0xffffff);
            } else {
                this.surface.drawstring("Sound effects - @gre@on", x, y, 1, 0xffffff);
            }
        }

        y += 15;
        this.surface.drawstring("To change your contact details,", x, y, 0, 0xffffff);
        y += 15;
        this.surface.drawstring("password, recovery questions, etc..", x, y, 0, 0xffffff);
        y += 15;
        this.surface.drawstring("please select \'account management\'", x, y, 0, 0xffffff);
        y += 15;
        if (this.referid != 0) {
            if (this.referid == 1) {
                this.surface.drawstring("from the link below the gamewindow", x, y, 0, 0xffffff);
            } else {
                this.surface.drawstring("from the runescape front webpage", x, y, 0, 0xffffff);
            }
        } else {
            this.surface.drawstring("from the runescape.com front page", x, y, 0, 0xffffff);
        }

        y += 15;
        y += 5;
        this.surface.drawstring("Privacy settings. Will be applied to", 3 + uiX, y, 1, 0);
        y += 15;
        this.surface.drawstring("all people not on your friends list", uiX + 3, y, 1, 0);
        y += 15;
        if (this.settingsBlockChat == 0) {
            this.surface.drawstring("Block chat messages: @red@<off>", 3 + uiX, y, 1, 0xffffff);
        } else {
            this.surface.drawstring("Block chat messages: @gre@<on>", uiX - -3, y, 1, 0xffffff);
        }

        y += 15;
        if (this.settingsBlockPrivate != 0) {
            this.surface.drawstring("Block private messages: @gre@<on>", 3 + uiX, y, 1, 0xffffff);
        } else {
            this.surface.drawstring("Block private messages: @red@<off>", 3 + uiX, y, 1, 0xffffff);
        }

        if (var2) {
            y += 15;
            if (this.settingsBlockTrade == 0) {
                this.surface.drawstring("Block trade requests: @red@<off>", uiX + 3, y, 1, 0xffffff);
            } else {
                this.surface.drawstring("Block trade requests: @gre@<on>", 3 + uiX, y, 1, 0xffffff);
            }

            y += 15;
            if (this.members) {
                if (this.settingsBlockDuel != 0) {
                    this.surface.drawstring("Block duel requests: @gre@<on>", uiX - -3, y, 1, 0xffffff);
                } else {
                    this.surface.drawstring("Block duel requests: @red@<off>", uiX - -3, y, 1, 0xffffff);
                }
            }

            y += 15;
            int color;
            if (this.inTutorial) {
                color = 0xffffff;
                y += 5;
                if (super.mouseX > x && uiWidth + x > super.mouseX && y - 12 < super.mouseY && super.mouseY < 4 + y) {
                    color = 0xffff00;
                }

                this.surface.drawstring("Skip the tutorial", x, y, 1, color);
                y += 15;
            }

            y += 5;
            this.surface.drawstring("Always logout when you finish", x, y, 1, 0);
            y += 15;
            color = 0xffffff;
            if (super.mouseX > x && (uiWidth + x) > super.mouseX && super.mouseY > -12 + y && super.mouseY < (4 + y)) {
                color = 0xffff00;
            }

            this.surface.drawstring("Click here to logout", 3 + uiX, y, 1, color);
            if (var1) {
                int var13 = super.mouseY + -36;
                uiX = super.mouseX - (this.surface.width2 - 199);
                if (uiX >= 0 && var13 >= 0 && uiX < 196 && var13 < 265) {
                    int var9 = this.surface.width2 + -199;
                    byte var10 = 36;
                    x = var9 - -3;
                    uiWidth = 196;
                    y = 30 + var10;
                    if (super.mouseX > x && super.mouseX < (x + uiWidth) && y + -12 < super.mouseY && super.mouseY < (y - -4) && this.mouseButtonClick == 1) {
                        this.optionCameraModeAuto = !this.optionCameraModeAuto;
                        this.clientStream.newPacket(111);
                        this.clientStream.writeBuffer.putByte(0);
                        this.clientStream.writeBuffer.putByte(this.optionCameraModeAuto ? 1 : 0);
                        this.clientStream.sendPacket();
                    }

                    y += 15;
                    if (super.mouseX > x && x + uiWidth > super.mouseX && super.mouseY > (y + -12) && 4 + y > super.mouseY && this.mouseButtonClick == 1) {
                        this.optionMouseButtonOne = !this.optionMouseButtonOne;
                        this.clientStream.newPacket(111);
                        this.clientStream.writeBuffer.putByte(2);
                        this.clientStream.writeBuffer.putByte(!this.optionMouseButtonOne ? 0 : 1);
                        this.clientStream.sendPacket();
                    }

                    y += 15;
                    if (this.members && super.mouseX > x && uiWidth + x > super.mouseX && y + -12 < super.mouseY && y + 4 > super.mouseY && this.mouseButtonClick == 1) {
                        this.optionSoundDisabled = !this.optionSoundDisabled;
                        this.clientStream.newPacket(111);
                        this.clientStream.writeBuffer.putByte(3);
                        this.clientStream.writeBuffer.putByte(this.optionSoundDisabled ? 1 : 0);
                        this.clientStream.sendPacket();
                    }

                    y += 15;
                    y += 15;
                    y += 15;
                    y += 15;
                    y += 15;
                    y += 35;
                    boolean var11 = false;
                    if (x < super.mouseX && (x - -uiWidth) > super.mouseX && super.mouseY > (y + -12) && super.mouseY < 4 + y && this.mouseButtonClick == 1) {
                        this.settingsBlockChat = -this.settingsBlockChat + 1;
                        var11 = true;
                    }

                    y += 15;
                    if (x < super.mouseX && uiWidth + x > super.mouseX && super.mouseY > y - 12 && super.mouseY < (4 + y) && this.mouseButtonClick == 1) {
                        var11 = true;
                        this.settingsBlockPrivate = 1 - this.settingsBlockPrivate;
                    }

                    y += 15;
                    if (super.mouseX > x && (x - -uiWidth) > super.mouseX && super.mouseY > y - 12 && super.mouseY < y + 4 && this.mouseButtonClick == 1) {
                        this.settingsBlockTrade = 1 + -this.settingsBlockTrade;
                        var11 = true;
                    }

                    y += 15;
                    if (this.members && super.mouseX > x && (uiWidth + x) > super.mouseX && (-12 + y) < super.mouseY && super.mouseY < y - -4 && this.mouseButtonClick == 1) {
                        var11 = true;
                        this.settingsBlockDuel = 1 + -this.settingsBlockDuel;
                    }

                    y += 15;
                    if (var11) {
                        this.sendPrivacySettings(-18896, this.settingsBlockPrivate, this.settingsBlockChat, this.settingsBlockTrade, this.settingsBlockDuel);
                    }

                    if (this.inTutorial) {
                        y += 5;
                        if (x < super.mouseX && super.mouseX < uiWidth + x && y - 12 < super.mouseY && (4 + y) > super.mouseY && this.mouseButtonClick == 1) {
                            this.showInputPopup(9, aStringArray1659, false);//todo creates a confirmation window
                            this.showUiTab = 0;
                        }

                        y += 15;
                    }

                    y += 20;
                    if (x < super.mouseX && (x + uiWidth) > super.mouseX && super.mouseY > -12 + y && 4 + y > super.mouseY && this.mouseButtonClick == 1) {
                        this.sendLogout(2);
                    }

                    this.mouseButtonClick = 0;
                }
            }
        }
        ;
    }

    private Character createPlayer(int var1, int serverIndex, int var3, int var4, int var5) {
        if (this.playerServer[serverIndex] == null) {
            this.playerServer[serverIndex] = new Character();
            this.playerServer[serverIndex].serverIndex = serverIndex;
        }

        Character c = this.playerServer[serverIndex];
        boolean var7 = false;
        if (var1 != 1) {
            this.tradeConfirmItems = null;
        }

        for (int var8 = 0; var8 < this.knownPlayerCount; ++var8) {
            if (this.knownPlayers[var8].serverIndex == serverIndex) {
                var7 = true;
                break;
            }
        }

        if (!var7) {
            c.waypointsX[0] = c.currentX = var4;
            c.waypointCurrent = 0;
            c.serverIndex = serverIndex;
            c.movingStep = 0;
            c.stepCount = 0;
            c.waypointsY[0] = c.currentY = var3;
            c.animationNext = c.animationCurrent = var5;
        } else {
            c.animationNext = var5;
            int var9 = c.waypointCurrent;
            if (var4 != c.waypointsX[var9] || c.waypointsY[var9] != var3) {
                c.waypointCurrent = var9 = (var9 + 1) % 10;
                c.waypointsX[var9] = var4;
                c.waypointsY[var9] = var3;
            }
        }

        this.players[this.playerCount++] = c;
        return c;
    }

    private void drawUi() {
        boolean doShitGetHit = false;
        if(logoutTimeout != 0) {
            this.drawDialogLogout(-111);
        } else if (this.showDialogMessage) {
            this.drawDialogWelcome((byte) 93);
        } else if (this.showDialogServermessage) {
            this.drawDialogServermessage((byte) 122);
        } else if (this.showUiWildWarn == 1) {
            this.drawDialogWildWarn(-2109);
        } else if (this.showDialogBank && this.combatTimeout == 0) {
            this.drawDialogBank(-19428);
        } else if (this.showDialogShop && this.combatTimeout == 0) {
            this.drawDialogShop(27103);
        } else if (this.showDialogTradeConfirm) {
            this.drawDialogTradeConfirm(-28437);
        } else if (this.showDialogTrade) {
            this.drawDialogTrade(-2);
        } else if (this.showDialogDuelConfirm) {
            this.drawDialogDuelConfirm(true);
        } else if (this.showDialogDuel) {
            this.drawDialogDuel(76);
        } else if (this.showDialogReportAbuseStep == 1) {
            this.drawDialogReportAbuseInput(256);
        } else if (this.showDialogReportAbuseStep == 2) {
            this.drawDialogReportAbuse((byte) 58);
        } else if (this.showDialogSocialInput != 0) {
            this.drawDialogSocialInput(20);
        } else {
            doShitGetHit = true;
        }

        if (this.inputPopupType != 0) {
            this.drawInputPopup();
        }

        if (doShitGetHit) {
            if (this.showOptionMenu) {
                this.drawOptionsMenu(11);
            }

            if (this.localPlayer.animationCurrent == 8 || this.localPlayer.animationCurrent == 9) {
                this.drawDialogCombatStyle(-1);
            }

            this.setActiveUiTab(108);
            boolean nomenus = !this.showOptionMenu && !this.showRightClickMenu;
            if (nomenus) {
                this.menuCommon.recalculateSize();
            }

            if (this.showUiTab == 0 && nomenus) {
                this.createRightClickMenu(-917);
            }

            if (this.showUiTab == 1) {
                this.method85(nomenus, -6135);
            }

            if (this.showUiTab == 2) {
                this.drawUiTabMinimap(nomenus, -1033933134);
            }

            if (this.showUiTab == 3) {
                this.drawUiTabPlayerInfo(true, nomenus);
            }

            if (this.showUiTab == 4) {
                this.drawUiTabMagic(40, nomenus);
            }

            if (this.showUiTab == 5) {
                this.drawUiTabSocial(-3998, nomenus);
            }

            if (this.showUiTab == 6) {
                this.drawUiTabOptions(nomenus, true);
            }

            if (!this.showRightClickMenu && !this.showOptionMenu) {
                this.createTopMouseMenu((byte) 45);
            }

            if (this.showRightClickMenu && !this.showOptionMenu) {
                this.drawRightClickMenu(-39);
            }
        }

        this.mouseButtonClick = 0;
    }

    private void method131(int var1, int var2, int var3, int var4) {
        if (var3 != 56) {
            this.duelOptionWeapons = -77;
        }

        if (var4 == 0) {
            this.walkToSource(var1, this.localRegionY, var2 - 1, var3 ^ 71, var1, this.localRegionX, false, true, var2);
        } else if (var4 == 1) {
            this.walkToSource(var1 + -1, this.localRegionY, var2, 122, var1, this.localRegionX, false, true, var2);
        } else {
            this.walkToSource(var1, this.localRegionY, var2, 93, var1, this.localRegionX, true, true, var2);
        }
    }

    private void createAppearancePanel(byte var1) {
        this.panelAppearance = new Panel(this.surface, 100);
        this.panelAppearance.addText(true, false, 4, 10, 256, "Please design Your Character");
        short var2 = 140;
        int var6 = var2 + 116;
        byte var3 = 34;
        int var7 = var3 - 10;
        this.panelAppearance.addText(true, false, 3, var7 - -110, -55 + var6, "Front");
        if (var1 != -92) {
            this.bankItemCount = 10;
        }

        this.panelAppearance.addText(true, false, 3, var7 + 110, var6, "Side");
        this.panelAppearance.addText(true, false, 3, var7 - -110, 55 + var6, "Back");
        byte var4 = 54;
        var7 += 145;
        this.panelAppearance.method508(var7, -var4 + var6, (byte) -2, 41, 53);
        this.panelAppearance.addText(true, false, 1, var7 + -8, -var4 + var6, "Head");
        this.panelAppearance.addText(true, false, 1, 8 + var7, -var4 + var6, "Type");
        this.panelAppearance.method528(7 + baseSpriteStart, false, -40 + -var4 + var6, var7);
        this.controlButtonAppearanceHead1 = this.panelAppearance.addButton(20, 20, var7, -var4 + var6 + -40, var1 ^ -38);
        this.panelAppearance.method528(6 + baseSpriteStart, false, 40 + -var4 + var6, var7);
        this.anInt1380 = this.panelAppearance.addButton(20, 20, var7, 40 + -var4 + var6, 126);
        this.panelAppearance.method508(var7, var4 + var6, (byte) -2, 41, 53);
        this.panelAppearance.addText(true, false, 1, var7 + -8, var4 + var6, "Hair");
        this.panelAppearance.addText(true, false, 1, 8 + var7, var6 + var4, "Color");
        this.panelAppearance.method528(7 + baseSpriteStart, false, -40 + var4 + var6, var7);
        this.anInt1493 = this.panelAppearance.addButton(20, 20, var7, -40 + var6 + var4, var1 + 218);
        this.panelAppearance.method528(6 + baseSpriteStart, false, var6 + var4 + 40, var7);
        this.anInt1577 = this.panelAppearance.addButton(20, 20, var7, 40 + var6 - -var4, var1 ^ -37);
        var7 += 50;
        this.panelAppearance.method508(var7, -var4 + var6, (byte) -2, 41, 53);
        this.panelAppearance.addText(true, false, 1, var7, -var4 + var6, "Gender");
        this.panelAppearance.method528(7 + baseSpriteStart, false, -40 + -var4 + var6, var7);
        this.anInt1571 = this.panelAppearance.addButton(20, 20, var7, var6 + (-var4 - 40), 123);
        this.panelAppearance.method528(6 + baseSpriteStart, false, 40 + (var6 - var4), var7);
        this.anInt1623 = this.panelAppearance.addButton(20, 20, var7, 40 + -var4 + var6, var1 ^ -37);
        this.panelAppearance.method508(var7, var4 + var6, (byte) -2, 41, 53);
        this.panelAppearance.addText(true, false, 1, -8 + var7, var4 + var6, "Top");
        this.panelAppearance.addText(true, false, 1, 8 + var7, var6 + var4, "Color");
        this.panelAppearance.method528(baseSpriteStart - -7, false, var4 + (var6 - 40), var7);
        this.anInt1403 = this.panelAppearance.addButton(20, 20, var7, -40 + var4 + var6, 123);
        this.panelAppearance.method528(baseSpriteStart - -6, false, var4 + var6 + 40, var7);
        this.anInt1390 = this.panelAppearance.addButton(20, 20, var7, 40 + var6 + var4, 127);
        var7 += 50;
        this.panelAppearance.method508(var7, -var4 + var6, (byte) -2, 41, 53);
        this.panelAppearance.addText(true, false, 1, var7 + -8, -var4 + var6, "Skin");
        this.panelAppearance.addText(true, false, 1, var7 + 8, -var4 + var6, "Color");
        this.panelAppearance.method528(baseSpriteStart - -7, false, -var4 + (var6 - 40), var7);
        this.anInt1603 = this.panelAppearance.addButton(20, 20, var7, -40 + -var4 + var6, 126);
        this.panelAppearance.method528(baseSpriteStart + 6, false, -var4 + var6 + 40, var7);
        this.anInt1485 = this.panelAppearance.addButton(20, 20, var7, var6 - var4 + 40, 123);
        this.panelAppearance.method508(var7, var4 + var6, (byte) -2, 41, 53);
        this.panelAppearance.addText(true, false, 1, var7 - 8, var4 + var6, "Bottom");
        this.panelAppearance.addText(true, false, 1, 8 + var7, var6 - -var4, "Color");
        this.panelAppearance.method528(7 + baseSpriteStart, false, -40 + var6 + var4, var7);
        this.anInt1381 = this.panelAppearance.addButton(20, 20, var7, -40 + var6 + var4, 125);
        this.panelAppearance.method528(6 + baseSpriteStart, false, 40 + var6 + var4, var7);
        this.anInt1424 = this.panelAppearance.addButton(20, 20, var7, 40 + var4 + var6, var1 ^ -39);
        var7 += 82;
        var7 -= 35;
        this.panelAppearance.addButtonBackground(var6, 200, var7, 30, (byte) -8);
        this.panelAppearance.addText(false, false, 4, var7, var6, "Accept");
        this.anInt1370 = this.panelAppearance.addButton(200, 30, var7, var6, var1 + 215);
    }

    final void startGame(byte var1) {
        if (this.appletMode) {
            String var2 = this.getDocumentBase().getHost().toLowerCase();
            if (!var2.equals("runescape.com") && !var2.endsWith(".runescape.com")) {
                this.errorLoadingCodebase = true;
                return;
            }
        }

        this.method42(121);
        if (!this.drawLoading(0)) {
            this.errorLoadingData = true;
        } else {
            method339((byte) -96, aClass11_315);

            try {
                if (cachePackageManager.cacheRandom != null) {
                    Isaac.cacheRandomDat = new CacheFileManager(cachePackageManager.cacheRandom, 24, 0);
                    cachePackageManager.cacheRandom = null;
                }
            } catch (IOException var9) {
                Isaac.cacheRandomDat = null;
            }

            int var11 = 0;
            int var3 = 0;

            int var5;
            int servertype;
            for (int var4 = 62 / ((-19 - var1) / 43); var3 < 99; ++var3) {
                var5 = 1 + var3;
                servertype = (int) (300.0D * Math.pow(2.0D, (double) var5 / 7.0D) + (double) var5);
                var11 += servertype;
                this.experienceArray[var3] = Utility.bitwiseAnd(var11, 268435452);
            }

            String param;
            try {
                param = this.getParameter("referid");
                this.referid = Integer.parseInt(param);
            } catch (Exception var8) {
                ;
            }

            try {
                param = this.getParameter("servertype");
                servertype = Integer.parseInt(param);
                if ((1 & servertype) != 0) {
                    this.members = true;
                }

                if ((2 & servertype) != 0) {
                    this.veterans = true;
                }
            } catch (Exception var7) {
                ;
            }

            if (modewhere == GameModeWhere.LIVE) {
                this.serverAddress = this.getCodeBase().getHost();
                this.serverJagGrabPort = 443;
                this.serverPort = '\uaa4a';// 43 594
            }
            if (GameFrame.method145(modewhere, true)) {
                this.serverAddress = this.getCodeBase().getHost();
                this.serverPort = nodeid + '\u9c40';// 40 000 + nodeid
                this.serverJagGrabPort = nodeid + '\uc350';// 50 000  + nodeid
            } else if (GameModeWhere.LOCAL == modewhere) {
                this.serverPort = 43594; //'\u9c40' - -nodeid;// 40 000 + nodeid
                this.serverAddress = "classic2.runescape.com";
                //this.serverAddress = "127.0.0.1";
                this.serverJagGrabPort = nodeid + '\uc350';// 50 000
            }

            maxReadTries = 1000;
            this.loadGameConfig(true);
            if (!this.errorLoadingData) {
                this.spriteMedia = 2000;
                this.spriteUtil = 100 + this.spriteMedia;
                this.spriteItem = this.spriteUtil + 50;
                this.spriteLogo = this.spriteItem + 1000;
                this.spriteProjectile = 10 + this.spriteLogo;
                this.spriteTexture = this.spriteProjectile - -50;
                this.spriteCrowns = this.spriteTexture + 10;
                this.spriteTextureWorld = 5 + this.spriteCrowns;
                this.graphics = this.getGraphics();
                this.setFPS((byte) 126, 50);
                this.surface = new SurfaceSprite(this.gameWidth, 12 + this.gameHeight, 4000, this);
                this.surface.mudclientref = this;
                this.surface.setBounds(0, 0, this.gameWidth, 12 + this.gameHeight);
                this.menuCommon = new Menu(this.surface, 1, "Choose option");
                this.menuTrade = new Menu(this.surface, 1);
                this.menuDuel = new Menu(this.surface, 1);
                baseSpriteStart = this.spriteUtil;
                Panel.drawBackgroundArrow = false;
                this.panelMagic = new Panel(this.surface, 5);
                var5 = -199 + this.surface.width2;
                byte var13 = 36;
                this.controlMagicPanel = this.panelMagic.addTextListInteractive(var5, var13 - -24, 196, 90, 1, 500, true, false);
                this.panelSocial = new Panel(this.surface, 5);
                this.controlListSocialPlayers = this.panelSocial.addTextListInteractive(var5, 40 + var13, 196, 126, 1, 500, true, false);
                this.panelPlayerinfo = new Panel(this.surface, 5);
                this.controlPlayerinfopanel = this.panelPlayerinfo.addTextListInteractive(var5, 24 + var13, 196, 251, 1, 500, true, false);
                this.loadMedia(-23);
                if (!this.errorLoadingData) {
                    this.loadEntities(-25763);
                    if (!this.errorLoadingData) {
                        this.scene = new Scene(this.surface, 15000, 15000, 1000);
                        this.scene.setMidpoints(this.gameWidth / 2, this.gameHeight / 2, this.gameWidth, this.gameWidth / 2, this.anInt1285, this.gameHeight / 2, 0);
                        this.scene.anInt367 = 2400;
                        this.scene.anInt317 = 2400;
                        this.scene.fogZFalloff = 1;
                        this.scene.fogZDistance = 2300;
                        this.scene.method321(-50, -50, -10, 1);
                        this.world = new World(this.scene, this.surface);
                        this.world.baseMediaSprite = this.spriteMedia;
                        this.loadTextures(11);
                        if (!this.errorLoadingData) {
                            this.loadModels(-3726);
                            if (!this.errorLoadingData) {
                                this.loadMaps((byte) -60);
                                if (!this.errorLoadingData) {
                                    if (this.members) {
                                        this.loadSounds(10);
                                    }

                                    if (!this.errorLoadingData) {
                                        this.showLoadingProgress(100, "Starting game...");
                                        this.createMessageTabPanel(5);
                                        this.createLoginPanels(40);
                                        this.createAppearancePanel((byte) -92);
                                        this.resetLoginScreenVariables(0);
                                        this.method32(111);
                                        this.renderLoginScreenViewports(true);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void drawDialogServermessage(byte var1) {
        short width = 400;
        short height = 100;
        if (this.serverMessageBoxTop) {
            boolean var8 = true;
            height = 300;
        }

        this.surface.drawBox(-(width / 2) + 256, -(height / 2) + 167, width, height, 0);
        this.surface.drawBoxEdge(256 + -(width / 2), -(height / 2) + 167, width, height, 0xffffff);
        int var4 = 22 / ((var1 - 62) / 59);
        this.surface.centrepara(256, false, true, 1, 0xffffff, this.serverMessage, width + -40, 20 + -(height / 2) + 167);
        int var5 = 157 + height / 2;
        int var6 = 0xffffff;
        if (super.mouseY > (var5 + -12) && super.mouseY <= var5 && super.mouseX > 106 && super.mouseX < 406) {
            var6 = 0xff0000;
        }

        this.surface.drawstringCenter("Click here to close window", 1, var5, 256, var6);
        if (this.mouseButtonClick == 1) {
            if (var6 == 0xff0000) {
                this.showDialogServermessage = false;
            }

            if (((256 + -(width / 2)) > super.mouseX || 256 + width / 2 < super.mouseX) && (super.mouseY < 167 + -(height / 2) || height / 2 + 167 < super.mouseY)) {
                this.showDialogServermessage = false;
            }
        }

        this.mouseButtonClick = 0;
    }

    private void method134(boolean var1) {
        if (var1) {
            this.cantLogout((byte) 68);
        }
    }

    private void createTopMouseMenu(byte var1) {
        if (this.selectedSpell >= 0 || this.selectedItemInventoryIndex >= 0) {
            this.menuCommon.addItem("Cancel", "", 4000);
        }

        this.menuCommon.method453();
        int var2 = this.menuCommon.getItemCount();

        for (int var3 = var2; var3 > 20; --var3) {
            this.menuCommon.removeItem(var3 - 1);
        }

        int var5;
        int var6;
        if (this.showUiTab == 5) {
            String var4 = null;
            if (this.anInt1367 == 0 && this.anInt1626 != -1) {
                if (this.anInt1626 < 0) {
                    var5 = -(2 + this.anInt1626);
                    var4 = "Click to remove " + friendListNames[var5];
                    if (friendListOldNames[var5] != null && friendListOldNames[var5].length() > 0) {
                        var4 = var4 + " (formerly " + friendListOldNames[var5] + ")";
                    }
                } else {
                    String var12 = "";
                    var6 = this.anInt1626;
                    if ((4 & friendListOnline[var6]) != 0) {
                        if (friendListServer[var6] != null) {
                            var12 = " on " + friendListServer[var6];
                        }

                        var4 = "Click to message " + friendListNames[var6];
                    } else {
                        var12 = " is offline";
                        var4 = friendListNames[var6];
                    }

                    if (friendListOldNames[var6] != null && friendListOldNames[var6].length() > 0) {
                        var4 = var4 + " (formerly " + friendListOldNames[var6] + ")" + var12;
                    } else {
                        var4 = var4 + var12;
                    }
                }
            }

            if (this.anInt1367 == 1 && this.anInt1435 != -1) {
                if (this.anInt1435 >= 0) {
                    var5 = this.anInt1435;
                    var4 = "Ignoring " + ignoreListNames[var5];
                    if (ignoreListOldNames[var5] != null && ignoreListOldNames[var5].length() > 0) {
                        var4 = var4 + " (formerly " + ignoreListOldNames[var5] + ")";
                    }
                } else {
                    var5 = -(2 + this.anInt1435);
                    var4 = "Click to remove " + ignoreListNames[var5];
                    if (ignoreListOldNames[var5] != null && ignoreListOldNames[var5].length() > 0) {
                        var4 = var4 + " (formerly " + ignoreListOldNames[var5] + ")";
                    }
                }
            }

            if (var4 != null) {
                this.surface.drawstring(var4, 6, 14, 1, 0xffff00);
            }
        }

        int var13 = -95 % ((var1 - -30) / 40);
        var5 = this.menuCommon.getItemCount();
        if (var5 > 0) {
            var6 = -1;

            String var8;
            for (int var7 = 0; var7 < var5; ++var7) {
                var8 = this.menuCommon.getItemText2(var7);
                if (var8 != null && var8.length() > 0) {
                    var6 = var7;
                    break;
                }
            }

            var8 = null;
            if ((this.selectedItemInventoryIndex >= 0 || this.selectedSpell >= 0) && var5 == 1) {
                var8 = "Choose a target";
            } else if ((this.selectedItemInventoryIndex >= 0 || this.selectedSpell >= 0) && var5 > 1) {
                var8 = "@whi@" + this.menuCommon.getItemText1(0) + " " + this.menuCommon.getItemText2(0);
            } else if (var6 != -1) {
                var8 = this.menuCommon.getItemText2(var6) + ": @whi@" + this.menuCommon.getItemText1(0);
            }

            if (var5 == 2 && var8 != null) {
                var8 = var8 + "@whi@ / 1 more option";
            }

            if (var5 > 2 && var8 != null) {
                var8 = var8 + "@whi@ / " + (var5 - 1) + " more options";
            }

            if (var8 != null) {
                this.surface.drawstring(var8, 6, 14, 1, 0xffff00);
            }

            if (!this.optionMouseButtonOne && this.mouseButtonClick == 1 || this.optionMouseButtonOne && this.mouseButtonClick == 1 && var5 == 1) {
                if (super.ctrl_down && super.shift_down && this.menu_visible) {
                    System.out.println("FUCKING FIRED CUNT");
                    this.clientStream.newPacket(59);
                    this.clientStream.writeBuffer.putShort(this.anInt1383);
                    this.clientStream.writeBuffer.putShort(this.anInt1488);
                    this.clientStream.sendPacket();
                } else {
                    this.menuItemClickl(0, (byte) -14);
                }

                this.mouseButtonClick = 0;
                return;
            }

            if (!this.optionMouseButtonOne && this.mouseButtonClick == 2 || this.optionMouseButtonOne && this.mouseButtonClick == 1) {
                int var9 = this.menuCommon.getWidth();
                int var10 = this.menuCommon.getHeight();
                this.anInt1379 = super.mouseX - var9 / 2;
                this.anInt1625 = -7 + super.mouseY;
                this.showRightClickMenu = true;
                if (this.anInt1625 < 0) {
                    this.anInt1625 = 0;
                }

                if (this.anInt1379 < 0) {
                    this.anInt1379 = 0;
                }

                if (this.anInt1625 + var10 > 315) {
                    this.anInt1625 = -var10 + 315;
                }

                this.mouseButtonClick = 0;
                if ((this.anInt1379 - -var9) > 510) {
                    this.anInt1379 = 510 - var9;
                    return;
                }
            }
        }
    }

    private void drawChatMessageTabs(boolean var1) {
        this.surface.drawSprite((byte) 61, -4 + this.gameHeight, this.spriteMedia + 23, 0);
        int var2 = Surface.rgb2long(255, 200, 200);
        if (this.messageTabSelected == 0) {
            var2 = Surface.rgb2long(50, 200, 255);
        }

        if ((this.messageTabFlashAll % 30) > 15) {
            var2 = Surface.rgb2long(50, 50, 255);
        }

        this.surface.drawstringCenter("All messages", 0, this.gameHeight - -6, 54, var2);
        var2 = Surface.rgb2long(255, 200, 200);
        if (this.messageTabSelected == 1) {
            var2 = Surface.rgb2long(50, 200, 255);
        }

        if (var1) {
            this.createNpc(-81, 99, 115, (byte) 34, 111, -109);
        }

        if ((this.messageTabFlashHistory % 30) > 15) {
            var2 = Surface.rgb2long(50, 50, 255);
        }

        this.surface.drawstringCenter("Chat history", 0, this.gameHeight - -6, 155, var2);
        var2 = Surface.rgb2long(255, 200, 200);
        if (this.messageTabSelected == 2) {
            var2 = Surface.rgb2long(50, 200, 255);
        }

        if ((this.messageTabFlashQuest % 30) > 15) {
            var2 = Surface.rgb2long(50, 50, 255);
        }

        this.surface.drawstringCenter("Quest history", 0, 6 + this.gameHeight, 255, var2);
        var2 = Surface.rgb2long(255, 200, 200);
        if (this.messageTabSelected == 3) {
            var2 = Surface.rgb2long(50, 200, 255);
        }

        if (this.messageTabFlashPrivate % 30 > 15) {
            var2 = Surface.rgb2long(50, 50, 255);
        }

        this.surface.drawstringCenter("Private history", 0, 6 + this.gameHeight, 355, var2);
        this.surface.drawstringCenter("Report abuse", 0, this.gameHeight - -6, 457, 0xffffff);
    }

    private void loadModels(int var1) {
        GameData.getModelIndex((byte) 107, "torcha2");
        GameData.getModelIndex((byte) 120, "torcha3");
        GameData.getModelIndex((byte) 52, "torcha4");
        GameData.getModelIndex((byte) 47, "skulltorcha2");
        GameData.getModelIndex((byte) 68, "skulltorcha3");
        GameData.getModelIndex((byte) 84, "skulltorcha4");
        GameData.getModelIndex((byte) 90, "firea2");
        GameData.getModelIndex((byte) 46, "firea3");
        GameData.getModelIndex((byte) 74, "fireplacea2");
        GameData.getModelIndex((byte) 101, "fireplacea3");
        GameData.getModelIndex((byte) 118, "firespell2");
        GameData.getModelIndex((byte) 108, "firespell3");
        GameData.getModelIndex((byte) 108, "lightning2");
        GameData.getModelIndex((byte) 55, "lightning3");
        GameData.getModelIndex((byte) 71, "clawspell2");
        GameData.getModelIndex((byte) 97, "clawspell3");
        GameData.getModelIndex((byte) 41, "clawspell4");
        GameData.getModelIndex((byte) 78, "clawspell5");
        GameData.getModelIndex((byte) 46, "spellcharge2");
        if (var1 == -3726) {
            GameData.getModelIndex((byte) 58, "spellcharge3");
            //if (gameFrameReference == null) {
            if (true) {
                byte[] var6 = this.readDataFile(9, 60, "3d models", -10);
                if (var6 == null) {
                    this.errorLoadingData = true;
                } else {
                    for (int var3 = 0; var3 < GameData.modelCount; ++var3) {
                        int var4 = Utility.getDataFileOffset(GameData.modelName[var3] + ".ob3", var6);
                        if (var4 == 0) {
                            this.gameModels[var3] = new GameModel(1, 1);
                        } else {
                            this.gameModels[var3] = new GameModel(var6, var4, true);
                        }

                        if (GameData.modelName[var3].equals("giantcrystal")) {
                            this.gameModels[var3].transparent = true;
                        }
                    }

                }
            } else {
                this.showLoadingProgress(70, "Loading 3d models");

                for (int var2 = 0; var2 < GameData.modelCount; ++var2) {
                    this.gameModels[var2] = new GameModel("../content/src/models/" + GameData.modelName[var2] + ".ob2");
                    if (GameData.modelName[var2].equals("giantcrystal")) {
                        this.gameModels[var2].transparent = true;
                    }
                }

            }
        }
    }

    private boolean method138(int var1, int var2) {
        if (var2 <= 28) {
            this.method53((byte) -99, -36);
        }

        int var3 = this.localPlayer.currentX / 128;
        int var4 = this.localPlayer.currentY / 128;

        for (int var5 = 2; var5 >= 1; --var5) {
            if (var1 == 1 && ((this.world.anIntArrayArray276[var3][var4 + -var5] & 128) == 128 || (this.world.anIntArrayArray276[-var5 + var3][var4] & 128) == 128 || (this.world.anIntArrayArray276[var3 + -var5][var4 + -var5] & 128) == 128)) {
                return false;
            }

            if (var1 == 3 && ((128 & this.world.anIntArrayArray276[var3][var5 + var4]) == 128 || (this.world.anIntArrayArray276[-var5 + var3][var4] & 128) == 128 || (128 & this.world.anIntArrayArray276[var3 - var5][var5 + var4]) == 128)) {
                return false;
            }

            if (var1 == 5 && ((this.world.anIntArrayArray276[var3][var4 + var5] & 128) == 128 || (this.world.anIntArrayArray276[var3 + var5][var4] & 128) == 128 || (128 & this.world.anIntArrayArray276[var3 + var5][var5 + var4]) == 128)) {
                return false;
            }

            if (var1 == 7 && ((this.world.anIntArrayArray276[var3][var4 + -var5] & 128) == 128 || (128 & this.world.anIntArrayArray276[var3 + var5][var4]) == 128 || (this.world.anIntArrayArray276[var3 - -var5][-var5 + var4] & 128) == 128)) {
                return false;
            }

            if (var1 == 0 && (this.world.anIntArrayArray276[var3][var4 - var5] & 128) == 128) {
                return false;
            }

            if (var1 == 2 && (this.world.anIntArrayArray276[-var5 + var3][var4] & 128) == 128) {
                return false;
            }

            if (var1 == 4 && (128 & this.world.anIntArrayArray276[var3][var4 - -var5]) == 128) {
                return false;
            }

            if (var1 == 6 && (this.world.anIntArrayArray276[var5 + var3][var4] & 128) == 128) {
                return false;
            }
        }

        return true;
    }

    private void handleAppearancePanelControls(int var1) {
        this.panelAppearance.handleMouse(var1 + -113, super.lastMouseButtonDown, super.mouseX, super.anInt84, super.mouseY);
        if (this.panelAppearance.isClicked(3, this.controlButtonAppearanceHead1)) {
            do {
                do {
                    this.appearanceHeadType = (GameData.animationCount + this.appearanceHeadType - 1) % GameData.animationCount;
                } while ((3 & GameData.animationSomething[this.appearanceHeadType]) != 1);
            } while ((this.appearanceHeadGender * 4 & GameData.animationSomething[this.appearanceHeadType]) == 0);
        }

        if (this.panelAppearance.isClicked(var1, this.anInt1380)) {
            do {
                do {
                    this.appearanceHeadType = (1 + this.appearanceHeadType) % GameData.animationCount;
                } while ((GameData.animationSomething[this.appearanceHeadType] & 3) != 1);
            } while ((this.appearanceHeadGender * 4 & GameData.animationSomething[this.appearanceHeadType]) == 0);
        }

        if (this.panelAppearance.isClicked(3, this.anInt1493)) {
            this.anInt1407 = (-1 + this.anInt1407 + this.anIntArray1465.length) % this.anIntArray1465.length;
        }

        if (this.panelAppearance.isClicked(3, this.anInt1577)) {
            this.anInt1407 = (this.anInt1407 + 1) % this.anIntArray1465.length;
        }

        if (this.panelAppearance.isClicked(var1, this.anInt1571) || this.panelAppearance.isClicked(3, this.anInt1623)) {
            for (this.appearanceHeadGender = 3 + -this.appearanceHeadGender; (3 & GameData.animationSomething[this.appearanceHeadType]) != 1 || (this.appearanceHeadGender * 4 & GameData.animationSomething[this.appearanceHeadType]) == 0; this.appearanceHeadType = (this.appearanceHeadType + 1) % GameData.animationCount) {
                ;
            }

            while ((GameData.animationSomething[this.anInt1447] & 3) != 2 || (GameData.animationSomething[this.anInt1447] & this.appearanceHeadGender * 4) == 0) {
                this.anInt1447 = (this.anInt1447 + 1) % GameData.animationCount;
            }
        }

        if (this.panelAppearance.isClicked(3, this.anInt1403)) {
            this.anInt1555 = (this.anIntArray1592.length + this.anInt1555 + -1) % this.anIntArray1592.length;
        }

        if (this.panelAppearance.isClicked(var1, this.anInt1390)) {
            this.anInt1555 = (this.anInt1555 - -1) % this.anIntArray1592.length;
        }

        if (this.panelAppearance.isClicked(3, this.anInt1603)) {
            this.anInt1423 = (this.anIntArray1548.length + this.anInt1423 + -1) % this.anIntArray1548.length;
        }

        if (this.panelAppearance.isClicked(3, this.anInt1485)) {
            this.anInt1423 = (1 + this.anInt1423) % this.anIntArray1548.length;
        }

        if (this.panelAppearance.isClicked(3, this.anInt1381)) {
            this.anInt1573 = (this.anInt1573 + -1 + this.anIntArray1592.length) % this.anIntArray1592.length;
        }

        if (this.panelAppearance.isClicked(3, this.anInt1424)) {
            this.anInt1573 = (this.anInt1573 - -1) % this.anIntArray1592.length;
        }

        if (this.panelAppearance.isClicked(3, this.anInt1370)) {
            this.clientStream.newPacket(235);
            this.clientStream.writeBuffer.putByte(this.appearanceHeadGender);
            this.clientStream.writeBuffer.putByte(this.appearanceHeadType);
            this.clientStream.writeBuffer.putByte(this.anInt1447);
            this.clientStream.writeBuffer.putByte(this.anInt1611);
            this.clientStream.writeBuffer.putByte(this.anInt1407);
            this.clientStream.writeBuffer.putByte(this.anInt1555);
            this.clientStream.writeBuffer.putByte(this.anInt1573);
            this.clientStream.writeBuffer.putByte(this.anInt1423);
            this.clientStream.sendPacket();
            this.surface.blackScreen((byte) 127);
            this.showAppearanceChange = false;
        }
    }

    final void method17(byte var1) {
        if (var1 >= 18) {
            this.closeConnection(true, -10);
            if (this.soundPlayer != null) {
                this.soundPlayer.method493((byte) 125);
            }
        }
    }

    private void playSoundFile(String var2) {
        if (this.aClass8_Sub2_Sub1_1378 != null) {
            if (!this.optionSoundDisabled) {
                int offset = Utility.getDataFileOffset(var2 + ".pcm", this.soundData);
                int length = Utility.getDataFileLength(var2 + ".pcm", this.soundData);
                BufferBase_Sub4_Sub1 var5 = new BufferBase_Sub4_Sub1(8000, method429(offset, length, this.soundData), 0, length);
                this.aClass8_Sub2_Sub1_1378.method171(var5, 100, 256);
            }
        }
    }

    private String getHostnameIp(int var1, int var2) {
        if (var2 < 99) {
            this.showDialogTradeConfirm = false;
        }

        CacheState var3 = cachePackageManager.method586(20757, var1);

        while (var3.state == 0) {
            sleep(50L, -17239);
        }

        return var3.state == 1 && var3.stateObject != null ? (String) var3.stateObject : BufferBase_Sub3_Sub1.method234(var1);
    }

    private void walkToGroundItem(int var1, int var2, int var3, boolean var4, int var5, int var6) {
        if (var5 != 0) {
            this.method105(90);
        }

        if (!this.walkTo(var2, var1, var6, var1, var6, var3, var4, false, (byte) 98)) {
            this.walkToSource(var6, var2, var1, var5 + 87, var6, var3, true, var4, var1);
        }
    }

    private void loadMedia(int var1) {
        byte[] var2 = this.readDataFile(8, 20, "2d graphics", -10);
        if (var2 == null) {
            this.errorLoadingData = true;
        } else {
            byte[] buff = Utility.loadData(var2, 0, "index.dat");
            this.surface.parseSprite(buff, this.spriteMedia, (byte) 97, Utility.loadData(var2, 0, "inv1.dat"), 1);
            this.surface.parseSprite(buff, 1 + this.spriteMedia, (byte) 74, Utility.loadData(var2, 0, "inv2.dat"), 6);
            this.surface.parseSprite(buff, 9 + this.spriteMedia, (byte) 115, Utility.loadData(var2, 0, "bubble.dat"), 1);
            this.surface.parseSprite(buff, 10 + this.spriteMedia, (byte) 108, Utility.loadData(var2, 0, "runescape.dat"), 1);
            this.surface.parseSprite(buff, this.spriteMedia - -11, (byte) 125, Utility.loadData(var2, 0, "splat.dat"), 3);
            this.surface.parseSprite(buff, this.spriteMedia + 14, (byte) 73, Utility.loadData(var2, 0, "icon.dat"), 8);
            this.surface.parseSprite(buff, this.spriteMedia + 22, (byte) 95, Utility.loadData(var2, 0, "hbar.dat"), 1);
            this.surface.parseSprite(buff, 23 + this.spriteMedia, (byte) 86, Utility.loadData(var2, 0, "hbar2.dat"), 1);
            this.surface.parseSprite(buff, 24 + this.spriteMedia, (byte) 68, Utility.loadData(var2, 0, "compass.dat"), 1);
            this.surface.parseSprite(buff, 25 + this.spriteMedia, (byte) 111, Utility.loadData(var2, 0, "buttons.dat"), 2);
            this.surface.parseSprite(buff, this.spriteUtil, (byte) 108, Utility.loadData(var2, 0, "scrollbar.dat"), 2);
            this.surface.parseSprite(buff, 2 + this.spriteUtil, (byte) 84, Utility.loadData(var2, 0, "corners.dat"), 4);
            this.surface.parseSprite(buff, 6 + this.spriteUtil, (byte) 121, Utility.loadData(var2, 0, "arrows.dat"), 2);
            this.surface.parseSprite(buff, this.spriteProjectile, (byte) 74, Utility.loadData(var2, 0, "projectile.dat"), GameData.anInt199);
            this.surface.parseSprite(buff, this.spriteCrowns, (byte) 87, Utility.loadData(var2, 0, "crowns.dat"), 2);
            this.surface.setCrownStartId(this.spriteCrowns, -97);
            int var4 = GameData.itemSpriteCount;

            int var6;
            for (int var5 = 1; var4 > 0; ++var5) {
                var6 = var4;
                var4 -= 30;
                if (var6 > 30) {
                    var6 = 30;
                }

                this.surface.parseSprite(buff, 30 * (-1 + var5) + this.spriteItem, (byte) 106, Utility.loadData(var2, 0, "objects" + var5 + ".dat"), var6);
            }

            this.surface.loadSprite(1, this.spriteMedia);
            this.surface.loadSprite(1, 9 + this.spriteMedia);

            for (var6 = 11; var6 <= 26; ++var6) {
                this.surface.loadSprite(1, this.spriteMedia + var6);
            }

            for (int var7 = 0; GameData.anInt199 > var7; ++var7) {
                this.surface.loadSprite(1, var7 + this.spriteProjectile);
            }

            for (int var8 = 0; GameData.itemSpriteCount > var8; ++var8) {
                this.surface.loadSprite(1, var8 + this.spriteItem);
            }

            int var9 = -120 % ((-82 - var1) / 38);
        }
    }

    private int getInventoryCount(int id) {
        int var3 = 0;
        int var4 = 0;
        for (; var4 < this.inventoryItemsCount; ++var4) {
            if (id == this.inventoryItemId[var4]) {
                if (GameData.itemStackable[id] == 1) {
                    ++var3;
                } else {
                    var3 += this.inventoryItemStackCount[var4];
                }
            }
        }

        return var3;
    }

}
