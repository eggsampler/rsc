import java.awt.*;
import java.awt.image.*;
import java.util.Hashtable;

class Surface implements ImageProducer, ImageObserver {

    static byte[][] gameFonts = new byte[50][];
    static int anInt196 = 0;
    static int anInt1129 = 0;
    static int anInt751 = 0;
    int[] spriteHeight;
    int[][] surfacePixels;
    int[][] spriteColourList;
    int[] spriteWidth;
    int[] pixels;
    int[] spriteWidthFull;
    int width2;
    int[] spriteHeightFull;
    byte[][] spriteColoursUsed;
    int height2;
    boolean interlace = false;
    boolean loggedIn = false;
    private int[] anIntArray566;
    private Image anImage578;
    private int[] anIntArray579;
    private boolean[] spriteTranslate;
    private int crownStartId;
    private ImageConsumer anImageConsumer593;
    private int[] spriteTranslateY;
    private int[] anIntArray610;
    private int[] anIntArray614;
    private int boundsTopX = 0;
    private int[] spriteTranslateX;
    private ColorModel aColorModel634;
    private int boundsBottomY = 0;
    private int[] anIntArray637;
    private int boundsTopY = 0;
    private int[] anIntArray653;
    private int boundsBottomX = 0;
    private int[] anIntArray664;

    Surface(int var1, int var2, int var3, Component var4) {
        this.spriteWidthFull = new int[var3];
        this.height2 = var2;
        this.spriteColoursUsed = new byte[var3][];
        this.spriteTranslate = new boolean[var3];
        this.boundsBottomY = var2;
        this.spriteTranslateX = new int[var3];
        this.boundsBottomX = var1;
        this.width2 = var1;
        this.spriteWidth = new int[var3];
        this.spriteHeight = new int[var3];
        this.spriteTranslateY = new int[var3];
        this.surfacePixels = new int[var3][];
        this.pixels = new int[var2 * var1];
        this.spriteColourList = new int[var3][];
        this.spriteHeightFull = new int[var3];
        if (var1 > 1 && var2 > 1 && var4 != null) {
            this.aColorModel634 = new DirectColorModel(32, 16711680, '\uff00', 255);
            int var5 = this.width2 * this.height2;

            for (int var6 = 0; var5 > var6; ++var6) {
                this.pixels[var6] = 0;
            }

            this.anImage578 = var4.createImage(this);
            this.method390(false);
            var4.prepareImage(this.anImage578, var4);
            this.method390(false);
            var4.prepareImage(this.anImage578, var4);
            this.method390(false);
            var4.prepareImage(this.anImage578, var4);
        }
    }

    static int rgb2long(int var1, int var2, int var3) {
        return (var3 << 16) - (-(var2 << 8) + -var1);
    }

    private void method372(int var1, int var2, int var3, int var4, byte var5, int var6, int[] var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, int var15, int[] var16) {
        int var17 = 125 % ((-35 - var5) / 41);
        int var18 = (var12 & 16765487) >> 16;
        int var19 = var12 >> 8 & 255;
        int var20 = var12 & 255;

        try {
            int var21 = var10;

            for (int var22 = -var1; var22 < 0; ++var22) {
                int var23 = (var15 >> 16) * var4;
                int var24 = var13 >> 16;
                int var25 = var9;
                int var26;
                if (var24 < this.boundsTopX) {
                    var26 = this.boundsTopX - var24;
                    var25 = var9 - var26;
                    var10 += var2 * var26;
                    var24 = this.boundsTopX;
                }

                if (this.boundsBottomX <= (var24 + var25)) {
                    var26 = -this.boundsBottomX + var25 + var24;
                    var25 -= var26;
                }

                var6 = -var6 + 1;
                if (var6 != 0) {
                    for (var26 = var24; (var25 + var24) > var26; ++var26) {
                        var8 = var16[(var10 >> 16) - -var23];
                        var10 += var2;
                        if (var8 != 0) {
                            int var27 = (var8 & '\uff97') >> 8;
                            int var28 = var8 & 255;
                            int var29 = (16720564 & var8) >> 16;
                            if (var29 == var27 && var27 == var28) {
                                var7[var26 + var14] = (var20 * var28 >> 8) + (var19 * var27 >> 8 << 8) + (var18 * var29 >> 8 << 16);
                            } else {
                                var7[var26 + var14] = var8;
                            }
                        }
                    }
                }

                var15 += var3;
                var14 += this.width2;
                var13 += var11;
                var10 = var21;
            }

        } catch (Exception var30) {
            System.out.println("error in transparent sprite plot routine");
        }
        ;
    }

    public final synchronized void removeConsumer(ImageConsumer var1) {
        if (this.anImageConsumer593 == var1) {
            this.anImageConsumer593 = null;
        }
    }

    final void setBounds(int x1, int y1, int x2, int y2) {
        if (x2 > this.width2) {
            x2 = this.width2;
        }

        if (x1 < 0) {
            x1 = 0;
        }

        if (y1 < 0) {
            y1 = 0;
        }

        if (this.height2 < y2) {
            y2 = this.height2;
        }

        this.boundsBottomX = x2;

        this.boundsTopX = x1;
        this.boundsTopY = y1;
        this.boundsBottomY = y2;
    }

    final void drawGradient(int x, int y, int width, int height, int colourTop, int colourBottom) {
        if (x < this.boundsTopX) {
            width -= -x + this.boundsTopX;
            x = this.boundsTopX;
        }

        if ((width + x) > this.boundsBottomX) {
            width = -x + this.boundsBottomX;
        }

        int var8 = (colourBottom & 16761084) >> 16;
        int var9 = ('\uffbb' & colourBottom) >> 8;
        int var10 = colourBottom & 255;
        int var11 = 255 & colourTop >> 16;
        int var12 = 255 & colourTop >> 8;
        int var13 = colourTop & 255;

        int var14 = this.width2 - width;
        byte var15 = 1;
        if (this.interlace) {
            var14 += this.width2;
            var15 = 2;
            if ((y & 1) != 0) {
                ++y;
                --height;
            }
        }

        int var16 = x + y * this.width2;

        for (int var17 = 0; height > var17; var17 += var15) {
            if (this.boundsTopY <= y + var17 && this.boundsBottomY > (var17 + y)) {
                int var18 = ((var17 * var9 + (-var17 + height) * var12) / height << 8) + (((height + -var17) * var11 + var8 * var17) / height << 16) - -((var17 * var10 - -((-var17 + height) * var13)) / height);

                for (int var19 = -width; var19 < 0; ++var19) {
                    this.pixels[var16++] = var18;
                }

                var16 += var14;
            } else {
                var16 += this.width2;
            }
        }
    }

    final void loadSprite(int var1, int spriteId) {
        if (this.spriteColoursUsed[spriteId] != null) {
            int size = this.spriteHeight[spriteId] * this.spriteWidth[spriteId];
            byte[] idx = this.spriteColoursUsed[spriteId];
            int[] cols = this.spriteColourList[spriteId];
            int[] pixels = new int[size];
            if (var1 != 1) {
                this.aColorModel634 = null;
            }

            for (int pixel = 0; size > pixel; ++pixel) {
                int colour = cols[idx[pixel] & 255];
                if (colour != 0) {
                    if (colour == 16711935) {
                        colour = 0;
                    }
                } else {
                    colour = 1;
                }

                pixels[pixel] = colour;
            }

            this.surfacePixels[spriteId] = pixels;
            this.spriteColoursUsed[spriteId] = null;
            this.spriteColourList[spriteId] = null;
        }
    }

    public final void requestTopDownLeftRightResend(ImageConsumer var1) {
        System.out.println("TDLR");
    }

    private void drawSprite(int var1, int var2, int var3, int var4, int[] var5, int var6, int var7, int var8, int var9, int var10, int[] var11) {
        if (var4 == -10642) {
            int var12 = -(var1 >> 2);
            var1 = -(var1 & 3);

            for (int var13 = -var9; var13 < 0; var13 += var8) {
                for (int var14 = var12; var14 < 0; ++var14) {
                    var7 = var11[var10++];
                    if (var7 != 0) {
                        var5[var3++] = var7;
                    } else {
                        ++var3;
                    }

                    var7 = var11[var10++];
                    if (var7 != 0) {
                        var5[var3++] = var7;
                    } else {
                        ++var3;
                    }

                    var7 = var11[var10++];
                    if (var7 == 0) {
                        ++var3;
                    } else {
                        var5[var3++] = var7;
                    }

                    var7 = var11[var10++];
                    if (var7 != 0) {
                        var5[var3++] = var7;
                    } else {
                        ++var3;
                    }
                }

                for (int var15 = var1; var15 < 0; ++var15) {
                    var7 = var11[var10++];
                    if (var7 == 0) {
                        ++var3;
                    } else {
                        var5[var3++] = var7;
                    }
                }

                var3 += var2;
                var10 += var6;
            }

        }
    }

    private void method377(int var1, boolean var2, int var3, int var4, int var5, int var6, String var7) {
        this.drawstring(var7, -this.textWidth(var3, -127, var7) + var6, var1, var3, var4, var5);
        if (!var2) {
            this.method382(-30, -61, (byte[]) null, (int[]) null, -18, 66, 47, -54, 76, -22, -39, -17, 65, -100, 99, (int[]) null, 94);
        }
    }

    final int textHeight(int var1, boolean var2) {
        return var1 == 0 ? 12 : (var1 == 1 ? 14 : (var1 == 2 ? 14 : (!var2 ? 78 : (var1 == 3 ? 15 : (var1 == 4 ? 15 : (var1 == 5 ? 19 : (var1 == 6 ? 24 : (var1 == 7 ? 29 : this.method393((byte) -120, var1)))))))));
    }

    final void drawBoxEdge(int x, int y, int width, int height, int colour) {
        this.drawLineHoriz(x, y, width, colour);
        this.drawLineHoriz(x, -1 + height + y, width, colour);
        this.drawLineVert(x, y, height, colour);
        this.drawLineVert(-1 + (x - -width), y, height, colour);
    }

    final void drawMinimapSprite(int var1, int var2, int var3, int var4, boolean var5, int var6) {
        int var7 = this.width2;
        int var8 = this.height2;
        int var9;
        if (this.anIntArray637 == null) {
            this.anIntArray637 = new int[512];

            for (var9 = 0; var9 < 256; ++var9) {
                this.anIntArray637[var9] = (int) (32768.0D * Math.sin((double) var9 * 0.02454369D));
                this.anIntArray637[var9 + 256] = (int) (Math.cos((double) var9 * 0.02454369D) * 32768.0D);
            }
        }

        var9 = -this.spriteWidthFull[var3] / 2;
        int var10 = -this.spriteHeightFull[var3] / 2;
        if (this.spriteTranslate[var3]) {
            var10 += this.spriteTranslateY[var3];
            var9 += this.spriteTranslateX[var3];
        }

        int var11 = this.spriteWidth[var3] + var9;
        int var12 = this.spriteHeight[var3] + var10;
        var6 &= 255;
        int var17 = this.anIntArray637[var6] * var2;
        int var18 = this.anIntArray637[var6 + 256] * var2;
        int var19 = var1 + (var9 * var18 + var17 * var10 >> 22);
        int var20 = (-(var9 * var17) + var18 * var10 >> 22) + var4;
        int var21 = (var10 * var17 - -(var11 * var18) >> 22) + var1;
        int var22 = (-(var17 * var11) + var18 * var10 >> 22) + var4;
        int var23 = (var12 * var17 + var11 * var18 >> 22) + var1;
        int var24 = var4 + (-(var17 * var11) + var12 * var18 >> 22);
        int var25 = (var9 * var18 + var17 * var12 >> 22) + var1;
        int var26 = (var18 * var12 - var17 * var9 >> 22) + var4;
        if (var2 == 192 && (63 & anInt196) == (var6 & 63)) {
            ++anInt1129;
        } else if (var2 == 128) {
            anInt196 = var6;
        } else {
            ++anInt751;
        }

        int var27 = var20;
        int var28 = var20;
        if (var20 <= var22) {
            if (var20 < var22) {
                var28 = var22;
            }
        } else {
            var27 = var22;
        }

        if (var27 > var24) {
            var27 = var24;
        } else if (var28 < var24) {
            var28 = var24;
        }

        if (!var5) {
            this.method399(-93, (int[]) null, 127, (int[]) null, true, -113, 41, -68, 55, 9, -125);
        }

        if (var27 <= var26) {
            if (var26 > var28) {
                var28 = var26;
            }
        } else {
            var27 = var26;
        }

        if (this.anIntArray653 == null || this.anIntArray653.length != (var8 + 1)) {
            this.anIntArray610 = new int[1 + var8];
            this.anIntArray566 = new int[1 + var8];
            this.anIntArray664 = new int[1 + var8];
            this.anIntArray653 = new int[var8 - -1];
            this.anIntArray614 = new int[1 + var8];
            this.anIntArray579 = new int[var8 - -1];
        }

        if (var28 > this.boundsBottomY) {
            var28 = this.boundsBottomY;
        }

        if (this.boundsTopY > var27) {
            var27 = this.boundsTopY;
        }

        for (int var29 = var27; var28 >= var29; ++var29) {
            this.anIntArray653[var29] = 99999999;
            this.anIntArray614[var29] = -99999999;
        }

        int var30 = 0;
        int var31 = 0;
        int var32 = 0;
        int var33 = this.spriteWidth[var3];
        int var13 = var33 - 1;
        byte var54 = 0;
        int var34 = this.spriteHeight[var3];
        var11 = var33 - 1;
        byte var14 = 0;
        byte var55 = 0;
        byte var15 = 0;
        var12 = -1 + var34;
        int var16 = var34 + -1;
        int var35;
        int var38;
        int var36;
        int var37;
        if (var26 >= var20) {
            var35 = var19 << 8;
            var36 = var54 << 8;
            var37 = var20;
            var38 = var26;
        } else {
            var38 = var20;
            var36 = var16 << 8;
            var37 = var26;
            var35 = var25 << 8;
        }

        if (var26 != var20) {
            var30 = (-var19 + var25 << 8) / (-var20 + var26);
            var32 = (-var54 + var16 << 8) / (var26 + -var20);
        }

        if (var38 > var8 + -1) {
            var38 = -1 + var8;
        }

        if (var37 < 0) {
            var35 -= var37 * var30;
            var36 -= var32 * var37;
            var37 = 0;
        }

        for (int var39 = var37; var38 >= var39; ++var39) {
            this.anIntArray653[var39] = this.anIntArray614[var39] = var35;
            this.anIntArray664[var39] = this.anIntArray566[var39] = 0;
            var35 += var30;
            this.anIntArray610[var39] = this.anIntArray579[var39] = var36;
            var36 += var32;
        }

        int var40;
        if (var22 >= var20) {
            var40 = var55 << 8;
            var35 = var19 << 8;
            var37 = var20;
            var38 = var22;
        } else {
            var37 = var22;
            var35 = var21 << 8;
            var40 = var13 << 8;
            var38 = var20;
        }

        if (var20 != var22) {
            var31 = (var13 + -var55 << 8) / (-var20 + var22);
            var30 = (-var19 + var21 << 8) / (var22 + -var20);
        }

        if (var37 < 0) {
            var35 -= var30 * var37;
            var40 -= var31 * var37;
            var37 = 0;
        }

        if (var38 > -1 + var8) {
            var38 = -1 + var8;
        }

        for (int var41 = var37; var38 >= var41; ++var41) {
            if (var35 < this.anIntArray653[var41]) {
                this.anIntArray653[var41] = var35;
                this.anIntArray664[var41] = var40;
                this.anIntArray610[var41] = 0;
            }

            if (var35 > this.anIntArray614[var41]) {
                this.anIntArray614[var41] = var35;
                this.anIntArray566[var41] = var40;
                this.anIntArray579[var41] = 0;
            }

            var35 += var30;
            var40 += var31;
        }

        if (var24 >= var22) {
            var38 = var24;
            var36 = var14 << 8;
            var37 = var22;
            var35 = var21 << 8;
            var40 = var13 << 8;
        } else {
            var38 = var22;
            var36 = var12 << 8;
            var40 = var11 << 8;
            var35 = var23 << 8;
            var37 = var24;
        }

        if (var22 != var24) {
            var32 = (-var14 + var12 << 8) / (var24 - var22);
            var30 = (var23 - var21 << 8) / (-var22 + var24);
        }

        if ((-1 + var8) < var38) {
            var38 = var8 - 1;
        }

        if (var37 < 0) {
            var36 -= var37 * var32;
            var35 -= var37 * var30;
            var37 = 0;
        }

        for (int var42 = var37; var38 >= var42; ++var42) {
            if (this.anIntArray653[var42] > var35) {
                this.anIntArray653[var42] = var35;
                this.anIntArray664[var42] = var40;
                this.anIntArray610[var42] = var36;
            }

            if (var35 > this.anIntArray614[var42]) {
                this.anIntArray614[var42] = var35;
                this.anIntArray566[var42] = var40;
                this.anIntArray579[var42] = var36;
            }

            var36 += var32;
            var35 += var30;
        }

        if (var24 != var26) {
            var30 = (var25 - var23 << 8) / (var26 - var24);
            var31 = (-var11 + var15 << 8) / (var26 + -var24);
        }

        if (var26 < var24) {
            var40 = var15 << 8;
            var37 = var26;
            var38 = var24;
            var35 = var25 << 8;
            var36 = var16 << 8;
        } else {
            var35 = var23 << 8;
            var40 = var11 << 8;
            var37 = var24;
            var38 = var26;
            var36 = var12 << 8;
        }

        if (var37 < 0) {
            var35 -= var37 * var30;
            var40 -= var37 * var31;
            var37 = 0;
        }

        if (-1 + var8 < var38) {
            var38 = var8 + -1;
        }

        for (int var43 = var37; var43 <= var38; ++var43) {
            if (this.anIntArray653[var43] > var35) {
                this.anIntArray653[var43] = var35;
                this.anIntArray664[var43] = var40;
                this.anIntArray610[var43] = var36;
            }

            if (this.anIntArray614[var43] < var35) {
                this.anIntArray614[var43] = var35;
                this.anIntArray566[var43] = var40;
                this.anIntArray579[var43] = var36;
            }

            var40 += var31;
            var35 += var30;
        }

        int var44 = var27 * var7;
        int[] var45 = this.surfacePixels[var3];

        for (int var46 = var27; var28 > var46; ++var46) {
            int var47 = this.anIntArray653[var46] >> 8;
            int var48 = this.anIntArray614[var46] >> 8;
            if (-var47 + var48 <= 0) {
                var44 += var7;
            } else {
                int var49 = this.anIntArray664[var46] << 9;
                int var50 = (-var49 + (this.anIntArray566[var46] << 9)) / (var48 - var47);
                int var51 = this.anIntArray610[var46] << 9;
                int var52 = (-var51 + (this.anIntArray579[var46] << 9)) / (var48 - var47);
                if (this.boundsTopX > var47) {
                    var51 += (this.boundsTopX + -var47) * var52;
                    var49 += var50 * (-var47 + this.boundsTopX);
                    var47 = this.boundsTopX;
                }

                if (var48 > this.boundsBottomX) {
                    var48 = this.boundsBottomX;
                }

                if (!this.interlace || (var46 & 1) == 0) {
                    if (this.spriteTranslate[var3]) {
                        this.method410(var33, this.pixels, var51, var45, var49, -var48 + var47, var47 + var44, 0, var52, var50, 185018513);
                    } else {
                        this.method399(var33, var45, var44 + var47, this.pixels, true, var50, 0, var47 - var48, var51, var52, var49);
                    }
                }

                var44 += var7;
            }
        }
        ;
    }

    final void drawLineVert(int x, int y, int height, int colour) {
        if (this.boundsTopX <= x && x < this.boundsBottomX) {
            if (this.boundsTopY > y) {
                height -= this.boundsTopY - y;
                y = this.boundsTopY;
            }

            if (y - -height > this.boundsBottomY) {
                height = -y + this.boundsBottomY;
            }

            if (height > 0) {
                int var6 = x - -(this.width2 * y);

                for (int var7 = 0; var7 < height; ++var7) {
                    this.pixels[var6 + this.width2 * var7] = colour;
                }

            }
        }
    }

    private void method382(int var1, int var2, byte[] var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, int var15, int[] var16, int var17) {
        int var18 = (16757176 & var7) >> 16;
        int var19 = ('\uff11' & var7) >> 8;
        if (var11 != -1321763928) {
            this.anIntArray566 = null;
        }

        int var20 = var7 & 255;

        try {
            int var21 = var9;

            for (int var22 = -var14; var22 < 0; ++var22) {
                int var23 = var12 * (var17 >> 16);
                int var24 = var6 >> 16;
                int var25 = var5;
                int var26;
                if (this.boundsTopX > var24) {
                    var26 = this.boundsTopX + -var24;
                    var9 += var15 * var26;
                    var24 = this.boundsTopX;
                    var25 = var5 - var26;
                }

                var2 = -var2 + 1;
                if (this.boundsBottomX <= var25 + var24) {
                    var26 = var24 - (-var25 - -this.boundsBottomX);
                    var25 -= var26;
                }

                var17 += var8;
                if (var2 != 0) {
                    for (var26 = var24; var25 + var24 > var26; ++var26) {
                        var1 = 255 & var3[(var9 >> 16) + var23];
                        var9 += var15;
                        if (var1 != 0) {
                            var1 = var16[var1];
                            int var27 = (var1 & 16741842) >> 16;
                            int var28 = var1 >> 8 & 255;
                            int var29 = var1 & 255;
                            if (var28 == var27 && var29 == var28) {
                                var4[var26 - -var13] = (var20 * var29 >> 8) + (var19 * var28 >> 8 << 8) + (var18 * var27 >> 8 << 16);
                            } else {
                                var4[var13 + var26] = var1;
                            }
                        }
                    }
                }

                var9 = var21;
                var13 += this.width2;
                var6 += var10;
            }

        } catch (Exception var30) {
            System.out.println("error in transparent sprite plot routine");
        }
        ;
    }

    final void drawstring(String text, int x, int y, int font, int colour, int crown) {
        try {
            if (crown > 0) {
                int crownSprite = this.crownStartId - (-crown - -1);
                if (this.spriteColoursUsed[crownSprite] != null) {
                    this.drawSprite((byte) 61, y - this.spriteHeight[crownSprite], crownSprite, x);
                    x += this.spriteWidth[crownSprite] - -5;
                }
            }

            byte[] fontData = gameFonts[font];

            for (int idx = 0; idx < text.length(); ++idx) {
                if (text.charAt(idx) == 64 && 4 + idx < text.length() && text.charAt(4 + idx) == 64) {
                    if (text.substring(1 + idx, idx + 4).equalsIgnoreCase("red")) {
                        colour = 16711680;
                    } else if (!text.substring(1 + idx, 4 + idx).equalsIgnoreCase("lre")) {
                        if (text.substring(1 + idx, idx + 4).equalsIgnoreCase("yel")) {
                            colour = 16776960;
                        } else if (text.substring(idx + 1, 4 + idx).equalsIgnoreCase("gre")) {
                            colour = '\uff00';
                        } else if (!text.substring(1 + idx, 4 + idx).equalsIgnoreCase("blu")) {
                            if (!text.substring(1 + idx, 4 + idx).equalsIgnoreCase("cya")) {
                                if (!text.substring(1 + idx, 4 + idx).equalsIgnoreCase("mag")) {
                                    if (text.substring(idx - -1, 4 + idx).equalsIgnoreCase("whi")) {
                                        colour = 16777215;
                                    } else if (text.substring(idx + 1, idx + 4).equalsIgnoreCase("bla")) {
                                        colour = 0;
                                    } else if (text.substring(1 + idx, idx + 4).equalsIgnoreCase("dre")) {
                                        colour = 12582912;
                                    } else if (!text.substring(idx + 1, idx + 4).equalsIgnoreCase("ora")) {
                                        if (!text.substring(idx + 1, 4 + idx).equalsIgnoreCase("ran")) {
                                            if (!text.substring(1 + idx, 4 + idx).equalsIgnoreCase("or1")) {
                                                if (!text.substring(1 + idx, idx + 4).equalsIgnoreCase("or2")) {
                                                    if (!text.substring(1 + idx, 4 + idx).equalsIgnoreCase("or3")) {
                                                        if (!text.substring(1 + idx, 4 + idx).equalsIgnoreCase("gr1")) {
                                                            if (text.substring(idx + 1, 4 + idx).equalsIgnoreCase("gr2")) {
                                                                colour = 8453888;
                                                            } else if (text.substring(1 + idx, idx - -4).equalsIgnoreCase("gr3")) {
                                                                colour = 4259584;
                                                            }
                                                        } else {
                                                            colour = 12648192;
                                                        }
                                                    } else {
                                                        colour = 16723968;
                                                    }
                                                } else {
                                                    colour = 16740352;
                                                }
                                            } else {
                                                colour = 16756736;
                                            }
                                        } else {
                                            colour = (int) (Math.random() * 1.6777215E7D);
                                        }
                                    } else {
                                        colour = 16748608;
                                    }
                                } else {
                                    colour = 16711935;
                                }
                            } else {
                                colour = '\uffff';
                            }
                        } else {
                            colour = 255;
                        }
                    } else {
                        colour = 16748608;
                    }

                    idx += 4;
                } else {
                    char chr;
                    if (text.charAt(idx) == 126 && (4 + idx) < text.length() && text.charAt(idx - -4) == 126) {
                        chr = text.charAt(1 + idx);
                        char var16 = text.charAt(idx + 2);
                        char var12 = text.charAt(3 + idx);
                        if (chr >= 48 && chr <= 57 && var16 >= 48 && var16 <= 57 && var12 >= 48 && var12 <= 57) {
                            x = Integer.parseInt(text.substring(1 + idx, 4 + idx));
                        }

                        idx += 4;
                    } else {
                        chr = text.charAt(idx);
                        if (chr == 160) {
                            chr = 32;
                        }

                        if (chr < 0 || JagGrab.caracterWidth.length <= chr) {
                            chr = 32;
                        }

                        int width = JagGrab.caracterWidth[chr];
                        if (this.loggedIn && !Scene.aBooleanArray418[font] && colour != 0) {
                            this.drawCharacter(width, x + 1, y, 0, fontData, Scene.aBooleanArray418[font]);
                        }

                        if (this.loggedIn && !Scene.aBooleanArray418[font] && colour != 0) {
                            this.drawCharacter(width, x, y - -1, 0, fontData, Scene.aBooleanArray418[font]);
                        }

                        this.drawCharacter(width, x, y, colour, fontData, Scene.aBooleanArray418[font]);
                        x += fontData[7 + width];
                    }
                }
            }
        } catch (Exception var13) {
            System.out.println("drawstring: " + var13);
            var13.printStackTrace();
        }
    }

    final void parseSprite(byte[] indexData, int spriteId, byte var3, byte[] spriteData, int frameCount) {
        if (var3 <= 66) {
            this.drawSpriteAlpha(-16, -9, -7, -45);
        }

        int indexOff = Utility.getUnsignedShort(0, spriteData, 117);
        int fullWidth = Utility.getUnsignedShort(indexOff, indexData, 118);
        indexOff += 2;
        int fullHeight = Utility.getUnsignedShort(indexOff, indexData, 98);
        indexOff += 2;
        int colourCount = indexData[indexOff++] & 255;
        int[] colours = new int[colourCount];
        colours[0] = 16711935;

        for (int i = 0; i < (colourCount - 1); ++i) {
            colours[1 + i] = Utility.bitwiseAnd(255, indexData[indexOff - -2]) + Utility.bitwiseAnd('\uff00', indexData[1 + indexOff] << 8) + (Utility.bitwiseAnd(indexData[indexOff], 255) << 16);
            indexOff += 3;
        }

        int var12 = 2;

        for (int id = spriteId; frameCount + spriteId > id; ++id) {
            this.spriteTranslateX[id] = Utility.bitwiseAnd(255, indexData[indexOff++]);
            this.spriteTranslateY[id] = Utility.bitwiseAnd(indexData[indexOff++], 255);
            this.spriteWidth[id] = Utility.getUnsignedShort(indexOff, indexData, 98);
            indexOff += 2;
            this.spriteHeight[id] = Utility.getUnsignedShort(indexOff, indexData, 85);
            indexOff += 2;
            int unknown = indexData[indexOff++] & 255;
            int size = this.spriteHeight[id] * this.spriteWidth[id];
            this.spriteColoursUsed[id] = new byte[size];
            this.spriteColourList[id] = colours;
            this.spriteWidthFull[id] = fullWidth;
            this.spriteHeightFull[id] = fullHeight;
            this.surfacePixels[id] = null;
            this.spriteTranslate[id] = false;
            if (this.spriteTranslateX[id] != 0 || this.spriteTranslateY[id] != 0) {
                this.spriteTranslate[id] = true;
            }

            if (unknown == 0) {
                for (int pixel = 0; size > pixel; ++pixel) {
                    this.spriteColoursUsed[id][pixel] = spriteData[var12++];
                    if (this.spriteColoursUsed[id][pixel] == 0) {
                        this.spriteTranslate[id] = true;
                    }
                }
            } else if (unknown == 1) {
                for (int x = 0; this.spriteWidth[id] > x; ++x) {
                    for (int y = 0; this.spriteHeight[id] > y; ++y) {
                        this.spriteColoursUsed[id][x - -(y * this.spriteWidth[id])] = spriteData[var12++];
                        if (this.spriteColoursUsed[id][x + this.spriteWidth[id] * y] == 0) {
                            this.spriteTranslate[id] = true;
                        }
                    }
                }
            }
        }
        ;
    }

    final void blackScreen(byte var1) {
        if (var1 != 127) {
            this.removeConsumer((ImageConsumer) null);
        }

        int var2 = this.width2 * this.height2;
        int var3;
        if (!this.interlace) {
            for (var3 = 0; var3 < var2; ++var3) {
                this.pixels[var3] = 0;
            }

        } else {
            var3 = 0;

            for (int var4 = -this.height2; var4 < 0; var4 += 2) {
                for (int var5 = -this.width2; var5 < 0; ++var5) {
                    this.pixels[var3++] = 0;
                }

                var3 += this.width2;
            }

        }
        ;
    }

    final void drawSprite(byte var1, int var2, int id, int var4) {
        if (this.spriteTranslate[id]) {
            var4 += this.spriteTranslateX[id];
            var2 += this.spriteTranslateY[id];
        }

        int var5 = var2 * this.width2 + var4;
        int var6 = 0;
        int var7 = this.spriteHeight[id];
        int var8 = this.spriteWidth[id];
        if (var1 != 61) {
            this.spriteTranslateX = null;
        }

        int var9 = -var8 + this.width2;
        int var10;
        if (this.boundsTopY > var2) {
            var10 = this.boundsTopY - var2;
            var2 = this.boundsTopY;
            var6 += var10 * var8;
            var5 += var10 * this.width2;
            var7 -= var10;
        }

        int var11 = 0;
        if (var4 < this.boundsTopX) {
            var10 = -var4 + this.boundsTopX;
            var6 += var10;
            var8 -= var10;
            var11 += var10;
            var5 += var10;
            var9 += var10;
            var4 = this.boundsTopX;
        }

        if ((var7 + var2) >= this.boundsBottomY) {
            var7 -= 1 + var7 + (var2 - this.boundsBottomY);
        }

        if (var8 + var4 >= this.boundsBottomX) {
            var10 = var8 + var4 - (this.boundsBottomX - 1);
            var11 += var10;
            var9 += var10;
            var8 -= var10;
        }

        if (var8 > 0 && var7 > 0) {
            byte var13 = 1;
            if (this.interlace) {
                var11 += this.spriteWidth[id];
                var9 += this.width2;
                if ((1 & var2) != 0) {
                    var5 += this.width2;
                    --var7;
                }

                var13 = 2;
            }

            if (this.surfacePixels[id] == null) {
                this.drawSprite(var11, this.pixels, var8, this.spriteColourList[id], var13, var5, (byte) 64, var6, this.spriteColoursUsed[id], var7, var9);
            } else {
                this.drawSprite(var8, var9, var5, -10642, this.pixels, var11, 0, var13, var7, var6, this.surfacePixels[id]);
            }
        }
        ;
    }

    final void drawBoxAlpha(int x, int y, int width, int height, int colour, int alpha) {
        if (y < this.boundsTopY) {
            height -= -y + this.boundsTopY;
            y = this.boundsTopY;
        }

        if (this.boundsTopX > x) {
            width -= -x + this.boundsTopX;
            x = this.boundsTopX;
        }

        if (y + height > this.boundsBottomY) {
            height = -y + this.boundsBottomY;
        }

        if (this.boundsBottomX < x - -width) {
            width = -x + this.boundsBottomX;
        }

        int var9 = 256 + -alpha;
        int var10 = (colour >> 16 & 255) * alpha;
        int var11 = ((colour & '\uffc4') >> 8) * alpha;
        int var12 = (255 & colour) * alpha;
        int var13 = -width + this.width2;
        byte var14 = 1;
        if (this.interlace) {
            var13 += this.width2;
            var14 = 2;
            if ((y & 1) != 0) {
                ++y;
                --height;
            }
        }

        int var15 = this.width2 * y + x;

        for (int var16 = 0; var16 < height; var16 += var14) {
            for (int var17 = -width; var17 < 0; ++var17) {
                int var18 = var9 * (255 & this.pixels[var15]);
                int var19 = var9 * (255 & this.pixels[var15] >> 8);
                int var20 = var9 * (this.pixels[var15] >> 16 & 255);
                int var21 = (var18 + var12 >> 8) + (var10 - -var20 >> 8 << 16) + (var11 - -var19 >> 8 << 8);
                this.pixels[var15++] = var21;
            }

            var15 += var13;
        }
    }

    final void drawSprite(byte var1, int var2, int var3, int var4, int var5, int var6) {
        this.spriteWidth[var3] = var4;
        this.spriteHeight[var3] = var2;
        this.spriteTranslate[var3] = false;
        int var7 = -15 % ((-75 - var1) / 43);
        this.spriteTranslateX[var3] = 0;
        this.spriteTranslateY[var3] = 0;
        this.spriteWidthFull[var3] = var4;
        this.spriteHeightFull[var3] = var2;
        int var8 = var2 * var4;
        this.surfacePixels[var3] = new int[var8];
        int var9 = 0;

        for (int var10 = var5; var10 < var5 + var2; ++var10) {
            for (int var11 = var6; var6 - -var4 > var11; ++var11) {
                this.surfacePixels[var3][var9++] = this.pixels[var11 + this.width2 * var10];
            }
        }
    }

    final void drawstringCenter(String var1, int var3, int var4, int var5, int var6) {
        this.drawstring(var4, var6, var5, var3, 2, var1, 0);
    }

    private synchronized void method390(boolean var1) {
        if (!var1) {
            if (this.anImageConsumer593 != null) {
                this.anImageConsumer593.setPixels(0, 0, this.width2, this.height2, this.aColorModel634, this.pixels, 0, this.width2);
                this.anImageConsumer593.imageComplete(2);
            }
        }
    }

    final void readSleepWord(int var1, byte var2, byte[] var3) {
        int[] var4 = this.surfacePixels[var1] = new int[10200];
        this.spriteWidth[var1] = 255;
        this.spriteHeight[var1] = 40;
        this.spriteTranslateX[var1] = 0;
        this.spriteTranslateY[var1] = 0;
        this.spriteWidthFull[var1] = 255;
        this.spriteHeightFull[var1] = 40;
        this.spriteTranslate[var1] = false;
        int var5 = 0;
        int var6 = 1;

        int var7;
        int var8;
        int var9;
        for (var7 = 0; var7 < 255; var5 = -var5 + 16777215) {
            var8 = 255 & var3[var6++];

            for (var9 = 0; var9 < var8; ++var9) {
                var4[var7++] = var5;
            }
        }

        var8 = 1;
        if (var2 == -120) {
            while (var8 < 40) {
                var9 = 0;

                while (var9 < 255) {
                    int var10 = 255 & var3[var6++];

                    for (int var11 = 0; var11 < var10; ++var11) {
                        var4[var7] = var4[-255 + var7];
                        ++var9;
                        ++var7;
                    }

                    if (var9 < 255) {
                        var4[var7] = -var4[-255 + var7] + 16777215;
                        ++var9;
                        ++var7;
                    }
                }

                ++var8;
            }

        }
    }

    private void method392(int var1, int var2, int[] var3, int[] var4, int var5, int var6, int var7, int var8, int var9, byte var10, int var11, int var12, int var13, int var14, int var15) {
        int var16 = 255 & var6 >> 16;
        int var17 = 255 & var6 >> 8;
        if (var10 <= 67) {
            this.fade2black(27);
        }

        int var18 = var6 & 255;

        try {
            int var19 = var15;

            for (int var20 = -var2; var20 < 0; var20 += var13) {
                int var21 = (var5 >> 16) * var7;

                for (int var22 = -var12; var22 < 0; ++var22) {
                    var1 = var4[var21 + (var15 >> 16)];
                    if (var1 == 0) {
                        ++var9;
                    } else {
                        int var23 = 255 & var1 >> 16;
                        int var24 = 255 & var1 >> 8;
                        int var25 = var1 & 255;
                        if (var23 == var24 && var25 == var24) {
                            var3[var9++] = (var24 * var17 >> 8 << 8) + (var16 * var23 >> 8 << 16) - -(var18 * var25 >> 8);
                        } else {
                            var3[var9++] = var1;
                        }
                    }

                    var15 += var8;
                }

                var5 += var14;
                var9 += var11;
                var15 = var19;
            }

        } catch (Exception var26) {
            System.out.println("error in plot_scale");
        }
        ;
    }

    private int method393(byte var1, int var2) {
        if (var1 >= -112) {
            this.spriteHeightFull = null;
        }

        return var2 == 0 ? -2 + gameFonts[var2][8] : -1 + gameFonts[var2][8];
    }

    final void draw(int var1, Graphics var2, int var3, int var4) {
        this.method390(false);
        var2.drawImage(this.anImage578, var3, var4, this);
        if (var1 != -2020315800) {
            this.spriteTranslateY = null;
        }
    }

    final void method395(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        try {
            int var8 = this.spriteWidth[var3];
            int var9 = this.spriteHeight[var3];
            int var10 = 0;
            int var11 = 0;
            int var12 = (var8 << 16) / var2;
            int var13 = (var9 << 16) / var5;
            if (var4 == 1536) {
                int var14;
                int var15;
                if (this.spriteTranslate[var3]) {
                    var14 = this.spriteWidthFull[var3];
                    var15 = this.spriteHeightFull[var3];
                    if (var14 == 0 || var15 == 0) {
                        return;
                    }

                    var12 = (var14 << 16) / var2;
                    if ((var2 * this.spriteTranslateX[var3] % var14) != 0) {
                        var10 = (var14 - var2 * this.spriteTranslateX[var3] % var14 << 16) / var2;
                    }

                    var7 += (-1 + var14 + var2 * this.spriteTranslateX[var3]) / var14;
                    if (this.spriteTranslateY[var3] * var5 % var15 != 0) {
                        var11 = (-(var5 * this.spriteTranslateY[var3] % var15) + var15 << 16) / var5;
                    }

                    var13 = (var15 << 16) / var5;
                    var6 += (-1 + this.spriteTranslateY[var3] * var5 - -var15) / var15;
                    var5 = var5 * (-(var11 >> 16) + this.spriteHeight[var3]) / var15;
                    var2 = var2 * (-(var10 >> 16) + this.spriteWidth[var3]) / var14;
                }

                var14 = var7 - -(this.width2 * var6);
                var15 = this.width2 + -var2;
                int var16;
                if (this.boundsTopY > var6) {
                    var16 = this.boundsTopY - var6;
                    var11 += var16 * var13;
                    var6 = 0;
                    var14 += this.width2 * var16;
                    var5 -= var16;
                }

                if (this.boundsBottomY <= (var5 + var6)) {
                    var5 -= -this.boundsBottomY + var5 + var6 + 1;
                }

                if (var7 < this.boundsTopX) {
                    var16 = this.boundsTopX + -var7;
                    var15 += var16;
                    var14 += var16;
                    var10 += var12 * var16;
                    var2 -= var16;
                    var7 = 0;
                }

                if (this.boundsBottomX <= var7 + var2) {
                    var16 = 1 + -this.boundsBottomX + var2 + var7;
                    var2 -= var16;
                    var15 += var16;
                }

                byte var19 = 1;
                if (this.interlace) {
                    var19 = 2;
                    if ((var6 & 1) != 0) {
                        --var5;
                        var14 += this.width2;
                    }

                    var15 += this.width2;
                    var13 += var13;
                }

                this.method392(0, var5, this.pixels, this.surfacePixels[var3], var11, var1, var8, var12, var14, (byte) 121, var15, var2, var19, var13, var10);
            }
        } catch (Exception var17) {
            System.out.println("error in sprite clipping routine");
        }
    }

    private void method396(int[] ai, byte[] abyte0, int i, int j, int k, int l, int i1, int j1, int k1) {
        for (int l1 = -i1; l1 < 0; ++l1) {
            for (int i2 = -l; i2 < 0; ++i2) {
                int j2 = abyte0[j++] & 255;
                if (j2 <= 30) {
                    ++k;
                } else if (j2 >= 230) {
                    ai[k++] = i;
                } else {
                    int k2 = ai[k];
                    ai[k++] = Utility.bitwiseAnd(Utility.bitwiseAnd(16711935, i) * j2 + (256 - j2) * Utility.bitwiseAnd(k2, 16711935), -16711936) + Utility.bitwiseAnd(16711680, Utility.bitwiseAnd(k2, '\uff00') * (-j2 + 256) + j2 * Utility.bitwiseAnd(i, '\uff00')) >> 8;
                }
            }

            k += j1;
            j += k1;
        }
    }

    private void plotLetter(int[] ai, byte[] abyte0, int i, int j, int k, int l, int i1, int j1, int k1) {
        try {
            int l1 = -(l >> 2);
            l = -(l & 3);
            for(int i2 = -i1; i2 < 0; i2++) {
                for (int j2 = l1; j2 < 0; ++j2) {
                    if (abyte0[j++] != 0) {
                        ai[k++] = i;
                    } else {
                        ++k;
                    }

                    if (abyte0[j++] != 0) {
                        ai[k++] = i;
                    } else {
                        ++k;
                    }

                    if (abyte0[j++] == 0) {
                        ++k;
                    } else {
                        ai[k++] = i;
                    }

                    if (abyte0[j++] == 0) {
                        ++k;
                    } else {
                        ai[k++] = i;
                    }
                }

                for (int var14 = l; var14 < 0; ++var14) {
                    if (abyte0[j++] == 0) {
                        ++k;
                    } else {
                        ai[k++] = i;
                    }
                }

                k += j1;
                j += k1;
            }

        } catch (Exception var15) {
            System.out.println("plotletter: " + var15);
            var15.printStackTrace();
        }
    }

    final void method398(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        int var8 = var2;

        for (int var9 = -70 % ((var1 - -64) / 37); (var7 + var2) > var8; ++var8) {
            for (int var10 = var4; var10 < var6 + var4; ++var10) {
                int var11 = 0;
                int var12 = 0;
                int var13 = 0;
                int var14 = 0;

                for (int var15 = var8 + -var5; var15 <= var5 + var8; ++var15) {
                    if (var15 >= 0 && var15 < this.width2) {
                        for (int var16 = -var3 + var10; var10 + var3 >= var16; ++var16) {
                            if (var16 >= 0 && this.height2 > var16) {
                                int var17 = this.pixels[this.width2 * var16 + var15];
                                ++var14;
                                var11 += var17 >> 16 & 255;
                                var13 += 255 & var17;
                                var12 += ('\uffc3' & var17) >> 8;
                            }
                        }
                    }
                }

                this.pixels[var10 * this.width2 + var8] = var13 / var14 + (var12 / var14 << 8) + (var11 / var14 << 16);
            }
        }
    }

    private void method399(int var1, int[] var2, int var3, int[] var4, boolean var5, int var6, int var7, int var8, int var9, int var10, int var11) {
        for (var7 = var8; var7 < 0; ++var7) {
            this.pixels[var3++] = var2[(var9 >> 17) * var1 + (var11 >> 17)];
            var11 += var6;
            var9 += var10;
        }

        if (!var5) {
            this.anIntArray566 = null;
        }
    }

    final void drawBox(int x, int y, int w, int h, int color) {
        if (x < this.boundsTopX) {
            w -= -x + this.boundsTopX;
            x = this.boundsTopX;
        }

        if (this.boundsTopY > y) {
            h -= this.boundsTopY - y;
            y = this.boundsTopY;
        }

        if ((x + w) > this.boundsBottomX) {
            w = -x + this.boundsBottomX;
        }

        if ((h + y) > this.boundsBottomY) {
            h = -y + this.boundsBottomY;
        }

        int var7 = this.width2 + -w;
        byte var8 = 1;
        if (this.interlace) {
            var8 = 2;
            var7 += this.width2;
            if ((y & 1) != 0) {
                ++y;
                --h;
            }
        }

        int var9 = x + this.width2 * y;

        for (int var10 = -h; var10 < 0; var10 += var8) {
            for (int var11 = -w; var11 < 0; ++var11) {
                this.pixels[var9++] = color;
            }

            var9 += var7;
        }
    }

    final void method401(byte var1, int var2, int var3, int var4) {
        if (this.boundsTopX <= var3 && var2 >= this.boundsTopY && this.boundsBottomX > var3 && this.boundsBottomY > var2) {
            this.pixels[var3 + var2 * this.width2] = var4;
            if (var1 != 118) {
                this.drawWorld(16, 86);
            }
        }
    }

    final void drawLineHoriz(int x, int y, int width, int colour) {
        if (y >= this.boundsTopY && y < this.boundsBottomY) {
            if (x < this.boundsTopX) {
                width -= -x + this.boundsTopX;
                x = this.boundsTopX;
            }

            if ((width + x) > this.boundsBottomX) {
                width = this.boundsBottomX - x;
            }

            if (width > 0) {
                int var6 = y * this.width2 + x;

                for (int var7 = 0; width > var7; ++var7) {
                    this.pixels[var7 + var6] = colour;
                }

            }
        }
    }

    public final synchronized boolean isConsumer(ImageConsumer var1) {
        return var1 == this.anImageConsumer593;
    }

    private void method403(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int[] var9, int[] var10, int var11, int var12) {
        int var13 = 116 % ((var12 - -37) / 60);
        int var14 = 256 - var3;

        for (int var15 = -var8; var15 < 0; var15 += var2) {
            for (int var16 = -var6; var16 < 0; ++var16) {
                var4 = var10[var11++];
                if (var4 != 0) {
                    int var17 = var9[var1];
                    var9[var1++] = Utility.bitwiseAnd(16711680, var3 * Utility.bitwiseAnd(var4, '\uff00') - -(var14 * Utility.bitwiseAnd(var17, '\uff00'))) + Utility.bitwiseAnd(var14 * Utility.bitwiseAnd(16711935, var17) + Utility.bitwiseAnd(var4, 16711935) * var3, -16711936) >> 8;
                } else {
                    ++var1;
                }
            }

            var1 += var7;
            var11 += var5;
        }
        ;
    }

    final void drawWorld(int var1, int var2) {
        int var3 = this.spriteWidth[var2] * this.spriteHeight[var2];
        if (var1 == -257) {
            int[] var4 = this.surfacePixels[var2];
            int[] var5 = new int['\u8000'];

            for (int var6 = 0; var6 < var3; ++var6) {
                int var7 = var4[var6];
                ++var5[(var7 >> 3 & 31) + (('\uf800' & var7) >> 6) + ((var7 & 16252928) >> 9)];
            }

            int[] var26 = new int[256];
            var26[0] = 16711935;
            int[] var8 = new int[256];

            int var11;
            int var12;
            for (int var9 = 0; var9 < '\u8000'; ++var9) {
                int var10 = var5[var9];
                if (var10 > var8[255]) {
                    for (var11 = 1; var11 < 256; ++var11) {
                        if (var8[var11] < var10) {
                            for (var12 = 255; var12 > var11; --var12) {
                                var26[var12] = var26[-1 + var12];
                                var8[var12] = var8[var12 + -1];
                            }

                            var26[var11] = (Utility.bitwiseAnd(31744, var9) << 9) + Utility.bitwiseAnd('\uf800', var9 << 6) + Utility.bitwiseAnd(248, var9 << 3) + 263172;
                            var8[var11] = var10;
                            break;
                        }
                    }
                }

                var5[var9] = -1;
            }

            byte[] var27 = new byte[var3];

            for (var11 = 0; var11 < var3; ++var11) {
                var12 = var4[var11];
                int var13 = (('\uf800' & var12) >> 6) + ((16252928 & var12) >> 9) + (var12 >> 3 & 31);
                int var14 = var5[var13];
                if (var14 == -1) {
                    int var15 = 999999999;
                    int var16 = var12 >> 16 & 255;
                    int var17 = ('\uff8f' & var12) >> 8;
                    int var18 = var12 & 255;

                    for (int var19 = 0; var19 < 256; ++var19) {
                        int var20 = var26[var19];
                        int var21 = (16740634 & var20) >> 16;
                        int var22 = (var20 & '\uff0b') >> 8;
                        int var23 = 255 & var20;
                        int var24 = (var18 + -var23) * (-var23 + var18) + (-var21 + var16) * (var16 - var21) + (var17 - var22) * (var17 - var22);
                        if (var15 > var24) {
                            var15 = var24;
                            var14 = var19;
                        }
                    }

                    var5[var13] = var14;
                }

                var27[var11] = (byte) var14;
            }

            this.spriteColoursUsed[var2] = var27;
            this.spriteColourList[var2] = var26;
            this.surfacePixels[var2] = null;
        }
    }

    private void method405(int var1, int[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, byte var11, int var12, int var13, int var14, int var15, int var16, int var17) {
        int var18 = (16720276 & var1) >> 16;
        if (var11 > -96) {
            this.drawBoxEdge(-98, -13, 119, -64, -14);
        }

        int var19 = 255 & var1 >> 8;
        int var20 = var1 & 255;
        int var21 = (16724553 & var16) >> 16;
        int var22 = ('\uff53' & var16) >> 8;
        int var23 = 255 & var16;

        try {
            int var24 = var5;

            for (int var25 = -var4; var25 < 0; ++var25) {
                int var26 = var17 * (var8 >> 16);
                int var27 = var10 >> 16;
                int var28 = var13;
                int var29;
                if (var27 < this.boundsTopX) {
                    var29 = -var27 + this.boundsTopX;
                    var27 = this.boundsTopX;
                    var5 += var29 * var15;
                    var28 = var13 - var29;
                }

                var14 = 1 + -var14;
                if ((var28 + var27) >= this.boundsBottomX) {
                    var29 = var27 - (-var28 - -this.boundsBottomX);
                    var28 -= var29;
                }

                var8 += var6;
                if (var14 != 0) {
                    for (var29 = var27; var27 - -var28 > var29; ++var29) {
                        var9 = var3[var26 + (var5 >> 16)];
                        var5 += var15;
                        if (var9 != 0) {
                            int var30 = 255 & var9 >> 16;
                            int var31 = var9 & 255;
                            int var32 = (var9 & '\uffae') >> 8;
                            if (var32 == var30 && var32 == var31) {
                                var2[var12 + var29] = (var31 * var20 >> 8) + ((var18 * var30 >> 8 << 16) - -(var32 * var19 >> 8 << 8));
                            } else if (var30 == 255 && var32 == var31) {
                                var2[var12 + var29] = (var31 * var23 >> 8) + (var21 * var30 >> 8 << 16) + (var32 * var22 >> 8 << 8);
                            } else {
                                var2[var29 + var12] = var9;
                            }
                        }
                    }
                }

                var10 += var7;
                var12 += this.width2;
                var5 = var24;
            }

        } catch (Exception var33) {
            System.out.println("error in transparent sprite plot routine");
        }
    }

    final void drawCircle(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (var4 != -1385529104) {
            this.anIntArray579 = null;
        }

        int var7 = 256 - var6;
        int var8 = var6 * (255 & var5 >> 16);
        int var9 = var6 * ((var5 & '\uff98') >> 8);
        int var10 = (255 & var5) * var6;
        int var11 = var3 + -var1;
        if (var11 < 0) {
            var11 = 0;
        }

        int var12 = var3 - -var1;
        if (this.height2 <= var12) {
            var12 = this.height2 - 1;
        }

        byte var13 = 1;
        if (this.interlace) {
            if ((1 & var11) != 0) {
                ++var11;
            }

            var13 = 2;
        }

        for (int var14 = var11; var14 <= var12; var14 += var13) {
            int var15 = var14 - var3;
            int var16 = (int) Math.sqrt((double) (var1 * var1 + -(var15 * var15)));
            int var17 = var2 + -var16;
            if (var17 < 0) {
                var17 = 0;
            }

            int var18 = var2 - -var16;
            if (var18 >= this.width2) {
                var18 = this.width2 + -1;
            }

            int var19 = var17 - -(var14 * this.width2);

            for (int var20 = var17; var18 >= var20; ++var20) {
                int var21 = ((16750808 & this.pixels[var19]) >> 16) * var7;
                int var22 = var7 * ((this.pixels[var19] & '\uff80') >> 8);
                int var23 = (this.pixels[var19] & 255) * var7;
                int var24 = (var23 + var10 >> 8) + (var21 + var8 >> 8 << 16) + (var22 + var9 >> 8 << 8);
                this.pixels[var19++] = var24;
            }
        }
        ;
    }

    final void setCrownStartId(int var1, int var2) {
        if (var2 > -63) {
            this.anIntArray653 = null;
        }

        this.crownStartId = var1;
    }

    final void resetBounds(byte var1) {
        if (var1 != 10) {
            this.drawstring((String) null, -38, 23, 36, 79, -41);
        }

        this.boundsBottomX = this.width2;
        this.boundsTopX = 0;
        this.boundsTopY = 0;
        this.boundsBottomY = this.height2;
    }

    final void method409(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, boolean var9, int var10) {
        try {
            if (var6 == 0) {
                var6 = 16777215;
            }

            if (var1 == 0) {
                var1 = 16777215;
            }

            int var11 = this.spriteWidth[var2];
            int var12 = this.spriteHeight[var2];
            int var13 = 0;
            int var14 = 0;
            int var15 = var10 << 16;
            int var16 = (var11 << 16) / var7;
            int var17 = (var12 << 16) / var5;
            int var18 = -(var10 << 16) / var5;
            int var19;
            int var20;
            if (this.spriteTranslate[var2]) {
                var19 = this.spriteWidthFull[var2];
                var20 = this.spriteHeightFull[var2];
                if (var19 == 0 || var20 == 0) {
                    return;
                }

                var16 = (var19 << 16) / var7;
                var17 = (var20 << 16) / var5;
                int var21 = this.spriteTranslateX[var2];
                if (var9) {
                    var21 = var19 + -this.spriteWidth[var2] - var21;
                }

                int var22 = this.spriteTranslateY[var2];
                var8 += (var19 + (var7 * var21 - 1)) / var19;
                int var23 = (-1 + var20 + var22 * var5) / var20;
                var15 += var23 * var18;
                var3 += var23;
                if (var22 * var5 % var20 != 0) {
                    var14 = (var20 - var5 * var22 % var20 << 16) / var5;
                }

                if ((var21 * var7 % var19) != 0) {
                    var13 = (-(var21 * var7 % var19) + var19 << 16) / var7;
                }

                var7 = ((this.spriteWidth[var2] << 16) - (var13 + -var16 + 1)) / var16;
                var5 = (-1 + var17 + -var14 + (this.spriteHeight[var2] << 16)) / var17;
            }

            var19 = var3 * this.width2;
            var15 += var8 << 16;
            if (var3 < this.boundsTopY) {
                var20 = this.boundsTopY + -var3;
                var3 = this.boundsTopY;
                var5 -= var20;
                var14 += var20 * var17;
                var15 += var18 * var20;
                var19 += var20 * this.width2;
            }

            if (this.boundsBottomY <= var5 + var3) {
                var5 -= 1 + -this.boundsBottomY + var3 + var5;
            }

            var20 = var19 / this.width2 & 1;
            if (!this.interlace) {
                var20 = 2;
            }

            if (var6 == 16777215) {
                if (this.surfacePixels[var2] != null) {
                    if (!var9) {
                        this.method372(var5, var16, var17, var11, (byte) -93, var20, this.pixels, 0, var7, var13, var18, var1, var15, var19, var14, this.surfacePixels[var2]);
                    } else {
                        this.method372(var5, -var16, var17, var11, (byte) -88, var20, this.pixels, 0, var7, -1 + ((this.spriteWidth[var2] << 16) - var13), var18, var1, var15, var19, var14, this.surfacePixels[var2]);
                    }
                } else if (var9) {
                    this.method382(0, var20, this.spriteColoursUsed[var2], this.pixels, var7, var15, var1, var17, -var13 + ((this.spriteWidth[var2] << 16) - 1), var18, -1321763928, var11, var19, var5, -var16, this.spriteColourList[var2], var14);
                } else {
                    this.method382(0, var20, this.spriteColoursUsed[var2], this.pixels, var7, var15, var1, var17, var13, var18, -1321763928, var11, var19, var5, var16, this.spriteColourList[var2], var14);
                }
            } else if (this.surfacePixels[var2] != null) {
                if (var9) {
                    this.method405(var1, this.pixels, this.surfacePixels[var2], var5, -var13 + (this.spriteWidth[var2] << 16) + -1, var17, var18, var14, 0, var15, (byte) -127, var19, var7, var20, -var16, var6, var11);
                } else {
                    this.method405(var1, this.pixels, this.surfacePixels[var2], var5, var13, var17, var18, var14, 0, var15, (byte) -119, var19, var7, var20, var16, var6, var11);
                }
            } else if (var9) {
                this.method418(0, this.spriteColourList[var2], var1, var18, this.spriteColoursUsed[var2], var5, var7, this.pixels, var20, var11, (byte) -21, var6, (this.spriteWidth[var2] << 16) + -var13 - 1, var19, var17, var15, -var16, var14);
            } else {
                this.method418(0, this.spriteColourList[var2], var1, var18, this.spriteColoursUsed[var2], var5, var7, this.pixels, var20, var11, (byte) -21, var6, var13, var19, var17, var15, var16, var14);
            }

            if (var4 >= -27) {
                this.method426(-99, -110, -43, 30, (byte) -29, -96);
            }
        } catch (Exception var24) {
            System.out.println("error in sprite clipping routine");
        }
    }

    public final boolean imageUpdate(Image var1, int var2, int var3, int var4, int var5, int var6) {
        return true;
    }

    private void method410(int var1, int[] var2, int var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
        if (var11 != 185018513) {
            this.drawCharacter(-121, 65, -121, 113, (byte[]) null, true);
        }

        for (int var12 = var6; var12 < 0; ++var12) {
            var8 = var4[(var5 >> 17) - -((var3 >> 17) * var1)];
            var5 += var10;
            if (var8 != 0) {
                this.pixels[var7++] = var8;
            } else {
                ++var7;
            }

            var3 += var9;
        }
    }

    public final synchronized void addConsumer(ImageConsumer var1) {
        this.anImageConsumer593 = var1;
        var1.setDimensions(this.width2, this.height2);
        var1.setProperties((Hashtable) null);
        var1.setColorModel(this.aColorModel634);
        var1.setHints(14);
    }

    public final void startProduction(ImageConsumer var1) {
        this.addConsumer(var1);
    }

    final void drawSpriteAlpha(int x, int y, int spriteId, int alpha) {
        if (this.spriteTranslate[spriteId]) {
            y += this.spriteTranslateY[spriteId];
            x += this.spriteTranslateX[spriteId];
        }

        int var6 = this.width2 * y + x;
        int var7 = 0;
        int var8 = this.spriteHeight[spriteId];
        int var9 = this.spriteWidth[spriteId];
        int var10 = -var9 + this.width2;
        int var11;
        if (this.boundsTopY > y) {
            var11 = this.boundsTopY - y;
            y = this.boundsTopY;
            var8 -= var11;
            var7 += var11 * var9;
            var6 += var11 * this.width2;
        }

        int var12 = 0;
        if (y - -var8 >= this.boundsBottomY) {
            var8 -= 1 + -this.boundsBottomY + y + var8;
        }

        if (x < this.boundsTopX) {
            var11 = -x + this.boundsTopX;
            var12 += var11;
            var10 += var11;
            var6 += var11;
            x = this.boundsTopX;
            var9 -= var11;
            var7 += var11;
        }

        if (x + var9 >= this.boundsBottomX) {
            var11 = x + var9 + (-this.boundsBottomX - -1);
            var10 += var11;
            var9 -= var11;
            var12 += var11;
        }

        if (var9 > 0 && var8 > 0) {
            byte var14 = 1;
            if (this.interlace) {
                var14 = 2;
                var10 += this.width2;
                var12 += this.spriteWidth[spriteId];
                if ((y & 1) != 0) {
                    --var8;
                    var6 += this.width2;
                }
            }

            if (this.surfacePixels[spriteId] != null) {
                this.method403(var6, var14, alpha, 0, var12, var9, var10, var8, this.pixels, this.surfacePixels[spriteId], var7, 28);
            } else {
                this.method427(this.spriteColoursUsed[spriteId], var10, false, var7, alpha, var14, this.pixels, var9, var8, var12, this.spriteColourList[spriteId], var6);
            }
        }
        ;
    }

    final int textWidth(int var1, int var2, String var3) {
        int var4 = 0;
        byte[] var5 = gameFonts[var1];
        if (var2 != -127) {
            GameData.npcWidth = null;
        }

        for (int var6 = 0; var3.length() > var6; ++var6) {
            if (var3.charAt(var6) == 64 && var6 - -4 < var3.length() && var3.charAt(4 + var6) == 64) {
                var6 += 4;
            } else if (var3.charAt(var6) == 126 && (4 + var6) < var3.length() && var3.charAt(4 + var6) == 126) {
                var6 += 4;
            } else {
                char var7 = var3.charAt(var6);
                if (var7 < 0 || var7 >= JagGrab.caracterWidth.length) {
                    var7 = 32;
                }

                var4 += var5[7 + JagGrab.caracterWidth[var7]];
            }
        }

        return var4;
    }

    private void drawSprite(int var1, int[] var2, int var3, int[] var4, int var5, int var6, byte var7, int var8, byte[] var9, int var10, int var11) {
        int var12 = 40 / ((var7 - -20) / 49);
        int var13 = -(var3 >> 2);
        var3 = -(var3 & 3);

        for (int var14 = -var10; var14 < 0; var14 += var5) {
            for (int var15 = var13; var15 < 0; ++var15) {
                byte var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(var16, 255)];
                } else {
                    ++var6;
                }

                var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(var16, 255)];
                } else {
                    ++var6;
                }

                var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(255, var16)];
                } else {
                    ++var6;
                }

                var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(255, var16)];
                } else {
                    ++var6;
                }
            }

            for (int var19 = var3; var19 < 0; ++var19) {
                byte var17 = var9[var8++];
                if (var17 == 0) {
                    ++var6;
                } else {
                    var2[var6++] = var4[Utility.bitwiseAnd(255, var17)];
                }
            }

            var6 += var11;
            var8 += var1;
        }
        ;
    }

    final void drawstringRight(int var1, int var2, int var3, int var4, String var5, int var6) {
        this.method377(var1, true, var4, var2, 0, var3, var5);
        if (var6 == -5169) {
        }
    }

    private void method415(int[] var1, int var2, int var3, int var4, int[] var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, boolean var14, int var15) {
        if (var14) {
            GameData.itemBasePrice = null;
        }

        int var16 = -var15 + 256;

        try {
            int var17 = var13;

            for (int var18 = -var9; var18 < 0; var18 += var11) {
                int var19 = var4 * (var12 >> 16);
                var12 += var2;

                for (int var20 = -var6; var20 < 0; ++var20) {
                    var10 = var5[(var13 >> 16) + var19];
                    var13 += var8;
                    if (var10 == 0) {
                        ++var7;
                    } else {
                        int var21 = var1[var7];
                        var1[var7++] = Utility.bitwiseAnd(-16711936, Utility.bitwiseAnd(var21, 16711935) * var16 + Utility.bitwiseAnd(var10, 16711935) * var15) + Utility.bitwiseAnd(16711680, Utility.bitwiseAnd('\uff00', var21) * var16 + var15 * Utility.bitwiseAnd('\uff00', var10)) >> 8;
                    }
                }

                var13 = var17;
                var7 += var3;
            }

        } catch (Exception var22) {
            System.out.println("error in tran_scale");
        }
        ;
    }

    final void fade2black(int var1) {
        if (var1 != -915682524) {
            this.anImage578 = null;
        }

        int var2 = this.height2 * this.width2;

        for (int var3 = 0; var2 > var3; ++var3) {
            int var4 = this.pixels[var3] & 16777215;
            this.pixels[var3] = Utility.bitwiseAnd(var4 >>> 4, -2146496753) + ((Utility.bitwiseAnd(var4, 16711423) >>> 1) - -Utility.bitwiseAnd(-1069596865, var4 >>> 2)) + (Utility.bitwiseAnd(var4, 16316664) >>> 3);
        }
        ;
    }

    private void drawstring(int var1, int var2, int var3, int var4, int var5, String var6, int var7) {
        this.drawstring(var6, var3 - this.textWidth(var4, -127, var6) / var5, var1, var4, var2, var7);
    }

    private void method418(int var1, int[] var2, int var3, int var4, byte[] var5, int var6, int var7, int[] var8, int var9, int var10, byte var11, int var12, int var13, int var14, int var15, int var16, int var17, int var18) {
        int var19 = 255 & var3 >> 16;
        int var20 = 255 & var3 >> 8;
        int var21 = 255 & var3;
        if (var11 == -21) {
            int var22 = var12 >> 16 & 255;
            int var23 = (var12 & '\uff6d') >> 8;
            int var24 = 255 & var12;

            try {
                int var25 = var13;

                for (int var26 = -var6; var26 < 0; ++var26) {
                    int var27 = var10 * (var18 >> 16);
                    int var28 = var16 >> 16;
                    int var29 = var7;
                    int var30;
                    if (var28 < this.boundsTopX) {
                        var30 = -var28 + this.boundsTopX;
                        var29 = var7 - var30;
                        var13 += var17 * var30;
                        var28 = this.boundsTopX;
                    }

                    if (var28 - -var29 >= this.boundsBottomX) {
                        var30 = var29 + (var28 - this.boundsBottomX);
                        var29 -= var30;
                    }

                    var9 = -var9 + 1;
                    if (var9 != 0) {
                        for (var30 = var28; var30 < (var29 + var28); ++var30) {
                            var1 = var5[(var13 >> 16) - -var27] & 255;
                            var13 += var17;
                            if (var1 != 0) {
                                var1 = var2[var1];
                                int var31 = var1 & 255;
                                int var32 = var1 >> 8 & 255;
                                int var33 = (var1 & 16739633) >> 16;
                                if (var33 == var32 && var32 == var31) {
                                    var8[var14 + var30] = (var32 * var20 >> 8 << 8) + (var19 * var33 >> 8 << 16) + (var21 * var31 >> 8);
                                } else if (var33 == 255 && var32 == var31) {
                                    var8[var30 - -var14] = (var31 * var24 >> 8) + (var22 * var33 >> 8 << 16) + (var32 * var23 >> 8 << 8);
                                } else {
                                    var8[var30 + var14] = var1;
                                }
                            }
                        }
                    }

                    var18 += var15;
                    var13 = var25;
                    var14 += this.width2;
                    var16 += var4;
                }

            } catch (Exception var34) {
                System.out.println("error in transparent sprite plot routine");
            }
        }
        ;
    }

    private void drawCharacter(int width, int x, int y, int colour, byte[] font, boolean noPlotLetter) {
        int i1 = x - -font[width - -5];
        int j1 = y + -font[6 + width];
        int k1 = font[3 + width];
        int l1 = font[width - -4];
        int i2 = font[2 + width] + 128 * font[1 + width] + font[width] * 16384;
        int j2 = j1 * this.width2 + i1;
        int k2 = -k1 + this.width2;
        if (j1 < this.boundsTopY) {
            int i3 = -j1 + this.boundsTopY;
            j2 += this.width2 * i3;
            j1 = this.boundsTopY;
            i2 += k1 * i3;
            l1 -= i3;
        }

        int l2 = 0;
        if (l1 + j1 >= this.boundsBottomY) {
            l1 -= -this.boundsBottomY + j1 + l1 + 1;
        }

        if (this.boundsTopX > i1) {
            int j3 = this.boundsTopX + -i1;
            l2 += j3;
            j2 += j3;
            i2 += j3;
            k1 -= j3;
            k2 += j3;
            i1 = this.boundsTopX;
        }

        if (this.boundsBottomX <= k1 + i1) {
            int k3 = 1 + -this.boundsBottomX + i1 + k1;
            k1 -= k3;
            l2 += k3;
            k2 += k3;
        }

        if (k1 > 0 && l1 > 0) {
            if (!noPlotLetter) {
                this.plotLetter(this.pixels, font, colour, i2, j2, k1, l1, k2, l2);
            } else {
                this.method396(this.pixels, font, colour, i2, j2, k1, l1, k2, l2);
            }
        }
        ;
    }

    void method420(int var1, int var2, int var3, int var4, int var5, byte var6, int var7, int var8) {
        if (var6 >= -53) {
            this.textWidth(99, 44, (String) null);
        }

        this.method426(var8, var3, var1, var4, (byte) -92, var2);
    }

    final void drawstring(String text, int x, int y, int foint, int color) {
        this.drawstring(text, x, y, foint, color, 0);
    }

    private void plot_scale(int var1, int var2, int var3, int var4, int[] var5, int var6, int var7, int var8, int var9, int var10, int[] var11, int var12, int var13, int var14) {
        try {
            int var15 = var8;
            int var16 = -var1;
            if (var6 == -15265) {
                while (var16 < 0) {
                    int var17 = (var12 >> 16) * var4;
                    var12 += var13;

                    for (int var18 = -var10; var18 < 0; ++var18) {
                        var2 = var11[var17 + (var8 >> 16)];
                        var8 += var7;
                        if (var2 == 0) {
                            ++var14;
                        } else {
                            var5[var14++] = var2;
                        }
                    }

                    var8 = var15;
                    var14 += var3;
                    var16 += var9;
                }

            }
        } catch (Exception var19) {
            System.out.println("error in plot_scale");
        }
    }

    final void centrepara(int var1, boolean var2, boolean var3, int var4, int var5, String var6, int var7, int var8) {
        try {
            int var9 = 0;
            byte[] var10 = gameFonts[var4];
            int var11 = 0;
            int var12 = 0;

            for (int var13 = 0; var13 < var6.length(); ++var13) {
                if (var6.charAt(var13) == 64 && (4 + var13) < var6.length() && var6.charAt(var13 + 4) == 64) {
                    var13 += 4;
                } else if (var6.charAt(var13) == 126 && var6.length() > (var13 - -4) && var6.charAt(4 + var13) == 126) {
                    var13 += 4;
                } else {
                    char var14 = var6.charAt(var13);
                    if (var14 < 0 || var14 >= JagGrab.caracterWidth.length) {
                        var14 = 32;
                    }

                    var9 += var10[JagGrab.caracterWidth[var14] - -7];
                }

                if (var6.charAt(var13) == 32) {
                    var12 = var13;
                }

                if (var6.charAt(var13) == 37 && var3) {
                    var9 = 1000;
                    var12 = var13;
                }

                if (var9 > var7) {
                    if (var11 >= var12) {
                        var12 = var13;
                    }

                    var9 = 0;
                    this.drawstring(var8, var5, var1, var4, 2, var6.substring(var11, var12), 0);
                    var8 += this.textHeight(var4, true);
                    var11 = var13 = var12 + 1;
                }
            }

            if (var9 > 0) {
                this.drawstring(var8, var5, var1, var4, 2, var6.substring(var11), 0);
            }
        } catch (Exception var15) {
            System.out.println("centrepara: " + var15);
            var15.printStackTrace();
        }

        if (!var2) {
        }
    }

    final void method424(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.spriteWidth[var5] = var2;
        this.spriteHeight[var5] = var1;
        this.spriteTranslate[var5] = false;
        this.spriteTranslateX[var5] = 0;
        this.spriteTranslateY[var5] = 0;
        this.spriteWidthFull[var5] = var2;
        this.spriteHeightFull[var5] = var1;
        int var7 = var2 * var1;
        this.surfacePixels[var5] = new int[var7];
        int var8 = var4;

        for (int var9 = var6; var9 < (var2 + var6); ++var9) {
            for (int var10 = var3; var1 + var3 > var10; ++var10) {
                this.surfacePixels[var5][var8++] = this.pixels[this.width2 * var10 + var9];
            }
        }
    }

    final void method425(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        try {
            int var8 = this.spriteWidth[var4];
            int var9 = this.spriteHeight[var4];
            int var10 = 0;
            if (var5 > -38) {
                this.anIntArray610 = null;
            }

            int var11 = 0;
            int var12 = (var8 << 16) / var1;
            int var13 = (var9 << 16) / var6;
            int var14;
            int var15;
            if (this.spriteTranslate[var4]) {
                var14 = this.spriteWidthFull[var4];
                var15 = this.spriteHeightFull[var4];
                if (var14 == 0 || var15 == 0) {
                    return;
                }

                var7 += (var6 * this.spriteTranslateY[var4] - -var15 - 1) / var15;
                if (this.spriteTranslateX[var4] * var1 % var14 != 0) {
                    var10 = (-(this.spriteTranslateX[var4] * var1 % var14) + var14 << 16) / var1;
                }

                var2 += (-1 + (var1 * this.spriteTranslateX[var4] - -var14)) / var14;
                var13 = (var15 << 16) / var6;
                var12 = (var14 << 16) / var1;
                if ((this.spriteTranslateY[var4] * var6 % var15) != 0) {
                    var11 = (var15 + -(this.spriteTranslateY[var4] * var6 % var15) << 16) / var6;
                }

                var1 = var1 * (-(var10 >> 16) + this.spriteWidth[var4]) / var14;
                var6 = var6 * (this.spriteHeight[var4] + -(var11 >> 16)) / var15;
            }

            var14 = var2 - -(this.width2 * var7);
            int var16;
            if (var7 < this.boundsTopY) {
                var16 = this.boundsTopY - var7;
                var6 -= var16;
                var7 = 0;
                var11 += var13 * var16;
                var14 += var16 * this.width2;
            }

            var15 = this.width2 + -var1;
            if (var2 < this.boundsTopX) {
                var16 = this.boundsTopX - var2;
                var10 += var12 * var16;
                var1 -= var16;
                var2 = 0;
                var14 += var16;
                var15 += var16;
            }

            if (this.boundsBottomY <= var7 + var6) {
                var6 -= var6 + var7 + -this.boundsBottomY + 1;
            }

            if ((var2 - -var1) >= this.boundsBottomX) {
                var16 = -this.boundsBottomX + var2 + var1 + 1;
                var15 += var16;
                var1 -= var16;
            }

            byte var19 = 1;
            if (this.interlace) {
                var15 += this.width2;
                if ((1 & var7) != 0) {
                    var14 += this.width2;
                    --var6;
                }

                var19 = 2;
                var13 += var13;
            }

            this.method415(this.pixels, var13, var15, var8, this.surfacePixels[var4], var1, var14, var12, var6, 0, var19, var11, var10, false, var3);
        } catch (Exception var17) {
            System.out.println("error in sprite clipping routine");
        }
    }

    final void method426(int var1, int var2, int var3, int var4, byte var5, int var6) {
        try {
            int var7 = this.spriteWidth[var4];
            int var8 = this.spriteHeight[var4];
            int var9 = 0;
            int var10 = 36 % ((var5 - -36) / 36);
            int var11 = 0;
            int var12 = (var7 << 16) / var6;
            int var13 = (var8 << 16) / var2;
            int var14;
            int var15;
            if (this.spriteTranslate[var4]) {
                var14 = this.spriteWidthFull[var4];
                var15 = this.spriteHeightFull[var4];
                if (var14 == 0 || var15 == 0) {
                    return;
                }

                var12 = (var14 << 16) / var6;
                var13 = (var15 << 16) / var2;
                if (this.spriteTranslateY[var4] * var2 % var15 != 0) {
                    var11 = (-(var2 * this.spriteTranslateY[var4] % var15) + var15 << 16) / var2;
                }

                var3 += (-1 + var6 * this.spriteTranslateX[var4] + var14) / var14;
                if ((var6 * this.spriteTranslateX[var4] % var14) != 0) {
                    var9 = (-(this.spriteTranslateX[var4] * var6 % var14) + var14 << 16) / var6;
                }

                var1 += (var15 + var2 * this.spriteTranslateY[var4] + -1) / var15;
                var6 = var6 * (-(var9 >> 16) + this.spriteWidth[var4]) / var14;
                var2 = (-(var11 >> 16) + this.spriteHeight[var4]) * var2 / var15;
            }

            var14 = var3 - -(this.width2 * var1);
            int var16;
            if (this.boundsTopY > var1) {
                var16 = -var1 + this.boundsTopY;
                var11 += var13 * var16;
                var2 -= var16;
                var14 += var16 * this.width2;
                var1 = 0;
            }

            var15 = -var6 + this.width2;
            if (var3 < this.boundsTopX) {
                var16 = -var3 + this.boundsTopX;
                var15 += var16;
                var6 -= var16;
                var3 = 0;
                var14 += var16;
                var9 += var12 * var16;
            }

            if (this.boundsBottomY <= var2 + var1) {
                var2 -= 1 + (var1 + var2 - this.boundsBottomY);
            }

            if (this.boundsBottomX <= (var3 - -var6)) {
                var16 = -this.boundsBottomX + (var3 - -var6) - -1;
                var6 -= var16;
                var15 += var16;
            }

            byte var19 = 1;
            if (this.interlace) {
                var13 += var13;
                var15 += this.width2;
                var19 = 2;
                if ((1 & var1) != 0) {
                    --var2;
                    var14 += this.width2;
                }
            }

            this.plot_scale(var2, 0, var15, var7, this.pixels, -15265, var12, var9, var19, var6, this.surfacePixels[var4], var11, var13, var14);
        } catch (Exception var17) {
            System.out.println("error in sprite clipping routine");
        }
    }

    private void method427(byte[] var1, int var2, boolean var3, int var4, int var5, int var6, int[] var7, int var8, int var9, int var10, int[] var11, int var12) {
        int var13 = -var5 + 256;
        int var14 = -var9;
        if (var3) {
            this.spriteHeightFull = null;
        }

        while (var14 < 0) {
            for (int var15 = -var8; var15 < 0; ++var15) {
                byte var16 = var1[var4++];
                if (var16 != 0) {
                    int var19 = var11[var16 & 255];
                    int var17 = var7[var12];
                    var7[var12++] = Utility.bitwiseAnd(var13 * Utility.bitwiseAnd(16711935, var17) + Utility.bitwiseAnd(16711935, var19) * var5, -16711936) + Utility.bitwiseAnd(var5 * Utility.bitwiseAnd(var19, '\uff00') + Utility.bitwiseAnd(var17, '\uff00') * var13, 16711680) >> 8;
                } else {
                    ++var12;
                }
            }

            var4 += var10;
            var12 += var2;
            var14 += var6;
        }
        ;
    }

}
