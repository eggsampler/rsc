import java.io.IOException;
import java.net.Socket;

final class ClientProxyDirect extends ClientProxyBase {

    final Socket getSystemProxy(int skip) throws IOException {
        return skip != 0 ? null : this.openDirectConnection();
    }

}
