final class BufferBase_Sub2_Sub2 extends BufferBase_Sub2 {

    private int anInt1716;
    private int anInt1717;
    private int anInt1718;
    private int anInt1719;
    private int anInt1720;
    private int anInt1721;
    private int anInt1722;
    private int anInt1723;
    private boolean aBoolean1724;
    private int anInt1725;
    private int anInt1726;
    private int anInt1727;
    private int anInt1728;
    private int anInt1729;
    private int anInt1730;


    private BufferBase_Sub2_Sub2(BufferBase_Sub4_Sub1 var1, int var2, int var3) {
        super.aClass8_Sub4_1634 = var1;
        this.anInt1725 = var1.anInt1742;
        this.anInt1727 = var1.anInt1745;
        this.aBoolean1724 = var1.aBoolean1746;
        this.anInt1730 = var2;
        this.anInt1720 = var3;
        this.anInt1717 = 8192;
        this.anInt1728 = 0;
        this.method184();
    }

    private static int method178(int var0, int var1) {
        return var1 < 0 ? var0 : (int) ((double) var0 * Math.sqrt((double) (16384 - var1) * 1.220703125E-4D) + 0.5D);
    }

    private static int method179(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, BufferBase_Sub2_Sub2 var9) {
        var2 >>= 8;
        var8 >>= 8;
        var4 <<= 2;
        var5 <<= 2;
        if ((var6 = var3 + var8 - var2) > var7) {
            var6 = var7;
        }

        var9.anInt1723 += var9.anInt1726 * (var6 - var3);
        var9.anInt1719 += var9.anInt1718 * (var6 - var3);

        int var10001;
        for (var6 -= 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
        }

        for (var6 += 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
        }

        var9.anInt1729 = var4 >> 2;
        var9.anInt1728 = var2 << 8;
        return var3;
    }

    static BufferBase_Sub2_Sub2 method180(BufferBase_Sub4_Sub1 var0, int var1, int var2) {
        return var0.aByteArray1744 != null && var0.aByteArray1744.length != 0 ? new BufferBase_Sub2_Sub2(var0, (int) ((long) var0.anInt1743 * 256L * (long) var1 / (long) (100 * SoundPlayer.sampleRate)), var2 << 6) : null;
    }

    private static int method181(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, BufferBase_Sub2_Sub2 var12) {
        var3 >>= 8;
        var11 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        var7 <<= 2;
        var8 <<= 2;
        if ((var9 = var4 + var11 - var3) > var10) {
            var9 = var10;
        }

        var12.anInt1729 += var12.anInt1721 * (var9 - var4);
        var4 <<= 1;
        var9 <<= 1;

        int var10001;
        byte var13;
        for (var9 -= 6; var4 < var9; var6 += var8) {
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        for (var9 += 6; var4 < var9; var6 += var8) {
            var13 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        var12.anInt1723 = var5 >> 2;
        var12.anInt1719 = var6 >> 2;
        var12.anInt1728 = var3 << 8;
        return var4 >> 1;
    }

    private static int method182(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, BufferBase_Sub2_Sub2 var10, int var11, int var12) {
        if (var11 == 0 || (var7 = var5 + (var9 + 256 - var4 + var11) / var11) > var8) {
            var7 = var8;
        }

        int var10001;
        while (var5 < var7) {
            var1 = var4 >> 8;
            byte var13 = var2[var1 - 1];
            var10001 = var5++;
            var3[var10001] += ((var13 << 8) + (var2[var1] - var13) * (var4 & 255)) * var6 >> 6;
            var4 += var11;
        }

        if (var11 == 0 || (var7 = var5 + (var9 - var4 + var11) / var11) > var8) {
            var7 = var8;
        }

        var0 = var12;

        for (var1 = var11; var5 < var7; var4 += var1) {
            var10001 = var5++;
            var3[var10001] += ((var0 << 8) + (var2[var4 >> 8] - var0) * (var4 & 255)) * var6 >> 6;
        }

        var10.anInt1728 = var4;
        return var5;
    }

    private static int method183(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, BufferBase_Sub2_Sub2 var11, int var12, int var13) {
        var11.anInt1723 -= var11.anInt1726 * var5;
        var11.anInt1719 -= var11.anInt1718 * var5;
        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 257) / var12) > var9) {
            var8 = var9;
        }

        int var10001;
        byte var14;
        while (var5 < var8) {
            var1 = var4 >> 8;
            var14 = var2[var1];
            var10001 = var5++;
            var3[var10001] += ((var14 << 8) + (var2[var1 + 1] - var14) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
            var4 += var12;
        }

        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 1) / var12) > var9) {
            var8 = var9;
        }

        for (var1 = var13; var5 < var8; var4 += var12) {
            var14 = var2[var4 >> 8];
            var10001 = var5++;
            var3[var10001] += ((var14 << 8) + (var1 - var14) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
        }

        var11.anInt1723 += var11.anInt1726 * var5;
        var11.anInt1719 += var11.anInt1718 * var5;
        var11.anInt1729 = var6;
        var11.anInt1728 = var4;
        return var5;
    }

    private static int method185(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, BufferBase_Sub2_Sub2 var13, int var14, int var15) {
        var13.anInt1729 -= var13.anInt1721 * var5;
        if (var14 == 0 || (var10 = var5 + (var12 - var4 + var14 - 257) / var14) > var11) {
            var10 = var11;
        }

        var5 <<= 1;

        int var10001;
        byte var16;
        for (var10 <<= 1; var5 < var10; var4 += var14) {
            var1 = var4 >> 8;
            var16 = var2[var1];
            var0 = (var16 << 8) + (var2[var1 + 1] - var16) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        if (var14 == 0 || (var10 = (var5 >> 1) + (var12 - var4 + var14 - 1) / var14) > var11) {
            var10 = var11;
        }

        var10 <<= 1;

        for (var1 = var15; var5 < var10; var4 += var14) {
            var16 = var2[var4 >> 8];
            var0 = (var16 << 8) + (var1 - var16) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        var5 >>= 1;
        var13.anInt1729 += var13.anInt1721 * var5;
        var13.anInt1723 = var6;
        var13.anInt1719 = var7;
        var13.anInt1728 = var4;
        return var5;
    }

    private static int method187(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, BufferBase_Sub2_Sub2 var9) {
        var2 >>= 8;
        var8 >>= 8;
        var4 <<= 2;
        var5 <<= 2;
        if ((var6 = var3 + var2 - (var8 - 1)) > var7) {
            var6 = var7;
        }

        var9.anInt1723 += var9.anInt1726 * (var6 - var3);
        var9.anInt1719 += var9.anInt1718 * (var6 - var3);

        int var10001;
        for (var6 -= 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var4 += var5;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
        }

        for (var6 += 3; var3 < var6; var4 += var5) {
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
        }

        var9.anInt1729 = var4 >> 2;
        var9.anInt1728 = var2 << 8;
        return var3;
    }

    private static int method189(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, BufferBase_Sub2_Sub2 var10) {
        var3 >>= 8;
        var9 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        if ((var7 = var4 + var3 - (var9 - 1)) > var8) {
            var7 = var8;
        }

        var4 <<= 1;
        var7 <<= 1;

        byte var11;
        int var10001;
        for (var7 -= 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        for (var7 += 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        var10.anInt1728 = var3 << 8;
        return var4 >> 1;
    }

    private static int method190(int var0, int var1) {
        return var1 < 0 ? -var0 : (int) ((double) var0 * Math.sqrt((double) var1 * 1.220703125E-4D) + 0.5D);
    }

    private static int method191(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, BufferBase_Sub2_Sub2 var13, int var14, int var15) {
        var13.anInt1729 -= var13.anInt1721 * var5;
        if (var14 == 0 || (var10 = var5 + (var12 + 256 - var4 + var14) / var14) > var11) {
            var10 = var11;
        }

        var5 <<= 1;

        int var10001;
        for (var10 <<= 1; var5 < var10; var4 += var14) {
            var1 = var4 >> 8;
            byte var16 = var2[var1 - 1];
            var0 = (var16 << 8) + (var2[var1] - var16) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        if (var14 == 0 || (var10 = (var5 >> 1) + (var12 - var4 + var14) / var14) > var11) {
            var10 = var11;
        }

        var10 <<= 1;

        for (var1 = var15; var5 < var10; var4 += var14) {
            var0 = (var1 << 8) + (var2[var4 >> 8] - var1) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var6 += var8;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
            var7 += var9;
        }

        var5 >>= 1;
        var13.anInt1729 += var13.anInt1721 * var5;
        var13.anInt1723 = var6;
        var13.anInt1719 = var7;
        var13.anInt1728 = var4;
        return var5;
    }

    private static int method192(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, BufferBase_Sub2_Sub2 var10, int var11, int var12) {
        if (var11 == 0 || (var7 = var5 + (var9 - var4 + var11 - 257) / var11) > var8) {
            var7 = var8;
        }

        int var10001;
        byte var13;
        while (var5 < var7) {
            var1 = var4 >> 8;
            var13 = var2[var1];
            var10001 = var5++;
            var3[var10001] += ((var13 << 8) + (var2[var1 + 1] - var13) * (var4 & 255)) * var6 >> 6;
            var4 += var11;
        }

        if (var11 == 0 || (var7 = var5 + (var9 - var4 + var11 - 1) / var11) > var8) {
            var7 = var8;
        }

        for (var1 = var12; var5 < var7; var4 += var11) {
            var13 = var2[var4 >> 8];
            var10001 = var5++;
            var3[var10001] += ((var13 << 8) + (var1 - var13) * (var4 & 255)) * var6 >> 6;
        }

        var10.anInt1728 = var4;
        return var5;
    }

    private static int method193(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, BufferBase_Sub2_Sub2 var11, int var12, int var13) {
        if (var12 == 0 || (var8 = var5 + (var10 + 256 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        var5 <<= 1;

        int var10001;
        for (var8 <<= 1; var5 < var8; var4 += var12) {
            var1 = var4 >> 8;
            byte var14 = var2[var1 - 1];
            var0 = (var14 << 8) + (var2[var1] - var14) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        if (var12 == 0 || (var8 = (var5 >> 1) + (var10 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        var8 <<= 1;

        for (var1 = var13; var5 < var8; var4 += var12) {
            var0 = (var1 << 8) + (var2[var4 >> 8] - var1) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        var11.anInt1728 = var4;
        return var5 >> 1;
    }

    private static int method194(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, BufferBase_Sub2_Sub2 var8) {
        var2 >>= 8;
        var7 >>= 8;
        var4 <<= 2;
        if ((var5 = var3 + var7 - var2) > var6) {
            var5 = var6;
        }

        int var10001;
        for (var5 -= 3; var3 < var5; var1[var10001] += var0[var2++] * var4) {
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2++] * var4;
            var10001 = var3++;
        }

        for (var5 += 3; var3 < var5; var1[var10001] += var0[var2++] * var4) {
            var10001 = var3++;
        }

        var8.anInt1728 = var2 << 8;
        return var3;
    }

    private static int method195(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, BufferBase_Sub2_Sub2 var10) {
        var3 >>= 8;
        var9 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        if ((var7 = var4 + var9 - var3) > var8) {
            var7 = var8;
        }

        var4 <<= 1;
        var7 <<= 1;

        byte var11;
        int var10001;
        for (var7 -= 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
            var2[var10001] += var11 * var6;
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        for (var7 += 6; var4 < var7; var2[var10001] += var11 * var6) {
            var11 = var1[var3++];
            var10001 = var4++;
            var2[var10001] += var11 * var5;
            var10001 = var4++;
        }

        var10.anInt1728 = var3 << 8;
        return var4 >> 1;
    }

    private static int method197(byte[] var0, int[] var1, int var2, int var3, int var4, int var5, int var6, int var7, BufferBase_Sub2_Sub2 var8) {
        var2 >>= 8;
        var7 >>= 8;
        var4 <<= 2;
        if ((var5 = var3 + var2 - (var7 - 1)) > var6) {
            var5 = var6;
        }

        int var10001;
        for (var5 -= 3; var3 < var5; var1[var10001] += var0[var2--] * var4) {
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var10001 = var3++;
            var1[var10001] += var0[var2--] * var4;
            var10001 = var3++;
        }

        for (var5 += 3; var3 < var5; var1[var10001] += var0[var2--] * var4) {
            var10001 = var3++;
        }

        var8.anInt1728 = var2 << 8;
        return var3;
    }

    private static int method198(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, BufferBase_Sub2_Sub2 var11, int var12, int var13) {
        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12 - 257) / var12) > var9) {
            var8 = var9;
        }

        var5 <<= 1;

        int var10001;
        byte var14;
        for (var8 <<= 1; var5 < var8; var4 += var12) {
            var1 = var4 >> 8;
            var14 = var2[var1];
            var0 = (var14 << 8) + (var2[var1 + 1] - var14) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        if (var12 == 0 || (var8 = (var5 >> 1) + (var10 - var4 + var12 - 1) / var12) > var9) {
            var8 = var9;
        }

        var8 <<= 1;

        for (var1 = var13; var5 < var8; var4 += var12) {
            var14 = var2[var4 >> 8];
            var0 = (var14 << 8) + (var1 - var14) * (var4 & 255);
            var10001 = var5++;
            var3[var10001] += var0 * var6 >> 6;
            var10001 = var5++;
            var3[var10001] += var0 * var7 >> 6;
        }

        var11.anInt1728 = var4;
        return var5 >> 1;
    }

    private static int method200(int var0, byte[] var1, int[] var2, int var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, BufferBase_Sub2_Sub2 var12) {
        var3 >>= 8;
        var11 >>= 8;
        var5 <<= 2;
        var6 <<= 2;
        var7 <<= 2;
        var8 <<= 2;
        if ((var9 = var4 + var3 - (var11 - 1)) > var10) {
            var9 = var10;
        }

        var12.anInt1729 += var12.anInt1721 * (var9 - var4);
        var4 <<= 1;
        var9 <<= 1;

        int var10001;
        byte var13;
        for (var9 -= 6; var4 < var9; var6 += var8) {
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
            var6 += var8;
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        for (var9 += 6; var4 < var9; var6 += var8) {
            var13 = var1[var3--];
            var10001 = var4++;
            var2[var10001] += var13 * var5;
            var5 += var7;
            var10001 = var4++;
            var2[var10001] += var13 * var6;
        }

        var12.anInt1723 = var5 >> 2;
        var12.anInt1719 = var6 >> 2;
        var12.anInt1728 = var3 << 8;
        return var4 >> 1;
    }

    private static int method201(int var0, int var1, byte[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, BufferBase_Sub2_Sub2 var11, int var12, int var13) {
        var11.anInt1723 -= var11.anInt1726 * var5;
        var11.anInt1719 -= var11.anInt1718 * var5;
        if (var12 == 0 || (var8 = var5 + (var10 + 256 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        int var10001;
        while (var5 < var8) {
            var1 = var4 >> 8;
            byte var14 = var2[var1 - 1];
            var10001 = var5++;
            var3[var10001] += ((var14 << 8) + (var2[var1] - var14) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
            var4 += var12;
        }

        if (var12 == 0 || (var8 = var5 + (var10 - var4 + var12) / var12) > var9) {
            var8 = var9;
        }

        var0 = var13;

        for (var1 = var12; var5 < var8; var4 += var1) {
            var10001 = var5++;
            var3[var10001] += ((var0 << 8) + (var2[var4 >> 8] - var0) * (var4 & 255)) * var6 >> 6;
            var6 += var7;
        }

        var11.anInt1723 += var11.anInt1726 * var5;
        var11.anInt1719 += var11.anInt1718 * var5;
        var11.anInt1729 = var6;
        var11.anInt1728 = var4;
        return var5;
    }

    final BufferBase_Sub2 method165() {
        return null;
    }

    private void method184() {
        this.anInt1729 = this.anInt1720;
        this.anInt1723 = method178(this.anInt1720, this.anInt1717);
        this.anInt1719 = method190(this.anInt1720, this.anInt1717);
    }

    private int method186(int[] var1, int var2, int var3, int var4, int var5) {
        do {
            if (this.anInt1716 <= 0) {
                if (this.anInt1730 == 256 && (this.anInt1728 & 255) == 0) {
                    if (SoundPlayer.stereo) {
                        return method195(0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, 0, var4, var3, this);
                    }

                    return method194(((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, 0, var4, var3, this);
                }

                if (SoundPlayer.stereo) {
                    return method198(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, 0, var4, var3, this, this.anInt1730, var5);
                }

                return method192(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, 0, var4, var3, this, this.anInt1730, var5);
            }

            int var6 = var2 + this.anInt1716;
            if (var6 > var4) {
                var6 = var4;
            }

            this.anInt1716 += var2;
            if (this.anInt1730 == 256 && (this.anInt1728 & 255) == 0) {
                if (SoundPlayer.stereo) {
                    var2 = method181(0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, this.anInt1726, this.anInt1718, 0, var6, var3, this);
                } else {
                    var2 = method179(((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, this.anInt1721, 0, var6, var3, this);
                }
            } else if (SoundPlayer.stereo) {
                var2 = method185(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, this.anInt1726, this.anInt1718, 0, var6, var3, this, this.anInt1730, var5);
            } else {
                var2 = method183(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, this.anInt1721, 0, var6, var3, this, this.anInt1730, var5);
            }

            this.anInt1716 -= var2;
            if (this.anInt1716 != 0) {
                return var2;
            }
        } while (!this.method188());

        return var4;
    }

    final synchronized void method170(int var1) {
        if (this.anInt1716 > 0) {
            if (var1 >= this.anInt1716) {
                if (this.anInt1720 == Integer.MIN_VALUE) {
                    this.anInt1720 = 0;
                    this.anInt1729 = this.anInt1723 = this.anInt1719 = 0;
                    this.method160();
                    var1 = this.anInt1716;
                }

                this.anInt1716 = 0;
                this.method184();
            } else {
                this.anInt1729 += this.anInt1721 * var1;
                this.anInt1723 += this.anInt1726 * var1;
                this.anInt1719 += this.anInt1718 * var1;
                this.anInt1716 -= var1;
            }
        }

        BufferBase_Sub4_Sub1 var2 = (BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634;
        int var3 = this.anInt1725 << 8;
        int var4 = this.anInt1727 << 8;
        int var5 = var2.aByteArray1744.length << 8;
        int var6 = var4 - var3;
        if (var6 <= 0) {
            this.anInt1722 = 0;
        }

        if (this.anInt1728 < 0) {
            if (this.anInt1730 <= 0) {
                this.method199();
                this.method160();
                return;
            }

            this.anInt1728 = 0;
        }

        if (this.anInt1728 >= var5) {
            if (this.anInt1730 >= 0) {
                this.method199();
                this.method160();
                return;
            }

            this.anInt1728 = var5 - 1;
        }

        this.anInt1728 += this.anInt1730 * var1;
        if (this.anInt1722 < 0) {
            if (!this.aBoolean1724) {
                if (this.anInt1730 < 0) {
                    if (this.anInt1728 < var3) {
                        this.anInt1728 = var4 - 1 - (var4 - 1 - this.anInt1728) % var6;
                    }
                } else if (this.anInt1728 >= var4) {
                    this.anInt1728 = var3 + (this.anInt1728 - var3) % var6;
                }
            } else {
                if (this.anInt1730 < 0) {
                    if (this.anInt1728 >= var3) {
                        return;
                    }

                    this.anInt1728 = var3 + var3 - 1 - this.anInt1728;
                    this.anInt1730 = -this.anInt1730;
                }

                while (this.anInt1728 >= var4) {
                    this.anInt1728 = var4 + var4 - 1 - this.anInt1728;
                    this.anInt1730 = -this.anInt1730;
                    if (this.anInt1728 >= var3) {
                        return;
                    }

                    this.anInt1728 = var3 + var3 - 1 - this.anInt1728;
                    this.anInt1730 = -this.anInt1730;
                }

            }
        } else {
            if (this.anInt1722 > 0) {
                if (this.aBoolean1724) {
                    label126:
                    {
                        if (this.anInt1730 < 0) {
                            if (this.anInt1728 >= var3) {
                                return;
                            }

                            this.anInt1728 = var3 + var3 - 1 - this.anInt1728;
                            this.anInt1730 = -this.anInt1730;
                            if (--this.anInt1722 == 0) {
                                break label126;
                            }
                        }

                        do {
                            if (this.anInt1728 < var4) {
                                return;
                            }

                            this.anInt1728 = var4 + var4 - 1 - this.anInt1728;
                            this.anInt1730 = -this.anInt1730;
                            if (--this.anInt1722 == 0) {
                                break;
                            }

                            if (this.anInt1728 >= var3) {
                                return;
                            }

                            this.anInt1728 = var3 + var3 - 1 - this.anInt1728;
                            this.anInt1730 = -this.anInt1730;
                        } while (--this.anInt1722 != 0);
                    }
                } else {
                    int var7;
                    if (this.anInt1730 < 0) {
                        if (this.anInt1728 >= var3) {
                            return;
                        }

                        var7 = (var4 - 1 - this.anInt1728) / var6;
                        if (var7 < this.anInt1722) {
                            this.anInt1728 += var6 * var7;
                            this.anInt1722 -= var7;
                            return;
                        }

                        this.anInt1728 += var6 * this.anInt1722;
                        this.anInt1722 = 0;
                    } else {
                        if (this.anInt1728 < var4) {
                            return;
                        }

                        var7 = (this.anInt1728 - var3) / var6;
                        if (var7 < this.anInt1722) {
                            this.anInt1728 -= var6 * var7;
                            this.anInt1722 -= var7;
                            return;
                        }

                        this.anInt1728 -= var6 * this.anInt1722;
                        this.anInt1722 = 0;
                    }
                }
            }

            if (this.anInt1730 < 0) {
                if (this.anInt1728 < 0) {
                    this.anInt1728 = -1;
                    this.method199();
                    this.method160();
                    return;
                }
            } else if (this.anInt1728 >= var5) {
                this.anInt1728 = var5;
                this.method199();
                this.method160();
            }

        }
    }

    private boolean method188() {
        int var1 = this.anInt1720;
        int var2;
        int var3;
        if (var1 == Integer.MIN_VALUE) {
            var2 = 0;
            var3 = 0;
            var1 = 0;
        } else {
            var3 = method178(var1, this.anInt1717);
            var2 = method190(var1, this.anInt1717);
        }

        if (this.anInt1729 == var1 && this.anInt1723 == var3 && this.anInt1719 == var2) {
            if (this.anInt1720 == Integer.MIN_VALUE) {
                this.anInt1720 = 0;
                this.anInt1729 = this.anInt1723 = this.anInt1719 = 0;
                this.method160();
                return true;
            } else {
                this.method184();
                return false;
            }
        } else {
            if (this.anInt1729 < var1) {
                this.anInt1721 = 1;
                this.anInt1716 = var1 - this.anInt1729;
            } else if (this.anInt1729 > var1) {
                this.anInt1721 = -1;
                this.anInt1716 = this.anInt1729 - var1;
            } else {
                this.anInt1721 = 0;
            }

            if (this.anInt1723 < var3) {
                this.anInt1726 = 1;
                if (this.anInt1716 == 0 || this.anInt1716 > var3 - this.anInt1723) {
                    this.anInt1716 = var3 - this.anInt1723;
                }
            } else if (this.anInt1723 > var3) {
                this.anInt1726 = -1;
                if (this.anInt1716 == 0 || this.anInt1716 > this.anInt1723 - var3) {
                    this.anInt1716 = this.anInt1723 - var3;
                }
            } else {
                this.anInt1726 = 0;
            }

            if (this.anInt1719 < var2) {
                this.anInt1718 = 1;
                if (this.anInt1716 == 0 || this.anInt1716 > var2 - this.anInt1719) {
                    this.anInt1716 = var2 - this.anInt1719;
                }
            } else if (this.anInt1719 > var2) {
                this.anInt1718 = -1;
                if (this.anInt1716 == 0 || this.anInt1716 > this.anInt1719 - var2) {
                    this.anInt1716 = this.anInt1719 - var2;
                }
            } else {
                this.anInt1718 = 0;
            }

            return false;
        }
    }

    final int method167() {
        return this.anInt1720 == 0 && this.anInt1716 == 0 ? 0 : 1;
    }

    final synchronized void method164(int[] var1, int var2, int var3) {
        if (this.anInt1720 == 0 && this.anInt1716 == 0) {
            this.method170(var3);
        } else {
            BufferBase_Sub4_Sub1 var4 = (BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634;
            int var5 = this.anInt1725 << 8;
            int var6 = this.anInt1727 << 8;
            int var7 = var4.aByteArray1744.length << 8;
            int var8 = var6 - var5;
            if (var8 <= 0) {
                this.anInt1722 = 0;
            }

            int var9 = var2;
            var3 += var2;
            if (this.anInt1728 < 0) {
                if (this.anInt1730 <= 0) {
                    this.method199();
                    this.method160();
                    return;
                }

                this.anInt1728 = 0;
            }

            if (this.anInt1728 >= var7) {
                if (this.anInt1730 >= 0) {
                    this.method199();
                    this.method160();
                    return;
                }

                this.anInt1728 = var7 - 1;
            }

            if (this.anInt1722 < 0) {
                if (this.aBoolean1724) {
                    if (this.anInt1730 < 0) {
                        var9 = this.method196(var1, var2, var5, var3, var4.aByteArray1744[this.anInt1725]);
                        if (this.anInt1728 >= var5) {
                            return;
                        }

                        this.anInt1728 = var5 + var5 - 1 - this.anInt1728;
                        this.anInt1730 = -this.anInt1730;
                    }

                    while (true) {
                        var9 = this.method186(var1, var9, var6, var3, var4.aByteArray1744[this.anInt1727 - 1]);
                        if (this.anInt1728 < var6) {
                            return;
                        }

                        this.anInt1728 = var6 + var6 - 1 - this.anInt1728;
                        this.anInt1730 = -this.anInt1730;
                        var9 = this.method196(var1, var9, var5, var3, var4.aByteArray1744[this.anInt1725]);
                        if (this.anInt1728 >= var5) {
                            return;
                        }

                        this.anInt1728 = var5 + var5 - 1 - this.anInt1728;
                        this.anInt1730 = -this.anInt1730;
                    }
                } else if (this.anInt1730 < 0) {
                    while (true) {
                        var9 = this.method196(var1, var9, var5, var3, var4.aByteArray1744[this.anInt1727 - 1]);
                        if (this.anInt1728 >= var5) {
                            return;
                        }

                        this.anInt1728 = var6 - 1 - (var6 - 1 - this.anInt1728) % var8;
                    }
                } else {
                    while (true) {
                        var9 = this.method186(var1, var9, var6, var3, var4.aByteArray1744[this.anInt1725]);
                        if (this.anInt1728 < var6) {
                            return;
                        }

                        this.anInt1728 = var5 + (this.anInt1728 - var5) % var8;
                    }
                }
            } else {
                if (this.anInt1722 > 0) {
                    if (this.aBoolean1724) {
                        label133:
                        {
                            if (this.anInt1730 < 0) {
                                var9 = this.method196(var1, var2, var5, var3, var4.aByteArray1744[this.anInt1725]);
                                if (this.anInt1728 >= var5) {
                                    return;
                                }

                                this.anInt1728 = var5 + var5 - 1 - this.anInt1728;
                                this.anInt1730 = -this.anInt1730;
                                if (--this.anInt1722 == 0) {
                                    break label133;
                                }
                            }

                            do {
                                var9 = this.method186(var1, var9, var6, var3, var4.aByteArray1744[this.anInt1727 - 1]);
                                if (this.anInt1728 < var6) {
                                    return;
                                }

                                this.anInt1728 = var6 + var6 - 1 - this.anInt1728;
                                this.anInt1730 = -this.anInt1730;
                                if (--this.anInt1722 == 0) {
                                    break;
                                }

                                var9 = this.method196(var1, var9, var5, var3, var4.aByteArray1744[this.anInt1725]);
                                if (this.anInt1728 >= var5) {
                                    return;
                                }

                                this.anInt1728 = var5 + var5 - 1 - this.anInt1728;
                                this.anInt1730 = -this.anInt1730;
                            } while (--this.anInt1722 != 0);
                        }
                    } else {
                        int var10;
                        if (this.anInt1730 < 0) {
                            while (true) {
                                var9 = this.method196(var1, var9, var5, var3, var4.aByteArray1744[this.anInt1727 - 1]);
                                if (this.anInt1728 >= var5) {
                                    return;
                                }

                                var10 = (var6 - 1 - this.anInt1728) / var8;
                                if (var10 >= this.anInt1722) {
                                    this.anInt1728 += var8 * this.anInt1722;
                                    this.anInt1722 = 0;
                                    break;
                                }

                                this.anInt1728 += var8 * var10;
                                this.anInt1722 -= var10;
                            }
                        } else {
                            while (true) {
                                var9 = this.method186(var1, var9, var6, var3, var4.aByteArray1744[this.anInt1725]);
                                if (this.anInt1728 < var6) {
                                    return;
                                }

                                var10 = (this.anInt1728 - var5) / var8;
                                if (var10 >= this.anInt1722) {
                                    this.anInt1728 -= var8 * this.anInt1722;
                                    this.anInt1722 = 0;
                                    break;
                                }

                                this.anInt1728 -= var8 * var10;
                                this.anInt1722 -= var10;
                            }
                        }
                    }
                }

                if (this.anInt1730 < 0) {
                    this.method196(var1, var9, 0, var3, 0);
                    if (this.anInt1728 < 0) {
                        this.anInt1728 = -1;
                        this.method199();
                        this.method160();
                        return;
                    }
                } else {
                    this.method186(var1, var9, var7, var3, 0);
                    if (this.anInt1728 >= var7) {
                        this.anInt1728 = var7;
                        this.method199();
                        this.method160();
                    }
                }

            }
        }
    }

    final BufferBase_Sub2 method169() {
        return null;
    }

    final int method168() {
        int var1 = this.anInt1729 * 3 >> 6;
        var1 = (var1 ^ var1 >> 31) + (var1 >>> 31);
        if (this.anInt1722 == 0) {
            var1 -= var1 * this.anInt1728 / (((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744.length << 8);
        } else if (this.anInt1722 >= 0) {
            var1 -= var1 * this.anInt1725 / ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744.length;
        }

        return var1 > 255 ? 255 : var1;
    }

    private int method196(int[] var1, int var2, int var3, int var4, int var5) {
        do {
            if (this.anInt1716 <= 0) {
                if (this.anInt1730 == -256 && (this.anInt1728 & 255) == 0) {
                    if (SoundPlayer.stereo) {
                        return method189(0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, 0, var4, var3, this);
                    }

                    return method197(((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, 0, var4, var3, this);
                }

                if (SoundPlayer.stereo) {
                    return method193(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, 0, var4, var3, this, this.anInt1730, var5);
                }

                return method182(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, 0, var4, var3, this, this.anInt1730, var5);
            }

            int var6 = var2 + this.anInt1716;
            if (var6 > var4) {
                var6 = var4;
            }

            this.anInt1716 += var2;
            if (this.anInt1730 == -256 && (this.anInt1728 & 255) == 0) {
                if (SoundPlayer.stereo) {
                    var2 = method200(0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, this.anInt1726, this.anInt1718, 0, var6, var3, this);
                } else {
                    var2 = method187(((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, this.anInt1721, 0, var6, var3, this);
                }
            } else if (SoundPlayer.stereo) {
                var2 = method191(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1723, this.anInt1719, this.anInt1726, this.anInt1718, 0, var6, var3, this, this.anInt1730, var5);
            } else {
                var2 = method201(0, 0, ((BufferBase_Sub4_Sub1) super.aClass8_Sub4_1634).aByteArray1744, var1, this.anInt1728, var2, this.anInt1729, this.anInt1721, 0, var6, var3, this, this.anInt1730, var5);
            }

            this.anInt1716 -= var2;
            if (this.anInt1716 != 0) {
                return var2;
            }
        } while (!this.method188());

        return var4;
    }

    private void method199() {
        if (this.anInt1716 != 0) {
            if (this.anInt1720 == Integer.MIN_VALUE) {
                this.anInt1720 = 0;
            }

            this.anInt1716 = 0;
            this.method184();
        }

    }
}
