import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.*;
import java.net.Proxy.Type;
import java.nio.charset.Charset;
import java.util.List;

public final class ClientProxyIndirect extends ClientProxyBase {

    private ProxySelector proxySelector = ProxySelector.getDefault();

    static ClientProxyBase create(String address, int port) {
        ClientProxyBase var3;
        try {
            var3 = (ClientProxyBase) Class.forName("ClientProxyIndirect").newInstance();
        } catch (Throwable var4) {
            var3 = new ClientProxyDirect();
        }
        var3.port = port;
        var3.address = address;
        return var3;
    }


    private Socket openProxyConnection(String address, int port, String var3) throws IOException {
        Socket socket = new Socket(address, port);
        socket.setSoTimeout(10000);
        OutputStream socketOut = socket.getOutputStream();
        if (var3 != null) {
            socketOut.write(("CONNECT " + this.address + ":" + this.port + " HTTP/1.0\n" + var3 + "\n\n").getBytes(Charset.forName("ISO-8859-1")));
        } else {
            socketOut.write(("CONNECT " + this.address + ":" + this.port + " HTTP/1.0\n\n").getBytes(Charset.forName("ISO-8859-1")));
        }

        socketOut.flush();
        BufferedReader socketReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String line = socketReader.readLine();
        if (null != line) {
            if (line.startsWith("HTTP/1.0 200") || line.startsWith("HTTP/1.1 200")) {
                return socket;
            }

            if (line.startsWith("HTTP/1.0 407") || line.startsWith("HTTP/1.1 407")) {
                int index = 0;
                line = socketReader.readLine();

                for (String header = "proxy-authenticate: "; line != null && 50 > index; line = socketReader.readLine()) {
                    if (line.toLowerCase().startsWith(header)) {
                        line = line.substring(header.length()).trim();
                        int spaceIndex = line.indexOf(' ');
                        if (-1 != spaceIndex) {
                            line = line.substring(0, spaceIndex);
                        }

                        throw new ProxyAuthenticationException(line);
                    }

                    ++index;
                }

                throw new ProxyAuthenticationException("");
            }
        }

        socketOut.close();
        socketReader.close();
        socket.close();
        return null;
    }

    private Socket authenticate(Proxy proxy) throws IOException {
        if (proxy.type() != Type.DIRECT) {
            SocketAddress socketAddress = proxy.address();
            if (!(socketAddress instanceof InetSocketAddress)) {
                return null;
            } else {
                InetSocketAddress inetAddress = (InetSocketAddress) socketAddress;
                if (proxy.type() == Type.HTTP) {
                    String var15 = null;

                    try {
                        Class classAuthInfo = Class.forName("sun.net.www.protocol.http.AuthenticationInfo");
                        Method methodGetProxyAuth = classAuthInfo.getDeclaredMethod("getProxyAuth", new Class[]{String.class, Integer.TYPE});
                        methodGetProxyAuth.setAccessible(true);
                        Object authInfo = methodGetProxyAuth.invoke((Object) null, new Object[]{inetAddress.getHostName(), new Integer(inetAddress.getPort())});
                        if (null != authInfo) {
                            Method supportsPreemptiveAuth = classAuthInfo.getDeclaredMethod("supportsPreemptiveAuthorization", new Class[0]);
                            supportsPreemptiveAuth.setAccessible(true);
                            if (((Boolean) supportsPreemptiveAuth.invoke(authInfo, new Object[0])).booleanValue()) {
                                Method var10 = classAuthInfo.getDeclaredMethod("getHeaderName", new Class[0]);
                                var10.setAccessible(true);
                                Method var11 = classAuthInfo.getDeclaredMethod("getHeaderValue", new Class[]{URL.class, String.class});
                                var11.setAccessible(true);
                                String var12 = (String) var10.invoke(authInfo, new Object[0]);
                                String var13 = (String) var11.invoke(authInfo, new Object[]{new URL("https://" + this.address + "/"), "https"});
                                var15 = var12 + ": " + var13;
                            }
                        }
                    } catch (Exception var14) {
                        ;
                    }

                    return this.openProxyConnection(inetAddress.getHostName(), inetAddress.getPort(), var15);
                } else if (proxy.type() == Type.SOCKS) {
                    Socket var5 = new Socket(proxy);
                    var5.connect(new InetSocketAddress(this.address, this.port));
                    return var5;
                } else {
                    return null;
                }
            }
        } else {
            return this.openDirectConnection();
        }
    }

    final Socket getSystemProxy(int skip) throws IOException {
        boolean useSystemProxies = Boolean.parseBoolean(System.getProperty("java.net.useSystemProxies"));
        if (!useSystemProxies) {
            System.setProperty("java.net.useSystemProxies", "true");
        }

        boolean useSsl = 443 == this.port;

        List<Proxy> proxiesSsl;
        List<Proxy> proxiesRegular;
        try {
            proxiesSsl = this.proxySelector.select(new URI((!useSsl ? "http" : "https") + "://" + this.address));
            proxiesRegular = this.proxySelector.select(new URI((useSsl ? "http" : "https") + "://" + this.address));
        } catch (URISyntaxException var15) {
            return this.openDirectConnection();
        }

        proxiesSsl.addAll(proxiesRegular);
        Proxy[] proxies = proxiesSsl.toArray(new Proxy[0]);
        ProxyAuthenticationException error = null;

        for (int index = skip; proxies.length > index; ++index) {
            Proxy proxy = proxies[index];

            try {
                Socket var12 = this.authenticate(proxy);
                if (var12 != null) {
                    return var12;
                }
            } catch (ProxyAuthenticationException var13) {
                error = var13;
            } catch (IOException var14) {
                ;
            }
        }

        if (error != null) {
            throw error;
        } else {
            return this.openDirectConnection();
        }
    }

}
